<?php  
?>
<!DOCTYPE html>
<html class="bg-blue">
    <head>
        <meta charset="UTF-8">
        <title>:: Welcome to Seven Oceans Commercials ::</title>
		<link rel="shortcut icon" href="favicon.ico">

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
		<style type="text/css">
		form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
		</style>
    </head> 
    <body class="bg-white" style="background-color:#FFFFFF;" >
        <div class="form-box" id="login-box" align="center" style="margin-top:70px;" >
		<!--	<img src="./img/slogo.png"> -->
			<!--<table width="100%">
			<tr>
			<td><img src="./img/logo.png"></td>
		<td style="float:right"><label style="letter-spacing:0.7px;">Seven Oceans' Commercial</label><br><label style="letter-spacing:0.7px; font-style:italic; float:right">Enterprise</label></td>
			</tr>
			</table>-->
			
            <div style="height:15px;"></div>
            <div class="header">Hotel Manorama</div>
            <form method="post" name="login-form" id="login-form" enctype="multipart/form-data" action="checklogin.php">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="txtUser" id="txtUser" class="form-control" placeholder="User ID" autocomplete="off"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="txtPass" id="txtPass" class="form-control" placeholder="Password"/>
                    </div>          
                    <div class="form-group">
                        <!--<input type="checkbox" name="remember_me"/> Remember me-->
                    </div>
                </div>
                <div class="footer" style="border:1px solid #eaeaec;">                                                               
                    <button type="submit" class="btn bg-blue btn-block">Sign me in</button>
                </div>
            </form>
		<!--	<div><a href="brokerlogin.html" style="float:left; text-decoration:none; margin-top:2px;" title="Broker Login">Broker's Login</a><a href="agentlogin.html" style="float:right; text-decoration:none; margin-top:2px;" title="Agent Login">Agent's Login</a></div>-->
			
			<div style="height:15px;"></div>
			
		<!--	<div class="margin text-right">
              <label style="letter-spacing:0.7px;">Seven Oceans Commercials</label><br>
			  <label style="letter-spacing:0.7px; font-style:italic;">Cloud & Enterprise</label>

            </div> -->
			<div class="margin text-center">
               &nbsp;

            </div>
			<div class="margin text-center">
               &nbsp;<!--<span>A Seven Oceans Holdings production</span>-->

            </div>
			<div class="margin text-center">
               &nbsp;<!--<span>A Seven Oceans Holdings production</span>-->

            </div>
        </div>
 <div class="footer" align="right">                                                               
	<img src="img/Logo2.jpg" height="14" width="14">&nbsp;<a href="http://seowebmantra.com" target="_blank" style="font-weight:bold;">Seowebmantra.com production</a>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
<!-- jQuery 2.0.2 -->
<script src="js/jquery-1.8.3.js"></script>
<!-- Bootstrap -->
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
		
<script type="text/javascript">
$(document).ready(function(){ 
$("#login-form").validate({
rules: {
	txtUser: "required",
	txtPass: "required"
	},
messages: {
	txtUser : "*",
	txtPass : "*"
	}
});
}); 

</script>
</body>
</html>
