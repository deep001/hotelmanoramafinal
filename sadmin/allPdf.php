<?php
session_start();
require_once("../includes/functions_sadmin.inc.php");
include_once("../includes/fpdf.php");
@$i = $_REQUEST['id'];
$ret = "";
switch ($i)
{
  case 1:
        $ret = create_audit_trail_pdf();
				break;
	default:
		    //$ret = funError();
				//echo $ret;
				break; 				
}

function create_audit_trail_pdf()
{
	$obj = new data();
	$obj->funConnect();
	
	$loginid = $_REQUEST['loginid'];
class PDF extends FPDF
	{
	function Header()
				{
					$obj = new data();
					$obj->funConnect();
					
					$this->SetFillColor(255,255,255);
					$image_path1 = '../img/logo1.jpg';
					$this->Image($image_path1,87,3,15,15);
					$this->SetFont('Arial','',10);
					$this->SetTextColor(220, 99, 30);
					$this->SetXY(140,13);
					$this->Cell(0,5,"Audit Trail",0,1,'R',1);	
					$this->SetFont('Arial','',10);
					$this->SetFillColor(255,255,255);
					$this->SetTextColor(177,175,175);
					$this->Cell(190,1,"_________________________________________________________________________________________________",0,1,'L','1');
					$this->Ln(2);
				}
			
			function Footer()
				{
					$this->SetY(-25);
					//Select Arial italic 8
					$this->SetFillColor(255,255,255);
					$this->SetTextColor(177,175,175);
					$this->SetFont('Arial','',10);
					$this->Cell(190,1,"_________________________________________________________________________________________________",0,1,'L','1');
					$this->Ln(1);
					$this->SetFont('Arial','',6);
					//$this->Cell(190,4,"BW Maritime.",0,1,'C');
					
					if($_SESSION['company'] == 1)
					{
						$this->Cell(190,4,"BW Maritime",0,1,'C');
						$this->Cell(190,4,"Mapletree Business City, #18-01, 10 Pasir Panjang Road, Singapore 117438, Tel: +65 6337 2133",0,1,'C');
						$this->Cell(190,4,"E-mail: enquiries@bwshipping.com  Website:www.bwshipping.com",0,1,'C');
					}
					else if($_SESSION['company'] == 2)
					{
						$this->Cell(190,4,"BW GAS.",0,1,'C');
						$this->Cell(190,4,"Professor Kohts vei 5 1366 Lysaker, Norway Tel: +47 6721 1600",0,1,'C');
						$this->Cell(190,4,"E-mail: bwgas@bwgas.com",0,1,'C');
					}
				}
	}

$pdf = new PDF();
$pdf->AddPage('P');
$pdf->SetDisplayMode('real','single');
$pdf->SetTitle('Audit Trail Pdf');
$pdf->SetAuthor('Seven Oceans Holdings');
$pdf->SetSubject('Audit Trail Pdf');
$pdf->SetKeywords('Designed and Developed By Seven Oceans Holdings pvt. Ltd.');

$pdf->Ln(5);

$arr1 = $arr2 = $arr3 = array();	
$pdf->SetFont('Arial','B',7);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L','L','L','L');
$arr2=array(40,50,36,35,30);
$arr3=array("Name","Company Name","Username","Password","Update On Date");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$sql = "select * from audit_trail where LOGINID='".$loginid."'";
$res = mysql_query($sql) or die(mysql_error());
$rec = mysql_num_rows($res);

$sql1 = "select * from login where LOGINID='".$loginid."'";
$res1 = mysql_query($sql1)  or die(mysql_error());
$rows1 = mysql_fetch_assoc($res1);

$i = 0;
if($rec == 0)
{
	$arr1 = $arr2 = $arr3 = array();	
	$i=$i+1;
	if($i % 2 == 0)
	{
		$pdf->SetFillColor(244,244,244);
	}
	else
	{
		$pdf->SetFillColor(255,255,255);
	}
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('C');
	$arr2=array(191);
	$arr3=array("sorry, currently there are zero records");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
}
else
{
	while($rows = mysql_fetch_assoc($res))
	{
		$arr1 = $arr2 = $arr3 = array();	
		$i=$i+1;
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(244,244,244);
		}
		else
		{
			$pdf->SetFillColor(255,255,255);
		}
		
		$pdf->SetFont('Arial','',7);
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L','L');
		$arr2=array(40,50,36,35,30);
		$arr3=array($rows1['CONTACT_PERSON'],$obj->getCompanyData($rows1['MCOMPANYID'],"COMPANY_NAME"),$rows['USERNAME'],$rows['PASSWORD'],date("d-M-Y",strtotime($rows['DATE_ON'])));
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		$pdf->Row2($arr3);
	}
}

$filename = "Audit Trail (".$rows1['CONTACT_PERSON'].") ".date("d-M-Y",time()).".pdf";

$pdf->Output($filename,'D');
}

?>