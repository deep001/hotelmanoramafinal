<?php 
session_start();
require_once("../includes/display_sadmin.inc.php");
require_once("../includes/functions_sadmin.inc.php");
$obj = new data();
$display = new display();
$display->logout_sa();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertCompanyDetails();
	header('Location:./company_creation.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-suitcase"></i>&nbsp;Company Creation&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Company Creation</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div align="right"><a href="company_creation.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				
				<!--<div class="col-md-6">-->
				<div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">ADD NEW COMPANY DETAILS</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="txtCName">Company Name</label>
                                            <input type="text" class="form-control" id="txtCName" name="txtCName" placeholder="Company Name" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <label for="txtCName">Company Short Name</label>
                                            <input type="text" class="form-control" id="txtShortName" name="txtShortName" placeholder="Company Short Name" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label for="txtCID">Company ID</label>
                                            <input type="text" class="form-control" id="txtCID" name="txtCID" placeholder="Company ID" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label>Company Address</label>
                                            <textarea class="form-control" rows="3" name="txtAddress" id="txtAddress" placeholder="Company Address"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtPhoneNo">Phone Number</label>
                                            <input type="text" class="form-control" id="txtPhoneNo" name="txtPhoneNo" placeholder="Phone Number" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label for="txtEmailid">E-mail ID</label>
                                            <input type="email" class="form-control" id="txtEmailid" name="txtEmailid" placeholder="E-mail ID" autocomplete="off">
                                        </div>
										<div class="form-group">
                                            <label for="txtEmailid">Vendor Code(Copy from Vendor Master in Internal User)</label>
                                            <input type="text" class="form-control" id="txtVendorCode" name="txtVendorCode" placeholder="Vendor Code" autocomplete="off">
                                        </div>
                                    </div><!-- /.box-body -->
                                    <div class="box-footer" align="center">
                                        <button type="submit" class="btn btn-primary btn-flat">Submit</button><input type="hidden" name="action" value="submit" />
                                    </div>
                                </form>
                            </div>
					<!--</div>			-->
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src='../js/jquery.autosize.js'></script>
<link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.alerts.js"></script>
<script src="../js/timer.js" type="text/javascript"></script>

 <script type="text/javascript">
 $(document).ready(function(){ 
 
           $('#txtAddress').autosize({append: "\n"});
			$("#frm1").validate({
			rules: {
			txtCName: {required :true },
			txtCID: {required: true},
			txtAddress: {required: true},
			txtEmailid: {required: true ,email :true},
			txtShortName: {required: true}
			},
		messages: {
			txtCName: {required :"*" },
			txtCID: {required: "*"},
			txtAddress: {required: "*"},
			txtEmailid: {required: "*" }
			},
		submitHandler: function(form)  {
				jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../img/loading.gif"  />', 'Alert');
				$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
				$("#popup_content").css({"background":"none","text-align":"center"});
				$("#popup_ok,#popup_title").hide();  
				form.submit();
			}
		});
});
        </script>
    </body>
</html>