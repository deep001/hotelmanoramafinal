<?php 
session_start();
require_once("../../includes/display_external_agent_dryout.inc.php");  
require_once("../../includes/functions_external_agent_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if(@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertLoadPortCostDetails();
	header('Location:./index.php');
 } 
$pagename = basename($_SERVER['PHP_SELF']);
$cost_sheet_id = $obj->getLatestCostSheetID($_SESSION['comid']);
$rdoExchange = $obj->getLoadPortCostData($_SESSION['uid'],"RDO_EXCHANGE");
if($rdoExchange=='' || $rdoExchange==NULL)
{
	$rdoExchange = 1;
}

$coaid = $obj->getCompareEstimateData($_SESSION['comid'],'COAID');
if($coaid!='')
{
	$sqlcoa = "select * from coa_master where COAID='".$coaid."'";
	$resultcoa = mysql_query($sqlcoa) or die($sqlcoa);
	$rowscoa   = mysql_fetch_assoc($resultcoa);
	$currency = $rowscoa['CURRENCY'];
}
else
{
	$currency = "USD";
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
	
	$('#rdoExchange1').on('ifChecked', function () { 
	    $("#txtERate").removeAttr('readonly');
		$('[id^=txtEstmdCostUSD_]').attr('readonly',true);
		$('[id^=txtEstmdCost_]').removeAttr('readonly');
	});
	 
	$('#rdoExchange1').on('ifUnchecked', function () { 
	    $("#txtERate").attr('readonly',true);
		$('[id^=txtEstmdCostUSD_]').removeAttr('readonly');
		$('[id^=txtEstmdCost_]').attr('readonly',true);
	});
	
	$('#rdoExchange2').on('ifChecked', function () { 
	    $("#txtERate").attr('readonly',true);
		$('[id^=txtEstmdCostUSD_]').removeAttr('readonly');
		$('[id^=txtEstmdCost_]').attr('readonly',true);
	});
	 
	$('#rdoExchange2').on('ifUnchecked', function () { 
	    $("#txtERate").removeAttr('readonly');
		$('[id^=txtEstmdCostUSD_]').attr('readonly',true);
		$('[id^=txtEstmdCost_]').removeAttr('readonly');
	});
	
	$('[id^=txtRemarksOperator_],[id^=txtRemarksAgent_],#txtBDetails').autosize({append: "\n"});
	$("#txtERate,[id^=txtEstmdCost_],[id^=txtEstmdCostUSD_],[id^=txtActualCost_],[id^=txtActualCostUSD_],#txtAdvActualCost,#txtFinalPayment,#txtAdvRequestedCost1,#txtAdvRequestedCost2").numeric();
	getCalculateAttr();
});


function getCalculateAttr()
{
	if($("#rdoExchange1").is(":checked"))
	{
		$("#txtERate").removeAttr('readonly');
		$('[id^=txtEstmdCostUSD_]').attr('readonly',true);
		$('[id^=txtEstmdCost_]').removeAttr('readonly');
	}
	if($("#rdoExchange2").is(":checked"))
	{
		$("#txtERate").attr('readonly',true);
		$('[id^=txtEstmdCostUSD_]').removeAttr('readonly');
		$('[id^=txtEstmdCost_]').attr('readonly',true);
	}
}

function calculateEstmdCostUSD(var1)
{
	if($("#rdoExchange1").is(":checked"))
	{
		if($("#txtERate").val() == ""){$e_rate = 0;}else{$e_rate = $("#txtERate").val();}
		if($("#txtEstmdCost_"+var1).val() == ""){$cost = 0;}else{$cost = $("#txtEstmdCost_"+var1).val();}
		
		var calc = parseFloat($cost) * parseFloat($e_rate);
		
		$("#txtEstmdCostUSD_"+var1).val(calc.toFixed(2));
		$("#txtttlEstmdCost").val($("[id^=txtEstmdCost_]").sum().toFixed(2));
	}
	$("#txtttlEstmdCostUSD").val($("[id^=txtEstmdCostUSD_]").sum().toFixed(2));
}

function calculateTTLEstmdCost()
{
	if($("#rdoExchange1").is(":checked"))
	{
		$('[id^=txtEstmdCost_]').each(function(index) {
			var rowid = this.id;
			
			var var1 = rowid.split('_')[1];
			if($("#txtERate").val() == ""){$e_rate = 0;}else{$e_rate = $("#txtERate").val();}
			if($("#txtEstmdCost_"+var1).val() == ""){$cost = 0;}else{$cost = $("#txtEstmdCost_"+var1).val();}
			
			var calc = parseFloat($cost) * parseFloat($e_rate);
			$("#txtEstmdCostUSD_"+var1).val(calc.toFixed(2));
		});
		$("#txtttlEstmdCost").val($("[id^=txtEstmdCost_]").sum().toFixed(2));
	}
	$("#txtttlEstmdCostUSD").val($("[id^=txtEstmdCostUSD_]").sum().toFixed(2));
}

function getValidate(val)
{ 
    if($("#rdoExchange1").is(":checked"))
	{
		if($("#txtERate").val() == "" || $("#txtERate").val() == 0 || $("#txtttlEstmdCostUSD").val() == "" || $("#txtttlEstmdCostUSD").val() == 0 )
		{
			jAlert('Please fill Exchange Rate and Estimated Cost.', 'Alert');
			return false;
		}
		else
		{
			jConfirm('Are you sure you want to submit this data?', 'Confirmation', function(r) 
			{
				if(r)
				{
					var file_temp_name1 = $("[id^=file_]").map(function () {return this.value;}).get().join(",");
					$('#txtCRM').val(file_temp_name1);
					
					var file_actual_name1 = $("[id^=name_]").map(function () {return this.value;}).get().join(",");
					$('#txtCRM1').val(file_actual_name1);
					$('#upstatus').val(val);
					document.frm1.submit();
				} 
				else 
				{
					return false;
				}
			});
		}
	}
	else
	{
		if($("#txtttlEstmdCostUSD").val() == "" || $("#txtttlEstmdCostUSD").val() == 0 )
		{
			jAlert('Please fill Estimated Cost.', 'Alert');
			return false;
		}
		else
		{
			jConfirm('Are you sure you want to submit this data?', 'Confirmation', function(r) 
			{
				if(r)
				{
					var file_temp_name1 = $("[id^=file_]").map(function () {return this.value;}).get().join(",");
					$('#txtCRM').val(file_temp_name1);
					
					var file_actual_name1 = $("[id^=name_]").map(function () {return this.value;}).get().join(",");
					$('#txtCRM1').val(file_actual_name1);
					$('#upstatus').val(val);
					document.frm1.submit();
			    } 
				else 
				{
					return false;
				}
			});
		}
	}
}


function Del_Upload(var2)
{
	jConfirm('Are you sure you want to delete this upload permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file_'+var2).remove();
	}
	else{return false;}
	});
}

</script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(1); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-usd"></i>&nbsp;Port Costs&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Port Costs</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="index.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header" align="center">
                             <?php echo $obj->getAgencyLetterData($_SESSION['uid'],"PORT")." - ".$obj->getPortNameBasedOnID($obj->getAgencyLetterData($_SESSION['uid'],"PORTID"));?> Port Costs   
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Date
                            <address>
                               <?php if(date("d-M-Y",strtotime($obj->getLoadPortCostData($_SESSION['uid'],"DATE"))) == "01-Jan-1970"){?>
								<input type="text" name="txtDate" id="txtDate" class="form-control" readonly value="<?php echo date("d-M-Y",time());?>" />
								<?php }else{?>
								<input type="text" name="txtDate" id="txtDate" class="form-control" readonly value="<?php echo date("d-M-Y",strtotime($obj->getLoadPortCostData($_SESSION['uid'],"DATE")));?>" />
								<?php }?>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Agent
                            <address>
                                <?php echo strtoupper($obj->getVendorListNewBasedOnID($_SESSION['username'])).' ('.$_SESSION['username'].')';?>
                            </address>
                        </div><!-- /.col -->
                      </div>
					
					<div class="row invoice-info">
						 <div class="col-sm-4 invoice-col">
                           Vessel
                            <address>
                                <input type="text" name="txtVessel" id="txtVessel" class="form-control" readonly value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($_SESSION['comid'],"VESSEL_IMO_ID"),"VESSEL_NAME");?>" />
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-4 invoice-col">
                           Port
                            <address>
                                <input type="text" name="txtVoyage" id="txtVoyage" class="form-control" readonly value="<?php echo $obj->getPortNameBasedOnID($obj->getAgencyLetterData($_SESSION['uid'],"PORTID"));?>" />
                            </address>
                        </div><!-- /.col -->
					</div>
					
					<div class="row invoice-info">
						 <div class="col-sm-3 invoice-col">
                           Country
                            <address>
                                <?php echo $obj->getCountryNameBasedOnID($obj->getAgencyLetterData($_SESSION['uid'],"COUNTRY_ID"));?>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                           Currency
                            <address>
                              <?php echo $obj->getCountryData($obj->getAgencyLetterData($_SESSION['uid'],"COUNTRY_ID"),"CURRENCY_NAME");?>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col"><br/>
                        	<input name="rdoExchange" id="rdoExchange1" type="radio" style="cursor:pointer;" value="1" class="input-text"  <?php if($rdoExchange == 1) echo "checked"; ?>/>&nbsp;&nbsp;Local Currency&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="rdoExchange" id="rdoExchange2" type="radio" style="cursor:pointer;" value="2" class="input-text"  <?php if($rdoExchange ==2) echo "checked"; ?>/>&nbsp;&nbsp;<?php echo $currency;?>&nbsp;&nbsp;
                        </div>
                        <div class="col-sm-3 invoice-col">
                           Exchange Rate <span style="font-size:10px; font-style:italic;">( Local to <?php echo $currency;?> )</span>
                            <address>
                               
                               <?php if($obj->getLoadPortCostData($_SESSION['uid'],"EXCHANGE_RATE")==0){$exchangerate ='';}else{$exchangerate =$obj->getLoadPortCostData($_SESSION['uid'],"EXCHANGE_RATE");}?>
								<input type="text" name="txtERate" id="txtERate" onKeyUp="calculateTTLEstmdCost();" class="form-control" value="<?php echo $obj->getLoadPortCostData($_SESSION['uid'],"EXCHANGE_RATE");?>" />
								
                            </address>
                        </div><!-- /.col -->
					</div>
					
					<div>
					<?php echo $obj->getPortCostsBasedOnCountryID($obj->getAgencyLetterData($_SESSION['uid'],"COUNTRY_ID"));?>
					</div>
                    
                   <?php if(in_array($obj->getAgencyLetterData($_SESSION['uid'],"COUNTRY_ID"),$obj->getCountryArrayPortCostType())){?> 
				   
					<div class="row invoice-info">
						 <div class="col-sm-4 invoice-col">
                           Bank Details <span style="font-size:9px; font-style:italic;">( Agency )</span>
                            <address>
                                <textarea class="form-control areasize" name="txtBDetails" id="txtBDetails"><?php echo $obj->getLoadPortCostData($_SESSION['uid'],"BANK_DETAILS")?></textarea>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Prepared By <span style="font-size:9px; font-style:italic;">( Agency )</span>
                            <address>
                             <input type="text" name="txtPreparedBy" id="txtPreparedBy" class="form-control" autocomplete="off" value="<?php echo $obj->getLoadPortCostData($_SESSION['uid'],"PREPARED_BY")?>" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Authorized By <span style="font-size:9px; font-style:italic;">( Agency )</span>
                            <address>
                               <input type="text" name="txtAuthorizedBy" id="txtAuthorizedBy" class="form-control" autocomplete="off" value="<?php echo $obj->getLoadPortCostData($_SESSION['uid'],"AUTHORIZED_BY")?>" />
                            </address>
                        </div><!-- /.col -->
					</div>
				
					
                    
					<div class="row invoice-info">
                        <?php if($obj->getLoadPortCostData($_SESSION['uid'],"STATUS") < 1){?>
                        <div class="col-sm-6 invoice-col">
                             &nbsp;
                                <address>
                                <div class="btn btn-success btn-file btn-flat" data-toggle="tooltip">
                                    <i class="fa fa-paperclip"></i> Attachment
                                    <input type="file" class="form-control" multiple name="attach_file[]" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                </div>
                            </address>
                         </div><!-- /.col -->
                         <?php }?>
                         <?php 
						 $attachments = $obj->getLoadPortCostData($_SESSION['uid'],"ATTACHMENTS");
						 $attachments_name = $obj->getLoadPortCostData($_SESSION['uid'],"ATTACHMENTS_NAME");
						  if($attachments != '')
                            { 
                                $file = explode(",",$attachments); 
                                $name = explode(",",$attachments_name); ?>										
                            <div class="col-sm-6 invoice-col">
                              Previous Attachments
                                <address>
                                        <table cellpadding="1" cellspacing="1" border="0" width="100%" align="left">
                                        <?php
                                        $j =1;
                                        for($i=0;$i<sizeof($file);$i++)
                                        {
                                        ?>
                                        <tr height="20" id="row_file_<?php echo $j;?>">
                                            <td width="40%" align="left" class="input-text"  valign="top"><a href="../../attachment/<?php echo $file[$i]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="<?php echo $name[$i];?>"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $name[$i]; ?></a>
                                            <input type="hidden" name="file_<?php echo $j;?>" id="file_<?php echo $j;?>" value="<?php echo $file[$i]; ?>" />
                                            <input type="hidden" name="name_<?php echo $j;?>" id="name_<?php echo $j;?>" value="<?php echo $name[$i]; ?>" />
                                            </td>
                                            <?php if($obj->getLoadPortCostData($_SESSION['uid'],"STATUS") < 1){?>
                                            <td width="30%" align="left" class="input-text"  valign="top"><a href="#1" onClick="Del_Upload(<?php echo $j;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
                                            <?php }?>
                                        </tr>
                                        <?php $j++;}?>
                                    </table>
                                </address>
                            </div><!-- /.col -->
                         <?php } ?>
                     </div>
                     
                     <div class="row invoice-info">
						 <div class="col-sm-4 invoice-col">
                           Agent's Remarks
                            <address>
                                <textarea class="form-control areasize" name="txtAgentRemarks" id="txtAgentRemarks" placeholder="Agent's Remarks"><?php echo $obj->getLoadPortCostData($_SESSION['uid'],"AGENT_REMARKS")?></textarea>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Operator's Remarks
                            <address>
                             <textarea class="form-control areasize" name="txtOperatorRemarks" id="txtOperatorRemarks" placeholder="Operator's Remarks" disabled><?php echo $obj->getLoadPortCostData($_SESSION['uid'],"OPERATOR_REMARKS")?></textarea>
                            </address>
                        </div><!-- /.col -->
					</div>
                     <?php if($obj->getLoadPortCostData($_SESSION['uid'],"STATUS") < 1){?>
					<div class="box-footer" align="right">
						<button type="button" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>&nbsp;&nbsp;<button type="button" class="btn btn-primary btn-flat" onClick="return getValidate(1);">Submit to Close</button>
						<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
                        <input type="hidden" name="txtCRM" id="txtCRM" value="" />
                        <input type="hidden" name="txtCRM1" id="txtCRM1" value="" />
					</div>
					<?php } ?>
					
                    
                    
                    <?php }?>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
    </body>
</html>