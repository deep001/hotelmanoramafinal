<?php 
session_start();
require_once("../../includes/display_external_agent_dryout.inc.php");
require_once("../../includes/functions_external_agent_dryout.inc.php");
$display = new display();
$obj = new data();
$display->logout_iu();
$id = $_SESSION['uid']; 
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->updateUserProfileRecords();
	header('Location:./profile.php?msg='.$msg);
 }

$obj->viewAdminRecords($id); 
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
$msg = NULL;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(1); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-user"></i>&nbsp;Profile&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Profile</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b>  Profile password added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating profile password.
				</div>
				<?php }}?>
				<!--<div class="col-md-6">-->
				<div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">EDIT PROFILE</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                                    <div class="box-body">
										<div class="form-group">
                                            <label for="selCName" >Company Name</label><br>
											<label  style='color:#dc631e;font-size:14px;' >&nbsp;&nbsp;&nbsp;&nbsp;<?php echo strtoupper($obj->getCompanyNameBasedOnID($_SESSION['company'],"COMPANY_NAME"));?></label>                                        
                                        </div>
                                        <!--<div class="form-group">
                                            <label for="txtName">Name</label><br>
											<label style="font-weight:normal;" >&nbsp;&nbsp;&nbsp;&nbsp;<?php //echo $obj->getFun5();?></label>      
                                        </div>
										
										<div class="form-group">
                                            <label>Address</label><br>
											<label style="font-weight:normal;" >&nbsp;&nbsp;&nbsp;&nbsp;<?php //echo $obj->getFun2();?></label>  
                                           
                                        </div>
										<div class="form-group">
                                            <label for="txtPhoneNo">Phone Number</label><br>
											<label style="font-weight:normal;" >&nbsp;&nbsp;&nbsp;&nbsp;<?php //echo $obj->getFun3();?></label>  
                                       </div>
										<div class="form-group">
                                            <label for="txtEmailid">E-mail ID</label><br>
											<label style="font-weight:normal;" >&nbsp;&nbsp;&nbsp;&nbsp;<?php //echo $obj->getFun4();?></label> 
                                           
                                        </div>-->
                                        
										<div class="form-group">
                                            <label for="txtUName">User Name</label><br>
											<label style="font-weight:normal;" >&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $obj->getFun7();?></label> 
                                            <input type="hidden" class="form-control" id="txtUName" name="txtUserName" placeholder="User Name" autocomplete="off" value="<?php echo $obj->getFun7();?>">
                                        </div>
										
										<div class="form-group">
                                            <label for="txtPassword">Password</label>
                                            <input type="password" class="form-control" id="txtPassword" name="txtPassword" placeholder="Password" autocomplete="off" value="<?php echo $obj->getFun8();?>">
                                        </div>
										
										<div class="form-group">
                                            <label for="txtCPassword">Confirm Password</label>
                                            <input type="password" class="form-control" id="txtCPassword" name="txtCPassword" placeholder="Confirm Password" autocomplete="off" value="<?php echo $obj->getFun8();?>">
                                        </div>
										
										
                                    </div><!-- /.box-body -->

                                    <div class="box-footer" align="center">
                                        <button type="submit" class="btn btn-primary btn-flat">Submit</button><input type="hidden" name="action" value="submit" />
                                    </div>
                                </form>
                            </div>
					<!--</div>			-->
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
 <script type="text/javascript">
 $(document).ready(function(){ 
 
           $("#frm1").validate({
				rules: {
					txtPassword: {required: true},
					txtCPassword : {required :true, equalTo:"#txtPassword"}
					},
				messages: {
					txtPassword: {required: "*"},
					txtCPassword : {required :"*", equalTo:"Your password does not match with the original password."}
					},
			submitHandler: function(form)  {
					jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../img/loading.gif"  />', 'Alert');
					$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
					$("#popup_content").css({"background":"none","text-align":"center"});
					$("#popup_ok,#popup_title").hide();  
					form.submit();
				}
			});
});
        </script>
    </body>
</html>