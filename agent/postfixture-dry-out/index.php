<?php 
session_start();
require_once("../../includes/display_external_agent_dryout.inc.php");  
require_once("../../includes/functions_external_agent_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();  
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(1); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
               <section class="content-header">
                    <h1>
                        <i class="fa fa-dashboard"></i>&nbsp;Dashboard&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div style="height:50px; width:100%;"></div>
				<div class="box-body table-responsive">
				<table aria-describedby="example2_info" id="example2" class="table table-bordered table-striped dataTable">
					<thead>
						<tr role="row">
						<th align="left">Voyage No.</th>
						<th align="left">Vessel</th>
						<th align="left">Port</th>
						<th align="center">PDA Request Letter</th>
						<th align="center">Initial PDA</th>
						<th align="center">FDA</th>
						<th align="center">SoF</th>
						</tr>
					</thead>
						
					<tbody aria-relevant="all" aria-live="polite" role="alert">
						<tr>
							<td align="left"><?php echo $obj->getCompareTableData($_SESSION['comid'],"MESSAGE");?></td>
							<td align="left"><?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($_SESSION['comid'],"VESSEL_IMO_ID"),"VESSEL_NAME");?></td>
							<td align="left"><?php echo $obj->getPortNameBasedOnID($obj->getAgencyLetterData($_SESSION['uid'],"PORTID"));?></td>
							<td align="center"><p><a href="allPdf.php?id=1&gen_agency_id=<?php echo $_SESSION['uid'];?>&port_type=<?php echo $_SESSION['port_type'];?>&comid=<?php echo $_SESSION['comid'];?>&port_name=<?php echo $_SESSION['portid'];?>&agent_code=<?php echo $_SESSION['username'];?>&randomno=<?php echo $_SESSION['serialno'];?>" title="Pdf" ><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i> PDA Request Letter</button></a></p><p><a href="allPdf.php?id=2&gen_agency_id=<?php echo $_SESSION['uid'];?>&port_type=<?php echo $_SESSION['port_type'];?>&port_name=<?php echo $_SESSION['portid'];?>&agent_code=<?php echo $_SESSION['username'];?>&randomno=<?php echo $_SESSION['serialno'];?>" title="Pdf" ><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i> Letter to Agents - Bunker Stemmed</button></a></p></td>
							<td align="center"><a href="port_cost.php" title="Initial PDA" >Initial PDA</a></td>
							<?php if($obj->getLoadPortCostData($_SESSION['uid'],"STATUS") >= 2){?>
							<td align="center"><a href="pda.php" title="PDA" >FDA</a></td>	
							<?php }else{?>
							<td align="center"></td>
							<?php }?>
							<td align="center"><a href="sof.php?page=1" title="SOF" >SOF</a></td>
						</tr>
					</tbody>
				</table>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/timer.js" type="text/javascript"></script>
    </body>
</html>