<?php
require_once("../../includes/functions_internal_user_dryout.inc.php");
class display
{

	function title()
	{
		echo ":: Welcome to Seven Oceans Commercials ::";
	}
	
	function favicon()
	{
		echo '<link href="../../favicon.ico" rel="shortcut icon" />'; 
	}
	
	function css()
	{
		echo '<link rel="stylesheet" href="../../css/bootstrap.min.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../../css/font-awesome.min.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../../css/ionicons.min.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../../css/AdminLTE.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../../css/jClocksGMT.css" type="text/css"/>';
	}
	
	function js()
	{
		echo '<script src="../../js/jquery-1.8.3.js" type="text/javascript"></script>';
		echo '<script src="../../js/bootstrap.min.js" type="text/javascript"></script>';
		echo '<script src="../../js/AdminLTE/app.js" type="text/javascript"></script>';
		echo '<script src="../../js/jquery.validate.js" type="text/javascript"></script>';
		echo '<script src="../../js/jClocksGMT.js" type="text/javascript"></script>';
		echo '<script src="../../js/jquery.rotate.js" type="text/javascript"></script>';
	}
	
	
	function header_tag()
	{
		$obj = new data();
		if($_SESSION['gender'] == 1){$gender_img = "avatar5.png";}else{$gender_img = "avatar3.png";}
		$recent_work_arr = $obj->getLoginidWiseRecentFiveWorks();
		$company_name = $obj->getCompanyNameBasedOnID($_SESSION['company'],"COMPANY_NAME");
	echo '<header class="header">
            <a href="#Header" class="logo" style="font-size:14px;">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->'; ?>
               ::&nbsp;Hotel Manorama&nbsp;::
           <?php echo ' </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
						<!--<li><a href="../lockscreen.php" title="lock screen"><i class="fa fa-lock"></i></a></li>-->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope"></i>
                                <span class="label label-success">'.count($recent_work_arr).'</span>
                            </a>';
							if(count($recent_work_arr) == 0)
							{
                            echo '<ul class="dropdown-menu">
                                <li class="header">No recent work log</li> 
							</ul>';
							}
							else
							{	
								echo '<ul class="dropdown-menu">
                                <li class="header">Top '.count($recent_work_arr).' recent work</li> 
								
								<li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">';
									
                                       for($i=0;$i<count($recent_work_arr);$i++)
									   {
									    echo '<li><!-- start message -->
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="../../img/'.$gender_img.'" class="img-circle" alt="User Image"/>
                                                </div>
                                                <h4>
                                                    '.ucwords(strtolower($_SESSION['display'])).'
                                                    <small><i class="fa fa-clock-o"></i> '.$recent_work_arr[$i]['datetime'].'</small>
                                                </h4>
                                                <p>'.$recent_work_arr[$i]['work'].'</p>
                                            </a>
                                        </li><!-- end message -->';
                                      } 
                                    echo '</ul>
                                </li>
                               
							  <li class="footer"><a href="#">See All Works</a></li>
								</ul>';
							}
                        echo '</li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-warning"></i>
                                <span class="label label-warning">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have zero notifications</li>                           
                            </ul>
                        </li>
                        <!-- Tasks: style can be found in dropdown.less -->
                        <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-tasks"></i>
                                <span class="label label-danger">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have zero tasks</li>                             
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>';
								$display_short_name = explode(" ",$_SESSION['display']);
                                echo '<span> '.$_SESSION['display'].' <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">';
							       echo'<img src="../../img/logo.png" class="img-circle" alt="User Image" />
                                    <p>'.$_SESSION['display'].'
                                        <small>Internal User , '.$company_name.'</small>
                                    </p>
                                </li>
                               <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="profile.php" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="../../logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header><a href="#" class="back-to-top" title="Back to top"><i class="glyphicon glyphicon-circle-arrow-up" style="font-size:20px;"></i></a>';

	}
	
	function leftmenu($val,$val1)
	{
		$obj = new data();
		$display_short_name = explode(" ",$_SESSION['display']);
		$company_name = $obj->getCompanyNameBasedOnID($_SESSION['company'],"COMPANY_NAME");
		$dashclass = $fleets = $jaldhidatamanagement = $SOHdatamanagement = $coa = "";$masters = $daily_taskvc = $daily_tasktc = $reports =  $modulerights = $processtopay = $FandA = $ordertocash = $daily_taskcoa = "treeview";
		$moduleid = $_SESSION['moduleid'];
		$loginid = $_SESSION['uid']; 
		$linkid =  $obj->getUserAuthenticationModuleWiseForMainMenu($moduleid); 
		
		if($val==1){$dashclass ="active";}
		else if($val==2){$fleets ="active";}
		else if($val==3){$masters = "active ".$masters;}
		if($val==4){$coa ="active";}
		if($val==5){$daily_taskvc = "active ".$daily_taskvc;}
		
		if($val==6){$elibrary = "active";}
		if($val==7){$reports = "active ".$reports;}
		if($val1 == 1){ $subreports = "active "; }
		if($val1 == 2){ $subreports1 = "active "; }
		if($val1 == 3){ $subreports2 = "active "; }
		if($val==18){$cargo_planning ="active";}
		if($val==8){$voyage_estimatetc1 ="active";}
		if($val==9){$voyage_estimate1 = "active";}
		if($val==10){$deciginchart = "active";}
		if($val==20){$decigincharttc = "active";}
		if($val==21){$daily_tasktc = "active ".$daily_tasktc;}
		if($val==11){$jaldhidatamanagement = "active";}
		if($val==12){$SOHdatamanagement = "active";}
		if($val==13){$modulecbts = "active";}
		if($val==14){$daily_taskcoa = "active ".$daily_taskcoa;}
		if($val==15){$daily_taskcoa = "active ".$daily_taskcoa;}
		if($val==16){$daily_taskcoa = "active ".$daily_taskcoa;}
		if($val==17){$daily_taskcoa = "active ".$daily_taskcoa;}
		if($val==19){$daily_taskcoa = "active ".$daily_taskcoa;}
			echo '<aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image" >
						<img src="../../img/soctaralogo.jpg" class="img-circle" alt="User Image" style="background-color:#fff;"/>
                         </div>
                        <div class="pull-left info">
                            <p>Welcome, '.$_SESSION['display'].'</p>
							<small>'.$company_name.'</small>
						</div>
                    </div>
                   <!-- sidebar menu: : style can be found in sidebar.less -->
                    
					
					<ul class="sidebar-menu">';
					echo '<li class="'.$dashclass.'"><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>';
					
					echo '<li class="'.$fleets.'"><a href="fleet.php"><i class="fa fa-anchor"></i><span>Fleet</span></a></li>';
							
					echo '<li class="'.$masters.'"><a href="#"><i class="fa fa-folder-open"></i><span>Masters</span><i class="fa fa-angle-left pull-right"></i></a>
						   <ul class="treeview-menu">
						    <li><a href="agency_fee_record.php" ><i class="fa fa-angle-double-right"></i>Agency Fee Records</a></li>
						    <li><a href="baltic_route_list.php" ><i class="fa fa-angle-double-right"></i>Baltic Route</a></li>
							<li><a href="banking_details.php" ><i class="fa fa-angle-double-right"></i>Banking Details</a></li>
							<li><a href="basein_list.php" ><i class="fa fa-angle-double-right"></i>Basin</a></li>
							<li><a href="bunker_grade.php" ><i class="fa fa-angle-double-right"></i>Bunker Grade</a></li>
							<li><a href="cargo_work_stage.php" ><i class="fa fa-angle-double-right"></i>Cargo Work Stage</a></li>
							<li><a href="charterer_cost_master.php" ><i class="fa fa-angle-double-right"></i>Charterer Cost</a></li> 
							<li><a href="coaroute_list.php" ><i class="fa fa-angle-double-right"></i>COA Route</a></li>  
							<li><a href="contract_type_list.php" ><i class="fa fa-angle-double-right"></i>Contract Type</a></li> 
							<li><a href="expense_type_list.php" ><i class="fa fa-angle-double-right"></i>Expense Type</a></li> 
							<li><a href="reference_category_list.php" ><i class="fa fa-angle-double-right"></i>E-Library Category Master</a></li>
							<li><a href="reference_type_list.php" ><i class="fa fa-angle-double-right"></i>E-Library Reference Type Master</a></li>
							<li><a href="invoice_status_list.php" ><i class="fa fa-angle-double-right"></i>Invoice Status List</a></li>
							<li><a href="lawarbitration_list.php" ><i class="fa fa-angle-double-right"></i>Law Arbitration List</a></li>
							<li><a href="loadoption_list.php" ><i class="fa fa-angle-double-right"></i>Load Options</a></li>
                            <li><a href="material_list.php" ><i class="fa fa-angle-double-right"></i>Material</a></li> 
							<li><a href="material_safety_data_list.php" ><i class="fa fa-angle-double-right"></i>Material Safety Data Sheets</a></li>
							<li><a href="necessary_approval.php" ><i class="fa fa-angle-double-right"></i>Necessary Approval</a></li> 
							<li><a href="owner_related_cost.php" ><i class="fa fa-angle-double-right"></i>Owner Related Cost</a></li>  
							<li><a href="other_miscellaneous_cost_master.php" ><i class="fa fa-angle-double-right"></i>Other Miscellaneous Cost</a></li>
							<li><a href="other_shipping_cost_master.php" ><i class="fa fa-angle-double-right"></i>Other Shipping Cost</a></li>
							<li><a href="port_cost_type_list.php" ><i class="fa fa-angle-double-right"></i>Port Cost Type</a>
							<li><a href="port_data.php" ><i class="fa fa-angle-double-right"></i>Port Data</a></li>
							<li><a href="port_information.php" ><i class="fa fa-angle-double-right"></i>Port Information</a></li>
							<li><a href="tc_deductions.php" ><i class="fa fa-angle-double-right"></i>TC Deductions</a></li>
							<li><a href="terminal_list.php" ><i class="fa fa-angle-double-right"></i>Terminal</a></li>    
							<li><a href="vendor_list.php" ><i class="fa fa-angle-double-right"></i>Vendor</a></li>
							<li><a href="vc_deductions.php" ><i class="fa fa-angle-double-right"></i>VC Deductions</a></li>
							<li><a href="vesseltype.php" ><i class="fa fa-angle-double-right"></i>Vessel Type</a></li>
							<li><a href="vessel_cat_master_list.php" ><i class="fa fa-angle-double-right"></i>Vessel Category Master</a></li>
							
						   </ul>
						</li>';	
					   if(in_array(18,$linkid)){
					/*	echo '<li class="'.$cargo_planning.'"><a href="cargo_planning.php"><i class="fa fa-leaf"></i><span>Customer list</span></a></li>';*/
					echo '<li class="'.$cargo_planning.'"><a href="customerlist.php"><i class="fa fa-leaf"></i><span>Customer list</span></a></li>';
					   }
						
					   if(in_array(2,$linkid)){
						echo '<li class="'.$voyage_estimate1.'"><a href="estimate_list.php"><i class="fa fa-leaf"></i><span>VC Estimates</span></a></li>';
					   }
					   if(in_array(8,$linkid)){
						echo '<li class="'.$voyage_estimatetc1.'"><a href="estimate_tc_list.php"><i class="fa fa-leaf"></i><span>TC Estimates </span></a></li>';
					   }
					   if(in_array(3,$linkid)){
						echo '<li class="'.$deciginchart.'"><a href="decisionchart_list.php"><i class="fa fa-leaf"></i><span>Decision Chart VC</span></a></li>';
					   }
					   if(in_array(9,$linkid)){
						echo '<li class="'.$decigincharttc.'"><a href="decisionchart_tc_list.php"><i class="fa fa-leaf"></i><span>Decision Chart TC</span></a></li>';
					   }
					   if(in_array(4,$linkid) || in_array(5,$linkid) || in_array(6,$linkid) || in_array(7,$linkid))
					   { 
						echo '<li class="'.$daily_taskvc.'"><a href="#"><i class="fa fa-book"></i><span>Ops - VC</span><i class="fa fa-angle-left pull-right"></i></a>
						   <ul class="treeview-menu">';
						   
						   if(in_array(4,$linkid)){
							echo '<li><a href="finalisedvoyagefixtures.php" ><i class="fa fa-angle-double-right"></i>Finalised Voyage Fixtures VC</a></li>';       	}
							if(in_array(5,$linkid)){
							echo '<li><a href="in_ops_at_glance.php" ><i class="fa fa-angle-double-right"></i>In Ops at a glance VC</a></li>';           		}
							if(in_array(6,$linkid)){
							echo '<li><a href="vessel_in_post_ops.php" ><i class="fa fa-angle-double-right"></i>Vessels in Post Ops VC</a></li>';  				}
							if(in_array(7,$linkid)){
							echo '<li><a href="vessel_in_history.php" ><i class="fa fa-angle-double-right"></i>Vessels in History VC</a></li>';					}
							echo  '</ul>
						</li>';
					   }
					   if(in_array(10,$linkid) || in_array(11,$linkid) || in_array(12,$linkid) || in_array(13,$linkid))
					   { 
						echo '<li class="'.$daily_tasktc.'"><a href="#"><i class="fa fa-book"></i><span>Ops - TC</span><i class="fa fa-angle-left pull-right"></i></a>
						   <ul class="treeview-menu">';	
							if(in_array(10,$linkid)){
							echo '<li ><a href="finalisedvoyagefixturestc.php" ><i class="fa fa-angle-double-right"></i>Finalised Voyage Fixtures TC</a></li>';       	}
							if(in_array(11,$linkid)){
							echo '<li><a href="in_ops_tc.php" ><i class="fa fa-angle-double-right"></i>In Ops at a glance TC</a></li>';           		}
							if(in_array(12,$linkid)){
							echo '<li><a href="vessel_in_post_tc.php" ><i class="fa fa-angle-double-right"></i>Vessels in Post Ops TC</a></li>';  				}
							if(in_array(13,$linkid)){
							echo '<li><a href="vessel_in_history_tc.php" ><i class="fa fa-angle-double-right"></i>Vessels in History TC</a></li>';					}
							
							
						 echo  '</ul>
						</li>';	
						}
						
					   if(in_array(14,$linkid) || in_array(15,$linkid) || in_array(19,$linkid) || in_array(16,$linkid) || in_array(17,$linkid))
					   { 
						echo '<li class="'.$daily_taskcoa.'"><a href="#"><i class="fa fa-book"></i><span>COAs</span><i class="fa fa-angle-left pull-right"></i></a>
						   <ul class="treeview-menu">';	
							if(in_array(14,$linkid)){
							echo '<li ><a href="coa_list.php" ><i class="fa fa-angle-double-right"></i>Running COAs</a></li>';  }
							if(in_array(19,$linkid)){
							echo '<li><a href="cargo_relet_coa_ops.php" ><i class="fa fa-angle-double-right"></i>COA - Cargo Relet</a></li>';  }
							if(in_array(15,$linkid)){
							echo '<li><a href="coa_in_ops_at_glance.php" ><i class="fa fa-angle-double-right"></i>COA - In Ops</a></li>';  }
							if(in_array(16,$linkid)){
							echo '<li><a href="coa_in_post_ops.php" ><i class="fa fa-angle-double-right"></i>COA - Post Ops</a></li>';  }
							if(in_array(17,$linkid)){
							echo '<li><a href="coa_in_history.php" ><i class="fa fa-angle-double-right"></i>COA - History</a></li>';	}
						 echo  '</ul>
						    </li>';	
						}
						
					   
					   
					
						echo '<li class="'.$reports.'"><a href="#"><i class="fa  fa-bar-chart-o"></i><span>Reports</span><i class="fa fa-angle-left pull-right"></i></a>
								<ul class="treeview-menu">
									<li class="treeview '.$subreports.'">
										<a href="#">
											<i class="fa fa-bar-chart-o"></i>
											<span class="title">Chartering</span>
										</a>
										<ul class="treeview-menu">
										    <li>
												<a href="tc_baltic_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Vessel/TC Perf against Baltic</span>
												</a>
											</li>
											<li>
												<a href="cargo_tonnage_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Cargo Tonnage Book Report</span>
												</a>
											</li>
											<li>
												<a href="cargo_open_position_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Cargo Open Position Report</span>
												</a>
											</li>
											<!--<li>
												<a href="coa_status_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">COA Status Report</span>
												</a>
											</li>-->
											<li>
												<a href="owner_broker_fixture_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Owner/Broker Fixture Report</span>
												</a>
											<li>
											<li>
												<a href="spot_fixture.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Fixture Report</span>
												</a>
											</li>
											<li>
												<a href="chartering_register.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Chartering Register</span>
												</a>
											</li>
											<li>
												<a href="chartering_register_detailed.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Chartering Register - Detailed</span>
												</a>
											</li>
											<li>
												<a href="chartering_register_tc.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Chartering Register - TCs</span>
												</a>
											</li>
										</ul>
									</li>
					
									<li class="treeview '.$subreports1.'">
										<a href="#">
											<i class="fa fa-bar-chart-o"></i>
											<span class="title">Operations</span>
										</a>
										<ul class="treeview-menu">
											<li>
												<a href="agent_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Agent List</span>
												</a>
											</li>
											<li>
												<a href="daily_position_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Daily Position Report</span>
												</a>
											</li>
											<li>
												<a href="dead_freight_summary.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Dead Freight Summary</span>
												</a>
											<li>
											<li>
												<a href="demmurage_summary.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Demmurage Summary</span>
												</a>
											</li>
											<li>
												<a href="port_performance.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Port Performance Report</span>
												</a>
											</li>
											<!--<li>
												<a href="pni_club_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">PNI Club Declaration Report</span>
												</a>
											</li>-->						
										</ul>
									</li>
					
									<li class="treeview '.$subreports2.'">
										<a href="#">
											<i class="fa fa-bar-chart-o"></i>
											<span class="title">Accounts</span>
										</a>
										<ul class="treeview-menu">
											<li>
												<a href="aging_payable.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Aging Report (Payable)</span>
												</a>
											</li>
											<li>
												<a href="aging_receivables.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Aging Report (Receivables)</span>
												</a>
											</li>
											<li>
												<a href="profitability_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Profitability Analysis</span>
												</a>
											<li>
											<!--<li>
												<a href="profitability_headwise_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Profitability HeadWise Report</span>
												</a>
											<li>-->
											<li>
												<a href="projected_cash_flow_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Projected Cash Flow (VC)</span>
												</a>
											</li>
											<li>
												<a href="shipment_register.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Shipment Register</span>
												</a>
											</li>
											<!--<li>
												<a href="payment_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Vendor Wise Payment Report</span>
												</a>
											</li>
											<li>
												<a href="vessel_payment_report.php">
													<i class="fa fa-angle-double-right"></i>
													<span class="title">Vessel Wise Outstanding Report</span>
												</a>
											</li>	-->						
										</ul>
									</li>
					
						  
								</ul>
							</li>';	
					  echo '<li class="'.$elibrary.'"><a href="elibrary.php"><i class="fa fa-desktop"></i><span>E-Library</span></a></li>';	
					  
					  echo '<li class="'.$modulecbts.'"><a href="module_cbt.php"><i class="fa fa-video-camera"></i><span>Module CBTs</span></a></li>';
                   echo '</ul>	
                </section>
                <!-- /.sidebar -->
            </aside>';
	}
	

function footer()
	{
		echo '<div class="footer" align="right"><img src="../../img/Logo2.jpg" height="14" width="14">&nbsp;<a href="http://seowebmantra.com" target="_blank" style="font-weight:bold;">Seowebmantra.com production</a>&nbsp;&nbsp;&nbsp;&nbsp;</div>';
	}

	
	function logout_iu()
	{
		if(!isset($_SESSION['uid'],$_SESSION['username'],$_SESSION['display'],$_SESSION['iutype']))
		{
			header("Location:../../logout.php");
		}
	}
	
}
?>