<?php
require_once("../includes/functions_sadmin.inc.php");
class display
{

	function title()
	{
		echo ":: Welcome to Seven Oceans Commercials ::";
	}
	
	function favicon()
	{
		echo '<link href="../favicon.ico" rel="shortcut icon" />'; 
	}
	
	function css()
	{
		echo '<link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../css/ionicons.min.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../css/AdminLTE.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../css/jClocksGMT.css" type="text/css"/>';
	}
	
	function js()
	{
		echo '<script src="../js/jquery-1.8.3.js" type="text/javascript"></script>';
		echo '<script src="../js/bootstrap.min.js" type="text/javascript"></script>';
		echo '<script src="../js/AdminLTE/app.js" type="text/javascript"></script>';
		echo '<script src="../js/jquery.validate.js" type="text/javascript"></script>';
		echo '<script src="../js/jClocksGMT.js" type="text/javascript"></script>';
		echo '<script src="../js/jquery.rotate.js" type="text/javascript"></script>';
	}
	
	
	function header_tag()
	{
		$obj = new data();
		if($_SESSION['gender'] == 1){$gender_img = "avatar5.png";}else{$gender_img = "avatar3.png";}
		$recent_work_arr = $obj->getLoginidWiseRecentFiveWorks();
	echo '<header class="header">
            <a href="#Header" class="logo" style="font-size:17px;">
                <!-- Add the class icon to your logo image or logo icon to add the margining --> '?>
               ::&nbsp;Seven Oceans' Commercials&nbsp;::
           <?php echo ' </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope"></i>
                                <span class="label label-success">'.count($recent_work_arr).'</span>
                            </a>';
							if(count($recent_work_arr) == 0)
							{
                            echo '<ul class="dropdown-menu">
                                <li class="header">No recent work log</li> 
							</ul>';
							}
							else
							{	
								echo '<ul class="dropdown-menu">
                                <li class="header">Top '.count($recent_work_arr).' recent work</li> 
								
								<li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">';
									
                                       for($i=0;$i<count($recent_work_arr);$i++)
									   {
									    echo '<li><!-- start message -->
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="../img/'.$gender_img.'" class="img-circle" alt="User Image"/>
                                                </div>
                                                <h4>
                                                    Superadmin
                                                    <small><i class="fa fa-clock-o"></i> '.$recent_work_arr[$i]['datetime'].'</small>
                                                </h4>
                                                <p>'.$recent_work_arr[$i]['work'].'</p>
                                            </a>
                                        </li><!-- end message -->';
                                      } 
                                    echo '</ul>
                                </li>
                               
							  <li class="footer"><a href="#">See All Works</a></li>
								</ul>';
							}
                        echo '</li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-warning"></i>
                                <span class="label label-warning">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have zero notifications</li>                           
                            </ul>
                        </li>
                        <!-- Tasks: style can be found in dropdown.less -->
                        <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-tasks"></i>
                                <span class="label label-danger">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have zero tasks</li>                             
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>';
								$display_short_name = explode(" ",$_SESSION['display']);
                                echo '<span> '.$display_short_name[0].' <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">';
							//if($_SESSION['gender'] == 1){$gender_img = "avatar5.png";}else{$gender_img = "avatar3.png";}
							       echo'<img src="../img/logo.png" class="img-circle" alt="User Image" />
                                    <p>'.$_SESSION['display'].'
                                        <small>Superadmin , Progress Chartering</small>
                                    </p>
                                </li>
                               <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#Profile" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="../logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header><a href="#" class="back-to-top" title="Back to top"><i class="glyphicon glyphicon-circle-arrow-up" style="font-size:20px;"></i></a>';
	}
	
	function leftmenu($val)
	{
		$display_short_name = explode(" ",$_SESSION['display']);
	echo '<aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
						<img src="../img/soctaralogo.jpg" class="img-circle" alt="User Image" style="background-color:#fff;"/>
                         </div>
                        <div class="pull-left info">
                            <p>Welcome, '.$display_short_name[0].'</p>
							<small>Superadmin, Progress Chartering</small>
                            
						</div>
                    </div>
                   <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">';
					
					if($val == 1){		echo '<li class="active"><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>';  }
							else {		echo '<li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>';					}
					if($val == 2){		echo '<li class="active"><a href="company_creation.php"><i class="fa fa-suitcase"></i> <span>Company Creation</span></a></li>';	}
							else {		echo '<li><a href="company_creation.php"><i class="fa fa-suitcase"></i> <span>Company Creation</span></a></li>';				}
					if($val == 3){		echo '<li class="active"><a href="admin_creation.php"><i class="fa fa-user-md"></i> <span>Admin Creation</span></a></li>';		}
							else {		echo '<li><a href="admin_creation.php"><i class="fa fa-user-md"></i> <span>Admin Creation</span></a></li>';						}
							
                    echo '</ul>
                </section>
                <!-- /.sidebar -->
            </aside>';
	}
	
	function footer()
	{
		echo '<div class="footer" align="right"><img src="../img/Logo2.jpg" height="14" width="14">&nbsp;<a href="http://sevenoceansconsulting.com" target="_blank" style="font-weight:bold;">A Seven Oceans Holdings production</a>&nbsp;&nbsp;&nbsp;&nbsp;</div>';
	}
	
	function logout_sa()
	{
		if(!isset($_SESSION['uid'],$_SESSION['username'],$_SESSION['display'],$_SESSION['satype']))
		{
			header("location:../logout.php");
		}
	}
	
}
?>