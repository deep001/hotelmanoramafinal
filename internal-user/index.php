<?php 
session_start();
require_once("../includes/display_internal_user.inc.php");
require_once("../includes/functions_internal_user.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(1); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-home"></i>&nbsp;Home&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Module</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div class="row">
                        <!--<div class="col-lg-4 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h4>
                                      Seven Oceans' Commercials
                                    </h4>
                                    <p>
                                    <br/>
                                        TC Out
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="#" class="small-box-footer" onClick="getRelocate(5)">
                                    Get in <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>-->
                        <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                     <h4>
                                    Hotel Manorama Palace
                                    </h4>
                                    <p>
                                    <br/>
                                        Enter
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="#" class="small-box-footer" onClick="getRelocate(2)">
                                    Get in <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
						
						<!--<div class="col-lg-4 col-xs-6">
                            <!-- small box
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                     <h4>
                                      Seven Oceans' Commercials
                                    </h4>
                                    <p>
                                    <br/>
                                        Vessel Relet
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="#" class="small-box-footer" onClick="getRelocate(3)">
                                    Get in <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>-->
						
						
					</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../js/timer.js" type="text/javascript"></script>
<script>
function getRelocate(var1)
{
		if(var1 == 5){location.href = "./tcout/fleet.php";}
		if(var1 == 2){location.href = "./dryout/index.php";}
		if(var1 == 3){location.href = "./relet/";}
}
</script>
    </body>
</html>