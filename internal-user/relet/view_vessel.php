<?php 
session_start();
require_once("../../includes/display_internal_user_relet.inc.php");
require_once("../../includes/functions_internal_user_relet.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];
 
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-anchor"></i>&nbsp;Fleet &nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Fleet</li>
						<li class="active"><?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME")); ?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="fleet.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm0" id="frm0" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
					<div class="row">
						<div class="col-xs-12">
							<h2 style=" text-align:center;">
								Vessel Particulars ( <?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME")); ?> )                              
							</h2>                            
						</div><!-- /.col -->
					</div>
					<div class="row" style="height:20px">&nbsp;</div>	
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								Vessel Description                           
							</h2>                            
						</div><!-- /.col -->
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Vessel’s Name 
							<address>
								<label><?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME"));?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Vessel’s Previous Name(S)
							<address>
								<label><?php echo $obj->getVesselParticularData('VP_NAME','vessel_master_1',$_REQUEST['id']);?><label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Date (S) Of Change
							<address>
								<label><?php if($obj->checkVesselDate('DATE_OF_CHANGE','vessel_master_1',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_OF_CHANGE','vessel_master_1',$_REQUEST['id']))); } ?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Flag
							<address>
								<label>
									<?php //$obj->getCountryNameList(); ?>
								</label>
							</address>
						</div><!-- /.col -->
						
					</div>
					<hr>
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Month/Year And Where Built
							<address>
								<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"YEARBUILT");?></label>
							</address>
						</div><!-- /.col -->
					</div>
					<hr>
				
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Yard Name And Number
							<address>
								<label><?php echo $obj->getVesselParticularData('YARD_NAME','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Class Registration Number
							<address>
								<label><?php echo $obj->getVesselParticularData('CLASS_R_NO','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Official Number
							<address>
								<label><?php echo $obj->getVesselParticularData('OFFICIAL_NO','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							IMO Number
							<address>
								<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"IMO_NO");?><label>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Part Of Registry
							<address>
								<label>
									<?php echo $obj->getPortNameBasedOnID($obj->getVesselParticularData('PORT_ID','vessel_master_1',$_REQUEST['id']));?>
								</label>
							</address>
						</div><!-- /.col -->
						
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							Owner Full Style And Contact Number For Operational Purposes , If Appropriate 
							<address>
								<label><?php echo $obj->getVesselParticularData('OWNERS_FULL','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
					</div>
				
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							Managers Full Style And Contact Number For Operational Purpose, If Appropriate
							<address>
								<label><?php echo $obj->getVesselParticularData('MANAGERS_FULL','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
					</div>
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							If Contracting Party Are Disponent Owners State 
							<address>
								<label><?php echo $obj->getVesselParticularData('OWNERS_STATE','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-6 invoice-col">
							Full Style And Contact numbers For Operational Purpose
							<address>
								<label><?php echo $obj->getVesselParticularData('OPERATION_PURPOSE','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
					
						<div class="col-sm-6 invoice-col">
							<br/>If Vessel On Time Charter Or Bareboat
							<address>
								<label><?php echo $obj->getVesselParticularData('BAREBOAT','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-6 invoice-col">
							When Vessel Delivered To Disponent Owners
							<address>
								<label><?php if($obj->checkVesselDate('DISPONENT_OWNER','vessel_master_1',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DISPONENT_OWNER','vessel_master_1',$_REQUEST['id']))); } ?></label>
								
							</address>
						</div><!-- /.col -->
					</div>
				
					
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								Particulars Of Vessel                           
							</h2>                            
						</div><!-- /.col -->
					</div>
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Type Of Vessel
							<address>
								<label><?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_TYPE")); ?></label>
							</address>
						</div><!-- /.col -->
					</div>
				
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Deadweight All Told (Metric Tons)</h3>
						</div><!-- /.box-header -->					
						
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody>
									<tr>
										<th width="25%">&nbsp;</th>
										<th width="25%">Dwat</th>
										<th width="25%">Draft</th>
										<th width="25%">TPC Basis Full Draft</th>
									</tr>
									<tr>
										<td>Summer</td>
										<td><label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"DWT");?></label></td>
										<td><label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"DRAFTM");?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('SUMMER_3','vessel_master_1',$_REQUEST['id']);?></label></td>
									</tr>
									<tr>
										<td>Winter</td>
										<td><label><?php echo $obj->getVesselParticularData('WINTER_1','vessel_master_1',$_REQUEST['id']);?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('WINTER_2','vessel_master_1',$_REQUEST['id']);?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('WINTER_3','vessel_master_1',$_REQUEST['id']);?></label></td>
									</tr>
									
									<tr>
										<td>Tropical</td>
										<td><label><?php echo $obj->getVesselParticularData('TROPICAL_1','vessel_master_1',$_REQUEST['id']);?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('TROPICAL_2','vessel_master_1',$_REQUEST['id']);?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('TROPICAL_3','vessel_master_1',$_REQUEST['id']);?></label></td>
									</tr>
									
									<tr>
										<td>Summer Timber</td>
										<td><label><?php echo $obj->getVesselParticularData('SUMMER_TIM_1','vessel_master_1',$_REQUEST['id']);?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('SUMMER_TIM_2','vessel_master_1',$_REQUEST['id']);?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('SUMMER_TIM_3','vessel_master_1',$_REQUEST['id']);?></label></td>
									</tr>
									
									<tr>
										<td>Winter Timber</td>
										<td><label><?php echo $obj->getVesselParticularData('WINTER_TIM_1','vessel_master_1',$_REQUEST['id']);?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('WINTER_TIM_2','vessel_master_1',$_REQUEST['id']);?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('WINTER_TIM_3','vessel_master_1',$_REQUEST['id']);?></label></td>
									</tr>
									
									<tr>
										<td>Tropical Timber</td>
										<td><label><?php echo $obj->getVesselParticularData('TROPICAL_TIM_1','vessel_master_1',$_REQUEST['id']);?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('TROPICAL_TIM_2','vessel_master_1',$_REQUEST['id']);?></label></td>
										<td><label><?php echo $obj->getVesselParticularData('TROPICAL_TIM_3','vessel_master_1',$_REQUEST['id']);?></label></td>
									</tr>
								</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
				
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Is Vessel Field For Transit Of : Panama Canal ?
							<address>
								<label>
									<?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('PANAMA_CANAL','vessel_master_1',$_REQUEST['id'])); ?>
								</label>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Is Vessel Field For Transit Of : Suez Canal ?
							<address>
								<label>
									<?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('SUEZ_CANAL','vessel_master_1',$_REQUEST['id'])); ?>
								</label>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Is Vessel Field For Transit Of : ST Lawrence Seaway ?
							<address>
								<label>
									<?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('SEAWAY','vessel_master_1',$_REQUEST['id'])); ?>
								</label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							For Panama Canal Suitable Vessel State Deadweight All Told (Metric Tons) On 39ft 6ins (12.039M) (SG 0.9954)
							<address>
								<label><?php echo $obj->getVesselParticularData('PANAMA_CANAL_1','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Is Panama Deadweight All Told Affected By Vessel’S Bilge Turn Radius ?
							<address>
								<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('PANAMA_RADIUS','vessel_master_1',$_REQUEST['id'])); ?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							For ST Lawrence Seaway Size Vessel State Deadweight All Told (Metric Tons) Basis 26ft (7.92M) Fresh Water
							<address>
								<label><?php echo $obj->getVesselParticularData('SEAWAY_1','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							GT/NT : International
							<address>
								<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"GRT_NRT");?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							GT/NT : Suez
							<address>
								<label><?php echo $obj->getVesselParticularData('GT_SUEZ','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							GT/NT : Panama
							<address>
								<label><?php echo $obj->getVesselParticularData('GT_PANAMA','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							GT/NT : British
							<address>
								<label><?php echo $obj->getVesselParticularData('GT_BRITISH','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Length Overall(METRES)
							<address>
								<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"LOA");?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Length Between Perpendiculars (METRES)
							<address>
								<label><?php echo $obj->getVesselParticularData('LBW','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Extreme Breadth(METRES)
							<address>
								<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"EXT_BREADTH");?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Depth moulded(METRES)
							<address>
								<label><?php echo $obj->getVesselParticularData('DEPTH_MODULE','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
					</div>
					
					
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Distance (METRES) From Waterline to Top Of Hatch Coamings Basis 50 PCT Bunker - No Holds Flooded</h3>
						</div><!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody><tr>
									<th width="25%">&nbsp;</th>
									<th width="25%"> Ballast Condition (Ballast Holds Not Flooded)</th>
									<th width="25%"> Heavy Ballast Condition</th>
									<th width="25%"> Light Condition </th>
								</tr>
								<tr>
									<td>No 1 Hatch</td>
									<td><label><?php echo $obj->getVesselParticularData('NOH_1','vessel_master_1',$_REQUEST['id']);?></label></td>
									<td><label><?php echo $obj->getVesselParticularData('NOH_2','vessel_master_1',$_REQUEST['id']);?></label></td>
									<td><label><?php echo $obj->getVesselParticularData('NOH_3','vessel_master_1',$_REQUEST['id']);?></label></td>
									
								</tr>
								<tr>
									<td>Midships</td>
									<td><label><?php echo $obj->getVesselParticularData('MIDSHIPS_1','vessel_master_1',$_REQUEST['id']);?></label></td>
									<td><label><?php echo $obj->getVesselParticularData('MIDSHIPS_2','vessel_master_1',$_REQUEST['id']);?></label></td>
									<td><label><?php echo $obj->getVesselParticularData('MIDSHIPS_3','vessel_master_1',$_REQUEST['id']);?></label></td>
								</tr>
								<tr>
									<td>Last Hatch</td>
									<td><label><?php echo $obj->getVesselParticularData('LAST_HATCH_1','vessel_master_1',$_REQUEST['id']);?></label></td>
									<td><label><?php echo $obj->getVesselParticularData('LAST_HATCH_2','vessel_master_1',$_REQUEST['id']);?></label></td>
									<td><label><?php echo $obj->getVesselParticularData('LAST_HATCH_3','vessel_master_1',$_REQUEST['id']);?></label></td>
								</tr>
								
							</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
				
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Distance (METRES) From Keel to Top Of Hatch Coamings (Or Top Of Hatch Covers If Side-Rolling)</h3>
						</div><!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody><tr>
									<th width="50%">&nbsp;</th>
									<th width="50%">&nbsp;</th>
								</tr>
								<tr>
									<td>AT</td>
									<td><label><?php echo $obj->getVesselParticularData('AT_1','vessel_master_1',$_REQUEST['id']);?></label></td>
									
								</tr>
								<tr>
									<td>No 1 Hatch</td>
									<td><label><?php echo $obj->getVesselParticularData('NOH_1_1','vessel_master_1',$_REQUEST['id']);?></label></td>
								</tr>
								
								<tr>
									<td>Midships</td>
									<td><label><?php echo $obj->getVesselParticularData('MIDSHIPS_1_1','vessel_master_1',$_REQUEST['id']);?></label></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td><label><?php echo $obj->getVesselParticularData('LAST_HATCH_1_1','vessel_master_1',$_REQUEST['id']);?></label></td>
								</tr>
								
							</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							Vessel’s ballasting And Deballasting Time (Metric Tons Per Hour)
							<address>
								<label><?php echo $obj->getVesselParticularData('VESSEL_BALLAST','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-6 invoice-col">
							Distance (METRES) From Keel To Highest Points Of Vessel (INM Antenna)
							<address>
								<label><?php echo $obj->getVesselParticularData('DISTANCE_KEEL','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
					</div>
				
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Capacity Of : Ballast Tanks </h3>
						</div><!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody><tr>
									<th width="50%">&nbsp;</th>
									<th width="50%">&nbsp;</th>
								</tr>
								<tr>
									<td>Full Ballast</td>
									<td><label><?php echo $obj->getVesselParticularData('FULL_BALLAST_1','vessel_master_1',$_REQUEST['id']);?></label></td>
									
								</tr>
								<tr>
									<td>Light Ballast</td>
									<td><label><?php echo $obj->getVesselParticularData('LIGHT_BALLAST_1','vessel_master_1',$_REQUEST['id']);?></label></td>
								</tr>
								
								<tr>
									<td>Top Side Tank Empty</td>
									<td><label><?php echo $obj->getVesselParticularData('TSTE_1','vessel_master_1',$_REQUEST['id']);?></label></td>
								</tr>
								
								<tr>
									<td>&nbsp;</td>
									<td><label><?php echo $obj->getVesselParticularData('COBT','vessel_master_1',$_REQUEST['id']);?></label></td>
								</tr>
								
							</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							Capacity Of : Ballast Holds Capacity (State Which Hold (S))
							<address>
								<label><?php echo $obj->getVesselParticularData('COBC','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Constants Excluding Freshwater
							<address>
								<label><?php echo $obj->getVesselParticularData('CEF','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Daily Freshwater Consumption
							<address>
								<label><?php echo $obj->getVesselParticularData('DFC','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Freshwater Capacity 
							<address>
								<label><?php echo $obj->getVesselParticularData('FWC','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							State Capacity And Daily
							<address>
								<label><?php echo $obj->getVesselParticularData('SCAD','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Production Of Evaporator
							<address>
								<label><?php echo $obj->getVesselParticularData('POE','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Normal Fresh Water Reserve 
							<address>
								<label><?php echo $obj->getVesselParticularData('NFWR','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Vessel is Fitted With Shaft Generator 
							<address>
								<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('SHAFT_GEN','vessel_master_1',$_REQUEST['id'])); ?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Vessel’s On board Electrical Supply (V/Hz)
							<address>
								<label><?php echo $obj->getVesselParticularData('VOES','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Details Of Alternative Supply, If Any
							<address>
								<label><?php echo $obj->getVesselParticularData('DOAS','vessel_master_1',$_REQUEST['id']);?></label>
							</address>
						</div><!-- /.col -->
					</div>
					
				</form>
				
				<!----------------------------------------------------------------------------------------------------------->
				<!------------------------------------------End Main Form---------------------------------------------------->
				<!----------------------------------------------------------------------------------------------------------->
				
				<div class="row" style="height:20px"></div>
				<div class="row invoice-info">
					<div class="col-md-12">
						<!-- Custom Tabs -->
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#tab_1">CHAPTER 1</a></li>
								<li class=""><a data-toggle="tab" href="#tab_2">CHAPTER 2</a></li>
								<li class=""><a data-toggle="tab" href="#tab_3">CHAPTER 3</a></li>
								<li class=""><a data-toggle="tab" href="#tab_4">CHAPTER 4</a></li>
								<li class=""><a data-toggle="tab" href="#tab_5">CHAPTER 5</a></li>
								<li class=""><a data-toggle="tab" href="#tab_6">CHAPTER 6</a></li>
								<li class=""><a data-toggle="tab" href="#tab_7">CHAPTER 7</a></li>
								<li class=""><a data-toggle="tab" href="#tab_8">CHAPTER 8</a></li>
							</ul>
                            <div class="tab-content">
								<div id="tab_1" class="tab-pane active">
									<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
									
									
									
									</form>
								
								</div>
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab One Form------------------------------------------------->
								<!----------------------------------------------------------------------------------------------------------->
                                <div id="tab_2" class="tab-pane">
									<form role="form" name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Classification Society, Surveys And Certificates
												</h2>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Name Of Classification Society 
												<address>
													<label><?php echo $obj->getClaSocNameOnId($obj->getVesselParticularData('CLASS_SOC_ID','vessel_master_2',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Class Notation  
												<address>
													<label><?php echo $obj->getVesselParticularData('CLASS_NOTATION','vessel_master_2',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Date Of Last Special Survey<br/><br/>
												<address>
													<label><?php if($obj->checkVesselDate('DATE_LAST_1','vessel_master_2',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_LAST_1','vessel_master_2',$_REQUEST['id']))); } ?><label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Date Of Last Annual Survey<br/><br/>
												<address>
													<label><?php if($obj->checkVesselDate('DATE_LAST_2','vessel_master_2',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_LAST_2','vessel_master_2',$_REQUEST['id']))); } ?><label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Vessel Entered In Classification Approved Enhanced Survey Programme
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('CLASS_1','vessel_master_2',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/><br/><br/>Date Of Last Inspection
												<address>
													<label><?php if($obj->checkVesselDate('DATE_INS_1','vessel_master_2',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_INS_1','vessel_master_2',$_REQUEST['id']))); } ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/><br/><br/>Date Of Next Inspection
												<address>
													<label><?php if($obj->checkVesselDate('DATE_INS_2','vessel_master_2',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_INS_2','vessel_master_2',$_REQUEST['id']))); } ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Dose Vessel Comply With IACS Unified Requirements Regarding Number 1 Cargo Hold And Double Bottom Tank Steel Structure ?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('STR','vessel_master_2',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-6 invoice-col">
												Has This Compliance Been Verified By The Classification Society? 
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('STR_1','vessel_master_2',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="box">
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody><tr>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
													</tr>
													<tr>
														<td>Date And Place Of Last DryDock</td>
														<td>
															<label><?php if($obj->checkVesselDate('DATE_DRYDOCK','vessel_master_2',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_DRYDOCK','vessel_master_2',$_REQUEST['id']))); } ?><label>
														
														</td>
														
													</tr>
												</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												Has Vessel Been Involved in any Groundings Or Collision in last 12 Month? If So Give Full Details
												<address>
													<label><?php echo $obj->getVesselParticularData('GROUND','vessel_master_2',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/><br/>
												Is Vessel ISM Certified?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('ISM','vessel_master_2',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											
											<div class="col-sm-6 invoice-col">
												State - DOC (DOCUMENT OF COMPLIANCE) Certificate Number/ISsuing Authority
												<address>
													<label><?php echo $obj->getVesselParticularData('STATE_DOC','vessel_master_2',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-6 invoice-col">
												State - SMC (SAFETY MANAGEMENT) Certificate Number/Issuing Authority
												<address>
													<label><?php echo $obj->getVesselParticularData('STATE_SMC','vessel_master_2',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="box">
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody><tr>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
													</tr>
													<tr>
														<td>Give Date Of Last And Next Audit</td>
														<td>
															<label><?php if($obj->checkVesselDate('DATE_LAST_AUDIT','vessel_master_2',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_LAST_AUDIT','vessel_master_2',$_REQUEST['id']))); } ?></label>
														</td>
														<td>
															<label><?php if($obj->checkVesselDate('DATE_NEXT_AUDIT','vessel_master_2',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_NEXT_AUDIT','vessel_master_2',$_REQUEST['id']))); } ?></label>
														</td>
													</tr>
												</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										
										
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												State Outstanding Recommendations, If Any
												<address>
													<label><?php echo $obj->getVesselParticularData('STATE_OUTS','vessel_master_2',$_REQUEST['id']);?><label>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="box">
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody><tr>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
													</tr>
													<tr>
														<td>Advise Date And Place Of Last Port State Control</td>
														<td>
															<label><?php if($obj->checkVesselDate('DATE_ADVISE','vessel_master_2',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_ADVISE','vessel_master_2',$_REQUEST['id']))); } ?></label>
														</td>
														
													</tr>
												</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Did Vessel Pass Most Recent Port State Control Inspection Without Detention
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('DETEN','vessel_master_2',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
										
											<div class="col-sm-12 invoice-col">
												State Outstanding Recommendations , If Any
												<address>
													<label><?php echo $obj->getVesselParticularData('STATE_OUTS_1','vessel_master_2',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Is Vessel’s Crew Covered By Full ITF Or Bona Fide Trade Union Agreement Acceptable To ITF?
												<address>
													<label><?php echo $obj->getVesselParticularData('ITF','vessel_master_2',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												If Vessel Has ITF Agreement State Number, Date Of Issue And Expiry Date 
												<address>
													<label><?php echo $obj->getVesselParticularData('ITF_AGREE','vessel_master_2',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										</div>
										<div class="box">
											<div class="box-header">
												<h3 class="box-title">Certificates </h3>
											</div><!-- /.box-header -->
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody id="tb2">
														<tr>
															<th width="20%">Certificate Name</th>
															<th width="20%">Date Of Issue</th>
															<th width="20%">Date Of Last Annual Endorsement</th>
															<th width="20%">Date Of Expiry</th>
															<th width="15%">Upload</th>
														</tr>
														
														<?php
														$sql = "select * from vessel_master_slave where VESSEL_IMO_ID=".$_REQUEST['id'];
														$res = mysql_query($sql);
														$rec = mysql_num_rows($res);
														
														if($rec == 0)
														{
														?>
														<tr>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<?php }else{ ?>
														<?php 
															$i =1;
															while($rows = mysql_fetch_assoc($res))
															{
														?>
															<tr>
																<td>
																	<label><?php echo $obj->getCertificateNameOnId($rows['CERTIFICATE_ID']);?></label>
																</td>
																<td>
																	<label><?php if($obj->checkVesselDate('DATE_ISSUE','vessel_master_slave',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_ISSUE','vessel_master_slave',$_REQUEST['id']))); } ?></label>
																</td>
																<td>
																	<label><?php if($obj->checkVesselDate('DATE_LAST','vessel_master_slave',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_LAST','vessel_master_slave',$_REQUEST['id']))); } ?><label>
																	
																</td>
																<td>
																	<label><?php if($obj->checkVesselDate('DATE_EXPIRY','vessel_master_slave',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_EXPIRY','vessel_master_slave',$_REQUEST['id']))); } ?><label>
																</td>
																<?php if($obj->getVesselParticularData("UPLOAD","vessel_master_slave",$_REQUEST['id']) == ""){ ?>
																<td>
																											
																</td>
																<?php }else{ ?>
																<td>
																	<a href="../../attachment/<?php echo $rows['UPLOAD'];?>" target="_blank" data-toggle="tooltip" data-original-title="View Previous Upload"><i style="font-size:15px; margin-left:36px;"></i>View</a>
																	
																</td>
																<?php } ?>
															</tr>
														<?php } ?>
														<?php } ?>
													</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												Do Any Recommendations Appear On Any Of The Above Certificate ? If Yes State Full Details
												<address>
													<label><?php echo $obj->getVesselParticularData('FULL_DETAILS','vessel_master_2',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												IMO Registration Number
												<address>
													<label><?php echo $obj->getVesselParticularData('IMO_R_NO','vessel_master_2',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Expiry Date Of SMC Certificate
												<address>
													<label><?php if($obj->checkVesselDate('DATE_SMC','vessel_master_2',$_REQUEST['id']) == "0000-00-00"){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_SMC','vessel_master_2',$_REQUEST['id']))); } ?></label>
												</address>
											</div><!-- /.col -->
											
										</div>
										
									</form>	
								</div><!-- /.tab_2-pane -->
								
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Two Form------------------------------------------------->
								<!----------------------------------------------------------------------------------------------------------->
								
								<div id="tab_3" class="tab-pane">
									<form role="form" name="frm3" id="frm3" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Crew                   
												</h2>
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Number Of Crew
												<address>
													<label><?php echo $obj->getVesselParticularData('NOC','vessel_master_3',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Name And Nationality Of Master 
												<address>
													<label><?php echo $obj->getVesselParticularData('NOM','vessel_master_3',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Nationality Of Officers
												<address>
													<label><?php echo $obj->getVesselParticularData('NOO','vessel_master_3',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Nationality Of Crew
												<address>
													<label><?php echo $obj->getVesselParticularData('NOCR','vessel_master_3',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
									</form>
								</div><!-- /.tab_3-pane -->
								
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Three Form----------------------------------------------->
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_4" class="tab-pane">
									<form role="form" name="frm4" id="frm4" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Cargo Arrangements Holds
												</h1>
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-6 invoice-col">
												Number Of Holds
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"NOH");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-6 invoice-col">
												Are Vessel’s Holds Clear And Free Of Any Obstructions?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('HOLDS_CLEAR','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
										
											<div class="col-sm-12 invoice-col">
												GRAIN/BALE Capacity In Holds
												<address>
													<label><?php echo $obj->getVesselParticularData('GRAIN','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										
											<div class="col-sm-12 invoice-col">
												Is Vessel Strengthened For The Carriage Of Heavy Cargoes? If Yes State Witch Holds May Be Left Empty
												<address>
													<label><?php echo $obj->getVesselParticularData('CARRIAGE','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-6 invoice-col">
												Is Tanktop Steel And Suitable For Grab Discharge?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('TANKTOP','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-6 invoice-col">
												State Whether Bulkhead Corrugations Vertical Or Horizontal
												<address>
													<label><?php echo $obj->getVesselParticularData('BULKHEAD','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Tanktop Strength (METRIC TONS PER SQM)
												<address>
													<label><?php echo $obj->getVesselParticularData('SQM','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Are Holds CO2 Fitted?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('CO2','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
										</div>
										<div class="row invoice-info">
										
											<div class="col-sm-6 invoice-col">
												Are Holds Fitted With Smoke Detection System?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('DET_SYS','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-6 invoice-col">
												Is Vessel Fitted With Australian Type Approved Holds Ladders
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('LADDER','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Has Vessel A functioning Class Certified LOADMASTER/LOADICATOR Or Similar Calculator
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('CALC','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Are Holds Hoppered At? : Hold Side 
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('HOLD_SIDE','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Are Holds Hoppered AT? : Forward Bulkhead 
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('F_BULKHEAD','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Are Holds Hoppered At?  : AFT Bulkhead 
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('AFT_BULKHEAD','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Are Holds Hoppered At? : Can Vessel’s Holds Be Described As Box Shaped 
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('SHAPED','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Measurement Of Any Tank SLOPES/HOPPERING(Height And Distance From Vessel’s Side At Tank Top) (METRES)
												<address>
													<label><?php echo $obj->getVesselParticularData('MOATS','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="box">
											<div class="box-header">
												<h3 class="box-title"> Flat Floor Measurement Of Cargo Holds At Tank Top (METRES)</h3>
											</div><!-- /.box-header -->
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody>
													<tr>
														<th width="25%">&nbsp;</th>
														<th width="25%">Length- mtrs</th>
														<th width="25%">Width(Fwd)-mtrs</th>
														<th width="25%">Width(Aft)- mtrs</th>
													</tr>
													<?php
														$arr = array("H1","H2","H3","H4","H5","H6","H7");
														$j = 1;
														for($i=0;$i<sizeof($arr);$i++)
														{
													?>
													<tr>
														<td><?php echo $arr[$i];?></td>
														<td><label><?php echo $obj->getVesselParticularData($arr[$i].'_1','vessel_master_4',$_REQUEST['id']);?></label></td>
														<td><label><?php echo $obj->getVesselParticularData($arr[$i].'_2','vessel_master_4',$_REQUEST['id']);?></label></td>
														<td><label><?php echo $obj->getVesselParticularData($arr[$i].'_3','vessel_master_4',$_REQUEST['id']);?></label></td>
													</tr>
													<?php $j++;
													}?>
												</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												Are Vessel’s Holds electrically Ventilated?If Yes State Number Of Airchanges Per Hour Basis Empty Holds 
												<address>
													<label><?php echo $obj->getVesselParticularData('VENT','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Deck And Hatches
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Number Of Hatches
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"NOHA");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Make And Type Of Hatch Covers 
												<address>
													<label><?php echo $obj->getVesselParticularData('NOHA_COV','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Hatch Size (METRES)
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"HATCH_SIZE");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Strength Of Hatch Cover (METRIC TONS PER SQM)
												<address>
													<label><?php echo $obj->getVesselParticularData('HATCH_SQM','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Distance From Ship’s Rail TO Near And Far Edge Of Hatch COVERS/COAMING Near And Far (METRES)
												<address>
													<label><?php echo $obj->getVesselParticularData('FAR','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Distance From Bow To Fore Of 1st Hold Opening (METRES)
												<address>
													<label><?php echo $obj->getVesselParticularData('OPEN_1','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Distance From Stern To AFT Of Last Hold Opening (METRES)
												<address>
													<label><?php echo $obj->getVesselParticularData('LAST_OPEN','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												State Deck Strength (METRIC TONS PER SQM)
												<address>
													<label><?php echo $obj->getVesselParticularData('STR_SQM','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Cement Hole Numbers Per Hold
												<address>
													<label><?php echo $obj->getVesselParticularData('CHNPH','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Diameter Of Holes
												<address>
													<label><?php echo $obj->getVesselParticularData('DOH','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Position Of Holes
												<address>
													<label><?php echo $obj->getVesselParticularData('POH','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Cargo Gear
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												If Geared State Make And Type
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"CARGO_GEAR");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Number Of Cranes And Where Situated
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"CRANESIZE");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Outreach (METRES) Of Gear - Beyond Ship’s Rail
												<address>
													<label><?php echo $obj->getVesselParticularData('BSR','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Outreach (METRES) Of Gear - Beyond Ship’s Rail With Maximum Cargo Left On Hook
												<address>
													<label><?php echo $obj->getVesselParticularData('HOOK','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												If Gantry CRANES/HORIZONTAL Slewing Cranes state Minimum Clearance Distance Crane Hook To Top Of Hatch Coaming (METRES)
												<address>
													<label><?php echo $obj->getVesselParticularData('HATCH_COAM','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/><br/>Time Needed For Full Cycle With Maximum Cargo Lift On Hook
												<address>
													<label><?php echo $obj->getVesselParticularData('HOOK_CARGO','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Is Gear Combinable For Heavy Life
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('H_LIFT','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Are Winches ELECTRO-HYDRAULIC?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('E_HYDRA','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												If Vessel Has Grabs on Board State Type And Capacity
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"GRABSIZE");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Vessel Fitted With Sufficient Lights At Each Hatch For Night Work?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('N_WORK','vessel_master_4',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Is Vessel Logs Fitted? If Yes State Number And Type Of STANCHIONS/SOCKETS, If On Board
												<address>
													<label><?php echo $obj->getVesselParticularData('BOARD','vessel_master_4',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
										</div>
										
									</form>
								</div><!-- /.tab_4-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Four Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_5" class="tab-pane">
									<form role="form" name="frm5" id="frm5" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Speed/Consumption/Fuel Engine
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="box">
											<div class="box-header">
												<h3 class="box-title">
													State Vessel’s Consumption At About 13,5 Knots (Up To Beaufort Scale Force 4/Douglas Sea State 3,No Adverse Currents) As Follows:
												</h3>
											</div><!-- /.box-header -->
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody><tr>
														<th width="33%">&nbsp;</th>
														<th width="33%">About Metric Tons (MAIN ENGINE)</th>
														<th width="33%">About Metric Tons(AUXILIARIES)</th>
													</tr>
													<?php
														$arr_1 = array("Laden","Ballast");
														$j = 1;
														for($i=0;$i<sizeof($arr_1);$i++)
														{
														?>
														<tr>
															<td><?php echo $arr_1[$i];?></td>
															<td><label><?php echo $obj->getVesselParticularData($arr_1[$i].'_1','vessel_master_5',$_REQUEST['id']);?></label></td>
															<td><label><?php echo $obj->getVesselParticularData($arr_1[$i].'_2','vessel_master_5',$_REQUEST['id']);?></label></td>
														</tr>
													<?php $j++;}?>
												</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Bunker Grades
												<address>
													<label><?php echo $obj->getVesselParticularData('BUNKER_G','vessel_master_5',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Bunker Capacities 100 .PCT Capacity
												<address>
													<label><?php echo $obj->getVesselParticularData('BUNKER_C','vessel_master_5',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Port Consumption Per 24 Hours IDLE/WORKING (METRIC TONS)
												<address>
													<label><?php echo $obj->getVesselParticularData('P_WORK','vessel_master_5',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Engine Make And Type
												<address>
													<label><?php echo $obj->getVesselParticularData('EMAT','vessel_master_5',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Max Output BHP/RPM
												<address>
													<label><?php echo $obj->getVesselParticularData('MOB','vessel_master_5',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										</div>
									</form>
								</div><!-- /.tab_5-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Five Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
								
									
								<div id="tab_6" class="tab-pane">
									<form role="form" name="frm6" id="frm6" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Communications
												</h1>                            
											</div><!-- /.col -->
										</div>
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												<br/>Call Sign
												<address>
													<label><?php echo $obj->getVesselParticularData('CALL_SIGN','vessel_master_6',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Name Of Radio Station Which Vessel Monitoring
												<address>
													<label><?php echo $obj->getVesselParticularData('NOR','vessel_master_6',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Specify Vessel’s Satellite Communication System
												<address>
													<label><?php echo $obj->getVesselParticularData('COMM_SYS','vessel_master_6',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Insurances
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												<br/>Hull And Machinery Insured value
												<address>
													<label><?php echo $obj->getVesselParticularData('HAMIV','vessel_master_6',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Name Of Owners P And I Insures 
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"P_I");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Where is Owners Hull And Machinery Placed?
												<address>
													<label><?php echo $obj->getVesselParticularData('PLACED','vessel_master_6',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Miscellaneous
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												State Last 5 (Five) Cargoes Carried And Load And Discharge Port(S) With Most Recent First
												<address>
													<label><?php echo $obj->getVesselParticularData('CARGO','vessel_master_6',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Is Vessel Fitted For Carriage Of Grain in Accordance With Chapter V1 Of Solas 1974 And Amendments Without Requiring Bagging, Strapping And Securing When Loading A Full Cargo (DEADWEIGHT) Of Heavy Grain in Bulk (STOWAGE FACTOR 42 CUFT) With Ends Untrimmed?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('CARGO_1','vessel_master_6',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												State Number Of Holds Which May Be Left Slack Without Requiring Bagging, Strapping And Securing 
												<address>
													<label><?php echo $obj->getVesselParticularData('CARGO_2','vessel_master_6',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
										</div>
										
									</form>
										
								</div><!-- /.tab_6-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Six Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
								
								
								<div id="tab_7" class="tab-pane">
									<form role="form" name="frm7" id="frm7" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Container Bulkers/ Multi Purpose (Only To Be Completed If Applicable)               
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Capacity In Direct Stow Of TUE/FEU Basis : Empty
												<address>
													<label><?php echo $obj->getVesselParticularData('EMPTY','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Capacity In Direct Stow Of TEU/FEU Basis : Tons Homogeneous Weight
												<address>
													<label><?php echo $obj->getVesselParticularData('THW','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Are All Containers Within Reach Of Vessel’s GEAR:(YES/NO) If No State Self Sustained Capacity
												<address>
													<label><?php echo $obj->getVesselParticularData('GEAR','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>If Vessel Fitted With All Permanent And Loose FITTINGS/LASHING Materials For Above Number Of TEU/FEU?
												<address>
													<label>
													<?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('FEU','vessel_master_7',$_REQUEST['id'])); ?>
													</label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Vessel Fitted With Recent HOLES/SHOES On Tanktop And Container Shoes On Weatherdeck And Hatch Covers?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('COVERS','vessel_master_7',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/><br/>Advise Stack Weight And Number Of Tiers ON/UNDERDECK - PER TEU
												<address>
													<label><?php echo $obj->getVesselParticularData('PER_TEU','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Advise Stack Weight And Number Of Tiers ON/UNDERDECK - PER FEU
												<address>
													<label><?php echo $obj->getVesselParticularData('PER_FEU','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Has Vessel A Container Spreader On Board?
												<address>
													<label><?php echo $obj->getVesselParticularData('SPREAD_BOARD','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Number And Type Of Reefer Plugs
												<address>
													<label><?php echo $obj->getVesselParticularData('REEFER_PLUGS','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Tweendeckers               
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Has Vessel Folding Tweens?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('TWEENS','vessel_master_7',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Number Of HOLDS/HATCHES
												<address>
													<label><?php echo $obj->getVesselParticularData('NOHH','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Type Of Hatches
												<address>
													<label><?php echo $obj->getVesselParticularData('TOH','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Hatch Size (METRES) : WEATHERDECK
												<address>
													<label><?php echo $obj->getVesselParticularData('WEATHER_DECK','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->

											<div class="col-sm-4 invoice-col">
												Hatch Size (METRES) : TWEENDECK
												<address>
													<label><?php echo $obj->getVesselParticularData('TWEEN_DECK','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Are Tweendeckers Flush ?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('FLUSH','vessel_master_7',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Strengths (METRIC TONS PER SQM) : TANKTOP
												<address>
													<label><?php echo $obj->getVesselParticularData('TANKTOP','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Strengths (METRIC TONS PER SQM) : WEATHERDECK
												<address>
													<label><?php echo $obj->getVesselParticularData('SQM_WD','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Strengths (METRIC TONS PER SQM) : HATCHCOVERS
												<address>
													<label><?php echo $obj->getVesselParticularData('SQM_HC','vessel_master_7',$_REQUEST['id']);?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Is Vessel Fully Cargo Batten Fitted?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('B_FITTED','vessel_master_7',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Vessel CO2 FITTED/ELECTRICALLY VENTILATED?
												<address>
													<label><?php echo $obj->getNameBasedOnID($obj->getVesselParticularData('CO2_FITT','vessel_master_7',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Supplementary Information For Specific COMMODITIES/TRAD
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												Remark
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"REMARKS");?></label>
												</address>
											</div><!-- /.col -->
										</div>
										
									</form>
										
								</div><!-- /.tab_7-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Seven Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_8" class="tab-pane">
									<form role="form" name="frm8" id="frm8" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												&nbsp;
												<address>
													<label>
													<a  href="pdf_bulk_carrier.php?id=<?php echo $_REQUEST['id']; ?>&id1=1" title="Vessel Particular PDF">PDF</a>
													</label>
												</address>
											</div><!-- /.col -->
										
											<div class="col-sm-4 invoice-col">
												Name
												<address>
													<label><?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME"));?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Type
												<address>
													<label><?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_TYPE")); ?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Built Date
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"YEARBUILT");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Flag
												<address>
													<label><?php echo $obj->getCountryNameOnId($obj->getVesselParticularData('FLAG_ID','vessel_master_1',$_REQUEST['id']));?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Summer Dwat
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"DWT");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Summer Draft
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"DRAFTM");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												LOA (M)
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"LOA");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Extreme Breadth(M)
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"EXT_BREADTH");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												GRT / NRT : International
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"GRT_NRT");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Grain (M³)
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"GRAIN");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Bale (M³)
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"BALE");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Number Of Holds 
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"NOH");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Number Of Hatches
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"NOHA");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Cargo Gear (MAIN DETAILS)
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"CARGO_GEAR");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Crane Size
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"CRANESIZE");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Grab (MAIN DETAILS)
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"GRABSIZE");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												IMO Number
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"IMO_NO");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Name Of Owners P And I Insurers
												<address>
													<label><?php echo $obj->getVesselIMOData($_REQUEST['id'],"P_I");?></label>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Name Of Classification Society 
												<address>
													<label><?php echo $obj->getClaSocNameOnId($obj->getVesselParticularData('CLASS_SOC_ID','vessel_master_2',$_REQUEST['id'])); ?></label>
												</address>
											</div><!-- /.col -->
											
											
										</div>
									</form>						
								</div><!-- /.tab_8-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Eight Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
								
							</div><!-- /.tab-content -->
						</div><!-- nav-tabs-custom -->
					</div>
				</div>
				<!--  content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){

$(".areasize").autosize({append: "\n"});

$('.datepiker').datepicker({
    format: 'dd-mm-yyyy'
});

$("#frm0").validate({
	rules: {
		
	},
	messages: {
		
	},
	submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

});

</script>
	</body>
</html>