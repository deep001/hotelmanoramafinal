<?php 
session_start();
require_once("../../includes/display_internal_user_relet.inc.php");
require_once("../../includes/functions_internal_user_relet.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = @$_REQUEST['id'];
$user = $_REQUEST['user'];
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertVesselCommercialParametersDetails();
	header('Location : ./fleet.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id."&user=".$user;

if($user == 'EXTERNAL_USER')
{
	$name = $obj->getExternalVesselData($id,"VESSEL_NAME"); $type = $obj->getVesselTypeBasedOnID($obj->getExternalVesselData($id,"VESSEL_TYPE"));
}else{
	$name = $obj->getVesselIMOData($id,"VESSEL_NAME"); $type = $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($id,"VESSEL_TYPE"));}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-anchor"></i>&nbsp;Fleet &nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Fleet</li>
						<li class="active">Commercial Parameter</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
					<!--   content put here..................-->
					<div align="right"><a href="fleet.php"><button class="btn btn-info btn-flat">Back</button></a></div>
					<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header" style=" text-align:center;">
								 COMMERCIAL - PARAMETERS <span style="float:right"><a href="#?"  title="Pdf" onClick="getPdf();"><img src="../../img/pdf_button.png" height="16" width="16"></a></span>
								</h2>                            
							</div><!-- /.col -->
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Main Data</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<tbody>
										<tr>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
										</tr>
										<tr>
											<td>&nbsp;&nbsp;&nbsp;Vessel Name</td>
											<td>
												<input type="text" name="txtVName" id="txtVName" class="form-control" readonly value="<?php echo $name;?>" placeholder="Vessel Name"/>
											</td>
											<td>&nbsp;&nbsp;&nbsp;Vessel Type</td>
											<td>
												<input type="text" name="txtVType" id="txtVType" class="form-control" readonly value="<?php echo $type;?>" placeholder="Vessel Type"/>
											</td>
										</tr>
										
										<tr>
											<td>&nbsp;&nbsp;&nbsp;Date</td>
											<td>
												<?php if($obj->getVesselParameterData($id,"MAIN_DATA_DATE",$user) != ""){?>
													<input type="text" name="txtDate" id="txtDate" class="form-control datepiker" value="<?php echo date("d-m-Y",strtotime($obj->getVesselParameterData($id,"MAIN_DATA_DATE",$user)));?>" placeholder="dd-mm-yyyy" />
												<?php }else{?>
													<input type="text" name="txtDate" id="txtDate" class="form-control datepiker" value="<?php echo date("d-m-Y",time())?>" placeholder="dd-mm-yyyy" />
												<?php }?>
											</td>
											<td>&nbsp;&nbsp;&nbsp;DWT <span style="font-size:11px; font-style:italic;">(Summer)</span></td>
											<td>
												<input type="text" name="txtDWTS" id="txtDWTS" class="form-control" value="<?php echo $obj->getVesselIMOData($id,"DWT");?>" readonly placeholder="" />
											</td>
										</tr>
										
										<tr>
											<td>&nbsp;&nbsp;&nbsp;Draft <span style="font-size:10px; font-style:italic;">(Summer)</td>
											<td>
												<input type="text" name="txtDraft" id="txtDraft" class="form-control"  value="<?php echo $obj->getVesselIMOData($id,"DRAFTM");?>" readonly />
											</td>
											<td>&nbsp;&nbsp;&nbsp;TPC</td>
											<td>
												<input type="text" name="txtTPC" id="txtTPC" class="form-control" value="<?php echo $obj->getVesselParticularData('SUMMER_3','vessel_master_1',$id);?>" readonly placeholder="TPC" />
											</td>
										</tr>
										
										
									</tbody>
								</table>
							</div><!-- /.box-body -->
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Speed Data</h3>
							</div><!-- /.box-header -->	
							<div class="box-body no-padding">
								<table class="table table-striped">
									<tbody>
										<tr>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
										</tr>
										<tr style="font-weight:bold;">
											<td align="center">&nbsp;</td>
											<td align="right">Full Speed</td>
											<td align="right">Eco Speed 1</td>
											<td align="right">Eco Speed 2&nbsp;</td>
											
										</tr>
										<tr>
											<td>&nbsp;&nbsp;&nbsp;Ballast Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
											<td>
												<input type="text" name="txtBFullSpeed" id="txtBFullSpeed" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"B_FULL_SPEED",$user);?>" tabindex="1"/>
											</td>
											<td>
												<input type="text" name="txtBEcoSpeed1" id="txtBEcoSpeed1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"B_ECO_SPEED1",$user);?>" tabindex="9"/>
											</td>
											<td>
											<input type="text" name="txtBEcoSpeed2" id="txtBEcoSpeed2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"B_ECO_SPEED2",$user);?>" tabindex="17"/>
											</td>
										</tr>
										<tr>
											<td>&nbsp;&nbsp;&nbsp;Laden Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
											<td>
												<input type="text" name="txtLFullSpeed" id="txtLFullSpeed" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"L_FULL_SPEED",$user);?>" tabindex="2"/>
											</td>
											<td>
												<input type="text" name="txtLEcoSpeed1" id="txtLEcoSpeed1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"L_ECO_SPEED1",$user);?>" tabindex="10"/>
											</td>
											<td>
												<input type="text" name="txtLEcoSpeed2" id="txtLEcoSpeed2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"L_ECO_SPEED2",$user);?>" tabindex="18"/>
											</td>
										</tr>
									</tbody>
								</table>
							</div><!-- /.box-body -->
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">FO Consumption MT/Day</h3>
							</div><!-- /.box-header -->	
							<div class="box-body no-padding">
								<table class="table table-striped">
									<tbody>
										<tr>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;&nbsp;&nbsp;FO Grade</td>
											<td>
											<input type="text" name="txtFOGrade" id="txtFOGrade" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"FO_GRADE",$user);?>" />
											</td>
										</tr>										
										<tr style="font-weight:bold;">
											<td align="center">&nbsp;</td>
											<td align="right">Full Speed</td>
											<td align="right">Eco Speed 1</td>
											<td align="right">Eco Speed 2&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;&nbsp;&nbsp;Ballast Passage</td>
											<td>
												<input type="text" name="txtFOBPassageFS" id="txtFOBPassageFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"BP_FO_FULL_SPEED",$user);?>" tabindex="3"/>
											</td>
											<td>
												<input type="text" name="txtFOBPassageES1" id="txtFOBPassageES1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"BP_FO_ECO_SPEED1",$user);?>" tabindex="11"/>
											</td>
											<td>
												<input type="text" name="txtFOBPassageES2" id="txtFOBPassageES2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"BP_FO_ECO_SPEED2",$user);?>" tabindex="19"/>
											</td>
										</tr>
										
										<tr>
											<td>&nbsp;&nbsp;&nbsp;Laden Passage</td>
											<td>
												<input type="text" name="txtFOLPassageFS" id="txtFOLPassageFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"LP_FO_FULL_SPEED",$user);?>" tabindex="4"/>
											</td>
											<td>
												<input type="text" name="txtFOLPassageES1" id="txtFOLPassageES1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"LP_FO_ECO_SPEED1",$user);?>" tabindex="12"/>
											</td>
											<td>
												<input type="text" name="txtFOLPassageES2" id="txtFOLPassageES2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"LP_FO_ECO_SPEED2",$user);?>" tabindex="20"/>
											</td>
										</tr>
										
										<tr style="font-weight:bold;">
											<td align="center">&nbsp;</td>
											<td align="right">In Port - Idle</td>
											<td align="right">In Port - Working</td>
											<td align="right">Others&nbsp;</td>
										</tr>
										
										<tr>
											<td>&nbsp;&nbsp;&nbsp;</td>
											<td>
												<input type="text" name="txtFOIPIdleFS" id="txtFOIPIdleFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"PI_FO_FULL_SPEED",$user);?>" tabindex="5"/>
											</td>
											<td>
												<input type="text" name="txtFOIPWorkingFS" id="txtFOIPWorkingFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"PW_FO_FULL_SPEED",$user);?>" tabindex="13"/>
											</td>
											<td>
												<input type="text" name="txtFOOthersFS" id="txtFOOthersFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"OTH_FO_FULL_SPEED",$user);?>" tabindex="21"/>
											</td>
										</tr>
										
									</tbody>
								</table>
							</div><!-- /.box-body -->
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">DO Consumption per MT/Day</h3>
							</div><!-- /.box-header -->	
							<div class="box-body no-padding">
								<table class="table table-striped">
									<tbody>
										<tr>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;&nbsp;&nbsp;DO Grade</td>
											<td>
												<input type="text" name="txtDOGrade" id="txtDOGrade" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"DO_GRADE",$user);?>" />
											</td>
										</tr>										
										<tr style="font-weight:bold;">
											<td align="center">&nbsp;</td>
											<td align="right">Full Speed</td>
											<td align="right">Eco Speed 1</td>
											<td align="right">Eco Speed 2&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;&nbsp;&nbsp;Ballast Passage</td>
											<td>
												<input type="text" name="txtDOBPassageFS" id="txtDOBPassageFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"BP_DO_FULL_SPEED",$user);?>" tabindex="6"/>
											</td>
											<td>
												<input type="text" name="txtDOBPassageES1" id="txtDOBPassageES1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"BP_DO_ECO_SPEED1",$user);?>" tabindex="14"/>
											</td>
											<td>
												<input type="text" name="txtDOBPassageES2" id="txtDOBPassageES2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"BP_DO_ECO_SPEED2",$user);?>" tabindex="22"/>
											</td>
										</tr>
										
										<tr>
											<td>&nbsp;&nbsp;&nbsp;Laden Passage</td>
											<td>
												<input type="text" name="txtDOLPassageFS" id="txtDOLPassageFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"LP_DO_FULL_SPEED",$user);?>" tabindex="7"/>
											</td>
											<td>
												<input type="text" name="txtDOLPassageES1" id="txtDOLPassageES1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"LP_DO_ECO_SPEED1",$user);?>" tabindex="15"/>
											</td>
											<td>
												<input type="text" name="txtDOLPassageES2" id="txtDOLPassageES2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"LP_DO_ECO_SPEED2",$user);?>" tabindex="23"/>
											</td>
										</tr>
										
										<tr style="font-weight:bold;">
											<td align="center">&nbsp;</td>
											<td align="right">In Port - Idle</td>
											<td align="right">In Port - Working</td>
											<td align="right">Others&nbsp;</td>
										</tr>
										
										<tr>
											<td>&nbsp;&nbsp;&nbsp;</td>
											<td>
												<input type="text" name="txtDOIPIdleFS" id="txtDOIPIdleFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"PI_DO_FULL_SPEED",$user);?>" tabindex="8"/>
											</td>
											<td>
												<input type="text" name="txtDOIPWorkingFS" id="txtDOIPWorkingFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"PW_DO_FULL_SPEED",$user);?>" tabindex="16"/>
											</td>
											<td>
												<input type="text" name="txtDOOthersFS" id="txtDOOthersFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($id,"OTH_DO_FULL_SPEED",$user);?>" tabindex="24"/>
											</td>
										</tr>
										
									</tbody>
								</table>
							</div><!-- /.box-body -->
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Source / Other Details</h3>
							</div><!-- /.box-header -->	
							<div class="box-body no-padding">
								<table class="table table-striped">
									<tbody>
										<tr>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
											<th width="25%">&nbsp;</th>
										</tr>
										<tr>
											<tr>
											<td style="padding:25px;">&nbsp;&nbsp;&nbsp;Remarks</td>
											<td colspan="3">
												<textarea class="form-control areasize" name="txtRemarks" id="txtRemarks"><?php echo $obj->getVesselParameterData($id,"REMARKS",$user);?></textarea>
											</td>
										</tr>
										</tr>
									</tbody>
								</table>
							</div><!-- /.box-body -->
						</div>
						
						<div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="usertype" id="usertype" value="<?php echo $user; ?>" />
						</div>
					</form>
					<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function(){

$(".areasize").autosize({append: "\n"});

$('.datepiker').datepicker({
    format: 'dd-mm-yyyy'
});

$("#frm1").validate({
	rules: {
		txtDate:"required",
		txtBFullSpeed:{required : true , number : true},
		txtBEcoSpeed1:{required : true ,number : true},
		txtBEcoSpeed2:{required : true ,number : true},
		txtLFullSpeed:{required : true ,number : true},
		txtLEcoSpeed1:{required : true ,number : true},
		txtLEcoSpeed2:{required : true ,number : true},
		txtFOBPassageFS:{required : true ,number : true},
		txtFOBPassageES1:{required : true ,number : true},
		txtFOBPassageES2:{required : true ,number : true},
		txtFOLPassageFS:{required : true ,number : true},
		txtFOLPassageES1:{required : true ,number : true},
		txtFOLPassageES2:{required : true ,number : true},
		txtFOIPIdleFS:{required : true ,number : true},
		txtFOIPWorkingFS:{required : true ,number : true},
		txtFOOthersFS:{required : true ,number : true},
		txtDOBPassageFS:{required : true ,number : true},
		txtDOBPassageES1:{required : true ,number : true},
		txtDOBPassageES2:{required : true ,number : true},
		txtDOLPassageFS:{required : true ,number : true},
		txtDOLPassageES1:{required : true ,number : true},
		txtDOLPassageES2:{required : true ,number : true},
		txtDOIPIdleFS:{required : true ,number : true},
		txtDOIPWorkingFS:{required : true ,number : true},
		txtDOOthersFS:{required : true ,number : true},
		},
	messages: {	
		txtDate:"*",
		txtBFullSpeed:{number : "*"},
		txtBEcoSpeed1:{number : "*"},
		txtBEcoSpeed2:{number : "*"},
		txtLFullSpeed:{number : "*"},
		txtLEcoSpeed1:{number : "*"},
		txtLEcoSpeed2:{number : "*"},
		txtFOBPassageFS:{number : "*"},
		txtFOBPassageES1:{number : "*"},
		txtFOBPassageES2:{number : "*"},
		txtFOLPassageFS:{number : "*"},
		txtFOLPassageES1:{number : "*"},
		txtFOLPassageES2:{number : "*"},
		txtFOIPIdleFS:{number : "*"},
		txtFOIPWorkingFS:{number : "*"},
		txtFOOthersFS:{number : "*"},
		txtDOBPassageFS:{number : "*"},
		txtDOBPassageES1:{number : "*"},
		txtDOBPassageES2:{number : "*"},
		txtDOLPassageFS:{number : "*"},
		txtDOLPassageES1:{number : "*"},
		txtDOLPassageES2:{number : "*"},
		txtDOIPIdleFS:{number : "*"},
		txtDOIPWorkingFS:{number : "*"},

		txtDOOthersFS:{number : "*"},
		},
	submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

$("#txtBFullSpeed,#txtBEcoSpeed1,#txtBEcoSpeed2,#txtLFullSpeed,#txtLEcoSpeed1,#txtLEcoSpeed2,#txtFOBPassageFS,#txtFOBPassageES1,#txtFOBPassageES2,#txtFOLPassageFS,#txtFOLPassageES1,#txtFOLPassageES2,#txtFOIPIdleFS,#txtFOIPWorkingFS,#txtFOOthersFS,#txtDOBPassageFS,#txtDOBPassageES1,#txtDOBPassageES2,#txtDOLPassageFS,#txtDOLPassageES1,#txtDOLPassageES2,#txtDOIPIdleFS,#txtDOIPWorkingFS,#txtDOOthersFS").css({"text-align":"right"});
});

function getPdf()
{
	location.href='allPdf.php?id=40&imoid='+<?php echo $id; ?>+'&user='+$("#usertype").val();
}

</script>
    </body>
</html>