<?php 
session_start();
require_once("../../includes/display_internal_user_relet.inc.php");
require_once("../../includes/functions_internal_user_relet.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertVesselReletDetails();
	header('Location : ./vessel_relet.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rigts = $obj->getUserRights($uid,$moduleid,1);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 

$("#frm1").validate({
	rules: {
		selVName:"required"
		},
	messages: {	
		selVName:"*"
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

});


function getShowTcContractData()
{
   var tc_contract = <?php echo $obj->getTCContractDataWithJson();?>;
   if(tc_contract!=null && $("#selTCContractNo").val()!="")
   {
	   $.each(tc_contract, function(index, array) {
		   if(array['tc_conid'] == $("#selTCContractNo").val())
			$("#txtVName").val(array['vsl_name']);
			$("#txtVendorName").val(array['vendor_name']);
			$("#txtCPDate").val(array['cp_date']);
			$("#txtCPHire").val(array['cp_hire']);
			$("#txtFromDate").val(array['from_date']);
			$("#txtToDate").val(array['to_date']);
			$("#txtTotalDays").val(array['total_days']);
      });	
   }
   else
   {
	    $("#txtVName").val("");
		$("#txtVendorName").val("");
		$("#txtCPDate").val("");
		$("#txtCPHire").val("");
		$("#txtFromDate").val("");
		$("#txtToDate").val("");
		$("#txtTotalDays").val("");
   }
}
</script>

<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Vessel Relet</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="vessel_relet.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             ADD VESSEL RELET 
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                       <div class="col-sm-4 invoice-col">
                             TC Contract No.
                            <address>
                               <select  name="selTCContractNo" class="form-control" id="selTCContractNo" onChange="getShowTcContractData();" >
                                <?php 
                                $obj->getTCContractList();
                                ?>
                               </select>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                           Vessel Name
                            <address>
                               <input type="text" name="txtVName" id="txtVName" class="form-control" readonly  placeholder="Vessel Name" autocomplete="off" value=""/>
                              
                            </address>
                        </div><!-- /.col -->
                      <div class="col-sm-4 invoice-col">
                           Vendor Name
                            <address>
                                <input type="text" name="txtVendorName" id="txtVendorName" class="form-control" readonly placeholder="Vendor Name" autocomplete="off" value=""/>
                            </address>
                        </div> 
                     
					</div>
                    
                   
                    
                    <div class="row invoice-info">
                        
                        <div class="col-sm-4 invoice-col">
                             CP Date
                            <address>
                               <input type="text" name="txtCPDate" id="txtCPDate" class="form-control" readonly placeholder="dd-mm-yyyy" autocomplete="off" value=""/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             CP Hire
                            <address>
                               <input type="text" name="txtCPHire" id="txtCPHire" class="form-control" readonly placeholder="CP Hire" autocomplete="off" value=""/>
                            </address>
                        </div>  
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                             From Date
                            <address>
                               <input type="text" name="txtFromDate" id="txtFromDate" class="form-control" readonly placeholder="dd-mm-yyyy" autocomplete="off" value=""/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             To Date
                            <address>
                               <input type="text" name="txtToDate" id="txtToDate" class="form-control" readonly placeholder="dd-mm-yyyy" autocomplete="off" value=""/>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                             Total Days
                            <address>
                               <input type="text" name="txtTotalDays" id="txtTotalDays" class="form-control" readonly placeholder="Total Days" autocomplete="off" value="0"/>
                            </address>
                        </div>
					</div>
                     <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                             &nbsp;
                                <address>
								<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip">
									<i class="fa fa-paperclip"></i> Attachment
									<input type="file" class="form-control" multiple name="attach_file" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
								</div>
							</address>
                            </div><!-- /.col -->
                     </div>
				
					<?php 
					 	
					 	//if($rigts == 1)
						//{
					 ?>
					<div class="box-footer" align="right">
						<button type="submit" class="btn btn-primary btn-flat" >Submit</button>
						<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>
					<?php // } ?>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>