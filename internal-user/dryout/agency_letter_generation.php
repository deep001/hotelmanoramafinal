<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "vessel_in_post_ops.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "vessel_in_history.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "coa_in_ops_at_glance.php"; $page_bar="In Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else if($page == 5){$page_link = "coa_in_post_ops.php"; $page_bar="In Post Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else{$page_link = "coa_in_history.php"; $page_bar="Vessels in History - COA";$nav=16;$subtitle = 'COA';}

if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertGenerateAgencyLetterDetails();
	if($_REQUEST['txtsubmitid_'.$_REQUEST['txttabid'][0]] == 1)
	{
		
		header('Location:./agency_letter_generation.php?msg='.$msg.'&tab='.($_REQUEST['txttabid'][0]).'&comid='.$_REQUEST['comid'].'&page='.$_REQUEST['page']);
	}
	else
	{
		header('Location:./'.$page_link.'?msg=2');
	}
 }
$cost_sheet_id 	= 	$obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($cost_sheet_id);
$arr       = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$cost_sheet_id);
$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);


$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;
$msg = NULL;
if(isset($_REQUEST['tab']))
{
$tab = $_REQUEST['tab'];
}
else{$tab = 1;}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

$("[id^=txtPDAIniPayPer_],[id^=txtPDAFinPayPer_]").numeric();
$("[id^=txtETADATE_]").datetimepicker({
	format: 'dd-mm-yyyy hh:ii',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
});

$("[id^=txtDate_]").datepicker({ 
	format: 'dd-mm-yyyy',
	autoclose: true	 
});
$("textarea").autosize({append: "\n"});


<?php echo $obj->getValidateAgencyLetter($arr);?>;
});

function getSubmit(tabid,submitid,port,portid,ramdomid)
{ 
    if($("#selAList_"+tabid).val()!="")
	{
		if($("#txtCountry_"+tabid).val() != "")
		{
			jConfirm('Are you sure you want to submit this data?', 'Confirmation', function(r) 
			{
				if(r)
				{
					$("[id^=txttabid_],[id^=txtsubmitid_],[id^=txtport_],[id^=txtportid_]").val("");
					$("#txttabid_"+tabid).val(tabid);
					$("#txtsubmitid_"+tabid).val(submitid);
					$("#txtport_"+tabid).val(port);
					$("#txtportid_"+tabid).val(portid);
					$("#txtRandomID_"+tabid).val(ramdomid);
					var formname = "frm"+tabid;
					//alert(formname);
					$("#"+formname).submit();
					//document.formname.submit();
				} 
				else 
				{
					return false;
				}
			});
		}
		else
		{
			jAlert('Please add country for this port', 'Alert');
			return false;
		}
	}
	else
	{
		jAlert('Please add Vendor in cost sheet for this port', 'Alert');
		return false;
	}
} 


function getPdf(var1,var2)
{
	location.href='allPdf.php?id=2&gen_agency_id='+var1+'&port_type='+var2;
}

function getDelete(agencyid,tabid,comid,port,portid,var1)
{ 
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
	if(r){
			$.post("options.php?id=12",{agencyid:""+agencyid+"",comid:""+comid+"",port:""+port+"",portid:""+portid+"",tabid:""+tabid+""}, function(html) {
				if(html==1)
				{
					$("#tpRow_"+tabid+"_"+var1).remove();
				}
			});
		}
		else
		{
			return false;
		}
	});	
}


function getNewRow(val1)
{
	var id = $("#rowcount"+val1).val();
	if($("#selEntity_"+val1+"_"+id).val() != "" && $("#txtEntityName_"+val1+"_"+id).val()  != "" && $("#txtEntityEmail_"+val1+"_"+id).val()  != "")
	{
		id = parseInt(id) + 1;
		$('<tr id="prl_Row_'+val1+'_'+id+'"><td><a href="#pr" onclick="removeEntity('+val1+','+id+');" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select id="selEntity_'+val1+'_'+id+'" name="selEntity_'+val1+'_'+id+'" class="form-control" ></select></td><td><input type="text" name="txtEntityName_'+val1+'_'+id+'" id="txtEntityName_'+val1+'_'+id+'" class="form-control" placeholder=""  value="" autocomplete="off" /></td><td><input type="text" name="txtEntityEmail_'+val1+'_'+id+'" id="txtEntityEmail_'+val1+'_'+id+'" class="form-control" placeholder=""  value="" autocomplete="off" /></td></tr>').appendTo("#tblentity"+val1);
		$("#selEntity_"+val1+"_"+id).html($("#vendortype"+val1).html());
		$("#rowcount"+val1).val(id);
	}
	else
	{
		jAlert('Please fill all values.', 'Alert');
	}
}

function removeEntity(val1,val2)
{
   	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){
			$("#prl_Row_"+val1+"_"+val2).remove();
		}
		else
		{
			
		}
		});
}


function getNewRow1(val1)
{
	var id = $("#rowcount1"+val1).val();
	if($("#txtGrade_"+val1+"_"+id).val() != "" && $("#txtSupplier_"+val1+"_"+id).val()  != "" && $("#txtPhysical_"+val1+"_"+id).val()  != "")
	{
		id = parseInt(id) + 1;
		$('<tr id="prl_Row1_'+val1+'_'+id+'"><td><a href="#pr" onclick="removeEntity1('+val1+','+id+');" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select id="selBunkeringPort_'+val1+'_'+id+'" name="selBunkeringPort_'+val1+'_'+id+'" class="form-control" ></select></td><td><input type="text" name="txtGrade_'+val1+'_'+id+'" id="txtGrade_'+val1+'_'+id+'" class="form-control" placeholder=""  value="" autocomplete="off" /></td><td><input type="text" name="txtSupplier_'+val1+'_'+id+'" id="txtSupplier_'+val1+'_'+id+'" class="form-control" placeholder=""  value="" autocomplete="off" /></td><td><input type="text" name="txtPhysical_'+val1+'_'+id+'" id="txtPhysical_'+val1+'_'+id+'" class="form-control" placeholder=""  value="" autocomplete="off" /></td><td><input type="text" name="txtQuantity_'+val1+'_'+id+'" id="txtQuantity_'+val1+'_'+id+'" class="form-control" placeholder=""  value="" autocomplete="off" /></td></tr>').appendTo("#tblentity1"+val1);
		$("#rowcount1"+val1).val(id);
		$("#selBunkeringPort_"+val1+"_"+id).html($("#portlist"+val1).html());
	}
	else
	{
		jAlert('Please fill all values.', 'Alert');
	}
}

function removeEntity1(val1,val2)
{
   	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){
			$("#prl_Row1_"+val1+"_"+val2).remove();
		}
		else
		{
			
		}
		});
}

</script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}
form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?></li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content invoice">
					<!--   content put here..................-->
					<?php 
							if(isset($_REQUEST['msg'])){
								$msg = $_REQUEST['msg'];
								if($msg == 0){?>
									<div class="alert alert-success alert-dismissable">
										<i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<b>Congratulations!</b> Agency Letter Generation added/updated successfully.
									</div>
								<?php }?>
								<?php if($msg == 1){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Sorry!</b> this agent is already exists for this port.
								</div>
								<?php }?>
						<?php }?>
						<div align="right"><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
						<div style="height:10px;">&nbsp;</div>
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header" style="text-align:center;">
								 GENERATE PORT RELATED LETTERS
								</h2>                            
							</div><!-- /.col -->
						</div>			
						<div class="row invoice-info">
							<div class="col-md-12">
								<!-- Custom Tabs -->
								<div id="tabs" class="nav-tabs-custom">
									<ul class="nav nav-tabs">
									<?php for($i=0;$i<count($arr);$i++){$arr_portval = explode('@@',$arr[$i]);?>
										<li class="<?php if($tab == $i+1){ echo 'active';} ?>"><a href="#tabs_<?php echo $i+1;?>" data-toggle="tab"><?php echo $arr_portval[0]."-".$obj->getPortNameBasedOnID($arr_portval[1]);?></a></li>
									<?php }?>
									</ul>
									<div class="tab-content">
									
									<!----------------------------------------------- Start 1------------------------------------------------->
									<?php for($i=0;$i<count($arr);$i++){ 
									$arr_portval = explode('@@',$arr[$i]);?>
									<div id="tabs_<?php echo $i+1;?>" class="tab-pane <?php if($tab == $i+1){ echo 'active';} ?>">
									<form name="frm<?php echo $i+1;?>" id="frm<?php echo $i+1;?>" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>
                                    
                                    <?php
									$sql 		= "select * from generate_agency_letter where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_portval[0]."' and PORTID='".$arr_portval[1]."' and RANDOMID='".$arr_portval[2]."'";
									$res 	 	=  mysql_query($sql) or die($sql);
									$num        = mysql_num_rows($res); 
									$rows 		= mysql_fetch_assoc($res);
	?><input type="hidden" name="genAgencyId_<?php echo $i+1;?>" id="genAgencyId_<?php echo $i+1;?>" value="<?php echo $rows['GEN_AGENCY_ID'];?>" />
									<div class="row invoice-info">
										<div class="col-sm-9 invoice-col">&nbsp;</div>
										<div class="col-sm-3 invoice-col">
											Nom ID
											<address>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?php echo $obj->getCompareTableData($comid,"MESSAGE");?></strong>
											</address>
											<input type="hidden" name="txttabid[]" id="txttabid_<?php echo $i+1;?>" class="form-control" readonly value="" />
											<input type="hidden" name="txtsubmitid_<?php echo $i+1;?>" id="txtsubmitid_<?php echo $i+1;?>" class="form-control" readonly value="" />
											<input type="hidden" name="txtport_<?php echo $i+1;?>" id="txtport_<?php echo $i+1;?>" class="form-control" readonly value="" />
											<input type="hidden" name="txtportid_<?php echo $i+1;?>" id="txtportid_<?php echo $i+1;?>" class="form-control" readonly value="" /><input type="hidden" name="txtRandomID_<?php echo $i+1;?>" id="txtRandomID_<?php echo $i+1;?>" class="form-control" readonly value="" />
										</div>
									</div>
									
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Date
											<address>
												<input type="text" name="txtDate_<?php echo $i+1;?>" id="txtDate_<?php echo $i+1;?>" class="form-control" placeholder="dd-mm-yyyy" value="<?php if($num>0){echo date("d-m-Y",strtotime($rows['DATE'])) ;}?>" />
											</address>
										</div>
										<div class="col-sm-3 invoice-col">
											Vessel
											<address>
												<input type="text" name="txtVName_<?php echo $i+1;?>" id="txtVName_<?php echo $i+1;?>" class="form-control" placeholder="Vessel" readonly value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?>" />
											</address>
										</div>
										<div class="col-sm-3 invoice-col">
											Agent Name
											<address>
												<select name="selAList_<?php echo $i+1;?>" style="display:none;" class="form-control" id="selAList_<?php echo $i+1;?>">
												<?php 
												$obj->getVendorListNew();
												?>
												</select>
												<?php echo $obj->getVendorListNewData($arr_portval[4],"NAME")." (".$arr_portval[4].")";?>
												<script>$("#selAList_<?php echo $i+1;?>").val('<?php echo $arr_portval[4];?>');</script>
											</address>
										</div>
										<div class="col-sm-3 invoice-col">
											Cargo Qty (MT)
											<address>
												<input type="text" name="txtQty_<?php echo $i+1;?>" id="txtQty_<?php echo $i+1;?>" class="form-control" placeholder="Loaded Qty" value="<?php echo $arr_portval[3];?>" />
											</address>
										</div>
									</div>
									
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Country
											<address>
												<?php echo $obj->getCountryNameBasedOnID($obj->getCountryNameBasedOnPort($arr_portval[1]));?>
	<input type="hidden" name="txtCountry_<?php echo $i+1;?>" id="txtCountry_<?php echo $i+1;?>" class="form-control" placeholder="Country"  value="<?php echo $obj->getCountryNameBasedOnPort($arr_portval[1]);?>" readonly/>
											</address>
										</div>
										
										<div class="col-sm-3 invoice-col">
											Username
											<address>
												<input type="text" name="txtUName_<?php echo $i+1;?>" id="txtUName_<?php echo $i+1;?>" class="form-control" placeholder="Username" readonly value="<?php if($num>0){echo $rows['USERNAME'] ;}else{?><?php  echo $obj->getLoginShortNameBasedOnID()."/"?><?php echo $obj->getMaxAgencyNumber();?>/<?php echo $arr_portval[2];?><?php }?>" />
											</address>
										</div>
										<div class="col-sm-3 invoice-col">
											Password
											<address>
												<input type="text" name="txtPassword_<?php echo $i+1;?>" id="txtPassword_<?php echo $i+1;?>" class="form-control" placeholder="Password"  value="<?php if($num>0){echo $rows['PASSWORD'] ;}?>" autocomplete="off" />
											</address>
										</div>
									</div>
                                    
                                    
                                    
                                    <div class="row invoice-info">
										<div class="col-sm-2 invoice-col">
											Master's Name
											<address>
												<input type="text" name="txtTOfTolerance_<?php echo $i+1;?>" id="txtTOfTolerance_<?php echo $i+1;?>" class="form-control" placeholder="Master's Name"  value="<?php if($num>0){echo $rows['TERMO_OF_TOLERANCE'] ;}?>" autocomplete="off" />
											</address>
										</div>
										
										<div class="col-sm-3 invoice-col">
											Cargo Details
											<address>
												<input type="text" name="txtCargoPackDesc_<?php echo $i+1;?>" id="txtCargoPackDesc_<?php echo $i+1;?>" class="form-control" placeholder="Cargo Details"  value="<?php if($num>0){echo $rows['CARGO_PACKING_DESC'] ;}?>" autocomplete="off" />
											</address>
										</div>
										<div class="col-sm-7 invoice-col">
                                            <div class="row invoice-info">
                                                
                                                <div class="col-sm-3 invoice-col">
                                                    Tolerance (-) %
                                                    <address>
                                                        <input type="text" name="txtTolerancePer1_<?php echo $i+1;?>" id="txtTolerancePer1_<?php echo $i+1;?>" class="form-control" placeholder="Tolerance %"  value="<?php if($num>0){echo $rows['TOLERANCE_PERCENT_SUB'] ;}?>" autocomplete="off" />
                                                    </address>
                                                </div>
                                                
                                                <div class="col-sm-3 invoice-col">
                                                    Tolerance (+) %
                                                    <address>
                                                        <input type="text" name="txtTolerancePer2_<?php echo $i+1;?>" id="txtTolerancePer2_<?php echo $i+1;?>" class="form-control" placeholder="Tolerance %"  value="<?php if($num>0){echo $rows['TOLERANCE_PERCENT_ADD'] ;}?>" autocomplete="off" />
                                                    </address>
                                                </div>
                                             </div>
										</div>
                                    </div>
                                    
                                    
                                    <div class="box">
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Business Entity</th>
                                                        <th>PIC Name</th>
                                                        <th>Email Address <select style="display:none;" name="vendortype<?php echo $i+1;?>" id="vendortype<?php echo $i+1;?>" ><?php $obj->getVendorTypeList();?></select></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tblentity<?php echo $i+1;?>">
                                                <?php
												if($num==0)
												{$enrow=1;?>
                                                <tr id="prl_Row_<?php echo $i+1;?>_1">
                                                <td><a href="#pr<?php echo $i;?>" onClick="removeEntity(<?php echo $i+1;?>,1);" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                <td><select id="selEntity_<?php echo $i+1;?>_1" name="selEntity_<?php echo $i+1;?>_1" class="form-control" ><?php $obj->getVendorTypeList();?></select></td>
                                                <td><input type="text" name="txtEntityName_<?php echo $i+1;?>_1" id="txtEntityName_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                <td><input type="text" name="txtEntityEmail_<?php echo $i+1;?>_1" id="txtEntityEmail_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                </tr>
                                                <?php }else{
												$sql2 = "select * from generate_agency_letter_slave1 where GEN_AGENCY_ID='".$rows['GEN_AGENCY_ID']."'";
												$res2 = mysql_query($sql2);
												$num2 = mysql_num_rows($res2);
                                                $j=0;
												if($num2>0)
												{$enrow=$num2;
													while($rows2 = mysql_fetch_assoc($res2))
													{
													?>
													<tr id="prl_Row_<?php echo $i+1;?>_<?php echo $j+1;?>">
													<td><a href="#pr<?php echo $i;?>" onClick="removeEntity(<?php echo $i+1;?>,<?php echo $j+1;?>);" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td>
													<td><select id="selEntity_<?php echo $i+1;?>_<?php echo $j+1;?>" name="selEntity_<?php echo $i+1;?>_<?php echo $j+1;?>" class="form-control" ><?php $obj->getVendorTypeList();?></select></td>
													<td><input type="text" name="txtEntityName_<?php echo $i+1;?>_<?php echo $j+1;?>" id="txtEntityName_<?php echo $i+1;?>_<?php echo $j+1;?>" class="form-control" placeholder=""  value="<?php echo $rows2['ENTITY_NAME'];?>" autocomplete="off" /></td>
													<td><input type="text" name="txtEntityEmail_<?php echo $i+1;?>_<?php echo $j+1;?>" id="txtEntityEmail_<?php echo $i+1;?>_<?php echo $j+1;?>" class="form-control" placeholder=""  value="<?php echo $rows2['EMAILID'];?>" autocomplete="off" /></td>
												   <script>$("#selEntity_<?php echo $i+1;?>_<?php echo $j+1;?>").val(<?php echo $rows2['ENTITY'];?>);</script>
													</tr>
											 <?php $j++;}
											  }else{$enrow=1;?>
                                                <tr id="prl_Row_<?php echo $i+1;?>_1">
                                                <td><a href="#pr<?php echo $i;?>" onClick="removeEntity(<?php echo $i+1;?>,1);" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                <td><select id="selEntity_<?php echo $i+1;?>_1" name="selEntity_<?php echo $i+1;?>_1" class="form-control" ><?php $obj->getVendorTypeList();?></select></td>
                                                <td><input type="text" name="txtEntityName_<?php echo $i+1;?>_1" id="txtEntityName_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                <td><input type="text" name="txtEntityEmail_<?php echo $i+1;?>_1" id="txtEntityEmail_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                </tr>
                                              
                                            <?php }}?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td><button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="getNewRow(<?php echo $i+1;?>);" >Add</button><input type="hidden" name="rowcount<?php echo $i+1;?>" id="rowcount<?php echo $i+1;?>" value="<?php echo $enrow;?>"/></td>
                                                        <td colspan="3"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
									
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
										 	Ship Owner
											<address>
											<select name="selShipOwner_<?php echo $i+1;?>" id="selShipOwner_<?php echo $i+1;?>" class="form-control">
								  				<?php $obj->getVendorListNewForCOA(11);?>
								  			</select>
											<script> $("#selShipOwner_<?php echo $i+1;?>").val('<?php echo $rows['SHIP_OWNER'];?>'); </script>
											</address>
										</div>
                                     </div>
                                     
                                     <!----------------------------------BUNKER STEMMED--------------------------------->
                                     <div class="row invoice-info">
                                         <div class="col-sm-12 invoice-col">
                                         <h2 class="page-header">
                                               BUNKERS STEMMED - LETTERS TO MASTER AND AGENTS  
                                         </h2>                            
                                        </div>
                                    </div>
                                    <?php 
									
									if($rows['ETA_DATE']!='' && $rows['ETA_DATE']!='0000-00-00 00:00:00')
									{
										$etadate = date('d-m-Y H:i',strtotime($rows['ETA_DATE']));
									}
									else
									{
										$etadate = '';
									}
									?>
                                    <div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											ETA (LT)
											<address>
												<input type="text" name="txtETADATE_<?php echo $i+1;?>" id="txtETADATE_<?php echo $i+1;?>" class="form-control" placeholder="dd-mm-yyyy H:i"  value="<?php if($num>0){echo $etadate ;}?>" autocomplete="off" />
											</address>
										</div>
                                        <div class="col-sm-3 invoice-col">
											Bunker Surveyor (Name)
											<address>
												<input type="text" name="txtBUNKER_SURVEYOR_<?php echo $i+1;?>" id="txtBUNKER_SURVEYOR_<?php echo $i+1;?>" class="form-control" placeholder="Bunker Surveyor (Name)"  value="<?php if($num>0){echo $rows['BUNKER_SURVEYOR'];}?>" autocomplete="off" />
											</address>
										</div>
                                        <div class="col-sm-3 invoice-col">
											Bunker Surveyor (Company and Contact)
											<address>
                                                <textarea class="input-text" name="txtBUNKER_SURVEYORCOM_<?php echo $i+1;?>" id="txtBUNKER_SURVEYORCOM_<?php echo $i+1;?>" cols="50" rows="2" placeholder="Bunker Surveyor (Company and Contact)" ><?php if($num>0){echo $rows['BUNKER_SURVEYORCOM'] ;}?></textarea>
											</address>
										</div>
                                    </div>
                                    <div class="box">
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Bunkering Port<select style="display:none;" name="portlist<?php echo $i+1;?>" id="portlist<?php echo $i+1;?>" ><?php $obj->getPortListNew();?></select></th>
                                                        <th>GRADE</th>
                                                        <th>Supplier</th>
                                                        <th>Physical</th>
                                                        <th>Quantity (MT)</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody id="tblentity1<?php echo $i+1;?>">
                                                <?php
												if($num==0)
												{$enrow1=1;?>
                                                <tr id="prl_Row1_<?php echo $i+1;?>_1">
                                                <td><a href="#pr<?php echo $i;?>" onClick="removeEntity1(<?php echo $i+1;?>,1);" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                <td><select id="selBunkeringPort_<?php echo $i+1;?>_1" name="selBunkeringPort_<?php echo $i+1;?>_1" class="form-control" ><?php $obj->getPortListNew();?></select></td>
                                                <td><input type="text" name="txtGrade_<?php echo $i+1;?>_1" id="txtGrade_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                <td><input type="text" name="txtSupplier_<?php echo $i+1;?>_1" id="txtSupplier_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                <td><input type="text" name="txtPhysical_<?php echo $i+1;?>_1" id="txtPhysical_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                <td><input type="text" name="txtQuantity_<?php echo $i+1;?>_1" id="txtQuantity_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                </tr>
                                                <?php }else{
												$sql2 = "select * from generate_agency_letter_slave2 where GEN_AGENCY_ID='".$rows['GEN_AGENCY_ID']."'";
												$res2 = mysql_query($sql2);
												$num2 = mysql_num_rows($res2);
                                                $j=0;
												if($num2>0)
												{$enrow1=$num2;
													while($rows2 = mysql_fetch_assoc($res2))
													{
													?>
													<tr id="prl_Row1_<?php echo $i+1;?>_<?php echo $j+1;?>">
													<td><a href="#pr<?php echo $i;?>" onClick="removeEntity1(<?php echo $i+1;?>,<?php echo $j+1;?>);" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                    <td><select id="selBunkeringPort_<?php echo $i+1;?>_<?php echo $j+1;?>" name="selBunkeringPort_<?php echo $i+1;?>_<?php echo $j+1;?>" class="form-control" ><?php $obj->getPortListNew();?></select></td>
													<td><input type="text" name="txtGrade_<?php echo $i+1;?>_<?php echo $j+1;?>" id="txtGrade_<?php echo $i+1;?>_<?php echo $j+1;?>" class="form-control" placeholder=""  value="<?php echo $rows2['GRADE'];?>" autocomplete="off" /></td>
													<td><input type="text" name="txtSupplier_<?php echo $i+1;?>_<?php echo $j+1;?>" id="txtSupplier_<?php echo $i+1;?>_<?php echo $j+1;?>" class="form-control" placeholder=""  value="<?php echo $rows2['SUPPLIER'];?>" autocomplete="off" /></td>
													<td><input type="text" name="txtPhysical_<?php echo $i+1;?>_<?php echo $j+1;?>" id="txtPhysical_<?php echo $i+1;?>_<?php echo $j+1;?>" class="form-control" placeholder=""  value="<?php echo $rows2['PHYSICAL'];?>" autocomplete="off" /></td>
                                                    <td><input type="text" name="txtQuantity_<?php echo $i+1;?>_<?php echo $j+1;?>" id="txtQuantity_<?php echo $i+1;?>_<?php echo $j+1;?>" class="form-control" placeholder=""  value="<?php echo $rows2['QUANTITY'];?>" autocomplete="off" /></td>
                                                    
                                                    <script>$("#selBunkeringPort_<?php echo $i+1;?>_<?php echo $j+1;?>").val('<?php echo $rows2['BUNKERPORT'];?>');</script>
												   </tr>
											 <?php $j++;}
											  }else{$enrow1=1;?>
                                                <tr id="prl_Row1_<?php echo $i+1;?>_1">
                                                <td><a href="#pr<?php echo $i;?>" onClick="removeEntity1(<?php echo $i+1;?>,1);" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                <td><select id="selBunkeringPort_<?php echo $i+1;?>_1" name="selBunkeringPort_<?php echo $i+1;?>_1" class="form-control" ><?php $obj->getPortListNew();?></select></td>
                                                <td><input type="text" name="txtGrade_<?php echo $i+1;?>_1" id="txtGrade_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                <td><input type="text" name="txtSupplier_<?php echo $i+1;?>_1" id="txtSupplier_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                <td><input type="text" name="txtPhysical_<?php echo $i+1;?>_1" id="txtPhysical_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                <td><input type="text" name="txtQuantity_<?php echo $i+1;?>_1" id="txtQuantity_<?php echo $i+1;?>_1" class="form-control" placeholder=""  value="" autocomplete="off" /></td>
                                                
                                                </tr>
                                              
                                            <?php }}?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td><button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="getNewRow1(<?php echo $i+1;?>);" >Add</button><input type="hidden" name="rowcount1<?php echo $i+1;?>" id="rowcount1<?php echo $i+1;?>" value="<?php echo $enrow1;?>"/></td>
                                                        <td colspan="5"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    
                            
                                     
									<?php if($rights == 1 && $rows['STATUS'] !=2){ ?>
									<div class="box-footer" align="right">
									<button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="return getSubmit(<?php echo $i+1;?>,1,'<?php echo substr($arr_portval[0],0,2);?>',<?php echo $arr_portval[1];?>,<?php echo $arr_portval[2];?>);" >Submit</button>&nbsp;&nbsp;
                                    <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="return getSubmit(<?php echo $i+1;?>,2,'<?php echo substr($arr_portval[0],0,2);?>',<?php echo $arr_portval[1];?>,<?php echo $arr_portval[2];?>);"  >Submit & Close</button>
                                    <input type="hidden" name="action" id="action" value="submit" />
                                   
									</div>
                                     <div class="row invoice-info"> 
                                        <div class="col-sm-12 invoice-col">
                                          <span style="color:red;"><i>* It is important to Submit and Close this letter here, to ensure that the agent completes his sections later correctly.</i></span>
                                        </div>
                                    </div>
									<?php } ?>
									
									
									
									<div class="box-body no-padding" style="overflow:auto;">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="3%">#</th>
                                                    <th width="10%">Country</th>
                                                    <th width="10%">Port</th>
                                                    <th width="10%">Cargo Details</th>
                                                    <th width="10%">Agent Name</th>
                                                    <th width="10%">Date of Letter</th>
                                                    <th width="10%">User Name</th>
                                                    <th width="10%">Password</th>
                                                    <th width="10%">PDA Request</th>
                                                    <th width="3%">Edit/Cancel</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbl_txtID_<?php echo $i+1;?>">
                                            <?php 
                                            $sql = "select * from generate_agency_letter where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_portval[0]."' and PORTID='".$arr_portval[1]."' and RANDOMID='".$arr_portval[2]."' ";
                                            $res = mysql_query($sql);
                                            $rec = mysql_num_rows($res);
                                            if($rec == 0){
                                            ?>
                                            <tr id="tpRow_<?php echo $i+1;?>"><td valign="top" align="center" colspan="10" style="color:red;">Sorry , currently zero(0) records added.</td></tr>
                                            <?php }else{
                                            $m=1;
                                            while($rows = mysql_fetch_assoc($res))
                                            {
                                            ?>
                                            <tr id="tpRow_<?php echo $i+1;?>_<?php echo $m;?>">
                                            <td align="center"><?php echo $m.".";?></td>
                                            <td align="left"><?php echo $obj->getCountryNameBasedOnID($rows["COUNTRY_ID"]);?></td>
                                            <td align="left"><?php echo $obj->getPortNameBasedOnID($rows['PORTID']);?></td>
                                            <td align="left"><?php echo $rows['CARGO_PACKING_DESC'];?></td>
                                            <td align="left"><?php echo $obj->getVendorListNewBasedOnID($rows['VENDORID']);?></td>
                                            <td align="left"><?php echo date("d-M-Y",strtotime($rows['DATE']));?></td>
                                            <td align="left"><?php echo $rows['USERNAME'];?></td>
                                            <td align="left"><?php echo $rows['PASSWORD'];?></td>
                                            <td align="center"><?php if($arr_portval[0]!="TP"){?><p>
											<a href="allPdf.php?id=2&gen_agency_id=<?php echo $rows['GEN_AGENCY_ID'];?>&port_type=<?php echo $rows['PORT'];?>&comid=<?php echo $comid; ?>&port_name=<?php echo $arr_portval[1];?>&agent_code=<?php echo $arr_portval[4];?>&randomno=<?php echo $arr_portval[2];?>" title="Pdf" ><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i> PDA Request Letter</button></a></p><p>
											<a href="allPdf.php?id=65&gen_agency_id=<?php echo $rows['GEN_AGENCY_ID'];?>&port_type=<?php echo $rows['PORT'];?>&comid=<?php echo $comid; ?>&port_name=<?php echo $arr_portval[1];?>&agent_code=<?php echo $arr_portval[4];?>&randomno=<?php echo $arr_portval[2];?>" title="Pdf" ><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i> Agency Nomination Letter</button></a></p><?php }?><p>
											<a href="allPdf.php?id=51&gen_agency_id=<?php echo $rows['GEN_AGENCY_ID'];?>&port_type=<?php echo $arr_portval[0];?>&port_name=<?php echo $arr_portval[1];?>&agent_code=<?php echo $arr_portval[4]; ?>&randomno=<?php echo $arr_portval[2];?>" title="Pdf" ><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i> Letter to Agents - Bunker Stemmed </button></a></p><p>
											<a href="allPdf.php?id=63&gen_agency_id=<?php echo $rows['GEN_AGENCY_ID'];?>&port_type=<?php echo $arr_portval[0];?>&port_name=<?php echo $arr_portval[1];?>&agent_code=<?php echo $arr_portval[4]; ?>&randomno=<?php echo $arr_portval[2];?>" title="Pdf" ><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i> Letter to Master - Bunker Stemmed </button></a></p></td>
                                            
                                            <td align="center">
                                            
                                            <a href="#?" title="Delete Entry" onClick="getDelete(<?php echo $rows['GEN_AGENCY_ID'];?>,<?php echo $i+1;?>,<?php echo $comid;?>,'<?php echo substr($arr[$i],0,2);?>',<?php echo $arr_portval[1];?>,<?php echo $m;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                            </tr>
                                            <?php $m++;}}?>
                                            </tbody>
                                        </table>
									</div>
									</form>
									</div>
									<?php } ?>
									
									<!-- /.tab_1-pane -->
									
									</div><!-- /.tab-content -->
								</div><!-- nav-tabs-custom -->
							</div>
						</div>
					<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
	<?php $display->footer(); ?>
    </body>
</html>