<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$id = $_REQUEST['id'];
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
{
 	$msg = $obj->updateCargoOpenPositionDetails();
	header('Location:./open_cargo_position.php?msg='.$msg);
}
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
$obj->viewOpenCargoPositionRecords($id);

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rigts    = $obj->getUserRights($uid,$moduleid,2);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Open Cargo Position</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="open_cargo_position.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             UPDATE CARGO OPEN ENTRY
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           Cargo ID
                            <address>
                              <input type="text" name="txtCID" id="txtCID" class="form-control"  placeholder="Cargo ID" autocomplete="off" readonly value="<?php echo $obj->getFun1();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                          Shipper
                            <address>
                                <select  name="selShipper" class="form-control" id="selShipper" >
									<?php 
                                    $obj->getVendorListNewForCOA(15);
                                    ?>
                                    </select>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Material
                            <address>
                               <select  name="selMType" onChange="getMaterialCode();" class="form-control" id="selMType">
								<?php 
								$_REQUEST['selMType'] = $obj->getFun5();
                                $obj->getMaterialType();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Material Code
                            <address>
                               <input type="text"  name="txtMCode"  class="form-control" id="txtMCode" placeholder="Material Code" readonly autocomplete="off" value="<?php echo $obj->getFun6();?>"/>
								
                             </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                          Material Quantity (MT)
                            <address>
                                <input type="text"  name="txtQty"  class="form-control" id="txtQty" placeholder="Material Quantity (MT)" autocomplete="off" value="<?php echo $obj->getFun7();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                         Tolerance (+/- %)
                            <address>
                                <input type="text"  name="txtTolerance"  class="form-control" id="txtTolerance" placeholder="Tolerance (%)" autocomplete="off" value="<?php echo $obj->getFun13();?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                          Freight (USD/MT)
                            <address>
                                <input type="text"  name="txtGrossFreight"  class="form-control" id="txtGrossFreight" placeholder="Freight (USD/MT)" autocomplete="off" value="<?php echo $obj->getFun17();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                         Term Tolerance
                            <address>
                                <input type="text"  name="txtTermTolerance"  class="form-control" id="txtTermTolerance" placeholder="Term Tolerance" autocomplete="off" value="<?php echo $obj->getFun18();?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Load Port
                            <address>
                               <select  name="selLPort" class="form-control" id="selLPort">
								<?php 
                                $obj->getPortListNew();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Discharge Port
                            <address>
                                <select  name="selDPort" class="form-control" id="selDPort">
								<?php 
                                $obj->getPortListNew();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
					</div>
                    
                  	<div class="row invoice-info" >
                        
                        <div class="col-sm-6 invoice-col">
                            LayCan Start Date
                            <address>
                                <input type="text" name="txtFDate" id="txtFDate" class="form-control"  placeholder="LayCan Start Date" autocomplete="off" value="<?php echo date("d-m-Y",strtotime($obj->getFun11()));?>"/>
                             </address>
                        </div><!-- /.col -->
                        
                         <div class="col-sm-6 invoice-col">
                           LayCan Finish Date
                            <address>
                                <input type="text" name="txtTDate" id="txtTDate" class="form-control"  placeholder="LayCan Finish Date" autocomplete="off" value="<?php echo date("d-m-Y",strtotime($obj->getFun12()));?>"/>
                             </address>
                        </div><!-- /.col -->
                        
					</div>
                    
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                          Vessel Category
                            <address>
                               <select  name="selVCType" class="form-control" id="selVCType">
								<?php 
								$_REQUEST['selVCType'] = $obj->getFun15();
                                $obj->getVesselCategoryList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Open Vessel
                            <address>
                                <select  name="selVOPID" class="form-control" id="selVOPID">
								<?php 
								$_REQUEST['selVOPID'] = $obj->getFun14();
                                $obj->getOpenVesselPositionListForUpdate($obj->getFun14());
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
                        
                    </div>
					<?php
					if($rigts == 1)
					{
					?>
					<div class="box-footer" align="right">
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
						<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>
				<?php } ?>	
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#txtQty,#txtLINo,#txtTolerance").numeric();
$('#txtFDate,#txtTDate').datepicker({
    format: 'dd-mm-yyyy'
});
$("#selLPort").val('<?php echo $obj->getFun9();?>');
$("#selDPort").val('<?php echo $obj->getFun10();?>');
$("#selShipper").val('<?php echo $obj->getFun16();?>');
$(".areasize").autosize({append: "\n"});
$("[id^=ui-datepicker-div]").hide();
$("#frm1").validate({
	rules: {
		txtCID:"required",
		selMType:"required",
		txtMCode:"required",
		selLPort:"required",
		selDPort:"required",
		txtFDate:"required",
		txtTDate:"required",
		selVOPID:"required",
		selVCType:"required",
		txtQty : {required:true ,number : true}
		},
	messages: {
		txtCID:"*",
		selMType:"*",
		txtMCode:"*",
		selLPort:"*",
		selDPort:"*",
		txtFDate:"*",
		txtTDate:"*",
		selVOPID:"*",
		selVCType:"*",
		txtQty : {required:"*" ,number : "*"}
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

});


function getMaterialCode()
{
	if($("#selMType").val() != "")
	{
		$("#txtMCode").val($("#selMType").val().split("@@@")[1]);
	}
	else
	{
		$("#txtMCode").val("");
	}
}

function getValidate()
{
	if($("#selLPort").val() == $("#selDPort").val() && $("#selLPort").val()!="" && $("#selDPort").val()!="" )
	{
		jAlert("Sorry, Load port and Discharge Port can not be same.", 'Alert');
		return false;
	}
	else
	{
		return true;
	}
}

</script>
    </body>
</html>