<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page      = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "vessel_in_post_ops.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "vessel_in_history.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "coa_in_ops_at_glance.php"; $page_bar="In Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else if($page == 5){$page_link = "coa_in_post_ops.php"; $page_bar="In Post Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else{$page_link = "coa_in_history.php"; $page_bar="Vessels in History - COA";$nav=16;$subtitle = 'COA';}
if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertPICLUBDecelarationDetails();
	header('Location:./'.$page_link.'?msg='.$msg.'&comid='.$comid.'&page='.$page);
}

$cost_sheet_id = $obj->getLatestCostSheetID($comid);
$arr  = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$cost_sheet_id);
$l_cost_sheet_id = $obj->getCompareEstimateData($comid,"SHEET_NO");
$lp_name = ''; 
$dp_name = '';
$tp_name = '';

for($j=0;$j<count($arr);$j++)
{$arr_explode = explode('-',$arr[$j]);$port_name = $obj->getPortNameBasedOnID($arr_explode[1]);
	if($arr_explode[0] == "LP"){
		$lp_name .= $port_name.",";
	}
	elseif($arr_explode[0] == "DP"){
		$dp_name .= $port_name.",";
	}
	elseif($arr_explode[0] == "TP"){
		$tp_name .= $port_name.",";
	}
}

$lp_name  = substr($lp_name,0,-1);
$dp_name  = substr($dp_name,0,-1);
$tp_name  = substr($tp_name,0,-1);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
$('#txtRemarks').autosize({append: "\n"});
$("#frm1").validate({
	rules: {
	//selPICover: {required: true}
	//txtChartererParty: {required: true},
	//txtDateFrom: {required: true},
	//txtDateTo:{required: true},
	//txtDays:{required: true}
	},
messages: {
	//selPICover: {required: "*"}
	//txtChartererParty: {required: true},
	//txtDateFrom: {required: true},
	//txtDateTo:{required: "*"},
	//txtDays:{required: "*"}
	},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

/*$("#txtDateFrom").datepicker({ 
	format: 'dd-mm-yyyy',
	autoclose: true	 
//}).on('changeDate', function(){  getDateDifference(); });
 });*/

});

function getDateDifference()
{
	var delDays1 = parseFloat(getTimeDiff($("#txtDateTo").val(),$("#txtDateFrom").val()));
	if(isNaN(delDays1)){delDays1 = 0.00;}
	$("#txtDays").val(parseFloat(delDays1).toFixed(2));
}


function getTimeDiff(date2,date1)
{	
	if(date1 != '' &&  date2 != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat(date2);
		start_actual_time    =  getDateWithSpecificFormat(date1);
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000 + parseInt(1);
		return days.toFixed(2);
	}
}


function getString(var1)
{
  var var2 = var1.split(' ');
  var var3 = var2[0].split('-');  
  return var3[2]+'/'+var3[1]+'/'+var3[0]+' 00:00';
}
 
function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' 00:00';
	return currentDate;
}

</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?>&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>
                <?php
				$sql 	= "SELECT * from piclub_declaration where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and SHEET_NO='".$l_cost_sheet_id."'";
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				?>	
                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><?php if($rec >0){?><a href="allPdf.php?id=48&comid=<?php echo $comid; ?>&declid=<?php echo $rows['DECELARATIONID']; ?>&sheetid=<?php echo $l_cost_sheet_id;?>" title="Pdf"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a> <?php }?><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
							
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								Charterers Club Vessel Declaration
								</h2>                            
							</div><!-- /.col -->
						</div>
					
						<div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
							   Vessel
								<address>
										<input type="text" name="txtVesselName" id="txtVesselName" class="form-control" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?>" autocomplete="off" readonly/>
								</address>
							</div><!-- /.col -->
                         </div>
                         <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
							  Year built
								<address>
									<address>
										<input type="text" name="txtYearBuilt" id="txtYearBuilt" class="form-control" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"YEARBUILT");?>" autocomplete="off" readonly/>
								</address>
								</address>
							</div><!-- /.col -->
						</div>
						<div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Flag
								<address>
								   <input type="text" name="txtFlag" id="txtFlag" class="form-control" value="<?php echo $obj->getCountryData($obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"FLAG"),"COUNTRY_NAME");?>" autocomplete="off" readonly/>
								</address>
							</div><!-- /.col -->                     
						</div>
                        <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								GT/NT
								<address>
								   <input type="text" name="txtGT" id="txtGT" class="form-control" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"GRT_NRT");?>" autocomplete="off" readonly/>
								</address>
							</div><!-- /.col -->                     
						</div>
                        
                        
                         <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								DWT	
								<address>
								   <input type="text" name="txtDWT" id="txtDWT" class="form-control" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"DWT");?>" autocomplete="off" readonly/>
								</address>
							</div><!-- /.col -->                     
						</div>
                        
                        <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Classification
								<address>
								   <input type="text" name="txtGT" id="txtGT" class="form-control" value="<?php echo $obj->getClaSocNameOnId($obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"CLA_SOC_ID"));?>" autocomplete="off" readonly placeholder="Classification"/>
								</address>
							</div><!-- /.col -->                     
						</div>
                        
                        
                        <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Owners P&I Cover
								<address>
								    <input type="text" name="txtPI" id="txtPI" class="form-control" value="<?php echo $obj->getVendorOnlyNameBasedOnID($obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"P_I"));?>" autocomplete="off" readonly placeholder="Owners P&I Cover"/>
								</address>
							</div><!-- /.col -->                     
						</div>
                        
                        <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Charter party
								<address>
								   <input type="text" name="txtChartererParty" id="txtChartererParty" class="form-control" value="<?php echo $rows['CHARTERER_PARTY'];?>" autocomplete="off" placeholder="Charter party"/>
								</address>
							</div><!-- /.col -->                     
						</div>
                        
						<div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Date of Delivery
								<address>
                                
								   <input type="text" name="txtDateFrom" id="txtDateFrom" class="form-control" value="<?php echo $rows['DATE_FROM'];?>" autocomplete="off" placeholder="Date of Delivery"/>
								</address>
							</div><!-- /.col -->
                           
						</div>
						
						
                        <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Estimated Duration
								<address>
								   <input type="text" name="txtDays" id="txtDays" class="form-control" value="<?php echo $rows['DAYS'];?>" autocomplete="off" placeholder="Estimated Duration"/>
								</address>
							</div><!-- /.col -->
						</div>
                        
                        
                        <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Voyage
								<address>
								   <input type="text" name="txtVoyage" id="txtVoyage" class="form-control" value="<?php echo "Load Port: ".$lp_name.", Discharge Port: ".$dp_name.", Transit Port: ".$tp_name;?> " autocomplete="off" placeholder="Voyage" readonly/>
								</address>
							</div><!-- /.col -->
						</div>
                        
                        <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Cargo
								<address>
								   <input type="text" name="txtCargo" id="txtCargo" class="form-control" value="<?php echo $obj->getMaterialDescBasedOnMaterialCode($obj->getCargoIDBasedOnMaterialCode($obj->getCompareEstimateData($comid,"MATERIALID")));?>" autocomplete="off" placeholder="Cargo" readonly/>
								</address>
							</div><!-- /.col -->
						</div>
						
                        <div class="form-group">
							<label for="txtVesselGroup">Remarks</label>
							<textarea name="txtRemarks" id="txtRemarks" class="form-control" cols="42" rows="4"><?php echo $rows['REMARKS'];?></textarea>
						</div>
						
						<?php if($rights == 1){ ?>
						<div class="box-footer" align="right">
                        <input type="hidden" name="txtSheet_no" id="txtSheet_no" class="form-control" value="<?php echo $l_cost_sheet_id;?>"/>
                        <input type="hidden" name="action" id="action" class="form-control" value="submit"/>
							<button type="submit" class="btn btn-primary btn-flat">Submit</button>							
						</div>
						<?php } ?>				
					</form>
					
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>