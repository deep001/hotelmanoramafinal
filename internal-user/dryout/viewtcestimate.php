<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];
$ids = $_REQUEST['ids'];
if($_REQUEST['rttype']==''){$backButton = "estimate_tc_list.php";}if($_REQUEST['rttype'] == 1){$backButton = "in_ops_tc.php";}else if($_REQUEST['rttype'] == 2){$backButton = "vessel_in_post_tc.php";}else if($_REQUEST['rttype']==3){$backButton = "vessel_in_history_tc.php";}else if($_REQUEST['rttype']==4){$backButton = "decisionchart_tc_list.php";}else{$backButton = "finalisedvoyagefixturestc.php";}
//if($_REQUEST['rttype']==''){$backButton = "estimate_tc_list.php";}else if($_REQUEST['rttype']==1){$backButton = "in_ops_tc.php";}else if($_REQUEST['rttype']==2){$backButton = "decisionchart_tc_list.php";}else{$backButton = "finalisedvoyagefixturestc.php";}


$obj->viewTCEstimationTempleteRecordsNew($id);
$pagename = basename($_SERVER['PHP_SELF'])."?&id=".$id."&ids=".$ids;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
table {border-collapse:separate;}
select{height:19px;}
.select {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-weight: bold;
    color: #333333;
    /* color: #FBC763; */
    text-decoration: none;
    line-height: 13px;
	height:13px;
}

.input-text {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #333333;
	text-decoration: none;
	/*text-transform:uppercase;
	line-height: 15px;*/
}

.input-textsmall {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-weight: normal;
	color: #333333;
	text-decoration:none;
	text-transform:uppercase;
}


.input 
{
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 10px;
font-weight: normal;
color: #333333;
text-decoration: none;
}
</style>
</head>
	<div id="basic-modal-content" style="display:none;" align="center">
	<table cellpadding="1" cellspacing="4" border="0" align="center">
	<tr><td align="center"><img src="../../img/loading.gif" /><br /></td></tr>
	</table>
	</div>
	
    <body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onClick="getFinalCalculation();">Calculate</a>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(21); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-leaf"></i>&nbsp;TC Estimates&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">TC Estimates</li>
                    </ol>
                </section>
                
                <div align="right" style="margin-top:5px;margin-bottom:5px;margin-right:5px;"><a href="<?php echo $backButton;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                	<table width="100%" style="padding:0px 5px 0px 5px;">
                       <tr class="input-text">
                           <td width="50px;"><strong>Fixture&nbsp;Type&nbsp;:</strong></td>
                           <td>&nbsp;TC&nbsp;Out&nbsp;<input name="selFType" type="hidden" id="selFType" value="1"></td>
                           <td>
                           <strong>Vessel&nbsp;:</strong></td>
						   <td><select name="selVName" id="selVName" class="input-text" style="width:110px;color:#175c84;background-color:#EAEAEA;" onChange="getData();" disabled="disabled">
                           <?php
                                $obj->getVesselTypeListMemberWise($_SESSION['selBType']);
                                ?>
                           </select>
                           </td>
                           <td><strong>Vessel&nbsp;Type&nbsp;:</strong></td>
                           <td><input type="text" name="txtVType" id="txtVType" style="width:110px;color:#175c84;background-color:#EAEAEA;" class="input-text" readonly value="<?php echo $obj->getFun4();?>" />
                            <select  name="selBunker" style="display:none;" id="selBunker">
                                <?php $obj->getBunkerList(); ?>
                            </select>
                            
                            <select name="selExpenseType" id="selExpenseType" style="display:none;">
                            <?php $obj->getExpenseTypeList();?>
                            </select>
                           </td>
                           <td><strong>Flag&nbsp;:</strong></td>
                           <td><input type="text" name="txtFlag" id="txtFlag" style="width:110px;color:#175c84;background-color:#EAEAEA;" class="input-text" readonly value="<?php echo $obj->getFun5();?>" /></td>
                            <td><strong>Date&nbsp;:</strong></td>
                            <td><input type="text" name="txtDate" id="txtDate" style="width:110px;color:#175c84;background-color:#EAEAEA;" class="input-text" value="<?php echo date("d-m-Y",strtotime($obj->getFun6()));?>" placeholder"dd-mm-yy" readonly="true" /></td>
                            <td ><strong>TC&nbsp;No.&nbsp;:</strong></td>
                            <td><input type="text" name="txtTCNo" id="txtTCNo" style="width:110px;color:#175c84;background-color:#EAEAEA;" class="input-text" value="<?php echo $obj->getFun7();?>" readonly/></td>
                       </tr>
                       <tr class="input-text">
                          <td >CP&nbsp;Date&nbsp;:</td>
                          <td><input autocomplete="off" type="text" name="txtCPdate2" id="txtCPdate2" class="input-text" value="<?php echo date("d-m-Y", strtotime($obj->getFun9())); ?>" style="width:110px;background-color:#EAEAEA;" placeholder="dd-mm-yy" readonly /></td>
                          <td >CP&nbsp;Type&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                          <td><input autocomplete="off" type="text" name="txtCPType" id="txtCPType" class="input-text" style="width:110px; background-color:#EAEAEA;" readonly value="<?php echo $obj->getContractTypeMasterData($obj->getFun10(),'CONTRACT_TYPE');?>"/></td>
                          
                        <td ><span id="tripspan_2">Trip TC&nbsp;:</span><input name="selFType" type="hidden" id="selFType" value="1"></td>
                           <td><span id="tripspan_3"><input autocomplete="off" type="text" name="txtTripTC1" id="txtTripTC1" class="input-text" style="width:110px;background-color:#EAEAEA;" placeholder="Trip TC" value="<?php echo $obj->getFun74(); ?>" readonly/></span></td>
                           <td><span id="periodspan_2">Period&nbsp;:</span></td>
                           <td><span id="periodspan_3"><input autocomplete="off" type="text" name="txtPeriod1" id="txtPeriod1" style="width:110px;background-color:#EAEAEA;" class="input-text" placeholder="Period" value="<?php echo $obj->getFun75(); ?>" readonly/></span></td>
                           <td><span id="noOfTripspan_2">No. Of Trips&nbsp;:</span></td>
                           <td><span id="noOfTripspan_3"><input autocomplete="off" type="text" name="txtNoOfTrips1" id="txtNoOfTrips1" class="input-text" style="width:110px;background-color:#EAEAEA;" placeholder="No. Of Trips" value="<?php echo $obj->getFun76(); ?>" readonly/></span></td>
                           <td >Charterers&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                          <td><input autocomplete="off" type="text" name="txtCharterers" id="txtCharterers" class="input-text" style="width:200px;background-color:#EAEAEA;" readonly value="<?php echo $obj->getVendorListNewData($obj->getFun11(),'NAME');?>"/></td>
                       </tr>
                       
                   </table>
				   <hr style="margin-bottom:2px; margin-top:8px; color:#000;">
	<!---------------------------------------------------------------------------------------------------------------->
	            <div><strong style="font-size:14px;">&nbsp;&nbsp;Estimates</strong></div>
         			
	<!---------------START--------------------------Tab_1->TC Estimate------------------------------------------------>	
				
	                   <?php
						$sql2 = "select * from chartering_tc_estimate_slave1 where TCOUTID = '".$id."'";
						$res2 = mysql_query($sql2) or die($sql2);
						$num  = mysql_num_rows($res2);
						$rows = mysql_fetch_assoc($res2);
						
				        ?>
                           <table width="100%" style="margin-bottom:5px; padding:0px 5px 0px 5px;">
                               <tr class="input-text">
                                  <td >Del Date/Time&nbsp;<input type="hidden" name="txtCOUTSlaveRandomID" id="txtCOUTSlaveRandomID" value="<?php echo $rows['RANDOMID'];?>"/></td>
                                  <td><input autocomplete="off" type="text" name="txtDeldate" id="txtDeldate" class="input-text" value="<?php if($num == 0){ echo date("d-m-Y H:i", strtotime($obj->getFun46()));} else { echo date("d-m-Y H:i", strtotime($rows['DEL_DATE_EST'])); }?>" style="width:100px;" placeholder="dd-mm-yy hh:mm" /></td>
                                  <td >Re Del Date/Time&nbsp;</td>
                                  <td><input autocomplete="off" type="text" name="txtReDeldate" id="txtReDeldate" class="input-text" value="<?php if($num==0){ echo date("d-m-Y H:i", strtotime($obj->getFun47()));} else{echo date("d-m-Y H:i", strtotime($rows['REDEL_DATE_EST']));} ?>" style="width:100px;" placeholder="dd-mm-yy hh:mm" /></td>
                                  <td >TC Days&nbsp;</td>
                                  <td><input autocomplete="off" type="text" name="txtTCdays" id="txtTCdays" class="input-text" placeholder="TC Days" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo $rows['TC_DAYS_EST'];?>"/></td>
                                  <td></td><td></td><td></td><td></td><td></td><td></td>
                               </tr>
                               <tr class="input-text">
                                  <td colspan="6">
                                     <table width="100%">
                                         <tr class="input-text">
                                            <td colspan="5"><strong>Delivery</strong></td>
                                         </tr>
                                         <tr class="input-text">
                                           <td>#</td>
                                           <td>Bunker Grade</td>
                                           <td>Qty(MT)</td>
                                           <td>Bunker Date</td>
                                           <td>Price USD/MT)</td>
                                           <td>Amount(USD)</td>
                                         </tr>
                                         <tbody id="bunkerdelivery">
                                        <?php 
										 if($num == 0){
											 $sql3 = "select * from chartering_estimate_tc_slave4 where TCOUTID='".$id."' and IDENTITY='DEL'";
											 $result3 = mysql_query($sql3);
											 $num3 = mysql_num_rows($result3);
										 }else{
										     $sql3 = "select * from chartering_estimate_tc_slave5 where TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."' and IDENTITY='DEL'";
											 $result3 = mysql_query($sql3);
											 $num3 = mysql_num_rows($result3);
										 }
										 $k = 0;
                                         if($num3==0){$k++;?>
                                            <tr class="input-text" id="row_del_1">
                                               <td></td>
                                               <td><select name="selDelBunker_1" class="input-text" style=" width:90px;" id="selDelBunker_1"></select></td>
                                               <td><input type="text" name="txtDelQty_1" id="txtDelQty_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelBunDate_1" id="txtDelBunDate_1" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelPrice_1" id="txtDelPrice_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelAmount_1" id="txtDelAmount_1" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                            </tr>
                                            <script>$("#selDelBunker_1").html($("#selBunker").html());</script>
                                       <?php }else{
                                           while($rows3 = mysql_fetch_assoc($result3))
                                           {$k++;
                                           if($rows3['BUNKER_DATE']=="" || $rows3['BUNKER_DATE']=="0000-00-00"  || $rows3['BUNKER_DATE']=="1970-01-01"){$txtDelBunDate = "";}else{$txtDelBunDate = date('d-m-Y',strtotime($rows3['BUNKER_DATE']));}
                                           ?>
                                             <tr class="input-text" id="row_del_<?php echo $k;?>">
                                               <td></td>
                                               <td><select name="selDelBunker_<?php echo $k;?>" class="input-text" style=" width:90px;" id="selDelBunker_<?php echo $k;?>"></select></td>
                                               <td><input type="text" name="txtDelQty_<?php echo $k;?>" id="txtDelQty_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['QTY'];?>" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelBunDate_<?php echo $k;?>" id="txtDelBunDate_<?php echo $k;?>" class="input-text" style="width:85px;" value="<?php echo $txtDelBunDate;?>" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelPrice_<?php echo $k;?>" id="txtDelPrice_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['PRICE'];?>" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelAmount_<?php echo $k;?>" id="txtDelAmount_<?php echo $k;?>" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="<?php echo $rows3['AMOUNT'];?>" placeholder="0.00" autocomplete="off"/></td>
                                            </tr>
                                           <script>$("#selDelBunker_<?php echo $k;?>").html($("#selBunker").html());$("#selDelBunker_<?php echo $k;?>").val(<?php echo $rows3['BUNKERID'];?>)</script>
                                       <?php }}?>
                                        </tbody>
                                        <tbody>		
                                        <tr class="input-text">
                                            <td colspan="2"><input type="hidden" name="txtDELID" id="txtDELID" value="<?php echo $k;?>"/></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ><input type="text" name="txtTotalDelAmount" id="txtTotalDelAmount" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                            
                                        </tr>
                                     </table>
                                  
                                  </td>
                                  <td colspan="6">
                                     <table width="100%">
                                        <tr class="input-text">
                                            <td colspan="5"><strong>Re-Delivery</strong></td>
                                        </tr>
                                        <tr class="input-text">
                                           <td>#</td>
                                           <td>Bunker Grade</td>
                                           <td>Qty(MT)</td>
                                           <td>Bunker Date</td>
                                           <td>Price USD/MT)</td>
                                           <td>Amount(USD)</td>
                                        </tr>
                                        <tbody id="bunkerRedelivery">
                                        <?php 
										 
										 if($num == 0){
											 $sql3 = "select * from chartering_estimate_tc_slave4 where TCOUTID='".$id."' and IDENTITY='REDEL'";
											 $result3 = mysql_query($sql3);
											 $num3 = mysql_num_rows($result3);
										 }else{
										     $sql3 = "select * from chartering_estimate_tc_slave5 where TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."' and IDENTITY='REDEL'";
											 $result3 = mysql_query($sql3);
											 $num3 = mysql_num_rows($result3);
										 }
										 $k = 0;
                                         if($num3==0){$k++;?>
                                            <tr class="input-text" id="row_Redel_1">
                                               <td></td>
                                               <td><select name="selReDelBunker_1" class="input-text" style=" width:90px;" id="selReDelBunker_1"></select></td>
                                               <td><input type="text" name="txtReDelQty_1" id="txtReDelQty_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelBunDate_1" id="txtReDelBunDate_1" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelPrice_1" id="txtReDelPrice_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelAmount_1" id="txtReDelAmount_1" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                            </tr>
                                            <script>$("#selReDelBunker_1").html($("#selBunker").html());</script>
                                            <?php }else{
                                           while($rows3 = mysql_fetch_assoc($result3))
                                           {$k++;
                                           if($rows3['BUNKER_DATE']=="" || $rows3['BUNKER_DATE']=="0000-00-00"  || $rows3['BUNKER_DATE']=="1970-01-01"){$txtReDelBunDate = "";}else{$txtReDelBunDate = date('d-m-Y',strtotime($rows3['BUNKER_DATE']));}
                                           ?>
                                             <tr class="input-text" id="row_Redel_<?php echo $k;?>">
                                               <td></td>
                                               <td><select name="selReDelBunker_<?php echo $k;?>" class="input-text" style=" width:90px;" id="selReDelBunker_<?php echo $k;?>"></select></td>
                                               <td><input type="text" name="txtReDelQty_<?php echo $k;?>" id="txtReDelQty_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['QTY'];?>" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelBunDate_<?php echo $k;?>" id="txtReDelBunDate_<?php echo $k;?>" class="input-text" style="width:85px;" value="<?php echo $txtReDelBunDate;?>" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelPrice_<?php echo $k;?>" id="txtReDelPrice_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['PRICE'];?>" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelAmount_<?php echo $k;?>" id="txtReDelAmount_<?php echo $k;?>" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="<?php echo $rows3['AMOUNT'];?>" placeholder="0.00" autocomplete="off"/></td>
                                            </tr>
                                           <script>$("#selReDelBunker_<?php echo $k;?>").html($("#selBunker").html());$("#selReDelBunker_<?php echo $k;?>").val(<?php echo $rows3['BUNKERID'];?>)</script>
                                       <?php }}?>
                                        </tbody>
                                        <tbody>		
                                        <tr class="input-text">
                                            <td colspan="2"><input type="hidden" name="txtREDELID" id="txtREDELID" value="<?php echo $k;?>"/></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ><input type="text" name="txtTotalReDelAmount" id="txtTotalReDelAmount" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                        </tr>
                                        </tbody>
                                     </table>
                                  
                                  </td>
                               </tr>
                               
                               <tr class="input-text">
                                <td colspan="12">&nbsp;</td>
                               </tr>
                                <tr class="input-text">
                                   <td></th>
                                   <td>Off Hire Reason</td>
                                   <td>Off Hire From</td>
                                   <td>Off Hire To</td>
                                   <td>Off Hire Days</td>
                                   <td>Off Hire Rate/Day(USD)</td>
                                   <td>Off Hire(USD)</td>
                                   <td></td>
                                   <td >Utilisation Days&nbsp;</td>
                                   <td><input autocomplete="off" type="text" name="txtUtilisationDays" id="txtUtilisationDays" class="input-text" placeholder="Utilisation Days" style="width:90px;background-color:#EAEAEA;" readonly value="<?php echo $rows['UTILISATION_DAY_EST'];?>"></td>
                                   <td >HFO/MGO Diff.(USD)&nbsp;</td>
                                   <td><input autocomplete="off" type="text" name="txtBunkerDifference" id="txtBunkerDifference" class="input-text" placeholder="0.00" style="width:90px;background-color:#EAEAEA;" readonly value="<?php echo $rows['BUNKER_DIFF_AMT'];?>"></td>
                                   <td ></td>
                                </tr>
                                <tbody id="offhirebody">
								 <?php $sql3 = "select * from chartering_estimate_tc_slave3 where TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."'";
                                 $result3 = mysql_query($sql3);
                                 $num3 = mysql_num_rows($result3);$k = 0;
                                 if($num3==0){$k++;?>
                                    <tr class="input-text" id="row_offhire_<?php echo $k;?>">
                                        <td></td>
                                        <td valign="top"><textarea class="input-text areasize" name="txtOffHireReason_<?php echo $k;?>" style="width:120px;" id="txtOffHireReason_<?php echo $k;?>" rows="3" placeholder="Off Hire Reason..."><?php echo $rows2['OFFHIRE_REASON'];?></textarea></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireFrom_<?php echo $k;?>" id="txtOffHireFrom_<?php echo $k;?>" class="input-text" value="" style="width:100px;" placeholder="dd-mm-yyyy HH:MM" /></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireTo_<?php echo $k;?>" id="txtOffHireTo_<?php echo $k;?>" class="input-text" value="" style="width:100px;" placeholder="dd-mm-yyyy HH:MM" /></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireDays_<?php echo $k;?>" id="txtOffHireDays_<?php echo $k;?>" class="input-text" placeholder="Off Hire Days" style="width:90px;background-color:#EAEAEA;" readonly value="" ></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireRate_<?php echo $k;?>" id="txtOffHireRate_<?php echo $k;?>" class="input-text" placeholder="Rate/Day(USD)" style="width:100px;" onKeyUp="getFinalCalculation();" value="" ></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireAmt_<?php echo $k;?>" id="txtOffHireAmt_<?php echo $k;?>" class="input-text" placeholder="Off Hire(USD)" style="width:90px;background-color:#EAEAEA;" readonly value="" ></td>
                                        <td></td><td></td><td></td><td></td><td></td>
                                    </tr>
                                <?php }else{
                                   while($rows3 = mysql_fetch_assoc($result3))
                                   {$k++;?>
                                     <tr class="input-text" id="row_offhire_<?php echo $k;?>">
                                        <td></td>
                                        <td valign="top"><textarea class="input-text areasize" name="txtOffHireReason_<?php echo $k;?>" style="width:120px;" id="txtOffHireReason_<?php echo $k;?>" rows="2" placeholder="Off Hire Reason..."><?php echo $rows3['OFF_REASON'];?></textarea></td>
                                        <?php if($rows3['OFF_FROM']=='0000-00-00 00:00:00' || $rows3['OFF_FROM']=='1970-01-01 08:00:00'){$offfrom = '';}else{$offfrom = date('d-m-Y H:i',strtotime($rows3['OFF_FROM']));}?>
                                        <?php if($rows3['OFF_TO']=='0000-00-00 00:00:00' || $rows3['OFF_TO']=='1970-01-01 08:00:00'){$offto = '';}else{$offto = date('d-m-Y H:i',strtotime($rows3['OFF_TO']));}?>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireFrom_<?php echo $k;?>" id="txtOffHireFrom_<?php echo $k;?>" class="input-text" value="<?php echo $offfrom;?>" style="width:100px;" placeholder="dd-mm-yyyy HH:MM" /></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireTo_<?php echo $k;?>" id="txtOffHireTo_<?php echo $k;?>" class="input-text" value="<?php echo $offto;?>" style="width:100px;" placeholder="dd-mm-yyyy HH:MM" /></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireDays_<?php echo $k;?>" id="txtOffHireDays_<?php echo $k;?>" class="input-text" placeholder="Off Hire Days" style="width:90px;background-color:#EAEAEA;" readonly value="<?php echo $rows3['OFF_DAYS'];?>" ></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireRate_<?php echo $k;?>" id="txtOffHireRate_<?php echo $k;?>" class="input-text" placeholder="Rate/Day(USD)" style="width:100px;" onKeyUp="getFinalCalculation();" value="<?php echo $rows3['HIRE_RATE'];?>" ></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireAmt_<?php echo $k;?>" id="txtOffHireAmt_<?php echo $k;?>" class="input-text" placeholder="Off Hire(USD)" style="width:90px;background-color:#EAEAEA;" readonly value="<?php echo $rows3['OFF_HIRE'];?>" ></td>
                                        <td></td><td></td><td></td><td></td><td></td>
                                    </tr>
                                <?php }}?>
                                </tbody>
                                <tbody>		
                                <tr class="input-text">
                                    <td ><input type="hidden" name="txtOFFID" id="txtOFFID" value="<?php echo $k;?>"/></td>
                                    <td colspan="11"></td>
                                    
                                </tr>
                             </table>
						  <hr style="margin-bottom:4px; margin-top:8px; color:#000;">
		<!----------------------/////////------------LEFT SIDE PART------------/////////------------------------------>	
					   <table width="100%" style="margin-bottom:5px; padding:0px 5px 0px 5px;">
                       <tr class="input-text">
                           <td width="50%"><strong>Revenue Calculations - USD</strong></td>
                           <td width="50%"><strong>Expense Calculations - USD</strong></td>
                       </tr>
                       <tr>
                           <td width="50%" style="padding-top:4px; vertical-align:top;">
                             <table width="100%">
							    <tbody>
                                <tr class="input-text">
                                	<td width="30%">Hire PDPR Currency</td>
                                    <td width="25%"></td>
									<td width="25%"><select  name="selExchangeCurrency" id="selExchangeCurrency" class="input-text" style="width:120px;background-color:#EAEAEA;" disabled="disabled"><?php $obj->getCurrencyList();?></select><input type="hidden" name="txtExchangeCurrency" id="txtExchangeCurrency" value="<?php echo $obj->getFun137(); ?>"/></td>
									<script>$("#selExchangeCurrency").val('<?php echo $obj->getFun137(); ?>');</script>
								</tr>
                                <tr class="input-text">
                                	<td width="30%">Exchange Rate To USD</td>
									<td width="25%"></td>
									<td width="25%"><input type="text" name="txtExchangeRate" id="txtExchangeRate" class="input-text numeric" style="width:120px;background-color:#EAEAEA;" value="<?php echo $obj->getFun138(); ?>" placeholder="Exchange Rate To USD" autocomplete="off" readonly /></td>
									
                                </tr>
                                <tr class="input-text">
                                	<td width="30%">Hire Fixed Period PDPR <span id="currencyspan"></span></td>
									<td width="25%"></td>
									<td width="25%"><input autocomplete="off" type="text" name="txtDailyGrossHireUSD" id="txtDailyGrossHireUSD" class="input-text" style="width:120px;background-color:#EAEAEA;" value="<?php echo $obj->getFun54();?>" readonly/>
									</td>
                                </tr>
                                <tr class="input-text">
                                	<td width="30%">Hire Fixed Period PDPR (USD)</td>
									<td width="25%"></td>
									<td width="25%"><input autocomplete="off" type="text" name="txtDailyGrossHireUSD1" id="txtDailyGrossHireUSD1" class="input-text" style="width:120px;background-color:#EAEAEA;" value="<?php echo number_format(($obj->getFun54()*$obj->getFun138()),2,'.','');?>" readonly/>
									</td>
                                </tr>
                                <tr class="input-text">
                                	<td >Add Comm(%)</td>
									
									<td><input autocomplete="off" type="text" name="txtAddCommPerct" id="txtAddCommPerct" class="input-text" style="width:120px;background-color:#EAEAEA;" value="<?php if($num==0){ echo $obj->getFun68();}else{echo $rows['ADD_COMM_EST'];} ?>" readonly/></td>
									<td><input autocomplete="off" type="text" name="txtAddComm1" id="txtAddComm1" class="input-text" style="width:120px;background-color:#EAEAEA;" readonly value="<?php echo $rows['ADD_COMM_CAL_EST']; ?>"/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Broker's Comm.(%)</td>
									
									<td><input autocomplete="off" type="text" name="txtChartererAcc" id="txtChartererAcc" class="input-text" style="width:120px;background-color:#EAEAEA;" value="<?php if($num==0){echo $obj->getFun69();} else{ echo $rows['BROKER_COMM_EST'];}?>" readonly/></td>
									<td><input autocomplete="off" type="text" name="txtBrokerComm" id="txtBrokerComm" class="input-text" style="width:120px;background-color:#EAEAEA;" readonly value="<?php echo $rows['BROKER_COMM_CAL_EST']; ?>"/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Nett Hire(USD/day)</td>
									
									<td></td>
									<td><input autocomplete="off" type="text" name="txtNettHire" id="txtNettHire" class="input-text" style="width:120px;background-color:#EAEAEA;" readonly value="<?php echo $rows['NETT_HIRE_EST']; ?>" ></td>
                                </tr>
								<tr class="input-text">
                                	<td >Nett Rev(USD)</td>
									
									<td></td>
									<td><input autocomplete="off" type="text" name="txtNettRev" id="txtNettRev" class="input-text" style="width:120px;background-color:#EAEAEA;" readonly value="<?php echo $rows['NETT_REV_EST']; ?>" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Less Off hire</td>
									
									<td></td>
									<td><input autocomplete="off" type="text" name="txtLessOffHire" id="txtLessOffHire" class="input-text" style="width:120px;background-color:#EAEAEA;" readonly value="<?php echo $rows['LESS_OFF_HIRE_EST']; ?>" ></td>
                                </tr>
								<tr class="input-text">
                                	<td >CVE(USD/Month)</td>
									
									<td><input autocomplete="off" type="text" name="txtCVEM" id="txtCVEM" class="input-text" style="width:120px;background-color:#EAEAEA;" readonly value="<?php if($num==0){echo $obj->getFun63();} else{ echo $rows['CVE_MONTH'];}?>"/></td>
									<td><input autocomplete="off" type="text" name="txtCVE" id="txtCVE" class="input-text" style="width:120px;background-color:#EAEAEA;" readonly value="<?php echo $rows['CVE_EST']; ?>"/></td>
                                </tr>
                                <tr class="input-text">
                                    <td ><strong>Nett Hire to invoice(USD)</strong></td>
                                  
									<td></td>
                                    <td><input autocomplete="off" type="text" name="txtNETHire" id="txtNETHire" class="input-text" style="width:120px;background-color:#EAEAEA;" readonly value="<?php if($num==0){echo 0;} else{ echo $rows['NET_HIRE_AMT'];}?>"/></td>
                                    
                                </tr>
								<tr class="input-text">
                                	<td ><strong>Other Income</strong></td>
									
									<td></td>
									<td></td>
								</tr>
							</tbody>		
							<tbody id="otherIncomebody">
							 <?php $sql3 = "select * from chartering_estimate_tc_slave2 where STATUS=1 and TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."'";
							 $result3 = mysql_query($sql3);
							 $num3 = mysql_num_rows($result3);$k = 0;
							 if($num3==0){$k++;?>
							    <tr class="input-text" id="row_otherincome_1">
								    <td><a href="#tb1" onClick="removeOtherIncome(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                	<td><input autocomplete="off" type="text" name="txtOtherIncomeText_1" id="txtOtherIncomeText_1" class="input-text" style="width:150px;" placeholder="Description" value="" /></td>
									<td><input autocomplete="off" type="text" name="txtOtherIncome_1" id="txtOtherIncome_1" class="input-text" style="width:120px;" onKeyUp="getFinalCalculation();" placeholder="Other Income(USD)" value="" /></td>
								</tr>
							<?php }else{
							   while($rows3 = mysql_fetch_assoc($result3))
							   {$k++;?>
							   <tr class="input-text" id="row_otherincome_<?php echo $k;?>">
							        <td><a href="#tb1" onClick="removeOtherIncome(<?php echo $k;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                	<td ><input autocomplete="off" type="text" name="txtOtherIncomeText_<?php echo $k;?>" id="txtOtherIncomeText_<?php echo $k;?>" class="input-text" style="width:150px;" placeholder="Description" value="<?php echo $rows3['DESCRIPTION']; ?>" /></td>
									<td><input autocomplete="off" type="text" name="txtOtherIncome_<?php echo $k;?>" id="txtOtherIncome_<?php echo $k;?>" class="input-text" style="width:120px;" onKeyUp="getFinalCalculation();" placeholder="Other Income(USD)" value="<?php echo $rows3['OTHER_AMT']; ?>" /></td>
								</tr>
							<?php }}?>
							</tbody>
							<tbody>		
								<tr class="input-text">
                                	<td ><button type="button" onClick="addOtherIncomeRow();">Add</button><input type="hidden" name="hddnOtherID" id="hddnOtherID" value="<?php echo $k;?>"/></td>
									
									<td></td>
								</tr>
								<tr class="input-text">
                                	<td><strong>Total Rev</strong></td>
								
									<td></td>
									<td><input autocomplete="off" type="text" name="txtTotalRev" id="txtTotalRev" class="input-text" style="width:120px;background-color:#EAEAEA;" readonly value="<?php echo $rows['TOTAL_REV_EST']; ?>" /></td>
                                </tr>
							</tbody>
                             </table>
                           </td>
						   
	<!---------------------------------------------RIGHT SIDE PART----------------------------------------------------->					   
                           <td width="50%" style="padding-top:4px; vertical-align:top;">
						        <table width="100%">
								
                                <tr class="input-text">
                                	<td colspan="5"><strong>Other Expense</strong></td>
								</tr>
                                <tr class="input-text">
                                	<td></td>
									<td>Expense Type</td>
                                    <td>Expense Descp.</td>
									<td>Add to TTL</td>
                                    <td>Expense Amt</td>
								</tr>
                               
								<tbody id="otherExpensebody">
								 <?php $sql3 = "select * from chartering_estimate_tc_slave2 where STATUS=2 and TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."'";
								 $result3 = mysql_query($sql3);
								 $num3 = mysql_num_rows($result3);$k = 0;
								 if($num3==0){$k++;?>
									<tr class="input-text" id="row_otherExpense_1">
										<td></td>
										<td ><select name="selExpenseType_1" id="selExpenseType_1" class="input-text" style="width:110px;"></select></td>
                                        <td><input autocomplete="off" type="text" name="txtExpenseDesc_1" id="txtExpenseDesc_1" class="input-text" style="width:110px;" placeholder="Expense Descp." value="" /></td>
										<td><input name="ChkAddToTTL_1" class="checkbox" id="ChkAddToTTL_1" type="checkbox" value="1"/><input type="hidden" name="chkVal_1" id="chkVal_1" value="" /></td>
                                        <td><input autocomplete="off" type="text" name="txtOtherExpense_1" id="txtOtherExpense_1" class="input-text" style="width:110px;" onKeyUp="getFinalCalculation();" placeholder="Other Income(USD)" value="" /></td>
									</tr>
                                    <script>$("#selExpenseType_1").html($("#selExpenseType").html());</script>
								<?php }else{
								   while($rows3 = mysql_fetch_assoc($result3))
								   {$k++;?>
								   <tr class="input-text" id="row_otherExpense_<?php echo $k;?>">
										<td></td>
										<td ><select name="selExpenseType_<?php echo $k;?>" id="selExpenseType_<?php echo $k;?>" class="input-text" style="width:110px;"></select></td>
                                        <td><input autocomplete="off" type="text" name="txtExpenseDesc_<?php echo $k;?>" id="txtExpenseDesc_<?php echo $k;?>" class="input-text" style="width:110px;" placeholder="Expense Descp." value="<?php echo $rows3['DESCRIPTION']; ?>" /></td>
                                        <td><input name="ChkAddToTTL_<?php echo $k;?>" class="checkbox" id="ChkAddToTTL_<?php echo $k;?>" type="checkbox" value="1" <?php if($rows3['CHK_ADDTTL']==1){echo "checked";}?>/><input type="hidden" name="chkVal_<?php echo $k;?>" id="chkVal_<?php echo $k;?>" value="<?php if($rows3['CHK_ADDTTL']==1){echo "1";}?>" /></td>
										<td><input autocomplete="off" type="text" name="txtOtherExpense_<?php echo $k;?>" id="txtOtherExpense_<?php echo $k;?>" class="input-text" style="width:110px;" onKeyUp="getFinalCalculation();" placeholder="Other Income(USD)" value="<?php echo $rows3['OTHER_AMT']; ?>" /></td>
									</tr>
                                    <script>$("#selExpenseType_<?php echo $k;?>").html($("#selExpenseType").html());$("#selExpenseType_<?php echo $k;?>").val(<?php echo $rows3['EXPENSETYPEID']; ?>);</script>
								<?php }}?>
								</tbody>
								<tr class="input-text">
                                	<td colspan="5"><input type="hidden" name="hddnOtherExID" id="hddnOtherExID" value="<?php echo $k;?>"/></td>
								</tr>
                                <tr class="input-text">
                                	<td colspan="5">&nbsp;</td>
								</tr>
                                <tr class="input-text">
                                	<td colspan="5"><strong>TC In Expenses</strong></td>
                                </tr>
                                <tr class="input-text">
                                	<td>TC In (USD/Day)</td>
									<td><input autocomplete="off" type="text" name="txtTCPerDay" id="txtTCPerDay" class="input-text" style="width:110px;" placeholder="TC In (USD/Day)" value="<?php echo $rows['TC_RATE']; ?>" onKeyUp="getFinalCalculation();"/></td>
                                    <td>Total Hireage(USD) </td>
									<td><input autocomplete="off" type="text" name="txtTotalHireage" id="txtTotalHireage" class="input-text" style="width:110px;background-color:#EAEAEA;" placeholder="Total Hireage" readonly value="<?php echo $rows['TOTAL_HIREAGE']; ?>"/></td>
                                    <td></td>
                                </tr>
                                <tr class="input-text">
                                	<td>Add. Comm(%)</td>
									<td><input autocomplete="off" type="text" name="txtAddComm" id="txtAddComm" class="input-text" style="width:110px;" placeholder="Add. Comm(%)" value="<?php echo $rows['ADD_COMM']; ?>" onKeyUp="getFinalCalculation();"/></td>
                                    <td>Add. Comm(USD)</td>
									<td><input autocomplete="off" type="text" name="txtAddCommAmt" id="txtAddCommAmt" class="input-text" style="width:110px;background-color:#EAEAEA;" placeholder="Add. Comm(USD)" readonly value="<?php echo $rows['ADD_COMM_AMT']; ?>"/></td>
                                    <td></td>
                                </tr>
                                <tr class="input-text">
                                	<td></td>
									<td></td>
                                    <td>Nett Hireage(USD)</td>
									<td><input autocomplete="off" type="text" name="txtNetHireage" id="txtNetHireage" class="input-text" style="width:110px;background-color:#EAEAEA;" placeholder="Nett Hireage(USD)" readonly value="<?php echo $rows['NET_HIREAGE']; ?>"/></td>
                                    <td></td>
                                </tr>
                                <tr class="input-text">
                                	<td colspan="5">&nbsp;</td>
								</tr>
								<tr class="input-text">
                                	<td ><strong>Total Expenses</strong></td>
									<td></td>
                                    <td><input autocomplete="off" type="text" name="txtTotalExpenses" id="txtTotalExpenses" class="input-text" style="width:110px;background-color:#EAEAEA;" placeholder="Total Expenses" readonly value="<?php echo $rows['TOTAL_EXP_EST']; ?>"/></td>
                                    <td></td>
									<td></td>
                                    
                                </tr>
                                <tr class="input-text">
                                	<td ><strong>TC Earnings</strong></td>
                                    <td></td>
									<td><input autocomplete="off" type="text" name="txtVoyageEarnings" id="txtVoyageEarnings" class="input-text" style="width:110px;background-color:#EAEAEA;" placeholder="Voyage Earnings" value="<?php echo $rows['VOYAGE_EARN_EST']; ?>"/></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr class="input-text">
									<td ><strong>Profit / Day&nbsp;</strong></td>
                                    <td></td>
									<td><input autocomplete="off" type="text" name="txtProfitPerDay" id="txtProfitPerDay" class="input-text" style="width:110px;background-color:#EAEAEA;" placeholder="Voy Earnings/Utilisation Days" value="<?php echo $rows['PROFIT_PER_DAY_EST']; ?>"/></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                           </td>
                       </tr>
					   </table>
                
                        <div style="text-align:center;">
                        
                        <input autocomplete="off" type="hidden" name="action" id="action" value="submit" />
                        <input type="hidden" name="vesselrec" id="vesselrec" class="input-text" value="" /></div>
                        <!-- some hidden fields start -->
                        <input type="hidden" name="selDurFixPeriod" id="selDurFixPeriod"/>
                        <input type="hidden" name="txtCVEmonth" id="txtCVEmonth" value="<?php echo $obj->getFun63();?>"/>
                        <input type="hidden" name="txtBrokCommPayableBy" id="txtBrokCommPayableBy"/>
                         </div>
                    
				<!-- some hidden fields ends -->
                </form>
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/sort.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />
<script type="text/javascript">

$(document).ready(function(){
$(".areasize").autosize({append: "\n"});
$("#selVName").val(<?php echo $obj->getFun3();?>);
$("#selCPType").val(<?php echo $obj->getFun10();?>);
$("#selCharterers").val('<?php echo $obj->getFun11();?>');
$("#selCharOperation").val('<?php echo $obj->getFun12();?>');
$("#selLawArbit").val(<?php echo $obj->getFun13();?>);
$("#selDurFixPeriod").val(<?php echo $obj->getFun45();?>);
$("#txtBrokCommPayableBy").val('<?php echo $obj->getFun67();?>');
$("#txtBankingDetails").val(<?php echo $obj->getFun70();?>);

	
	$('#txtDelLtTimeGMT,#txtReDelLtTimeGMT').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		todayBtn: true,
		minuteStep: 1,
		autoclose:true
	});
	
	$('[id^=txtOffHireFrom_],[id^=txtOffHireTo_],#txtDeldate,#txtReDeldate').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
		}).on('changeDate', function(){
		getFinalCalculation();
		});
		
	$('#tripspan_1,#tripspan_2,#tripspan_3,#periodspan_1,#periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').hide();
		
	if($("#selDurFixPeriod").val() == 1)
	{
		$('#tripspan_1,#tripspan_2,#tripspan_3').show();
		$('#periodspan_1,periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').hide();
	}
	else if($("#selDurFixPeriod").val() == 2)
	{
		$('#periodspan_1,#periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').show();
		$('#tripspan_1,#tripspan_2,#tripspan_3').hide();
	}
	else
	{
		$('#tripspan_1,#tripspan_2,#tripspan_3,#periodspan_1,#periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').hide();
	}
	
	if($("#txtDailyGrossHireUSD").val()!="")
	{
		var addComm 		= $("#txtAddCommPerct").val();
		var dailyGrossHire  = $("#txtDailyGrossHireUSD").val();
		var addComm1		= ((addComm*dailyGrossHire)/100);
		$("#txtAddComm1").val(parseFloat(addComm1).toFixed(2));	
		
		var brokerComm		= $("#txtChartererAcc").val();
		var calBrokerComm	= ((brokerComm*dailyGrossHire)/100);
		$("#txtBrokerComm").val(parseFloat(calBrokerComm).toFixed(2));	
	}
	
	if($("#txtBrokCommPayableBy").val() == 'Charterer')
	{
		var dailyGrossHire1  = $("#txtDailyGrossHireUSD").val();
		var addComm2		 = $("#txtAddComm1").val();
		var brokerComm1		 = $("#txtBrokerComm").val();
		var cal1			 = parseFloat(addComm2) + parseFloat(brokerComm1);
	  	var cal2			 = parseFloat(dailyGrossHire1) - parseFloat(cal1);
		if(isNaN(cal2)){cal2 = 0.00;}
		$("#txtNettHire").val(parseFloat(cal2).toFixed(2));
	}
	
	if($("#txtBrokCommPayableBy").val() == 'Operator')
	{
		var dailyGrossHire2  = $("#txtDailyGrossHireUSD").val();
		var addComm3		 = $("#txtAddComm1").val();
		var cal1			 = parseFloat(dailyGrossHire2) - parseFloat(addComm3);
		if(isNaN(cal1)){cal1 = 0.00;}
		$("#txtNettHire").val(parseFloat(cal1).toFixed(2));
	}
	
	
	
	$("#txtVoyageEarnings, #txtDeliveryHFO, #txtDeliveryMGO, #txtPriceDelHFO, #txtPriceDelMGO, #txtSupercargoMeals, #txtHoldCleanInterm, #txtILOHCtcFix, #txtAddComm, #txtBrokerCommTCfix, #txtTCdays, #txtOffHireDays, #txtUtilisationDays, #txtOffHireRate, #txtNoOfTrips, #txtHireFixPerUSD, #txtOtherIncome, #txtDelHFOmt, #txtReDelHFOmt, #txtDelHFOusdmt, #txtReDelHFOusdmt, #txtDelMGOmt, #txtReDelMGOmt, #txtDelMGOusdmt, #txtReDelMGOusdmt, [id^=txtOtherIncome_], [id^=txtOtherExpense_], [id^=txtOffHireRate_], #txtTCPerDay, #txtAddComm").numeric();
	
	$('[id^=ChkAddToTTL_]').on('ifChecked', function () { 
	   var idd = $(this).attr('id');
	   var iddd = idd.split("_");
	   $("#chkVal_"+iddd[1]).val(1);
	   getFinalCalculation();
	});
	 
	$('[id^=ChkAddToTTL_]').on('ifUnchecked', function () { 
	   var idd = $(this).attr('id');
	   var iddd = idd.split("_");
	   $("#chkVal_"+iddd[1]).val('');
	   getFinalCalculation();
	});
	
	$("#txtDate,#txtCPdate,[id^=txtDelBunDate_],[id^=txtReDelBunDate_]").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
	
	
	$("#frm1").validate({
		rules: {
			selVName:"required",
			txtTCNo:"required"
			},
		messages: {
			selVName:"*",
			},
	submitHandler: function(form)  {
		jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
				if(r){
					jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
					$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
					$("#popup_content").css({"background":"none","text-align":"center"});
					$("#popup_ok,#popup_title").hide();  
					frm1.submit();
					}
					else{return false;}
				});
		}
	});
	getDelBunkerCalculation();
	getReDelBunkerCalculation();
	getFinalCalculation();
});

function getValue()
{
	
}


function getString(var1)
{
   var var2 = var1.split('-');  
   return var2[2]+'/'+var2[1]+'/'+var2[0];
}

function getData()
{
	  if($("#selVName").val()!="")
	  {
		  $.post("options.php?id=42",{vessel1d:""+$("#selVName").val()+""}, function(data) 
			{
				var vsldata = JSON.parse(data);
				$.each(vsldata[$("#selVName").val()], function(index, array) {
				
					$('#txtVType').val(array['type']);
					$('#txtDWTS').val(array['dwtsum']);
					$('#txtDWTT').val(array['dwttrop']);
					$('#txtGCap').val(array['grain']);
					$('#txtBCap').val(array['bale']);
					
					$('#txtBFullSpeed').val(array['bfs']);
					$('#txtBEcoSpeed1').val(array['bes1']);
					$('#txtBEcoSpeed2').val(array['bes2']);
					$('#txtBFOFullSpeed').val(array['fobfs']);
					$('#txtBFOEcoSpeed1').val(array['fobes1']);
					$('#txtBFOEcoSpeed2').val(array['fobes2']);
					$('#txtBDOFullSpeed').val(array['dobfs']);
					$('#txtBDOEcoSpeed1').val(array['dobes1']);
					$('#txtBDOEcoSpeed2').val(array['dobes2']);
					
					$('#txtLFullSpeed').val(array['lfs']);
					$('#txtLEcoSpeed1').val(array['les1']);
					$('#txtLEcoSpeed2').val(array['les2']);
					$('#txtLFOFullSpeed').val(array['folfs']);
					$('#txtLFOEcoSpeed1').val(array['foles1']);
					$('#txtLFOEcoSpeed2').val(array['foles2']);
					$('#txtLDOFullSpeed').val(array['dolfs']);
					$('#txtLDOEcoSpeed1').val(array['doles1']);
					$('#txtLDOEcoSpeed2').val(array['doles2']);
					
					$('#txtPIFOFullSpeed').val(array['foidle']);
					$('#txtPWFOFullSpeed').val(array['fwking']);
					$('#txtPIDOFullSpeed').val(array['ddle']);
					$('#txtPWDOFullSpeed').val(array['dwking']);
					$('#vesselrec').val(array['num']);
					$('#txtFlag').val(array['flag']);
					$('#txtBuiltYear').val(array['year']);
					$('#txtGNRT').val(array['gnrt']);
					$('#txtLOA').val(array['loa']);
					$('#txtGear').val(array['gear']);
					$('#txtBeam').val(array['beam']);
					$('#txtTPC').val(array['tpc']);
					
					$('#txtBuildYard').val(array['BuildYard']);
					$('#txtYearBuild').val(array['year']);
					$('#txtFlag1').val(array['flag']);
					$('#txtPortOfRegis').val(array['txtPortOfRegis']);
					$('#txtIMOnum').val(array['txtIMOnum']);
					$('#txtOwnerPandI').val(array['txtOwnerPandI']);
					$('#txtCallSign').val(array['txtCallSign']);
					$('#txtInmarsatTel').val(array['txtInmarsatTel']);
					$('#txtInmarsatEmail').val(array['txtInmarsatEmail']);
					$('#txtLOA1').val(array['loa']);
					$('#txtBreadth').val(array['beam']);
					$('#txtSummerDWT').val(array['dwtsum']);
					$('#txtSummerDraft').val(array['txtSummerDraft']);
					$('#txtTPC1').val(array['tpc']);
					$('#txtClassID').val(array['txtClassID']);
					$('#txtLastSpSurvey').val(array['txtLastSpSurvey']);
					$('#txtLastDD').val(array['txtLastDD']);
					$('#txtGrossTonnage').val(array['gnrt']);
					$('#txtNetTonnage').val(array['txtNetTonnage']);
					$('#txtSuezGRT').val(array['txtSuezGRT']);
					$('#txtSuezNRT').val(array['txtSuezNRT']);
					$('#txtPanamaNRT').val(array['PanamaNRT']);
					$('#txtGrainCap').val(array['txtGrainCap']);
					$('#txtBaleCap').val(array['txtBaleCap']);
					$('#txtCranes').val(array['txtCranes']);
					$('#txtGrabs').val(array['txtGrabs']);
					$('#txtKeelTopMast').val(array['KeelTopMast']);
					$('#txtWaterlineTopMast').val(array['WaterlineTopMast']);
					$('#txtCTankCap').val(array['CTankCap']);
					$('#txtCTankNoOfGrades').val(array['CTankNoOfGrades']);
					$('#txtCPumpCap').val(array['CPumpCap']);
					$('#txtCTotalSBTCap').val(array['CTotalSBTCap']);
				});
			});  
	  }
	  else
	  {
		  $('#txtVType,#txtDWTS,#txtDWTT,#txtGCap,#txtBCap,#txtBFullSpeed,#txtBEcoSpeed1,#txtBEcoSpeed2,#txtBFOFullSpeed,#txtBFOEcoSpeed1,#txtBFOEcoSpeed2,#txtBDOFullSpeed,#txtBDOEcoSpeed1,#txtBDOEcoSpeed2,#txtLFullSpeed,#txtLEcoSpeed1,#txtLEcoSpeed2,#txtLFOFullSpeed,#txtLFOEcoSpeed1,#txtLFOEcoSpeed2,#txtLDOFullSpeed,#txtLDOEcoSpeed1,#txtLDOEcoSpeed2,#txtPIFOFullSpeed,#txtPWFOFullSpeed,#txtPIDOFullSpeed,#txtPWDOFullSpeed,#vesselrec,#txtFlag,#txtGear,#txtBuiltYear,#txtGNRT,#txtLOA,#txtBuildYard,#txtYearBuild,#txtFlag1,#txtPortOfRegis,#txtIMOnum,#txtOwnerPandI,#txtCallSign,#txtInmarsatTel,#txtInmarsatEmail,#txtLOA1,#txtBreadth,#txtSummerDWT,#txtSummerDraft,#txtTPC1,#txtClassID,#txtLastSpSurvey,#txtLastDD,#txtGrossTonnage,#txtNetTonnage,#txtSuezGRT,#txtSuezNRT,#txtPanamaNRT,#txtGrainCap,#txtBaleCap,#txtCranes,#txtGrabs,#txtKeelTopMast,#txtWaterlineTopMast').val("");
	  }
	 getFinalCalculation();
}


function getValidate()
{
	getFinalCalculation();
}


function getChartererData()
{
	if($("#selCharOperation").val()!='')
	{
		$.post("options.php?id=59",{selCharOperation:""+$("#selCharOperation").val()+""}, function(data) 
			{
					$('#txtAddress').val(data);

			});
	}
	else
	{
		$('#txtAddress').val('');
	}
}

function getTimeDiff(date2,date1)
{	
	if(date1 != '' &&  date2 != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat(date2);
		start_actual_time    =  getDateWithSpecificFormat(date1);
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000;
		return days.toFixed(5);	
	}
}

function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' '+dateSplit1[1];
	return currentDate;
}


function getFinalCalculation()
{
	var tcdays = parseFloat(getTimeDiff($("#txtReDeldate").val(),$("#txtDeldate").val()));if(isNaN(tcdays)){tcdays = 0.00;}
	$("#txtTCdays").val(parseFloat(tcdays).toFixed(4));
    
	var delbunker          = $('#txtTotalDelAmount').sum();
	
	var redelbunker          = $('#txtTotalReDelAmount').sum();
	
	$("#txtBunkerDifference").val(parseFloat(parseFloat(delbunker) - parseFloat(redelbunker)).toFixed(2));
	
	var numoff = $("#txtOFFID").val();
	var offhiredays = 0;
	for(var i =1;i <=numoff;i++)
	{
		if(typeof($("#txtOffHireFrom_"+i).val())!='undefined')
		{
			var timediff = parseFloat(getTimeDiff($("#txtOffHireTo_"+i).val(),$("#txtOffHireFrom_"+i).val()));
			if(isNaN(timediff)){timediff = 0.00;}
			$("#txtOffHireDays_"+i).val(parseFloat(timediff).toFixed(4))
			var offrate  = parseFloat($("#txtOffHireRate_"+i).val());
			if(isNaN(offrate)){offrate = 0.00;}
			offhiredays =   parseFloat(parseFloat(offhiredays) + parseFloat(timediff));
			$("#txtOffHireAmt_"+i).val(parseFloat(parseFloat(timediff)*parseFloat(offrate)).toFixed(2));
		}
	}	
	
	if(isNaN(offhiredays)){offhiredays = 0.00;}
	$("#txtLessOffHire").val($('[id^=txtOffHireAmt_]').sum());
	
	var cal1 = parseFloat(tcdays) - parseFloat(offhiredays);
	if(isNaN(cal1)){cal1 = 0.00;}
	$("#txtUtilisationDays").val(parseFloat(cal1).toFixed(2));
	
	var utilisationDays  = parseFloat($("#txtUtilisationDays").val());if(isNaN(utilisationDays)){utilisationDays = 0.00;}
	var nettHire		 = parseFloat($("#txtNettHire").val());if(isNaN(nettHire)){nettHire = 0.00;}
	var cal1			 = parseFloat(utilisationDays) * parseFloat(nettHire);
	if(isNaN(cal1)){cal1 = 0.00;}
	$("#txtNettRev").val(parseFloat(cal1).toFixed(2));
	
	var cVEmonth  			= parseFloat($("#txtCVEM").val());if(isNaN(cVEmonth)){cVEmonth = 0.00;}
	var utilisationDays1    = parseFloat($("#txtUtilisationDays").val());if(isNaN(utilisationDays1)){utilisationDays1 = 0.00;}
	var cal1			    = (parseFloat(cVEmonth) / 30);
	var cal2				= cal1 * parseFloat(utilisationDays1);
	if(isNaN(cal2)){cal2 = 0.00;}
	$("#txtCVE").val(parseFloat(cal2).toFixed(2));	
	
	//-----------------otherIncome-----------
	var nettRev  	             = parseFloat($("#txtNettRev").val());if(isNaN(nettRev)){nettRev = 0.00;}
	var txtBunkerDifference  	 = parseFloat($("#txtBunkerDifference").val());if(isNaN(txtBunkerDifference)){txtBunkerDifference = 0.00;}
	var lessOffHire  			 = parseFloat($("#txtLessOffHire").val());if(isNaN(lessOffHire)){lessOffHire = 0.00;}
	var cVE  		             = parseFloat($("#txtCVE").val());if(isNaN(cVE)){cVE = 0.00;}
	var otherIncome              = parseFloat($('[id^=txtOtherIncome_]').sum());if(isNaN(otherIncome)){otherIncome = 0.00;}
	var cal1		             = parseFloat(nettRev) - parseFloat(lessOffHire) + parseFloat(cVE) + parseFloat(txtBunkerDifference);
	if(isNaN(cal1)){cal1 = 0.00;}
	$("#txtNETHire").val(parseFloat(cal1).toFixed(2));
	var cal2		             = parseFloat(cal1) + parseFloat(otherIncome);
	$("#txtTotalRev").val(parseFloat(cal2).toFixed(2));
	//--------------------------------------------
	
	//---------------otherExpense------------------
	var hddnOtherExID = $("#hddnOtherExID").val();
	var otherexpense = 0;
	for(var i =1;i <=hddnOtherExID;i++)
	{
		if(typeof($("#selExpenseType_"+i).val())!='undefined')
		{   
		    if($("#chkVal_"+i).val()==1)
	        {
				var otherexpense1 = parseFloat($("#txtOtherExpense_"+i).val());
				if(isNaN(otherexpense1)){otherexpense1 = 0.00;}
				otherexpense =   parseFloat(parseFloat(otherexpense) + parseFloat(otherexpense1));
			}
		}
	}
	
	var txtTCPerDay  	 		= parseFloat($("#txtTCPerDay").val());if(isNaN(txtTCPerDay)){txtTCPerDay = 0.00;}
	var totalhireage            = parseFloat(parseFloat(utilisationDays)*parseFloat(txtTCPerDay));if(isNaN(totalhireage)){totalhireage = 0.00;}
	$("#txtTotalHireage").val(parseFloat(totalhireage).toFixed(2));
	var txtAddComm  	 		= parseFloat($("#txtAddComm").val());if(isNaN(txtAddComm)){txtAddComm = 0.00;}
	var txtAddCommAmt           = parseFloat(parseFloat(parseFloat(totalhireage)*parseFloat(txtAddComm))/100);if(isNaN(txtAddCommAmt)){txtAddCommAmt = 0.00;}
	$("#txtAddCommAmt").val(parseFloat(txtAddCommAmt).toFixed(2));
	$("#txtNetHireage").val(parseFloat(parseFloat(totalhireage) - parseFloat(txtAddCommAmt)).toFixed(2));
	
	var cal2		 		= parseFloat(otherexpense) + parseFloat($("#txtNetHireage").val());
	if(isNaN(cal2)){cal2 = 0.00;}
	
	$("#txtTotalExpenses").val(parseFloat(cal2).toFixed(2));
	
	var totalRev1  	 	= parseFloat($("#txtTotalRev").val());if(isNaN(totalRev1)){totalRev1 = 0.00;}
	var totalExpenses  	= parseFloat($("#txtTotalExpenses").val());if(isNaN(totalExpenses)){totalExpenses = 0.00;}
	var cal2		 	= parseFloat(totalRev1) - parseFloat(totalExpenses);
	if(isNaN(cal2)){cal2 = 0.00;}
	$("#txtVoyageEarnings").val(parseFloat(cal2).toFixed(2));
	
	
	var voyageEarnings  	= parseFloat($("#txtVoyageEarnings").val());if(isNaN(voyageEarnings)){voyageEarnings = 0.00;}
	var utilisationDays  	= parseFloat($("#txtUtilisationDays").val());if(isNaN(utilisationDays)){utilisationDays = 0.00;}
	var cal2		 		= parseFloat(voyageEarnings) / parseFloat(utilisationDays);
	if(isNaN(cal2) || cal2 == "-Infinity" || cal2 == "Infinity"){cal2 = 0.00;}
	$("#txtProfitPerDay").val(parseFloat(cal2).toFixed(2));
	//------------------------------------------------------------
}



function getDelBunkerCalculation()
{
	var txtDELID = $("#txtDELID").val();
	for(var i =1;i <=txtDELID;i++)
	{
		var txtDelQty      = parseFloat($("#txtDelQty_"+i).val());if(isNaN(txtDelQty)){txtDelQty = 0.00;}
		var txtDelPrice    = parseFloat($("#txtDelPrice_"+i).val());if(isNaN(txtDelPrice)){txtDelPrice = 0.00;}
		var amount          =   parseFloat(parseFloat(txtDelQty)*parseFloat(txtDelPrice));if(isNaN(amount)){amount = 0.00;}
		$('#txtDelAmount_'+i).val(parseFloat(amount).toFixed(2));
	}
	$("#txtTotalDelAmount").val($('[id^=txtDelAmount_]').sum());
	getFinalCalculation();
}

function getReDelBunkerCalculation()
{
	var txtREDELID = $("#txtREDELID").val();
	for(var i =1;i <=txtREDELID;i++)
	{
		var txtReDelQty      = parseFloat($("#txtReDelQty_"+i).val());if(isNaN(txtReDelQty)){txtReDelQty = 0.00;}
		var txtReDelPrice    = parseFloat($("#txtReDelPrice_"+i).val());if(isNaN(txtReDelPrice)){txtReDelPrice = 0.00;}
		var amount          =   parseFloat(parseFloat(txtReDelPrice)*parseFloat(txtReDelQty));if(isNaN(amount)){amount = 0.00;}
		$('#txtReDelAmount_'+i).val(parseFloat(amount).toFixed(2));
	}
	$("#txtTotalReDelAmount").val($('[id^=txtReDelAmount_]').sum());
	getFinalCalculation();
}


 
</script>
</body>
</html>