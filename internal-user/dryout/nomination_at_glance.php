<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

if (@$_REQUEST['action'] == 'submit')
{
 	if($_REQUEST['txtMappingid2'] == "")
	{
 		
	}else{
		
		$msg = $obj->deleteEntry($_REQUEST['txtMappingid2']);
	}
	header('Location:./nomination_at_glance.php?msg='.$msg);
}
if (@$_REQUEST['action1'] == 'submit1')
{
 	$msg = $obj->insertCostSheetName();
	header('Location:./nomination_at_glance.php?msg=5');
}
$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rights   = $obj->getUserRights($uid,$moduleid,3);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Nominations at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Nominations at a glance added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating Nominations at a glance.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
					<h3 style="text-align:center">Nominations at a glance List</h3>
						<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>                 
							<div style="height:10px;">
								<input type="hidden" name="action" value="submit" />
								<input type="hidden" name="txtMappingid2" id="txtMappingid2" value="" />
							</div>
							<div class="box-body table-responsive">
								<table id="apen_cargo_position_list" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Business Type</th>
											<th>Nom ID</th>
											<th>Vessel</th>
											<th>Standards Status</th>
											<th>Edit Nom</th>
											<th>Voyage Financials</th>	
											<th>Documents</th>	
											<?php if($rights == 1) { ?>				
											<th>Convert to Fixture</th>
											<th>Cancel</th>
											<?php } ?>
										</tr>
									</thead>
									<tbody>
									<?php 
									$sql = "select * from mapping_master where  MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1 order by MAPPINGID"; 
									$res = mysql_query($sql);
									$rec = mysql_num_rows($res);
									$i=1;
									$submit = 0;
									if($rec == 0)
									{
									
									}else{
										while($rows = mysql_fetch_assoc($res))
									{
									?>
										<tr style="font-size:11px;">
											<td><?php echo $obj->getBusinessTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'VESSEL_IMO_ID'),"BUSINESSTYPEID"));?></td>
											<td><?php echo $rows['NOMINATION_ID'];?></td>
											<td><?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'VESSEL_IMO_ID'),"VESSEL_NAME");?></td>
											<td>
												<?php if($obj->getAllStandardsValues($rows['MAPPINGID']) == 0){ $sstatus = 1;?>
													<a href="check_standards.php?mappingid=<?php echo $rows['MAPPINGID'];?>" style="color:blue;" title="Click me" >Check</a>
												<?php }else{ $sstatus = 0;?>
													<a href="check_standards.php?mappingid=<?php echo $rows['MAPPINGID'];?>" style="color:red;" title="Click me" >Check</a>
												<?php }?>
											</td>
											<td align="center" valign="middle" class="input-text" style="font-size:10px;">
												<?php if($obj->getEditNominationValues($rows['MAPPINGID']) == 0){?>
													<a href="edit_nominations.php?mappingid=<?php echo $rows['MAPPINGID'];?>&tab=1" style="color:red;" title="Click me" >Edit Nom</a>
												<?php }else{?>
													<a href="edit_nominations.php?mappingid=<?php echo $rows['MAPPINGID'];?>&tab=1" style="color:blue;" title="Click me" >Edit Nom</a>
												<?php }?>
											</td>
										
											<td style="text-align:center;">
												<?php 
													$sql1 = "select * from cost_sheet_name_master where MAPPINGID='".$rows['MAPPINGID']."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";
									
													$res1 = mysql_query($sql1);
													$rec1 = mysql_num_rows($res1);
													if($rec1 > 0)
													{
														while($rows1 = mysql_fetch_assoc($res1))
														{
															if($_SESSION['company'] == 2)
															{
																$href = "./voyage_charterer_lpg.php?mappingid=".$rows['MAPPINGID']."&cost_sheet_id=".$rows1['COST_SHEETID'];
															}else{
																
																$href = "./voyage_charterer.php?mappingid=".$rows['MAPPINGID']."&cost_sheet_id=".$rows1['COST_SHEETID'];
															}
															echo "<a href='".$href."'>".$rows1['SHEET_NAME']."</a>";
															echo "<br/>";
														}
													}
													if($rights == 1)
													{
													?>
													<br/><button class="btn btn-primary btn-flat" type="button" data-toggle="modal" data-target="#compose-modal1" title="Add New CS" onClick="openWin2(<?php echo $rows['MAPPINGID'];?>);">A</button>
													<?php } ?>
												</td>
												<td>
													<a href="documents_nom.php?mappingid=<?php echo $rows['MAPPINGID'];?>" title="upload documents">Docs</a>
												</td>
												<?php if($rights == 1) { ?>
												<td>
													<?php 
													if($obj->getFreightEstimationStatusForAll($rows['MAPPINGID']) != ""){
													?>
														<a href="convert_to_fixture.php?mappingid=<?php echo $rows['MAPPINGID'];?>" ><button class="btn btn-primary btn-flat" id="inner-login-button" type="button" title="Convert to Fixture"  ><b><span id="d27e53">Convert</span></b></button></a>
													<?php }?>
												</td>
												<td>
												<a href="#A" title="remove entry" onClick="getValidate2(<?php echo $rows['MAPPINGID'];?>);"><i class="fa fa-times" style="color:red;"></i></a>
												</td>
												<?php } ?>
											</tr>
										<?php $i++;}}?>
									</tbody>
								</table>
							</div>
						</form>
						<div class="modal fade" id="compose-modal1" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									</div>
									<div class="modal-body">
										<div class="box box-primary">
											<div class="box-body no-padding">
												<form name="frm2" id="frm2" method="post" enctype="multipart/form-data" action="<?php echo $pagename;?>">
													<div class="row invoice-info">
														<div class="col-sm-12 invoice-col">&nbsp;</div>
													</div>
													<div class="row invoice-info" style="padding:2%">
														<div class="col-sm-12 invoice-col">
															Voyage Financials Name
															<address>
																<input type="text" name="txtFile" id="txtFile" class="form-control" autocomplete="off" value="<?php echo $obj->getNomDetailsData($mappingid,"FREIGHT_RATE");?>" placeholder="Voyage Financials Name" />
																<input type="hidden" name="txtMappingid1" id="txtMappingid1" />
															</address>
														</div><!-- /.col -->
													</div>
													<?php if($rights == 1){ ?>
													 <div class="row invoice-info">
														<div class="col-sm-12 invoice-col" style="text-align:center;">
															 <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate1();">Submit</button>
															 <input type="hidden" name="action1" id="action1" value="submit1" />
														</div><!-- /.col -->
													 </div>
													 <?php } ?>
												</form>
											</div><!-- /.box-body -->
										</div>
									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div>
					</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
 <script type="text/javascript">
 $(document).ready(function(){
	$("#apen_cargo_position_list").dataTable();
});

function openWin2(mappinid)
{
	$('#basic-modal-content1').modal();
	$("#txtMappingid1").val(mappinid);
	$("#simplemodal-container").css({"height":"150px"});
	
}

function getValidate1()
{
	if($("#txtFile").val() != "")
	{
		return true;
	}
	else
	{
		jAlert('Please fill the file name', 'Alert');
		return false;
	}
}

function getValidate2(var1)
{
	jConfirm('Are you sure you want to cancel this entry ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtMappingid2").val(var1);
			document.frm1.submit();
		}
		else{return false;}
		});	
}

</script>
		
</body>
</html>