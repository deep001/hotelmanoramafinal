<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_tc.php";}else if($page == 2){$page_link = "vessel_in_post_tc.php";}else {$page_link = "vessel_in_history_tc.php";}

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertTcTripDetails();
	header('Location:./'.$page_link.'?msg='.$msg);
}

$id = $obj->getLatestCostSheetIDTC($comid);
$obj->viewTCEstimationTempleteRecordsNew($id);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(21); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops TC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops TC&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance - TC&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
					<div align="right">
						
						<a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
					<div style="height:10px;">&nbsp;</div>
					
				<div class="box box-primary">
					<h3 style=" text-align:center;">Payment / Invoice Grid : <?php echo $obj->getVesselIMOData($obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></h3>
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>					 
						    <?php $i = 0;
							$sql = "select * from chartering_tc_estimate_slave1 where TCOUTID = '".$obj->getFun1()."' ";
							$result = mysql_query($sql);
							while($rows = mysql_fetch_assoc($result))
							{$i++;
							?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Trip/Period <?=$i;?> (<?php echo date('d-m-Y H:i',strtotime($rows['DEL_DATE_EST']));?> To <?php echo date('d-m-Y H:i',strtotime($rows['REDEL_DATE_EST']));?>)</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="20%">Name</th>
												<th width="20%">Decription</th>
                                                <th width="25%">Vendor</th>
                                                <th width="35%">&nbsp;</th>											
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><strong>Nett Hire</strong></td>
												<td>Hire Invoice</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($rows['TTL_REV_VENDOR']);?></td>
                                                <td>
												<?php if($obj->getVendorListNewBasedOnID($rows['TTL_REV_VENDOR']) != '') 
                                                 {
												  ?>
                                                      
                                                    <a href="hireinvoicetc.php?comid=<?php echo $comid;?>&page=<?php echo $page;?>&randomid=<?php echo $rows['RANDOMID'];?>" ><button class="btn btn-info btn-flat" type="button">Hire Invoice</button></a>&nbsp;&nbsp;
                                             <?php }?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Address Commission</strong></td>
												<td>Add Comm(<?php echo $rows['ADD_COMM_EST'];?>%)</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($rows['ADD_COMM_VENDOR']);?></td>
                                                <td>
												<?php if($obj->getVendorListNewBasedOnID($rows['ADD_COMM_VENDOR']) != '') 
                                                  {
												      $desc = "Add Comm(".$rows['ADD_COMM_EST']."%)";
													  $intoicetype = 'Address Commission Invoice';
												  ?>
                                                      
                                                    <a href="otherinvoicetc.php?comid=<?php echo $comid;?>&page=<?php echo $page;?>&randomid=<?php echo $rows['RANDOMID'];?>&vendor=<?php echo $rows['ADD_COMM_VENDOR'];?>&amount=<?php echo $rows['ADD_COMM_CAL_EST'];?>&desc=<?php echo $desc;?>&invtype=<?php echo $intoicetype;?>" ><button class="btn btn-info btn-flat" type="button">Add Comm Invoice</button></a>&nbsp;&nbsp;
                                             <?php }?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Broker's Commission</strong></td>
												<td>Broker's Comm(<?php echo $rows['BROKER_COMM_EST'];?>%)</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($rows['BROKER_COMM_VENDOR']);?></td>
                                                <td>
												<?php if($obj->getVendorListNewBasedOnID($rows['BROKER_COMM_VENDOR']) != '') 
                                                  {
													  $desc = "Broker's Comm(".$rows['BROKER_COMM_EST']."%)";
													  $intoicetype = "Broker's Commission Invoice";
												  ?>
                                                      
                                                    <a href="otherinvoicetc.php?comid=<?php echo $comid;?>&page=<?php echo $page;?>&randomid=<?php echo $rows['RANDOMID'];?>&vendor=<?php echo $rows['BROKER_COMM_VENDOR'];?>&amount=<?php echo $rows['BROKER_COMM_CAL_EST'];?>&desc=<?php echo $desc;?>&invtype=<?php echo $intoicetype;?>" ><button class="btn btn-info btn-flat" type="button">Broker's Comm Invoice</button></a>&nbsp;&nbsp;
                                             <?php }?>
                                                </td>
                                            </tr>
                                            <?php $k = 0;
											$sql1 = "select * from chartering_estimate_tc_slave2 where STATUS=1 and TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."'";
                                            $result1 = mysql_query($sql1);
                                            while($rows1 = mysql_fetch_assoc($result1))
											{$k++;
											    
											?>
                                            <tr>
                                                <td><strong>Other Income</strong></td>
												<td><?php echo $rows1['DESCRIPTION'];?></td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($rows1['OTHER_EXP_VENDOR']);?></td>
                                                <td>
												<?php if($obj->getVendorListNewBasedOnID($rows1['OTHER_EXP_VENDOR']) != '') 
                                                  {
													  $desc = $rows1['DESCRIPTION'];
											          $intoicetype = "Other Income Invoice";
												  ?>
                                                    <a href="otherinvoicetc.php?comid=<?php echo $comid;?>&page=<?php echo $page;?>&randomid=<?php echo $rows['RANDOMID'];?>&vendor=<?php echo $rows1['OTHER_EXP_VENDOR'];?>&amount=<?php echo $rows1['OTHER_AMT'];?>&desc=<?php echo $desc;?>&invtype=<?php echo $intoicetype;?>" ><button class="btn btn-info btn-flat" type="button">Invoice</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="payment_othertc.php?comid=<?php echo $comid;?>&page=<?php echo $page;?>&randomid=<?php echo $rows['RANDOMID'];?>&vendor=<?php echo $rows1['OTHER_EXP_VENDOR'];?>&amount=<?php echo $rows1['OTHER_AMT'];?>&desc=<?php echo $desc;?>&invtype=<?php echo $intoicetype;?>" ><button class="btn btn-warning btn-flat" type="button">Payment Advice</button></a>
                                             <?php }?>
                                                </td>
                                            </tr>
									  <?php }?>
                                            <?php $k = 0;
											$sql1 = "select * from chartering_estimate_tc_slave2 where STATUS=2 and TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."'";
                                            $result1 = mysql_query($sql1);
                                            while($rows1 = mysql_fetch_assoc($result1))
											{$k++;
											?>
                                            <tr>
                                                <td><strong>Other Expense</strong></td>
												<td><?php echo $obj->getExpenseTypeNameBasedOnID($rows1['EXPENSETYPEID'])." (".$rows1['DESCRIPTION'].")";?></td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($rows1['OTHER_EXP_VENDOR']);?></td>
                                                <td>
												<?php if($obj->getVendorListNewBasedOnID($rows1['OTHER_EXP_VENDOR']) != '') 
                                                  {
													  $desc = $obj->getExpenseTypeNameBasedOnID($rows1['EXPENSETYPEID'])." (".$rows1['DESCRIPTION'].")";
											          $intoicetype = "Other Expense Invoice";
												  ?>
                                                    <a href="otherinvoicetc.php?comid=<?php echo $comid;?>&page=<?php echo $page;?>&randomid=<?php echo $rows['RANDOMID'];?>&vendor=<?php echo $rows1['OTHER_EXP_VENDOR'];?>&amount=<?php echo $rows1['OTHER_AMT'];?>&desc=<?php echo $desc;?>&invtype=<?php echo $intoicetype;?>" ><button class="btn btn-info btn-flat" type="button">Invoice</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="payment_othertc.php?comid=<?php echo $comid;?>&page=<?php echo $page;?>&randomid=<?php echo $rows['RANDOMID'];?>&vendor=<?php echo $rows1['OTHER_EXP_VENDOR'];?>&amount=<?php echo $rows1['OTHER_AMT'];?>&desc=<?php echo $desc;?>&invtype=<?php echo $intoicetype;?>" ><button class="btn btn-warning btn-flat" type="button">Payment Advice</button></a>
                                             <?php }?>
                                                </td>
                                            </tr>
									  <?php }?>
									        <tr>
                                                <td><strong>Nett Hireage</strong></td>
												<td>Hireage Invoice</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($rows['NET_HIR_VENDOR']);?></td>
                                                <td>
												<?php if($obj->getVendorListNewBasedOnID($rows['NET_HIR_VENDOR']) != '') 
                                                  {
												  ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="invoice_hiretc.php?comid=<?php echo $comid;?>&page=<?php echo $page;?>&randomid=<?php echo $rows['RANDOMID'];?>" ><button class="btn btn-default btn-flat" type="button">Hire Statement</button></a>&nbsp;&nbsp;
                                             <?php }?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
						<?php }?>
						
					</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
		
</body>
</html>