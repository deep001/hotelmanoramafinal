<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$mappingid = $_REQUEST['mappingid'];
if (@$_REQUEST['action'] == 'submit')
 {
 	if($_REQUEST['txtStatus'] == "1")
	{
		$msg = $obj->insertPdfDetails($_REQUEST['mappingid']);
	}
	else if($_REQUEST['txtStatus'] == "2")
	{
		$msg = $obj->deletePdfDetails($_REQUEST['mappingid'],$_REQUEST['txtFileName']);
	}
	
	header('Location:./documents_nom.php?mappingid='.$_REQUEST['mappingid']);
}
$id = $obj->getMappingData($mappingid,"OPEN_VESSEL_ID");
$obj->viewVesselOpenPositionRecords($id);
$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rigts = $obj->getUserRights($uid,$moduleid,3);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Nominations at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="nomination_at_glance.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             DOCUMENTS<input type="hidden" name="txtStatus" id="txtStatus" value="1" /><input type="hidden" name="txtFileName" id="txtFileName" value="" /><input type="hidden" name="action" id="action" value="submit" />
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    
                    
                    <div class="row invoice-info" >
                        <div class="col-sm-4 invoice-col">
                        File Name
                            <address>
                              <input type="text" name="txtFile" id="txtFile" value="" class="form-control" placeholder="File Name" autocomplete="off" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                             &nbsp;
                            <address>
								<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Attachment">
                                    <i class="fa fa-paperclip"></i> Attachment
                                    <input type="file" class="form-control" multiple name="mul_file[]" id="mul_file" title="" data-widget="Add Attachment" data-toggle="tooltip" data-original-title="Add Attachment"/>
                                </div>
							</address>
						</div><!-- /.col -->
                        
						<?php
							if($rigts == 1)
							{ 
						?>
						<div class="col-sm-4 invoice-col">
                        &nbsp;
                            <address>
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
				           </address>
                        </div><!-- /.col -->
                       <?php } ?>
                       
					</div>
					
					<div class="row">
                      <div class="col-xs-12">
                         <div class="box box-primary">
						    <div class="box-body no-padding">
							  <table class="table table-striped">
							     <thead>
                                  <tr>
                                    <th width="40%" align="left" valign="middle">File Name</th>
                                    <th width="30%" align="center" valign="middle">Upload</th>
                                    <th width="30%" align="center" valign="middle">Details</th>
                                  </tr>	  
                                  
                                </thead>
                                 <tbody>
                                    <?php 
									$qsql = "select * from cp_pdf_details where MAPPINGID=".$mappingid." order by CPID";
									$qres = mysql_query($qsql);
									$qrec = mysql_num_rows($qres);
									if($qrec == 0)
									{
										
									}
									else 
									{$i=1;
										while($qrows = mysql_fetch_assoc($qres))
										{?>
                                        <tr>
                                            <td align="left" valign="middle"><?php echo $qrows['USER_FILE_NAME'];?></td>
                                            <td align="center" valign="middle">
                                            <?php if($qrows['FILE_NAME'] != " "){?>
                                            <a href="../../attachment/<?php echo $qrows['FILE_NAME'];?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="Click to view file"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $qrows['FILE_NAME'];?></a>
                                            
                                            <?php }?>
                                            </td>
                                            <td align="center" valign="middle" ><a href="#1" onClick="getValidate2('<?php echo $qrows['FILE_NAME'];?>');"><i class="fa fa-times " style="color:red;"></i></a></td>
                                            </tr>
                                        <?php $i++;}
                                        }?>
                                    </tbody>
                               
                              </table>
                            </div>
                         </div>
                      </div>
                   </div>
               
               <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             Open Vessel Position (<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?>) : Attachments
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
               
               <div class="row">
                      <div class="col-xs-12">
                         <div class="box box-primary">
						    <div class="box-body no-padding">
							  <table class="table table-striped">
							     <thead>
                                  <tr>
                                    <th width="40%" align="left" valign="middle">File Name</th>
		                            <th width="40%" align="center" valign="middle">Upload</th>
                                  </tr>	  
                                  
                                </thead>
                                <tbody>
                                 
                                    <?php if($obj->getFun15() != " ")
										{
										$attachments = explode(",",$obj->getFun15());
										for($m=0;$m<count($attachments);$m++)
											{
												echo "<tr>";
												echo '<td align="left" valign="middle">'.$attachments[$m].'</td>';
												echo '<td align="center" valign="middle">';
												echo '<a href="../../attachment/'.$attachments[$m].'" target="_blank" >'.$attachments[$m].'</a>';
												echo '</td>';
												echo '</tr>';
											}
										}
										else
										{
										}
                                   ?>
                                    </tbody>
                               
                              </table>
                            </div>
                         </div>
                      </div>
                   </div>
                   
                   
                   
                   
                   
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             Nomination Remarks
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
               
               <div class="row">
                      <div class="col-xs-12">
                         <div class="box box-primary">
						    <div class="box-body no-padding">
							  <table class="table table-striped">
							     <thead>
                                  <tr>
                                    <th width="2%" align="center">#</th>
		                            <th width="80%" align="left">Remarks</th>
                                  </tr>	  
                                  
                                </thead>
                                <tbody>
                                 
                                   <?php if($obj->getFun7() != " ")
										{
												echo "<tr>";
												echo '<td align="center" 1.</td>';
												echo '<td align="left">'.$obj->getFun7().'</td>';
												echo '</tr>';
										}
										else
										{
												echo "<tr>";
												echo '<td align="center" colspan="2" valign="middle" style="color:#ff0000;letter-spacing:1px;">'.strtoupper("Sorry Currently there are zero(0) records").'</td>';
												echo '</tr>';
										}
										?>
                                    </tbody>
                               
                              </table>
                            </div>
                         </div>
                      </div>
                   </div>
                   
                   
                   
              
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
$("#frm1").validate({
rules: {
	txtFile:"required"
	},
messages: {	
	txtFile:"*"
	}
});
});


function getValidate2(file_name)
{
	jConfirm('Are you sure to remove this document permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtStatus").val(2);
			$("#txtFileName").val(file_name);
			document.frm1.submit();
		}
		else{return false;}
		});	
}

</script>
    </body>
</html>