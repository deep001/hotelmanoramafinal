<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
 
$pagename = basename($_SERVER['PHP_SELF']);
?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}

</style>
</head>
	<div id="basic-modal-content" style="display:none;" align="center">
	<table cellpadding="1" cellspacing="4" border="0" align="center">
	<tr><td align="center"><img src="../../img/loading.gif" /><br /></td></tr>
	</table>
	</div>
	<body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(8); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-leaf"></i>&nbsp;Voyage Estimate&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Freight Cost Estimates</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="fce.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				     <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Voyage Financials Estimates Comparison
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-10 invoice-col">
                            <address>
                              <select data-placeholder="Choose a Voyage Financials here..." class="chzn-select" style="width:800px;" multiple name="selVFEstimate[]" id="selVFEstimate">
									<?php $obj->getVoyageFinancialsEstimatesList(); ?>
								</select>
								
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-2">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getData();">Compare</button>
						</div>
                   </div>
                  
					<div id="VFE_Compare_report" align="center" style="width:100%;" >
	
					</div>
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/chosen.css" rel="stylesheet" type="text/css" />
<script src="../../js/chosen.jquery.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#selVFEstimate").chosen(); 
$(".areasize").autosize({append: "\n"});
});

function getData()
{
	if($("#selVFEstimate").val() != null)
	{
		$("#VFE_Compare_report").html("");
		$("#VFE_Compare_report").css({"text-align":""});
		$("#VFE_Compare_report").html('<img src="../../img/loading.gif" />');
		$.post("options.php?id=33",{selVFEstimate:""+$("#selVFEstimate").val()+""}, function(html) {
		$("#VFE_Compare_report").html("");
		$("#VFE_Compare_report").css({"text-align":"left"});
		$("#VFE_Compare_report").html(html);
		});
		return false;
	}
	else
	{
		jAlert('Please select Voyage Financials Estimates first.', 'Alert');
		return false;
	}
}

function getPdf()
{
	var value = $("#selVFEstimate").val();
	var value1 = value.toString();
    var words = value1.split(",");
  
  if(words.length > 4){
        jConfirm('PDF will be generated for first 4 ESTIMATES only.', 'Confirmation', function(r) {
			if(r){
        			location.href='allPdf.php?id=39&selVFEstimate='+$("#selVFEstimate").val();
				 }
			else{return false;}
			});
    }
	else
	{
		location.href='allPdf.php?id=39&selVFEstimate='+$("#selVFEstimate").val();
	}
	
}

</script>
    </body>
</html>