<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "vessel_in_post_ops.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "vessel_in_history.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "coa_in_ops_at_glance.php"; $page_bar="In Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else if($page == 5){$page_link = "coa_in_post_ops.php"; $page_bar="In Post Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else{$page_link = "coa_in_history.php"; $page_bar="Vessels in History - COA";$nav=16;$subtitle = 'COA';}

if (@$_REQUEST['action'] == 'submit')
 {	
 	$msg = $obj->updationSOFRecords();
	if($_REQUEST['txtsubmitid_'.$_REQUEST['txttabid'][0]] == 1)
	{
		header('Location:./sof.php?msg='.$msg.'&tabs='.($_REQUEST['txttabid'][0]-1).'&comid='.$_REQUEST['comid'].'&page='.$page);
	}
	else
	{
		header('Location:./'.$page_link.'?msg=3');
	}
 }
  
if(isset($_REQUEST['tabs'])){$redirecttab = $_REQUEST['tabs'];}
else{$redirecttab = 1;}
$cost_sheet_id = $obj->getLatestCostSheetID($comid);
$arr  = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$cost_sheet_id);
$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);

$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;
$msg = NULL;
if(isset($_REQUEST['tab']))
{
$tab = $_REQUEST['tab'] + 1;
}
else{$tab = 0;}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
//$("#tabs").tabs({selected : <?php //echo $tab;?> });
$('[id^=txtNarration_],[id^=txtNotes],[id^=txtRemarks],[id^=txtSat_],[id^=txtSun_],[id^=txtHol_],[id^=txtRRemarks_],[id^=txtCRemarks_],[id^=txtMRemarks_]').autosize({append: "\n"});
$("[id^=txtBLQty_],[id^=txtQty_],[id^=txtVA_1_]").numeric();

$("[id^=txtBLDate_]").datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true		
});

$("[id^=txtVAPS_1_],[id^=txtNT_1_],[id^=txtNR_1_],[id^=txtNA_1_],[id^=txtAA_1_],[id^=txtPBFB_1_],[id^=txtVAF_],[id^=txtFPG_],[id^=txtCC_],[id^=txtIDSC_],[id^=txtIDSC1_],[id^=txtHIC_],[id^=txtHIC1_],[id^=txtHP_],[id^=txtOHSC_],[id^=txtOHSC1_],[id^=txtLC_],[id^=txtLC1_],[id^=txtFDSS_],[id^=txtFDSC_],[id^=txtCLAPDS_],[id^=txtDC1_],[id^=txtVS_]").datetimepicker({
	format: 'dd-mm-yyyy hh:ii', 
	autoclose: true,
	todayBtn: true,
	minuteStep: 1	
});

$("[id^=txtETA_],[id^=txtETCD12_],[id^=txtDStart],[id^=txtDEnd]").datetimepicker({
	format: 'dd-mm-yyyy hh:ii', 
	autoclose: true,
	todayBtn: true,
	minuteStep: 1	
});

});

/*$(function() {
	$( "[id^=sortable]" ).sortable();
    $( "[id^=sortable]" ).disableSelection();
});*/

function addUI_Row_1(var1)
{
	var id = $("#txtTID_"+var1).val();
	if($("#txtBLDate_"+var1+"_"+id).val() != "" && $("#txtBLQty_"+var1+"_"+id).val() != "")
	{
		id  = (id - 1 )+ 2;
		$('<tr id="row_bl_'+var1+'_'+id+'"><td align="center" valign="middle" ><input type="text" class="form-control" name="txtBLDate_'+var1+'_'+id+'" id="txtBLDate_'+var1+'_'+id+'" value="" autocomplete="off" /></td><td align="center" valign="middle" ><input type="text" class="form-control" name="txtCargo_'+var1+'_'+id+'" id="txtCargo_'+var1+'_'+id+'" value="" autocomplete="off" /></td><td align="center"  valign="middle"><input type="text" class="form-control" name="txtBLQty_'+var1+'_'+id+'" id="txtBLQty_'+var1+'_'+id+'" value="" autocomplete="off" /></td><td width="10%" align="center" valign="top"><a href="#1" onClick="Del_BLRow('+var1+','+id+');"><i class="fa fa-times " style="color:red;"></i></a></td></tr>').appendTo("#sort_"+var1);
		$("#txtTID_"+var1).val(id);
		
		$("[id^=txtBLDate_]").datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true		
		});
		
		$("[id^=txtBLQty_]").numeric();
	}
	else
	{
		jAlert('Please fill the above entries.', 'Alert');
	}
}

function Del_BLRow(var1,var2)
{
	jConfirm('Are you sure you want to delete this entry permanently?', 'Confirmation', function(r) {
		if(r)
		{
			$('#row_bl_'+var1+'_'+var2).remove();
		}
		else
		{
			return false;
		}
	});
	
}

function getString(var1)
{
	 var var2 = var1.split(' ');
	 var var3 = var2[0].split('-');
	 
	 return var3[2]+'/'+var3[1]+'/'+var3[0]+' '+var2[1];
}

function getTimeDiff(thisid)
{
	var rowid = thisid;
	var var1 = rowid.split('_')[1];
	var var2 = rowid.split('_')[2];
	var var3 = rowid.split('_')[0].substring(9,11);
	
	if($('#txtDEnd'+var3+'_'+var1+'_'+var2).val() != '' &&  $('#txtDStart'+var3+'_'+var1+'_'+var2).val() != '')
	{
	var end_actual_time  = getString($('#txtDEnd'+var3+'_'+var1+'_'+var2).val());
	var start_actual_time    =  getString($('#txtDStart'+var3+'_'+var1+'_'+var2).val());

	start_actual_time = new Date(start_actual_time);
	end_actual_time = new Date(end_actual_time);

	var diff = end_actual_time - start_actual_time;

	var diffSeconds = diff/1000;
	var HH = Math.floor(diffSeconds/3600);
	
	var MM = Math.floor(diffSeconds%3600)/60/60;
	var HH = parseFloat(HH)+parseFloat(MM);
	var HH = HH.toFixed(4);
	$('#txtDiff'+var3+'_'+var1+'_'+var2).val(HH);
	}
	else
	{
		$('#txtDiff'+var3+'_'+var1+'_'+var2).val("0.0");
	}
}

function getTimeDiff1(thisid)
{
	var rowid = thisid;
	var var1 = rowid.split('_')[1];
	var var2 = rowid.split('_')[2];
	var var3 = rowid.split('_')[0].substring(7,9);
	
	if($('#txtDEnd'+var3+'_'+var1+'_'+var2).val() != '' &&  $('#txtDStart'+var3+'_'+var1+'_'+var2).val() != '')
	{
	var end_actual_time  = getString($('#txtDEnd'+var3+'_'+var1+'_'+var2).val());
	var start_actual_time    =  getString($('#txtDStart'+var3+'_'+var1+'_'+var2).val());

	start_actual_time = new Date(start_actual_time);
	end_actual_time = new Date(end_actual_time);

	var diff = end_actual_time - start_actual_time;

	var diffSeconds = diff/1000;
	var HH = Math.floor(diffSeconds/3600);
	
	var MM = Math.floor(diffSeconds%3600)/60/60;
	var HH = parseFloat(HH)+parseFloat(MM);
	var HH = HH.toFixed(4);
	$('#txtDiff'+var3+'_'+var1+'_'+var2).val(HH);
	}
	else
	{
		$('#txtDiff'+var3+'_'+var1+'_'+var2).val("0.0");
	}
}

function Del_Row(var1,var2,var3)
{
	jConfirm('Are you sure you want to delete this entry permanently?', 'Confirmation', function(r) {
		if(r)
		{
			$('#txtROW_ID'+var3+'_'+var1+'_'+var2).val(0);
			$('#row'+var3+'_'+var1+'_'+var2).hide();
		}
		else
		{
			return false;
		}
	});
}

function addUI_Row_2(var1)
{
	var id = $("#txtCID_"+var1).val();
	var id1 = $("#txtCID1_"+var1).val();
	
	if(id == 0)
	{
		id  = (id - 1 )+ 2;
		id1  = (id1 - 1 )+ 2;
		
		$('<tr><td width="2%" align="left" valign="top">'+id1+'.</td><td width="48%" align="left" valign="top"><input type="text" name="txtAV_Name_'+var1+'_'+id+'" id="txtAV_Name_'+var1+'_'+id+'" class="form-control" autocomplete="off" value="" /></td><td width="50%" align="left" valign="top"><input type="text" name="txtAV_Value_'+var1+'_'+id+'" id="txtAV_Value_'+var1+'_'+id+'" class="form-control" autocomplete="off" value="" /></td></tr>').appendTo("#sort1_"+var1);
		$("#txtCID_"+var1).val(id);
		$("#txtCID1_"+var1).val(id1);
		
		for(i=0;i<18;i++)
		{ id1  = (id1 - 1 )+ 2; $('#id_'+i).text(id1+'.');}
	}
	else
	{
		if($("#txtAV_Name_"+var1+"_"+id).val() != "" && $("#txtAV_Value_"+var1+"_"+id).val() != "")
		{
			id  = (id - 1 )+ 2;
			id1  = (id1 - 1 )+ 2;
			
			$('<tr><td width="2%" align="left" valign="top">'+id1+'.</td><td width="48%" align="left" valign="top"><input type="text" name="txtAV_Name_'+var1+'_'+id+'" id="txtAV_Name_'+var1+'_'+id+'" class="form-control" autocomplete="off" value="" /></td><td width="50%" align="left" valign="top"><input type="text" name="txtAV_Value_'+var1+'_'+id+'" id="txtAV_Value_'+var1+'_'+id+'" class="form-control" autocomplete="off" value="" /></td></tr>').appendTo("#sort1_"+var1);
			$("#txtCID_"+var1).val(id);
			$("#txtCID1_"+var1).val(id1);
			
			for(i=0;i<18;i++)
			{ id1  = (id1 - 1 )+ 2; $('#id_'+i).text(id1+'.');}
		}
		else
		{
			jAlert('Please fill the above entries.', 'Alert');
		}
	}	
}

function getSubmit(tabid,submitid,port,portid,ramdomid)

{
	var var3 = $("[id^=fileup_]").map(function () {return this.value;}).get().join(",");
 	$('#txtCRMFILE2').val(var3);
	for(i=1;i<12;i++)
	{
		var var1 = $("[id^=txtROW_ID"+i+"_"+tabid+"]").map(function () {return this.value;}).get().join(",");
		$('#txtCRM'+i+'_'+tabid).val(var1);
	}
	
	var var2 = parseFloat($("[id^=txtQty_"+tabid+"]").sum()).toFixed(2);
	$('#txtTTLQty_'+tabid).val(var2);
	
	var var3 = parseFloat($("[id^=txtBLQty_"+tabid+"]").sum()).toFixed(2);
	$('#txtTTLBLQty_'+tabid).val(var3);
	
	$("[id^=txttabid_],[id^=txtsubmitid_],[id^=txtport_],[id^=txtportid_]").val("");
	$("#txttabid_"+tabid).val(tabid);
	$("#txtsubmitid_"+tabid).val(submitid);
	$("#txtport_"+tabid).val(port);
	$("#txtportid_"+tabid).val(portid);
	$("#txtRandomID_"+tabid).val(ramdomid);
	
	if(submitid == 2)
	{
		if($('#txtVA_1_'+tabid).val() != '' && $('#txtVAPS_1_'+tabid).val() != '' && $('#txtNT_1_'+tabid).val() != '' && $('#txtLC_'+tabid).val() != '' && $('#txtLC1_'+tabid).val() != '' && $('#txtVS_'+tabid).val() != '')
		{
			jConfirm('Are you sure all entries prior to sailing are made?', 'Confirmation', function(r) {
			if(r){
					document.getElementById("frm"+tabid).submit();
				}
				else
				{
					return false;
				}
				});
		}
		else
		{
			jAlert('Please fill the STOWAGE PLAN QUANTITY, VESSEL ARRIVED, NOR TENDERED, LOAD/DISCH COMMENCED, LOAD/DISCH COMPLETED & VESSEL SAILED.', 'Alert');
		}
	}
	else
	{
		if($('#txtVA_1_'+tabid).val() != '' && $('#txtVAPS_1_'+tabid).val() != '' && $('#txtNT_1_'+tabid).val() != '' && $('#txtLC_'+tabid).val() != '' && $('#txtLC1_'+tabid).val() != '' && $('#txtVS_'+tabid).val() != '')
		{
			document.getElementById("frm"+tabid).submit();
		}
		else
		{
			jAlert('Please fill the STOWAGE PLAN QUANTITY, VESSEL ARRIVED, NOR TENDERED, LOAD/DISCH COMMENCED, LOAD/DISCH COMPLETED & VESSEL SAILED.', 'Alert');
		}
	}
}


function addUI_Row_AIP(var1,var2)
{
	var id = idd = $("#txtTBID"+var2+"_"+var1).val();
	
	id  = (id - 1 )+ 2;
	$('<tr id="row'+var2+'_'+var1+'_'+id+'"><td align="left" valign="top" style="cursor:move;"><span  class="handle"><i class="fa fa-ellipsis-v"></i>&nbsp;<i class="fa fa-ellipsis-v"></i></span></td><td align="left" valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtActivity'+var2+'_'+var1+'_'+id+'" id="txtActivity'+var2+'_'+var1+'_'+id+'" value="" autocomplete="off" /></td><td align="left" valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtDStart'+var2+'_'+var1+'_'+id+'" id="txtDStart'+var2+'_'+var1+'_'+id+'" onChange="getTimeDiff(this.id);" value="" /></td><td align="left" valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtDEnd'+var2+'_'+var1+'_'+id+'" id="txtDEnd'+var2+'_'+var1+'_'+id+'" onChange="getTimeDiff1(this.id);" value="" /></td><td align="left" valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtDiff'+var2+'_'+var1+'_'+id+'" id="txtDiff'+var2+'_'+var1+'_'+id+'" value="" /></td><td align="left" valign="middle" style=" cursor:move;"><textarea class="form-control" name="txtNotes'+var2+'_'+var1+'_'+id+'" id="txtNotes'+var2+'_'+var1+'_'+id+'"></textarea></td><td align="center"  valign="middle" style="cursor:move;"><a href="#1" onclick="Del_Row('+var1+','+id+','+var2+');"><i class="fa fa-times " style="color:red;"></i></a><input type="hidden" class="input" name="txtROW_ID'+var2+'_'+var1+'_'+id+'" id="txtROW_ID'+var2+'_'+var1+'_'+id+'" value="'+id+'"/></td></tr>').appendTo("#sortable"+var2+"_"+var1);
	$("#txtTBID"+var2+"_"+var1).val(id);
	
	$('[id^=txtNotes]').autosize({append: "\n"});
	
	$("[id^=txtDStart],[id^=txtDEnd]").datetimepicker({
	format: 'dd-mm-yyyy hh:ii', 
	autoclose: true,
	todayBtn: true,
	minuteStep: 1	
	});
	$("#txtDStart"+var2+"_"+var1+"_"+id).val($("#txtDEnd"+var2+"_"+var1+"_"+idd).val());
}

function openWin()
{
	$('#basic-modal').modal();
}

function Del_Upload(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently?', 'Confirmation', function(r) {
	if(r){
			$('#row_file_'+var1).remove();		
		}
		else
		{
			return false;
		}
	});
}

</script>

<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}
form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?></li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content invoice">
					<!--   content put here..................-->
					<?php 
							if(isset($_REQUEST['msg'])){
								$msg = $_REQUEST['msg'];
								if($msg == 0){?>
									<div class="alert alert-success alert-dismissable">
										<i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<b>Congratulations!</b> SOF added/updated successfully.
									</div>
								<?php }?>
								<?php if($msg == 1){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Sorry!</b> this SOF is already exists for this port.
								</div>
								<?php }?>
						<?php }?>
						<div align="right"><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
						<div style="height:10px;">&nbsp;</div>
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header" style="text-align:center;">
								 SOF
								</h2>                            
							</div><!-- /.col -->
						</div>			
						<div class="row invoice-info">
							<div class="col-md-12">
								<!-- Custom Tabs -->
								<div id="tabs" class="nav-tabs-custom">
									<ul class="nav nav-tabs">
									<?php for($i=0;$i<count($arr);$i++){
										$arr_explode = explode('@@',$arr[$i]);
										$port_name = $obj->getPortNameBasedOnID($arr_explode[1]);
										?>
										<li class="<?php if($redirecttab == $i+1){ echo 'active';} ?>"><a href="#tabs_<?php echo $i+1;?>" data-toggle="tab"><?php echo $arr_explode[0]."-".$port_name;?></a></li>
									<?php }?>
									</ul>
									<div class="tab-content">
									
									<!----------------------------------------------- Start 1------------------------------------------------->
									<?php for($i=0;$i<count($arr);$i++){ 
									$arr_explode = explode('@@',$arr[$i]);?>
									<div id="tabs_<?php echo $i+1;?>" class="tab-pane <?php if($redirecttab == $i+1){ echo 'active';} ?>">
									<form name="frm<?php echo $i+1;?>" id="frm<?php echo $i+1;?>" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>
									<input type="hidden" name="txttabid[]" id="txttabid_<?php echo $i+1;?>" class="form-control" readonly value="" />
								    <input type="hidden" name="txtsubmitid_<?php echo $i+1;?>" id="txtsubmitid_<?php echo $i+1;?>" class="form-control" readonly value="" />
								    <input type="hidden" name="txtport_<?php echo $i+1;?>" id="txtport_<?php echo $i+1;?>" class="form-control" readonly value="" />
								    <input type="hidden" name="txtportid_<?php echo $i+1;?>" id="txtportid_<?php echo $i+1;?>" class="form-control" readonly value="" />
                                    <input type="hidden" name="txtRandomID_<?php echo $i+1;?>" id="txtRandomID_<?php echo $i+1;?>" class="form-control" readonly value="" />
                                    
									<div class="row invoice-info">
										<div class="col-sm-4 invoice-col">
											<address>
												&nbsp;
											</address>
										</div>
										<div class="col-sm-4 invoice-col">&nbsp;&nbsp;</div>
                                        <div class="col-sm-4 invoice-col">
										    <a href="allPdf.php?id=15&comid=<?php echo $comid; ?>&port=<?php echo $arr_explode[0]; ?>&portid=<?php echo $arr_explode[1]; ?>&randomid=<?php echo $arr_explode[2]; ?>" title="CHeck List Pdf" style="float:right;">
                                                <button class="btn btn-default" type="button" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                                            </a>
                                        </div>
									</div>
									
									<div class="box-body table-responsive">
									<div>
									<table class="table table-bordered dataTable">
									<tbody id="sort1_<?php echo $i+1;?>">
									<tr>
										<td width="2%" align="left" valign="top">1.</td>
										<td width="48%" align="left" valign="top">NAME OF VESSEL</td>
										<td width="50%" align="left" valign="top"><?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?><input type="hidden" name="txtportSerialid_<?php echo $i+1;?>" id="txtportSerialid_<?php echo $i+1;?>" class="form-control" readonly value="<?php echo $i+1;?>" /></td>
									</tr>
									<tr>
									<td width="2%" align="left" valign="top">2.</td>
									<td width="48%" align="left" valign="top">BUILT</td>
									<td width="50%" align="left" valign="top"><input type="text" name="txtVAPS_<?php echo $i+1;?>" id="txtVAPS_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"YEARBUILT");?>" readonly/></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">3.</td>
										<td width="48%" align="left" valign="top">GRT/NRT</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtVA_<?php echo $i+1;?>" id="txtVA_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"GRT_NRT").'/'.$obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"NRT");?>" readonly/></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">4.</td>
										<td width="48%" align="left" valign="top">FLAG</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtNT_<?php echo $i+1;?>" id="txtNT_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getCountryNameBasedOnID($obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"FLAG"));?>" readonly/></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">5.</td>
										<td width="48%" align="left" valign="top">DWT(s)</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtNR_<?php echo $i+1;?>" id="txtNR_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"DWT");?>" readonly/></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">6.</td>
										<td width="48%" align="left" valign="top">LOA/BEAM</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtNA_<?php echo $i+1;?>" id="txtNA_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"LOA");?>" readonly/></td>
									</tr>
									
									<?php if($obj->getCompareEstimateData($comid,"ESTIMATE_TYPE")!=2){?>
                                    <tr>
                                        <td width="2%" align="left" valign="top">7.</td>
                                        <td width="48%" align="left" valign="top">GEAR/GRABS</td>
                                        <td width="50%" align="left" valign="top"><input type="text" name="txtAA_<?php echo $i+1;?>" id="txtAA_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"CARGO_GEAR");?>" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td width="2%" align="left" valign="top">8.</td>
                                        <td width="48%" align="left" valign="top">HATCH/HOLD</td>
                                        <td width="50%" align="left" valign="top"><input type="text" name="txtPBFB_<?php echo $i+1;?>" id="txtPBFB_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"HATCH_SIZE");?>" readonly/></td>
                                    </tr>
                                    <?php }else{?>
                                    <tr>
                                        <td width="2%" align="left" valign="top">7.</td>
                                        <td width="48%" align="left" valign="top">NO. OF CARGO PUMP(Main)</td>
                                        <td width="50%" align="left" valign="top"><input type="text" name="txtAA_<?php echo $i+1;?>" id="txtAA_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"TANKER_CARGO_PUMP");?>" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td width="2%" align="left" valign="top">8.</td>
                                        <td width="48%" align="left" valign="top">CARGO PUMP MAIN CAP(CBM/Hr)</td>
                                        <td width="50%" align="left" valign="top"><input type="text" name="txtPBFB_<?php echo $i+1;?>" id="txtPBFB_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"TANKER_PUMP_MAINCAP");?>" readonly/></td>
                                    </tr>	
                                   <?php }?>	
									
									<tr>
										<td width="2%" height="20" align="left" valign="top" colspan="3"></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">9.</td>
										<td width="48%" align="left" valign="top">PORT/TERMINAL/BERTH/ANCHORAGE</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtTerminal_<?php echo $i+1;?>" id="txtTerminal_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"TERMINAL");?>" /></td>
									</tr>
									
									<tr>
										<td width="2%" height="20" align="left" valign="top" colspan="3"></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">10.</td>
										<td width="48%" align="left" valign="top">STOWAGE PLAN QUANTITY (MT)</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtVA_1_<?php echo $i+1;?>" id="txtVA_1_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"VA_1");?>" onClick="Remove_Date(<?php echo $i+1;?>,'VA_1')"/></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">11.</td>
										<td width="48%" align="left" valign="top">VESSEL ARRIVED</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtVAPS_1_<?php echo $i+1;?>" id="txtVAPS_1_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"VAPS_1");?>"  onclick="Remove_Date(<?php echo $i+1;?>,'VAPS_1')"/></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">12.</td>
										<td width="48%" align="left" valign="top">NOR TENDERED</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtNT_1_<?php echo $i+1;?>" id="txtNT_1_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"NT_1");?>"  onclick="Remove_Date(<?php echo $i+1;?>,'NT_1')"/></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">13.</td>
										<td width="48%" align="left" valign="top">PILOT ON BOARD</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtPBFB_1_<?php echo $i+1;?>" id="txtPBFB_1_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"PBFB_1");?>"  onclick="Remove_Date(<?php echo $i+1;?>,'PBFB_1')"/></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">14.</td>
										<td width="48%" align="left" valign="top">LOAD/DISCH COMMENCED</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtLC_<?php echo $i+1;?>" id="txtLC_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"LC");?>" onClick="Remove_Date(<?php echo $i+1;?>,'LC')"/></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">15.</td>
										<td width="48%" align="left" valign="top">LOAD/DISCH COMPLETED</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtLC1_<?php echo $i+1;?>" id="txtLC1_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"LC1");?>"  onclick="Remove_Date(<?php echo $i+1;?>,'LC1')"/></td>
									</tr>
									
									<tr>
										<td width="2%" align="left" valign="top">16.</td>
										<td width="48%" align="left" valign="top">VESSEL SAILED</td>
										<td width="50%" align="left" valign="top"><input type="text" name="txtVS_<?php echo $i+1;?>" id="txtVS_<?php echo $i+1;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"VS");?>" onClick="Remove_Date(<?php echo $i+1;?>,'VS')"/></td>
									</tr>
									
									<?php
									$k = 16; $j = 0;
									
									$sql = "select * from sof_master where COMID='".$comid."' and LOGIN='INTERNAL_USER' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_explode[0]."' and RANDOMID='".$arr_explode[2]."' and PORTID='".$arr_explode[1]."'";
									$res = mysql_query($sql) or die($sql);
									$rec = mysql_num_rows($res);
									
									if($rec == 0)
									{
										$sql = "select * from sof_master where COMID='".$comid."' and LOGIN='AGENT' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_explode[0]."' and RANDOMID='".$arr_explode[2]."' and PORTID='".$arr_explode[1]."' and SUBMITID='2'";
										$res = mysql_query($sql) or die($sql);
										$rows = mysql_fetch_assoc($res);
									}
									else
									{
										$rows = mysql_fetch_assoc($res);
									}
									
									$sql1 = "select * from sof_slave_3 where SOFID='".$rows['SOFID']."'";
									$res1 = mysql_query($sql1);
									$rec1 = mysql_num_rows($res1);
									if($rec1 > 0)
									{ 
										while($rows1 = mysql_fetch_assoc($res1))
										{ $k = $k+1; $j = $j+1;
									?>
									<tr>
									<td width="2%" align="left" valign="top"><?php echo $k;?>.</td>
									<td width="48%" align="left" valign="top"><input type="text" name="txtAV_Name_<?php echo $i+1;?>_<?php echo $j;?>" id="txtAV_Name_<?php echo $i+1;?>_<?php echo $j;?>" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_NAME']; ?>" /></td>
									<td width="50%" align="left" valign="top"><input type="text" name="txtAV_Value_<?php echo $i+1;?>_<?php echo $j;?>" id="txtAV_Value_<?php echo $i+1;?>_<?php echo $j;?>" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE']; ?>" /></td>
									</tr>
									<?php }}else{$k = 16; $j = 0;}?>
									
									</tbody>
									
									<tr>
										<td colspan="6" align="left" valign="middle"><button type="button" onClick="addUI_Row_2(<?php echo $i+1;?>);" class="btn btn-primary btn-flat">Add</button><input type="hidden" class="form-control" name="txtCID_<?php echo $i+1;?>" id="txtCID_<?php echo $i+1;?>" value="<?php echo $j;?>" size="5" /><input type="hidden" class="form-control" name="txtCID1_<?php echo $i+1;?>" id="txtCID1_<?php echo $i+1;?>" value="<?php echo $k;?>" size="5" /></td>
									</tr>
									
									</table>
									</div>
									</div>
									
									<div class="box-body table-responsive">
									
									<table class="table table-bordered dataTable">
										<thead>
										<tr>
										<th width="30%" align="left" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;"  colspan="4"><span id="id_0"><?php $k = $k+1; echo $k;?>.</span></th>
										</tr>
										
										<tr>
										<th width="30%" align="center" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;" >BL Date</th>
                                        <th width="30%" align="center" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;" >Cargo</th>
										<th width="30%" align="center" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;" >BL Qty(MT)</th>
                                        <th width="10%" align="center" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;" ></th>
										</tr>
										</thead>
										
										<tbody id="sort_<?php echo $i+1;?>">
										<?php
										$j = 0;
										$sql2 = "select * from sof_slave_1 where SOFID='".$rows['SOFID']."'";
										$res2 = mysql_query($sql2);
										$rec2 = mysql_num_rows($res2);
										if($rec2 == 0)
										{ $j = $j+1;
										?>
										<tr id="row_bl_<?php echo $i+1;?>_1">
											<td width="40%" align="center" valign="top"><input type="text" name="txtBLDate_<?php echo $i+1;?>_1" id="txtBLDate_<?php echo $i+1;?>_1" class="form-control"  value="" /></td>
                                            <td width="30%" align="center" valign="top"><input type="text" name="txtCargo_<?php echo $i+1;?>_1" id="txtCargo_<?php echo $i+1;?>_1" class="form-control"  value="" /></td>
											<td width="55%" align="center" valign="top"><input type="text" name="txtBLQty_<?php echo $i+1;?>_1" id="txtBLQty_<?php echo $i+1;?>_1" class="form-control" autocomplete="off" value="" /></td>
                                            <td width="10%" align="center" valign="top"><a href="#1" onClick="Del_BLRow(<?php echo $i+1;?>,1);"><i class="fa fa-times " style="color:red;"></i></a></td>
										</tr>
										<?php }else{while($rows2 = mysql_fetch_assoc($res2)){ $j = $j+1;?>
										<tr id="row_bl_<?php echo $i+1;?>_<?php echo $j;?>">
											<td width="40%" align="center" valign="top"><input type="text" name="txtBLDate_<?php echo $i+1;?>_<?php echo $j;?>" id="txtBLDate_<?php echo $i+1;?>_<?php echo $j;?>" class="form-control"  value="<?php echo  date("d-M-Y",strtotime($rows2['BL_DATE']));?>" /></td>
                                            <td width="30%" align="center" valign="top"><input type="text" name="txtCargo_<?php echo $i+1;?>_<?php echo $j;?>" id="txtCargo_<?php echo $i+1;?>_<?php echo $j;?>" class="form-control"  value="<?php echo  $rows2['CARGO'];?>" /></td>
											<td width="55%" align="center"  valign="top"><input type="text" name="txtBLQty_<?php echo $i+1;?>_<?php echo $j;?>" id="txtBLQty_<?php echo $i+1;?>_<?php echo $j;?>" class="form-control" autocomplete="off" value="<?php echo  $rows2['BL_QTY'];?>" /></td>
                                            <td width="10%" align="center" valign="top"><a href="#1" onClick="Del_BLRow(<?php echo $i+1;?>,<?php echo $j;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
										</tr>
										<?php }}?>
										</tbody>
											
										<tr>
											<td colspan="6" align="left" valign="middle"><button type="button" onClick="addUI_Row_1(<?php echo $i+1;?>);" class="btn btn-primary btn-flat">Add</button><input type="hidden" class="input" name="txtTID_<?php echo $i+1;?>" id="txtTID_<?php echo $i+1;?>" value="<?php echo $j;?>" size="5" /></td>
										</tr>
									
									</table><div>
									</div>
									</div>
									
									<div class="box-body table-responsive">
									<div style="overflow:auto;">
									<table class="table table-bordered ">
										<thead>
										<tr>
											<th width="25%" align="left" style="background-color:#3c8dbc;font-size:12px; color:#fff;" colspan="8"><span id="id_1"><?php $k = $k+1; echo $k;?>.&nbsp;The Lines within this block can be interchanged by drag and drop. Please see instructions for filling.</span></th>
										</tr>
											
										<tr>
											<th width="5%" align="left" valign="top" style="background-color:#3c8dbc;font-size:12px; color:#fff;"  >#</th>
											<th width="20%" align="left" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;"  >ACTIVITY IN PORT</th>
											<th width="19%" align="center" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;"  >FROM</th>
											<th width="19%" align="center" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;" >TO<br />(leave blank if no "To" timings available)</th>
											<th width="15%" align="left" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;"  >DURATION</th>
											<th width="20%" align="left" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;"  >REMARKS</th>
											<th width="3%" align="left" valign="middle" style="background-color:#3c8dbc;font-size:12px; color:#fff;" ></th>
										</tr>
										</thead>
										
										<tbody id="sortable11_<?php echo $i+1;?>" class="todo-list">
										<?php
										$j = 0;
										$sql13 = "select * from sof_slave where SOFID='".$rows['SOFID']."' and GROUP_NAME='1'";
										$res13 = mysql_query($sql13);
										$rec13 = mysql_num_rows($res13);
										if($rec13 == 0)
										{ $j = $j+1;
										?>
										<tr id="row11_<?php echo $i+1;?>_1">
											<td align="left" valign="top" style="cursor:move;">
												<span class="handle">
													<i class="fa fa-ellipsis-v"></i>
													<i class="fa fa-ellipsis-v"></i>
												</span> 
											</td>
											<td align="left" valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtActivity11_<?php echo $i+1;?>_1" id="txtActivity11_<?php echo $i+1;?>_1" value="" autocomplete="off" /></td>
											<td align="left"  valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtDStart11_<?php echo $i+1;?>_1" id="txtDStart11_<?php echo $i+1;?>_1" onChange="getTimeDiff(this.id);" value="" /></td>
											<td align="left"  valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtDEnd11_<?php echo $i+1;?>_1" id="txtDEnd11_<?php echo $i+1;?>_1" onChange="getTimeDiff1(this.id);" value=""/></td>
											<td align="left"  valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtDiff11_<?php echo $i+1;?>_1" id="txtDiff11_<?php echo $i+1;?>_1" value="" /></td>
											<td align="left" valign="middle" style="cursor:move;"><textarea class="form-control" name="txtNotes11_<?php echo $i+1;?>_1" id="txtNotes11_<?php echo $i+1;?>_1" ></textarea></td>
											<td align="center" valign="middle" style="cursor:move;"><a href="#1" onClick="Del_Row(<?php echo $i+1;?>,1,11);"><i class="fa fa-times " style="color:red;"></i></a><input type="hidden" class="input" name="txtROW_ID11_<?php echo $i+1;?>_1" id="txtROW_ID11_<?php echo $i+1;?>_1" value="1" /></td>
										</tr>
										<?php }else{while($rows13 = mysql_fetch_assoc($res13)){ $j = $j+1;
										if(date("d-M-Y",strtotime($rows13['START_DATETIME'])) == '01-Jan-1970') {$s_date = "";} else {$s_date = date("d-M-Y H:i",strtotime($rows13['START_DATETIME']));}
										if(date("d-M-Y",strtotime($rows13['FINISH_DATETIME'])) == '01-Jan-1970') {$f_date = "";} else {$f_date = date("d-M-Y H:i",strtotime($rows13['FINISH_DATETIME']));}
										?>
										
										<tr id="row11_<?php echo $i+1;?>_<?php echo $j;?>">
											<td align="left" valign="top" style="cursor:move;">
												<span class="handle">
													<i class="fa fa-ellipsis-v"></i>
													<i class="fa fa-ellipsis-v"></i>
												</span> 
											</td>
											<td align="left" valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtActivity11_<?php echo $i+1;?>_<?php echo $j;?>" id="txtActivity11_<?php echo $i+1;?>_<?php echo $j;?>" value="<?php echo $rows13['ACTIVITYID'];?>" autocomplete="off" /></td>
											<td align="left" valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtDStart11_<?php echo $i+1;?>_<?php echo $j;?>" id="txtDStart11_<?php echo $i+1;?>_<?php echo $j;?>" onChange="getTimeDiff(this.id);" value="<?php echo $s_date;?>" /></td>
											<td align="left" valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtDEnd11_<?php echo $i+1;?>_<?php echo $j;?>" id="txtDEnd11_<?php echo $i+1;?>_<?php echo $j;?>" onChange="getTimeDiff1(this.id);" value="<?php echo $f_date;?>" /></td>
											<td align="left" valign="middle" style="cursor:move;"><input type="text" class="form-control" name="txtDiff11_<?php echo $i+1;?>_<?php echo $j;?>" id="txtDiff11_<?php echo $i+1;?>_<?php echo $j;?>" value="<?php echo $rows13['DURATION'];?>"/></td>
											<td align="left" valign="middle" style="cursor:move;"><textarea class="form-control" name="txtNotes11_<?php echo $i+1;?>_<?php echo $j;?>" id="txtNotes11_<?php echo $i+1;?>_<?php echo $j;?>" ><?php echo $rows13['NOTES'];?></textarea></td>
											<td align="center" valign="middle" style="cursor:move;"><a href="#1" onClick="Del_Row(<?php echo $i+1;?>,1,11);"><i class="fa fa-times " style="color:red;"></i></a><input type="hidden" class="input" name="txtROW_ID11_<?php echo $i+1;?>_<?php echo $j;?>" id="txtROW_ID11_<?php echo $i+1;?>_<?php echo $j;?>" value="<?php echo $j;?>" /></td>
										</tr>
										<?php }}?>
										</tbody>
											
										<tr>
											<td colspan="7" align="left" valign="middle" ><button type="button" onClick="addUI_Row_AIP(<?php echo $i+1;?>,11);"class="btn btn-primary btn-flat">Add</button><input type="hidden" class="input" name="txtTBID11_<?php echo $i+1;?>" id="txtTBID11_<?php echo $i+1;?>" value="<?php echo $j;?>" size="5" /><input name="txtCRM11_<?php echo $i+1;?>" type="hidden" id="txtCRM11_<?php echo $i+1;?>" size="100000"/></td>
										</tr>
									
									</table>
									</div>
									</div>
									
									<div class="box-body table-responsive">
									<div>
									<table class="table table-bordered dataTable">
									<tr>
										<td width="29%" align="left" valign="top"><span id="id_2"><?php $k = $k+1; echo $k;?>.</span>AGENT'S REMARKS (If Any)</td>
										<td width="70%" align="left" valign="top"><textarea class="form-control" name="txtRRemarks_<?php echo $i+1;?>" id="txtRRemarks_<?php echo $i+1;?>"><?php echo $obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"SHIPPER_REMARKS");?></textarea></td>
									</tr>
									</table>
									</div>
									</div>
									
									<div class="box-body table-responsive">
									<div>
									<table class="table table-bordered dataTable">
									
									<tr>
										<td width="29%" align="left" valign="top"><span id="id_3"><?php $k = $k+1; echo $k;?>.</span>&nbsp;&nbsp;DOCUMENT'S UPLOAD<br /><span style="padding-left:20px;">(Press CTRL+CLICK For Multiple)</span></td>
										<td width="70%" align="left" valign="top"><div class="col-sm-12 invoice-col">
										<!----------------- View data ----------------------->	
										<?php
										$upload = trim($obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"UPLOAD"));
										$upload_1 = array_filter(explode(",",$upload));
										if(sizeof($upload_1) > 0)
										{ 
										$g = 1;
										for($j=0;$j<sizeof($upload_1);$j++)
										{
										?>
										<div id="row_file_<?php echo $g;?>">
										<a href="../../attachment/<?php echo $upload_1[$j]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="Click to View File"><i class="fa fa-external-link">&nbsp;&nbsp;<?php echo $upload_1[$j]; ?></i></a>&nbsp;&nbsp;
										<?php if($rows['LOGIN'] != 'AGENT'){?>
										<a href="#1" onClick="return Del_Upload(<?php echo $g;?>);"><input type="hidden" name="fileup_<?php echo $g;?>" id="fileup_<?php echo $g;?>" value="<?php echo $upload_1[$j]; ?>" /><i class="fa fa-times" style="color:red;"></i></a>
										<?php } ?>
										</div>
										<?php $g++;}} ?>
									<!----------------- first data ----------------------->	
									  <address>											
										   <div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Attachment">
												<i class="fa fa-paperclip"></i> Attachment
												<input type="file" class="form-control" name="mul_file[]" id="mul_file" multiple title=""><input name="txtSID_<?php echo $i+1;?>" type="hidden" id="txtSID_<?php echo $i+1;?>" value="<?php echo sizeof($arr1); ?>"/><input name="txtSOFID_<?php echo $i+1;?>" type="hidden" id="txtSOFID_<?php echo $i+1;?>" value="<?php echo $obj->getSOFdata($comid,$arr_explode[0],$arr_explode[1],$arr_explode[2],"SOFID"); ?>"/><input name="txtUPLOADID_<?php echo $i+1;?>" type="hidden" id="txtUPLOADID_<?php echo $i+1;?>" value=""/>
											</div><div class="tooltip fade top" style="top: -12px; left: 37.5px; display: block;"><div class="tooltip-arrow"></div><div class="tooltip-inner">Attachment</div></div><br>	
											<span style="font-style:italic; font-size:10px;">(max upload size per file - 2 MB)</span>										
										</address>
									</div>
									<input name="txtCRMFILE2" type="hidden" id="txtCRMFILE2" value=""/>		
									</td>
									</tr>
									</table>
									</div>
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
											 PRE ARRIVAL & OTHER
											</h2>                            
										</div>
									</div>
									
									<div class="box-body no-padding">
										<table class="table table-condensed">
										   <tr>
											   <td width="35%" align="left">VESSEL NAME</td>
												<td width="20%" align="left">
													<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											 <tr>
											   <td width="35%" align="left">NOMINATION ID</td>
												<td width="20%" align="left">
												   <?php echo $obj->getCompareTableData($comid,"MESSAGE");?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											 <?php
												if($arr_explode[0] == 'LP')
												{
													$name = 'CARGO DECL.SIGN MASTER';
													$name_1 = 'STOW PLAN QTY';
													$name_2 = 'SP DEP DRAFT';
													$name_3 = 'SP ARR DRAFT';
													$name_4 = 'ENGAGEMENT';
													$name_5 = 'LOADED';
												}
												else
												{
													$name = 'BL MANIFEST';
													$name_1 = 'BL QUANTITY';
													$name_2 = 'ARR DRAFT';
													$name_3 = 'DEP DRAFT';
													$name_4 = 'BL';
													$name_5 = 'DISCHARGE';
												}
												
												$sql15 = "select * from sof_slave_4 where COMID='".$comid."' and LOGIN='AGENT' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_explode[0]."' and PORTID='".$arr_explode[1]."'";
												$res15 = mysql_query($sql15) or die($sql15);
												$rows15 = mysql_fetch_assoc($res15);
												
												if($rows15['CARGO_DECL'] == 1){$style = 'checked="checked"';}else{$style = '';}
												if(date("d-M-Y",strtotime($rows15['ETA_30_DAYS'])) == '01-Jan-1970') {$eta_1 = "";} else {$eta_1 = date("d-M-Y H:i",strtotime($rows15['ETA_30_DAYS']));}
												if(date("d-M-Y",strtotime($rows15['ETA_25_DAYS'])) == '01-Jan-1970') {$eta_2 = "";} else {$eta_2 = date("d-M-Y H:i",strtotime($rows15['ETA_25_DAYS']));}
												if(date("d-M-Y",strtotime($rows15['ETA_20_DAYS'])) == '01-Jan-1970') {$eta_3 = "";} else {$eta_3 = date("d-M-Y H:i",strtotime($rows15['ETA_20_DAYS']));}
												if(date("d-M-Y",strtotime($rows15['ETA_25_DAYS'])) == '01-Jan-1970') {$eta_4 = "";} else {$eta_4 = date("d-M-Y H:i",strtotime($rows15['ETA_20_DAYS']));}
												if(date("d-M-Y",strtotime($rows15['ETA_10_DAYS'])) == '01-Jan-1970') {$eta_5 = "";} else {$eta_5 = date("d-M-Y H:i",strtotime($rows15['ETA_10_DAYS']));}
												
												if(date("d-M-Y",strtotime($rows15['ETA_7_DAYS'])) == '01-Jan-1970') {$eta_6 = "";} else {$eta_6 = date("d-M-Y H:i",strtotime($rows15['ETA_7_DAYS']));}
												if(date("d-M-Y",strtotime($rows15['ETA_5_DAYS'])) == '01-Jan-1970') {$eta_7 = "";} else {$eta_7 = date("d-M-Y H:i",strtotime($rows15['ETA_5_DAYS']));}
												if(date("d-M-Y",strtotime($rows15['ETA_3_DAYS'])) == '01-Jan-1970') {$eta_8 = "";} else {$eta_8 = date("d-M-Y H:i",strtotime($rows15['ETA_3_DAYS']));}
												if(date("d-M-Y",strtotime($rows15['ETA_2_DAYS'])) == '01-Jan-1970') {$eta_9 = "";} else {$eta_9 = date("d-M-Y H:i",strtotime($rows15['ETA_2_DAYS']));}
												if(date("d-M-Y",strtotime($rows15['ETA_1_DAYS'])) == '01-Jan-1970') {$eta_10 = "";} else {$eta_10 = date("d-M-Y H:i",strtotime($rows15['ETA_1_DAYS']));}
												if(date("d-M-Y",strtotime($rows15['ACTUAL_ARRIVAL'])) == '01-Jan-1970') {$eta_11 = "";} else {$eta_11 = date("d-M-Y H:i",strtotime($rows15['ACTUAL_ARRIVAL']));}
												if(date("d-M-Y",strtotime($rows15['NOR_TENDERED'])) == '01-Jan-1970') {$eta_12 = "";} else {$eta_12 = date("d-M-Y H:i",strtotime($rows15['NOR_TENDERED']));}
												
												?>
											<tr>
											   <td width="35%" align="left"><?php echo $name;?></td>
												<td width="20%" align="left">
												   <input type="checkbox" name="txtcheckbox_<?php echo $i+1;?>" id="txtcheckbox_<?php echo $i+1;?>" class="form-control" value="1" <?php echo $style;?> disabled="disabled"/><label for="txtcheckbox_<?php echo $i+1;?>"></label>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											<tr>
											   <td width="35%" align="left"><?php echo $name_1;?></td>
												<td width="20%" align="left">
												  <?php echo $rows15['STOW_PLAN_QTY'];?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											 <tr>
											   <td width="35%" align="left"><?php echo $name_2;?></td>
												<td width="20%" align="left">
												  <?php echo $rows15['SP_DEPT_DRAFT'];?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											 <tr>
											   <td width="35%" align="left"><?php echo $name_3;?></td>
												<td width="20%" align="left">
												<?php echo $rows15['SP_ARR_DRAFT'];?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											  <tr>
											   <td width="35%" align="left">ETA 30 DAYS</td>
												<td width="20%" align="left">
												 <?php echo $eta_1;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											  <tr>
											   <td width="35%" align="left">ETA 25 DAYS</td>
												<td width="20%" align="left">
												 <?php echo $eta_2;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											  <tr>
											   <td width="35%" align="left">ETA 20 DAYS</td>
												<td width="20%" align="left">
												 <?php echo $eta_3;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											  <tr>
											   <td width="35%" align="left">ETA 15 DAYS</td>
												<td width="20%" align="left">
												 <?php echo $eta_4;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											  <tr>
											   <td width="35%" align="left">ETA 10 DAYS</td>
												<td width="20%" align="left">
												 <?php echo $eta_5;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											 
											 <tr>
											   <td width="35%" align="left">ETA 7 DAYS</td>
												<td width="20%" align="left">
												 <?php echo $eta_6;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											  <tr>
											   <td width="35%" align="left">ETA 5 DAYS</td>
											    <td width="20%" align="left">
												<?php echo $eta_7;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											 <tr>
											   <td width="35%" align="left">ETA 3 DAYS</td>
												<td width="20%" align="left">
												<?php echo $eta_8;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											 <tr>
											   <td width="35%" align="left">ETA 2 DAYS</td>
												<td width="20%" align="left">
												 <?php echo $eta_9;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											 <tr>
											   <td width="35%" align="left">ETA 1 DAYS</td>
												<td width="20%" align="left">
												<?php echo $eta_10;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											 <tr>
											   <td width="35%" align="left">ACTUAL ARRIVAL</td>
												<td width="20%" align="left">
												 <?php echo $eta_11;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
											 <tr>
											   <td width="35%" align="left">NOR TENDERED</td>
												<td width="20%" align="left">
												 <?php echo $eta_12;?>
												</td>
												<td width="25%" align="left">&nbsp;&nbsp;</td>
											 </tr>
										 </table>
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
											   DAILY QTY
											</h2>                            
										</div><!-- /.col -->
									</div>
									
									<div class="row">
									   <div class="col-xs-12">
										  <div class="box box-primary">
											 <div class="box-body no-padding" style="overflow:auto;">
											  <table class="table table-striped" >
											   <thead>
												<tr>
												<th width="12%" align="left" valign="top">DATE (EVERY N/N OR ON COMPLETION)</th>
												<th width="17%" align="left" valign="top">TOTAL AGREED QTY (MT)</th>
												<?php if($arr_explode[0]=="LP"){?>
                                                <th width="20%" align="left" valign="top">LOADED LAST 24 HRS (MT)</th>
                                                <th width="15%" align="left" valign="top">TOTAL LOADED THIS FAR (MT)</th>
                                                <?php }else if($arr_explode[0]=="DP"){?>
                                                <th width="20%" align="left" valign="top">DISCHARGED LAST 24 HRS (MT)</th>
                                                <th width="15%" align="left" valign="top">TOTAL DISCHARGED THIS FAR (MT)</th>
                                                <?php }?>
												<th width="15%" align="left" valign="top">BALANCE (MT)</th>
												<th width="15%" align="left" valign="top">ETC CARGO/CARGO COMPLETED</th>
												</tr>
											   </thead>
											   <tbody id="pre_arrival_<?php echo $i+1;?>" class="todo-list">
											   <?php
												 $sql16 = "select * from sof_slave_5 where COMID='".$comid."' and LOGIN='AGENT' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_explode[0]."' and PORTID='".$arr_explode[1]."'"; 
												
												$res16 = mysql_query($sql16) or die($sql16);
												$rec16 = mysql_num_rows($res16);
												if($rec16 == 0)
												{ 
												?>
												<tr>
													<td align="center" colspan="6" valign="middle">sorry currently there are zero(0) record</td>
												</tr>
												<?php }else{while($rows16 = mysql_fetch_assoc($res16)){ 
												if(date("d-M-Y H:i",strtotime($rows16['PRE_DATE'])) == '01-Jan-1970') {$pre_date = "";} else {$pre_date = date("d-M-Y",strtotime($rows16['PRE_DATE']));}
												if(date("d-M-Y H:i",strtotime($rows16['ETCD'])) == '01-Jan-1970') {$etcd = "";} else {$etcd = date("d-M-Y H:i",strtotime($rows16['ETCD']));}
												?>
												<tr>
													<td align="left" valign="middle"><?php echo $pre_date;?></td>
													<td align="left" valign="middle"><?php echo $rows16['ENGAGEMENT_QTY'];?></td>
													<td align="left" valign="middle"><?php echo $rows16['LOAD_LAST'];?></td>
													<td align="left" valign="middle"><?php echo $rows16['TTL_LOAD'];?></td>
													<td align="left" valign="middle"><?php echo $rows16['BALANCE'];?></td>
													<td align="left" valign="middle"><?php echo $etcd;?></td>
												</tr>
												<?php }}?>
												</tbody>
												
												</table>
												</div><!-- /.box-body -->
											</div>                          
										</div><!-- /.col -->
									</div>
									
                                    
                                    <div class="row invoice-info">
									    <div class="col-sm-4 invoice-col">
                                            <address>
                                                <strong><label >&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-default btn-sm" data-toggle="modal" data-target="#compose-modal" title="Instructions" style="color:#C44000;"><i class="fa  fa-file-text-o"></i>&nbsp;&nbsp;Instructions&nbsp;&nbsp;</a></label> </strong>
                                            </address>
                                        </div>
                                    </div>
                                    
									<?php 
									
									
									$sqlag = "select * from sof_master where COMID='".$comid."' and LOGIN='AGENT' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_explode[0]."' and RANDOMID='".$arr_explode[2]."' and PORTID='".$arr_explode[1]."' and SUBMITID='2'";
									
									$resag = mysql_query($sqlag) or die($sqlag);
									$numsag = mysql_num_rows($resag);
									if($numsag>0)
									{
									$query = "select * from sof_master where COMID='".$comid."' and LOGIN='INTERNAL_USER' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_explode[0]."' and RANDOMID='".$arr_explode[2]."' and PORTID='".$arr_explode[1]."' ";
									
									$qres = mysql_query($query) or die($query);
									$qrec = mysql_num_rows($qres);
									$qrows = mysql_fetch_assoc($qres);
									if($qrows["SUBMITID"] == 1 || $qrec == 0)
									{
									?>
									<div class="box-footer" align="right"><button  class="btn btn-primary btn-flat" type="button" onClick="return getSubmit(<?php echo $i+1;?>,1,'<?php echo $arr_explode[0];?>',<?php echo $arr_explode[1];?>,<?php echo $arr_explode[2];?>);" >Submit</button>&nbsp;&nbsp;<button  class="btn btn-primary btn-flat" type="button" onClick="return getSubmit(<?php echo $i+1;?>,2,'<?php echo $arr_explode[0];?>',<?php echo $arr_explode[1];?>,<?php echo $arr_explode[2];?>);">Submit & Close</button>&nbsp;&nbsp;&nbsp;<input type="hidden" name="action" id="action" value="submit" /><input name="txtTTLQty_<?php echo $i+1;?>" type="hidden" id="txtTTLQty_<?php echo $i+1;?>" size="10"/><input name="txtTTLBLQty_<?php echo $i+1;?>" type="hidden" id="txtTTLBLQty_<?php echo $i+1;?>" size="10"/></div>
									<?php }}?>
									
									</form>
									</div>
									<?php }?>
									
									<!-- /.tab_1-pane -->
									
									</div><!-- /.tab-content -->
								</div><!-- nav-tabs-custom -->
							</div>
						</div>
					<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
			<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-file-text-o"></i> INSTRUCTIONS</h4>
                    </div>
						<div class="modal-body">
							<div class="box box-primary">
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <tr>
                                            <td>A.</td>
                                            <td>Please fill in the item 10 - 16 without fail, for anchorage port.</td>
                                        </tr>
                                        <tr>
                                            <td>B.</td>
                                            <td>Activity in ports :     All Activities in Port to be mentioned in below section from vessel arrival, shifting, working, stoppages, including fields as mentioned in 10-16 above,  till vessel sailing and clearing port.</td>
                                        </tr>
										<tr>
                                            <td>C.</td>
                                            <td>End of a day to be inserted as 00:00 hrs of new day.<br />eg. #1 : if time to be inserted as full day :  From : 11-Sep-2014 00:00   To : 12-Sep-2014 00:00<br /><br /> eg. #2:  if time to be inserted as part day with day ending:  From : 11-Sep-2014 16:30   To : 12-Sep-2014 00:00<br /><br />eg. #3: to start new time / new day From : 12-Sep-2014 00:00   To :  .....</td>
                                        </tr>
										<tr>
                                            <td>D.</td>
                                            <td>Please click "Submit" to save the form and once complete and sure click "Submit and Close". Please note after "Submit and Close" the form is not editable.</td>
                                        </tr>
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
							
						</div>                       
                   
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<script src="../../js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>  
<script>
$(function() {
    "use strict";
    //jQuery UI sortable for the todo list
    $(".todo-list").sortable({
        placeholder: "sort-highlight",
        handle: ".handle",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
});
</script>
</body>
</html>