<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "vessel_in_post_ops.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "vessel_in_history.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "coa_in_ops_at_glance.php"; $page_bar="In Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else if($page == 5){$page_link = "coa_in_post_ops.php"; $page_bar="In Post Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else{$page_link = "coa_in_history.php"; $page_bar="Vessels in History - COA";$nav=16;$subtitle = 'COA';}

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertTcTripDetails();
	header('Location:./'.$page_link.'?msg='.$msg);
}

$id = $obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($id);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?>&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
					
					<div align="right">
						
						<a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
					<div style="height:10px;">&nbsp;</div>
					
				<div class="box box-primary">
					<h3 style=" text-align:center;">Payment / Invoice Grid : <?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></h3>
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>					 
						<?php
                            $res11 = mysql_query("select * from freight_cost_estimete_slave10 where FCAID='".$obj->getFun1()."' and SHIPPER_CHARTER!='' and STATUS=1 ");
                            $num11 = mysql_num_rows($res11);
                            if($num11>0)
                            {
                            ?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Main Cargo Freight Details</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="50%">Name</th>
                                                <th width="25%">Vendor</th>
                                                <th width="25%">&nbsp;</th>											
                                            </tr>
                                        </thead>
                                        <tbody>
                                         <?php
                                            $i = 0; 
                                            while($row11 = mysql_fetch_assoc($res11))
                                            {$i = $i + 1;
											$addcomm = $obj->getFun74();
											
											$amount = $row11['AMOUNT_USD'];
                                            $id = $comid.','.$obj->getFun1().','.$row11['SHIPPER_CHARTER'].','.number_format($amount,2,'.','').',0,'.$row11['CARGO_MT'].',0,0,0';
                                            ?>
                                            <tr>
                                                <td>Final Nett Freight</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($row11['SHIPPER_CHARTER']);?></td>
                                                <td>
                                                    <a href="invoice.php?id=<?php echo $id;?>&name=Final Nett Main Cargo Freight&page=<?php echo $page;?>" ><button class="btn btn-info btn-flat" type="button">Invoice</button></a>&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php }?>
						   
                           
                           <?php
                            $res11 = mysql_query("select * from freight_cost_estimete_slave10 where FCAID='".$obj->getFun1()."' and SHIPPER_CHARTER!='' and STATUS=2 ");
                            $num11 = mysql_num_rows($res11);
                            if($num11>0)
                            {
                            ?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Overage Cargo Freight Details</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="50%">Name</th>
                                                <th width="25%">Vendor</th>
                                                <th width="25%">&nbsp;</th>											
                                            </tr>
                                        </thead>
                                        <tbody>
                                         <?php
                                            $i = 0; 
                                            while($row11 = mysql_fetch_assoc($res11))
                                            {$i = $i + 1;
											$addcomm = $obj->getFun74();
											
											$amount = $row11['AMOUNT_USD'];
                                            $id = $comid.','.$obj->getFun1().','.$row11['SHIPPER_CHARTER'].','.number_format($amount,2,'.','').',0,'.$row11['CARGO_MT'].',0,0,0';
                                            ?>
                                            <tr>
                                                <td>Final Nett Freight</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($row11['SHIPPER_CHARTER']);?></td>
                                                <td>
                                                    <a href="invoice.php?id=<?php echo $id;?>&name=Final Nett Overage Cargo Freight&page=<?php echo $page;?>" ><button class="btn btn-info btn-flat" type="button">Invoice</button></a>&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php }?>
                            
                            <?php
                            $res11 = mysql_query("select * from freight_cost_estimete_slave10 where FCAID='".$obj->getFun1()."' and SHIPPER_CHARTER!='' and STATUS=3 ");
                            $num11 = mysql_num_rows($res11);
                            if($num11>0)
                            {
                            ?>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Dead Freight Details</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="50%">Name</th>
                                                <th width="25%">Vendor</th>
                                                <th width="25%">&nbsp;</th>											
                                            </tr>
                                        </thead>
                                        <tbody>
                                         <?php
                                            $i = 0; 
                                            while($row11 = mysql_fetch_assoc($res11))
                                            {$i = $i + 1;
											$addcomm = $obj->getFun74();
											
											$amount = $row11['AMOUNT_USD'];
                                            $id = $comid.','.$obj->getFun1().','.$row11['SHIPPER_CHARTER'].','.number_format($amount,2,'.','').',0,'.$row11['CARGO_MT'].',0,0,0';
                                            ?>
                                            <tr>
                                                <td>Final Nett Freight</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($row11['SHIPPER_CHARTER']);?></td>
                                                <td>
                                                    <a href="invoice.php?id=<?php echo $id;?>&name=Final Nett Dead Freight&page=<?php echo $page;?>" ><button class="btn btn-info btn-flat" type="button">Invoice</button></a>&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php }?>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Bunkers Nett Supply</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="50%">Name</th>
											<th width="25%">Vendor</th>
											<th width="25%">&nbsp;</th>											
										</tr>
									</thead>
									<tbody>
										<?php 
										$query = "select * from freight_cost_estimete_slave8 where FCAID='".$obj->getFun1()."'";
										$qres = mysql_query($query);
										$qrec = mysql_num_rows($qres);
										if($qrec > 0)
										{
										while($qrows = mysql_fetch_assoc($qres))
										{
										$id = '2'.','.'Bunkers Nett Supply'.','.$qrows['BUNKERGRADEID'].','.$qrows['VENDORID'].','.$comid.','.$qrows['COST'];
										?>
										<tr>
											<td><?php echo $obj->getBunkerGradeBasedOnID($qrows['BUNKERGRADEID']);?> Nett</td>
											<td><?php echo $obj->getVendorListNewBasedOnID($qrows['VENDORID']);?></td>
											<td>
										<?php if($obj->getVendorListNewBasedOnID($qrows['VENDORID']) != '')
										    {
                                             $sql = "select * from request_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and NAME='Bunkers Nett Supply' and NAME_ID='2' and GRADEID='".$qrows['BUNKERGRADEID']."' and VENDOR='".$qrows['VENDORID']."' and P_AMT > 0";
											 $res 	= mysql_query($sql);
											 $rec 	= mysql_num_rows($res);
											 if($rec==0)
											 {?>
											 <a href="request_port_costcoa.php?id=<?php echo $id;?>&name=<?php echo $obj->getBunkerGradeBasedOnID($qrows['BUNKERGRADEID']);?> Nett&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										   <?php }else{?>
										   <a href="request_port_costcoa.php?id=<?php echo $id;?>&name=<?php echo $obj->getBunkerGradeBasedOnID($qrows['BUNKERGRADEID']);?> Nett&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										   <?php }?>
                                      <?php }?>
											</td>
										</tr>
										<?php }}?>
									</tbody>
								</table>
							</div>
						</div>						
						
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Operational Costs (Others)</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="50%">Name</th>
											<th width="25%">Vendor</th>
											<th width="25%">&nbsp;</th>											
										</tr>
									</thead>
									<tbody>
										<?php 
										$sql_brok = "select * from freight_cost_estimete_slave4 where FCAID='".$obj->getFun1()."' ";
										
										$res_brok = mysql_query($sql_brok);
										$num_brok = mysql_num_rows($res_brok);
										while($rows_brok = mysql_fetch_assoc($res_brok))
										 {
										$id = '3,'.'Operational Costs (Others),0,'.$rows_brok['VENDORID'].','.$comid.','.$rows_brok["BROKAGE_AMT"];
										?>
										<tr>
											<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($rows_brok['VENDORID']);?></td>
											<td>
											<?php if($obj->getVendorListNewBasedOnID($rows_brok['VENDORID']) != '') 
                                            {
                                             $sql = "select * from request_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and NAME='Operational Costs (Others)' and NAME_ID='3' and GRADEID='0' and VENDOR='".$rows_brok['VENDORID']."' and P_AMT > 0";
											 $res 	= mysql_query($sql);
											 $rec 	= mysql_num_rows($res);
											 if($rec==0)
											 {?>
											 <a href="request_port_costcoa.php?id=<?php echo $id;?>&name=Brokerage Commission&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										   <?php }else{?>
										   <a href="request_port_costcoa.php?id=<?php echo $id;?>&name=Brokerage Commission&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										   <?php }?>
                                       <?php }?>
											</td>
										</tr>
										<?php } ?>
										
										<?php 
										$sql4 = "select * from freight_cost_estimete_slave3 where FCAID='".$obj->getFun1()."'  and IDENTIFY='ORC'";
										$res4 = mysql_query($sql4);
										$rec4 = mysql_num_rows($res4);$i=0;
										while($rows4 = mysql_fetch_assoc($res4))
										{$i= $i +1;
											$id = '3'.','.'Operational Costs'.','.$i.','.$rows4['VENDORID'].','.$comid.','.$rows4['RAW_AMOUNT'];
										?>
										<tr>
											<td><?php echo $obj->getOwnerRelatedCostNameBasedOnID($rows4['IDENTY_ID']);?></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($rows4['VENDORID']);?></td>
											<td>
											<?php if($obj->getVendorListNewBasedOnID($rows4['VENDORID']) != '') 
											{
                                               $sql = "select * from request_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and NAME='Operational Costs' and NAME_ID='3' and GRADEID='".$i."' and VENDOR='".$rows4['VENDORID']."' and P_AMT > 0";
											   $res 	= mysql_query($sql);
											   $rec 	= mysql_num_rows($res);
											   if($rec==0)
											   {?>
											   <a href="request_port_costcoa.php?id=<?php echo $id;?>&name=<?php echo $obj->getOwnerRelatedCostNameBasedOnID($rows4['IDENTY_ID']);?>&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										    <?php }else{?>
										       <a href="request_port_costcoa.php?id=<?php echo $id;?>&name=<?php echo $obj->getOwnerRelatedCostNameBasedOnID($rows4['IDENTY_ID']);?>&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										  <?php }?>
                                       <?php }?>
											</td>
										</tr>
										<?php }?>
									
										
									</tbody>
								</table>
							</div>
						</div>						
						
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Port Costs</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="50%">Name</th>
											<th width="25%">Vendor</th>
											<th width="25%">&nbsp;</th>											
										</tr>
									</thead>
									<tbody>
									<?php 
									$mysql = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."' order by FCA_SLAVEID";
									$myres = mysql_query($mysql);
									$myrec = mysql_num_rows($myres);
									if($myrec > 0)
									{$i=1;
										while($myrows = mysql_fetch_assoc($myres))
										{
											$id = '5'.','.'Load Port Costs'.','.$myrows['FROM_PORT'].','.$myrows['PORT_COSTLP_VENDOR'].','.$comid.','.$myrows['LOAD_PORT_COST'].','.'Load';
											?>
											<tr>
											<td>Load Port  <?php echo $obj->getPortNameBasedOnID($myrows['FROM_PORT']);?></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($myrows['PORT_COSTLP_VENDOR']);?></td>
											<td>
											<?php if( $obj->getVendorListNewBasedOnID($myrows['PORT_COSTLP_VENDOR']) != '') 
											{
                                               $sql = "select * from request_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and NAME='Load Port Costs' and NAME_ID='5' and GRADEID='".$myrows['FROM_PORT']."' and VENDOR='".$myrows['PORT_COSTLP_VENDOR']."' and P_AMT > 0";
											   $res 	= mysql_query($sql);
											   $rec 	= mysql_num_rows($res);
											   if($rec==0)
											   {?>
											   <a href="request_port_costcoa.php?id=<?php echo $id;?>&name=Load Port  <?php echo $obj->getPortNameBasedOnID($myrows['FROM_PORT']);?>&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										    <?php }else{?>
										       <a href="request_port_costcoa.php?id=<?php echo $id;?>&name=Load Port  <?php echo $obj->getPortNameBasedOnID($myrows['FROM_PORT']);?>&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										  <?php }?>
											<?php }?>
											</td>
											</tr>
											<?php 
											$id2 = '5'.','.'Discharge Port Costs'.','.$myrows['TO_PORT'].','.$myrows['PORT_COSTDP_VENDOR'].','.$comid.','.$myrows['DISC_PORT_COST'].','.'Discharge';
											?>
											<tr>
											<td>Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows['TO_PORT']);?></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($myrows['PORT_COSTDP_VENDOR']);?></td>
											<td>
											<?php if( $obj->getVendorListNewBasedOnID($myrows['PORT_COSTDP_VENDOR']) != '') 
											{
                                               $sql = "select * from request_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and NAME='Discharge Port Costs' and NAME_ID='5' and GRADEID='".$myrows['TO_PORT']."' and VENDOR='".$myrows['PORT_COSTDP_VENDOR']."' and P_AMT > 0";
											   $res 	= mysql_query($sql);
											   $rec 	= mysql_num_rows($res);
											   if($rec==0)
											   {?>
											   <a href="request_port_costcoa.php?id=<?php echo $id;?>&name=Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows['TO_PORT']);?>&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										    <?php }else{?>
										       <a href="request_port_costcoa.php?id=<?php echo $id;?>&name=Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows['TO_PORT']);?>&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										  <?php }?>
											<?php }?>
											</td>
											</tr>
                                            <?php 
											$id3 = '5'.','.'Transit Port Costs'.','.$myrows['TO_PORT'].','.$myrows['PORT_COSTTP_VENDOR'].','.$comid.','.$myrows['TRANSIT_PORT_COST'].','.'Transit';
											?>
											<tr>
											<td>Transit Port  <?php echo $obj->getPortNameBasedOnID($myrows['TO_PORT']);?></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($myrows['PORT_COSTTP_VENDOR']);?></td>
											<td>
											<?php if( $obj->getVendorListNewBasedOnID($myrows['PORT_COSTTP_VENDOR']) != '') 
											{
											   $sql = "select * from request_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and NAME='Transit Port Costs' and NAME_ID='5' and GRADEID='".$myrows['TO_PORT']."' and VENDOR='".$myrows['PORT_COSTTP_VENDOR']."' and P_AMT > 0";
											   $res 	= mysql_query($sql);
											   $rec 	= mysql_num_rows($res);
											   if($rec==0)
											   {?>
											   <a href="request_port_costcoa.php?id=<?php echo $id3;?>&name=Transit Port  <?php echo $obj->getPortNameBasedOnID($myrows['TO_PORT']);?>&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										    <?php }else{?>
										       <a href="request_port_costcoa.php?id=<?php echo $id3;?>&name=Transit Port  <?php echo $obj->getPortNameBasedOnID($myrows['TO_PORT']);?>&page=<?php echo $page;?>" ><button class="btn btn-warning btn-flat" type="button">Payment</button></a>&nbsp;&nbsp;
										  <?php }?>
											<?php }?>
											</td>
											</tr>
											<?php 
											$i++;}?>
									<?php }?>
									</tbody>
								</table>
							</div>
						</div>
						
                        <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Demurrage Dispatch Ship Owner</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="50%">Name</th>
                                                <th width="25%">Vendor</th>
                                                <th width="25%">&nbsp;</th>											
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $sql121    = "select * from freight_cost_estimete_slave10 where FCAID='".$obj->getFun1()."' and DEM_AMT>0 order by STATUS asc";
                                            $result    = mysql_query($sql121);
                                            $record    = mysql_num_rows($result);
                                            if($record > 0){
                                            while($rows11 = mysql_fetch_array($result))
                                            {
												if($rows11['STATUS']==1){$qtytest = 'Main Quantity';}
												if($rows11['STATUS']==2){$qtytest = 'Overage Quantity';}
												$id = $comid.','.$obj->getFun1().','.$rows11['SHIPPER_CHARTER'].','.$rows11['DEM_AMT'].',Demurrage/Dispatch('.$qtytest.')';
												$invtitle = "Demurrage Dispatch Invoice for ".$qtytest;
												$amounttitle = "Demurrage/Dispatch (".$obj->getVendorListNewBasedOnID($rows11['SHIPPER_CHARTER']).')';
                                            ?>
                                            <tr>
                                                <td>Demurrage Dispatch (<?php echo $qtytest;?>)</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($rows11['SHIPPER_CHARTER']); ?></td>
                                                <td>
                                        
                                               <a href="invoice_others.php?id=<?php echo $id;?>&name=<?php echo $invtitle;?>&amounttitle=<?php echo $amounttitle;?>&page=<?php echo $page;?>"><button class="btn btn-info btn-flat" type="button">Invoice</button></a>&nbsp;&nbsp;
                                        
                                                </td>
                                            </tr>
                                            
                                            <?php }}?>
        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Other Income</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="50%">Name</th>
											<th width="25%">Vendor</th>
											<th width="25%">&nbsp;</th>											
										</tr>
									</thead>
									<tbody>
										<?php 
										$sql3 = "select * from freight_cost_estimete_slave3 where FCAID='".$obj->getFun1()."' and IDENTIFY='OTHERINCOME' and RAW_AMOUNT>0";
										$res3 = mysql_query($sql3);
										$rec3 = mysql_num_rows($res3);
										$i=1;
										while($rows3 = mysql_fetch_assoc($res3))
										{
									    $id = $comid.','.$obj->getFun1().','.$rows3['VENDORID'].','.$rows3['RAW_AMOUNT'].',Other Income';
										$invtitle = "Other Income Invoice for ".$rows3['IDENTY_ID'];
										$amounttitle = $rows3['IDENTY_ID'];
										?>
										<tr>
										<td><?php echo $rows3['IDENTY_ID'];?></td>
										<td><?php echo $obj->getVendorListNewBasedOnID($rows3['VENDORID']);?></td>
										<td>
										<?php if($obj->getVendorListNewBasedOnID($rows3['VENDORID']) != '') 
										{?>
										   <a href="invoice_others.php?id=<?php echo $id;?>&name=<?php echo $invtitle;?>&amounttitle=<?php echo $amounttitle;?>&page=<?php echo $page;?>"><button class="btn btn-info btn-flat" type="button">Invoice</button></a>&nbsp;&nbsp;											
							     <?php }?>
										</td>
										</tr>
										<?php $i++;}?>
									</tbody>
								</table>
							</div>
						</div>
                        
                        
                       <div class="box-header">
								<h3 class="box-title">Hireage</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="50%">Name</th>
											<th width="25%">Vendor</th>
											<th width="25%">&nbsp;</th>											
										</tr>
									</thead>
									<tbody>
									  <tr>
										<td>Hire</td>
										<td><?php echo $obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($comid,"DTCVENDORID"));?></td>
										<td>
										<?php if($obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($comid,"DTCVENDORID")) != '') 
									    { ?>
										   
										   <a href="invoice_hire.php?comid=<?php echo $comid;?>&page=<?php echo $page;?>" ><button class="btn btn-default btn-flat" type="button">Hire Statement</button></a>&nbsp;&nbsp;
                                  <?php }?>
										</td>
										</tr>
									</tbody>
								</table>
							</div>
						
					</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
		
</body>
</html>