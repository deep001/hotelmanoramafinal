<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->updateVesselMasterRecords();
	header('Location:./fleet.php?msg='.$msg);
 }
$obj->viewVesselRecords($id);
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">

<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->

<style>
form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-anchor"></i>&nbsp;Fleet&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Fleet</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div align="right"><a href="fleet.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				
				
				<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">UPDATE VESSEL</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                    <div class="box-body">
                    
                        <div class="form-group" style="width:49%;  float:left;">
							<label for="selBType">Business Type</label>
							<select  name="selBType" class="form-control" id="selBType" onChange="getVesselTypeList();showhideDiv();">
							<?php 
							$obj->getBusinessTypeList1();
							?>
							</select>
                            <input type="hidden" id="uid" name="uid" value="<?php echo $id;?>" >
						</div>
                        <div class="form-group" style="width:49%;  float:left;padding-left:1%;">
							<label for="selVType">Vessel Type</label>
							<select  name="selVType" class="form-control" id="selVType" >
							<option value="">----Select From List----</option>
							</select>
						</div>
                        <div class="form-group" style="width:49%; float:left;">
							<label for="txtVName" >IMO number</label>
							<input type="text" class="form-control" id="txtIMONo" name="txtIMONo" placeholder="IMO number" autocomplete="off" value="<?php echo $obj->getFun14();?>">                        </div>
                    
                        <div class="form-group" style="width:49%; float:left;padding-left:1%;">
							<label for="txtVName" >Vessel Name</label>
							<input type="text" class="form-control" id="txtVName" name="txtVName" placeholder="Vessel Name" autocomplete="off" value="<?php echo $obj->getFun2();?>">                        
                        </div>
						
						<div class="form-group" style="width:49%; float:left;">
							<label for="txtVCode" >Vessel Code</label>
							<input type="text" class="form-control" id="txtVCode" name="txtVCode" placeholder="Vessel Code" autocomplete="off" value="<?php echo $obj->getFun42(); ?>">     </div><br/>
						<div style="clear:both;"></div>	
                            
                        <div class="form-group" style="width:49%; float:left;">
							<label for="txtYBDate" >Year Built</label>
							<input type="text" class="form-control" id="txtYBDate" name="txtYBDate" placeholder="yyyy" autocomplete="off" value="<?php echo $obj->getFun7();?>">                       
                        </div>
                            
						<div class="form-group" style="width:49%; float:left;padding-left:1%;">
							<label for="selHullType" >Flag</label>
							<select  name="selFlag" class="form-control" id="selFlag" >
							<?php 
							$obj->getCountryNameList();
							?>
							</select>                                         
						</div>
						<div class="form-group" style="width:49%;  float:left;">
							<label for="txtDWT">Summer DWT (MT)</label>
							<input type="text" class="form-control" id="txtDWT" name="txtDWT" placeholder="Summer DWT (M)" autocomplete="off" value="<?php echo $obj->getFun6();?>">
						</div>
                        
                        <div class="form-group" style="width:49%; float:left;padding-left:1%;">
							<label for="txtDWT">Summer Draft (M)</label>
							<input type="text" class="form-control" id="txtDraft" name="txtDraft" placeholder="Summer Draft (M)" autocomplete="off" value="<?php echo $obj->getFun11();?>">
						</div>
						
						<div class="form-group" style="width:49%;  float:left;">
							<label for="txtCoating" >Loa (M)</label>
							<input type="text" class="form-control" id="txtLOA" name="txtLOA" placeholder="Loa (M)" autocomplete="off" value="<?php echo $obj->getFun16();?>">                                         
						</div>
						<div class="form-group" style="width:49%;  padding-left:1%;float:left;">
							<label for="txtOwner">Extreme Breadth(M)</label>
							<input type="text" class="form-control" id="txtEX_Breadth" name="txtEX_Breadth" placeholder="Extreme Breadth(M)" autocomplete="off" value="<?php echo $obj->getFun17();?>">							  
						</div>
						
						<div class="form-group" style="width:49%;  float:left;">
							<label for="txtDraft" >GRT</label>
							<input type="text" class="form-control" id="txtGRT" name="txtGRT" placeholder="GRT" autocomplete="off" value="<?php echo $obj->getFun18();?>">                                         
						</div>
                        <div class="form-group" style="width:49%; padding-left:1%;float:left;">
							<label for="txtDraft" >NRT</label>
							<input type="text" class="form-control" id="txtNRT" name="txtNRT" placeholder="NRT" autocomplete="off" value="<?php echo $obj->getFun39();?>">                                         
						</div>
                        <div style="clear:both;"></div>
						<div class="form-group" style="width:49%; float:left;" id="drycargo1">
							<label for="txtCraneSize">Grain(M³)</label>
							<input type="text" class="form-control" id="txtGrain" name="txtGrain" placeholder="Grain(M³)" autocomplete="off" value="<?php echo $obj->getFun19();?>">
						</div>
						
						<div class="form-group" style="width:49%;  float:left;padding-left:1%;" id="drycargo2">
							<label for="txtGrabSize" >Bale(M³)</label>
							<input type="text" class="form-control" id="txtBale" name="txtBale" placeholder="Bale(M³)" autocomplete="off" value="<?php echo $obj->getFun20();?>">                                         
						</div>
						
                        <div class="form-group" style="width:49%; float:left;" id="drycargo3">
							<label for="txtIMO">No. Of Holds</label>
							<input type="text" class="form-control" id="txtNOH" name="txtNOH" placeholder="No. Of Holds" autocomplete="off" value="<?php echo $obj->getFun21();?>">
						</div>
						
						<div class="form-group" style="width:49%;  float:left;padding-left:1%;" id="drycargo4">
							<label for="txtVesselCode" >No. Of Hatches</label>
							<input type="text" class="form-control" id="txtNOHA" name="txtNOHA" placeholder="No. Of Hatches" autocomplete="off" value="<?php echo $obj->getFun22();?>">                                         
						</div>
                        
                        <div class="form-group" style="width:49%;float:left;" id="drycargo5">
							<label for="txtVesselGroup">Hatch Sizes (Meters)</label>
							<input type="text" class="form-control" id="txtHatch_Size" name="txtHatch_Size" placeholder="Hatch Sizes (Meters)" autocomplete="off" value="<?php echo $obj->getFun23();?>">
						</div>
						
						<div class="form-group" style="width:49%; float:left;padding-left:1%;" id="drycargo6">
							<label for="txtVesselGroup">Cargo Gear</label>
							<input type="text" class="form-control" id="txtCargo_Gear" name="txtCargo_Gear" placeholder="Cargo Gear" autocomplete="off" value="<?php echo $obj->getFun24();?>">
						</div>
                        
                        <div class="form-group" style="width:49%;float:left;" id="drycargo7">
							<label for="txtVesselGroup">Crane Size</label>
							<input type="text" class="form-control" id="txtCraneSize" name="txtCraneSize" autocomplete="off" placeholder="Crane Size" value="<?php echo $obj->getFun12();?>">
						</div>
						
						<div class="form-group" style="width:49%; float:left;padding-left:1%;" id="drycargo8">
							<label for="txtVesselGroup">Grab Size</label>
							<input type="text" class="form-control" id="txtGrabSize" name="txtGrabSize" autocomplete="off" placeholder="Grab Size" value="<?php echo $obj->getFun13();?>">
						</div>
                        
                        <div class="form-group" style="width:49%; float:left;" id="gas1">
							<label for="txtIMO">No. Of Cargo Tanks</label>
							<input type="text" class="form-control" id="txtNoOfCargoTanks" name="txtNoOfCargoTanks" placeholder="No. Of Cargo Tanks" autocomplete="off" value="<?php echo $obj->getFun28();?>">
						</div>
						
						<div class="form-group" style="width:49%;  float:left;padding-left:1%;" id="gas2">
							<label for="txtVesselCode" >Cargo Tank Capacity 98%(CBM)</label>
							<input type="text" class="form-control" id="txtTankCapacity98" name="txtTankCapacity98" placeholder="Cargo Tank Capacity 98%(CBM)" autocomplete="off" value="<?php echo $obj->getFun29();?>">
						</div>
                        
						<div class="form-group" style="width:49%; float:left;" id="gas3">
							<label for="txtIMO">No. Of Cargo Pump(Main)</label>
							<input type="text" class="form-control" id="txtNoOfCargoPump" name="txtNoOfCargoPump" placeholder="No. Of Cargo Pump(Main)" autocomplete="off" value="<?php echo $obj->getFun30();?>">
						</div>
						
						<div class="form-group" style="width:49%;  float:left;padding-left:1%;" id="gas4">
							<label for="txtVesselCode" >Cargo Pumps Main Cap(CBM/Hr)</label>
							<input type="text" class="form-control" id="txtCargoPumpCap" name="txtCargoPumpCap" placeholder="Cargo Pumps Main Cap(CBM/Hr)" autocomplete="off" value="<?php echo $obj->getFun31();?>">          
						</div>
                        
                        <div class="form-group" style="width:49%; float:left;" id="gas5">
							<label for="txtIMO">No. and Size of Manilfolds</label>
							<input type="text" class="form-control" id="txtNoOfManilfolds" name="txtNoOfManilfolds" placeholder="No. and Size of Manilfolds" autocomplete="off" value="<?php echo $obj->getFun32();?>">
						</div>
						
						<div class="form-group" style="width:49%;  float:left;padding-left:1%;" id="gas6">
							<label for="txtVesselCode" >Total SBT Capacity(CBM)</label>
							<input type="text" class="form-control" id="txtGasSBTCapacity" name="txtGasSBTCapacity" placeholder="Total SBT Capacity(CBM)" autocomplete="off" value="<?php echo $obj->getFun33();?>">          
						</div>
                        
                        
                        <div class="form-group" style="width:49%; float:left;" id="tanker1">
							<label for="txtIMO">Cargo Tank Capacity (CBM)</label>
							<input type="text" class="form-control" id="txtTankCapacity" name="txtTankCapacity" placeholder="Cargo Tank Capacity" autocomplete="off" value="<?php echo $obj->getFun34();?>">
						</div>
						
						<div class="form-group" style="width:49%;  float:left;padding-left:1%;" id="tanker2">
							<label for="txtVesselCode" >No. of Grades(Double V/V Seg)</label>
							<input type="text" class="form-control" id="txtNoofGradeDouble" name="txtNoofGradeDouble" placeholder="Cargo Tank Capacity 98%(CBM)" autocomplete="off" value="<?php echo $obj->getFun35();?>">
						</div>
                        
						<div class="form-group" style="width:49%; float:left;" id="tanker3">
							<label for="txtIMO">No. Of Cargo Pump(Main)</label>
							<input type="text" class="form-control" id="txtNoOfCargoPumpTank" name="txtNoOfCargoPumpTank" placeholder="No. Of Cargo Pump(Main)" autocomplete="off" value="<?php echo $obj->getFun36();?>">
						</div>
						
						<div class="form-group" style="width:49%;  float:left;padding-left:1%;" id="tanker4">
							<label for="txtVesselCode" >Total SBT Capacity(CBM)</label>
							<input type="text" class="form-control" id="txtSBTCapacity" name="txtSBTCapacity" placeholder="Total SBT Capacity(CBM)" autocomplete="off" value="<?php echo $obj->getFun37();?>">          
						</div>
                        <div class="form-group" style="width:49%;  float:left;" id="tanker5">
							<label for="txtVesselCode" >Cargo Pump Main Cap(CBM/Hr)</label>
							<input type="text" class="form-control" id="txtTankCargoPumpCap" name="txtTankCargoPumpCap" placeholder="Cargo Pump Main Cap(CBM/Hr)" autocomplete="off" value="<?php echo $obj->getFun38();?>">
						</div>
                        <div style="clear:both;"></div>
						
						<div class="form-group" style="width:49%;float:left;">
							<label for="txtVesselGroup">Owners P & I</label>
                            <select  name="txtPI" class="form-control" id="txtPI" >
                            <?php 
                                $obj->getVendorBasedOnTypeIDList();
                            ?>
                            </select>
							<!--<input type="text" class="form-control" id="txtPI" name="txtPI" placeholder="Owners P & I" autocomplete="off">-->
						</div>
                        
                        <div class="form-group" style="width:49%;float:left;padding-left:1%;">
							<label for="txtVesselGroup">Classification Society</label>
							<select  name="selCLASS_SOC" class="form-control" id="selCLASS_SOC" >
							<?php
                            $obj->getClaSocList();
                            ?>
                            </select>
						</div>
                        <div class="form-group" style="width:49%;float:left;">
							<label for="txtVesselGroup">Remarks</label>
							<textarea name="txtRemarks" id="txtRemarks" class="form-control" cols="42" rows="4"></textarea>
						</div>
						
						<div class="form-group" style="width:49%;float:left;padding-left:1%;">
							<label for="txtVesselGroup">Attachment</label><br>
								<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" style="">
                                    <i class="fa fa-paperclip"></i> Attachment
                                    <input type="file" class="form-control" multiple name="attach_file[]" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                </div>
						</div>						
						<?php if($obj->getFun40() != '')
                            { 
                                $file = explode(",",$obj->getFun40()); 
                                $name = explode(",",$obj->getFun41()); ?>
						<table>									
                            <tr>
                                <td colspan="2">
                              	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Previous Attachments:
                                </td>
                            </tr>
                                <?php
                                $j =1;
                                for($i=0;$i<sizeof($file);$i++)
                                {
                                ?>
                                <tr id="row_file_<?php echo $j;?>">
                                    <td width="40%" align="left" class="input-text"  valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../../attachment/<?php echo $file[$i]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="<?php echo $name[$i];?>"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $name[$i]; ?></a>
                                    <input type="hidden" name="file_<?php echo $j;?>" id="file_<?php echo $j;?>" value="<?php echo $file[$i]; ?>" />
                                    <input type="hidden" name="name_<?php echo $j;?>" id="name_<?php echo $j;?>" value="<?php echo $name[$i]; ?>" />
                                    </td>
                                    <td ><a href="#1" onClick="Del_Upload(<?php echo $j;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
                                </tr>
                                <?php $j++;}?>
						</table>
                     	<?php }?>
						<div style="clear:both;"></div>
					</div><!-- /.box-body -->
					<!-- /.box-body -->
					<div class="box-footer" align="center">
						<input type="hidden" name="txtCRMFILE" id="txtCRMFILE" value="" />
						<input type="hidden" name="txtCRMNAME" id="txtCRMNAME" value="" />
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
						<input type="hidden" name="action" value="submit" />
					</div>
				</form>
			</div>
			
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>

<script type="text/javascript">
$(document).ready(function(){ 

$("#selHullType").val(<?php echo $obj->getFun5(); ?>);
$("#selBType").val(<?php echo $obj->getFun10(); ?>);
$('#selFlag').val(<?php echo $obj->getFun15(); ?>);
$('#selCLASS_SOC').val(<?php echo $obj->getFun26(); ?>);
$('#txtPI').val('<?php echo $obj->getFun25(); ?>');

$('#txtRemarks').autosize({append: "\n"});
$("#txtIMO,#txtDWT,#txtIMONo,#txtDraft,#txtLOA,#txtEX_Breadth,#txtYBDate,#txtGrain").numeric();

$("#txtYBDate").datepicker( {format: " yyyy", viewMode: "years", minViewMode: "years"});


$("#frm1").validate({
	rules: {
		selVType:"required",
		txtDWT:"required",
		txtYBDate:"required",
		selFlag: "required",
		txtDWT: "required",
		//txtDraft: "required"
		},
	messages: {
		selVType:"*",
		txtDWT:"*",
		txtYBDate:"*",
		selFlag: "*",
		txtDWT: "*",
		//txtDraft: "*"
		},
	submitHandler: function(form)  {
	jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
			if(r){
				var var1 = $("[id^=file_]").map(function () {return this.value;}).get().join(",");
				$('#txtCRMFILE').val(var1);
				var var2 = $("[id^=name_]").map(function () {return this.value;}).get().join(",");
				$('#txtCRMNAME').val(var2);
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	 			}
			else{return false;}
		});
	}
});
getVesselTypeList();
$("#selVType").val(<?php echo $obj->getFun3(); ?>);
showhideDiv();
});


function getVesselTypeList()
{
    var vesseltypelist = <?php echo $obj->getVesselTypeListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selVType');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selVType");
	$.each(vesseltypelist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selVType");
	});
}

function showhideDiv()
{
	var val =  $('#selBType').val();
	if(val==1)
	{
		$("#drycargo1,#drycargo2,#drycargo3,#drycargo4,#drycargo5,#drycargo6,#drycargo7,#drycargo8").hide();
		$("#tanker1,#tanker2,#tanker3,#tanker4,#tanker5").hide();
		$("#gas1,#gas2,#gas3,#gas4,#gas5,#gas6").show();
	}
	
	if(val==2)
	{
		$("#drycargo1,#drycargo2,#drycargo3,#drycargo4,#drycargo5,#drycargo6,#drycargo7,#drycargo8").hide();
		$("#gas1,#gas2,#gas3,#gas4,#gas5,#gas6").hide();
		$("#tanker1,#tanker2,#tanker3,#tanker4,#tanker5").show();
	}
	
	if(val==3)
	{
		$("#drycargo1,#drycargo2,#drycargo3,#drycargo4,#drycargo5,#drycargo6,#drycargo7,#drycargo8").show();
		$("#tanker1,#tanker2,#tanker3,#tanker4,#tanker5").hide();
		$("#gas1,#gas2,#gas3,#gas4,#gas5,#gas6").hide();
	}
	
}

function Del_Upload(var1)
{
	jConfirm('Are you sure you want to remove this attachment permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_file_"+var1).remove();	
		 }
	else{return false;}
	});
}

</script>
    </body>
</html>