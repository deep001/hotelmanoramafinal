<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->InsertWSPCRecords();
	header('Location:./wspc.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;World Scale Port Combinations</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="wspc.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            ADD NEW WORLD SCALE PORT COMBINATION
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Port(s) From
                            <address>
                             <select data-placeholder="Choose a module here..." class="form-control chzn-select" multiple name="selP_From[]" id="selP_From" onChange="getFromPort()">
							<?php	$obj->getPortListWSPC();?>
							</select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Port(s) To
                            <address>
                              <select data-placeholder="Choose a module here..." class="chzn-select" multiple name="selP_To[]" id="selP_To" onChange="getToPort()">
								<?php	$obj->getPortListWSPC();?>
							  </select>
                            </address><span id="loader1" ></span>
                        </div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
                            WS Flat Rate (USD/MT)
                            <address>
                               <input type="text" name="txtWSFR" id="txtWSFR" class="form-control" placeholder="WS Flat Rate" autocomplete="off" value="" >
                               
                            </address>
                        </div><!-- /.col -->
					</div>
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Fixed Diff
                            <address>
                               <input type="text" name="txtFixedDiff" id="txtFixedDiff" class="form-control" placeholder="Fixed Diff" autocomplete="off" value="" >
                             </address><span id="loader4" ></span>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Status
                            <address>
                              <select name="selStatus" class="form-control" id="selStatus"  >
								<?php 
								$obj->getUserStatusList();
								?>
							</select>
                            </address><span id="loader2" ></span>
                        </div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
                            Remarks
                            <address>
                              <textarea class="form-control" name="txtRemarks" id="txtRemarks" placeholder="Summary.." style="overflow: hidden; word-wrap: break-word; resize: horizontal;"></textarea>
                            </address><span id="loader3" ></span>
                        </div><!-- /.col -->
					</div>
					<div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/chosen.css" rel="stylesheet" type="text/css" />
<script src="../../js/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$(".areasize").autosize({append: "\n"});
$("#selP_From,#selP_To").chosen();
$("#frm1").validate({
	rules: {
		txtWSFR:"required",
		txtFixedDiff:"required",
		},
	messages: {	
		txtWSFR:"*",
		txtFixedDiff:"*",
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

});

function getFromPort()
{
	if($('#selP_To').val() != null)
	{
		$('#txtWSFR,#txtFixedDiff').val("");
		$("#loader1,#loader2,#loader3,#loader4").show();
		$("#loader1,#loader2,#loader3,#loader4").html('<img src="../../img/ajax-loader.gif" />');
		$.post("options.php?id=7",{selFPort:""+$("#selP_From").val()+"",selTPort:""+$("#selP_To").val()+""}, function(html) {
		var var1 = html.split("#");
		$('#txtWSFR').val(var1[0]);
		$('#selStatus').val(var1[1]);
		$('#txtRemarks').val(var1[2]);
		$('#txtFixedDiff').val(var1[3]);
		$("#loader1,#loader2,#loader3,#loader4").hide();
		});
	}
	else
	{
		$('#selP_To').val("");
	}
}

function getToPort()
{
	if($('#selP_From').val() != null)
	{
		$('#txtWSFR,#txtFixedDiff').val("");
		$("#loader1,#loader2,#loader3,#loader4").show();
		$("#loader1,#loader2,#loader3,#loader4").html('<img src="../../img/ajax-loader.gif" />');
		$.post("options.php?id=7",{selFPort:""+$("#selP_From").val()+"",selTPort:""+$("#selP_To").val()+""}, function(html) {
		var var1 = html.split("#");
		$('#txtWSFR').val(var1[0]);
		$('#selStatus').val(var1[1]);
		$('#txtRemarks').val(var1[2]);
		$('#txtFixedDiff').val(var1[3]);
		$("#loader1,#loader2,#loader3,#loader4").hide();
		});
	}
	else
	{
		$('#selP_From').val("");
	}
}

function getValid()
{
	var val1 = $('#selP_From').val();
	var val2 = $('#selP_To').val();
	if(val1 != null && val2 !=null)
	{
		return true;
	}
	else
	{
		jAlert('Select atleast one From Port & one To Port', 'Alert');
		return false;
	}
}

</script>
    </body>
</html>