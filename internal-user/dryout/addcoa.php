<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertCOADetails();
	header('Location:./coa_list.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
$rigts    = explode(',',$obj->getUserRights($_SESSION['uid'],$_SESSION['moduleid'],14));
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(14); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-truck"></i>&nbsp;Contract of Affreightment&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Contract of Affreightment</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="coa_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">	
				<select id="hiddenSel1" name="hiddenSel1" style='display:none;'/>	
				<?php $obj->getPortList(); ?>
				</select>	
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             CREATE A NEW COA    
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                            COA ID
                            <address>
                               <input type="text" name="txtCOAid" id="txtCOAid" style="background-color:#E1E1E1;" class="form-control"  placeholder="COA ID" value="<?php echo $obj->getMessageNumberForCOA();?>" autocomplete="off" readonly/>
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
                            COA No.
                            <address>
                               <input type="text" name="txtCOANo" id="txtCOANo" class="form-control"  placeholder="COA No." autocomplete="off"/>
                            </address>
                        </div>
						<div class="col-sm-3 invoice-col">
						    COA Date
                            <address>
                               <input type="text" name="txtCOADate" id="txtCOADate" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy" />
                            </address>
                        </div>
						<div class="col-sm-3 invoice-col">
                            COA Route
                            <address>
                                <select  name="selCOAroute" class="form-control" id="selCOAroute">
								<?php 
                                $obj->getCOArouteNamesList();
                                ?>
                                </select>
                            </address>
                        </div>
					</div>
                    
                    <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                          Charterer
                            <address>
                                <select  name="selCharterer" class="form-control" id="selCharterer">
								<?php 
								$obj->getVendorListNewForCOA(7);
								?>
                                </select>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                          Owner
                            <address>
                                <select  name="selOwner" class="form-control" id="selOwner">
								<?php 
                                $obj->getVendorListNewForCOA(11);
                                ?>
                                </select>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                          Broker
                            <address>
                                <select  name="selBroker" class="form-control" id="selBroker">
								<?php 
								$obj->getVendorListNewForCOA(12);
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
					</div>
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                           Business Type
                            <address>
                               <select  name="selBType" class="form-control required" id="selBType" onChange="getVesselTypeList(),getCargoTypList();">
                               <option value="">----Select from list----</option>
								<?php 
                                $obj->getBusinessTypeList1();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
                          Vessel Type
                            <address>
                                <select  name="selVesselType" class="form-control" id="selVesselType">
								<?php 
								$obj->getVesselTypeList();
								?>
                                </select>
                            </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
                          Load Options
                            <address>
                                <select  name="selLoadOpt" class="form-control" id="selLoadOpt">
								<?php 
								$obj->getLoadOptionsNameList();
								?>
                                </select>
                            </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
                            Total Shipments(No.'s)
                            <address>
                               <input type="text" name="txtNoofShipment" id="txtNoofShipment" class="form-control"  placeholder="Total Shipments(No.'s)" autocomplete="off"/>
                            </address>
                        </div>
					</div>                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Cargo
                            <address>
                                <select  name="selCargo" class="form-control" id="selCargo">
								<?php 
								$obj->getCargoNameList();
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <!-- /.col -->
                        
                        <div class="col-sm-4 invoice-col">
                           Tolerance ( % )
                            <address>
                               <input type="text" name="txtTolerance" id="txtTolerance" class="form-control" autocomplete="off" placeholder="Tolerance ( % )" />
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
                            COA Notice(Days)
                            <address>
                                <input type="text" name="txtCOAnoticeDays" id="txtCOAnoticeDays" class="form-control"  placeholder="COA Notice(Days)" autocomplete="off" />
                             </address>
                        </div>
                    </div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                            Min Guaranteed Qty(MT)
                            <address>
                                <input type="text" name="txtMinGuarQty" id="txtMinGuarQty" class="form-control"  placeholder="Min Guaranteed Qty(MT)" autocomplete="off" />
                             </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                           Load Port ETA Notices
                            <address>
                                <input type="text" name="txtLdPrtETAnot" class="form-control" id="txtLdPrtETAnot" placeholder="Load Port ETA Notices" autocomplete="off"/>
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
                           Vessel Substitutions
                            <address>
                                <select  name="selVslSubs" class="form-control" id="selVslSubs">
								<?php 
								$obj->getVesselSubstitutionList();
								?>
                                </select>
                            </address>
                        </div>
                    </div>
					<div class="box-body table-responsive">
					    <table width="100%" class="table table-bordered table-striped">
							<tr>
								<td></td>
								<td>Min Guaranteed Qty(CBM or MT)</td>
								<td>Ex - Port</td>
								
							</tr>
						
						<tbody id="minGuarenteedBody">
						    <tr id="minGuarenteedBodyRow_1">
								<td>
									&nbsp;&nbsp;&nbsp;&nbsp;<a href="#tb1" onClick="removeMinGuarenteedBody(1);" ><i class="fa fa-times" style="color:red;"></i></a>
								</td>
								<td>
									<input type="text" name="txtminGuarenteed_1" id="txtminGuarenteed_1" class="form-control" autocomplete="off" value="" placeholder="Min Guaranteed Vol/Voyage(CBM)" />
								</td>
								<td>
									<select name="selExPort_1" id="selExPort_1" class="form-control">
									<?php 
									$obj->getPortList();
									?>
									</select>
								</td>
							</tr>	
						</tbody>
						
						<tfoot>	
							<tr>
								<td colspan="3" style="padding-top:10px;">
										<button type="button" class="btn btn-primary btn-flat" onClick="addOthMinGuarenteedBody();">Add</button>
										<input type="hidden" name="hddnOtherID" id="hddnOtherID" value="1"/>
									
								</td>
							 </tr>
						  </tfoot>
					   </table>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							COA Duration
							<address>
							   <textarea class="form-control areasize" name="txtCOAduration" id="txtCOAduration" rows="1" placeholder="COA Duration" ></textarea>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
                           Start Date
                            <address>
                               <input type="text" name="txtStartDate" id="txtStartDate" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy" />
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
                           	End Date
                            <address>
                               <input type="text" name="txtEndDate" id="txtEndDate" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy" />
                            </address>
                        </div>
                        <div class="col-sm-2 invoice-col">
                           	Working Currency
                            <address>
                               <select  name="selCurrency" class="select form-control" id="selCurrency" onChange="getCurrency();"><?php $obj->getCurrencyList();?></select>
                            </address>
                        </div>
					</div>
                  
				 <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Freight Details
							<address>
							   <textarea class="form-control areasize" name="txtFreightDetails" id="txtFreightDetails" rows="2" placeholder="Freight Details" ></textarea>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Load Port Details
							<address>
							   <input type="text" class="form-control" name="txtLPDetails" id="txtLPDetails" placeholder="Load Port Details" autocomplete="off" />
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Discharge Port Details
							<address>
							   <input type="text" class="form-control" name="txtDPDetails" id="txtDPDetails" placeholder="Discharge Port Details" autocomplete="off"/>
							</address>
						</div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Contract FO Price(<span id="curr_1">USD</span>/MT)
							<address>
							   <input type="text" class="form-control numeric" name="txtFOPrice" id="txtFOPrice" placeholder="0.00" autocomplete="off" />
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							BAF
							<address>
							   <input type="text" class="form-control numeric" name="txtBAF" id="txtBAF" placeholder="0.00" autocomplete="off" />
							</address>
						</div><!-- /.col -->
						
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Demurrage & Laytime
							<address>
                               <textarea class="form-control areasize" name="txtDemLaytime" id="txtDemLaytime" rows="2" placeholder="Demurrage & Laytime"></textarea>
							   
							</address>
						</div><!-- /.col -->
						<div class="col-sm-8 invoice-col">
						Overall Remarks
						<address>
						   <textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" rows="3" placeholder="Overall Remarks..." ></textarea>
						</address>
						</div><!-- /.col -->
					</div>
					<div class="row invoice-info" style="width:100%;float:left;padding-left:1.5%;">
						Attach COA recap<br>
						<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" style="width:23%;">
                        <i class="fa fa-paperclip"></i> Attachment
                        <input type="file" class="form-control" multiple name="attach_file[]" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                        </div>
				    </div>
					
					<div class="box-footer" align="right">
					<input type="hidden" name="txtCRMFILE" id="txtCRMFILE" value="" />
					<input type="hidden" name="txtCRMNAME" id="txtCRMNAME" value="" />
                    <?php if(in_array(2, $rigts)){?>
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Save as Draft</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-success btn-flat" onClick="return getValidate(1);">Send to Checker</button> 
					<?php }?>
					<input type="hidden" name="action" value="submit" />
					<input type="hidden" name="upstatus" id="upstatus"/>
					</div>
					
			</form>				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 

	$("#txtCQty,#txtMinGuarQty,#txtSQty,#txtTolerance,#txtNoofShipment,#txtPeriodDays,#txtCOAnoticeDays,#txtBAF,#txtFOPrice").numeric();

	$('#txtEndDate,#txtStartDate,#txtCOADate').datepicker({
		format: 'dd-mm-yyyy',
		autoclose:true
	});
	
	$(".areasize").autosize({append: "\n"});

	$("#frm1").validate({
		rules: {
			txtCOADate:{required:true},
			txtCOAid:{required:true},
			selCharterer:{required:true},
			txtSQty:{required:true},
			txtCOANo:{required:true},
			selOwner:{required:true},
			txtNoofShipment:{required:true},
			selBroker:{required:true},
			txtCQty:{required:true},
			selVesselType:{required:true},
			txtPeriodDays:{required:true},
			selCargo:{required:true},
			txtStartDate:{required:true},
			txtEndDate:{required:true},
			selCurrency:{required:true}
			},
		messages: {
			
			},
			submitHandler: function(form)  {
					jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
				if(r){
					jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
					$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
					$("#popup_content").css({"background":"none","text-align":"center"});
					$("#popup_ok,#popup_title").hide();  
					frm1.submit();
					}
					else{return false;}
				});
			}
		});

});

function getVesselTypeList()
{
    var vesseltypelist = <?php echo $obj->getVesselTypeListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selVesselType');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selVesselType");
	$.each(vesseltypelist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selVesselType");
	});
}


function getCargoTypList()
{
    var cargolist = <?php echo $obj->getCargoListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selCargo');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selCargo");
	$.each(cargolist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selCargo");
	});
}

function getValidate()
{
	
}

function addOthMinGuarenteedBody()
{
    var idd = $("#hddnOtherID").val();
	if($("#txtminGuarenteed_"+idd).val()!='' && $("#selExPort_"+idd).val()!='')
	{
	    idd = parseInt(idd) + 1;
	    $('<tr id="minGuarenteedBodyRow_'+idd+'"><td>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#tb1" onClick="removeMinGuarenteedBody('+idd+');"><i class="fa fa-times" style="color:red;"></i></a></td><td><input type="text" name="txtminGuarenteed_'+idd+'" id="txtminGuarenteed_'+idd+'" class="form-control" autocomplete="off" value="" placeholder="Min Guaranteed Vol/Voyage(CBM)" /></td><td><select name="selExPort_'+idd+'" id="selExPort_'+idd+'" class="form-control"></select></td></tr>').appendTo("#minGuarenteedBody");
		$("#hddnOtherID").val(idd);
		$("#selExPort_"+idd).html($("#hiddenSel1").html());
	}
	else
	{
		jAlert('Please fill previous fields', 'Alert');
		return false;
	}
}


function removeMinGuarenteedBody(var1)
{
		jConfirm('Are you sure you want to remove this row permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#minGuarenteedBodyRow_"+var1).remove();		
				 }
			else{return false;}
			});
}

function getValidate(val)
{
	$("#upstatus").val(val);
	return true;
}

function getCurrency()
{
	if($("#selCurrency").val()!='')
	{
		$("#curr_1").text($("#selCurrency").val());
	}
	else
	{
		$("#curr_1").text('USD');
	}
}

</script>
    </body>
</html>