<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
	if($_REQUEST['txtComid1'] != "")
	{
 		$msg = $obj->insertActualCostSheetName();
	}
	else
	{
		$msg = $obj->completeVoyageEntry($_REQUEST['txtComid2']);
	}
	header('Location:./vessel_in_post_ops.php?msg=3');
 }
if (@$_REQUEST['action1'] == 'submit1')
{
	if (@$_REQUEST['txtuid'] == 1)
	{
		$msg = $obj->insertPerfAtSeaUploadDetails();
		header('Location:./vessel_in_post_ops.php?msg='.$msg);
	}
	else if (@$_REQUEST['txtuid'] == 2)
	{
		$msg = $obj->insertPerfAtPortUploadDetails();
		header('Location:./vessel_in_post_ops.php?msg='.$msg);
	}
}

if (isset($_REQUEST['selYear']) && $_REQUEST['selYear']!="")
{
	$selYear = $_REQUEST['selYear'];
}
else
{
	$selYear = date('Y',time());
}

if (isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$selBType = $_SESSION['selBType'] = $_REQUEST['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	unset($_SESSION['selBType']);
	$selBType = '';
}
$pagename = basename($_SERVER['PHP_SELF']);

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];	
$rights = $obj->getUserRights($uid,$moduleid,5);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops VC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops VC&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Vessels in Post Ops VC</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> In Post Ops at a glance added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> In Post Ops at a glance sheet successfully create.
				</div>
				<?php }?>
                <?php if($msg == 4){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> new sheet added successfully.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }?>
				<?php if($msg == 3){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Nomination sent to "History".
				</div>
				<?php }}?>
				<div class="box box-primary">
					<h3 style=" text-align:center;">In Post Ops at a glance - VC</h3>
				
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                            <div class="col-xs-6">
                            </div>
                            <div class="col-xs-3">
                                 <select name="selBType" id="selBType" class="form-control" onChange="getSubmit();">
                                 <option value="">---Select Business Type---</option>
								   <?php echo $obj->getBusinessTypeList1($selBType);?>
                                  </select>                  
							</div><!-- /.col -->
						    <div class="col-xs-3">
								  <select name="selYear" id="selYear" class="form-control" onChange="getSubmit();">
                                   <?php echo $obj->getYearList($selYear);?>
                                  </select>                      
							</div><!-- /.col -->
						<div style="height:10px;">
							<input type="hidden" name="txtComid2" id="txtComid2" value="zsd" />
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<input type="hidden" name="txtFileName" id="txtFileName" value="" />
							<input type="hidden" name="action" id="action" value="submit" />
						</div>
                
						<div class="box-body table-responsive" style="overflow:auto;">
							<table class="table table-bordered table-striped" id='in_ops_at_a_glance'>
								<thead>
									<tr valign="top">
                                        <th align="left">FVF/ Voyage Docs</th>
										<th align="left">Nom ID</th>
                                        <th align="left">Business Type</th>
										<th align="left">Material Name</th>
										<th align="left">Vessel</th>
                                        <th align="left">CP Date</th>
                                        <th align="left">Fixture Date/ (Voyage No.)</th>
                                        <th align="left">Check List</th>
										<th align="left">Voyage Financials</th>
                                        <th align="left">PDA Request</th>
										<th align="left">Appoint Agent</th>
										<th align="left">SOF</th>	
                                        <th align="left">Laytime</th>			
                                        <th align="left">Payment Grid</th>
                                        <?php if($rights == 1){ ?>
										<th align="center">Deactivate</th>
										<?php } ?>
										<th align="center">Re&nbsp;-&nbsp;Del</th>
										<?php if($rights == 1){ ?>
										<th align="center">Complete</th>
										<?php } ?>
									</tr>
								</thead>
									<?php 
									if($selBType!='')
									{
									$sql = "select * from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS=2 and year(freight_cost_estimete_master.ADD_ON_DATE)='".$selYear."' and freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' and COAAID is NULL order by date(freight_cost_estimete_master.FINAL_DATETIME) desc"; 

									$res = mysql_query($sql);
									$rec = mysql_num_rows($res);
									$i=1;
									$submit = 0;
								   if($rec == 0)
									{
										echo '<tbody>';
								
										echo '</tbody>';
									}else{
									?>
								<tbody>
								<?php while($rows = mysql_fetch_assoc($res)) 
								{ 
								$cp_date = $obj->getOpenCargoPositionData($obj->getCompareEstimateData($rows['COMID'],"OPEN_CARGOID"),'COA_DATE');
								if($cp_date!="0000-00-00" && $cp_date!=""){$cp_date = date('d-m-Y',strtotime($cp_date));}else{$cp_date = "";}
								$ESTIMATE_TYPE = $obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE");
								$materialid = $obj->getCompareEstimateData($rows['COMID'],"CARGO_ID");
								$fixture_date = $obj->getCompareEstimateData($rows['COMID'],"TRANS_DATE");
								if($fixture_date!="0000-00-00" && $fixture_date!=""){$fixture_date = date('d-m-Y',strtotime($fixture_date));}else{$fixture_date = "";}?>
								
								<tr style="font-size:11px;">
									<td>
                                        <a href="viewestimate.php?id=<?php echo $rows['FCAID'];?>&rttype=3" style="color:green;" title="Click me" >FVF</a><br/><br/>
                                        <a href="documents.php?comid=<?php echo $rows['COMID'];?>&page=2" style="color:green;" title="Click me" >Docs</a>
                                    </td>
                                    <td>
										<?php echo $rows['MESSAGE'];?>
                                    </td>
                                    <td>
                                        <?php echo $obj->getEstimateList($ESTIMATE_TYPE);?>
                                    </td>
                                    <td>
										<?php echo $obj->getCargoNameListForMultipleNamemultiple($materialid);?>
                                    </td>
                                    <td>
                                        <?php echo $obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME");?>/<br/> <?php echo $obj->getCompareEstimateData($rows['COMID'],"VESSEL_TYPE");?>
                                    </td>
									<td>
										<?php echo $cp_date;?>
									</td>
                                    <td>
										<?php echo $fixture_date;?>/<?php echo $obj->getCompareEstimateData($rows['COMID'],"VOYAGE_NO");?>
                                    </td>
									<td>
										<?php echo $obj->getFreightFixtureBasedOnID($rows['FIXTURETYPEID']);?><br/><br/>
                                        <?php if($rows['FIXTURETYPEID'] == 1){?>
                                            <a href="check_list_tci.php?comid=<?php echo $rows['COMID'];?>&page=2" style="color:red;" title="Click me" >Check List</a>	
                                        <?php }else{?>
                                            <a href="check_list.php?comid=<?php echo $rows['COMID'];?>&page=2" style="color:red;" title="Click me" >Check List</a>	
                                        <?php }?>
                                        <?php if($rows['FIXTURETYPEID'] == 2){?><br/><br/>
                                            
                                        <?php }?>
                                        
                                    </td>
                                    
									<td style="text-align:center;">
                                        
                                        <?php
                                            $sql1 = "select * from cost_sheet_name_master where COMID='".$rows['COMID']."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";

                                            $res1 = mysql_query($sql1);
                                            $rec1 = mysql_num_rows($res1);
                                            if($rec1 > 0)
                                            {
                                                while($rows1 = mysql_fetch_assoc($res1))
                                                {
                                                    if($rows1['PROCESS'] != "EST")
                                                    {
                                                        if($_SESSION['company'] == 2)
                                                        {
                                                            $href = "./cost_sheet_tci_lpg.php?comid=".$rows['COMID']."&cost_sheet_id=".$rows1['COST_SHEETID']."&page=2";
                                                        }
                                                        else
                                                        {
                                                            $href = "./cost_sheet_tci.php?comid=".$rows['COMID']."&cost_sheet_id=".$rows1['COST_SHEETID']."&page=2";
                                                        }
                                                        echo "<a href='".$href."'>".str_replace(' ','&nbsp;',$rows1['SHEET_NAME'])."</a><br/>";
                                                        $curr_cst = $rows1['COST_SHEETID'];
                                                    }
                                                    else
                                                    {
                                                        $curr_cst = $obj->getCostSheetFixtureID($rows['COMID'],"SHEET_NO");
                                                    }
                                                }
                                            }
											if($rights == 1){
											$fcaid = $obj->getLatestCostSheetID($rows['COMID']);	
											if($obj->getEstimateData($fcaid,"FINAL_STATUS")==1)
											{
											?>
												<br/><button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-modal1" type="button" title="Add New CS" onClick="openWin1(<?php echo $rows['COMID'];?>);">A</button>
											<?php }else{?>
											<br/><button class="btn btn-primary btn-flat" type="button" title="Add New CS" onClick="msg();">A</button>
											<?php }}?>	
                                           
                                    </td>
                                    <td>
                                        <a href="agency_letter_generation.php?comid=<?php echo $rows['COMID'];?>&tab=0&page=2">Generate Agency Letter</a>
                                    </td>
                                    <td>
                                        <a href="nominate_agent.php?comid=<?php echo $rows['COMID'];?>&page=2">Nominate Agent</a>
                                    </td>			
                                    <td>
                                        <a href="sof.php?comid=<?php echo $rows['COMID'];?>&page=2">SOF</a>		
                                    </td>
                                    <td>
                                        <?php if($obj->getFreightEstimationStatus($rows['COMID']) == 2 )
                                            {?>
                                        <a href="laytime_calculation.php?comid=<?php echo $rows['COMID'];?>&page=2" style="color:blue;" title="Click me" >Calculations</a>
                                        <?php }?>
                                    </td>
			
									<td>
                                        <a href="payment_grid.php?comid=<?php echo $rows['COMID'];?>&page=2" >View</a><br><br>
                                        <!--<a href="profit_head.php?comid=<?php echo $rows['COMID'];?>&page=2" >Headwise Detail</a>
                                        <br><br>-->
                                        <a href="piclubdeclaration.php?comid=<?php echo $rows['COMID'];?>&page=2" >P & I Club Declaration</a>
                                        
                                    </td>
                                    
                                    <?php if($rights == 1){ ?>
                                    <td style="text-align:center;">
                                        <a href="#A" title="Deactivate entry" onClick="getValidate2(<?php echo $rows['COMID'];?>,<?php echo $i;?>);">
                                            <i class="fa fa-times" style="color:red;"></i>
                                        </a>
                                    </td>
                                    <?php } ?>
                                    
                                    <td>
									<?php 
                                        if($obj->getFreightEstimationStatus($rows['COMID']) == 1)
										{
                                            $re_del_date = $obj->getReDelDate($rows['COMID']);
											$alarm = (int)$obj->getNomDetailsData($rows['COMID'],"ALARM");
											$cost_sheet_id = $obj->getLatestCostSheetID($rows['COMID']);
											$fcaid = $obj->getCurrentCostSheetFCAID($rows['COMID'],$cost_sheet_id);
											$ttl_days = (int)$obj->getFreightEstimationTotalRecords($fcaid,"TTL_DAYS");
								
											$redate = strtotime($re_del_date);
											$days = $ttl_days - $alarm;
											$date = strtotime("+".$days." day", $redate);
											$date1 = strtotime("+".$ttl_days." day", $redate);
											if($re_del_date != "" && date('Y-m-d', $date) <= date("Y-m-d",time()))
											{
											$title = "  Vessel Name&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;".$obj->getVesselIMOData($obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID"),"VESSEL_NAME")."<br>  Re - Del&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;".date("d M Y",$date1);
										?>
										<a href="#$i" title="<?php echo $title;?>" id="alarm_<?php echo $i;?>"><button class="ox-button2" id="inner-login-button"  type="button"><b><span id="d27e53" style="background-color:red; color:#fff;" >&nbsp;Alert&nbsp;</span></b></button></a>
                                        <?php }}?>
                                    </td>
									<?php if($rights == 1){ ?>
									<td>
										<button class="btn btn-primary btn-flat" id="inner-login-button"  type="button" title="Push to History" onClick="getValidate2(<?php echo $rows['COMID'];?>);"><b><span id="d27e53">History</span></b></button>
									</td>
                                    <?php } ?>
								</tr>
								<?php $i++;}}?>
                                <?php }?>
								</tbody>
							</table>
						</div>
					</form>
					<div class="modal fade" id="compose-modal1" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body">
									<div class="box box-primary">
										<div class="box-body no-padding">
											<form name="frm2" id="frm2" method="post" enctype="multipart/form-data" action="<?php echo $pagename;?>">
												<div class="row invoice-info" style="padding:2%">
													<div class="col-sm-12 invoice-col">
														Please enter Voyage Financials Name and Submit
														<address>
															<input type="text" name="txtFile" id="txtFile" class="form-control" autocomplete="off" value="<?php echo $obj->getNomDetailsData($comid,"FREIGHT_RATE");?>" placeholder="Voyage Financials Name" />
															<input type="hidden" name="txtComid1" id="txtComid1" />
														</address>
													</div><!-- /.col -->
												</div>
												<div class="row invoice-info">
													<div class="col-sm-12 invoice-col" style="text-align:center;">
														 <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
														 <input type="hidden" name="action" id="action" value="submit" />
													</div><!-- /.col -->
												</div>
											</form>
										</div><!-- /.box-body -->
									</div>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>					
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>

<script type="text/javascript">
$(document).ready(function(){ 

$("#in_ops_at_a_glance").dataTable({
    "iDisplayLength": 100
    });

$(function() {
      $("[id^=alarm_]").tooltip();
});

//for uploading........
 $('#photoimg').live('change', function(){ 
 			$("#cpDiv_"+$("#txtVar").val()).append("<span id='labelDiv_"+$("#txtVar").val()+"_"+$("#txtID_"+$("#txtVar").val()).val()+"'>abc</span>");
			$("#labelDiv_"+$("#txtVar").val()+"_"+$("#txtID_"+$("#txtVar").val()).val()).html('<img src="../../images/loader.gif" alt="Uploading...." width="20" height="10"  />');
			$("#imageform").ajaxForm({
						target: '#labelDiv_'+$("#txtVar").val()+'_'+$("#txtID_"+$("#txtVar").val()).val()
			}).submit();
			var id = $("#txtID_"+$("#txtVar").val()).val();
			id = (id - 1) + 2;
			$("#txtID_"+$("#txtVar").val()).val(id);
			$.modal.close();
 			});
//....................
}); 
function getData()
{
	$("#in_ops_at_glance tbody").empty();
	$("#in_ops_at_glance tbody").html('<tr><td height="200" colspan="17" align="center" ><img src="../../images/loading.gif" /></td></tr>');
	$.post("options.php?id=43",{selVName:""+$("#selVName").val()+""}, function(html) {
	$("#in_ops_at_glance tbody").empty();
	$("#in_ops_at_glance tbody").append(html);
	});
}

function openWin1(comid)
{
	$('#basic-modal-content').modal();
	$("#txtComid1").val(comid);
	$("#simplemodal-container").css({"height":"150px"});
}

function getValidate()
{
	if($("#txtFile").val() != "")
	{
		return true;
	}
	else
	{
		jAlert('Please fill the file name', 'Alert');
		return false;
	}
}

function msg()
{
	jAlert('Please make sure the last cost sheet is Submit to Accounts', 'Alert');
}

function openWin2(var1,comid)
{
	$('#basic-modal-content1').modal();
	$("#txtVar").val(var1);
	$("#txtMappingid").val(comid);
	$("#simplemodal-container").css({"width":"300px","height":"50px"});
}	

function openWin(comid,vesselid,id)
{
	$('#basic-modal').modal();
	$("#txtMapid").val(comid);
	$("#txtVesselid").val(vesselid);
	$("#txtuid").val(id);
	$("#simplemodal-container").css({"width":"300px","height":"50px"});
}

function getValid()
{
	if($('#txtfile').val() == '')
	{
		jAlert('Please choose the file', 'Alert');
		return false;
	}
	else
	{
		var name = $('#txtfile').val();
		name = name.split('.');
		if(name[1] == 'txt')
		{
			return true;
		}
		else
		{
			jAlert('Please choose only .txt file', 'Alert');
			$('#txtfile').val('');
			return false;
		}
	}
}

function getPdfForSea(comid,vesselid)
{
	location.href='allPdf.php?id=12&comid='+comid+'&vesselid='+vesselid;
}

function getValidate2(var1)
{
	jConfirm('Are you sure to send this Nom ID to history ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtComid2").val(var1);
			document.frm1.submit();
		}
		else{return false;}
		});	
}

function getSubmit()
{ 
    $("#action").val("");
	document.frm1.submit();
}

</script>
		
</body>
</html>