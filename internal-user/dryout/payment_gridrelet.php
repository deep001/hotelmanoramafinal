<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];

$id = $obj->getLatestCostSheetIDRelet($comid);
$obj->viewCargoReletEstimationTempleteRecordsNew($id);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(19); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops COA Cargo Relet&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops COA Cargo Relet&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Cargo Relet - COA</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
					
					<div align="right">
						
						<a href="cargo_relet_coa_ops.php"><button class="btn btn-info btn-flat">Back</button></a></div>
					<div style="height:10px;">&nbsp;</div>
					
				<div class="box box-primary">
					<h3 style=" text-align:center;">Payment / Invoice Grid : <?php echo $obj->getVesselIMOData($obj->getCompareCargoReletData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></h3>
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>					 
						    
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><strong>Cargo IN</strong></h3>
                                </div>
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="50%">Name</th>
                                                <th width="25%">Vendor</th>
                                                <th width="25%">&nbsp;</th>											
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Freight & Others</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($obj->getFun99());?></td>
                                                <td>
                                                <?php if($obj->getFun99()!=''){?>
                                                    <a href="invoice_relet.php?comid=<?php echo $comid;?>" ><button class="btn btn-info btn-flat" type="button">Invoice</button></a>&nbsp;&nbsp;
                                                <?php }?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Bunker Surcharge</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($obj->getFun100());?></td>
                                                <td>
                                                <?php if($obj->getFun100()!=''){
													$id = $comid.',0,'.$obj->getFun100().','.$obj->getFun34().',Bunker Surcharge';
													$invtitle = "Bunker Surcharge Invoice";
												    $amounttitle = "Bunker Surcharge";
													?>
                                                    <a href="invoice_relet_others.php?id=<?php echo $id;?>&name=<?php echo $invtitle;?>&amounttitle=<?php echo $amounttitle;?>&page=1"><button class="btn btn-info btn-flat" type="button">Invoice</button></a>&nbsp;&nbsp;
                                                <?php }?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Demurrage</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($obj->getFun101());?></td>
                                                <td>
                                                <?php if($obj->getFun101()!=''){
													$id = $comid.',0,'.$obj->getFun101().','.$obj->getFun35().',Demurrage';
													$invtitle = "Demurrage Invoice";
												    $amounttitle = "Demurrage";
													?>
                                                    <a href="invoice_relet_others.php?id=<?php echo $id;?>&name=<?php echo $invtitle;?>&amounttitle=<?php echo $amounttitle;?>&page=1"><button class="btn btn-info btn-flat" type="button">Invoice</button></a>&nbsp;&nbsp;
                                                <?php }?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
						   
						
							<div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><strong>Cargo OUT</strong></h3>
                                </div>
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="50%">Name</th>
                                                <th width="25%">Vendor</th>
                                                <th width="25%">&nbsp;</th>											
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Freight</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($obj->getFun102());?></td>
                                                <td>
                                                <?php if($obj->getFun102()!=''){
													$id3 = 'Freight'.','.$obj->getFun102().','.$comid.','.$obj->getFun41();
													?>
                                                    <a href="request_payment_relet.php?id=<?php echo $id3;?>&page=<?php echo $page;?>&name=Freight" ><button class="btn btn-warning btn-flat" type="button">Payment Advice</button></a>&nbsp;&nbsp;
                                                <?php }?>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>Demurrage</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($obj->getFun103());?></td>
                                                <td>
                                                <?php if($obj->getFun103()!=''){
													$id3 = 'Demurrage'.','.$obj->getFun103().','.$comid.','.$obj->getFun43();?>
                                                    <a href="request_payment_relet.php?id=<?php echo $id3;?>&page=<?php echo $page;?>&name=Freight"><button class="btn btn-warning btn-flat" type="button">Payment Advice</button></a>&nbsp;&nbsp;
                                                <?php }?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>					
						
						
												
						
						
						
						
                        
						
						
                        
                        
                       
							
						
					</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
		
</body>
</html>