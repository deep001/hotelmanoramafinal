<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertProfitHeadDetails();
	header('Location:./'.$page_link.'?msg='.$msg);
}
$id = $obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($id);
$l_cost_sheet_id = $obj->getCompareEstimateData($comid,"SHEET_NO");
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
					<?php if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}?>
					
					<div align="right">
						<span>Nom ID : <?php echo  $obj->getCompareEstimateData($comid,"VOYAGE_NO");?></span><br/>
						<a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
					<div style="height:10px;">&nbsp;</div>
					
				<div class="box box-primary">
					<h3 style=" text-align:center;">Headwise Detail : <?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></h3>
					
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>					
						<?php if($obj->getFun2() == 2){?>
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Freight Details</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="30%">Name</th>
											<th width="20%">Vendor</th>
											<th width="15%">Cost</th>	
											<th width="15%">Cost in(Revenue)</th>	
											<th width="15%">Cost out (Expense)</th>												
										</tr>
									</thead>
									<tbody>
										<?php 
										if($obj->getFun132() == 1)
										{$num = 1;
										$id = '1'.','.'Freight Details'.','.'0'.','.$obj->getCompareEstimateData($comid,"FGFF_VENDORID").','.$comid.','.$obj->getCompareEstimateData($comid,"FINAL_NET_FREIGHT");
										?>
										<tr>
											<td>Final Nett Freight</td>
											<td><?php echo $obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($comid,"FGFF_VENDORID"));?>                                                <input type="hidden" name="txtFreightVendor_1" id="txtFreightVendor_1" class="form-control" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"FGF_VENDORID");?>"/></td>
											<td><?php echo $obj->getCompareEstimateData($comid,"FINAL_NET_FREIGHT");?></td>
											<td><input type="text" name="txtfreightCostIn_1" id="txtfreightCostIn_1" class="form-control" onKeyUp="getFinalCalculation();" autocomplete="off" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"FREIGHT",1);?>" /></td>
											<td><input type="text" name="txtfreightCostOut_1" id="txtfreightCostOut_1" class="form-control" onKeyUp="getFinalCalculation();" autocomplete="off" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"FREIGHT",1);?>"/></td>
										</tr>
                                        <?php }else{
											$sql1= "select * from freight_cost_estimete_slave7 where FCAID='".$id."'";
											$result1 = mysql_query($sql1);$num = mysql_num_rows($result1);
											$i=0;
											while($rows1 = mysql_fetch_assoc($result1))
											{$i = $i + 1;
												//FCA_SLAVE7ID, FCAID, QTY_VENDORID, AGREED_GROSS_FREIGHT, QUANTITY, GROSS_FREIGHT, BROKERAGE, NET_BROKERAGE, NET_FREIGHT, NET_FREIGHT_PERMT, CURRENCYID, AGREED_GROSS_FREIGHT_LOCAL, EXCHANGE_RATE
											$id = '1'.','.'Freight Details'.','.'0'.','.$rows1["QTY_VENDORID"].','.$comid.','.$rows1["NET_FREIGHT"];
										?>
										<tr>
											<td>Final Nett Freight</td>
											<td><?php echo $obj->getVendorListNewBasedOnID($rows1["QTY_VENDORID"]);?>                                                <input type="hidden" name="txtFreightVendor_<?php echo $i;?>" id="txtFreightVendor_<?php echo $i;?>" class="form-control" value="<?php echo $rows1["QTY_VENDORID"];?>"/></td>
											<td><?php echo $rows1["NET_FREIGHT"];?></td>
											<td><input type="text" name="txtfreightCostIn_<?php echo $i;?>" id="txtfreightCostIn_<?php echo $i;?>" class="form-control" onKeyUp="getFinalCalculation();" autocomplete="off" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"FREIGHT",$i);?>" /></td>
											<td><input type="text" name="txtfreightCostOut_<?php echo $i;?>" id="txtfreightCostOut_<?php echo $i;?>" class="form-control" onKeyUp="getFinalCalculation();" autocomplete="off" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"FREIGHT",$i);?>"/></td>
									  <?php }}?>
									</tbody>
                                    <input type="hidden" name="txtFreightNum" id="txtFreightNum" class="form-control" value="<?php echo $num;?>"/>
								</table>
							</div>
						</div>
						<?php } ?>
						<?php if($obj->getFun2() == 2){?>
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Bunkers Nett Supply</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="30%">Name</th>
											<th width="20%">Vendor</th>
											<th width="15%">Cost</th>	
											<th width="15%">Cost in(Revenue)</th>	
											<th width="15%">Cost out (Expense)</th>											
										</tr>
									</thead>
									<tbody>
										<?php 
										$query = "select * from freight_cost_estimete_slave8 where FCAID='".$obj->getFun1()."'";
										$qres = mysql_query($query);
										$qrec = mysql_num_rows($qres);
										if($qrec > 0)
										{
										$i = 1;
										while($qrows = mysql_fetch_assoc($qres))
										{//FCA_SLAVE8ID, FCAID, BUNKERGRADEID, COST, VENDORID, COST_MT
										?>
										<tr>
											<td><?php echo $obj->getBunkerGradeBasedOnID($qrows['BUNKERGRADEID']);?> Nett</td>
											<td><?php echo $obj->getVendorListNewBasedOnID($qrows['VENDORID']);?></td>
											<td><?php echo $qrows['COST'];?><input type="hidden" name="txtbunkerSupplyVendor_<?php echo $i; ?>" id="txtbunkerSupplyVendor_<?php echo $i; ?>" class="form-control" value="<?php echo $qrows['VENDORID'];?>"/></td>
											<td><input type="text" name="txtbunkerSupplyCostIn_<?php echo $i; ?>" id="txtbunkerSupplyCostIn_<?php echo $i; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"BUNKERNETT",$qrows['BUNKERGRADEID']);?>"/></td>
											<td><input type="text" name="txtbunkerSupplyCostOut_<?php echo $i; ?>" id="txtbunkerSupplyCostOut_<?php echo $i; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"BUNKERNETT",$qrows['BUNKERGRADEID']);?>"/><input type="hidden" name="txtbunker_nett_supid_<?php echo $i; ?>" id="txtbunker_nett_supid_<?php echo $i; ?>" value="<?php echo $qrows['BUNKERGRADEID'];?>"/></td>
										</tr>
										<?php $i++; }}?>
								  <input type="hidden" name="txtbunker_nettid" id="txtbunker_nettid" value="<?php echo $qrec;?>"/>
									</tbody>
								</table>
							</div>
						</div>						
						<?php } ?>
						<?php if($obj->getFun2() == 2){?>
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Owner Related Costs</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="30%">Name</th>
											<th width="20%">Vendor</th>
											<th width="15%">Cost</th>	
											<th width="15%">Cost in(Revenue)</th>	
											<th width="15%">Cost out (Expense)</th>										
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span><input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="<?php echo $num_brok;?>"/></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($rows_brok['VENDORID']);?></td>
											<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM");?><input type="hidden" name="txtBrkCommVendor" id="txtBrkCommVendor" class="form-control" value="<?php echo $rows_brok['VENDORID'];?>"/></td>
											<td><input type="text" name="txtBrkCommCostIn" id="txtBrkCommCostIn" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"BROKERAGE","");?>"/></td>
											<td><input type="text" name="txtBrkCommCostOut" id="txtBrkCommCostOut" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"BROKERAGE","");?>"/></td>
										</tr>
										<?php 
										$sql4 = "select * from fca_tci_owner_related_cost where FCAID='".$obj->getFun1()."'";
										$res4 = mysql_query($sql4);
										$rec4 = mysql_num_rows($res4);
										$i=1;
										while($rows4 = mysql_fetch_assoc($res4))
										{
										?>
										<tr>
											<td><?php echo $obj->getOwnerRelatedCostNameBasedOnID($rows4['OWNER_RCOSTID']);?></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($rows4['VENDORID']);?><input type="hidden" name="txtORCAmtVendor_<?php echo $i; ?>" id="txtORCAmtVendor_<?php echo $i; ?>" class="form-control" value="<?php echo $rows4['VENDORID'];?>"/></td>
											<td><?php echo abs($rows4['AMOUNT']);?></td>
											<td><input type="text" name="txtORCAmtCostIn_<?php echo $i; ?>" id="txtORCAmtCostIn_<?php echo $i; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"OWNERRELATED",$rows4['OWNER_RCOSTID']);?>"/></td>
											<td><input type="text" name="txtORCAmtCostOut_<?php echo $i; ?>" id="txtORCAmtCostOut_<?php echo $i; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"OWNERRELATED",$rows4['OWNER_RCOSTID']);?>"/><input type="hidden" name="txtownercost_<?php echo $i; ?>" id="txtownercost_<?php echo $i; ?>" value="<?php echo $rows4['OWNER_RCOSTID'];?>"/></td>
										</tr>
										<?php $i++;}?>
									<input type="hidden" name="txtORC_id" id="txtORC_id" value="<?php echo $rec4;?>"/>
										
									</tbody>
								</table>
							</div>
						</div>						
						<?php } ?>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Charterers' Costs</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="30%">Name</th>
											<th width="20%">Vendor</th>
											<th width="15%">Cost</th>	
											<th width="15%">Cost in(Revenue)</th>	
											<th width="15%">Cost out (Expense)</th>												
										</tr>
									</thead>
									<tbody>
										<?php 
										if($obj->getFun2() == 1)
										{
											
										}
										else
										{
											$sql2 = "select * from fca_tci_charterer_cost where FCAID='".$obj->getFun1()."'";
										}
										$res2 = mysql_query($sql2);
										$rec2 = mysql_num_rows($res2);
										$i=1;
										while($rows2 = mysql_fetch_assoc($res2))
										{
										?>
										<tr>
										<td><?php echo $obj->getChartererCostNameBasedOnID($rows2['CHARTERER_COSTID']);?></td>
										<td><?php echo $obj->getVendorListNewBasedOnID($rows2['VENDORID']); ?><input type="hidden" name="txtCCCostVendor_<?php echo $i; ?>" id="txtCCCostVendor_<?php echo $i; ?>" class="form-control" value="<?php echo $rows2['VENDORID'];?>"/></td>
										<td><?php echo $rows2['ABSOLUTE'];?></td>
										<td><input type="text" name="txtCCCostIn_<?php echo $i; ?>" id="txtCCCostIn_<?php echo $i; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"CHARTERER",$rows2['CHARTERER_COSTID']);?>"/></td>
										<td><input type="text" name="txtCCCostOut_<?php echo $i; ?>" id="txtCCCostOut_<?php echo $i; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"CHARTERER",$rows2['CHARTERER_COSTID']);?>"/><input type="hidden" name="txtCharterCost_<?php echo $i; ?>" id="txtCharterCost_<?php echo $i; ?>" value="<?php echo $rows2['CHARTERER_COSTID'];?>"/></td>
										</tr>
										<?php $i++;}?>
								  <input type="hidden" name="txtCC_id" id="txtCC_id" value="<?php echo $rec2;?>" />
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Port Costs</h3>
							</div><!-- /.box-header -->
							
							<?php
								$sql11 = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."'";
								$res11 = mysql_query($sql11);
								$rec11 = mysql_num_rows($res11);
							?>
							
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="30%">Name</th>
											<th width="20%">Vendor</th>
											<th width="15%">Cost</th>	
											<th width="15%">Cost in(Revenue)</th>	
											<th width="15%">Cost out (Expense)</th>
											<input type="hidden" name="load_portID" id="load_portID" class="input" size="5" value="<?php echo $rec11;?>"/>	
											</th>										
										</tr>
									</thead>
									<tbody>
									<?php 
									$mysql = "select * from fca_tci_portcosts where FCAID='".$obj->getFun1()."' order by FCA_PORTCOSTID asc";
									
									$myres = mysql_query($mysql);
									$myrec = mysql_num_rows($myres);
									if($myrec > 0)
									{$i=$j=$k=1;
										while($myrows = mysql_fetch_assoc($myres))
										{
											if($myrows['PORT'] == "Load")
											{
											?>
											<tr>
											<td>Load Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?><input type="hidden" name="txtLoadPort_<?php echo $i;?>" id="txtLoadPort_<?php echo $i;?>" class="input" size="10" value="<?php echo $myrows['LOADPORTID'];?>"/></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($myrows['VENDORID']);?></td>
											
											<?php if($obj->getFDA($comid,'LP',$myrows['LOADPORTID']) > 0){?>
											<td><?php echo $obj->getFDA($comid,'LP',$myrows['LOADPORTID']);?></td>
											 <?php }else{?>
											<td><?php echo $myrows['COST'];?></td>
											<?php }?>
											
											<td><input type="text" name="txtLPOSCCostIn_<?php echo $i; ?>" id="txtLPOSCCostIn_<?php echo $i; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"LPCOST",$myrows['LOADPORTID']);?>"/></td>
											<td><input type="text" name="txtLPOSCCostOut_<?php echo $i; ?>" id="txtLPOSCCostOut_<?php echo $i; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"LPCOST",$myrows['LOADPORTID']);?>"/><input type="hidden" name="txtLoadPortid_<?php echo $i; ?>" id="txtLoadPortid_<?php echo $i; ?>" value="<?php echo $myrows['LOADPORTID'];?>" /><input type="hidden" name="txtLPOSCCostVendor_<?php echo $i; ?>" id="txtLPOSCCostVendor_<?php echo $i; ?>" class="form-control" value="<?php echo $myrows['VENDORID'];?>"/></td>
											</tr>
											<?php $i++;}else if($myrows['PORT'] == "Discharge")
											{
											?>
											<tr>
											<td>Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?><input type="hidden" name="txtDisPort_<?php echo $j;?>" id="txtDisPort_<?php echo $j;?>" value="<?php echo $myrows['LOADPORTID'];?>"/></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($myrows['VENDORID']);?></td>
											
											<?php if($obj->getFDA($comid,'DP',$myrows['LOADPORTID']) > 0){?>
											<td><?php echo $obj->getFDA($comid,'DP',$myrows['LOADPORTID']);?></td>
											<?php }else{?>
											<td><?php echo $myrows['COST'];?></td>
											<?php }?>
											
											<td><input type="text" name="txtDPOSCCostIn_<?php echo $j; ?>" id="txtDPOSCCostIn_<?php echo $j; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"DPCOST",$myrows['LOADPORTID']);?>"/></td>
											<td><input type="text" name="txtDPOSCCostOut_<?php echo $j; ?>" id="txtDPOSCCostOut_<?php echo $j; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"DPCOST",$myrows['LOADPORTID']);?>"/><input type="hidden" name="txtDischargePortid_<?php echo $j; ?>" id="txtDischargePortid_<?php echo $j; ?>" value="<?php echo $myrows['LOADPORTID'];?>"/><input type="hidden" name="txtDPOSCCostVendor_<?php echo $j; ?>" id="txtDPOSCCostVendor_<?php echo $j; ?>" class="form-control" value="<?php echo $myrows['VENDORID'];?>"/></td>
											</tr>
											<?php $j++;}else if($myrows['PORT'] == "Transit")
											{
											?>
											<tr>
											<td>Transit Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?><input type="hidden" name="txtTLoadPort_<?php echo $k;?>" id="txtTLoadPort_<?php echo $k;?>" value="<?php echo $myrows['LOADPORTID'];?>"/></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($myrows['VENDORID']);?><input type="hidden" name="txtTPOSCCostVendor_<?php echo $i; ?>" id="txtTPOSCCostVendor_<?php echo $i; ?>" class="form-control" value="<?php echo $myrows['VENDORID'];?>"/></td>
											<td><?php echo $myrows['COST'];?></td>
											<td><input type="text" name="txtTPOSCCostIn_<?php echo $k; ?>" id="txtTPOSCCostIn_<?php echo $k; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"TPCOST",$myrows['LOADPORTID']);?>"/></td>
											<td><input type="text" name="txtTPOSCCostOut_<?php echo $k; ?>" id="txtTPOSCCostOut_<?php echo $k; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"TPCOST",$myrows['LOADPORTID']);?>"/><input type="hidden" name="txtTransPortid_<?php echo $k; ?>" id="txtTransPortid_<?php echo $k; ?>" value="<?php echo $myrows['LOADPORTID'];?>"/></td>
											</tr>
											<?php $k++;}?>
									<?php }}?>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Demurrage Dispatch Ship Owner</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="30%">Name</th>
											<th width="20%">Vendor</th>
											<th width="15%">Cost</th>	
											<th width="15%">Cost in(Revenue)</th>	
											<th width="15%">Cost out (Expense)</th>										
										</tr>
									</thead>
									<tbody>
										<?php 
										$sql121    = "select * from fca_tci_dd_shipowner where FCAID='".$obj->getFun1()."' order by FCA_DD_SHIPOWNERID";
										$result    = mysql_query($sql121);
										$record    = mysql_num_rows($result);
										if($record > 0){
										$k=$p=1;
										while($rows11 = mysql_fetch_array($result)){
										if($rows11['PORT_TYPE']== "Load") {
										?>
										<tr>
											<td>Load Port <?php echo $obj->getPortNameBasedOnID($rows11['LOADPORTID']);?></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($rows11['VENDORID']); ?><input type="hidden" name="txtDDSOLPVendor_<?php echo $i; ?>" id="txtDDSOLPVendor_<?php echo $i; ?>" class="form-control" value="<?php echo $rows11['VENDORID'];?>"/></td>
											<td><?php echo $rows11['COST'];?></td>
											<td><input type="text" name="txtDDSOLPCommCostIn_<?php echo $k; ?>" id="txtDDSOLPCommCostIn_<?php echo $k; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"DLPCOST",$rows11['LOADPORTID']);?>"/></td>
											<td><input type="text" name="txtDDSOLPCommCostOut_<?php echo $k; ?>" id="txtDDSOLPCommCostOut_<?php echo $k; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"DLPCOST",$rows11['LOADPORTID']);?>"/><input type="hidden" name="txtDDSPLPPortid_<?php echo $k; ?>" id="txtDDSPLPPortid_<?php echo $k; ?>" value="<?php echo $rows11['LOADPORTID'];?>"/></td>
										</tr>
										<?php $k++; } else if($rows11['PORT_TYPE'] == "Discharge"){ ?>
										<tr>
											<td>Discharge Port <?php echo $obj->getPortNameBasedOnID($rows11['LOADPORTID']);?></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($rows11['VENDORID']); ?><input type="hidden" name="txtDDSODPVendor_<?php echo $i; ?>" id="txtDDSODPVendor_<?php echo $i; ?>" class="form-control" value="<?php echo $rows11['VENDORID'];?>"/></td>
											<td><?php echo $rows11['COST'];?></td>
											<td><input type="text" name="txtDDSODPCommCostIn_<?php echo $p; ?>" id="txtDDSODPCommCostIn_<?php echo $p; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"DDPCOST",$rows11['LOADPORTID']);?>"/></td>
											<td><input type="text" name="txtDDSODPCommCostOut_<?php echo $p; ?>" id="txtDDSODPCommCostOut_<?php echo $p; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"DDPCOST",$rows11['LOADPORTID']);?>"/><input type="hidden" name="txtDDSPDPPortid_<?php echo $p; ?>" id="txtDDSPDPPortid_<?php echo $p; ?>" value="<?php echo $rows11['LOADPORTID'];?>"/></td>
										</tr>
										<?php $p++; }}} else{ 
										$arr  = $obj->getLoadPortAndDischargePortArrBasedOnMappingidAndProcessWithoutTBN($comid,$obj->getFun1());
										 for($i=0;$i<count($arr);$i++)
										 {
										?>
											<tr>
												<td colspan="3"><?php  echo $arr[$i];?></td>
											</tr>
										<?php }} ?>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Other Misc. Income</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="30%">Name</th>
											<th width="20%">Vendor</th>
											<th width="15%">Cost</th>	
											<th width="15%">Cost in(Revenue)</th>	
											<th width="15%">Cost out (Expense)</th>											
										</tr>
									</thead>
									<tbody>
										<?php 
										if($obj->getFun2() == 1)
										{
											
										}
										else
										{
											$sql3 = "select * from fca_tci_other_misc_cost where FCAID='".$obj->getFun1()."'";
										}
										$res3 = mysql_query($sql3);
										$rec3 = mysql_num_rows($res3);
										$i=1;
										while($rows3 = mysql_fetch_assoc($res3))
										{
										?>
										<tr>
										<td><?php echo $obj->getOtherMiscCostNameBasedOnID($rows3['OTHER_MCOSTID']);?></td>
										<td><?php echo $obj->getVendorListNewBasedOnID($rows3['VENDORID']);?><input type="hidden" name="txtOMCCostVendor_<?php echo $i; ?>" id="txtOMCCostVendor_<?php echo $i; ?>" class="form-control" value="<?php echo $rows3['VENDORID'];?>"/></td>
										<td><?php echo $rows3['AMOUNT'];?></td>
										<td><input type="text" name="txtOMCCostIn_<?php echo $i; ?>" id="txtOMCCostIn_<?php echo $i; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTIN",$l_cost_sheet_id,$comid,"OTHERMISC",$rows3['OTHER_MCOSTID']);?>"/></td>
										<td><input type="text" name="txtOMCCostOut_<?php echo $i; ?>" id="txtOMCCostOut_<?php echo $i; ?>" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getHeadWiseProfitdata("COSTOUT",$l_cost_sheet_id,$comid,"OTHERMISC",$rows3['OTHER_MCOSTID']);?>"/><input type="hidden" name="txtOMCost_<?php echo $i; ?>" id="txtOMCost_<?php echo $i; ?>" value="<?php echo $rows3['OTHER_MCOSTID'];?>"/></td>
										</tr>
										<?php $i++;}?>
								  <input type="hidden" name="txtAOMC_id" id="txtAOMC_id" value="<?php echo $rec3;?>" />
									</tbody>
								</table>
							</div>
						</div>
                        
						<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="30%"></th>
											<th width="20%"></th>
											<th width="15%"></th>	
											<th width="15%">Total Cost in(Revenue)</th>	
											<th width="15%">Total Cost out (Expense)</th>											
										</tr>
									</thead>
									<tbody>
									  
										<tr>
										<td>Totals</td>
										<td></td>
										<td></td>
										<td><input type="text" name="txttotalCostIn" id="txttotalCostIn" class="form-control" readonly/></td>
										<td><input type="text" name="txttotalCostOut" id="txttotalCostOut" class="form-control" readonly/></td>
										</tr>
										
										<tr>
										<td>P/L</td>
										<td style="color:#FF0000;"><?php echo $obj->getFreightCostEstimationData($obj->getFun1(),"PROFITANDLOSS"); ?></td>
										<td></td>
										<td colspan="2"><input type="text" style="text-align:center;" name="txttotalDiff" id="txttotalDiff" class="form-control" readonly/></td>
										
										</tr>
									</tbody>
								</table>
				  		</div>
				  </div>	
						
						<div class="box-footer" align="right">
						<button class="btn btn-primary btn-flat" type="submit">Submit</button>
						<input type="hidden" name="action" value="submit" /><input type="hidden" name="sheetid" id="sheetid" value="<?php echo $l_cost_sheet_id; ?>" />
						</div>
						
					</form>
					
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#txtfreightCostIn,#txtfreightCostOut,#txtBrkCommCostIn,#txtBrkCommCostOut,[id^=txtbunkerSupplyCostIn_],[id^=txtbunkerSupplyCostOut_],[id^=txtBrCommPerCostOut_],[id^=txtBrCommPerCostIn_],[id^=txtBrCommPerCostOut_],[id^=txtORCAmtCostIn_],[id^=txtORCAmtCostOut_],[id^=txtCCCostIn_],[id^=txtCCCostOut_],[id^=txtLPOSCCostIn_],[id^=txtLPOSCCostOut_],[id^=txtDPOSCCostIn_],[id^=txtDPOSCCostOut_],[id^=txtTPOSCCostIn_],[id^=txtTPOSCCostOut_],[id^=txtTPOSCCostOut_],[id^=txtDDSOLPCommCostIn_],[id^=txtDDSOLPCommCostOut_],[id^=txtDDSODPCommCostIn_],[id^=txtDDSODPCommCostOut_],[id^=txtOMCCostIn_],[id^=txtOMCCostOut_],[id^=txtfreightCostIn_],[id^=txtfreightCostOut_]").numeric();
	getFinalCalculation();
});

function getFinalCalculation()
{
	<!------------------- For In ---------------------->
	
	//if($("#txtfreightCostIn").val() == ""){var freightCostIn = 0;}else{var freightCostIn = $("#txtfreightCostIn").val();}
	if($("#txtBrkCommCostIn").val() == ""){var BrkCommCostIn = 0;}else{var BrkCommCostIn = $("#txtBrkCommCostIn").val();}
	
	freightCostIn = 0;
	$('[id^=txtfreightCostIn_]').each(function(index) {
		var rowid = this.id;
		var freightnet = rowid.split('_')[1];
		if($("#txtfreightCostIn_"+freightnet).val() == ""){var freightCostIn1 = 0;}else{ var freightCostIn1 = $("#txtfreightCostIn_"+freightnet).val();}
		freightCostIn = parseFloat(freightCostIn) + parseFloat(freightCostIn1);
	});
	
	var bunkerCostIn = 0;
	$('[id^=txtbunkerSupplyCostIn_]').each(function(index) {
		var rowid = this.id;
		var bunkerSupply = rowid.split('_')[1];
		if($("#txtbunkerSupplyCostIn_"+bunkerSupply).val() == ""){var bunkerSupplyCostIn = 0;}else{ var bunkerSupplyCostIn = $("#txtbunkerSupplyCostIn_"+bunkerSupply).val();}
		bunkerCostIn = parseFloat(bunkerCostIn) + parseFloat(bunkerSupplyCostIn);
	});
	
	var brokerageCostIn = 0; 
	$('[id^=txtBrCommPerCostIn_]').each(function(index){ 
		var rowid = this.id;
		var brCommCost = rowid.split('_')[1];
		if($("#txtBrCommPerCostIn_"+brCommCost).val()==""){ var brkCostPer = 0; } else{ var brkCostPer = $("#txtBrCommPerCostIn_"+brCommCost).val(); }
		brokerageCostIn = parseFloat(brokerageCostIn) + parseFloat(brkCostPer);
	});
	
	var ORCAmtCostIn = 0;
	$('[id^=txtORCAmtCostIn_]').each(function(index){
		var rowid = this.id;
		var ORCAmt = rowid.split('_')[1];
		if($("#txtORCAmtCostIn_"+ORCAmt).val()==""){ var ORCAmtCost = 0; } else{ var ORCAmtCost = $("#txtORCAmtCostIn_"+ORCAmt).val(); }
		ORCAmtCostIn = parseFloat(ORCAmtCostIn) + parseFloat(ORCAmtCost);
	});
	
	var CCCostIn = 0;
	$('[id^=txtCCCostIn_]').each(function(index){
		var rowid = this.id;
		var txtCC = rowid.split('_')[1];
		if($("#txtCCCostIn_"+txtCC).val()==""){ var CCCost = 0; } else{ var CCCost = $("#txtCCCostIn_"+txtCC).val(); }
		CCCostIn = parseFloat(CCCostIn) + parseFloat(CCCost);
	});
	
	var LPOSCCostIn = 0;
	$('[id^=txtLPOSCCostIn_]').each(function(index){
		var rowid = this.id;
		var txtLP = rowid.split('_')[1];
		if($("#txtLPOSCCostIn_"+txtLP).val()==""){ var LPOSCCost = 0; } else{ var LPOSCCost = $("#txtLPOSCCostIn_"+txtLP).val(); }
		LPOSCCostIn = parseFloat(LPOSCCostIn) + parseFloat(LPOSCCost);
	});
	
	var DPOSCCostIn = 0;
	$('[id^=txtDPOSCCostIn_]').each(function(index){
		var rowid = this.id;
		var txtDP = rowid.split('_')[1];
		if($("#txtDPOSCCostIn_"+txtDP).val()==""){ var DPOSCCost = 0; } else{ var DPOSCCost = $("#txtDPOSCCostIn_"+txtDP).val(); }
		DPOSCCostIn = parseFloat(DPOSCCostIn) + parseFloat(DPOSCCost);
	});
	
	var TPOSCCostIn = 0;
	$('[id^=txtTPOSCCostIn_]').each(function(index){
		var rowid = this.id;
		var txtTP = rowid.split('_')[1];
		if($("#txtTPOSCCostIn_"+txtTP).val()==""){ var TPOSCCost = 0; } else{ var TPOSCCost = $("#txtTPOSCCostIn_"+txtTP).val(); }
		TPOSCCostIn = parseFloat(TPOSCCostIn) + parseFloat(TPOSCCost);
	});
	
	var DDSOLPCommCostIn = 0;
	$('[id^=txtDDSOLPCommCostIn_]').each(function(index){
		var rowid = this.id;
		var DDSOLProw = rowid.split('_')[1];
		if($("#txtDDSOLPCommCostIn_"+DDSOLProw).val()==""){ var DDSOLPCommCost = 0; } else{ var DDSOLPCommCost = $("#txtDDSOLPCommCostIn_"+DDSOLProw).val(); }
		DDSOLPCommCostIn = parseFloat(DDSOLPCommCostIn) + parseFloat(DDSOLPCommCost);
	});
	
	var DDSODPCommCostIn = 0;
	$('[id^=txtDDSODPCommCostIn_]').each(function(index){
		var rowid = this.id;
		var DDSODProw = rowid.split('_')[1];
		if($("#txtDDSODPCommCostIn_"+DDSODProw).val()==""){ var DDSODPCommCost = 0; } else{ var DDSODPCommCost = $("#txtDDSODPCommCostIn_"+DDSODProw).val(); }
		DDSODPCommCostIn = parseFloat(DDSODPCommCostIn) + parseFloat(DDSODPCommCost);
	});
	
	var OMCCostIn = 0;
	$('[id^=txtOMCCostIn_]').each(function(index){
		var rowid = this.id;
		var txtOMCrow = rowid.split('_')[1];
		if($("#txtOMCCostIn_"+txtOMCrow).val()==""){ var OMCCost = 0; } else{ var OMCCost = $("#txtOMCCostIn_"+txtOMCrow).val(); }
		OMCCostIn = parseFloat(OMCCostIn) + parseFloat(OMCCost);
	});
	
	
	var netCostIn = parseFloat(freightCostIn) + parseFloat(BrkCommCostIn) + parseFloat(bunkerCostIn) + parseFloat(brokerageCostIn) + parseFloat(ORCAmtCostIn) + parseFloat(CCCostIn) + parseFloat(LPOSCCostIn) + parseFloat(DPOSCCostIn) + parseFloat(TPOSCCostIn) + parseFloat(DDSOLPCommCostIn) + parseFloat(DDSODPCommCostIn) + parseFloat(OMCCostIn);
	$("#txttotalCostIn").val(netCostIn.toFixed(2));
	
	<!------------------- For Out ---------------------->
	
	if($("#txtBrkCommCostOut").val() == ""){var BrkCommCostOut = 0;}else{var BrkCommCostOut = $("#txtBrkCommCostOut").val();}
	
	freightCostOut = 0;
	$('[id^=txtfreightCostOut_]').each(function(index) {
		var rowid = this.id;
		var freightnet = rowid.split('_')[1];
		if($("#txtfreightCostOut_"+freightnet).val() == ""){var freightCostOut1 = 0;}else{ var freightCostOut1 = $("#txtfreightCostOut_"+freightnet).val();}
		freightCostOut = parseFloat(freightCostOut) + parseFloat(freightCostOut1);
	});
	
	var bunkerCostOut = 0;
	$('[id^=txtbunkerSupplyCostOut_]').each(function(index) {
		var rowid = this.id;
		var bunkerSupply = rowid.split('_')[1];
		if($("#txtbunkerSupplyCostOut_"+bunkerSupply).val() == ""){var bunkerSupplyCostOut = 0;}else{ var bunkerSupplyCostOut = $("#txtbunkerSupplyCostOut_"+bunkerSupply).val();}
		bunkerCostOut = parseFloat(bunkerCostOut) + parseFloat(bunkerSupplyCostOut);
	});
	
	var brokerageCostOut = 0; 
	$('[id^=txtBrCommPerCostOut_]').each(function(index){ 
		var rowid = this.id;
		var brCommCost = rowid.split('_')[1];
		if($("#txtBrCommPerCostOut_"+brCommCost).val()==""){ var brkCostPer = 0; } else{ var brkCostPer = $("#txtBrCommPerCostOut_"+brCommCost).val(); }
		brokerageCostOut = parseFloat(brokerageCostOut) + parseFloat(brkCostPer);
	});
	
	var ORCAmtCostOut = 0;
	$('[id^=txtORCAmtCostOut_]').each(function(index){
		var rowid = this.id;
		var ORCAmt = rowid.split('_')[1];
		if($("#txtORCAmtCostOut_"+ORCAmt).val()==""){ var ORCAmtCost = 0; } else{ var ORCAmtCost = $("#txtORCAmtCostOut_"+ORCAmt).val(); }
		ORCAmtCostOut = parseFloat(ORCAmtCostOut) + parseFloat(ORCAmtCost);
	});
	
	var CCCostOut = 0;
	$('[id^=txtCCCostOut_]').each(function(index){
		var rowid = this.id;
		var txtCC = rowid.split('_')[1];
		if($("#txtCCCostOut_"+txtCC).val()==""){ var CCCost = 0; } else{ var CCCost = $("#txtCCCostOut_"+txtCC).val(); }
		CCCostOut = parseFloat(CCCostOut) + parseFloat(CCCost);
	});
	
	var LPOSCCostOut = 0;
	$('[id^=txtLPOSCCostOut_]').each(function(index){
		var rowid = this.id;
		var txtLP = rowid.split('_')[1];
		if($("#txtLPOSCCostOut_"+txtLP).val()==""){ var LPOSCCost = 0; } else{ var LPOSCCost = $("#txtLPOSCCostOut_"+txtLP).val(); }
		LPOSCCostOut = parseFloat(LPOSCCostOut) + parseFloat(LPOSCCost);
	});
	
	var DPOSCCostOut = 0;
	$('[id^=txtDPOSCCostOut_]').each(function(index){
		var rowid = this.id;
		var txtDP = rowid.split('_')[1];
		if($("#txtDPOSCCostOut_"+txtDP).val()==""){ var DPOSCCost = 0; } else{ var DPOSCCost = $("#txtDPOSCCostOut_"+txtDP).val(); }
		DPOSCCostOut = parseFloat(DPOSCCostOut) + parseFloat(DPOSCCost);
	});
	
	var TPOSCCostOut = 0;
	$('[id^=txtTPOSCCostOut_]').each(function(index){
		var rowid = this.id;
		var txtTP = rowid.split('_')[1];
		if($("#txtTPOSCCostOut_"+txtTP).val()==""){ var TPOSCCost = 0; } else{ var TPOSCCost = $("#txtTPOSCCostOut_"+txtTP).val(); }
		TPOSCCostOut = parseFloat(TPOSCCostOut) + parseFloat(TPOSCCost);
	});
	
	var DDSOLPCommCostOut = 0;
	$('[id^=txtDDSOLPCommCostOut_]').each(function(index){
		var rowid = this.id;
		var DDSOLProw = rowid.split('_')[1];
		if($("#txtDDSOLPCommCostOut_"+DDSOLProw).val()==""){ var DDSOLPCommCost = 0; } else{ var DDSOLPCommCost = $("#txtDDSOLPCommCostOut_"+DDSOLProw).val(); }
		DDSOLPCommCostOut = parseFloat(DDSOLPCommCostOut) + parseFloat(DDSOLPCommCost);
	});
	
	var DDSODPCommCostOut = 0;
	$('[id^=txtDDSODPCommCostOut_]').each(function(index){
		var rowid = this.id;
		var DDSODProw = rowid.split('_')[1];
		if($("#txtDDSODPCommCostOut_"+DDSODProw).val()==""){ var DDSODPCommCost = 0; } else{ var DDSODPCommCost = $("#txtDDSODPCommCostOut_"+DDSODProw).val(); }
		DDSODPCommCostOut = parseFloat(DDSODPCommCostOut) + parseFloat(DDSODPCommCost);
	});
	
	var OMCCostOut = 0;
	$('[id^=txtOMCCostOut_]').each(function(index){
		var rowid = this.id;
		var txtOMCrow = rowid.split('_')[1];
		if($("#txtOMCCostOut_"+txtOMCrow).val()==""){ var OMCCost = 0; } else{ var OMCCost = $("#txtOMCCostOut_"+txtOMCrow).val(); }
		OMCCostOut = parseFloat(OMCCostOut) + parseFloat(OMCCost);
	});
	
	var netCostOut = parseFloat(freightCostOut) + parseFloat(BrkCommCostOut) + parseFloat(bunkerCostOut) + parseFloat(brokerageCostOut) + parseFloat(ORCAmtCostOut) + parseFloat(CCCostOut) + parseFloat(LPOSCCostOut) + parseFloat(DPOSCCostOut) + parseFloat(TPOSCCostOut) + parseFloat(DDSOLPCommCostOut) + parseFloat(DDSODPCommCostOut) + parseFloat(OMCCostOut);
	$("#txttotalCostOut").val(netCostOut.toFixed(2));
	
	var diff = parseFloat(netCostIn) - parseFloat(netCostOut);
	$('#txttotalDiff').val(diff.toFixed(2));
	
}
</script>
		
</body>
</html>