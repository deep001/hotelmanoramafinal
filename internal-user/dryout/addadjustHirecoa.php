<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$comid = $_REQUEST['id'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "vessel_in_post_ops.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "vessel_in_history.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "coa_in_ops_at_glance.php"; $page_bar="In Ops at a glance - COA";$nav=20;$subtitle = 'COA';}else if($page == 5){$page_link = "coa_in_post_ops.php"; $page_bar="In Post Ops at a glance - COA";$nav=20;$subtitle = 'COA';}else{$page_link = "coa_in_history.php"; $page_bar="Vessels in History - COA";$nav=20;$subtitle = 'COA';}

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertInvoiceHireDetailsAdjustment();
	$msg = explode(",",$msg);
	header('Location:./hire_invoice_list.php?msg='.$msg[0].'&id='.$msg[1].'&page='.$page);
}
$fcaid = $id = $obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($id);
$arr       = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$fcaid);

$lp_name = array(); 
$dp_name = array();
$discharge_port = "";

for($j=0;$j<count($arr);$j++)
{   
    $arr_explode = explode('@@',$arr[$j]);
	if($arr_explode[0] == "LP"){$lp_name[]= $obj->getPortNameBasedOnID($arr_explode[1]);}
	else{$dp_name[]= $obj->getPortNameBasedOnID($arr_explode[1]);}
	$k = $j+1;
	if($k = count($arr) && $arr_explode[0] == "DP"){$dp_name[] = $obj->getPortNameBasedOnID($arr_explode[1]);}
}


$lp_name  = implode(', ',$lp_name);
$dp_name  = implode(', ',$dp_name);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}

$bunkergrade = array( 1 =>1 , 2 =>2, 3 =>3 , 4 =>4);
$bunkergrade1= array( 1 =>'HSFO' , 2 =>'LSFO', 3 =>'HSDO' , 4 =>'LSDO');
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
$("#frm1").validate({
	rules: {
	selIType: {required: true},
	txtInvoiceNo: {required: true},
	txtDate: {required: true},
	txtFinalAmt:{required: true}
	},
messages: {
	selIType: {required: "*"},
	txtInvoiceNo: {required: true},
	txtDate: {required: true},
	txtFinalAmt:{required: "*"}
	},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

$("#txtFinalAmt").numeric();

$('#txtRemarks,#txtBankingDetails').autosize({append: "\n"});

$('#txtDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose:true
});


});

function getValidate(var1)
{
	$("#txtStatus").val(var1);
}

function getAdjustmentDiv()
{
	if($("#selIType option:selected").text()!="Adjustment")
	window.location= "invoice_hire.php?id=<?php echo $comid;?>&page=<?php echo $page;?>";
}


</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?>&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
                <?php
				$sql = "SELECT * from invoice_hire_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and INVOICE_TYPE='Adjustment' LIMIT 1";
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				
				?>	
				<div align="right"><a href="hire_invoice_listcoa.php?id=<?php echo $comid;?>&page=<?php echo  $page;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
							
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12" style="text-align:center;">
								<h2 class="page-header">
								Hire Invoice
                                <input type="hidden" name="txtFcaid" id="txtFcaid" value="<?php echo $obj->getFun1();?>"/>
                                <input type="hidden" name="txtMapId" id="txtMapId" value="<?php echo $comid;?>"/>
                                <input type="hidden" name="txtInID" id="txtInID" value="<?php echo $rows['INVOICEID'];?>"/>
                                <input type="hidden" name="txtVendorID" id="txtVendorID" value="<?php echo $vendorid;?>"/>
								</h2>                            
							</div><!-- /.col -->
						</div>
					
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Invoice Type
								<address>
                                     
									<select  name="selIType" class="select form-control" id="selIType" onChange="getAdjustmentDiv();" >
										<?php 
										$obj->getInvoiceType();
										?>
									</select>
									<script>$('#selIType').val('Adjustment');</script>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
							   Invoice No.
								<address>
									<input type="text" name="txtInvoiceNo" id="txtInvoiceNo" class="form-control" autocomplete="off" value="<?php echo $rows['INVOICE_NO'];?>"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								CP Date
								<address>
								   <label><?php
								   $cpdate = $obj->getCompareEstimateData($comid,"CP_DATE");
								   if($cpdate=="" || $cpdate=="0000-00-00"){$cpdate = ""; }
								   else{$cpdate = date("d-m-Y",strtotime($obj->getCompareEstimateData($comid,"CP_DATE"))); }
								    echo $cpdate; ?></label>
								   <input type="hidden" name="txtCP_Date" id="txtCP_Date" class="input-text" size="10" value="<?php echo $cpdate; ?>" autocomplete="off" readonly/>
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
						  <div class="col-sm-4 invoice-col">
								 Invoice Date
								<address>
								   <?php if($rec == 0){?>
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo date("d-m-Y",time());?>"/>
									<?php }else{?>
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo date("d-m-Y",strtotime($rows['INVOICE_DATE']));?>"/>
								<?php }?>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Nom ID
								<address>
								 <?php echo $obj->getCompareTableData($comid,"MESSAGE");?>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Vessel
								<address>
								   <label><?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></label>
								</address>
							</div><!-- /.col -->
						</div>
                        <div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
								Vessel IMO No.
								<address>
								   <label><?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"IMO_NO");?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-3 invoice-col">
								Supplier
								<address>
								        <label><?php echo $obj->getVendorListNewBasedOnID($vendorid);?></label>
										<input type="hidden" name="txtVendor" id="txtVendor" value="<?php echo $vendorid;?>" />
										<input type="hidden" name="txtNameid" id="txtNameid" value="<?php echo $id[0];?>" />
										<input type="hidden" name="txtGradeid" id="txtGradeid" value="<?php echo $id[2];?>" />
										<input type="hidden" name="txtFCAID" id="txtFCAID" value="<?php echo $obj->getFun1();?>" />
								</address>
							</div><!-- /.col -->                    
							<div class="col-sm-3 invoice-col">
								Loading Port
								<address>
								   <label><?php echo $lp_name;?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-3 invoice-col">
								Discharging Port
								<address>
								   <label><?php echo $dp_name;?></label>
								</address>
							</div><!-- /.col -->
						</div>
                        
                        <div class="box-body no-padding">
                            <table class="table table-striped">
                               <tbody>
                                
                                   <tr>
                                        <td> Adjustment amount</td>
                                        <td></td>
                                        <td><input type="text" name="txtFinalAmt" id="txtFinalAmt" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows['BALANCE_TO_OWNER'];?>"/></td>
                                    </tr>
                                </tbody>
                              </table>
                        </div>
                        
						<div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Remarks
								<address>
									<textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" rows="3" placeholder="Remarks ..." ><?php echo $rows['REMARKS'];?></textarea>
								</address>
							</div><!-- /.col -->						
						</div>
						
                       <div class="box-footer" align="right">
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<button class="btn btn-primary btn-flat" type="submit" onClick="return getValidate(1);">Submit to Edit</button>
							<button class="btn btn-primary btn-flat" type="submit" onClick="return getValidate(2);">Submit to Close</button>
							<input type="hidden" name="action" id="action" value="submit" />
						    <input type="hidden" name="txtDEL_ID" id="txtDEL_ID" value="0"/>
				        </div>
												
					</form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>