<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
$randomid  = $_REQUEST['randomid'];
$vendor = $_REQUEST['vendor'];
$amount  = $_REQUEST['amount'];
$desc  = $_REQUEST['desc'];
$invtype  = $_REQUEST['invtype'];
if(@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertTCInvoiceOthersDetails();
	header('Location:./otherinvoicetc.php?msg='.$msg.'&comid='.$comid.'&page='.$_REQUEST['page']."&randomid=".$_REQUEST['randomid']."&vendor=".$_REQUEST['vendor']."&amount=".$_REQUEST['amount']."&desc=".$_REQUEST['desc']."&invtype=".$_REQUEST['invtype']);
}
if (@$_REQUEST['action1'] == 'submit1')
{
	$msg = $obj->inserttcOthersPaymentReceivedDetails();
	header('Location:./otherinvoicetc.php?msg='.$msg.'&comid='.$comid.'&page='.$_REQUEST['page']."&randomid=".$_REQUEST['randomid']."&vendor=".$_REQUEST['vendor']."&amount=".$_REQUEST['amount']."&desc=".$_REQUEST['desc']."&invtype=".$_REQUEST['invtype']);
}
$id = $obj->getLatestCostSheetIDTC($comid);
$obj->viewTCEstimationTempleteRecordsNew($id);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$_REQUEST['comid'].'&page='.$_REQUEST['page']."&randomid=".$_REQUEST['randomid']."&vendor=".$_REQUEST['vendor']."&amount=".$_REQUEST['amount']."&desc=".$_REQUEST['desc']."&invtype=".$_REQUEST['invtype'];
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
	$(".autosize").autosize({append: "\n"});
	$("#frm1").validate({
		rules: {
		selIType: {required: true},
		txtInvNo: {required: true},
		txtInvDate: {required: true},
		txtDueDate:{required: true},
		txtExchangeRate:{required: true},
		selExchangeCurrency:{required: true},
		txtPaymentTerms:{required: true},
		selBankingDetails:{required: true},
		txtAttenName:{required: true},
		txtExchangeDate:{required: true},
		selFromOwner:{required: true}
		},
	messages: {
		selIType: {required: "*"},
		txtInvNo: {required: true},
		txtInvDate: {required: true},
		txtDueDate:{required: "*"},
		txtExchangeRate:{required: "*"},
		selExchangeCurrency:{required: "*"},
		txtPaymentTerms:{required: "*"},
		selBankingDetails:{required: "*"},
		txtHireFrom:{required: "*"},
		txtHireTo:{required: "*"},
		txtAttenName:{required: "*"},
		txtExchangeDate:{required: "*"},
		selFromOwner:{required: "*"}
		},
	submitHandler: function(form)  {
			jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
			$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
			$("#popup_content").css({"background":"none","text-align":"center"});
			$("#popup_ok,#popup_title").hide();  
			form.submit();
		}
	});

	$(".numeric").numeric();
	$('.datepicker').datepicker({
		format: 'dd-mm-yyyy',
		autoclose:true
	});
	$("#txtHireFrom,#txtHireTo").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
		}).on('changeDate', function(){
		   getFullClaculation();
	});
	getBankingDetailsData();
	getFullClaculation();
});


function getTimeDiff(date2,date1)
{	
	if(date1 != '' &&  date2 != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat(date2);
		start_actual_time    =  getDateWithSpecificFormat(date1);
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000;
		return days.toFixed(5);	
	}
}


function getString(var1)
{
  var var2 = var1.split(' ');
  var var3 = var2[0].split('-');  
  return var3[2]+'/'+var3[1]+'/'+var3[0];
}
 
function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0];
	return currentDate;
}

function AddNewAddRow()
{
	var id = $("#txtAddID").val();
	if($("#txtAddDes_"+id).val() != "" && $("#txtAddDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="add_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtAddDes_'+id+'" id="txtAddDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtAddDesAmt_'+id+'" id="txtAddDesAmt_'+id+'" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td></tr>').appendTo("#tblAdd");
		$("#txtAddID").val(id);
		$(".numeric").numeric();	
	}
}


function removeAddRow(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#add_Row_"+var1).remove();
					getFullClaculation();
				 }
			else{return false;}
			});
}



function SubNewSubRow()
{
	var id = $("#txtSubID").val();
	if($("#txtAddDes_"+id).val() != "" && $("#txtSubDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="Sub_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtSubDes_'+id+'" id="txtSubDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtSubDesAmt_'+id+'" id="txtSubDesAmt_'+id+'" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td></tr>').appendTo("#tblSub");
		$("#txtSubID").val(id);
		$(".numeric").numeric();
	}
}


function removeSubRow(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#Sub_Row_"+var1).remove();
				getFullClaculation();
			 }
		else{return false;}
		});
}

function getFullClaculation()
{
    var txtInvAmt = parseFloat($("#txtInvAmt").val());
	if(isNaN(txtInvAmt)){txtInvAmt = 0.00;}
	
	var otheradd = parseFloat($("[id^=txtAddDesAmt_]").sum()).toFixed(2);
	var otherless = parseFloat($("[id^=txtSubDesAmt_]").sum()).toFixed(2);
	var finalamt = parseFloat(txtInvAmt) + parseFloat(otheradd) - parseFloat(otherless);
	$("#txtFinalAmt").val(parseFloat(finalamt).toFixed(2));
}

function getValidate(var1)
{
	$("#txtStatus").val(var1);
	return true;
}

function getBankingDetailsData()
{   
	var1 = $('#selBankingDetails').val();
	if($("#selBankingDetails").val()!="")
	{
		$.post("options.php?id=43",{selNOB:""+$("#selBankingDetails").val()+""}, function(html) {
			if(html!="")
			{
				$("#bankingrow").show();
				var res = html.split("@@@@");
				$('#span_1_0').text(res[0]);
				$('#span_1_1').text(res[1]);
				$('#span_1_2').text(res[2]);
				$('#span_1_3').text(res[3]);
				$('#span_1_4').text(res[4]);
				$('#span_1_5').text(res[5]);
				$('#span_1_6').text(res[6]);
				$('#span_1_7').text(res[7]);
				$('#span_1_8').text(res[8]);
				$('#span_1_9').text(res[9]);
				$('#span_1_10').text(res[10]);
			}
		});
	}
	else
	{
	    $("#bankingrow").hide();	
		$('#span_1_0').text("");
	    $('#span_1_1').text("");
		$('#span_1_2').text("");
		$('#span_1_3').text("");
		$('#span_1_4').text("");
		$('#span_1_5').text("");
		$('#span_1_6').text("");
		$('#span_1_7').text("");
		$('#span_1_8').text("");
		$('#span_1_9').text("");
		$('#span_1_10').text("");
	}
}


function getValid()
{
	if($('#txtP_PR').val() == '' || $('#txtP_Date').val() =='' || $('#txtP_Remarks').val()=='')
	{
		jAlert('Please fill the Payment Received & Date & Remarks', 'Alert');
		return false;
	}
	else
	{
		var file_temp_name = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
	    $('#txtCRMFILE1').val(file_temp_name);
	    var file_actual_name = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
	    $('#txtCRMNAME1').val(file_actual_name);
		document.frm2.submit();
		return true;
	}
}


function openWin(var1)
{
	$("#divPayment").empty();
	$("#divPayment").html('<div><img src="../../img/ajax-loader.gif" /><br><b>Loading...</b></div>'); 
	$.post("options.php?id=63",{invoiceid:""+var1+""}, function(html) {
		$("#divPayment").empty();
		$("#divPayment").append(html);
		
		$("#txtP_PR").numeric();
        $('#txtP_Remarks').autosize({append: "\n"});
		$('#txtP_Date').datepicker({
			format: 'dd-mm-yyyy',
			autoclose:true
		});
	});
}

function Del_Upload1(var2)
{
	jConfirm('Are you sure you want to delete this upload permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file1_'+var2).remove();
	}
	});
}

</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(21); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops TC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops TC&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;In Ops at a glance TC&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
                        <?php 
							if(isset($_REQUEST['msg'])){
								$msg = $_REQUEST['msg'];
								if($msg == 0){?>
									<div class="alert alert-success alert-dismissable">
										<i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<b>Congratulations!</b> <?php echo $invtype;?> added/updated successfully.
									</div>
								<?php }?>
								<?php if($msg == 1){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Sorry!</b> <?php echo $invtype;?> can not be created.
								</div>
								<?php }?>
								<?php if($msg == 2){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Congratulations!</b> <?php echo $invtype;?> deleted successfully.
								</div>
								<?php }?>
                                <?php if($msg == 3){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Congratulations!</b> Payment received details saved successfully.
								</div>
								<?php }?>
						<?php }?>
				<!--   content put here..................-->
                <?php
				$sqltc = "SELECT * from chartering_tc_estimate_slave1 where TCOUTID='".$obj->getFun1()."' and RANDOMID='".$randomid."'";
				$restc 	= mysql_query($sqltc);
				$rectc 	= mysql_num_rows($restc);
				$rowstc = mysql_fetch_assoc($restc);
				
				$sql = "SELECT * from invoice_tcother_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and RANDOMID='".$randomid."' and VENDOR='".$vendor."' and INV_IDENTITY='".str_replace("'",'',$invtype)."' and SHORT_DESC='".$desc."'";
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				
				?>	
				<div align="right"><?php if($rec>0){?><a href="allPdf.php?id=68&comid=<?php echo $comid; ?>&invoiceid=<?php echo $rows['INVOICEID']; ?>" title="Pdf"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a><?php }?><a href="payment_grid_tc.php?comid=<?php echo $comid;?>&page=<?php echo $page;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
							
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12" style="text-align:center;">
								<h2 class="page-header">
								<?php echo strtoupper($invtype);?>
                                <input type="hidden" name="txtInID" id="txtInID" value="<?php echo $rows['INVOICEID'];?>"/>
								</h2>                            
							</div><!-- /.col -->
						</div>
					
						<div class="row invoice-info">
							<div class="col-sm-3 invoice-col">
							   <strong>Invoicing Company :</strong>
								<address>
                                
									<select  name="selFromOwner" class="select form-control" id="selFromOwner">
										<?php 
										$obj->getVendorListNewForCOA(11);
										?>
									</select>
									<script>$('#selFromOwner').val('<?php echo $rows['SHIP_OWNER'];?>');</script>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-1 invoice-col">
                            </div>
							<div class="col-sm-3 invoice-col">
							   <strong>To :</strong>
								<address>
									<?php echo $obj->getVendorListNewData($vendor,"NAME");?><br/>
                                    <?php echo $obj->getVendorListNewData($vendor,"STREET_1");?><br/>
                                    <?php if($obj->getVendorListNewData($vendor,"STREET_2")!=''){echo $obj->getVendorListNewData($vendor,"STREET_2").'<br/>';}?>
                                     <?php echo $obj->getVendorListNewData($vendor,"CITY");?> <?php echo $obj->getVendorListNewData($vendor,"COUNTRY");?> <?php echo $obj->getVendorListNewData($vendor,"CITY_POSTAL_CODE");?>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-1 invoice-col">
                            </div>
							<div class="col-sm-3 invoice-col">
								<strong>Hire Details :</strong>
								<address>
								    TC NO :- <?php echo $obj->getFun7();?><br/>
                                    Vessel :- <?php echo $obj->getVesselIMOData($obj->getFun3(),"VESSEL_NAME");?><br/>
                                    CP Date :- <?php echo date('d-m-Y',strtotime($obj->getFun9()));?><br/>
                                    DEL :- <?php echo $obj->getFun44();?><br/>
                                    RE DEL :- <?php echo $obj->getFun53();?>
								</address>
							</div><!-- /.col -->
						</div>
						<br/>
						<div class="row invoice-info">
						   <div class="col-sm-6 invoice-col">
                               <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;">Invoice Type :</th>
                                            <td><select  name="selIType" class="select form-control" id="selIType"><?php $obj->getInvoiceType();?></select></td>
                                        </tr>
                                        <script>$("#selIType").val('<?php echo $rows['INVOICE_TYPE'];?>');</script>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;">Attn :</th>
                                            <td><input type="text" name="txtAttenName" id="txtAttenName" class="form-control"  placeholder="Attn" autocomplete="off" value="<?php echo $rows['ATTEN'];?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Invoice Number :</th>
                                            <td><input type="text" name="txtInvNo" id="txtInvNo" class="form-control"  placeholder="Invoice Number" autocomplete="off" value="<?php echo $rows['INVOICE_NO'];?>"/></td>
                                        </tr>
                                        <?php
										$invdate = date('d-m-Y',strtotime($rows['INVOICE_DATE']));
										if($invdate=='01-01-1970'){$invdate = '';}
										$duedate = date('d-m-Y',strtotime($rows['DUE_DATE']));
										if($duedate=='01-01-1970'){$duedate = '';}
										$exchangeDate = date('d-m-Y',strtotime($rows['EXCHANGE_DATE']));
										if($exchangeDate=='01-01-1970'){$exchangeDate = '';}
										?>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Invoice Date :</th>
                                            <td><input type="text" name="txtInvDate" id="txtInvDate" class="form-control datepicker"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $invdate;?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Due Date :</th>
                                            <td><input type="text" name="txtDueDate" id="txtDueDate" class="form-control datepicker"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $duedate;?>"/></td>
                                        </tr>
                                        
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Exchange Rate :</th>
                                            <td><input type="text" name="txtExchangeRate" id="txtExchangeRate" class="form-control numeric"  placeholder="Exchange Rate" autocomplete="off" value="<?php echo $rows['EXCHANGE_RATE'];?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Exchange Date :</th>
                                            <td><input type="text" name="txtExchangeDate" id="txtExchangeDate" class="form-control datepicker"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $exchangeDate;?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;">Exchange To Currency :</th>
                                            <td><select  name="selExchangeCurrency" class="select form-control" id="selExchangeCurrency"><?php $obj->getCurrencyList();?></select></td>
                                        </tr>
                                        <script>$("#selExchangeCurrency").val('<?php echo $rows['EXCHANGE_CURRENCY'];?>');</script>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;">Delivery Date :</th>
                                            <td><input type="text" name="txtDelDate" id="txtDelDate" class="form-control" readonly style="background-color:#eee;"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($rowstc['DEL_DATE_EST']));?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;">Re Delivery Date :</th>
                                            <td><input type="text" name="txtReDelDate" id="txtReDelDate" class="form-control" readonly style="background-color:#eee;" placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($rowstc['REDEL_DATE_EST']));?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Payment Terms :</th>
                                            <td><input type="text" name="txtPaymentTerms" id="txtPaymentTerms" class="form-control"  placeholder="Payment Terms" autocomplete="off" value="<?php echo $rows['PAYMENT_TERMS'];?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Description :</th>
                                            <td><textarea name="txtDesc" id="txtDesc" class="form-control autosize" rows="3" placeholder="Description"><?php echo $rows['DESCRIPTION'];?></textarea></td>
                                        </tr>
                                        <?php if($rec>0){?>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Payment Received :</th>
                                            <td align="left" valign="middle">
                                            <a href="#Payment" title="Payment Received" class="btn btn-default btn-sm" data-toggle="modal" data-target="#compose-modal" onClick="openWin(<?php echo $rows['INVOICEID'];?>);">Payment Received</a>
                                            </td>
                                        </tr>
                                        <?php }?>
                                   </table>
                               </div>
						   </div><!-- /.col -->
						   <div class="col-sm-6 invoice-col">
							   <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;" colspan="2"><?php echo $desc;?></th>
                                            <td><input type="text" name="txtInvAmt" id="txtInvAmt" class="form-control" readonly style="background-color:#eee;"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $amount;?>"/></td>
                                        </tr>
                                        
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;" colspan="3">Other Add</th>
                                        </tr>
                                     </tbody>
                                     <?php $sql2 = "select * from invoice_tcother_slave1 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='ADD'";
										   $res2 = mysql_query($sql2);
										   $num2 = mysql_num_rows($res2);
										   if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									       ?>
                                      <tbody id="tblAdd">
                                  <?php if($num2 >0)
									    {$i = 0;
										  while($rows2 = mysql_fetch_assoc($res2))
										  {$i++;
										  ?>
										    <tr id="add_Row_<?php echo $i;?>">
										  	    <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeAddRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
										     	<td><textarea class="form-control areasize" name="txtAddDes_<?php echo $i;?>" id="txtAddDes_<?php echo $i;?>" rows="2" placeholder="Description..."><?php echo $rows2['DESCRIPTION'];?></textarea></td>
										   	    <td><input type="text" name="txtAddDesAmt_<?php echo $i;?>" id="txtAddDesAmt_<?php echo $i;?>" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="<?php echo $rows2['AMOUNT'];?>" onKeyUp="getFullClaculation();"/></td>
										    </tr>
										<?php }}else{?>
										   <tr id="add_Row_1">
										       <td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
										       <td><textarea class="form-control areasize" name="txtAddDes_1" id="txtAddDes_1" rows="2" placeholder="Description..."></textarea></td>
										       <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td>
										   </tr>
								   <?php }?>
                                       </tbody>
                                       <tbody>
                                           <tr>
                                               <td align="left" colspan="3"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewAddRow();">Add</button><input type="hidden" name="txtAddID" id="txtAddID" value="<?php echo $num21;?>"/></td>
                                           </tr>
                                           <tr>
                                               <th style="width:50%;padding-top:15px;padding-bottom:15px;" colspan="3">Other Less</th>
                                           </tr>
                                      <?php $sql2 = "select * from invoice_tcother_slave1 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='SUB'";
											$res2 = mysql_query($sql2);
											$num2 = mysql_num_rows($res2);
											if($num2 >0){$num21 = $num2;}else{$num21 = 1;}?>
                                       </tbody>
                                       <tbody id="tblSub">
                                     <?php if($num2 >0)
										   {$i = 0;
											while($rows2 = mysql_fetch_assoc($res2))
											{$i++;
											?>
											<tr id="Sub_Row_<?php echo $i;?>">
											   <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeSubRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
											   <td><textarea class="form-control areasize" name="txtSubDes_<?php echo $i;?>" id="txtSubDes_<?php echo $i;?>" rows="2" placeholder="Description..."><?php echo $rows2['DESCRIPTION'];?></textarea></td>
											   <td><input type="text" name="txtSubDesAmt_<?php echo $i;?>" id="txtSubDesAmt_<?php echo $i;?>" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="<?php echo $rows2['AMOUNT'];?>" onKeyUp="getFullClaculation();"/></td>
											</tr>
											<?php }}else{?>
											<tr id="Sub_Row_1">
											   <td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
											   <td><textarea class="form-control areasize" name="txtSubDes_1" id="txtSubDes_1" rows="2" placeholder="Description..."></textarea></td>
											   <td><input type="text" name="txtSubDesAmt_1" id="txtSubDesAmt_1" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td>
											</tr>
									<?php }?>
                                       </tbody>
                                       <tbody>
                                           <tr>
                                               <td align="left" colspan="3"><button type="button" class="btn btn-primary btn-flat" onClick="SubNewSubRow();">Add</button><input type="hidden" name="txtSubID" id="txtSubID" value="<?php echo $num21;?>"/></td>
                                           </tr>
                                           <tr>
                                                <td colspan="2"><strong>Final Amount (USD) to invoice</strong></td>
                                                
                                                <td><input type="text" name="txtFinalAmt" id="txtFinalAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows['FINAL_AMOUNT'];?>"/></td>
                                            </tr>
                                       <tr>
                                            <th colspan="2" style="padding-top:15px;padding-bottom:15px;">Banking Details :</th>
                                            <td><select  name="selBankingDetails" class="select form-control" id="selBankingDetails" onChange="getBankingDetailsData()"><?php $obj->getBankingDetails();?></select></td>
                                            <script>$('#selBankingDetails').val('<?php echo $rows['BANKINGID'];?>');</script>
                                        </tr>
                                     </tbody>
                                     <tbody id="bankingrow" style="display:none;">
                                        <tr>
											<td colspan="2">Address</td>
											<td><span id="span_1_0"></span></td>
										</tr>
										<tr>
											<td colspan="2">Beneficiary A/C No.</td>
											<td><span id="span_1_1"></span></td>
										</tr>
										<tr>
											<td colspan="2">Beneficiary Bank</td>
											<td><span id="span_1_2"></span></td>
										</tr>
										<tr>
											<td colspan="2">Beneficiary Bank Address</td>
											<td><span id="span_1_3"></span></td>
										</tr>
										<tr>
											<td colspan="2">Beneficiary Bank Swift Code</td>
											<td><span id="span_1_4"></span></td>
										</tr>
										<tr>
											<td colspan="2">IBAN No.</td>
											<td><span id="span_1_5"></span></td>
										</tr>
										<tr>
											<td colspan="2">FED ABA</td>
											<td><span id="span_1_6"></span></td>
										</tr>
										<tr>
											<td colspan="4" style="background-color:grey; color:white;">CORRESPONDENT DETAILS</td>
										</tr>
										<tr>
											<td colspan="2">Correspondent Bank Name</td>
											<td><span id="span_1_7"></span></td>
										</tr>
										
                                        <tr>
                                            <td colspan="2">Correspondent Bank Address</td>
											<td><span id="span_1_9"></span></td>
										</tr>
                                        <tr>
											<td colspan="2">Account Number</td>
											<td><span id="span_1_10"></span></td>
										</tr>
                                        <tr>
											<td colspan="2">Swift Code</td>
											<td><span id="span_1_8"></span></td>
										</tr>
                                     </tbody>
                                   </table>
                               </div>
						   </div><!-- /.col -->
						</div>
                        
					
						
						
                       <div class="box-footer" align="right">
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<button class="btn btn-primary btn-flat" type="submit" onClick="return getValidate(0);">Submit to Edit</button>
							<button class="btn btn-primary btn-flat" type="submit" onClick="return getValidate(1);">Submit to Close</button>
							<input type="hidden" name="action" id="action" value="submit" />
                            <input type="hidden" name="action" id="action" value="submit" />
						    <input type="hidden" name="txtINVidentity" id="txtINVidentity" value="<?php echo str_replace("'",'',$invtype);?>"/>
                            <input type="hidden" name="txtINVShortDesc" id="txtINVShortDesc" value="<?php echo $desc;?>"/>
                            <input type="hidden" name="txtINVVendor" id="txtINVVendor" value="<?php echo $vendor;?>"/>
				        </div>
                        					
					</form>
                    
                    <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								</div>
								<div class="modal-body">
									<form name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>
                                        <div id="divPayment">
                                            
                                        </div>
                                        <div class="box-footer" align="right">
                                            <button type="submit" id="btnhide" class="btn btn-primary btn-flat" onClick="return getValid();" >Submit</button>
                                            <input type="hidden" id="action1" name="action1" value="submit1" />
                                            <input type="hidden" name="txtCRMFILE1" id="txtCRMFILE1" value="" />
                                            <input type="hidden" name="txtCRMNAME1" id="txtCRMNAME1" value="" />
                                        </div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>