<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid 		= $_REQUEST['comid'];
$cost_sheet_id  = $_REQUEST['cost_sheet_id'];
$page  			= $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}
if (@$_REQUEST['action'] == 'submit')
 { 
 	$msg = $obj->updateTCICostSheetDetails();
	header('Location:./'.$page_link.'?msg='.$msg);
 }
$id = $obj->getTCICostSheetID($comid,$cost_sheet_id);
$obj->viewFreightCostEstimationTempleteRecordsNew($id);
$pagename   = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&cost_sheet_id=".$cost_sheet_id."&page=".$page;
$rdoCap = $obj->getFun12();
$rdoMMarket= $obj->getFun45();
$rdoDWT =$obj->getFun46();
$rdoQty = $obj->getFun96();
$rdoQtyType = $obj->getFun155();
if($rdoQtyType ==""){$rdoQtyType = 1;}
$display1 = '';
$display2 = '';
if($rdoQtyType ==1){$display1 = 'style="display:none;"';$display2 = '';}
if($rdoQtyType ==2){$display1 = '';$display2 = 'style="display:none;"';}
$rdoEstimateType = $obj->getFun158();
$rdoQtyMar =$obj->getFun159();
if($rdoQtyMar=='')
{$rdoQtyMar=1;}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
table {border-collapse:separate;}
select{height:19px;}
.select {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-weight: bold;
    color: #333333;
    /* color: #FBC763; */
    text-decoration: none;
    line-height: 13px;
	height:13px;
}

.input-text {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #333333;
	text-decoration: none;
	/*text-transform:uppercase;
	line-height: 15px;*/
}

.input-textsmall {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-weight: normal;
	color: #333333;
	text-decoration:none;
	text-transform:uppercase;
}


.input 
{
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 10px;
font-weight: normal;
color: #333333;
text-decoration: none;
}

</style>
</head>
	<div id="basic-modal-content" style="display:none;" align="center">
	<table cellpadding="1" cellspacing="4" border="0" align="center">
	<tr><td align="center"><img src="../../img/loading.gif" /><br /></td></tr>
	</table>
	</div>
	
    <body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onClick="getFinalCalculation();">Calculate</a>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops VC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Voyage Financials : Estimate</li>
                    </ol>
                </section>
                
                <div align="right" style="margin-top:5px;margin-bottom:5px;margin-right:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="in_ops_at_glance.php"><button class="btn btn-info btn-flat">Back</button></a></div>
                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                    <table width="100%" style="margin-bottom:5px; padding:0px 5px 0px 5px;">
                       <tr>
                           <td width="12%" class="input-text" style="color:#367fa9;font-size:11px;"><strong >Fixture&nbsp;Type&nbsp;:</strong>&nbsp;VC&nbsp;Out&nbsp;<input type="hidden" name="selFType" id="selFType" value="2"></td>
                           
                           <td class="input-text" style="color:#367fa9;font-size:11px;">
                           <strong>Vessel&nbsp;:&nbsp;</strong><?php echo $obj->getVesselIMOData($obj->getFun5(),'VESSEL_NAME');?><input type="hidden" name="selVName" class="input-text" id="selVName" value="<?php echo $obj->getFun5();?>">
                           </td>
                           <td class="input-text" style="color:#367fa9;font-size:11px;"><strong>Vessel&nbsp;Type&nbsp;:</strong><input type="text" name="txtVType" id="txtVType" style="width:90px;color:#367fa9;font-size:11px;" class="input-text" readonly value="<?php echo $obj->getFun6();?>" />
                            <select  name="selVendor" style="display:none;"  id="selVendor" >
                            <?php $obj->getVendorListNewUpdate();	?>
                            </select> 
                            <select  name="selPort" style="display:none;" id="selPort">
                                <?php $obj->getPortList(); ?>
                            </select>
                            <select name="selOwnerCost" id="selOwnerCost" style="display:none;">
                            <?php $obj->getOwnerRelatedCostList();?>
                            </select>
                            <select name="selMaterial1" id="selMaterial1" style="display:none;">
							<?php $obj->getCargoNameListForMultiple(1);?>
                            </select>
                            <select name="selMaterial2" id="selMaterial2" style="display:none;">
                            <?php $obj->getCargoNameListForMultiple(2);?>
                            </select>
                            <select name="selMaterial3" id="selMaterial3" style="display:none;">
                            <?php $obj->getCargoNameListForMultiple(3);?>
                            </select>
                           </td>
                           <td class="input-text" style="color:#367fa9;font-size:11px;"><strong>Flag&nbsp;:</strong><input type="text" name="txtFlag" id="txtFlag" style="width:100px;color:#367fa9;font-size:11px;" class="input-text" readonly value="<?php echo $obj->getFun7();?>" /></td>
                           <?php if($obj->getFun3()!=""){$date = date('d-m-Y',strtotime($obj->getFun3()));}else{$date = "";}?>
                            <td class="input-text" style="color:#367fa9;font-size:11px;"><strong>Date&nbsp;:</strong><input type="text" name="txtDate" id="txtDate" style="width:90px;color:#367fa9;font-size:11px;" class="input-text" value="<?php echo $date ;?>" placeholder"dd-mm-yy" /></td>
                            <td class="input-text" style="color:#367fa9;font-size:11px;"><strong>Voyage&nbsp;No.&nbsp;:</strong><input type="text" name="txtVNo" id="txtVNo" style="width:90px;color:#367fa9;font-size:11px;" class="input-text" value="<?php echo $obj->getFun8();?>" placeholder"" /></td>
                            <td class="input-text" style="color:#367fa9;font-size:11px;"><strong>Voyage&nbsp;Financials&nbsp;Name&nbsp;:</strong><input type="text" name="txtENo" id="txtENo" style="width:115px;color:#367fa9;font-size:11px;" class="input-text" value="<?php echo $obj->getFun9();?>" placeholder="" /></td>
                       </tr>
                   </table>
                <div id="tabs" class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tabs_2" data-toggle="tab">Voyage Financials : Estimate</a></li>
                        <li><a href="#tabs_3" data-toggle="tab">Commercial Parameters</a></li>
                        <li><a href="#tabs_4" data-toggle="tab">Open Vessel Details</a></li>
                        <li><a href="#tabs_5" data-toggle="tab">Planned Cargo/Intake</a></li>
                    </ul>
                    
                    <div class="tab-content">
                    <!----------------------------------------------- Start 1------------------------------------------------->
                    <div id="tabs_2" class="tab-pane active" style="overflow:auto;">
                       <table width="100%">
                         
                          <tr>
                           <td colspan="2" >
                              <table class='tablesorter' width="100%">
                              <tbody>
                               <tr>
                                   <td colspan="2">
                                       <table width="100%">
                                       <tr>
                                       <td><strong>Business Type&nbsp;:</strong></td>
                                        <td><strong>&nbsp;&nbsp;&nbsp;<span id="cargotypespan"><?=$obj->getBusinessTypeBasedOnID1($rdoEstimateType);?></span></strong></td>
                                        <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cargo Name&nbsp;:</strong><input type="hidden" name="rdoEstimateType" id="rdoEstimateType" value="<?=$rdoEstimateType;?>"/></td>
                                        <td>
                                         <select data-placeholder="Choose Cargo Name..." class="input-text chzn-select" style="width:400px;" multiple name="selCName[]" onChange="getMaterialCode();" id="selCName"><?php $obj->getCargoNameListForMultiple($rdoEstimateType,$obj->getFun52());?></select>
                                        </td>
                                        <td>Cargo Planning</td>
                                            <td><select  name="selCargoPlanning" class="input-text required" style="width:250px;" id="selCargoPlanning" onChange="getCargoPlanningdata();"><?php $obj->getCargoPlanningList($_SESSION['selBType'],1,2);?></select></td>
                                       </tr>
                                       </table>
                                   </td>
                                 </tr>    
                              <tr id="distypeDiv1">
                               <td colspan="2">      
                                 <table class='tablesorter' width="100%">
                                  <tbody>
                                    <tr>
                                        <td><strong>Market</strong>&nbsp;&nbsp;</td>
                                        <td><input name="rdoQtyMar" class="checkbox" id="rdoQtyMar1" type="radio" value="1"  <?php if($rdoQtyMar == 1) echo "checked"; ?>  onclick="showGasMarket(1);"  /></td>
                                        <td>Baltic <span style="font-size:10px; font-style:italic;">(USD/MT)</span>&nbsp;&nbsp;</td>
                                        <td><input type="text" name="txtBalticRate" id="txtBalticRate" class="input-text" autocomplete="off" size="13" placeholder="0.00" value="<?php echo $obj->getFun160();?>" onKeyUp="getFinalCalculation();" /></td>
                                        <td>Addnl. Prenium <span style="font-size:10px; font-style:italic;">(USD/MT)</span>&nbsp;&nbsp;</td>
                                        <td><input type="text" name="txtAdnlPrenium" id="txtAdnlPrenium" class="input-text" autocomplete="off" size="13" placeholder="0.00" value="<?php echo $obj->getFun161();?>" onKeyUp="getFinalCalculation();" /></td>
                                        <td>Base&nbsp;Rate&nbsp;<span style="font-size:10px; font-style:italic;">(USD/MT)</span>&nbsp;</td>
                                        <td><input type="text"  name="txtBaseRate" id="txtBaseRate" readonly class="input-text" autocomplete="off" size="13" value="<?php echo $obj->getFun162();?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                        <td><input name="rdoQtyMar" class="checkbox" id="rdoQtyMar2" type="radio" value="2"  <?php if($rdoQtyMar == 2) echo "checked"; ?> onClick="showGasMarket(2);" />&nbsp;&nbsp;</td>
                                        <td>Lumpsum&nbsp;<span style="font-size:10px; font-style:italic;">(USD)</span>&nbsp;&nbsp;</td>
                                        <td><input type="text" name="txtGASLumpsum" id="txtGASLumpsum" class="input-text" autocomplete="off" size="13" value="<?php echo $obj->getFun164();?>" placeholder="0.00" disabled="disabled" onKeyUp="getFinalCalculation();"/></td>
                                        <td>Quantity&nbsp;<span style="font-size:10px; font-style:italic;">(MT)</span>&nbsp;&nbsp;</td>
                                        <td><input type="text" name="txtGasCQMT" id="txtGasCQMT" class="input-text" size="13" autocomplete="off" value="<?php echo $obj->getFun163();?>" placeholder="0.00"  onkeyup="getFinalCalculation();" /></td>
                                     </tr>
                                    </tbody>
                                </table>                                           
                               </td>
                           </tr>
                               
                              <tr id="distypeDiv2">
                                   <td colspan="2">      
                                       <table class='tablesorter' width="100%" id="divTanker1">
                                           <tbody>
                                           <?php if($obj->getFun171()==1){$displayws1 = '';$displayws2 = 'none';}else{$displayws1 = 'none';$displayws2 = '';} ?>
                                            <tr>
                                                <td width="140px;"><span id="spanwsports1" style="display:<?php echo $displayws2;?>;">WS Port(s) Combo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                                                <td width="60px;"><span id="spanwsports2" style="display:<?php echo $displayws2;?>;">From :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                                                <td width="142px;"><span id="spanwsports3" style="display:<?php echo $displayws2;?>;">
                                                <select  name="selWSFPort" class="input-text" style="width:120px;" id="selWSFPort">
                                                </select> </span>
                                                </td>
                                                <td width="40px;"><span id="spanwsports4" style="display:<?php echo $displayws2;?>;">To :</span></td>
                                                <td width="152px;"><span id="spanwsports5" style="display:<?php echo $displayws2;?>;">
                                                    <select  name="selWSTPort" class="input-text" style="width:120px;" id="selWSTPort">
                                                    </select></span>
                                                </td>
                                                <td width="150px;" align="right"><input name="ChkLumpsum" class="checkbox" id="ChkLumpsum" type="checkbox" value="1" <?php if($obj->getFun171()==1){echo 'checked';}else{} ?>/>&nbsp;&nbsp;&nbsp;&nbsp;Lump-sum(USD)</td>
                                                <td width="110px;" align="right"><span id="lumpsumspan1" style="display:<?php echo $displayws1;?>;">Cargo&nbsp;Qty(MT)&nbsp;:</span></td>
                                                <td width="120px;"><span id="lumpsumspan2" style="display:<?php echo $displayws1;?>;"><input type="text" name="txtLumpsumQty" id="txtLumpsumQty" class="input-text" autocomplete="off" size="13" value="<?php echo $obj->getFun173();?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></span></td>
                                                <td width="50px;"><span id="lumpsumspan3" style="display:<?php echo $displayws1;?>;">Amount(USD)&nbsp;&nbsp;:&nbsp;&nbsp;</span></td>
                                                <td width="120px;"><span id="lumpsumspan4" style="display:<?php echo $displayws1;?>;"><input type="text" name="txtLumpsum" id="txtLumpsum" class="input-text" autocomplete="off" size="13" value="<?php echo $obj->getFun172();?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></span></td>
                                                <td width="125px;" align="left"><span id="lumpsumspan5" style="display:<?php echo $displayws1;?>;"><select  name="selLumsumVendor" class="input-text" style="width:120px;" id="selLumsumVendor">
                                                    </select></span></td>
                                            </tr>
                                            <script>$("#selWSFPort,#selWSTPort").html($("#selPort").html());$("#selWSFPort").val(<?php echo $obj->getFun165();?>);$("#selWSTPort").val(<?php echo $obj->getFun166();?>);$("#selLumsumVendor").html($("#selVendor").html());$("#selLumsumVendor").val('<?php echo $obj->getFun174()?>');</script>
                                          </tbody>
                                      </table>
                                      <table id="divTanker2" class='tablesorter' width="100%" style="display:<?php echo $displayws2;?>;">
                                        <tbody id="tblTankerBody">
                                            <tr><td colspan="9"><div><strong style="font-size:13px;">Freight Adjustment</strong></div></td></tr>
                                           <?php 
                                            $sql12 = "select * from freight_cost_estimete_slave12 where FCAID='".$obj->getFun1()."'";
                                            $res12 = mysql_query($sql12);
                                            $rec12 = mysql_num_rows($res12);
                                            if($rec12==0)
                                            {$num=1;
                                            ?> 
                                            <tr id="tankRow1_1">
                                               <td><a href="#tb1" onClick="deleteTankerDetails(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                               <td>Freight Specs</td>
                                               <td><input type="text" name="txtFreightSpecs_1" id="txtFreightSpecs_1" class="input-text" autocomplete="off" size="30" value="" placeholder="Freight Specs" onKeyUp="getFinalCalculation();"/></td>
                                               <td>Flat Rate</td>
                                               <td>WS</td>
                                               <td>Distance Leg</td>
                                               <td>Total Distance</td>
                                               <td>Amount</td>
                                               <td>Customer</td>
                                             </tr>
                                             <tr id="tankRow2_1">
                                               <td></td>
                                               <td>Min Cargo Qty</td>
                                               <td><input type="text" name="txtMinCargoQty_1" id="txtMinCargoQty_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtMinFlatRateQty_1" id="txtMinFlatRateQty_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtMinWSQty_1" id="txtMinWSQty_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtMinDisLeg_1" id="txtMinDisLeg_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtMinTotalDis_1" id="txtMinTotalDis_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtMinAmount_1" id="txtMinAmount_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td>
                                                    <select  name="selTankVendor_1" class="input-text" style="width:120px;" id="selTankVendor_1">
                                                    </select>
                                                </td>
                                               <script>$("#selTankVendor_1").html($("#selVendor").html());</script>
                                            </tr>
                                            <tr id="tankRow3_1">
                                               <td></td>
                                               <td>Overage Qty</td>
                                               <td><input type="text" name="txtOveCargoQty_1" id="txtOveCargoQty_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtOveFlatRateQty_1" id="txtOveFlatRateQty_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtOveWSQty_1" id="txtOveWSQty_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtOveDisLeg_1" id="txtOveDisLeg_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtOveTotalDis_1" id="txtOveTotalDis_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtOveAmount_1" id="txtOveAmount_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td></td>
                                            </tr>	
                                            <tr id="tankRow4_1">
                                               <td></td>
                                               <td>Total Cargo Qty</td>
                                               <td><input type="text" name="txtTotalTankQty_1" id="txtTotalTankQty_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" readonly/></td>
                                               <td></td>
                                               <td></td>
                                               <td></td>
                                               <td></td>
                                               <td><input type="text" name="txtTotalTankAmount_1" id="txtTotalTankAmount_1" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" readonly/></td>
                                               <td></td>
                                            </tr>
                                            <tr id="tankRow5_1"><td colspan="9">&nbsp;</td></tr>	
                                        <?php }
                                        else{$i = 0;$num=$rec12;
                                        while($rows12 = mysql_fetch_assoc($res12))
                                        { 	 $i++;?>
                                        <tr id="tankRow1_<?php echo $i;?>">
                                               <td><a href="#tb1" onClick="deleteTankerDetails(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                               <td>Freight Specs</td>
                                               <td><input type="text" name="txtFreightSpecs_<?php echo $i;?>" id="txtFreightSpecs_<?php echo $i;?>" class="input-text" autocomplete="off" size="30" value="<?php echo $rows12['FREIGHT_SPECS'];?>" placeholder="Freight Specs" onKeyUp="getFinalCalculation();"/></td>
                                               <td>Flat Rate</td>
                                               <td>WS</td>
                                               <td>Distance Leg</td>
                                               <td>Total Distance</td>
                                               <td>Amount</td>
                                               <td>Customer</td>
                                             </tr>
                                             <tr id="tankRow2_<?php echo $i;?>">
                                               <td></td>
                                               <td>Min Cargo Qty</td>
                                               <td><input type="text" name="txtMinCargoQty_<?php echo $i;?>" id="txtMinCargoQty_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['MIN_CARGO_QTY'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtMinFlatRateQty_<?php echo $i;?>" id="txtMinFlatRateQty_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['MIN_FLAT_RATE'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtMinWSQty_<?php echo $i;?>" id="txtMinWSQty_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['MIN_WS'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtMinDisLeg_<?php echo $i;?>" id="txtMinDisLeg_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['MIN_DIS_LEG'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtMinTotalDis_<?php echo $i;?>" id="txtMinTotalDis_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['MIN_DISTANCE'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtMinAmount_<?php echo $i;?>" id="txtMinAmount_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['MIN_AMOUNT'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td>
                                                    <select  name="selTankVendor_<?php echo $i;?>" class="input-text" style="width:120px;" id="selTankVendor_<?php echo $i;?>">
                                                    </select>
                                                </td>
                                               <script>$("#selTankVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selTankVendor_<?php echo $i;?>").val('<?php echo $rows12['CUSTOMER'];?>');</script>
                                            </tr>
                                            <tr id="tankRow3_<?php echo $i;?>">
                                               <td></td>
                                               <td>Overage Qty</td>
                                               <td><input type="text" name="txtOveCargoQty_<?php echo $i;?>" id="txtOveCargoQty_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['OVE_CARGO_QTY'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtOveFlatRateQty_<?php echo $i;?>" id="txtOveFlatRateQty_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['OVE_FLAT_RATE'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtOveWSQty_<?php echo $i;?>" id="txtOveWSQty_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['OVE_WS'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtOveDisLeg_<?php echo $i;?>" id="txtOveDisLeg_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['OVE_DIS_LEG'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtOveTotalDis_<?php echo $i;?>" id="txtOveTotalDis_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['OVE_DISTANCE'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td><input type="text" name="txtOveAmount_<?php echo $i;?>" id="txtOveAmount_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['OVE_AMOUNT'];?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                               <td></td>
                                            </tr>	
                                            <tr id="tankRow4_<?php echo $i;?>">
                                               <td></td>
                                               <td>Total Cargo Qty</td>
                                               <td><input type="text" name="txtTotalTankQty_<?php echo $i;?>" id="txtTotalTankQty_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['TOTAL_QTY'];?>" placeholder="0.00" readonly/></td>
                                               <td></td>
                                               <td></td>
                                               <td></td>
                                               <td></td>
                                               <td><input type="text" name="txtTotalTankAmount_<?php echo $i;?>" id="txtTotalTankAmount_<?php echo $i;?>" class="input-text" autocomplete="off" size="13" value="<?php echo $rows12['TOTAL_AMOUNT'];?>" placeholder="0.00" readonly/></td>
                                               <td></td>
                                            </tr>
                                            <tr id="tankRow5_<?php echo $i;?>"><td colspan="9">&nbsp;</td></tr>	
                                        
                                  <?php }}?>
                                        </tbody>
                                        <tfoot>
                                          <tr><th colspan="9"><button type="button" onClick="addTankDetails()" >Add</button><input type="hidden" name="txtTankID" id="txtTankID" value="<?php echo $num;?>" /><input type="hidden" name="txtTankQuantity" id="txtTankQuantity" value="" /></th>
                                          </tr>
                                        </tfoot>
                                    </table>
                                   </td>
                               </tr>
                               
                              <tr id="distypeDiv3">
                                   <td colspan="2">
                                       <table width="100%">
                                       <tr>
                                       <td width="20%"><input name="rdoQtyType" id="rdoQtyType1" type="radio" style="cursor:pointer;" value="1" class="input-text"  <?php if($rdoQtyType == 1) echo "checked"; ?> onClick="showHideQtyVendorDiv(1);"  /> <strong>Single</strong></td>
                                
                                       <td width="20%"><input name="rdoQtyType" id="rdoQtyType2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoQtyType == 2) echo "checked"; ?> onClick="showHideQtyVendorDiv(2);"  /> <strong>Distributed</strong></td>
                                       <td width="20%"><div id="divQty2" <?php echo $display1;?>><textarea class="input-text" name="txtDistributedRemarks" id="txtDistributedRemarks" cols="50" rows="2" placeholder="Distributed Remarks ..." ><?php echo $obj->getFun156();?></textarea></div></td>
                                       <td></td>
                                       </tr>
                                       </table>
                                       <table class='tablesorter' width="100%" id="divMarket3" <?php echo $display2;?>>
                                  <tbody>
                                    <tr>
                                        <td><strong>Market</strong>&nbsp;&nbsp;</td>
                                        <td><input name="rdoMMarket" class="checkbox" id="rdoMMarket1" type="radio" value="1"  <?php if($rdoMMarket == 1) echo "checked"; ?>  onclick="showMMarketField(1);"  /></td>
                                        <td>Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span>&nbsp;&nbsp;</td>
                                        <td><input type="text" name="txtMTCPDRate" id="txtMTCPDRate" class="input-text" autocomplete="off" size="13" placeholder="0.00" value="<?php echo $obj->getFun47();?>" onKeyUp="getFinalCalculation();" /></td>
                                        <td><input name="rdoMMarket" class="checkbox" id="rdoMMarket2" type="radio" value="2"  <?php if($rdoMMarket == 2) echo "checked"; ?> onClick="showMMarketField(2);" />&nbsp;&nbsp;</td>
                                        <td>Lumpsum&nbsp;<span style="font-size:10px; font-style:italic;">(USD)</span>&nbsp;&nbsp;</td>
                                        <td><input type="text" name="txtMLumpsum" id="txtMLumpsum" class="input-text" autocomplete="off" size="13" value="<?php echo $obj->getFun48();?>" placeholder="0.00" disabled="disabled" onKeyUp="getFinalCalculation();"/></td>
                                        <td colspan="2">Addnl&nbsp;Cargo&nbsp;Rate&nbsp;<span style="font-size:10px; font-style:italic;">(USD/MT)</span>&nbsp;</td>
                                        <td><input type="text"  name="txtAddnlCRate" id="txtAddnlCRate" class="input-text" autocomplete="off" size="13" value="<?php echo $obj->getFun49();?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td>
                                        <td>Quantity&nbsp;<span style="font-size:10px; font-style:italic;">(MT)</span>&nbsp;&nbsp;</td>
                                        <td><input type="text" name="txtCQMT" id="txtCQMT" class="input-text" size="13" autocomplete="off" value="<?php echo $obj->getFun50();?>" placeholder="0.00"  onkeyup="getFinalCalculation();" /></td>
                                        <td></td>
                                    </tr>
                                    <tr><td colspan="13"></td></tr>
                                    <tr>
                                        <td><input type="hidden" name="txtAQMT" id="txtAQMT" class="input-text" autocomplete="off" onKeyUp="getFinalCalculation();"/><span style="font-size:10px; font-style:italic; color:#dc631e">( Please put dead freight quantity<br/> / addnl quantity separately )</span></td>
                                        <td><input name="rdoQty" class="checkbox" id="rdoQty1" type="radio" value="1"  <?php if($rdoQty == 1) echo "checked"; ?>  onclick="showQtyField(1);"  /></td>
                                        <td>DF Qty <span style="font-size:10px; font-style:italic;">(MT)</span></td>
                                        <td><input type="text" name="txtDFQMT" id="txtDFQMT" class="input-text" autocomplete="off" value="<?php echo $obj->getFun53();?>" placeholder="0.00" size="13"  onkeyup="getFinalCalculation();" /></td>
                                        <td><input name="rdoQty" class="checkbox" id="rdoQty2" type="radio" value="2" <?php if($rdoQty == 2) echo "checked"; ?> onClick="showQtyField(2);" /></td>
                                        <td>Addnl&nbsp;Qty<span style="font-size:10px; font-style:italic;">(MT)</span></td>
                                        <td><input type="text" name="txtAddnlQMT" id="txtAddnlQMT" class="input-text" autocomplete="off" placeholder="0.00" value="<?php echo $obj->getFun54();?>" size="13" onKeyUp="getFinalCalculation();" disabled /></td>
                                        <td>Cargo Type</td>
                                        <td><select  name="selCType" style="width:90px;" class="input-text" id="selCType"><?php $obj->getCargoTypeList(); ?></select></td>
                                        <td></td>
                                        <td colspan="3">
                                        
                                        </td>
                                        
                                    </tr>
                                  
                                    <tr>   
                                        
                                        <td colspan="2">Gross&nbsp;Freight&nbsp;&nbsp;<input type="text"  name="txtFrAdjUsdGF" id="txtFrAdjUsdGF" size="13" class="input-text" readonly value="<?php echo $obj->getFun66();?>" /><input type="text" style="display:none;" name="txtFrAdjUsdGFMT" id="txtFrAdjUsdGFMT" size="13" class="input-text" readonly value="<?php echo $obj->getFun70();?>" /></td>
                                        <td>Dead Freight</td>
                                        <td><input type="text"  name="txtFrAdjUsdDF" id="txtFrAdjUsdDF" size="13" class="input-text" readonly value="<?php echo $obj->getFun67();?>" /><input type="text" style="display:none;" name="txtFrAdjUsdDFMT" id="txtFrAdjUsdDFMT" size="13" class="input-text" readonly value="<?php echo $obj->getFun71();?>"/></td>
                                        <td></td>
                                        <td>Addnl Freight</td>
                                        <td><input type="text"  name="txtFrAdjUsdAF" id="txtFrAdjUsdAF" size="13" class="input-text" readonly value="<?php echo $obj->getFun68();?>" /><input type="text" style="display:none;" name="txtFrAdjUsdAFMT" id="txtFrAdjUsdAFMT" class="input-text" readonly value="<?php echo $obj->getFun72();?>" /></td>
                                        <td>Total Freight</td>
                                        <td><input type="text"  name="txtFrAdjUsdTF" id="txtFrAdjUsdTF" size="13" class="input-text" readonly value="<?php echo $obj->getFun69();?>" /><input type="text" style="display:none;" name="txtFrAdjUsdTFMT" id="txtFrAdjUsdTFMT" class="input-text" readonly value="<?php echo $obj->getFun73();?>" /></td>
                                        <td>Brokerage (%)</td>
                                        <td><input type="text"  name="txtFrAdjPerAgC" size="13" id="txtFrAdjPerAgC"  onkeyup="getFinalCalculation(),getValue()" class="input-text" autocomplete="off" value="<?php echo $obj->getFun77();?>" placeholder="0.00" ></td>
                                        <td>Brokerage <span style="font-size:10px; font-style:italic;">(USD)</span></td>
                                        <td><input type="text"  name="txtFrAdjUsdAgC" size="13" id="txtFrAdjUsdAgC" class="input-text" readonly value="<?php echo $obj->getFun78();?>"   /></td>
                                        <td style="display:none;">Net Freight Payable</td>
                                        <td style="display:none;"><input type="text" size="13" name="txtFrAdjUsdFP" id="txtFrAdjUsdFP" class="input-text" readonly value="<?php echo $obj->getFun80();?>"   /></td>
                                    </tr>
                                    
                                    </tbody>
                                </table>
                               <table id="divQty1" class='tablesorter' width="100%" <?php echo $display1;?>>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th width="25%">&nbsp;&nbsp;&nbsp;Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <th>Agreed&nbsp;Gross Freight <span style="font-size:10px; font-style:italic;">(Local Currency/MT)</span></th>
                                            <th>Currency&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <th>Exchange&nbsp;Rate</th>
                                            <th>Agreed&nbsp;Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span></th>
                                            <th>Quantity&nbsp;<span style="font-size:10px; font-style:italic;">(MT)</span>&nbsp;</th>
                                            <?php 
                                            $sql12 = "select * from freight_cost_estimete_slave7 where FCAID='".$obj->getFun1()."'";
                                            $res12 = mysql_query($sql12);
                                            $rec12 = mysql_num_rows($res12);
                                            ?>
                                            <th>Gross&nbsp;Freight <span style="font-size:10px; font-style:italic;">(USD)</span><input type="hidden" name="txtQTYID" id="txtQTYID" value="<?php echo $rec12;?>" /></th>
                                            <th>Brokerage(%)</th>
                                            <th>Net Brokerage</th>
                                            <th>Final Net Freight <span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                            <th>(%)</th>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tblQtyFreight">	 
                                    <?php if($rec12==0)
                                    {?>
                                       
                                        <?php }
                                        else
                                        {$i = 0;
                                        while($rows2 = mysql_fetch_assoc($res12))
                                        {$i = $i + 1;
                                        ?>
                                        
                                         <tr id="tbrQtyVRow_<?php echo $i;?>">
                                         <td><a href="#tb'+id+'" onClick="deleteQtyVendorDetails(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                         <td><select  name="selQtyVendorList_<?php echo $i;?>" class="input-text" style="width:120px;" id="selQtyVendorList_<?php echo $i;?>"></select></td>
                                         <script>
                                         $(document).ready(function(){
                                             $("#selQtyVendorList_<?php echo $i;?>").html($("#selVendor").html());
                                             $("#selQtyVendorList_<?php echo $i;?>").val('<?php echo $rows2['QTY_VENDORID'];?>');
                                         });	 
                                        </script>
                                         <td><input type="text" name="txtQtyLocalAggriedFreight_<?php echo $i;?>" id="txtQtyLocalAggriedFreight_<?php echo $i;?>" class="input-text" style="width:100px;"  autocomplete="off" value="<?php echo $rows2['AGREED_GROSS_FREIGHT_LOCAL'];?>" placeholder="0.00" onKeyUp="getQtyLocalFreightCal();"/></td>
                                         <td><select  name="selCurrencyDisList_<?php echo $i;?>" class="input-text" style="width:100px;" id="selCurrencyDisList_<?php echo $i;?>"><?php $obj->getCurrencyList(); ?></select></td>
                                         <td><input type="text" name="txtDisExchangeRate_<?php echo $i;?>" id="txtDisExchangeRate_<?php echo $i;?>" class="input-text" style="width:100px;" autocomplete="off" value="<?php echo $rows2['EXCHANGE_RATE'];?>" placeholder="0.00" onKeyUp="getQtyLocalFreightCal();"/></td>
                                         <td><input type="text" name="txtQtyAggriedFreight_<?php echo $i;?>" id="txtQtyAggriedFreight_<?php echo $i;?>" class="input-text" style="width:100px;" autocomplete="off" value="<?php echo $rows2['AGREED_GROSS_FREIGHT'];?>" placeholder="0.00" onKeyUp="getQtyLocalFreightCal();"/></td>
                                         <td><input type="text" name="txtFreightQty_<?php echo $i;?>" id="txtFreightQty_<?php echo $i;?>" class="input-text" style="width:100px;" value="<?php echo $rows2['QUANTITY'];?>" placeholder="0.00" onKeyUp="getQtyLocalFreightCal();"/></td>
                                         <td><input type="text" name="txtQtyFreight_<?php echo $i;?>" id="txtQtyFreight_<?php echo $i;?>" class="input-text" style="width:100px;" value="<?php echo $rows2['GROSS_FREIGHT'];?>" readonly/></td>
                                         <td><input type="text" name="txtQtyBrokeragePer_<?php echo $i;?>" id="txtQtyBrokeragePer_<?php echo $i;?>" style="width:100px;" value="<?php echo $rows2['BROKERAGE'];?>" onKeyUp="getQtyLocalFreightCal();" class="input-text" placeholder="0.00" /></td>
                                         <td><input type="text" name="txtQtyBrokerageAmt_<?php echo $i;?>" id="txtQtyBrokerageAmt_<?php echo $i;?>" class="input-text" style="width:100px;" readonly value="<?php echo $rows2['NET_BROKERAGE'];?>" placeholder="0.00" /></td>
                                         <td><input type="text" name="txtQtyNetFreight_<?php echo $i;?>" style="width:100px;" id="txtQtyNetFreight_<?php echo $i;?>" placeholder="0.00" class="input-text" readonly value="<?php echo $rows2['NET_FREIGHT'];?>" /></td>
                                         <td><input type="text"  name="txtQtyNetFreightMT_<?php echo $i;?>" id="txtQtyNetFreightMT_<?php echo $i;?>" class="input-text" style="width:100px;" readonly value="0.00" /></td>
                                         </tr>
                                        <?php }}?>
                                    </tbody>
                                    <tfoot>
                                      <tr><th><button type="button" onClick="addQtyVendorDetails()" >Add</button></th><th></th><th></th><th></th><th></th><th></th><th><input type="text" name="txtTotalFreightQty" id="txtTotalFreightQty" style="width:100px;" class="input-text" readonly value=""/></th><th><input type="text" name="txtTotalQtyFreight" id="txtTotalQtyFreight" style="width:100px;" class="input-text" readonly value=""/></th>
                                      <th width="108px">Final Net Freight</th>
                                      <th><input type="text"  name="txtTotalNetBrokerage" id="txtTotalNetBrokerage" autocomplete="off" style="width:100px;" class="input-text" readonly value="" placeholder="0.00" /></th>
                                      <th><input type="text"  name="txtTotalNetFreight" id="txtTotalNetFreight" autocomplete="off" style="width:100px;" class="input-text" readonly value="0.00" /></th>
                                      <th></th>
                                      </tr>
                                    </tfoot>
                                </table>
                                   </td>
                               </tr>
                           </tbody>
                        </table>
                               
                                 
                                
                           </td>
                       </tr>
                       <tr><td colspan="2"><div><strong style="font-size:13px;">Passage & Ports</strong></div></td></tr>
                       <tr>
                           <td width="55%" style="vertical-align:top;">
                           
                              <table class='tablesorter'>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>From Port</th>
                                        <th>To Port</th>
                                        <th>Distance Type</th>
                                        <th>Passage Type</th>
                                        <th>Distance</th>
                                        <th>Speed Type</th>
                                        <th>Speed Adj.</th>
                                        <th>Margin Dist<span style="font-size:10px; font-style:italic;">(%)</span></th>
                                        
                                    </tr>
                                </thead>
                                <tbody id="tblPortRotation">
                                <?php 
								$sql1 = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."'";
								
								$res1 = mysql_query($sql1);
								$num1 = mysql_num_rows($res1);
								if($num1==0)
								{$num1 =1;
								?>
                                    <tr id="pr_Row_1">
                                        <td align="center" class="input-text" >
                                          <a href="#pr1" id="spcancel_1" onClick="removePortRotation(1);" ><i class="fa fa-times" style="color:red;"></i></a>
                                        </td>
                                        <td align="left" class="input-text" >
                                           <select  name="selFPort_1" class="input-text" style="width:100px;" id="selFPort_1" onChange="getDistance(1);">
                                         
                                           </select> 
                                        </td>
                                        <td align="left" class="input-text" >
                                           <select  name="selTPort_1" class="input-text" style="width:100px;" id="selTPort_1" onChange="getDistance(1);">
                                                
                                            </select>
                                            <script>$("#selFPort_1,#selTPort_1").html($("#selPort").html());</script>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <select  name="selDType_1" class="input-text" style="width:90px;" id="selDType_1" onChange="getDistance(1);">
                                                <?php $obj->getPortDistanceType(); ?>
                                            </select>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <select  name="selPType_1" class="input-text" style="width:80px;" id="selPType_1" onChange="getVoyageTime()"; >
                                                <?php $obj->getPassageType(); ?>
                                            </select>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDistance_1" id="txtDistance_1" style="width:70px;" class="input-text"  value="" placeholder="0.00" onKeyUp="getVoyageTime()"; /><span id="ploader_1" style="display:none;"><img src="../../img/ajax-loader2.gif" /></span>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <select  name="selSSpeed_1" class="input-text" style="width:80px;" id="selSSpeed_1" onChange="getVoyageTime()"; >
                                                <?php $obj->getSelectSpeedList(); ?>
                                            </select>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtWeather_1" id="txtWeather_1" style="width:70px;" class="input-text" autocomplete="off" placeholder="0.00"  onKeyUp="getVoyageTime()"; />
                                        </td>
                                        
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtMargin_1" style="width:70px;" id="txtMargin_1" class="input-text" autocomplete="off" placeholder="0.00"  onKeyUp="getVoyageTime()"; />
                                            <input type="hidden" name="txtVoyageTime_1" id="txtVoyageTime_1" class="input-text" autocomplete="off" value="0.00" />
                                            <input type="hidden" name="txtTTLVoyageDays_1" id="txtTTLVoyageDays_1" class="input-text" autocomplete="off" value="0.00" />
                                            <input type="hidden" name="txtTTLFoConsp_1" id="txtTTLFoConsp_1" class="input-text" readonly value="0.00"  />
                                            <input type="hidden" name="txtTTLDoConsp_1" id="txtTTLDoConsp_1" class="input-text" readonly value="0.00"  />
                                        </td>
                                    </tr>	
                                 <?php }
								  else
								  {$i=0;$num1 = $num1;
									  while($rows1 = mysql_fetch_assoc($res1))
									  {$i = $i + 1;
									  ?>
                                     <tr id="pr_Row_<?php echo $i;?>">
                                        <td align="center" class="input-text" >
                                          <a href="#pr<?php echo $i;?>" id="spcancel_1" onClick="removePortRotation(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a><input type="hidden" name="txtFCASlaveID_<?php echo $i;?>" id="txtFCASlaveID_<?php echo $i;?>" value="<?php echo $rows1['FCA_SLAVEID'];?>"/><input type="hidden" name="txtFCASlaveRandomID_<?php echo $i;?>" id="txtFCASlaveRandomID_<?php echo $i;?>" value="<?php echo $rows1['RANDOMID'];?>"/>
                                        </td>
                                        <td align="left" class="input-text" >
                                           <select  name="selFPort_<?php echo $i;?>" class="input-text" style="width:100px;" id="selFPort_<?php echo $i;?>" onChange="getDistance(<?php echo $i;?>);">
                                         
                                           </select> 
                                        </td>
                                        <td align="left" class="input-text" >
                                           <select  name="selTPort_<?php echo $i;?>" class="input-text" style="width:100px;" id="selTPort_<?php echo $i;?>" onChange="getDistance(<?php echo $i;?>);">
                                                
                                            </select>
                                            <script>$("#selFPort_<?php echo $i;?>,#selTPort_<?php echo $i;?>").html($("#selPort").html());$("#selFPort_<?php echo $i;?>").val(<?php echo $rows1['FROM_PORT'];?>);$("#selTPort_<?php echo $i;?>").val(<?php echo $rows1['TO_PORT'];?>);</script>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <select  name="selDType_<?php echo $i;?>" class="input-text" style="width:90px;" id="selDType_<?php echo $i;?>" onChange="getDistance(<?php echo $i;?>);">
                                                <?php $obj->getPortDistanceType(); ?>
                                            </select>
                                            <script>$("#selDType_<?php echo $i;?>").val(<?php echo $rows1['DIS_TYPE'];?>);</script>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <select  name="selPType_<?php echo $i;?>" class="input-text" style="width:80px;" id="selPType_<?php echo $i;?>" onChange="getVoyageTime()"; >
                                                <?php $obj->getPassageType(); ?>
                                            </select>
                                            <script>$("#selPType_<?php echo $i;?>").val(<?php echo $rows1['PASSAGE_TYPE'];?>);</script>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDistance_<?php echo $i;?>" id="txtDistance_<?php echo $i;?>" style="width:70px;" class="input-text"  value="<?php echo $rows1['DISTANCE'];?>" placeholder="0.00" onKeyUp="getVoyageTime()"; /><span id="ploader_<?php echo $i;?>" style="display:none;"><img src="../../img/ajax-loader2.gif" /></span>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <select  name="selSSpeed_<?php echo $i;?>" class="input-text" style="width:80px;" id="selSSpeed_<?php echo $i;?>" onChange="getVoyageTime()"; >
                                                <?php $obj->getSelectSpeedList(); ?>
                                            </select>
                                            <script>$("#selSSpeed_<?php echo $i;?>").val(<?php echo $rows1['SPEED_TYPE'];?>);</script>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtWeather_<?php echo $i;?>" id="txtWeather_<?php echo $i;?>" style="width:70px;" class="input-text" autocomplete="off" placeholder="0.00" value="<?php echo $rows1['SPEED_ADJ'];?>"  onKeyUp="getVoyageTime()"; />
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtMargin_<?php echo $i;?>" style="width:70px;" id="txtMargin_<?php echo $i;?>" class="input-text" autocomplete="off" placeholder="0.00" value="<?php echo $rows1['MARGIN_DISTANCE'];?>" onKeyUp="getVoyageTime()"; />
                                            <input type="hidden" name="txtVoyageTime_<?php echo $i;?>" id="txtVoyageTime_<?php echo $i;?>" class="input-text" autocomplete="off" value="0.00" />
                                            <input type="hidden" name="txtTTLVoyageDays_<?php echo $i;?>" id="txtTTLVoyageDays_<?php echo $i;?>" class="input-text" autocomplete="off" value="0.00" />
                                            <input type="hidden" name="txtTTLFoConsp_<?php echo $i;?>" id="txtTTLFoConsp_<?php echo $i;?>" class="input-text" readonly value="0.00"  />
                                            <input type="hidden" name="txtTTLDoConsp_<?php echo $i;?>" id="txtTTLDoConsp_<?php echo $i;?>" class="input-text" readonly value="0.00"  />
                                        </td>
                                    </tr>  
                                      
                                 <?php }}
								  ?>									
                                </tbody>
                                <tfoot>
                                <tr>
                                <th colspan="9"><button type="button" onClick="addPortRotationDetails()" >Add</button><input type="hidden" name="p_rotationID" id="p_rotationID" class="input" value="<?php echo $num1;?>" /></th>
                                </tr>
                                </tfoot>
                            </table>
                            <table class='tablesorter'>
                                <thead>
                                    <tr>
                                        <th>Load Port(s)</th>
                                        <th>Port&nbsp;Costs<span style="font-size:10px; font-style:italic;">(USD)</span>&nbsp;</th>
                                        <th>Qty&nbsp;<span id="loadqtylabel">(MT)</span>&nbsp;&nbsp;</th>
                                        <th>Rate&nbsp;<span style="font-size:10px; font-style:italic;" id="loadtablelebel">(MT/Day)</span></th>
                                        <th>LP/Terms&nbsp;&nbsp;</th>
                                        <th>Work&nbsp;Days&nbsp;&nbsp;</th>
                                        <th>Idle&nbsp;Days&nbsp;</th>
                                        <th>Port Cost Vendor</th>
                                        <th>No&nbsp;Dem /Disp?</th>
                                    </tr>
                                </thead>
                                <tbody id="tblLoadPort">
                                <?php 
								$sql1 = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."'";
								
								$res1 = mysql_query($sql1);
								$num1 = mysql_num_rows($res1);
								if($num1==0)
								{$num1 =1;
								?>
                                    <tr id="lp_Row_1">
                                        <td align="center" class="input-text" >
                                            <span id="spanLoadPort_1"></span>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtPCosts_1" id="txtPCosts_1" style="width:70px;" class="input-text" autocomplete="off" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtQMT_1" id="txtQMT_1" class="input-text" style="width:70px;" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="0.00" />
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtRate_1" id="txtRate_1" class="input-text" style="width:70px;" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="0.00" />
                                        </td>
                                        <td align="left" class="input-text" >
                                            <select  name="selLPTerms_1" class="input-text" id="selLPTerms_1" style="width:70px;" onChange="getLPRemoveDaysAttr(1),getPortCalculation();"><?php $obj->getLPTermsList(0);?></select>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtWDays_1" id="txtWDays_1" class="input-text" style="width:65px;" autocomplete="off" readonly value="" onKeyUp= "getVoyageTime();" />
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtIDays_1" id="txtIDays_1" class="input-text" style="width:65px;" autocomplete="off" value="" placeholder="0.00" onKeyUp="getVoyageTime();"/>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <select  name="selLPVendor_1" class="input-text" style="width:100px;" id="selLPVendor_1">
                                            </select>
                                        </td>
                                        <script>$("#selLPVendor_1").html($("#selVendor").html());</script>
                                        <td align="left" class="input-text">
                                            <input name="ChkShowDDCLP_1" class="checkbox" id="ChkShowDDCLP_1" type="checkbox" value="1" />
                                        </td>
                                    </tr>
                                 <?php }
								 else
								  {$i=0;$num1 = $num1;
								  while($rows1 = mysql_fetch_assoc($res1))
								  {$i = $i + 1;
								  $loadportdata = $obj->getFDA($comid,'LP',$rows1['FROM_PORT'],$rows1['RANDOMID']);
								  if($loadportdata>0)
								   {
									   $amount = $loadportdata;
									   $color = 'color:red';
									   $readonly = 'readonly="true"';
								   }
								   else
								   {
									   $amount = $rows1['LOAD_PORT_COST'];
									   $color = '';
									   $readonly = '';
								   }
								  ?>
                                  <tr id="lp_Row_<?php echo $i;?>">
                                    <td align="center" class="input-text" >
                                        <span id="spanLoadPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($rows1['FROM_PORT']);?></span>
                                    </td>
                                    <td align="left" class="input-text" >
                                        <input type="text" name="txtPCosts_<?php echo $i;?>" id="txtPCosts_<?php echo $i;?>" style="width:70px;<?=$color;?>" <?=$readonly;?> class="input-text" autocomplete="off" value="<?php echo $amount;?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/>
                                    </td>
                                    <td align="left" class="input-text" >
                                        <input type="text" name="txtQMT_<?php echo $i;?>" id="txtQMT_<?php echo $i;?>" class="input-text" style="width:70px;" onKeyUp="getPortCalculation();" autocomplete="off" value="<?php echo $rows1['LOAD_PORT_QTY'];?>" placeholder="0.00" />
                                    </td>
                                    <td align="left" class="input-text" >
                                        <input type="text" name="txtRate_<?php echo $i;?>" id="txtRate_<?php echo $i;?>" class="input-text" style="width:70px;" onKeyUp="getPortCalculation();" autocomplete="off" value="<?php echo $rows1['LOAD_PORT_RATE'];?>" placeholder="0.00" />
                                    </td>
                                    <td align="left" class="input-text" >
                                        <select  name="selLPTerms_<?php echo $i;?>" class="input-text" id="selLPTerms_<?php echo $i;?>" style="width:70px;" onChange="getLPRemoveDaysAttr(<?php echo $i;?>),getPortCalculation();"><?php $obj->getLPTermsList($rows1['LOAD_PORT_TERMS']);?></select>
                                    </td>
                                    <?php if($rows1['LOAD_PORT_TERMS']==4){$readonly="";}else{$readonly="readonly";}?>
                                    <td align="left" class="input-text" >
                                        <input type="text" name="txtWDays_<?php echo $i;?>" id="txtWDays_<?php echo $i;?>" class="input-text" style="width:65px;" <?php echo $readonly;?> autocomplete="off" value="<?php echo $rows1['LOAD_PORT_WORK_DAYS'];?>" onKeyUp= "getVoyageTime();" />
                                    </td>
                                    <td align="left" class="input-text" >
                                        <input type="text" name="txtIDays_<?php echo $i;?>" id="txtIDays_<?php echo $i;?>" class="input-text" style="width:65px;" autocomplete="off" value="<?php echo $rows1['LOAD_PORT_IDEAL_DAYS'];?>" placeholder="0.00" onKeyUp="getVoyageTime();"/>
                                    </td>
                                    <td align="left" class="input-text" >
                                        <select  name="selLPVendor_<?php echo $i;?>" class="input-text" style="width:100px;" id="selLPVendor_<?php echo $i;?>">
                                        </select>
                                    </td>
                                    <script>$("#selLPVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selLPVendor_<?php echo $i;?>").val('<?php echo $rows1['PORT_COSTLP_VENDOR'];?>');</script>
                                    <td align="left" class="input-text">
                                        <input name="ChkShowDDCLP_<?php echo $i;?>" class="checkbox" id="ChkShowDDCLP_<?php echo $i;?>" type="checkbox" <?php if($rows1['IS_SHOW_DDCLP'] > 0) echo "checked"; ?> value="<?php echo $i;?>" />
                                    </td>
                                </tr>
                                  
                              <?php }}?>										
                                </tbody>
                              </table>
                              <table class='tablesorter'>
                                <thead>
                                    <tr>
                                        <th>Discharge Port</th>
                                        <th>Port&nbsp;Costs<span style="font-size:10px; font-style:italic;">(USD)</span>&nbsp;</th>
                                        <th>Qty&nbsp;<span id="dischargeqtylabel">(MT)</span></th>
                                        <th>Rate&nbsp;<span style="font-size:10px; font-style:italic;" id="dischargetablelebel">(MT/Day)</span>&nbsp;&nbsp;&nbsp;</th>
                                        <th>&nbsp;DP/Terms</th>
                                        <th>Work&nbsp;Days&nbsp;&nbsp;</th>
                                        <th>Idle&nbsp;Days&nbsp;</th>
                                        <th>Port Cost Vendor</th>
                                        <th>No&nbsp;Dem /Disp?</th>
                                    </tr>
                                </thead>
                                <tbody id="tblDisPort">
									<?php 
                                    $sql1 = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."'";
                                    
                                    $res1 = mysql_query($sql1);
                                    $num1 = mysql_num_rows($res1);
                                    if($num1==0)
                                    {$num1 =1;
                                    ?>
                                    <tr id="dp_Row_1">
                                        <td align="center" class="input-text" >
                                            <span id="spanDisPort_1"></span>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDCosts_1" id="txtDCosts_1" style="width:70px;" class="input-text" autocomplete="off" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDQMT_1" id="txtDQMT_1" style="width:70px;" class="input-text" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="0.00" />
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDRate_1" id="txtDRate_1" style="width:70px;" class="input-text" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="0.00" />
                                        </td>
                                        <td align="left" class="input-text" >
                                            <select  name="selDPTerms_1" class="input-text" style="width:70px;" id="selDPTerms_1" onChange="getDPRemoveDaysAttr(1),getPortCalculation();"><?php $obj->getLPTermsList(0);?></select>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDWDays_1" id="txtDWDays_1" style="width:70px;" class="input-text" autocomplete="off" readonly value="" onKeyUp= "getVoyageTime();"/>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDIDays_1" id="txtDIDays_1" style="width:70px;" class="input-text" autocomplete="off" value="" placeholder="0.00"  onKeyUp="getVoyageTime();"/>
                                        </td>
                                        <td align="left" class="input-text" >
                                        <select  name="selDPVendor_1" class="input-text" style="width:100px;" id="selDPVendor_1">
                                        </select>
                                        </td>
                                        <script>$("#selDPVendor_1").html($("#selVendor").html());</script>
                                        <td align="left" class="input-text">
                                            <input name="ChkShowDDCDP_1" class="checkbox" id="ChkShowDDCDP_1" type="checkbox" value="1" />
                                        </td>
                                    </tr>
                                    <?php }
									 else
									  {$i=0;$num1 = $num1;
									  while($rows1 = mysql_fetch_assoc($res1))
									  {$i = $i + 1;
									  $dischargeportdata = $obj->getFDA($comid,'DP',$rows1['TO_PORT'],$rows1['RANDOMID']);
									  if($dischargeportdata>0)
									   {
										   $amount = $dischargeportdata;
										   $color = 'color:red';
										   $readonly = 'readonly="true"';
									   }
									   else
									   {
										   $amount = $rows1['DISC_PORT_COST'];
										   $color = '';
										   $readonly = '';
									   }
									 ?>
                                    <tr id="dp_Row_1">
                                        <td align="center" class="input-text" >
                                            <span id="spanDisPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($rows1['TO_PORT']);?></span>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDCosts_<?php echo $i;?>" id="txtDCosts_<?php echo $i;?>" style="width:70px;<?=$color;?>" <?=$readonly;?> class="input-text" autocomplete="off" value="<?php echo $amount;?>" placeholder="0.00" onKeyUp="getFinalCalculation();"/>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDQMT_<?php echo $i;?>" id="txtDQMT_<?php echo $i;?>" style="width:70px;" class="input-text" onKeyUp="getPortCalculation();" autocomplete="off" value="<?php echo $rows1['DISC_PORT_QTY'];?>" placeholder="0.00" />
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDRate_<?php echo $i;?>" id="txtDRate_<?php echo $i;?>" style="width:70px;" class="input-text" onKeyUp="getPortCalculation();" autocomplete="off" value="<?php echo $rows1['DISC_PORT_RATE'];?>" placeholder="0.00" />
                                        </td>
                                        <td align="left" class="input-text" >
                                            <select  name="selDPTerms_<?php echo $i;?>" class="input-text" style="width:70px;" id="selDPTerms_<?php echo $i;?>" onChange="getDPRemoveDaysAttr(<?php echo $i;?>),getPortCalculation();"><?php $obj->getLPTermsList($rows1['DISC_PORT_TERMS']);?></select>
                                        </td>
                                        <?php if($rows1['DISC_PORT_TERMS']==4){$readonly="";}else{$readonly="readonly";}?>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDWDays_<?php echo $i;?>" id="txtDWDays_<?php echo $i;?>" style="width:70px;" class="input-text" autocomplete="off" readonly value="<?php echo $rows1['DISC_PORT_WORK_DAYS'];?>" onKeyUp= "getVoyageTime();"/>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtDIDays_<?php echo $i;?>" id="txtDIDays_<?php echo $i;?>" style="width:70px;" class="input-text" autocomplete="off" value="<?php echo $rows1['DISC_PORT_IDEAL_DATE'];?>" placeholder="0.00"  onKeyUp="getVoyageTime();"/>
                                        </td>
                                        <td align="left" class="input-text" >
                                        <select  name="selDPVendor_<?php echo $i;?>" class="input-text" style="width:100px;" id="selDPVendor_<?php echo $i;?>">
                                        </select>
                                        </td>
                                        <script>$("#selDPVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selDPVendor_<?php echo $i;?>").val('<?php echo $rows1['PORT_COSTDP_VENDOR'];?>');</script>
                                        <td align="left" class="input-text">
                                            <input name="ChkShowDDCDP_<?php echo $i;?>" class="checkbox" id="ChkShowDDCDP_<?php echo $i;?>" <?php if($rows1['IS_SHOW_DDCDP'] > 0) echo "checked"; ?> type="checkbox" value="<?php echo $i;?>" />
                                        </td>
                                    </tr>
                                  <?php }}?>												
                                </tbody>
                                
                              </table>
                                 <table class='tablesorter'>
                                    <thead>
                                        <tr>
                                            <th>Transit Port</th>
                                            <th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                            <th>Idle Days</th>
                                            <th>Port Cost Vendor</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tblTransitPort">
                                        <?php 
										$sql1 = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."'";
										
										$res1 = mysql_query($sql1);
										$num1 = mysql_num_rows($res1);
										if($num1==0)
										{$num1 =1;
										?>
                                        <tr id="tp_Row_1">
                                            <td align="center" class="input-text" >
                                                <span id="TranDisPort_1"></span>
                                            </td>
                                            <td align="left" class="input-text" >
                                                <input type="text" name="txtTLPCosts_1" id="txtTLPCosts_1" style="width:70px;" class="input-text" autocomplete="off" value="" placeholder="0.00" onKeyUp="getPortCalculation();"/>
                                            </td>
                                            <td align="left" class="input-text" >
                                                <input type="text" name="txtTLIDays_1" id="txtTLIDays_1" style="width:70px;" class="input-text" autocomplete="off" value="" placeholder="0.00" onKeyUp="getPortCalculation();"/>
                                            </td>
                                            <td align="left" class="input-text" >
                                            <select  name="selTPVendor_1" class="input-text" style="width:120px;" id="selTPVendor_1">
                                            </select>
                                            </td>
                                            <script>$("#selTPVendor_1").html($("#selVendor").html());</script>
                                        </tr>
									<?php }
                                     else
                                      {$i=0;$num1 = $num1;
                                      while($rows1 = mysql_fetch_assoc($res1))
                                      {$i = $i + 1;
									  
									  $port_cost = $obj->getFDA($comid,"TP",$rows1['FROM_PORT'],$rows1['RANDOMID']);
									  if($port_cost>0){$readonlyTPcost= "readonly";$color="red";$port_cost1 = $port_cost;}else{$readonlyTPcost= "";$color="";$port_cost1 = $rows1['TRANSIT_PORT_COST'];}
                                      ?>
                                      <tr id="tp_Row_<?php echo $i;?>">
                                        <td align="center" class="input-text" >
                                            <span id="TranDisPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($rows1['FROM_PORT']);?></span>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtTLPCosts_<?php echo $i;?>" id="txtTLPCosts_<?php echo $i;?>" class="input-text" autocomplete="off" value="<?php echo $rows1['TRANSIT_PORT_COST'];?>" placeholder="0.00" onKeyUp="getPortCalculation();" style="width:70px;color:<?php echo $color;?>" <?php echo $readonlyTPcost;?>/>
                                        </td>
                                        <td align="left" class="input-text" >
                                            <input type="text" name="txtTLIDays_<?php echo $i;?>" id="txtTLIDays_<?php echo $i;?>" style="width:70px;" class="input-text" autocomplete="off" value="<?php echo $rows1['TRANSIT_PORT_IDLE_DAYS'];?>" placeholder="0.00" onKeyUp="getPortCalculation();"/>
                                        </td>
                                        <td align="left" class="input-text" >
                                        <select  name="selTPVendor_<?php echo $i;?>" class="input-text" style="width:120px;" id="selTPVendor_<?php echo $i;?>">
                                        </select>
                                        </td>
                                        <script>$("#selTPVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selTPVendor_<?php echo $i;?>").val('<?php echo $rows1['PORT_COSTTP_VENDOR'];?>');</script>
                                    </tr>	
                                      <?php }}?>															
                                    </tbody>
                                </table>
                                
                                <table>
                                  <tr class="input-text">
                                     <td>Laden Dist</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtLDist" id="txtLDist" class="input-text" style="width:100px;" readonly value="<?php echo $obj->getFun55();?>" /></td><td>&nbsp;&nbsp;&nbsp;&nbsp;Ballast Dist</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtBDist" id="txtBDist" style="width:100px;" class="input-text" readonly value="<?php echo $obj->getFun56();?>" /></td><td>&nbsp;&nbsp;&nbsp;&nbsp;Total Dist</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtTDist" id="txtTDist" class="input-text" style="width:100px;" readonly value="<?php echo $obj->getFun63();?>" /></td>
                                  </tr>
                                  <tr class="input-text">
                                     <td>Laden Days</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtLDays" id="txtLDays" class="input-text" style="width:100px;" readonly value="<?php echo $obj->getFun57();?>" /></td><td>&nbsp;&nbsp;&nbsp;&nbsp;Ballast Days</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtBDays" id="txtBDays" style="width:100px;" class="input-text" readonly value="<?php echo $obj->getFun58();?>" /></td><td>&nbsp;&nbsp;&nbsp;&nbsp;Total Sea Days</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtTSDays" id="txtTSDays" class="input-text" style="width:100px;" readonly value="<?php echo $obj->getFun59();?>" /></td>
                                  </tr>
                                  <tr class="input-text">
                                     <td>Ttl Port Idle Days</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtTtPIDays" id="txtTtPIDays" class="input-text" style="width:100px;" readonly value="<?php echo $obj->getFun60();?>" /></td><td>&nbsp;&nbsp;&nbsp;&nbsp;Ttl Port Work Days</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtTtPWDays" id="txtTtPWDays" style="width:100px;" class="input-text" readonly value="<?php echo $obj->getFun61();?>" /></td><td>&nbsp;&nbsp;&nbsp;&nbsp;Total Days</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtTDays" id="txtTDays" class="input-text" style="width:100px;" readonly value="<?php echo $obj->getFun62();?>" /></td>
                                  </tr>
                                  <tr class="input-text">
                                     <td>Ttl FO Consp MT</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtTFUMT" id="txtTFUMT" class="input-text" style="width:100px;" readonly value="<?php echo $obj->getFun64();?>" /></td><td>&nbsp;&nbsp;&nbsp;&nbsp;Ttl DO Consp MT</td><td>&nbsp;:&nbsp;</td><td><input type="text" name="txtTDUMT" id="txtTDUMT" style="width:100px;" class="input-text" readonly value="<?php echo $obj->getFun65();?>" /></td><td></td><td></td><td></td>
                                  </tr>
                                </table>
                                <div><strong style="font-size:13px;">Demurrage Dispatch</strong></div>
								    <table class='tablesorter'>
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="15%">Estimated(USD)</th>
                                                <th width="15%">Actual(USD)</th>
                                                <th width="15%">Add Comm(%)</th>
                                                <th width="15%">Nett Value(USD)</th>
												<th width="15%">Vendor</th>
                                            </tr>
										</thead>
										<tbody id="tbDDCBody">
                                        
                                        <?php 
											$mysql2 = "select * from freight_cost_estimete_slave1 where FCAID='".$id."'";
											$myres2 = mysql_query($mysql2);
											$myrec2 = mysql_num_rows($myres2);
											if($myrec2 ==0)
											{?>
										      <tr id="DDCLProw_1"> 
												<td>Load Port <span id="spanDDCLPort_1"></span></td>
												<td><input type="text"  name="txtEstDDCLPCost_1" id="txtEstDDCLPCost_1" class="input-text" value="" placeholder="0.00" autocomplete="off" onKeyUp="getFinalCalculation();" /><input type="hidden" name="txtLPLayTimeIs_1" id="txtLPLayTimeIs_1" value="" /></td>
												<td><input type="text"  name="txtDDCLPCost_1" id="txtDDCLPCost_1" class="input-text" value="" placeholder="0.00" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td>
                                                <td><input type="text" name="txtDDCLPNetCostMT_1" id="txtDDCLPNetCostMT_1" class="input-text" readonly value="" /><input type="hidden" name="txtDDCLPCostMT_1" id="txtDDCLPCostMT_1" class="input-text" readonly value="0" /></td>
                                                <td><input type="text"  name="txtLPADDComm_1" id="txtLPADDComm_1" class="input-text" value="" placeholder="0.00" autocomplete="off" onKeyUp="getFinalCalculation();" /></td>
                                                <td><select name="selDDCLPVendor_1" class="input-text" style="width:120px;" id="selDDCLPVendor_1" ></select></td>
                                              </tr>	
                                              <tr id="DDCDProw_1"> 
												<td>Discharge Port <span id="spanDDCDPort_1"></span></td>
												<td><input type="text"  name="txtEstDDCDPCost_1" id="txtEstDDCDPCost_1" class="input-text" value="" placeholder="0.00" autocomplete="off" onKeyUp="getFinalCalculation();" /><input type="hidden" name="txtDPLayTimeIs_1" id="txtDPLayTimeIs_1" value="" /></td>
												<td><input type="text"  name="txtDDCDPCost_1" id="txtDDCDPCost_1" class="input-text" value="" placeholder="0.00" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td>
                                                <td><input type="text" name="txtDDCDPNetCostMT_1" id="txtDDCDPNetCostMT_1" class="input-text" readonly value="" /><input type="hidden" name="txtDDCDPCostMT_1" id="txtDDCDPCostMT_1" class="input-text" readonly value="0" /></td>
                                                <td><input type="text"  name="txtDPADDComm_1" id="txtDPADDComm_1" class="input-text" value="" placeholder="0.00" autocomplete="off" onKeyUp="getFinalCalculation();" /></td>
                                                <td><select name="selDDCDPVendor_1" class="input-text" style="width:120px;" id="selDDCDPVendor_1" ></select></td>
                                                <script>$("#selDDCDPVendor_1,#selDDCLPVendor_1").html($("#selVendor").html());</script>
											 </tr>
									 <?php }
											else
											{$i=1;
											while($myrows2 = mysql_fetch_assoc($myres2))
											{
											$laytime_valLP = $obj->getLayTimeValuesBasedOnComID($comid,$myrows2['FROM_PORT'],"LP",$myrows2['RANDOMID']);
											$laytime_valLP = explode('@#@',$laytime_valLP);
											if($laytime_valLP[1]== 0){$laytimeLP = $laytime_valLP[2];}else{$laytimeLP = $laytime_valLP[1];}
											if($laytime_valLP[0]== 0){$readonlyLP = "";}else{$readonlyLP = "readonly";}
											
											$laytime_valDP = $obj->getLayTimeValuesBasedOnComID($comid,$myrows2['TO_PORT'],"DP",$myrows2['RANDOMID']);
											$laytime_valDP = explode('@#@',$laytime_valDP);
											if($laytime_valDP[1]== 0){$laytimeDP = $laytime_valDP[2];}else{$laytimeDP = $laytime_valDP[1];}
											if($laytime_valDP[0]== 0){$readonlyDP = "";}else{$readonlyDP = "readonly";}
											if($myrows2['IS_SHOW_DDCDP'] > 0){$display1 = 'style="display:none;"';}else{$display1 = '';}
											if($myrows2['IS_SHOW_DDCLP'] > 0){$display2 = 'style="display:none;"';}else{$display2 = '';}
											?>
											<tr id="DDCLProw_<?php echo $i;?>" <?php echo $display2;?>> 
												<td>Load Port <span id="spanDDCLPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($myrows2['FROM_PORT']);?></span></td>
												<td><input type="text"  name="txtEstDDCLPCost_<?php echo $i;?>" id="txtEstDDCLPCost_<?php echo $i;?>" class="input-text" value="<?php echo $myrows2['DDCLP_ESTCOST'];?>" autocomplete="off" onKeyUp="getFinalCalculation();" <?php echo $readonlyLP;?>/><input type="hidden" name="txtLPLayTimeIs_<?php echo $i;?>" id="txtLPLayTimeIs_<?php echo $i;?>" value="<?php echo $laytime_valLP[0];?>" /></td>
												
												<td><input type="text"  name="txtDDCLPCost_<?php echo $i;?>" id="txtDDCLPCost_<?php echo $i;?>" style="color:red;" class="input-text" value="<?php echo $laytimeLP;?>" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td>
                                                <td><input type="text"  name="txtLPADDComm_<?php echo $i;?>" id="txtLPADDComm_<?php echo $i;?>" class="input-text" value="<?php echo $myrows2['DDCLP_COMM'];?>" placeholder="0.00" autocomplete="off" onKeyUp="getFinalCalculation();" /></td>
                                                <?php $omcvalueLP = $myrows2['DDCLP_ESTCOST'] - (($myrows2['DDCLP_ESTCOST'] * $myrows2['DDCLP_COMM'])/100);?>
												<td><input type="text" name="txtDDCLPNetCostMT_<?php echo $i;?>" id="txtDDCLPNetCostMT_<?php echo $i;?>" class="input-text" readonly value="<?php echo $omcvalueLP;?>" /><input type="hidden" name="txtDDCLPCostMT_<?php echo $i;?>" id="txtDDCLPCostMT_<?php echo $i;?>" class="input-text" readonly value="<?php echo $myrows2['DDCLP_NETCOST'];?>" /></td>
                                                <td><select name="selDDCLPVendor_<?php echo $i;?>" class="input-text" style="width:120px;" id="selDDCLPVendor_<?php echo $i;?>" ></select></td>
                                              </tr>	
                                              <tr id="DDCDProw_<?php echo $i;?>" <?php echo $display1;?>> 
												<td>Discharge Port <span id="spanDDCDPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($myrows2['TO_PORT']);?></span></td>
												<td><input type="text"  name="txtEstDDCDPCost_<?php echo $i;?>" id="txtEstDDCDPCost_<?php echo $i;?>" class="input-text" value="<?php echo $myrows2['DDCDP_ESTCOST'];?>" autocomplete="off" onKeyUp="getFinalCalculation();" <?php echo $readonlyDP;?>/><input type="hidden" name="txtDPLayTimeIs_<?php echo $i;?>" id="txtDPLayTimeIs_<?php echo $i;?>" value="<?php echo $laytime_valDP[0];?>" /></td>
												
												<td><input type="text"  name="txtDDCDPCost_<?php echo $i;?>" id="txtDDCDPCost_<?php echo $i;?>" style="color:red;" class="input-text" value="<?php echo $laytimeDP;?>" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td>
                                                <td><input type="text"  name="txtDPADDComm_<?php echo $i;?>" id="txtDPADDComm_<?php echo $i;?>" class="input-text" value="<?php echo $myrows2['DDCDP_COMM'];?>" placeholder="0.00" autocomplete="off" onKeyUp="getFinalCalculation();" /></td>
                                                <?php $omcvalueDP = $myrows2['DDCDP_ESTCOST'] - (($myrows2['DDCDP_ESTCOST'] * $myrows2['DDCDP_COMM'])/100);?>
												<td><input type="text" name="txtDDCDPNetCostMT_<?php echo $i;?>" id="txtDDCDPNetCostMT_<?php echo $i;?>" class="input-text" readonly value="<?php echo $omcvalueDP;?>" /><input type="hidden" name="txtDDCDPCostMT_<?php echo $i;?>" id="txtDDCDPCostMT_<?php echo $i;?>" class="input-text" readonly value="<?php echo $myrows2['DDCDP_NETCOST'];?>" /></td>
                                                <td><select name="selDDCDPVendor_<?php echo $i;?>" class="input-text" style="width:120px;" id="selDDCDPVendor_<?php echo $i;?>" ></select></td>
                                                <script>$("#selDDCDPVendor_<?php echo $i;?>,#selDDCLPVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selDDCDPVendor_<?php echo $i;?>").val('<?php echo $myrows2['DDCDP_VENDOR'];?>');$("#selDDCLPVendor_<?php echo $i;?>").val('<?php echo $myrows2['DDCLP_VENDOR'];?>');</script>
                                              </tr>			
									<?php $i++;}} ?>
                                         </tbody>
									</table>
                                    
                                    <?php if($_SESSION['selBType']==1 || $_SESSION['selBType']==3){?>
                                    <div><strong style="font-size:13px;">Comparision</strong></div>
								    <table class='tablesorter'>
                                        <thead>
											<tr>
												<th width="40%" style="background-color:#8d8a8a;"></th>
												<th width="20%" style="background-color:#8d8a8a;">Floating</th>
                                                <th width="20%" style="background-color:#8d8a8a;">Fixed</th>
                                                <th width="20%" style="background-color:#8d8a8a;">Average</th>
											</tr>
										</thead>
										<tbody>
                                            <tr>
                                                <td>Base Rate</td>
                                                <td><input type="text"  name="txtBaseRateFloating" id="txtBaseRateFloating" style="width:100px;" class="input-text" value="<?php echo $obj->getFun176();?>" readonly autocomplete="off"/></td>
                                                <td><input type="text"  name="txtBaseRateFixed" id="txtBaseRateFixed" style="width:100px;" readonly class="input-text numeric" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getFun177();?>" autocomplete="off"/></td>
                                                <td><input type="text"  name="txtBaseRateAverage" id="txtBaseRateAverage" style="width:100px;" class="input-text" value="<?php echo $obj->getFun178();?>" readonly autocomplete="off"/></td>
                                            </tr>
                                            <tr>
                                                <td>Gross Freight</td>
                                                <td><input type="text"  name="txtGFreightFloating" id="txtGFreightFloating" style="width:100px;" class="input-text" value="<?php echo $obj->getFun179();?>" readonly autocomplete="off"/></td>
                                                <td><input type="text"  name="txtGFreightFixed" id="txtGFreightFixed" style="width:100px;" readonly class="input-text numeric" value="<?php echo $obj->getFun180();?>" onKeyUp="getFinalCalculation();" autocomplete="off"/></td>
                                                <td><input type="text"  name="txtGFreightAverage" id="txtGFreightAverage" style="width:100px;" class="input-text" value="<?php echo $obj->getFun181();?>" readonly autocomplete="off"/></td>
                                            </tr>
                                            <tr>
                                                <td>Net Freight</td>
                                                <td><input type="text"  name="txtNFreightFloating" id="txtNFreightFloating" style="width:100px;" class="input-text" value="<?php echo $obj->getFun182();?>" readonly autocomplete="off"/></td>
                                                <td><input type="text"  name="txtNFreightFixed" id="txtNFreightFixed" style="width:100px;" readonly class="input-text numeric" value="<?php echo $obj->getFun183();?>" onKeyUp="getFinalCalculation();" autocomplete="off"/></td>
                                                <td><input type="text"  name="txtNFreightAverage" id="txtNFreightAverage" style="width:100px;" class="input-text" value="<?php echo $obj->getFun184();?>" readonly autocomplete="off"/></td>
                                            </tr>
                                            <tr>
                                                <td>Total Owner Expenses</td>
                                                <td><input type="text"  name="txtOExpensesFloating" id="txtOExpensesFloating" style="width:100px;" class="input-text" value="<?php echo $obj->getFun185();?>" readonly autocomplete="off"/></td>
                                                <td><input type="text"  name="txtOExpensesFixed" id="txtOExpensesFixed" style="width:100px;" readonly class="input-text numeric" value="<?php echo $obj->getFun186();?>" onKeyUp="getFinalCalculation();" autocomplete="off"/></td>
                                                <td><input type="text"  name="txtOExpensesAverage" id="txtOExpensesAverage" style="width:100px;" class="input-text" value="<?php echo $obj->getFun187();?>" readonly autocomplete="off"/></td>
                                            </tr>
                                            <tr>
                                                <td>Voyage Earnings <span style="font-size:10px; font-style:italic;">((Frt + Dem))</span></td>
                                                <td><input type="text"  name="txtVEarningFloating" id="txtVEarningFloating" style="width:100px;" class="input-text" value="<?php echo $obj->getFun188();?>" readonly autocomplete="off"/></td>
                                                <td><input type="text"  name="txtVEarningFixed" id="txtVEarningFixed" style="width:100px;" readonly class="input-text numeric" value="<?php echo $obj->getFun189();?>" onKeyUp="getFinalCalculation();" autocomplete="off"/></td>
                                                <td><input type="text"  name="txtVEarningAverage" id="txtVEarningAverage" style="width:100px;" class="input-text" value="<?php echo $obj->getFun190();?>" readonly autocomplete="off"/></td>
                                            </tr>
                                            <?php if($_SESSION['selBType']==1){$readonlytr = 'readonly';$displaytr = '';$border_textbox='';}?>
                                            <?php if($_SESSION['selBType']==3){$readonlytr = '';$displaytr = 'style="display:none;"';$border_textbox='border:1px solid #F00;';}?>
                                            <tr>
                                                <td>Daily Earnings/TCE</td>
                                                <td><input type="text"  name="txtDailyEarningFloating" id="txtDailyEarningFloating" style="width:100px;" class="input-text" value="<?php echo $obj->getFun191();?>" readonly autocomplete="off"/></td>
                                                <td><input type="text"  name="txtDailyEarningFixed" id="txtDailyEarningFixed" style="width:100px;<?php echo $border_textbox;?>" <?php echo $readonlytr;?> class="input-text numeric" value="<?php echo $obj->getFun192();?>" onKeyUp="getFinalCalculation();" autocomplete="off"/></td>
                                                <td><input type="text"  name="txtDailyEarningAverage" id="txtDailyEarningAverage" style="width:100px;" class="input-text" value="<?php echo $obj->getFun193();?>" readonly autocomplete="off"/></td>
                                            </tr>
                                            <tr <?php echo $displaytr;?>>
                                                <td>TCE Per Calender Month</td>
                                                <td><input type="text"  name="txtTCEMonthFloating" id="txtTCEMonthFloating" style="width:100px;" class="input-text" value="<?php echo $obj->getFun194();?>" readonly autocomplete="off"/></td>
                                                <td><input type="text"  name="txtTCEMonthFixed" id="txtTCEMonthFixed" class="input-text numeric" style="width:100px; border:1px solid #F00;" value="<?php echo $obj->getFun195();?>" onKeyUp="getFinalCalculation();" autocomplete="off"/></td>
                                                <td><input type="text"  name="txtTCEMonthAverage" id="txtTCEMonthAverage" style="width:100px;" class="input-text" value="<?php echo $obj->getFun196();?>" readonly autocomplete="off"/></td>
                                            </tr>
                                            <tr>
                                                <td>Daily Vessel Operation Expenses(Technical)</td>
                                                <td><input type="text"  name="txtOPExpensesFloating" id="txtOPExpensesFloating" class="input-text numeric" style="width:100px; border:1px solid #F00;" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getFun197();?>" autocomplete="off"/></td>
                                                <td><input type="text"  name="txtOPExpensesFixed" id="txtOPExpensesFixed" style="width:100px;" class="input-text numeric" readonly value="<?php echo $obj->getFun198();?>" onKeyUp="getFinalCalculation();" autocomplete="off"/></td>
                                                <td><input type="text"  name="txtOPExpensesAverage" id="txtOPExpensesAverage" style="width:100px;" class="input-text" value="<?php echo $obj->getFun199();?>" readonly autocomplete="off"/></td>
                                            </tr>
                                            <tr>
                                                <td>Net Daily Profit</td>
                                                <td><input type="text"  name="txtNetDailyProfitFloating" id="txtNetDailyProfitFloating" style="width:100px;" class="input-text" value="<?php echo $obj->getFun203();?>" readonly autocomplete="off"/></td>
                                                <td><input type="text"  name="txtNetDailyProfitFixed" id="txtNetDailyProfitFixed" style="width:100px;" class="input-text" onKeyUp="getFinalCalculation();" value="<?php echo $obj->getFun204();?>" autocomplete="off"/></td>
                                                <td><input type="text"  name="txtNetDailyProfitAverage" id="txtNetDailyProfitAverage" style="width:100px;" class="input-text" value="<?php echo $obj->getFun205();?>" readonly autocomplete="off"/></td>
                                            </tr>
									 	</tbody>
									</table>
                                    <?php }?>
                           </td>
                           <td width="40%" id="parenttdID" style="border-left:1px solid #D8D8D8;vertical-align:top;">
                                
                                <input type="hidden" name="txtBunkerWidth" id="txtBunkerWidth" readonly value=""/>
                                <input type="hidden" name="txtBunkerWidth1" id="txtBunkerWidth1" readonly value="1"/>
                                <div style="overflow:auto; max-width:500px;" id="bunkercontainer">
                                <table class="tablesorter">
										<thead>
											<tr class="GridviewScrollHeader">
												<th colspan="1">Bunkers</th>
												<?php
													$ttl_bg = array();$ttl_bg1 = array();
													$sql = "SELECT * FROM bunker_grade_master where STATUS=1";
													$res = mysql_query($sql);
													$rec = mysql_num_rows($res);
													
													if($rec == 0)
													{
												?>
												<tr>
													<td valign="top" align="center" colspan="6" style="color:red;">First fill the Bunker Grade Master Data.</td>
												</tr>
												<?php	
												}else{
												?>
												<?php $m=0; while($rows = mysql_fetch_assoc($res)){
													$ttl_bg[]  = $rows['NAME'];
													$ttl_bgtype[]  = $rows['BUNKERTYPE'];
													$ttl_bg1[] = $rows['BUNKERGRADEID'];
												?>
													 <th colspan="3" class="input-text"><?php echo $rows['NAME'];?>&nbsp;&nbsp;</th>
												<?php $m++; ?>
											<?php } ?>
											</tr>
										</thead>
										<tbody>
											<tr class="GridviewScrollHeader">
												<td>&nbsp;</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){?>
												<td class="input-text">MT</td>
												<td class="input-text">Price</td>
												<td class="input-text">Cost<input type="hidden" name="txtBunkerRec" id="txtBunkerRec" class="input-text" style="width:60px;" readonly value="<?php echo $rec;?>"/>			 
												<input type="hidden" name="txtBunkerGradeName_<?php echo $i+1;?>" id="txtBunkerGradeName_<?php echo $i+1;?>" class="input-text" style="width:60px;" readonly value="<?php echo $ttl_bg[$i];?>"/>	
                                                <input type="hidden" name="txtBunkerGradeType_<?php echo $i+1;?>" id="txtBunkerGradeType_<?php echo $i+1;?>" class="input-text" readonly value="<?php echo $ttl_bgtype[$i];?>"/>	
												<input type="hidden" name="txtBunkerGradeID_<?php echo $i+1;?>" id="txtBunkerGradeID_<?php echo $i+1;?>" class="input-text" style="width:60px;" readonly value="<?php echo $ttl_bg1[$i];?>"/>
												 <input type="hidden" name="txtBHID_<?php echo $ttl_bg1[$i];?>" id="txtBHID_<?php echo $ttl_bg1[$i];?>" value="<?php echo $ttl_bg[$i];?>" class="input-text" style="width:60px;"/>
												 <input type="hidden" name="txtBHID1[]" id="txtBHID1_<?php echo $ttl_bg1[$i];?>" value="<?php echo $ttl_bg1[$i];?>" class="input-text" style="width:60px;"/>
												 <input type="hidden" name="txtTTLBAA_<?php echo $ttl_bg1[$i];?>" id="txtTTLBAA_<?php echo $ttl_bg1[$i];?>" class="input-text" style="width:60px;" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"OWNER_ADJ_COST");?>" />
												 <input type="hidden" name="txtTTLEst_<?php echo $ttl_bg1[$i];?>" id="txtTTLEst_<?php echo $ttl_bg1[$i];?>" class="input-text" style="width:60px;" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"EST_COST");?>"  /></td>
												<?php }?>
											</tr>
		
											<tr class="GridviewScrollItem">
												<td class="input-text">Estimated</td>
												<?php $j=$k=0; for($i=0;$i<count($ttl_bg);$i++){			
												?>
												<td>
												<input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_1" id="txt<?php echo $ttl_bg[$i];?>_1_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"EST_MT");?>" class="input-text" style="width:60px;" autocomplete="off"/>
												</td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_2" id="txt<?php echo $ttl_bg[$i];?>_1_2" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"EST_PRICE");?>" class="input-text" style="width:60px;" autocomplete="off"/></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_3" id="txt<?php echo $ttl_bg[$i];?>_1_3" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"EST_COST");?>" onKeyUp="getBunkerCalculation();" class="input-text" style="width:60px;" autocomplete="off" readonly/></td>
												<?php }?>
											</tr>
                                            
                                            
										<?php }?>									
										</tbody>
									</table>
                                
                                </div>
                                <div><strong style="font-size:13px;">Bunkers Consumed</strong></div>
                                <table width="100%" class='tablesorter'>
								  <tr>
								    <td></td>
									<td>Bunker Grade</td>
									<td>Qty(MT)</td>
									<td>Price(USD)</td>
									<td>Amount(USD)</td>
									</tr>
									<tbody id="ConbunkerTBody">
                                    <?php $sql_bunk = "select * from freight_cost_estimete_slave8 where FCAID='".$obj->getFun1()."' and IDENTIFY='CONSUMPTION'";
									$res_bunk = mysql_query($sql_bunk);
									$num_bunk = mysql_num_rows($res_bunk);
									if($num_bunk==0)
									{$num_bunk =1;?>
							  		<tr id="DivConBunkeractual_1">
									<td><a href="#tb1'" onClick="removeConBunkerRows(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
									<td><select name="selConBunker_1" id="selConBunker_1" style="width:90px;" class="input-text"><?php $obj->getBunkerList();?></select></td>
									<td><input type="text" name="txtConBunkerQty_1" id="txtConBunkerQty_1" style="width:90px;" class="input-text numeric" onKeyUp="getBunkerCalculation();" placeholder="0.00" autocomplete="off" value=""/></td>
									<td><input type="text" name="txtConBunkerPrice_1" id="txtConBunkerPrice_1" style="width:90px;" class="input-text numeric" onKeyUp="getBunkerCalculation();" placeholder="0.00" autocomplete="off" value=""/></td>
									<td><input type="text" name="txtConBunkerAmt_1" id="txtConBunkerAmt_1" style="width:90px;" class="input-text"  placeholder="0.00" readonly autocomplete="off" value=""/></td>
								  </tr>
                                  <?php }else{ $bunk = 0;
									  while($rows_bunk = mysql_fetch_assoc($res_bunk))
									  {$bunk++;
									  ?>
								   <tr id="DivConBunkeractual_<?php echo $bunk;?>">
									<td><a href="#tb<?php echo $bunk;?>'" onClick="removeConBunkerRows(<?php echo $bunk;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
									<td><select name="selConBunker_<?php echo $bunk;?>" id="selConBunker_<?php echo $bunk;?>" style="width:90px;" class="input-text"><?php $obj->getBunkerList();?></select></td>
									<td><input type="text" name="txtConBunkerQty_<?php echo $bunk;?>" id="txtConBunkerQty_<?php echo $bunk;?>" style="width:90px;" class="input-text numeric" onKeyUp="getBunkerCalculation();" placeholder="0.00" autocomplete="off" value="<?php echo $rows_bunk['QTY'];?>"/></td>
									<td><input type="text" name="txtConBunkerPrice_<?php echo $bunk;?>" id="txtConBunkerPrice_<?php echo $bunk;?>" style="width:90px;" class="input-text numeric" onKeyUp="getBunkerCalculation();" placeholder="0.00" autocomplete="off" value="<?php echo $rows_bunk['PRICE'];?>"/></td>
									<td><input type="text" name="txtConBunkerAmt_<?php echo $bunk;?>" id="txtConBunkerAmt_<?php echo $bunk;?>" style="width:90px;" class="input-text"  placeholder="0.00" readonly autocomplete="off" value="<?php echo $rows_bunk['COST'];?>"/></td>
								  </tr>
                                  <script>$("#selConBunker_<?php echo $bunk;?>").val('<?php echo $rows_bunk['BUNKERGRADEID'];?>');</script>
                                  <?php }$num_bunk =$bunk; }?>
								</tbody>
								  <tr>
									<td colspan="5"><button type="button" onClick="addConBunkerRow()">Add</button><input type="hidden" name="txtConBunkerCount" id="txtConBunkerCount" value="<?php echo $num_bunk;?>"/></td>
								  </tr>
								</table>
                                <div><strong style="font-size:13px;">Bunkers Supplied</strong></div>
                                <table width="100%" class='tablesorter'>
								  <tr>
								    <td></td>
									<td>Bunker Grade</td>
									<td>Qty(MT)</td>
									<td>Price(USD)</td>
									<td>Amount(USD)</td>
                                    <td>Port</td>
									<td>Vendor</td>
									</tr>
									<tbody id="bunkerTBody">
                                    <?php $sql_bunk = "select * from freight_cost_estimete_slave8 where FCAID='".$obj->getFun1()."' and IDENTIFY='SUPPLY'";
									$res_bunk = mysql_query($sql_bunk);
									$num_bunk = mysql_num_rows($res_bunk);
									if($num_bunk==0)
									{$num_bunk =1;?>
							  		<tr id="DivBunkeractual_1">
									<td><a href="#tb1'" onClick="removeBunkerRows(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
									<td><select name="selBunker_1" id="selBunker_1" style="width:100px;" class="input-text"><?php $obj->getBunkerList();?></select></td>
									<td><input type="text" name="txtBunkerQty_1" id="txtBunkerQty_1" style="width:95px;" class="input-text numeric" onKeyUp="getBunkerCalculation();" placeholder="0.00" autocomplete="off" value=""/></td>
									<td><input type="text" name="txtBunkerPrice_1" id="txtBunkerPrice_1" style="width:95px;" class="input-text numeric" onKeyUp="getBunkerCalculation();" placeholder="0.00" autocomplete="off" value=""/></td>
									<td><input type="text" name="txtBunkerAmt_1" id="txtBunkerAmt_1" style="width:95px;" class="input-text"  placeholder="0.00" readonly autocomplete="off" value=""/></td>
                                    <td><select name="selBunkerPort_1" id="selBunkerPort_1" style="width:100px;" class="input-text"></select></td>
									<td><select name="selBunkerVendor_1" id="selBunkerVendor_1" style="width:120px;" class="input-text"></select></td>
								  </tr>
                                  <script>$("#selBunkerVendor_1").html($("#selVendor").html());$("#selBunkerPort_1").html($("#selPort").html());</script>
                                  <?php }else{ $bunk = 0;
									  while($rows_bunk = mysql_fetch_assoc($res_bunk))
									  {$bunk++;
									  ?>
								   <tr id="DivBunkeractual_<?php echo $bunk;?>">
									<td><a href="#tb<?php echo $bunk;?>'" onClick="removeBunkerRows(<?php echo $bunk;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
									<td><select name="selBunker_<?php echo $bunk;?>" id="selBunker_<?php echo $bunk;?>" style="width:100px;" class="input-text"><?php $obj->getBunkerList();?></select></td>
									<td><input type="text" name="txtBunkerQty_<?php echo $bunk;?>" id="txtBunkerQty_<?php echo $bunk;?>" style="width:95px;" class="input-text numeric" onKeyUp="getBunkerCalculation();" placeholder="0.00" autocomplete="off" value="<?php echo $rows_bunk['QTY'];?>"/></td>
									<td><input type="text" name="txtBunkerPrice_<?php echo $bunk;?>" id="txtBunkerPrice_<?php echo $bunk;?>" style="width:95px;" class="input-text numeric" onKeyUp="getBunkerCalculation();" placeholder="0.00" autocomplete="off" value="<?php echo $rows_bunk['PRICE'];?>"/></td>
									<td><input type="text" name="txtBunkerAmt_<?php echo $bunk;?>" id="txtBunkerAmt_<?php echo $bunk;?>" style="width:95px;" class="input-text"  placeholder="0.00" readonly autocomplete="off" value="<?php echo $rows_bunk['COST'];?>"/></td>
                                    <td><select name="selBunkerPort_<?php echo $bunk;?>" id="selBunkerPort_<?php echo $bunk;?>" style="width:100px;" class="input-text"></select></td>
									<td><select name="selBunkerVendor_<?php echo $bunk;?>" id="selBunkerVendor_<?php echo $bunk;?>" style="width:120px;" class="input-text"></select></td>
								  </tr>
                                  <script>$("#selBunkerVendor_<?php echo $bunk;?>").html($("#selVendor").html());$("#selBunkerVendor_<?php echo $bunk;?>").val('<?php echo $rows_bunk['VENDORID'];?>');$("#selBunker_<?php echo $bunk;?>").val('<?php echo $rows_bunk['BUNKERGRADEID'];?>');$("#selBunkerPort_<?php echo $bunk;?>").html($("#selPort").html());$("#selBunkerPort_<?php echo $bunk;?>").val('<?php echo $rows_bunk['PORT'];?>');</script>
                                  <?php }$num_bunk =$bunk; }?>
								</tbody>
								  <tr>
									<td colspan="7"><button type="button" onClick="addBunkerRow()">Add</button><input type="hidden" name="txtBunkerCount" id="txtBunkerCount" value="<?php echo $num_bunk;?>"/></td>
								  </tr>
								</table>
                                
                                
                                <div><strong style="font-size:13px;">Operational Costs (Others)</strong></div>
								
                                <table class='tablesorter'>
                                    <tbody>
                                        <tr>
                                            <td colspan="2">Address Commission</td>
                                            <td><input type="text"  name="txtFrAdjPerAC" id="txtFrAdjPerAC" class="input-text" autocomplete="off" size="12" value="<?php echo $obj->getFun74();?>" onKeyUp="getFinalCalculation();" placeholder="0.00"/></td>
                                            <td><input type="text"  name="txtFrAdjUsdAC" id="txtFrAdjUsdAC" class="input-text" readonly value="<?php echo $obj->getFun75();?>" size="12" /></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    <tbody id="tbodyBrokerage">
                                    <?php 
									$sql_brok = "select * from freight_cost_estimete_slave4 where FCAID='".$obj->getFun1()."'";
									
									$res_brok = mysql_query($sql_brok);
									$num_brok = mysql_num_rows($res_brok);
									if($num_brok==0)
									{$num_brok =1;
									?>
									 <tr id="tbrRow_1">
                                        <td><a href="#tb1'" onClick="removeBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                        <td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
                                        
                                        <td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="input-text" autocomplete="off" size="12" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"  /></td>
                                        <td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="input-text" readonly value="0.00" size="12" /></td>
                                        <td><select  name="selBrCommVendor_1" class="input-text" style="width:120px;" id="selBrCommVendor_1"></select></td>
                                        <script>$("#selBrCommVendor_1").html($("#selVendor").html());</script>
                                    </tr>
									<?php }
									     else
									      {$i=0;$num_brok = $num_brok;
										  while($rows_brok = mysql_fetch_assoc($res_brok))
										  {$i = $i + 1;?>
										     <tr id="tbrRow_<?php echo $i;?>">
                                                <td><a href="#tb1'" onClick="removeBrokerage(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												<td><input type="text" name="txtBrCommPercent_<?php echo $i;?>" id="txtBrCommPercent_<?php echo $i;?>" class="input-text" autocomplete="off" size="12" value="<?php echo $rows_brok['BROKAGE_PERCENT'];?>" onKeyUp="getFinalCalculation();" placeholder="0.00"/></td>
												<td><input type="text" name="txtBrComm_<?php echo $i;?>" id="txtBrComm_<?php echo $i;?>" class="input-text" readonly size="12" value="<?php echo $rows_brok['BROKAGE_AMT'];?>" /></td>
												<td><select  name="selBrCommVendor_<?php echo $i;?>" class="input-text" style="width:120px;" id="selBrCommVendor_<?php echo $i;?>"></select></td>
                                                <script>$("#selBrCommVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selBrCommVendor_<?php echo $i;?>").val('<?php echo $rows_brok['VENDORID'];?>');</script>
											 </tr>
										  <?php }} ?>
                                     
                                    </tbody>
                                    
                                    <tbody>
                                    <tr>
                                        <td><button type="button" onClick="addBrokerageRow()">Add</button><input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="<?php echo $num_brok;?>"/></td>
                                        <td>Total&nbsp;Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
                                        
                                        <td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="input-text" size="12" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();" readonly/></td>
                                        <td><input type="text" name="txtBrComm" id="txtBrComm" class="input-text" readonly size="12" value="0.00" /></td>
                                        <td>
                                        </td>
                                    </tr>
                                   </tbody>
                                 </table>
								
                                <table class='tablesorter'>
                                    <thead>
                                        <tr>
                                            <th width="20%"></th>
                                            <th width="20%"></th>
                                            <th width="20%">Cost</th>
                                            <th width="20%">Vendor</th>
                                        </tr>
                                   </thead>
                                   <tbody id="OWCBody">
                                   <?php $sql1 = "select * from freight_cost_estimete_slave3 where FCAID='".$id."' and IDENTIFY='ORC' and RAW_AMOUNT>0";
								   $res1 = mysql_query($sql1) or die($sql1);
								   $num1 = mysql_num_rows($res1);
								   if($num1==0)
								   {$num1 = 1;?>
									   
                                   <tr id="OWCRow_1">
                                        <td><a href="#tb1'" onClick="removeCC(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                        <td><select name="txtHidORCID_1" id="txtHidORCID_1" style="width:130px;" >
                                        <?php $obj->getOwnerRelatedCostList(0);?>
                                        </select></td>
                                        <td><input type="text" name="txtORCAmt_1" id="txtORCAmt_1" onKeyUp="getORCCalculate(1);" class="input-text" autocomplete="off" value="" placeholder="0.00" size="12"  /></td>
                                        <td><select  name="selORCVendor_1" class="input-text" style="width:120px;" id="selORCVendor_1"></select></td>
											<script>$("#selORCVendor_1").html($("#selVendor").html());</script>
                                    </tr> 
								 <?php }else
								   {$i = 0;
								   while($rows1 = mysql_fetch_assoc($res1))
								   {
										$i = $i + 1;
										?>
									 <tr id="OWCRow_<?php echo $i;?>">
                                        <td><a href="#tb1'" onClick="removeCC(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                        <td><select name="txtHidORCID_<?php echo $i;?>" id="txtHidORCID_<?php echo $i;?>" style="width:130px;" >
                                        <?php $obj->getOwnerRelatedCostList($rows1['IDENTY_ID']);?>
                                        </select></td>
                                        <td><input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>" onKeyUp="getORCCalculate(<?php echo $i;?>);" class="input-text" autocomplete="off" value="<?php echo $rows1['RAW_AMOUNT'];?>" placeholder="0.00" size="12"  /></td>
                                        <td><select  name="selORCVendor_<?php echo $i;?>" class="input-text" style="width:120px;" id="selORCVendor_<?php echo $i;?>"></select></td>
										<script>$("#selORCVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selORCVendor_<?php echo $i;?>").val('<?php echo $rows1['VENDORID'];?>');</script>
                                    </tr>
									 <?php }}?>
									</tbody>
                                    <tfoot>
                                        <tr>
                                        <th colspan="4" align="left"><button type="button" onClick="addORCRow()">Add</button><input type="hidden" name="txtOWCCount" id="txtOWCCount" value="<?php echo $num1;?>"/></th>
                                        </tr>
                                    </tfoot>
                                 </table>
                                 <table width="100%" class='tablesorter'>
                                     <tbody>
                                         <tr>
                                            <td>CVE <span style="font-size:10px; font-style:italic;">(/Month)</span></td>
                                            <td><input type="text" name="txtCVE" style="width:90px;" id="txtCVE" onKeyUp="getFinalCalculation();" class="input-text" autocomplete="off" value="<?php echo $obj->getFun142();?>" placeholder="0.00"></td>
                                            <td>CVE <span style="font-size:10px; font-style:italic;">(USD)</span></td>
                                            <td><input type="text" name="txtCVEAmt" style="width:90px;" id="txtCVEAmt" class="input-text" autocomplete="off" value="<?php echo $obj->getFun143();?>" placeholder="0.00" readonly><input type="hidden" name="txtHidCVEAmt" style="width:90px;" id="txtHidCVEAmt" autocomplete="off" value="<?php echo $obj->getFun143();?>"></td>
                                            <td><select name="selCVEVendor" class="input-text" style="width:120px;" id="selCVEVendor"></select></td>
                                            
                                        </tr>
                                     </tbody>
                                 </table>
                                 <div><strong style="font-size:13px;">Other Income</strong></div>
                                <table class='tablesorter'>
                                    <thead>
                                        <tr>
                                            <th width="5%"></th>
                                            <th width="35%">Description</th>
                                            <th width="20%">Amount(USD)</th>
                                            <th width="20%">Add Comm(%)</th>
                                            <th width="20%">Net Amount(USD)</th>
                                            <th width="20%">Vendor</th>
                                        </tr>
                                   </thead>
                                   <tbody id="OthInBody">
                                   <?php $sql3 = "select * from freight_cost_estimete_slave3 where FCAID='".$id."' and IDENTIFY='OTHERINCOME' and RAW_AMOUNT>0";
								   $res3 = mysql_query($sql3) or die($sql3);
								   $num3 = mysql_num_rows($res3);
								   if($num3==0)
								   {$num3 = 1;?>
                                     <tr id="OthInRow_1">
                                        <td><a href="#tb1'" onClick="removeOtherIncome(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                        <td><input type="text" name="txtDesc_1" id="txtDesc_1" class="input-text" autocomplete="off" value="" placeholder="Description" size="29"  /></td>
                                        <td><input type="text" name="txtOtherAmt_1" id="txtOtherAmt_1" onKeyUp="getFinalCalculation(1);" class="input-text" autocomplete="off" value="" placeholder="0.00" size="12"  /></td>
                                        <td><input type="text" name="txtOtherAddComm_1" id="txtOtherAddComm_1" onKeyUp="getFinalCalculation(1);" class="input-text" autocomplete="off" value="" placeholder="0.00" size="12"  /></td>
                                        <td><input type="text" name="txtOtherNetAmt_1" id="txtOtherNetAmt_1"  class="input-text" autocomplete="off" size="12" value="0.00"  readonly /></td>
                                        <td><select  name="selOtherIncVendor_1" class="input-text" style="width:100px;" id="selOtherIncVendor_1"></select></td>
											<script>$("#selOtherIncVendor_1").html($("#selVendor").html());</script>
                                    </tr>
                                 <?php }else
								   {$i = 0;
								   while($rows3 = mysql_fetch_assoc($res3))
								   {$i = $i + 1;?>
                                     <tr id="OthInRow_<?php echo $i;?>">
                                        <td><a href="#tb1'" onClick="removeOtherIncome(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                        <td><input type="text" name="txtDesc_<?php echo $i;?>" id="txtDesc_<?php echo $i;?>" class="input-text" autocomplete="off" value="<?php echo $rows3['IDENTY_ID'];?>" placeholder="Description" size="29"  /></td>
                                        <td><input type="text" name="txtOtherAmt_<?php echo $i;?>" id="txtOtherAmt_<?php echo $i;?>" onKeyUp="getFinalCalculation();" class="input-text" autocomplete="off" value="<?php echo $rows3['COST'];?>" placeholder="0.00" size="12"  /></td>
                                        <td><input type="text" name="txtOtherAddComm_<?php echo $i;?>" id="txtOtherAddComm_<?php echo $i;?>" onKeyUp="getFinalCalculation();" class="input-text" autocomplete="off" value="<?php echo $rows3['ADDCOMM'];?>" placeholder="0.00" size="12"  /></td>
                                        <td><input type="text" name="txtOtherNetAmt_<?php echo $i;?>" id="txtOtherNetAmt_<?php echo $i;?>"  class="input-text" autocomplete="off" size="12" value="<?php echo $rows3['RAW_AMOUNT'];?>"  readonly /></td>
                                        <td><select  name="selOtherIncVendor_<?php echo $i;?>" class="input-text" style="width:100px;" id="selOtherIncVendor_<?php echo $i;?>"></select></td>
										<script>$("#selOtherIncVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selOtherIncVendor_<?php echo $i;?>").val('<?php echo $rows3['VENDORID'];?>');</script>
                                    </tr>
                                 <?php }}?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <th colspan="6" align="left"><button type="button" onClick="addOtherInRow()">Add</button><input type="hidden" name="txtOthInCount" id="txtOthInCount" value="<?php echo $num3;?>"/></th>
                                        </tr>
                                    </tfoot>
                                </table>
                                    <div><strong>Hire</strong></div>
                                    <table class='tablesorter'>
                                        <tbody>
                                            <?php $sqlinvoice = "select INVOICE_TYPE, INVOICE_NO, FINAL_AMT, P_AMT, INVOICEID from invoice_hire_master WHERE MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$comid."' and STATUS=1 and FCAID<='".$obj->getFun1()."' order by INVOICEID";
											$resinvoice = mysql_query($sqlinvoice) or die($sqlinvoice);
											$numinvoice = mysql_num_rows($resinvoice);
											if($numinvoice>0){$readonly = "readonly";}
											else{$readonly = "";}
											?>
                                            <tr>
                                                <td>Daily Time Charter (USD/Day)</td>
                                                <td></td>
                                                <td></td>
                                                <td><input type="text"  name="txtDailyVesselOperatingExpenses" style="width:90px;" class="input-text" id="txtDailyVesselOperatingExpenses"  autocomplete="off" value="<?php echo $obj->getFun91();?>" placeholder="0.00" onKeyUp="getFinalCalculation();" <?php echo $readonly;?>/></td>
                                                <td width="25%"><select name="selDTCVendor" class="input-text" style="width:120px;" id="selDTCVendor" ></select></td>
											<script>$("#selDTCVendor").html($("#selVendor").html());</script>
                                            </tr>
                                            <tr>
                                                <td>Hireage</td>
                                                <td><input type="text"  name="txtHireargeAmt" id="txtHireargeAmt" style="width:90px;" class="input-text" value="<?php echo $obj->getFun132();?>" placeholder="0.00" readonly /></td>
                                                <td>Ballast Bonus</td>
                                                <td><input type="text" name="txtBallastBonus" id="txtBallastBonus" style="width:90px;" onKeyUp="getFinalCalculation();" class="input-text" autocomplete="off" value="<?php echo $obj->getFun145();?>" placeholder="0.00"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Add Comm <span style="font-size:10px; font-style:italic;">(%)</span></td>
                                                <td><input type="text"  name="txtHireargePercent" style="width:90px;" id="txtHireargePercent" class="input-text" value="<?php echo $obj->getFun129();?>" placeholder="%" onKeyUp="getFinalCalculation();"/></td>
                                                <td>Add Comm <span style="font-size:10px; font-style:italic;">(USD)</span></td>
                                                <td><input type="text"  name="txtHireargePercentAmt" style="width:90px;" id="txtHireargePercentAmt" class="input-text" value="<?php echo $obj->getFun130();?>" placeholder="0.00" readonly /></td>
                                                <td></td>
                                            </tr>
                                            
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>Nett Hireage</td>
                                                <td><input type="text"  name="txtNettHireargeAmt" style="width:90px;" id="txtNettHireargeAmt" class="input-text" value="<?php echo $obj->getFun131();?>" placeholder="0.00" readonly /></td>
                                                <td></td>
                                            </tr>
											
                                            <?php 
                                            $i = 0;
											$hirage = $obj->getFun132();
											while($rowsinvoice = mysql_fetch_assoc($resinvoice))
											{$i = $i + 1;$hirage = $hirage - $rowsinvoice['P_AMT'];
											if($rowsinvoice['INVOICE_TYPE']=="Final"){$valueinv = 1;}else{$valueinv = 0;}
											?>
                                            <tr>
											   <td></td>
                                               <td></td>
												<td style="color:red;" ><?php echo $rowsinvoice['INVOICE_NO']. "(".$rowsinvoice['INVOICE_TYPE'].")";?></td>
												
                                                <td style="color:red;" ><input type="text" style="width:90px;color:red;" class="input-text"  name="txtInvoiceAmt_<?php echo $i;?>" id="txtInvoiceAmt_<?php echo $i;?>" value="<?php echo $rowsinvoice['P_AMT'];?>" placeholder="0.00" readonly/><input type="hidden"  name="txtInvoiceVal_<?php echo $i;?>" id="txtInvoiceVal_<?php echo $i;?>" value="<?php echo $valueinv;?>" style="width:90px;"/><input type="hidden"  name="txtInvoiceID_<?php echo $i;?>" id="txtInvoiceID_<?php echo $i;?>" value="<?php echo $rowsinvoice['INVOICEID'];?>" /></td>
                                               
                                               <td></td>
											</tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                    <script>$("#selCVEVendor").html($("#selVendor").html());$("#selCVEVendor").val('<?php echo $obj->getFun144();?>');$("#selDTCVendor").val('<?php echo $obj->getFun140();?>');</script>
									<div ><strong>Results</strong></div>
									<table class='tablesorter'>
										
										<tbody>
											<tr>
												<td>Revenue (Final Nett Freight)</td>
												<td><input type="text"  name="txtRevenue" style="width:90px;" id="txtRevenue" class="input-text" readonly placeholder="0.00" /></td>
												<td><span id="vendorspanfr">Vendor</span></td>
												<td><select  name="txtFGFFVendor" class="input-text" style="width:120px;" id="txtFGFFVendor" ></select></td>
                                                <script>$("#txtFGFFVendor").html($("#selVendor").html());$("#txtFGFFVendor").val('<?php echo $obj->getFun141();?>');</script>
											</tr>
                                            <tr>
												
												<td>Operational Expenses</td>
												<td><input type="text"  name="txtTotalOperationalCost" style="width:90px;" id="txtTotalOperationalCost" class="input-text" readonly value="<?php echo $obj->getFun84();?>" autocomplete="off"/></td>
                                                <td>Port Expenses</td>
                                                <td><input type="text"  name="txtTotalPortCost" style="width:90px;" id="txtTotalPortCost" class="input-text" readonly value=""/></td>
											</tr>
                                            <tr>
                                                
                                                <td>Bunker Expenses</td>
												<td><input type="text"  name="txtTotalBunkerCost" style="width:90px;" id="txtTotalBunkerCost" class="input-text" readonly value=""/></td>
                                                <td>Voyage Earnings</td>
												<td><input type="text"  name="txtVoyageEarnings" style="width:90px;" id="txtVoyageEarnings" class="input-text" readonly placeholder="0.00" /></td>
                                            </tr>
											
											<tr>
                                                <td>G Total Voyage Earnings&nbsp;&nbsp;&nbsp;<span style="font-size:10px; font-style:italic;">(Voyage Earnings + Demurrage)</span></td>
												<td><input type="text"  name="txtGTTLVoyageEarnings" style="width:90px;" id="txtGTTLVoyageEarnings" class="input-text" readonly  placeholder="0.00" /></td>
												<td style="color:#dc631e;" >Daily Earnings / TCE</td>
												<td><input type="text"  name="txtDailyEarnings" style="width:90px;" id="txtDailyEarnings" class="input-text" readonly placeholder="0.00" /></td>
											</tr>
											
											<tr>
												<td style="color:#dc631e;">Nett Daily Profit</td>
												<td><input type="text"  name="txtNettDailyProfit" style="width:90px;" id="txtNettDailyProfit" class="input-text" readonly placeholder="0.00" /></td>
												<td>P/L</td>
												<td><input type="text"  name="txtPL" id="txtPL" style="width:90px;" class="input-text" readonly placeholder="0.00" /></td>
											</tr>
											<tr>
												<td style="color:#dc631e;"></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</tbody>
									</table>
                           </td>
                       </tr>
                       </table>
                    
                    
                    </div>
                    <!-- /.tab_1-pane -->
                    <div id="tabs_3" class="tab-pane">
                         <table width="100%" style="margin-top:2px;">
                           <tr>
                               <td class="input-text">&nbsp;DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>&nbsp;:&nbsp;<input type="text" name="txtDWTS" id="txtDWTS" style="width:90px;" class="input-text" autocomplete="off" readonly value="<?php echo $obj->getFun10();?>" placeholder="0.00" />
                               </td>
                               <td class="input-text"></td>
                                <td class="input-text" id="tankgasnot1"><input name="rdoCap" id="rdoCap1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoCap == 1) echo "checked"; ?> onClick="showCapField(1);"  />&nbsp;Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span><input type="text" name="txtGCap" id="txtGCap" style="width:90px;" class="input-text" autocomplete="off" readonly value="<?php echo $obj->getFun13();?>" /></td>
                                <td class="input-text" id="tankgasnot2"><input name="rdoCap" id="rdoCap2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoCap == 2) echo "checked"; ?> onClick="showCapField(2);" />&nbsp;Bale&nbsp;Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span><input type="text" name="txtBCap" id="txtBCap" style="width:90px;" class="input-text" autocomplete="off" readonly value="<?php echo $obj->getFun14();?>" disabled="disabled" /></td>
                                <td class="input-text" id="tankgasnot3">SF<span style="font-size:10px; font-style:italic;">(ft3/lt)</span><input type="text" name="txtSF" id="txtSF"  autocomplete="off" style="width:90px;" class="input-text" value="<?php echo $obj->getFun15();?>"  onkeyup="getTotalDWT(),getTotalDWT1()" /></td>
                                <td class="input-text" id="tankgasnot4">Loadable<span style="font-size:10px; font-style:italic;">(MT)</span><input type="text" name="txtLoadable" id="txtLoadable" autocomplete="off" style="width:90px;" class="input-text" readonly value="<?php echo $obj->getFun16();?>" /></td>
                           </tr>
                       </table>
                         <table cellpadding="1" cellspacing="1" border="0" width="100%"  class='tablesorter' >
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
                        <tr>
						<td width="25%" align="left" class="text">GRT<br/><input type="text" name="txtGNRT" id="txtGNRT" class="input-text" readonly value="<?php echo $obj->getFun17();?>" /></td>
						<td width="25%" align="left" valign="top" class="text">LOA<br/><input type="text" name="txtLOA" id="txtLOA" class="input-text" readonly value="<?php echo $obj->getFun18();?>" /></td>
						<td width="25%" align="left" valign="top" class="text">Built Year<br/><input type="text" name="txtBuiltYear" id="txtBuiltYear" class="input-text" readonly value="<?php echo $obj->getFun20();?>" /></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						</tr>
                        <tr>
						<td width="25%" align="left" class="text">BEAM(m)<br/><input type="text" name="txtBeam" id="txtBeam" class="input-text" readonly value="<?php echo $obj->getFun21();?>" /></td>
						<td width="25%" align="left" valign="top" class="text">TPC<br/><input type="text" name="txtTPC" id="txtTPC" class="input-text" readonly value="<?php echo $obj->getFun22();?>" /></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						</tr>
						<tr>
						<td width="25%" align="left" class="text" style="color:#dc631e;">Speed Data</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="25%" align="left" class="input-text">Ballast Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBFullSpeed" id="txtBFullSpeed" class="input-text" value="<?php echo $obj->getFun23();?>" /></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBEcoSpeed1" id="txtBEcoSpeed1" class="input-text" value="<?php echo $obj->getFun24();?>" /></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBEcoSpeed2" id="txtBEcoSpeed2" class="input-text" value="<?php echo $obj->getFun25();?>" /></td>
						</tr>
						<tr>
						<td width="25%" align="left" class="input-text">Laden Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLFullSpeed" id="txtLFullSpeed" class="input-text" value="<?php echo $obj->getFun26();?>" /></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLEcoSpeed1" id="txtLEcoSpeed1" class="input-text" value="<?php echo $obj->getFun27();?>" /></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLEcoSpeed2" id="txtLEcoSpeed2" class="input-text" value="<?php echo $obj->getFun28();?>" /></td>
						</tr>
						</tbody>
						</table>
						</td></tr>
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">FO Consumption MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBFOFullSpeed" id="txtBFOFullSpeed" class="input-text" value="<?php echo $obj->getFun29();?>" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBFOEcoSpeed1" id="txtBFOEcoSpeed1" class="input-text" value="<?php echo $obj->getFun30();?>" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtBFOEcoSpeed2" id="txtBFOEcoSpeed2" class="input-text" value="<?php echo $obj->getFun31();?>" /></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLFOFullSpeed" id="txtLFOFullSpeed" class="input-text" value="<?php echo $obj->getFun32();?>" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLFOEcoSpeed1" id="txtLFOEcoSpeed1" class="input-text" value="<?php echo $obj->getFun33();?>" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtLFOEcoSpeed2" id="txtLFOEcoSpeed2" class="input-text" value="<?php echo $obj->getFun34();?>" /></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtPIFOFullSpeed" id="txtPIFOFullSpeed" class="input-text" value="<?php echo $obj->getFun35();?>" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtPWFOFullSpeed" id="txtPWFOFullSpeed" class="input-text" value="<?php echo $obj->getFun36();?>" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						
						</tbody>
						</table>
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">DO Consumption per MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBDOFullSpeed" id="txtBDOFullSpeed" class="input-text"  value="<?php echo $obj->getFun37();?>" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBDOEcoSpeed1" id="txtBDOEcoSpeed1" class="input-text" value="<?php echo $obj->getFun38();?>" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtBDOEcoSpeed2" id="txtBDOEcoSpeed2" class="input-text" value="<?php echo $obj->getFun39();?>" /></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLDOFullSpeed" id="txtLDOFullSpeed" class="input-text" value="<?php echo $obj->getFun40();?>" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLDOEcoSpeed1" id="txtLDOEcoSpeed1" class="input-text" value="<?php echo $obj->getFun41();?>" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtLDOEcoSpeed2" id="txtLDOEcoSpeed2" class="input-text" value="<?php echo $obj->getFun42();?>" /></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtPIDOFullSpeed" id="txtPIDOFullSpeed" class="input-text" value="<?php echo $obj->getFun43();?>" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtPWDOFullSpeed" id="txtPWDOFullSpeed" class="input-text" value="<?php echo $obj->getFun44();?>" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						</tbody>
						</table>
						
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						</table>
                    </div>
                    <!-- /.tab_2-pane -->
                    <div id="tabs_4" class="tab-pane" style="overflow:auto;">
                        <table width="100%">
                        <tr class="input-text">
                            <td>Port Open : </td>
                            <td><select name="selOpenPort" class="input-text" id="selOpenPort" style="width:137px;"></select><script>$("#selOpenPort").html($("#selPort").html());</script></td>
                            <td>Zone Open : </td>
                            <td><select name="selZone" class="input-text" style="width:137px;" id="selZone"><?php $obj->getZoneList();?></select></td>
                            <td>Broker : </td>
                            <td><select  name="selBroker" class="input-text" style="width:150px;" id="selBroker"><?php $obj->getVendorListNewForCOA(12);?></select></td>
                            <td>COA / Spot : </td>
                            <td><select  name="selCOASpot" class="input-text" style="width:137px;" id="selCOASpot" onChange="getShow();"><?php $obj->getCOASpotList();?></select></td>
                        </tr>
                        <script>
						    $(document).ready(function(){
							    if($("#selCOASpot").val()==2){$("#tr_coa").val();}
							});
                        </script>
                        <tr class="input-text" id="tr_coa" style="display:none;">
                        	<td>COA Number : </td>
                            <td><select  name="selCOA" class="input-text" style="width:137px;" id="selCOA"><?php $obj->getCOAList();?></select></td>
                            <td>Number of Lift : </td>
                            <td><input type="text" name="txtNoLift" id="txtNoLift" class="input-text" size="21" placeholder=" Number of Lift" autocomplete="off" value="<?php echo $obj->getFun103();?>"/></td>
                            <td>Total No. of Shipments : </td>
                            <td><strong><span id="ttl_shipment" ></span></strong></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="input-text">
                            <?php if($obj->getFun112()=="" || $obj->getFun112()=="0000-00-00" || $obj->getFun112()=="1970-01-01"){$txtCPDate = "";}else{$txtCPDate = date('d-m-Y',strtotime($obj->getFun112()));}?>
                            <td>CP Date : </td>
                            <td><input type="text" name="txtCPDate" id="txtCPDate" class="input-text" size="21"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtCPDate;?>"/></td>
                            <td>Remarks :</td>
                            <td><textarea class="input-text areasize" name="txtNotes" id="txtNotes" rows="3" cols="21" placeholder="Remarks ..." ><?php echo $obj->getFun113();?></textarea></td>
                            <td>Owner : </td>
                            <td><select  name="selOwner" class="input-text" style="width:150px;" id="selOwner"><?php $obj->getVendorListNewForCOA(11);?></select></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="input-text">
                        <?php if($obj->getFun109()=="" || $obj->getFun109()=="0000-00-00" || $obj->getFun109()=="1970-01-01"){$txtFDate = "";}else{$txtFDate = date('d-m-Y',strtotime($obj->getFun109()));}?>
                            <td>Laycan Start : </td>
                            <td><input type="text" name="txtFDate" id="txtFDate" class="input-text" size="21"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtFDate;?>"/></td>
                            <?php if($obj->getFun110()=="" || $obj->getFun110()=="0000-00-00" || $obj->getFun110()=="1970-01-01"){$txtTDate = "";}else{$txtTDate = date('d-m-Y',strtotime($obj->getFun110()));}?>
                            <td>Laycan Finish : </td>
                            <td><input type="text" name="txtTDate" id="txtTDate" class="input-text" size="21"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtTDate;?>"/></td>
                            <?php if($obj->getFun111()=="" || $obj->getFun111()=="0000-00-00" || $obj->getFun111()=="1970-01-01"){$txtETADate = "";}else{$txtETADate = date('d-m-Y',strtotime($obj->getFun111()));}?>
                            <td>ETA During Fixture : </td>
                            <td><input type="text" name="txtETADate" id="txtETADate" class="input-text" size="21" placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtETADate;?>"/></td>
                            <td></td>
                            <td></td>
                        </tr>
                        
                        </table>
                        <table width="100%" class="tablesorter">
                        <tbody id="sort">
                        <tr>
                            <td width="11%">Disponent Owner 1 : </td>
                            <td width="89%"><select name="txtDisponentOwner" class="input-text" style="width:200px;" id="txtDisponentOwner"><?php $obj->getVendorListNewForCOA(11);?></select></td>
                        </tr>
                        <?php $sql5 = "select * from freight_cost_estimete_slave5 where FCAID='".$id."'";
					    $result5 = mysql_query($sql5);
					    $k =0;
					    while($rows5 = mysql_fetch_assoc($result5))
					    {$k = $k + 1;?>
                            <tr>
                            <td width="11%">Disponent Owner <?php echo $k+1;?> : </td>
                            <td width="89%"><select name="txtDisponentOwner_<?php echo $k;?>" class="input-text" style="width:200px;" id="txtDisponentOwner_<?php echo $k;?>"><?php $obj->getVendorListNewForCOA(11);?></select></td>
                            </tr>
					   <?php }?>
                        </tbody>
                        <tr>
                        <td colspan="2"><button type="button" onClick="addUI_Row();">Add</button><input type="hidden" class="input" name="txtTID" id="txtTID" value="<?php echo $k;?>" /></td>
                        </tr>
                        </table>
                        <table width="100%" class="tablesorter">
                            <tbody>
                            <tr>
                                <td colspan="2"><div class="btn btn-success btn-file btn-flat" data-toggle="tooltip">
                                                <i class="fa fa-paperclip"></i> Attachment
                                                <input type="file" class="form-control" multiple name="attach_file[]" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                            </div></td>
                            </tr>
                            <?php if($obj->getFun115() != '')
                            { 
                                $file = explode(",",$obj->getFun115()); 
                                $name = explode(",",$obj->getFun116()); ?>										
                            <tr>
                                <td colspan="2">
                              Previous Attachments
                                </td>
                             </tr>
               
                                <?php
                                $j =1;
                                for($i=0;$i<sizeof($file);$i++)
                                {
                                ?>
                                <tr id="row_file_<?php echo $j;?>">
                                    <td width="40%" align="left" class="input-text"  valign="top"><a href="../../attachment/<?php echo $file[$i]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="<?php echo $name[$i];?>"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $name[$i];?></a>
                                    <input type="hidden" name="file_<?php echo $j;?>" id="file_<?php echo $j;?>" value="<?php echo $file[$i]; ?>" />
                                    <input type="hidden" name="name_<?php echo $j;?>" id="name_<?php echo $j;?>" value="<?php echo $name[$i]; ?>" />
                                    </td>
                                    <td ><a href="#1" onClick="Del_Upload(<?php echo $j;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
                                </tr>
                                <?php $j++;}?>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <div id="tabs_5" class="tab-pane">
                        <table width="100%">
                        <tr class="input-text">
                            <td>CP ID : </td>
                            <td><input type="text" name="txtCID" id="txtCID" class="input-text" size="21" placeholder="Cargo ID" readonly autocomplete="off" value=""/></td>
                            <td>Shipper : </td>
                            <td><select  name="selShipperCP" class="input-text" style="width:137px;" id="selShipperCP" ></select></td>
                            <td>Charterer : </td>
                            <td><select  name="selChartererCP" class="input-text" style="width:137px;" id="selChartererCP" ></select></td>
                            <td>Owner :</td>
                            <td><select  name="selOwnerCP" class="input-text" style="width:137px;" id="selOwnerCP" ></select></td>
                        </tr>
                        <tr class="input-text">
                        	<td>Receiver : </td>
                            <td><select  name="selReceiverCP" class="input-text" style="width:137px;" id="selReceiverCP" ></select></td>
                            <td>Cargo :</td>
                            <td><select  name="selCargoCP" class="input-text" style="width:137px;" id="selCargoCP"><?php $obj->getCargoNameListForMultiple($_SESSION['selBType']);?></select></td>
                            <td>Cargo Stem Size(MT) :</td>
                            <td><input type="text" name="txtQtyCP" id="txtQtyCP" class="input-text" readonly size="21"  placeholder="" autocomplete="off" value=""/></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="input-text">
                            <td>Tolerance (+/- %) : </td>
                            <td><input type="text" name="txtToleranceCP" id="txtToleranceCP" readonly class="input-text" size="21"  placeholder="" autocomplete="off" value=""/></td>
                            <td>Base Freight (USD/MT) : </td>
                            <td><input type="text" name="txtBaseFreightCP" id="txtBaseFreightCP" readonly class="input-text" size="21"  placeholder="" autocomplete="off" value=""/></td>
                            <td>COA/Spot :</td>
                            <td> <select  name="selPlannintTypeCP" class="input-text" style="width:137px;" id="selPlannintTypeCP"><?php $obj->getCOASpotList();?></select></td>
                            <td>COA date : </td>
                            <td><input type="text" name="txtCOADateCP" id="txtCOADateCP" class="input-text" readonly size="21" placeholder="dd-mm-yyyy" autocomplete="off" value=""/></td>
                        </tr>
                        <tr class="input-text">
                        	<td>Basin : </td>
                            <td><select  name="selBaseInCP" class="input-text" style="width:137px;" id="selBaseInCP" ><?php $obj->getBaseinList();?></select></td>
                            <td>Bunker Hedge :</td>
                            <td><select  name="selBunkerHedgeCP" class="input-text" style="width:137px;" id="selBunkerHedgeCP"><?php $obj->getLoader();?></select></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="input-text">
                            <td>Load Port : </td>
                            <td><select  name="selLPortCP" class="input-text" style="width:137px;" id="selLPortCP"></select></td>
                            <td>Discharge Port : </td>
                            <td><select  name="selDPortCP" class="input-text" style="width:137px;" id="selDPortCP"></select></td>
                            <td>LayCan Start Date :</td>
                            <td><input type="text" name="txtLCSDateCP" id="txtLCSDateCP" readonly class="input-text" size="21" autocomplete="off" value=""/></td>
                            <td>LayCan Finish Date : </td>
                            <td><input type="text" name="txtLCFDateCP" id="txtLCFDateCP" readonly class="input-text" size="21" autocomplete="off" value=""/></td>
                             <script>$("#selLPortCP,#selDPortCP").html($("#selPort").html());</script>
                        </tr>
                        <tr class="input-text">
                            <td>Cargo Relet/Voyage : </td>
                            <td><select  name="selCargoReletCP" class="input-text" style="width:137px;" id="selCargoReletCP"><?php $obj->getBusinessTypeForCargoPlanning('');?></select></td>
                            <td>Nom Clause : </td>
                            <td><input type="text" name="txtNomClauseCP" id="txtNomClauseCP" readonly class="input-text" size="21" autocomplete="off" value=""/></td>
                            <td>Remarks :</td>
                            <td><textarea class="input-text" name="txtRemarksCP" id="txtRemarksCP" rows="2" cols="21" readonly placeholder="Remarks" ></textarea></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </table>
                        <table class='tablesorter' width="100%">
							<tr><td><strong style="font-size:12px;">Cargo Intake Calculations</strong></td></tr>    
					    </table>
                        <table width="100%">
                            <tr class="input-text">
                               <td>Summer DWT(MT):</td>
                               <td><input type="text" name="txtSDWTMT" id="txtSDWTMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun207();?>"/></td>
                               <td>Summer DWT(LT):</td>
                               <td><input type="text" name="txtSDWTLT" id="txtSDWTLT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun208();?>"/></td>
                               <td>Summer Draft(M):</td>
                               <td><input type="text" name="txtSDraftM" id="txtSDraftM" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun209();?>"/></td>
                               <td>Summer Draft(FT):</td>
                               <td><input type="text" name="txtSDraftFT" id="txtSDraftFT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun210();?>"/></td>
                            </tr>
                            <tr class="input-text">
                               <td>TPI(MT/Inch):</td>
                               <td><input type="text" name="txtTPIMT" id="txtTPIMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun211();?>"/></td>
                               <td>TPI(LT/Inch):</td>
                               <td><input type="text" name="txtTPILT" id="txtTPILT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun212();?>"/></td>
                               <td>TPC(MT):</td>
                               <td><input type="text" name="txtTPCMT" id="txtTPCMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun213();?>"/></td>
                               <td>TPC(LT):</td>
                               <td><input type="text" name="txtTPCLT" id="txtTPCLT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun214();?>"/></td>
                            </tr>
                            <tr class="input-text">
                               <td>Constants(MT):</td>
                               <td><input type="text" name="txtConstantsMT" id="txtConstantsMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun215();?>"/></td>
                               <td>Constants(LT):</td>
                               <td><input type="text" name="txtConstantsLT" id="txtConstantsLT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun216();?>"/></td>
                               <td>Grain Cap(CBM):</td>
                               <td><input type="text" name="txtGrainCapCBM" id="txtGrainCapCBM" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun217();?>"/></td>
                               <td>Grain Cap(CFT):</td>
                               <td><input type="text" name="txtGrainCapCFT" id="txtGrainCapCFT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun218();?>"/></td>
                            </tr>
                        
							<tr><td colspan="8"><strong style="font-size:12px;">Basis Max Draft in Port</strong></td></tr>    
					    
                            <tr class="input-text">
                               <td>Allowed Draft(M):</td>
                               <td><input type="text" name="txtAllawedDraftM" id="txtAllawedDraftM" class="input-text numeric" size="21" autocomplete="off" value="<?php echo $obj->getFun219();?>" placeholder="0.00" onKeyUp="getIntakeCalculation();"/></td>
                               <td>Bunker ROB(MT):</td>
                               <td><input type="text" name="txtBunkerRobMT" id="txtBunkerRobMT" class="input-text numeric" size="21" autocomplete="off" value="<?php echo $obj->getFun220();?>" placeholder="0.00" onKeyUp="getIntakeCalculation();"/></td>
                               <td>Cargo Intake(MT) :</td>
                               <td><input type="text" name="txtCargoIntakeMT" id="txtCargoIntakeMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun221();?>"/></td>
                               <td></td>
                               <td></td>
                            </tr>
                        
							<tr><td colspan="8"><strong style="font-size:12px;">Basis Stowage Factor</strong></td></tr>    
					    
                            <tr class="input-text">
                               <td>SF(CBM/MT):</td>
                               <td><input type="text" name="txtSFCBM_MT" id="txtSFCBM_MT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun222();?>"/></td>
                               <td>SF(CBFT/MT):</td>
                               <td><input type="text" name="txtSFCBFT_MT" id="txtSFCBFT_MT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun223();?>"/></td>
                               <td>Cargo Loadable (MT) :</td>
                               <td><input type="text" name="txtCargoLoadableMT" id="txtCargoLoadableMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun224();?>"/></td>
                               <td></td>
                               <td></td>
                            </tr>
                        </table>
                    </div>
                    </div><!-- /.tab-content -->
                </div>
                
                <div style="text-align:center;">
                    <input type="hidden" name="txtCRMFILE" id="txtCRMFILE" value="" />
					<input type="hidden" name="txtCRMNAME" id="txtCRMNAME" value="" />
                    <input type="hidden" name="txtCRMFILE1" id="txtCRMFILE1" value="" />
					<input type="hidden" name="txtCRMNAME1" id="txtCRMNAME1" value="" />
                    
                    <input type="hidden" name="action" id="action" value="submit" /><input type="hidden" name="vesselrec" id="vesselrec" class="form-control" value="" /><input type="hidden" name="update_status" id="update_status" class="form-control" value="" />
                </div>
                
                
                <!-- Main content -->
                </form>
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/sort.css" rel="stylesheet" type="text/css" />
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />
<script src="../../js/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" href="../../css/chosen.css" rel="stylesheet" />
<script type="text/javascript">
$(document).ready(function(){ 
$("#selShipperCP,#selChartererCP,#selOwnerCP,#selReceiverCP").html($("#selVendor").html());
$('#txtDistributedRemarks').autosize({append: "\n"});
$("#selMType").chosen();
$("#selCName").chosen();
$("#frm1").validate({
	rules: {
		txtENo:"required",
		selVName:"required"
		},
	messages: {
		selVName:"*",
		
		},
submitHandler: function(form)  {
		jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
			if(r){
				var var3 = $("[id^=file_]").map(function () {return this.value;}).get().join(",");
				$('#txtCRMFILE').val(var3);
				var var2 = $("[id^=name_]").map(function () {return this.value;}).get().join(",");
				$('#txtCRMNAME').val(var2);
				var var3 = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
				$('#txtCRMFILE1').val(var3);
				var var2 = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
				$('#txtCRMNAME1').val(var2);
				jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
				$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
				$("#popup_content").css({"background":"none","text-align":"center"});
				$("#popup_ok,#popup_title").hide();  
				frm1.submit();
			    }
				else{return false;}
			});
	}
});

$("#selVName").val(<?php echo $obj->getFun5();?>);
$("#selCType").val(<?php echo $obj->getFun51();?>);
$("[id^=txtQtyAggriedFreight_],[id^=txtFreightQty_],#txtMTCPDRate,#txtAddnlCRate,#txtCQMT,#txtDFQMT,[id^=txtMCWS_],[id^=txtMCDistanceLeg_], [id^=txtMCTotalDistance_],[id^=txtOvrWS_],[id^=txtOvrDistanceLeg_],[id^=txtddswDPCost_],[id^=txtddswLPCost_],[id^=txtOvrTotalDistance_], [id^=txtAdditionAmt_],[id^=txtDeductionAmt_],#txtFrAdjUsdGF,#txtFrAdjPerACTF,#txtFrAdjPerACGF,#txtDWTS,#txtSF,#txtTFUMTManual, #txtTDUMTManual,#txtQtyAggriedFreight, #txtFreightQty,#txtQtyLocalAggriedFreight,#txtDisExchangeRate,#txtMarLocalAggriedFreight, #txtMarExchangeRate,[id^=txtORCAmt_],[id^=txtDistance_],[id^=txtWeather_],[id^=txtMargin_],[id^=txtPCosts_],[id^=txtQMT_],[id^=txtRate_],[id^=txtIDays_], [id^=txtDWDays_],[id^=txtWDays_],[id^=txtTLPCosts_],[id^=txtTLIDays_],#txtFrAdjPerAC,#txtFrAdjPerAgC,[id^=txtBrCommPercent_],[id^=txtORCAmt_],#txtDailyVesselOperatingExpenses,[id^=txtEstDDCLPCost_],[id^=txtEstDDCDPCost_], #txtHireargePercent,#txtBallastBonus,#txtCVE,#txtQtyAggriedFreight, #txtFreightQty,#txtQtyLocalAggriedFreight,#txtDisExchangeRate,#txtMarLocalAggriedFreight, #txtMarExchangeRate,[id^=txtQtyLocalAggriedFreight_],[id^=txtDisExchangeRate_],[id^=txtMarLocalAggriedFreight_],[id^=txtMarExchangeRate_],#txtBFullSpeed,#txtBEcoSpeed1,#txtBEcoSpeed2,#txtBFOFullSpeed,#txtBFOEcoSpeed1,#txtBFOEcoSpeed2,#txtBDOFullSpeed,#txtBDOEcoSpeed1,#txtBDOEcoSpeed2,#txtLFullSpeed,#txtLEcoSpeed1,#txtLEcoSpeed2,#txtLFOFullSpeed,#txtLFOEcoSpeed1,#txtLFOEcoSpeed2,#txtLDOFullSpeed,#txtLDOEcoSpeed1,#txtLDOEcoSpeed2,#txtPIFOFullSpeed,#txtPWFOFullSpeed,#txtPIDOFullSpeed,#txtPWDOFullSpeed,#txtAddnlCRatePer,#txtBalticRate,#txtAdnlPrenium,#txtGasCQMT,#txtGASLumpsum,#txtFixedDiff,[id^=txtLPADDComm_],[id^=txtDPADDComm_],[id^=txtOtherAmt_],[id^=txtOtherAddComm_]").numeric();

$("[id^=txtMinCargoQty_],[id^=txtMinFlatRateQty_],[id^=txtMinWSQty_],[id^=txtMinDisLeg_],[id^=txtMinTotalDis_],[id^=txtMinAmount_],[id^=txtMinCargoQty_],[id^=txtOveFlatRateQty_],[id^=txtOveWSQty_],[id^=txtOveTotalDis_],[id^=txtOveAmount_],[id^=txtTotalTankQty_],[id^=txtOveDisLeg_],#txtLumpsumQty,#txtLumpsum").numeric();


$("#txtDate,#txtCPDate,#txtETADate").datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
});

$('[id^=ChkShowDDCDP_]').on('ifChecked', function () { 
   $("#DDCDProw_"+$(this).val()).hide();
});
 
$('[id^=ChkShowDDCDP_]').on('ifUnchecked', function () { 
   $("#DDCDProw_"+$(this).val()).show();
});
 
$('[id^=ChkShowDDCLP_]').on('ifChecked', function () { 
    $("#DDCLProw_"+$(this).val()).hide();
});
 
$('[id^=ChkShowDDCLP_]').on('ifUnchecked', function () { 
    $("#DDCLProw_"+$(this).val()).show();
});

$('#ChkLumpsum').on('ifChecked', function () { 
    $("#lumpsumspan1,#lumpsumspan2,#lumpsumspan3,#lumpsumspan4,#lumpsumspan5").show();
	$("#divTanker2,#spanwsports1,#spanwsports2,#spanwsports3,#spanwsports4,#spanwsports5").hide();
	getFinalCalculation();
});

$('#ChkLumpsum').on('ifUnchecked', function () { 
    $("#lumpsumspan1,#lumpsumspan2,#lumpsumspan3,#lumpsumspan4,#lumpsumspan5").hide();
	$("#divTanker2,#spanwsports1,#spanwsports2,#spanwsports3,#spanwsports4,#spanwsports5").show();
	getFinalCalculation();
});

$('#txtLCSDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#txtLCFDate').datepicker('setStartDate', new Date(getString($(this).val())));
 });
 
$('#txtLCFDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){   });
 
$('#txtFDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#txtTDate').datepicker('setStartDate', new Date(getString($(this).val())));
 });
 
$('#txtTDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){   });


function getString(var1)
{
  var var2 = var1.split('-');  
  return var2[2]+'/'+var2[1]+'/'+var2[0];
}

$("#selZone").val(<?php echo $obj->getFun101();?>);
$("#selCOASpot").val(<?php echo $obj->getFun126();?>);

if($("#selCOASpot").val()==2)
{
	$("#tr_coa,#tr_coa1").show();
}
$("#selShipper").val('<?php echo $obj->getFun117();?>');
$("#selBroker").val("<?php echo $obj->getFun127();?>");
$("#selOwner").val("<?php echo $obj->getFun107();?>");
$("#selOpenPort").val(<?php echo $obj->getFun100();?>);
$("#selLPort").val(<?php echo $obj->getFun122();?>);
$("#selDPort").val(<?php echo $obj->getFun123();?>);
$("#selCOA").val(<?php echo $obj->getFun102();?>);
$("#selVCType").val(<?php echo $obj->getFun105();?>);
$("#txtDisponentOwner").val("<?php echo $obj->getFun108();?>");
$("#selCargoPlanning").val('<?php echo $obj->getFun206();?>');
<?php $sql5 = "select * from freight_cost_estimete_slave5 where FCAID='".$id."'";
$result5 = mysql_query($sql5);
$k =0;
while($rows5 = mysql_fetch_assoc($result5))
{$k = $k + 1;?>
$("#txtDisponentOwner_<?php echo $k;?>").val('<?php echo $rows5['DISPONENT_OWNER'];?>');
<?php }?>

<?php
$sql = "SELECT * FROM bunker_grade_master where STATUS=1";
$res = mysql_query($sql);
$rec = mysql_num_rows($res);
$m=0; 
while($rows = mysql_fetch_assoc($res)){
?>
$("#txt<?php echo $rows['NAME'];?>_1_2").numeric();
<?php } ?>

$("#txtNoLift").val(<?php echo $obj->getFun103();?>);
showQtyField(<?php echo $rdoQty;?>);
showCapField(<?php echo $rdoCap;?>);
getQtyLocalFreightCal();
getBunkerCalculation();
$(".navbar-btn").on('click',function(){getDivwidthAsParent(2)});
getDivwidthAsParent(1);
showHideEstimateTypeDiv(1);
showGasMarket(<?=$rdoQtyMar;?>);
getCargoPlanningdata();
});

function showHideEstimateTypeDiv(val)
{
	if($("#rdoEstimateType").val()==1)
	{
		$("#distypeDiv3,#distypeDiv2,#tankgasnot1,#tankgasnot2,#tankgasnot3,#tankgasnot4").hide();
		$("#distypeDiv1,#vendorspanfr,#txtFGFFVendor").show();
		$("#loadtablelebel").html('(CBM/Hr)');
	    $("#dischargetablelebel").html('(CBM/Hr)');
		$("#loadqtylabel,#dischargeqtylabel").html('(MT)');
		if(val==2)
		{
			$('#selCName option:selected').removeAttr("selected");
			$('#selMType option:selected').removeAttr("selected");
			$('#selCName,#selMType').html($('#selMaterial1').html());
			$('#selCName,#selMType').trigger("liszt:updated");
			$("#selCName,#selMType").chosen();
		}
	}
	if($("#rdoEstimateType").val()==2)
	{
		$("#distypeDiv3,#distypeDiv1,#vendorspanfr,#txtFGFFVendor,#tankgasnot1,#tankgasnot2,#tankgasnot3,#tankgasnot4").hide();
		$("#distypeDiv2").show();
		$("#loadtablelebel").html('(MT/Hr)');
	    $("#dischargetablelebel").html('(MT/Hr)');
		$("#loadqtylabel,#dischargeqtylabel").html('(MT)');
		if(val==2)
		{
			$('#selCName option:selected').removeAttr("selected");
			$('#selMType option:selected').removeAttr("selected");
			$('#selCName,#selMType').html($('#selMaterial2').html());
			$('#selCName,#selMType').trigger("liszt:updated");
			$("#selCName,#selMType").chosen();
		}
	}
	
	if($("#rdoEstimateType").val()==3)
	{
		$("#distypeDiv2,#distypeDiv1").hide();
		$("#distypeDiv3,#tankgasnot1,#tankgasnot2,#tankgasnot3,#tankgasnot4,#vendorspanfr,#txtFGFFVendor").show();
		$("#loadtablelebel").html('(MT/Day)');
	    $("#dischargetablelebel").html('(MT/Day)');
		$("#loadqtylabel,#dischargeqtylabel").html('(MT)');
		if(val==2)
		{
			$('#selCName option:selected').removeAttr("selected");
			$('#selMType option:selected').removeAttr("selected");
			$('#selCName,#selMType').html($('#selMaterial3').html());
			$('#selCName,#selMType').trigger("liszt:updated");
			$("#selCName,#selMType").chosen();
		}
	}
}

function getValue()
{
	
}

function getDivwidthAsParent(val)
{
	if(val==1)
	{
	    //txtBunkerWidth
	    var widths = parseInt($('#parenttdID').width());
	    $("#bunkercontainer").css( "maxWidth", widths+"px" );
		$("#txtBunkerWidth").val(widths);
	}
	else
	{
		var widthidentity = $("#txtBunkerWidth1").val();
		if(widthidentity == 1)
		{
			var widths = $('#parenttdID').width();
			$("#bunkercontainer").css( "maxWidth", widths+"px" );
			$("#txtBunkerWidth1").val(2);
		}
		else
		{
			var widths = $('#txtBunkerWidth').val();
			$("#bunkercontainer").css( "maxWidth", widths+"px" );
			$("#txtBunkerWidth1").val(1);
		}
	}
}

function getMaterialCode()
{
	var mcode = "";
	$("#selMType option:selected").each(function () {
		var $this1 = $(this).text();
		if(mcode!=""){mcode = mcode+", ";}
		mcode = mcode+""+$this1.split("(")[1].replace(')','');
	});
    $("#txtMCode").val(mcode);
}

function getShow()
{
	if($("#selCOASpot").val() == 1 || $("#selCOASpot").val() == 3 || $("#selCOASpot").val() == 4)
	{
		$("#tr_coa,#tr_coa1").hide();
		
		$("#ttl_shipment").html("");
	}
	else if($("#selCOASpot").val() == 2)
	{
		$("#ttl_shipment").html("");
		$("#tr_coa,#tr_coa1").show();
	}
}


function addUI_Row()
{
	var id = $("#txtTID").val();
	
	if(id == 0)
	{
		if($("#txtDisponentOwner").val() != "")
		{
			id  = (id - 1 )+ 2;
			num = parseFloat(id +1);
			$('<tr><td width="11%">Disponent Owner '+num+' : </td><td width="89%"><select name="txtDisponentOwner_'+id+'" class="input-text" style="width:200px;" id="txtDisponentOwner_'+id+'"><?php $obj->getVendorListNewForCOA(11);?></select></td></tr>').appendTo("#sort");
			$("#txtTID").val(id);
		}
		else
		{
			jAlert('Please fill the above entries.', 'Alert');
		}
	}
	else
	{
		if($("#txtDisponentOwner_"+id).val() != "")
		{
			id  = (id - 1 )+ 2;
			num = parseFloat(id +1);
			$('<tr><td width="11%">Disponent Owner '+num+' : </td><td width="89%"><select name="txtDisponentOwner_'+id+'" class="input-text" style="width:200px;" id="txtDisponentOwner_'+num+'"><?php $obj->getVendorListNewForCOA(11);?></select></td></tr>').appendTo("#sort");
			$("#txtTID").val(id);
		}
		else
		{
			jAlert('Please fill the above entries.', 'Alert');
		}
	}	
}



function showMMarketField(value)
{
	
	if(value == 1)
	{
		$("#txtMTCPDRate").removeAttr('disabled');
		$("#txtMLumpsum").attr('disabled',true);
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtMTCPDRate").focus();
		getFinalCalculation();
	}
	if(value == 2)
	{
			
		$("#txtMTCPDRate").attr('disabled',true);
		$("#txtMLumpsum").removeAttr('disabled');
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtMLumpsum").focus();
		getFinalCalculation();
	}
}

function showQtyField(val)
{
	if(val == 1)
	{
		$("#txtDFQMT").removeAttr('disabled');
		$("#txtAddnlQMT").attr('disabled',true);
		$("#txtAddnlQMT").val("");
		$("#txtDFQMT").focus();
	}
	if(val == 2)
	{
		$("#txtDFQMT").attr('disabled',true);
		$("#txtAddnlQMT").removeAttr('disabled');
		$("#txtDFQMT").val("");
		$("#txtAddnlQMT").focus();
	}
	getFinalCalculation();
}


function getBunkerCalculation()
{
	    var noOfBunker = $("#txtBunkerRec").val();
		var sum = "";
		for(var i=1;i<=noOfBunker;i++)
		{
		    var bunkergrade = $("#txtBunkerGradeName_"+i).val();
			var bunkergradetype = $("#txtBunkerGradeType_"+i).val();
		    var str = bunkergradetype.substr(0,3);
		    //var str = bunkergrade.substr(0,3);
		    if(str == 'IFO')
			{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTFUMT").val());
			}
			else if(str == 'MDO')
			{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMT").val());
			}
			else if(str == 'MGO')
			{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMT").val());
			}
			else
			{
				$("#txt"+bunkergrade+"_1_1").val('0.00');
			}
			
			var var3 = $("#txt"+bunkergrade+"_1_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_1_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_1_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_1_3").val(calc.toFixed(2));
			$("#txtBunkerCost_"+bunkergrade).val($("#txt"+bunkergrade+"_1_3").val());
		}
		$("#txtTotalBunkerCost").val($("[id^=txtBunkerCost_]").sum().toFixed(2));
		
		var txtBunkerCount = $("#txtBunkerCount").val();
		for(var i=1;i<=txtBunkerCount;i++)
		{
			var txtBunkerQty = $("#txtBunkerQty_"+i).val();if(isNaN(txtBunkerQty)){txtBunkerQty =0; }
			var txtBunkerPrice = $("#txtBunkerPrice_"+i).val();if(isNaN(txtBunkerPrice)){txtBunkerPrice =0; }
			var calc = parseFloat(txtBunkerQty) * parseFloat(txtBunkerPrice);
			if(isNaN(calc)){calc =0; }
			$("#txtBunkerAmt_"+i).val(calc.toFixed(2));
		}
		
		var txtConBunkerCount = $("#txtConBunkerCount").val();
		for(var i=1;i<=txtConBunkerCount;i++)
		{
			var txtConBunkerQty = $("#txtConBunkerQty_"+i).val();if(isNaN(txtConBunkerQty)){txtConBunkerQty =0; }
			var txtConBunkerPrice = $("#txtConBunkerPrice_"+i).val();if(isNaN(txtConBunkerPrice)){txtConBunkerPrice =0; }
			var calc = parseFloat(txtConBunkerQty) * parseFloat(txtConBunkerPrice);
			if(isNaN(calc)){calc =0; }
			$("#txtConBunkerAmt_"+i).val(calc.toFixed(2));
		}
		if($("[id^=txtConBunkerAmt_]").sum() > 0){$("#txtTotalBunkerCost").val($("[id^=txtConBunkerAmt_]").sum().toFixed(2));}
	getFinalCalculation();
}




function getDistance(i)
{
	
		$("#txtDistance_"+i).val("");
		$("#ploader_"+i).show();
		var loadporttext = disporttext = "";
		if($("#selFPort_"+i).val()!=""){loadporttext = $("#selFPort_"+i+" option:selected").text();}else{loadporttext = "";}
		if($("#selTPort_"+i).val()!=""){disporttext = $("#selTPort_"+i+" option:selected").text();}else{disporttext = "";}
		$("#spanLoadPort_"+i).html(loadporttext);
		$("#spanDisPort_"+i).html(disporttext);
		$("#TranDisPort_"+i).html(loadporttext);
		$("#spanDDCLPort_"+i).html(loadporttext);
		$("#spanDDCDPort_"+i).html(disporttext);
		if($('#selFPort_'+i).val() != "" && $('#selTPort_'+i).val() != "" && $('#selDType_'+i).val() != "")
		{
			$("#txtDistance_"+i).val(0);
			$.post("options.php?id=10",{selFPort:""+$("#selFPort_"+i).val()+"",selTPort:""+$("#selTPort_"+i).val()+"",selDType:""+$("#selDType_"+i).val()+""}, function(data) 
			{
					$('#txtDistance_'+i).val(data);
					$("#ploader_"+i).hide();
					getVoyageTime();
			});
		}
		else
		{
			$('#txtDistance_'+i).val(0);
			$("#ploader_"+i).hide();
			getVoyageTime();
		}
}

function getVoyageTime()
{
	var id = $("#p_rotationID").val();
	$("#txtBDays,#txtLDays").val(0);
	$("#txtLDist,#txtBDist,#txtTDUMT,#txtTFUMT").val(0);
	var ladendays = ballastdays = idealdays = workingdays = consumptiondo = consumptionfo =  0;
	for(var i=1;i<=id;i++)
	{
	    if($("#txtMargin_"+i).val() == ""){var margin = 0;}else{var margin = $("#txtMargin_"+i).val();}
		if($("#txtDistance_"+i).val() == ""){var distance = 0;}else{var distance = $("#txtDistance_"+i).val();}
		if($("#txtWeather_"+i).val() == ""){var speed_adj = 0;}else{var speed_adj = $("#txtWeather_"+i).val();}
		if($("#selPType_"+i).val() == 1)
		{
			if($("#selSSpeed_"+i).val() == 1)
			{
				var ballast = $("#txtBFullSpeed").val();
				var fo_ballast = $("#txtBFOFullSpeed").val();
				var do_ballast = $("#txtBDOFullSpeed").val();
			}
			if($("#selSSpeed_"+i).val() == 2)
			{
				var ballast = $("#txtBEcoSpeed1").val();
				var fo_ballast = $("#txtBFOEcoSpeed1").val();
				var do_ballast = $("#txtBDOEcoSpeed1").val();
			}
			if($("#selSSpeed_"+i).val() == 3)
			{
				var ballast = $("#txtBEcoSpeed2").val();
				var fo_ballast = $("#txtBFOEcoSpeed2").val();
				var do_ballast = $("#txtBDOEcoSpeed2").val();
			}
			var spd = ($("#txtDistance_"+i).val() /  (parseFloat(ballast) + parseFloat(speed_adj)));
			var calc = spd/24;
			var ttl_days = parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100);
			ballastdays = ballastdays + parseFloat(ttl_days.toFixed(3));
		
			var consp_fo = parseFloat((parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(fo_ballast));
			var consp_do = parseFloat((parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(do_ballast));	
			if(isNaN(consp_do) || consp_do=="")
	        {consp_do = 0;	}
			if(isNaN(consp_fo) || consp_fo=="")
	        {consp_fo = 0;	}
			consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
			consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
			
			var ttl_dis = parseFloat(distance) + parseFloat($("#txtBDist").val());
			$("#txtBDist").val(ttl_dis.toFixed(2));
			$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
		}
		if($("#selPType_"+i).val() == 2)
		{
			if($("#selSSpeed_"+i).val() == 1)
			{
				var ladan = $("#txtLFullSpeed").val();
				var fo_ladan = $("#txtLFOFullSpeed").val();
				var do_ladan = $("#txtLDOFullSpeed").val();
			}
			if($("#selSSpeed_"+i).val() == 2)
			{
				var ladan = $("#txtLEcoSpeed1").val();
				var fo_ladan = $("#txtLFOEcoSpeed1").val();
				var do_ladan = $("#txtLDOEcoSpeed1").val();
			}
			if($("#selSSpeed_"+i).val() == 3)
			{
				var ladan = $("#txtLEcoSpeed2").val();
				var fo_ladan = $("#txtLFOEcoSpeed2").val();
				var do_ladan = $("#txtLDOEcoSpeed2").val();
			}
			
			var spd = ($("#txtDistance_"+i).val() /  (parseFloat(ladan) + parseFloat(speed_adj)));
			var calc = spd/24;
			var ttl_days = parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100);
			ladendays = parseFloat(ladendays) + parseFloat(ttl_days.toFixed(3));
			
			var consp_fo = parseFloat((parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(fo_ladan));
			var consp_do = parseFloat((parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(do_ladan));
			if(isNaN(consp_do) || consp_do=="")
	        {consp_do = 0;	}
			if(isNaN(consp_fo) || consp_fo=="")
	        {consp_fo = 0;	}	
			consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
			consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
			
			var ttl_dis = parseFloat(distance) + parseFloat($("#txtLDist").val());
			$("#txtLDist").val(ttl_dis.toFixed(2));
			$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
		}
		
		//Load Port
		if($("#txtIDays_"+i).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtIDays_"+i).val();}
		if($("#txtWDays_"+i).val() == ""){var w_days = 0;}else{var w_days = $("#txtWDays_"+i).val();}	
		idealdays = parseFloat(idealdays) + parseFloat(idle_days);
		var ttl_days1 = parseFloat(w_days);
		var ttl_days = parseFloat(ttl_days1);
		workingdays = parseFloat(workingdays) + parseFloat(ttl_days);

		var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
		var wrkingdays_fo = parseFloat(w_days) * parseFloat($("#txtPWFOFullSpeed").val());
		var consp_fo =  parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
		
		var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
		var wrkingdays_do = parseFloat(w_days) * parseFloat($("#txtPWDOFullSpeed").val());			
		var consp_do = parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));	
		if(isNaN(consp_do) || consp_do=="")
		{consp_do = 0;	}
		if(isNaN(consp_fo) || consp_fo=="")
		{consp_fo = 0;	}
		consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
		consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
		
		//Dis Port
		if($("#txtDIDays_"+i).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtDIDays_"+i).val();}
		if($("#txtDWDays_"+i).val() == ""){var w_days = 0;}else{var w_days = $("#txtDWDays_"+i).val();}
		idealdays = parseFloat(idealdays) + parseFloat(idle_days);
		var ttl_days1 = parseFloat(w_days);
		var ttl_days = parseFloat(ttl_days1);
		workingdays = parseFloat(workingdays) + parseFloat(ttl_days);
		
		var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
		var wrkingdays_fo = parseFloat(w_days) * parseFloat($("#txtPWFOFullSpeed").val());
		var consp_fo =  parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
		
		var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
		var wrkingdays_do = parseFloat(w_days) * parseFloat($("#txtPWDOFullSpeed").val());	
		var consp_do =  parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));
		if(isNaN(consp_do) || consp_do=="")
		{consp_do = 0;	}
		if(isNaN(consp_fo) || consp_fo=="")
		{consp_fo = 0;	}
		consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
		consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
		
		//transit 
		if($("#txtTLIDays_"+i).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtTLIDays_"+i).val();}
		idealdays = parseFloat(idealdays) + parseFloat(idle_days);
		
		var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
		var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
		if(isNaN(margin_do) || margin_do=="")
		{margin_do = 0;	}
		if(isNaN(margin_fo) || margin_fo=="")
		{margin_fo = 0;	}
		consumptiondo = parseFloat(consumptiondo) + parseFloat(margin_do.toFixed(2));
		consumptionfo = parseFloat(consumptionfo) + parseFloat(margin_fo.toFixed(2));
	}
	if(isNaN(consumptionfo) || consumptionfo=="")
	{consumptionfo = 0;	}
	$("#txtTFUMT").val(consumptionfo.toFixed(2));
	if(isNaN(consumptiondo) || consumptiondo=="")
	{consumptiondo = 0;	}
	$("#txtTDUMT").val(consumptiondo.toFixed(2));
	if(isNaN(ladendays) || ladendays=="")
	{ladendays = 0;	}
	$("#txtLDays").val(ladendays.toFixed(3));
	if(isNaN(ballastdays) || ballastdays=="")
	{ballastdays = 0;	}
	$("#txtBDays").val(ballastdays.toFixed(3));
	if(isNaN(idealdays) || idealdays=="")
	{idealdays = 0;	}
	if(isNaN(workingdays) || workingdays=="")
	{workingdays = 0;	}
	$("#txtTtPIDays").val(parseFloat(idealdays.toFixed(3)));$("#txtTtPWDays").val(parseFloat(workingdays.toFixed(3)));
	$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(3));
	$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
	getBunkerCalculation();
}



function addPortRotationDetails()
{
	var id = idd = $("#p_rotationID").val();
	if($("#selFPort_"+id).val() != "" && $("#selTPort_"+id).val() != "" && $("#selPType_"+id).val() != "" && $("#txtDistance_"+id).val() != "" && $("#selSSpeed_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="pr_Row_'+id+'"><td align="center" class="input-text" ><a href="#pr'+id+'" id="spcancel_'+id+'" onclick="removePortRotation('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left" class="input-text" ><select name="selFPort_'+id+'" style="width:100px;" class="input-text" id="selFPort_'+id+'" onChange="getDistance('+id+');"></select></td><td align="left" class="input-text" ><select  name="selTPort_'+id+'" class="input-text" style="width:100px;" id="selTPort_'+id+'" onChange="getDistance('+id+');"></select></td><td align="left" class="input-text" ><select  name="selDType_'+id+'" class="input-text" style="width:90px;" id="selDType_'+id+'" onChange="getDistance('+id+');"><?php $obj->getPortDistanceType(); ?></select></td><td align="left" class="input-text" ><select name="selPType_'+id+'" class="input-text" style="width:80px;" id="selPType_'+id+'" onChange="getVoyageTime()"; ><?php $obj->getPassageType(); ?></select></td><td align="left" class="input-text" ><input type="text" name="txtDistance_'+id+'" style="width:70px;" id="txtDistance_'+id+'" class="input-text"  value="" placeholder="0.00" onKeyUp="getVoyageTime()"; /><span id="ploader_'+id+'" style="display:none;"><img src="../../img/ajax-loader2.gif" /></span></td><td align="left" class="input-text" ><select  name="selSSpeed_'+id+'" class="input-text" id="selSSpeed_'+id+'" style="width:80px;" onChange="getVoyageTime()"; ><?php $obj->getSelectSpeedList(); ?></select></td><td align="left" class="input-text" ><input type="text" name="txtWeather_'+id+'" id="txtWeather_'+id+'" style="width:70px;" class="input-text" autocomplete="off" placeholder="0.00" onKeyUp="getVoyageTime()"; /></td><td align="left" class="input-text" ><input type="text" name="txtMargin_'+id+'" id="txtMargin_'+id+'" style="width:70px;" class="input-text" autocomplete="off" placeholder="0.00" onKeyUp="getVoyageTime()"; /><input type="hidden" name="txtVoyageTime_'+id+'" id="txtVoyageTime_'+id+'" class="input-text" autocomplete="off" value="" placeholder="0.00" /><input type="hidden" name="txtTTLVoyageDays_'+id+'" id="txtTTLVoyageDays_'+id+'" class="input-text" autocomplete="off" value="" placeholder="0.00" /><input type="hidden" name="txtTTLFoConsp_'+id+'" id="txtTTLFoConsp_'+id+'" class="input-text" readonly value="0.00"  /><input type="hidden" name="txtTTLDoConsp_'+id+'" id="txtTTLDoConsp_'+id+'" class="input-text" readonly value="0.00"  /></td></tr>').appendTo("#tblPortRotation");
		
		$('<tr id="lp_Row_'+id+'"><td align="center" class="input-text" ><span id="spanLoadPort_'+id+'"></span></td><td align="left" class="input-text" ><input type="text" name="txtPCosts_'+id+'" id="txtPCosts_'+id+'" class="input-text" autocomplete="off" value="" style="width:70px;" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td align="left" class="input-text" ><input type="text" name="txtQMT_'+id+'" id="txtQMT_'+id+'" class="input-text" style="width:70px;" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="0.00" /></td><td align="left" class="input-text" ><input type="text" name="txtRate_'+id+'" id="txtRate_'+id+'" class="input-text" style="width:70px;" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="0.00" /></td><td align="left" class="input-text" ><select  name="selLPTerms_'+id+'" class="input-text" style="width:70px;" id="selLPTerms_'+id+'" onChange="getLPRemoveDaysAttr('+id+'),getPortCalculation();"><?php $obj->getLPTermsList(0);?></select></td><td align="left" class="input-text"><input type="text" name="txtWDays_'+id+'" id="txtWDays_'+id+'" class="input-text"  style="width:65px;"  autocomplete="off" readonly value="" onKeyUp= "getVoyageTime();"/></td><td align="left" class="input-text"><input type="text" name="txtIDays_'+id+'" id="txtIDays_'+id+'" class="input-text"  style="width:65px;"  autocomplete="off" value="" placeholder="0.00" onKeyUp="getVoyageTime();"/></td><td align="left" class="input-text" ><select  name="selLPVendor_'+id+'" class="input-text" style="width:100px;" id="selLPVendor_'+id+'"></select></td><td align="left" class="input-text" ><input name="ChkShowDDCLP_'+id+'" class="checkbox" id="ChkShowDDCLP_'+id+'" type="checkbox" value="'+id+'"/></td></tr>').appendTo("#tblLoadPort");
		
		
		$('<tr id="dp_Row_'+id+'"><td align="center" class="input-text" ><span id="spanDisPort_'+id+'"></span></td><td align="left" class="input-text" ><input type="text" name="txtDCosts_'+id+'" id="txtDCosts_'+id+'" class="input-text" style="width:70px;" autocomplete="off" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td align="left" class="input-text" ><input type="text" name="txtDQMT_'+id+'" id="txtDQMT_'+id+'" class="input-text" style="width:70px;" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="0.00" /></td><td align="left" class="input-text" ><input type="text" name="txtDRate_'+id+'" id="txtDRate_'+id+'" class="input-text" style="width:70px;" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="0.00" /></td><td align="left" class="input-text" ><select  name="selDPTerms_'+id+'" class="input-text" style="width:70px;" id="selDPTerms_'+id+'" onChange="getDPRemoveDaysAttr('+id+'),getPortCalculation();"><?php $obj->getLPTermsList(0);?></select></td><td align="left" class="input-text" ><input type="text" name="txtDWDays_'+id+'" id="txtDWDays_'+id+'" class="input-text" style="width:65px;" autocomplete="off" readonly value="" onKeyUp= "getVoyageTime();"/></td><td align="left" class="input-text" ><input type="text" name="txtDIDays_'+id+'" id="txtDIDays_'+id+'" class="input-text" style="width:65px;" autocomplete="off" value="" placeholder="0.00" onKeyUp="getVoyageTime();"/></td><td align="left" class="input-text" ><select  name="selDPVendor_'+id+'" class="input-text" style="width:100px;" id="selDPVendor_'+id+'"></select></td><td align="left" class="input-text" ><input name="ChkShowDDCDP_'+id+'" class="checkbox" id="ChkShowDDCDP_'+id+'" type="checkbox" value="'+id+'"/></td></tr>').appendTo("#tblDisPort");
		
		
		$('<tr id="tp_Row_'+id+'"><td align="center" class="input-text" ><span id="TranDisPort_'+id+'"></span></td><td align="left" class="input-text" ><input type="text" name="txtTLPCosts_'+id+'" id="txtTLPCosts_'+id+'" class="input-text" style="width:70px;" autocomplete="off" value="" placeholder="0.00" onKeyUp= "getPortCalculation();"/></td><td align="left" class="input-text" ><input type="text" name="txtTLIDays_'+id+'" id="txtTLIDays_'+id+'" class="input-text" style="width:70px;" autocomplete="off" value="" placeholder="0.00" onKeyUp="getVoyageTime();"/></td><td align="left" class="input-text" ><select  name="selTPVendor_'+id+'" class="input-text" style="width:120px;" id="selTPVendor_'+id+'"></select></td></tr>').appendTo("#tblTransitPort");
		
		
		$('<tr id="DDCLProw_'+id+'"><td>Load Port <span id="spanDDCLPort_'+id+'"></span></td><td><input type="text"  name="txtEstDDCLPCost_'+id+'" id="txtEstDDCLPCost_'+id+'" class="input-text" value="" placeholder="0.00" autocomplete="off" onKeyUp="getFinalCalculation();" /><input type="hidden" name="txtLPLayTimeIs_'+id+'" id="txtLPLayTimeIs_'+id+'" value="" /></td><td><input type="text"  name="txtDDCLPCost_'+id+'" id="txtDDCLPCost_'+id+'" class="input-text" value="0" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td><td><input type="text"  name="txtLPADDComm_'+id+'" id="txtLPADDComm_'+id+'" class="input-text" value="" placeholder="0.00" autocomplete="off" onKeyUp="getFinalCalculation();" /></td><td><input type="text" name="txtDDCLPNetCostMT_'+id+'" id="txtDDCLPNetCostMT_'+id+'" class="input-text" readonly value="" /><input type="hidden" name="txtDDCLPCostMT_'+id+'" id="txtDDCLPCostMT_'+id+'" class="input-text" readonly value="0" /></td><td><select name="selDDCLPVendor_'+id+'" class="input-text" style="width:120px;" id="selDDCLPVendor_'+id+'" ></select></td></tr><tr id="DDCDProw_'+id+'"><td>Discharge Port <span id="spanDDCDPort_'+id+'"></span></td><td><input type="text"  name="txtEstDDCDPCost_'+id+'" id="txtEstDDCDPCost_'+id+'" class="input-text" value="" placeholder="0.00" autocomplete="off" onKeyUp="getFinalCalculation();" /><input type="hidden" name="txtDPLayTimeIs_'+id+'" id="txtDPLayTimeIs_'+id+'" value="" /></td><td><input type="text"  name="txtDDCDPCost_'+id+'" id="txtDDCDPCost_'+id+'" class="input-text" value="" placeholder="0.00" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td><td><input type="text"  name="txtDPADDComm_'+id+'" id="txtDPADDComm_'+id+'" class="input-text" value="" placeholder="0.00" autocomplete="off" onKeyUp="getFinalCalculation();" /></td><td><input type="text" name="txtDDCDPNetCostMT_'+id+'" id="txtDDCDPNetCostMT_'+id+'" class="input-text" readonly value="" /><input type="hidden" name="txtDDCDPCostMT_'+id+'" id="txtDDCDPCostMT_'+id+'" class="input-text" readonly value="0" /></td><td><select name="selDDCDPVendor_'+id+'" class="input-text" style="width:120px;" id="selDDCDPVendor_'+id+'" ></select></td></tr>').appendTo("#tbDDCBody");
		$("[id^=txtDistance_],[id^=txtWeather_],[id^=txtMargin_],[id^=txtPCosts_],[id^=txtQMT_],[id^=txtRate_],[id^=txtIDays_],[id^=txtDWDays_],[id^=txtWDays_],[id^=txtTLPCosts_],[id^=txtTLIDays_],[id^=txtEstDDCLPCost_],[id^=txtEstDDCDPCost_],[id^=txtLPADDComm_],[id^=txtDPADDComm_]").numeric();
		$("input[type='checkbox']").iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
		});
		$("#selDDCDPVendor_"+id+",#selDDCLPVendor_"+id).html($("#selVendor").html());
		$("#selFPort_"+id+",#selTPort_"+id).html($("#selPort").html());
		$("#selFPort_"+id).val($("#selTPort_"+idd).val());
		$("#TranDisPort_"+id).html($("#selFPort_"+id+" option:selected").text());
		//.........ends...................
		$("#p_rotationID").val(id);
		if($("#selFPort_"+id).val()!=''){$("#spanLoadPort_"+id).html($("#selFPort_"+id+" option:selected").text());}
		if($("#selTPort_"+id).val()!=''){$("#spanDisPort_"+id).html($("#selTPort_"+id+" option:selected").text());}
		
		
		$('[id^=ChkShowDDCDP_]').on('ifChecked', function () { 
		   $("#DDCDProw_"+$(this).val()).hide();
		});
		 
		$('[id^=ChkShowDDCDP_]').on('ifUnchecked', function () { 
		   $("#DDCDProw_"+$(this).val()).show();
		});
		 
		$('[id^=ChkShowDDCLP_]').on('ifChecked', function () { 
			$("#DDCLProw_"+$(this).val()).hide();
		});
		 
		$('[id^=ChkShowDDCLP_]').on('ifUnchecked', function () { 
			$("#DDCLProw_"+$(this).val()).show();
		});
		$("#selLPVendor_"+id).html($("#selVendor").html());
		$("#selDPVendor_"+id).html($("#selVendor").html());
		$("#selTPVendor_"+id).html($("#selVendor").html());
		
	}
	else
	{
		jAlert('Please fill all the records for Sea Passage', 'Alert');
	}
}

function removePortRotation(var1)
{
	if($("#txtLPLayTimeIs_"+var1).val()!=1 && $("#txtDPLayTimeIs_"+var1).val()!=1)
	{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#pr_Row_"+var1).remove();
					$("#lp_Row_"+var1).remove();
					$("#dp_Row_"+var1).remove();
					$("#tp_Row_"+var1).remove();
					$("#DDCDProw_"+var1).remove();
					$("#DDCLProw_"+var1).remove();
					getPortCalculation();		
				 }
			else{return false;}
			});
	}
	else
	 {
		jAlert('Sea Passage can not be deleted because laytime already created for Load/Discharge Port.', 'Alert');
	 }
}


function getPortCalculation()
{
	var id = $("#p_rotationID").val();
	if($("#rdoEstimateType").val()==1)
	{
		var divideby = 24;
	}
	if($("#rdoEstimateType").val()==2)
	{
		var divideby = 24;
	}
	
	if($("#rdoEstimateType").val()==3)
	{
		var divideby = 1;
	}
	for(var i=1;i<=id;i++)
	{
		if($("#txtQMT_"+i).val() != "" && $("#txtRate_"+i).val() != "") 
		{
			var value = 0;
			if($("#selLPTerms_"+i).val() == 1)
			{
				value = parseFloat(parseFloat($("#txtQMT_"+i).val() / $("#txtRate_"+i).val())/parseInt(divideby));
			}
			else if($("#selLPTerms_"+i).val() == 2)
			{
				value = parseFloat(parseFloat($("#txtQMT_"+i).val() / $("#txtRate_"+i).val())/parseInt(divideby)) * 1.262 ;
			}
			else if($("#selLPTerms_"+i).val() == 3)
			{
				value = parseFloat(parseFloat($("#txtQMT_"+i).val() / $("#txtRate_"+i).val())/parseInt(divideby)) * 1.405 ;
			}
			else
			{
				value = parseFloat(parseFloat($("#txtQMT_"+i).val() / $("#txtRate_"+i).val())/parseInt(divideby));
			}
			
			if($("#selLPTerms_"+i).val() != 4)
			{
				if(isNaN(value) || value=="" || value=="Infinity")
				{
					value =0;
				}
				$("#txtWDays_"+i).val(value.toFixed(3));
			}
		}
		else
		{
			$("#txtWDays_"+i).val('0.00');
		}
		
		if($("#txtDQMT_"+i).val() != "" && $("#txtDRate_"+i).val() != "") 
		{
			var value = 0;
			if($("#selDPTerms_"+i).val() == 1)
			{
				value = parseFloat(parseFloat($("#txtDQMT_"+i).val() / $("#txtDRate_"+i).val())/parseInt(divideby));
			}
			else if($("#selDPTerms_"+i).val() == 2)
			{
				value = parseFloat(parseFloat($("#txtDQMT_"+i).val() / $("#txtDRate_"+i).val())/parseInt(divideby)) * 1.262 ;
			}
			else if($("#selDPTerms_"+i).val() == 3)
			{
				value = parseFloat(parseFloat($("#txtDQMT_"+i).val() / $("#txtDRate_"+i).val())/parseInt(divideby)) * 1.405 ;
			}
			else
			{
				value = parseFloat(parseFloat($("#txtDQMT_"+i).val() / $("#txtDRate_"+i).val())/parseInt(divideby));
			}
			
			if($("#selDPTerms_"+i).val() != 4)
			{
				if(isNaN(value) || value=="" || value=="Infinity")
				{
					value =0;
				}
				$("#txtDWDays_"+i).val(value.toFixed(3));
			}
		}
		else
		{
			$("#txtDWDays"+i+",#txtDWDays1_"+i).val('0.00');
		}
	}
	getVoyageTime();
}


function getLPRemoveDaysAttr(i)
{
	if($("#selLPTerms_"+i).val() == 4)
	{
		$("#txtWDays_"+i).removeAttr('readonly');
		$("#txtWDays_"+i).val(0);
	}
	else
	{
		$("#txtWDays_"+i).attr('readonly',true);
	}
}

function getDPRemoveDaysAttr(i)
{
	if($("#selDPTerms_"+i).val() == 4)
	{
		$("#txtDWDays_"+i).removeAttr('readonly');
		$("#txtDWDays_"+i).val(0);
	}
	else
	{
		$("#txtDWDays_"+i).attr('readonly',true);
	}	
}


function getFinalCalculation()
{
	var rev = 0;var freightcostforcom = 0;
	if($("#rdoEstimateType").val()==1)
	{
		if($("#rdoQtyMar1").is(":checked"))
		{
			var balticrate = $("#txtBalticRate").val();
			if(isNaN(balticrate) || balticrate=='')
			{balticrate = 0;}
			
			var txtAdnlPrenium = $("#txtAdnlPrenium").val();
			if(isNaN(txtAdnlPrenium) || txtAdnlPrenium=='')
			{txtAdnlPrenium = 0;}
			var summ = parseFloat(balticrate) + parseFloat(txtAdnlPrenium);
			if(isNaN(summ) || summ=='')
			{summ = 0;}
			$("#txtBaseRate").val(summ);
			
			var qty = $("#txtGasCQMT").val();
			if(isNaN(qty) || qty=='')
			{qty = 0;}
			
			rev = parseFloat(qty) * parseFloat(summ);
		}
		else
		{
			var txtGASLumpsum = parseFloat($("#txtGASLumpsum").val());
			if(isNaN(txtGASLumpsum) || txtGASLumpsum=='')
			{txtGASLumpsum = 0;}
			rev = parseFloat(txtGASLumpsum);
		}
		freightcostforcom = rev;
		$("#txtBaseRateFloating").val($("#txtBaseRate").val());
	}
	
	if($("#rdoEstimateType").val()==2)
	{
		var sum = 0;
		var quantity = 0;
		if($("#ChkLumpsum").is(":checked"))
		{
			sum = parseFloat($("#txtLumpsum").val());
			if(isNaN(sum) || sum==''){sum = 0;}
			var qty = parseFloat($("#txtLumpsumQty").val());
			if(isNaN(qty)){qty = 0.00;}
			quantity = qty;
		}
		else
		{
			var txtTankID = $("#txtTankID").val();
			for(var i =1;i<=txtTankID;i++)
			{
				var txtMinCargoQty = parseFloat($("#txtMinCargoQty_"+i).val());
				if(isNaN(txtMinCargoQty) || txtMinCargoQty==''){txtMinCargoQty = 0;}
				var txtMinFlatRateQty = parseFloat($("#txtMinFlatRateQty_"+i).val());
				if(isNaN(txtMinFlatRateQty) || txtMinFlatRateQty==''){txtMinFlatRateQty = 0;}
				var txtMinWSQty = parseFloat($("#txtMinWSQty_"+i).val());
				if(isNaN(txtMinWSQty) || txtMinWSQty==''){txtMinWSQty = 0;}
				var txtMinDisLeg = parseFloat($("#txtMinDisLeg_"+i).val());
				if(isNaN(txtMinDisLeg) || txtMinDisLeg==''){txtMinDisLeg = 0;}
				var txtMinTotalDis = parseFloat($("#txtMinTotalDis_"+i).val());
				if(isNaN(txtMinTotalDis) || txtMinTotalDis==''){txtMinTotalDis = 0;}
				var cladata = parseFloat(parseFloat(parseFloat(parseFloat(parseFloat(txtMinCargoQty)*parseFloat(txtMinFlatRateQty)*parseFloat(txtMinWSQty))/100)*parseFloat(txtMinDisLeg))/parseFloat(txtMinTotalDis));
				if(isNaN(cladata) || cladata=='Infinity'){cladata = 0;}
				$("#txtMinAmount_"+i).val(cladata.toFixed(2));
				
				var txtOveCargoQty = parseFloat($("#txtOveCargoQty_"+i).val());
				if(isNaN(txtOveCargoQty) || txtOveCargoQty==''){txtOveCargoQty = 0;}
				var txtOveFlatRateQty = parseFloat($("#txtOveFlatRateQty_"+i).val());
				if(isNaN(txtOveFlatRateQty) || txtOveFlatRateQty==''){txtOveFlatRateQty = 0;}
				var txtOveWSQty = parseFloat($("#txtOveWSQty_"+i).val());
				if(isNaN(txtOveWSQty) || txtOveWSQty==''){txtOveWSQty = 0;}
				var txtOveDisLeg = parseFloat($("#txtOveDisLeg_"+i).val());
				if(isNaN(txtOveDisLeg) || txtOveDisLeg==''){txtOveDisLeg = 0;}
				var txtOveTotalDis = parseFloat($("#txtOveTotalDis_"+i).val());
				if(isNaN(txtOveTotalDis) || txtOveTotalDis==''){txtOveTotalDis = 0;}
				var cladata = parseFloat(parseFloat(parseFloat(parseFloat(parseFloat(txtOveCargoQty)*parseFloat(txtOveFlatRateQty)*parseFloat(txtOveWSQty))/100)*parseFloat(txtOveDisLeg))/parseFloat(txtOveTotalDis));
				if(isNaN(cladata) || cladata=='Infinity')
				{cladata = 0;}
				$("#txtOveAmount_"+i).val(cladata.toFixed(2))
				
				$("#txtTotalTankQty_"+i).val(parseFloat(parseFloat(txtOveCargoQty) + parseFloat(txtMinCargoQty)));
				$("#txtTotalTankAmount_"+i).val(parseFloat(parseFloat($("#txtMinAmount_"+i).val()) + parseFloat($("#txtOveAmount_"+i).val())).toFixed(2));
				sum = parseFloat(sum) + parseFloat($("#txtTotalTankAmount_"+i).val());
				quantity = parseFloat(quantity) + parseFloat(txtMinCargoQty) + parseFloat(txtOveCargoQty);
			}
		}
		rev = parseFloat(sum);
		$("#txtTankQuantity").val(parseFloat(quantity));
	}
	
	if($("#rdoEstimateType").val()==3)
	{
		if($("#rdoQtyType1").is(":checked"))
		{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
		if($("#rdoQtyType2").is(":checked"))
		{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
		
		if(parseFloat(cargo_qty))
		{
			var per_mt = parseFloat(cargo_qty);
		}
		
		if($("#rdoQtyType1").is(":checked"))
		{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
		if($("#rdoQtyType2").is(":checked"))
		{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
		
		if(parseFloat(cargo_qty))
		{
			var per_mt = parseFloat(cargo_qty);
		}
		
		if($("#txtddswCost").val() == ""){var ddsw_cost = 0;}else{var ddsw_cost = $("#txtddswCost").val();}
		if($("#txtddswAddComm").val() == ""){var ddsw_addcomm = 0;}else{var ddsw_addcomm = $("#txtddswAddComm").val();}
		var ddsw_value = parseFloat(ddsw_cost) - parseFloat((parseFloat(ddsw_cost) * parseFloat(ddsw_addcomm))/100);
		$("#txtddswValue").val(ddsw_value.toFixed(2));
		
		
		if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
		if($("#txtWSFR").val() == ""){var ws_flat_rate = 0;}else{var ws_flat_rate = $("#txtWSFR").val();}
		if($("#txtWSFD").val() == ""){var fixed_diff = 0;}else{var fixed_diff = $("#txtWSFD").val();}
		if($("#txtMTCPDRate").val() == ""){var ws_rate = 0;}else{var ws_rate = $("#txtMTCPDRate").val();}
		if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
		
		$("[id^=txtMinCargo_]").val(min_qty);$("[id^=txtOverage_]").val(addnl_qty);
		$("[id^=txtMCWSFlatRate_],[id^=txtOvrWSFlatRate_]").val(ws_flat_rate);
		
		$("[id^=txtTotalCargoQty_]").val(cargo_qty);
		
		<!----------------- Quantity  ------------------->
		if($("#rdoQtyType1").is(":checked"))
		{if($("#txtCQMT").val() == ""){var per_amt = 0;}else{var per_amt = $("#txtCQMT").val();}}
		if($("#rdoQtyType2").is(":checked"))
		{if($("#txtTotalFreightQty").val() == ""){var per_amt = 0;}else{var per_amt = $("#txtTotalFreightQty").val();}}
		
		if($("#rdoMMarket1").is(":checked"))
		{
			<!----------------- Agreed Gross Freight  ------------------->
			if($("#txtMTCPDRate").val() == ""){var agr_gross_fr = 0;}else{var agr_gross_fr = $("#txtMTCPDRate").val();}
			
			<!----------------- (Freight Adjustment) Gross Freight  ------------------->
			var gross_fr = parseFloat(agr_gross_fr) * parseFloat(per_amt);
			$("#txtFrAdjUsdGF").val(gross_fr.toFixed(2));
			<!----------------- (Freight Adjustment) Gross Freight(MT)  ------------------->
			var gross_fr_mt = parseFloat(gross_fr) / parseFloat(per_amt);
			if(isNaN(gross_fr_mt) || gross_fr_mt=='Infinity'){gross_fr_mt = 0;}
			$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
			<!-----------------  DF Qty (MT)  ------------------->
			if($("#txtDFQMT").val() == "" || $("#txtDFQMT").val() == 0)
			{
				var df_qty = 0;
				$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
			}
			else
			{
				var df_qty = $("#txtDFQMT").val();
				var dead_fr = parseFloat(agr_gross_fr) * parseFloat(df_qty);
				$("#txtFrAdjUsdDF").val(dead_fr.toFixed(2));
				var dead_fr_mt = parseFloat(dead_fr) / parseFloat(df_qty);
				$("#txtFrAdjUsdDFMT").val(dead_fr_mt.toFixed(2));
			}
			<!-----------------  Addnl Cargo Rate (USD/MT)  ------------------->
			if($("#txtAddnlCRate").val() == ""){var addnl_rate = 0;}else{var addnl_rate = $("#txtAddnlCRate").val();}
			if($("#txtAddnlQMT").val() == "" || $("#txtAddnlQMT").val() == 0 )
			{
				var addnl_qty = 0;
				$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
			}
			else
			{
				<!-----------------  Addnl Qty (MT)  ------------------->
				var addnl_qty = $("#txtAddnlQMT").val();
				<!-----------------(Freight Adjustment)  Addnl Freight(USD)  ------------------->
				var addnl_fr = parseFloat(addnl_rate) * parseFloat(addnl_qty);
				$("#txtFrAdjUsdAF").val(addnl_fr.toFixed(2));
				<!-----------------(Freight Adjustment)  Addnl Freight(MT)  ------------------->
				var addnl_fr_mt = parseFloat(addnl_fr) / parseFloat(addnl_qty);
				$("#txtFrAdjUsdAFMT").val(addnl_fr_mt.toFixed(2));
			}
			<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
			$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum().toFixed(2));
			
			<!-----------------(Freight Adjustment)  Total Freight(MT)  ------------------->
			var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / (parseFloat(per_amt) + parseFloat(df_qty) + parseFloat(addnl_qty));
			if(isNaN(ttl_fr_mt) || ttl_fr_mt=='Infinity'){ttl_fr_mt = 0;}
			$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
			$("#txtBaseRateFloating").val($("#txtMTCPDRate").val());
		}
		
		if($("#rdoMMarket2").is(":checked"))
		{
			var txtMLumpsum = $("#txtMLumpsum").val();if(isNaN(txtMLumpsum)){txtMLumpsum = 0;}
			var txtCQMT = $("#txtCQMT").val();if(isNaN(txtCQMT)){txtCQMT = 0;}
			
			$("#txtFrAdjUsdGF").val($("#txtMLumpsum").val());
			
			<!-----------------(Freight Adjustment)  Gross Freight(MT)  ------------------->
			var gross_fr_mt = parseFloat($("#txtFrAdjUsdGF").val()) / parseFloat(per_amt);
			if(isNaN(gross_fr_mt) || gross_fr_mt=='Infinity'){gross_fr_mt = 0;}
			$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
			
			$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
			$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
			
			<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
			$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum());
			
			<!-----------------(Freight Adjustment)  Gross Freight(MT)  ------------------->
			var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / parseFloat(per_amt);
			if(isNaN(ttl_fr_mt) || ttl_fr_mt=='Infinity'){ttl_fr_mt = 0;}
			$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
			
			var freightforcom = parseFloat(txtMLumpsum)/parseFloat(txtCQMT);if(isNaN(freightforcom)){freightforcom = 0;}
			$("#txtBaseRateFloating").val(freightforcom.toFixed(2));
		}
		
		<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
		if($("#rdoQtyType1").is(":checked"))
		{
			if($("#txtFrAdjUsdTF").val() == ""){var ttl_fr = 0;}else{var ttl_fr = $("#txtFrAdjUsdTF").val();}
			
			<!-----------------(Freight Adjustment) Brokerage(Persent)  ------------------->
			if($("#txtFrAdjPerAgC").val() == ""){var ag_per = 0;}else{var ag_per = $("#txtFrAdjPerAgC").val();}
			var agent_commission = (parseFloat(ttl_fr) * parseFloat(ag_per)) / 100;
			
			<!-----------------(Freight Adjustment) Brokerage(USD)  ------------------->
			$("#txtFrAdjUsdAgC").val(agent_commission.toFixed(2));
			
			<!-----------------(Freight Adjustment) Net Freight Payable(USD)  ------------------->
			var net_fr = parseFloat(ttl_fr) - parseFloat($("#txtFrAdjUsdAgC").sum());
			$("#txtFrAdjUsdFP").val(net_fr.toFixed(2));
			
			<!-----------------(Freight Adjustment) Final Net Freight Payable(MT)  ------------------->
			var net_fr_mt = parseFloat(net_fr) / parseFloat(per_amt);
			if(isNaN(net_fr_mt) || net_fr_mt=='Infinity'){net_fr_mt = 0;}
			//$("#txtFGFFDMT").val(net_fr_mt.toFixed(2));
			
			<!----------------- Revenue (Final Nett Freight) ------------------->
			if($("#txtFrAdjUsdFP").val() == ""){var final_nett_frt = 0;}else{var final_nett_frt = $("#txtFrAdjUsdFP").val();}
			var rev = parseFloat(final_nett_frt);
		}
		if($("#rdoQtyType2").is(":checked"))
		{
			var rev = parseFloat($("[id^=txtQtyNetFreight_]").sum()); if(ttl_fr == ""){var ttl_fr = 0;} 
		}
		freightcostforcom = $("#txtFrAdjUsdTF").val();
	}
	
	for(var l = 1;l<=$("#txtBRokageCount").val();l++)
	{
		if($("#txtBrCommPercent_"+l).val() == ""){var brokerage_comm = 0;}else{var brokerage_comm = $("#txtBrCommPercent_"+l).val();}
		var brokerage_comm_usd = (parseFloat(rev) * parseFloat(brokerage_comm))/100;
		$("#txtBrComm_"+l).val(brokerage_comm_usd.toFixed(2));
	}
	$("#txtBrCommPercent").val($("[id^=txtBrCommPercent_]").sum().toFixed(2));
	$("#txtBrComm").val($("[id^=txtBrComm_]").sum().toFixed(2));
	
	if($("#txtFrAdjPerAC").val() == ""){var ac_per = 0;}else{var ac_per = $("#txtFrAdjPerAC").val();}
	var address_commission = parseFloat(parseFloat(rev) * parseFloat(ac_per)) / 100;
	$("#txtFrAdjUsdAC").val(address_commission.toFixed(2));
	
	rev = parseFloat(rev) - parseFloat(address_commission);
	
	var otherincomeAmt = 0;
	for(var l = 1;l<=$("#txtOthInCount").val();l++)
	{
		if($("#txtOtherAmt_"+l).val() == ""){var otheramt = 0;}else{var otheramt = $("#txtOtherAmt_"+l).val();}
		if($("#txtOtherAddComm_"+l).val() == "" && isNaN($("#txtOtherAddComm_"+l).val())){var otherpercent = 100;}else{var otherpercent = $("#txtOtherAddComm_"+l).val();}
		var other_usd = parseFloat(otheramt) - parseFloat(parseFloat(parseFloat(otheramt) * parseFloat(otherpercent))/100);
		if(isNaN(other_usd)){other_usd = 0;}
		$("#txtOtherNetAmt_"+l).val(other_usd.toFixed(2));
		otherincomeAmt = parseFloat(otherincomeAmt) + parseFloat(other_usd);
	}
	rev = parseFloat(rev) + parseFloat(otherincomeAmt);
	
	$("#txtRevenue").val(rev.toFixed(2));
	
	if($("#txtCVE").val() == ""){var txtCVE = 0;}else{var txtCVE = $("#txtCVE").val();}
	if($("#txtTDays").val() == ""){var txtTDays = 0;}else{var txtTDays = $("#txtTDays").val();}
	var calmul = parseFloat(parseFloat(parseFloat(txtCVE)/30)*parseFloat(txtTDays));
	if(isNaN(calmul) || calmul=="" || calmul=="Infinity"){calmul = 0;}
	$("#txtCVEAmt").val(calmul.toFixed(2));
	$("#txtHidCVEAmt").val(calmul.toFixed(2));
	
	
	if($("#txtDailyVesselOperatingExpenses").val() == ""){var daily_vessel_exp = 0;}else{var daily_vessel_exp = $("#txtDailyVesselOperatingExpenses").val();}
	var hireage_amt = parseFloat(parseFloat(daily_vessel_exp)*parseFloat($("#txtTDays").val())).toFixed(2);
	if(hireage_amt=="" || isNaN(hireage_amt)){hireage_amt =0;}
	
	$("#txtHireargeAmt").val(parseFloat(hireage_amt).toFixed(2));
	if($("#txtBallastBonus").val() == ""){var txtBallastBonus = 0;}else{var txtBallastBonus = $("#txtBallastBonus").val();}
	if($("#txtHireargePercent").val() == ""){var txtHireargePercent = 0;}else{var txtHireargePercent = $("#txtHireargePercent").val();}
	var ballast_hireage = parseFloat(hireage_amt) + parseFloat(txtBallastBonus);
	var txtHireargePercentAmt = parseFloat(parseFloat(parseFloat(ballast_hireage)*parseFloat(txtHireargePercent))/100);
	if(txtHireargePercentAmt=="" || isNaN(txtHireargePercentAmt)){txtHireargePercentAmt =0;}
	$("#txtHireargePercentAmt").val(parseFloat(txtHireargePercentAmt).toFixed(2));
	var txtNettHireargeAmt = parseFloat(parseFloat(ballast_hireage) -  parseFloat(txtHireargePercentAmt));
	$("#txtNettHireargeAmt").val(parseFloat(txtNettHireargeAmt).toFixed(2));	
	
	var hireinvoiceamt = $("[id^=txtInvoiceAmt_]").sum();
	var InvoiceVal = parseInt($("[id^=txtInvoiceVal_]").sum());
	var final_hireage_amt = parseFloat(hireinvoiceamt);
	if(InvoiceVal > 0)
	{
	    txtNettHireargeAmt = final_hireage_amt;
	}
		
	$("#txtTotalOperationalCost").val($("[id^=txtORCAmt_],#txtBrComm,#txtHidCVEAmt").sum());
	
	
	$("#txtTotalPortCost").val($("[id^=txtPCosts_],[id^=txtDCosts_],[id^=txtTLPCosts_]").sum().toFixed(2));
	
	var totalexpenses = $("#txtTotalOperationalCost,#txtTotalPortCost,#txtTotalBunkerCost").sum().toFixed(2);
	if($("#txtRevenue").val() == ""){var revenue = 0;}else{var revenue = $("#txtRevenue").val();}
	var costbeforebamarage = parseFloat(revenue) - parseFloat(totalexpenses) - parseFloat(txtNettHireargeAmt);
	var voyage_earning = parseFloat(revenue) - parseFloat(totalexpenses) + parseFloat(getDDCOwnerCalculation()) - parseFloat(txtNettHireargeAmt);
	
	$("#txtVoyageEarnings").val(costbeforebamarage.toFixed(2));
	$("#txtGTTLVoyageEarnings").val(voyage_earning.toFixed(2));
	if($("#txtTDays").val() == 0){var ttl_days = 1;}else{var ttl_days = $("#txtTDays").val();}
	var daily_earnings = parseFloat(voyage_earning) / parseFloat(ttl_days);
	var costbeforebamarage_erning = parseFloat(costbeforebamarage) / parseFloat(ttl_days);
	$("#txtDailyEarnings").val(daily_earnings.toFixed(2));
		
	var netprofilt = parseFloat(voyage_earning);
	var nett_daily_profit = parseFloat(parseFloat(voyage_earning)/parseFloat(ttl_days));
	if(nett_daily_profit=="" || isNaN(nett_daily_profit) || nett_daily_profit=="Infinity"){nett_daily_profit =0;}
	$("#txtNettDailyProfit").val(parseFloat(nett_daily_profit).toFixed(2));
	$("#txtPL").val(parseFloat(netprofilt).toFixed(2));
	
	$('[id^=txtLPOSCCost_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				var costMT = this.value / parseFloat(per_mt);
				$("#txtLPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
	$('[id^=txtDPOSCCost_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				var costMT = this.value / parseFloat(per_mt);
				$("#txtDPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
	$('[id^=txtTPOSCCost_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				var costMT = this.value / parseFloat(per_mt);
				$("#txtTPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
	
	<?php if($_SESSION['selBType']==1 || $_SESSION['selBType']==3){?>		
	var txtGFreightFloating = parseFloat(freightcostforcom);if(isNaN(txtGFreightFloating)){txtGFreightFloating = 0;}
	$("#txtGFreightFloating").val(txtGFreightFloating.toFixed(4));
	var txtNFreightFloating = parseFloat(freightcostforcom);if(isNaN(txtNFreightFloating)){txtNFreightFloating = 0;}
	$("#txtNFreightFloating").val(txtNFreightFloating.toFixed(4));
	
	var demurrage = parseFloat(getDDCOwnerCalculation());
	var txtOExpensesFloating = parseFloat($("#txtTotalOperationalCost").val()) + parseFloat($("#txtTotalPortCost").val()) + parseFloat($("#txtTotalBunkerCost").val()) + parseFloat($("#txtNettHireargeAmt").val());if(isNaN(txtOExpensesFloating)){txtOExpensesFloating = 0;}
	$("#txtOExpensesFloating,#txtOExpensesFixed,#txtOExpensesAverage").val(txtOExpensesFloating.toFixed(4));
	var txtVEarningFloating = parseFloat($("#txtNFreightFloating").val()) - parseFloat($("#txtOExpensesFloating").val()) + parseFloat(demurrage);if(isNaN(txtVEarningFloating)){txtVEarningFloating = 0;}
	<?php if($_SESSION['selBType']==1){?>
	var quantitydrygas = $("#txtGasCQMT").val();if(isNaN(quantitydrygas)){quantitydrygas = 0;}
	var txtTCEMonthFixed    = parseFloat($("#txtTCEMonthFixed").val());if(isNaN(txtTCEMonthFixed)){txtTCEMonthFixed = 0;}
	var txtDailyEarningFixed = parseFloat(parseFloat($("#txtTCEMonthFixed").val())*12)/365;if(isNaN(txtDailyEarningFixed)){txtDailyEarningFixed = 0;}
	$("#txtDailyEarningFixed").val(txtDailyEarningFixed.toFixed(4));
	<?php }?>
	<?php if($_SESSION['selBType']==3){?>
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var quantitydrygas = 0;}else{var quantitydrygas = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var quantitydrygas = 0;}else{var quantitydrygas = $("#txtTotalFreightQty").val();}}
	var txtDailyEarningFixed    = parseFloat($("#txtDailyEarningFixed").val());if(isNaN(txtDailyEarningFixed)){txtDailyEarningFixed = 0;}
	<?php }?>
	var txtVEarningFixed = parseFloat(txtDailyEarningFixed)*parseFloat($("#txtTDays").val());if(isNaN(txtVEarningFixed)){txtVEarningFixed = 0;}
	
	$("#txtVEarningFixed").val(txtVEarningFixed.toFixed(4));
	var txtNFreightFixed = parseFloat($("#txtVEarningFixed").val()) + parseFloat($("#txtOExpensesFixed").val()) - parseFloat(demurrage.toFixed(2));if(isNaN(txtNFreightFixed)){txtNFreightFixed = 0;}
	
	$("#txtNFreightFixed").val(txtNFreightFixed.toFixed(4));
	
	var betfreightfixed = parseFloat($("#txtNFreightFixed").val());
	var txtGFreightFixed = parseFloat(betfreightfixed);
	if(isNaN(txtGFreightFixed)){txtGFreightFixed = 0.00;}
	$("#txtGFreightFixed").val(txtGFreightFixed.toFixed(4));
	var txtFrAdjUsdAF = parseFloat($("#txtFrAdjUsdAF").val());if(isNaN(txtFrAdjUsdAF)){txtFrAdjUsdAF = 0;}
	var txtFrAdjUsdDF = parseFloat($("#txtFrAdjUsdDF").val());if(isNaN(txtFrAdjUsdDF)){txtFrAdjUsdDF = 0;}
	txtGFreightFixed = parseFloat(txtGFreightFixed) - parseFloat(txtFrAdjUsdAF) - parseFloat(txtFrAdjUsdDF);
	var txtBaseRateFixed = parseFloat(txtGFreightFixed)/parseFloat(quantitydrygas);if(isNaN(txtBaseRateFixed)){txtBaseRateFixed = 0;}
	$("#txtBaseRateFixed").val(txtBaseRateFixed.toFixed(4));
	
	var txtBaseRateFloating = $("#txtBaseRateFloating").val();if(isNaN(txtBaseRateFloating)){txtBaseRateFloating = 0;}
	var txtBaseRateFixed    = $("#txtBaseRateFixed").val();if(isNaN(txtBaseRateFixed)){txtBaseRateFixed = 0;}
	var averageRate = (parseFloat(txtBaseRateFloating) + parseFloat(txtBaseRateFixed))/2;if(isNaN(averageRate)){averageRate = 0;}
	$("#txtBaseRateAverage").val(averageRate.toFixed(2));
	
	var txtGFreightFloating = $("#txtGFreightFloating").val();if(isNaN(txtGFreightFloating)){txtGFreightFloating = 0;}
	var txtGFreightFixed    = $("#txtGFreightFixed").val();if(isNaN(txtGFreightFixed)){txtGFreightFixed = 0;}
	var averagefreight = (parseFloat(txtGFreightFloating) + parseFloat(txtGFreightFixed))/2;if(isNaN(averagefreight)){averagefreight = 0;}
	$("#txtGFreightAverage").val(averagefreight.toFixed(2));
	
	var txtNFreightFloating = $("#txtNFreightFloating").val();if(isNaN(txtNFreightFloating)){txtNFreightFloating = 0;}
	var txtNFreightFixed    = $("#txtNFreightFixed").val();if(isNaN(txtNFreightFixed)){txtNFreightFixed = 0;}
	var averagefreight = (parseFloat(txtNFreightFloating) + parseFloat(txtNFreightFixed))/2;if(isNaN(averagefreight)){averagefreight = 0;}
	$("#txtNFreightAverage").val(averagefreight.toFixed(2));
	
	$("#txtVEarningFloating").val(txtVEarningFloating.toFixed(4));
	var txtVEarningFloating = $("#txtVEarningFloating").val();if(isNaN(txtVEarningFloating)){txtVEarningFloating = 0;}
	var averageexpense = (parseFloat(txtVEarningFloating) + parseFloat(txtVEarningFixed))/2;if(isNaN(averageexpense)){averageexpense = 0;}
	$("#txtVEarningAverage").val(averageexpense.toFixed(2));
	
	var txtDailyEarningFloating = parseFloat($("#txtVEarningFloating").val())/parseFloat($("#txtTDays").val());if(isNaN(txtDailyEarningFloating)){txtDailyEarningFloating = 0;}
	$("#txtDailyEarningFloating").val(txtDailyEarningFloating.toFixed(4));
	
	var txtDailyEarningAverage = (parseFloat(txtDailyEarningFloating) + parseFloat(txtDailyEarningFixed))/2;if(isNaN(txtDailyEarningAverage)){txtDailyEarningAverage = 0;}
	$("#txtDailyEarningAverage").val(txtDailyEarningAverage.toFixed(2));
	
	var txtTCEMonthFloating = parseFloat(parseFloat(parseFloat($("#txtDailyEarningFloating").val())*365)/12);if(isNaN(txtTCEMonthFloating)){txtTCEMonthFloating = 0;}
	var txtTCEMonthAverage = (parseFloat(txtTCEMonthFloating) + parseFloat(txtTCEMonthFixed))/2;if(isNaN(txtTCEMonthAverage)){txtTCEMonthAverage = 0;}
	$("#txtTCEMonthFloating").val(txtTCEMonthFloating.toFixed(4));$("#txtTCEMonthAverage").val(txtTCEMonthAverage.toFixed(2));
	
	var txtOPExpensesFloating = $("#txtOPExpensesFloating").val();if(isNaN(txtOPExpensesFloating) || txtOPExpensesFloating ==''){txtOPExpensesFloating = 0;}
	$("#txtOPExpensesFixed").val(txtOPExpensesFloating);
	var txtOPExpensesFixed    = $("#txtOPExpensesFixed").val();if(isNaN(txtOPExpensesFixed) || txtOPExpensesFixed ==''){txtOPExpensesFixed = 0;}
	var txtOPExpensesAverage = (parseFloat(txtOPExpensesFloating) + parseFloat(txtOPExpensesFixed))/2;if(isNaN(txtOPExpensesAverage)){txtOPExpensesAverage = 0;}
	$("#txtOPExpensesAverage").val(txtOPExpensesAverage.toFixed(2));
	
	var txtNetDailyProfitFloating = parseFloat($("#txtDailyEarningFloating").val()) - parseFloat(txtOPExpensesFloating);if(isNaN(txtNetDailyProfitFloating)){txtNetDailyProfitFloating = 0;}
	var txtNetDailyProfitFixed = parseFloat(txtDailyEarningFixed) - parseFloat(txtOPExpensesFloating);if(isNaN(txtNetDailyProfitFixed)){txtNetDailyProfitFixed = 0;}
	var txtNetDailyProfitAverage = parseFloat(parseFloat(txtNetDailyProfitFloating) + parseFloat(txtNetDailyProfitFixed))/2;if(isNaN(txtNetDailyProfitAverage)){txtNetDailyProfitAverage = 0;}
	$("#txtNetDailyProfitFloating").val(txtNetDailyProfitFloating.toFixed(2));
	$("#txtNetDailyProfitFixed").val(txtNetDailyProfitFixed.toFixed(2));
	$("#txtNetDailyProfitAverage").val(txtNetDailyProfitAverage.toFixed(2));
	<?php }?>
}



function addBrokerageRow()
{
	var id = $("#txtBRokageCount").val();
	if($("#txtBrCommPercent_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="tbrRow_'+id+'"><td><a href="#tb1" onclick="removeBrokerage('+id+');" ><i class="fa fa-times" style="color:red;"></a></td><td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td><td><input type="text" name="txtBrCommPercent_'+id+'" id="txtBrCommPercent_'+id+'" size="12" class="input-text" autocomplete="off" value="" onKeyUp="getFinalCalculation();" placeholder="0.00" /></td><td><input type="text" name="txtBrComm_'+id+'" size="12" id="txtBrComm_'+id+'" class="input-text" readonly value="0.00" /></td><td><select  name="selBrCommVendor_'+id+'" class="input-text" style="width:120px;" id="selBrCommVendor_'+id+'"></select></td></tr>').appendTo("#tbodyBrokerage");
	    
		$("#selBrCommVendor_"+id).html($("#selVendor").html());
		$("[id^=txtBrCommPercent_]").numeric();	
		$("#txtBRokageCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeBrokerage(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#tbrRow_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}

function getTotalDWT()
{
	var dwt = $("#txtDWTS").val();
	
	if($("#rdoCap1").is(":checked"))
	{
		if($("#txtGCap").val() == ""){var gcap = 0;}else{var gcap = $("#txtGCap").val();}
		if($("#txtSF").val() != "" || $("#txtSF").val() != 0)
		{
			var ttl = parseFloat(parseFloat(gcap)*35.3146) / parseFloat($("#txtSF").val());
			if(parseFloat($("#txtSF").val())>0)
			{
				$("#txtLoadable").val(ttl.toFixed(2));
			}
			else
			{
				$("#txtLoadable").val(0);
			}
		}
	}
	
}

function showCapField(val)
{
	if(val == 1)
	{
		$("#txtGCap").removeAttr('disabled');
		$("#txtBCap").attr('disabled','disabled');
		getTotalDWT();
		getTotalDWT1();
	}
	if(val == 2)
	{
		$("#txtGCap").attr('disabled','disabled');
		$("#txtBCap").removeAttr('disabled');		
		getTotalDWT();
		getTotalDWT1();
	}
}

function getTotalDWT1()
{
	var dwt = $("#txtDWTS").val();
	
	if($("#rdoCap2").is(":checked"))
	{
		if($("#txtBCap").val() == ""){var bcap = 0;}else{var bcap = $("#txtBCap").val();}
		
		if($("#txtSF").val() != "" || $("#txtSF").val() != 0)
		{			
			var ttl = parseFloat(parseFloat(bcap)*35.3146) / parseFloat($("#txtSF").val());
			if(parseFloat($("#txtSF").val())>0)
			{
				$("#txtLoadable").val(ttl.toFixed(2));
			}
			else
			{
				$("#txtLoadable").val(0);
			}
		}
	}
	getFinalCalculation();
}

function getORCCalculate(var1)
{
	if($("#rdoEstimateType").val()==1)
	{
		var cargo_qty = $("#txtGasCQMT").val();
	}
	if($("#rdoEstimateType").val()==2)
	{
		var cargo_qty = $("#txtTankQuantity").val();
	}
	
	if($("#rdoEstimateType").val()==3)
	{
		if($("#rdoQtyType1").is(":checked"))
		{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
		if($("#rdoQtyType2").is(":checked"))
		{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	}
	
	
	if($("#txtORCAmt_"+var1).val() == ""){var cc_abs = 0;}else{var cc_abs = $("#txtORCAmt_"+var1).val();}
	var calc = parseFloat(cc_abs) / parseFloat(cargo_qty);
	$("#txtORCAmtMT_"+var1).val(calc.toFixed(2));
	getFinalCalculation();	
}

function getValidate(val)
{
	$("#update_status").val(val);
	getFinalCalculation();
	
}


function addORCRow()
{
	var id = $("#txtOWCCount").val();
	if($("#txtHidORCID_"+id).val() != "" && $("#txtORCAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="OWCRow_'+id+'"><td><a href="#tb1" onClick="removeCC('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select name="txtHidORCID_'+id+'" id="txtHidORCID_'+id+'" style="width:130px;" ></select></td><td><input type="text" name="txtORCAmt_'+id+'" id="txtORCAmt_'+id+'" onKeyUp="getORCCalculate('+id+');" class="input-text" autocomplete="off" value="" placeholder="0.00" size="12"  /></td><td><select name="selORCVendor_'+id+'" class="input-text" style="width:120px;" id="selORCVendor_'+id+'"></select></td></tr>').appendTo("#OWCBody");
		$("[id^=txtORCAmt_]").numeric();	
		$("#selORCVendor_"+id).html($("#selVendor").html());
		$("#txtHidORCID_"+id).html($("#selOwnerCost").html());	
		$("#txtOWCCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeCC(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#OWCRow_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}



function getDDCOwnerCalculation(){
	
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	
	$('[id^=txtDDCLPCost_]').each(function(index) {
		var rowid       = this.id;
		var ddswLP1     = rowid.split('_')[1];
		if($("#txtLPLayTimeIs_"+ddswLP1).val()==0)
		{
		$("#txtDDCLPCost_"+ddswLP1).val($("#txtEstDDCLPCost_"+ddswLP1).val());
		}
		if($("#txtDDCLPCost_"+ddswLP1).val() == ""){var addLPcost = 0;}else{var addLPcost = $("#txtDDCLPCost_"+ddswLP1).val();}
		var addcomm = $("#txtLPADDComm_"+ddswLP1).val();
		if(isNaN(addcomm) || addcomm==''){addcomm =0;}
		var addcommamt = parseFloat(parseFloat(parseFloat(addLPcost)*parseFloat(addcomm))/100);
		var remLpAmt  = parseFloat(addLPcost) - parseFloat(addcommamt);
		$('#txtDDCLPNetCostMT_'+ddswLP1).val(remLpAmt.toFixed(2));
		var ddswLPCCost = parseFloat(remLpAmt)/parseFloat(cargo_qty);
		$('#txtDDCLPCostMT_'+ddswLP1).val(ddswLPCCost.toFixed(2));
	});
	
	$('[id^=txtDDCDPCost_]').each(function(index) {
		var rowid       = this.id;
		var ddswDP1     = rowid.split('_')[1];
		if($("#txtDPLayTimeIs_"+ddswDP1).val()==0)
		{
		$("#txtDDCDPCost_"+ddswDP1).val($("#txtEstDDCDPCost_"+ddswDP1).val());
		}
		if($("#txtDDCDPCost_"+ddswDP1).val() == ""){var addDPcost = 0;}else{var addDPcost = $("#txtDDCDPCost_"+ddswDP1).val();}
		var addcomm = $("#txtDPADDComm_"+ddswDP1).val();
		if(isNaN(addcomm) || addcomm==''){addcomm =0;}
		var addcommamt = parseFloat(parseFloat(parseFloat(addDPcost)*parseFloat(addcomm))/100);
		var remLpAmt  = parseFloat(addDPcost) - parseFloat(addcommamt);
		$('#txtDDCDPNetCostMT_'+ddswDP1).val(remLpAmt.toFixed(2));
		var ddswDPCCost = parseFloat(remLpAmt)/parseFloat(cargo_qty);
		$('#txtDDCDPCostMT_'+ddswDP1).val(ddswDPCCost.toFixed(2));
	});
	var LPDPttl = parseFloat($('[id^=txtDDCLPNetCostMT_]').sum().toFixed(2)) + parseFloat($('[id^=txtDDCDPNetCostMT_]').sum().toFixed(2));
	return LPDPttl;
}

function showHideQtyVendorDiv(value)
{
	if(value ==1)
	{
		$("#divMarket1,#divMarket2,#divMarket3,#divMarket4,#divMarket5,#divMarket6,#divMarket7,#divMarket8,#divMarket9,#divMarket10").show();
		$("#divQty1,#divQty2,#divQty3,#divQty4,#tblQtyFreight").hide();
		$("#tblQtyFreight").empty();
		$("#txtQTYID").val(0);
		getQtyLocalFreightCal();
	}
	if(value ==2)
	{
		$("#divMarket1,#divMarket2,#divMarket3,#divMarket4,#divMarket5,#divMarket6,#divMarket7,#divMarket8,#divMarket9,#divMarket10").hide();
		$("#divQty1,#divQty2,#divQty3,#divQty4,#tblQtyFreight").show();
		if(value==2)
		{
		  getQtyLocalFreightCal();
		}
	}
	getPortCalculation();
}

function addQtyVendorDetails()
{
	var id = $("#txtQTYID").val();
	if($("#selQtyVendorList").val() != "" && $("#txtQtyAggriedFreight").val() != "" && $("#txtFreightQty").val() != "" )
	{
		$("#tbrQtyVRow_empty,#tbrQtyVRow_empty1,#tbrQtyVRow_empty2").remove();
		id = (id - 1) + 2;
		$('<tr id="tbrQtyVRow_'+id+'"><td><a href="#tb'+id+'" onclick="deleteQtyVendorDetails('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select  name="selQtyVendorList_'+id+'" class="input-text" style="width:120px;" id="selQtyVendorList_'+id+'"></select></td><td><input type="text" name="txtQtyLocalAggriedFreight_'+id+'" id="txtQtyLocalAggriedFreight_'+id+'" class="input-text" style="width:100px;"  autocomplete="off" value="" placeholder="0.00" onKeyUp="getQtyLocalFreightCal();"/></td><td><select  name="selCurrencyDisList_'+id+'" class="input-text" style="width:100px;" id="selCurrencyDisList_'+id+'"><?php $obj->getCurrencyList(); ?></select></td><td><input type="text" name="txtDisExchangeRate_'+id+'" id="txtDisExchangeRate_'+id+'" class="input-text" style="width:100px;" autocomplete="off" value="" placeholder="0.00" onKeyUp="getQtyLocalFreightCal();"/></td><td><input type="text" name="txtQtyAggriedFreight_'+id+'" id="txtQtyAggriedFreight_'+id+'" class="input-text" style="width:100px;" autocomplete="off" value="" placeholder="0.00" onKeyUp="getQtyLocalFreightCal();"/></td><td><input type="text" name="txtFreightQty_'+id+'" id="txtFreightQty_'+id+'" class="input-text" style="width:100px;" value="" placeholder="0.00" onKeyUp="getQtyLocalFreightCal();"/></td><td><input type="text" name="txtQtyFreight_'+id+'" id="txtQtyFreight_'+id+'" class="input-text" style="width:100px;" value="0" readonly/></td><td><input type="text" name="txtQtyBrokeragePer_'+id+'" id="txtQtyBrokeragePer_'+id+'" style="width:100px;" value="" onKeyUp="getQtyLocalFreightCal();" class="input-text" placeholder="0.00" /></td><td><input type="text" name="txtQtyBrokerageAmt_'+id+'" id="txtQtyBrokerageAmt_'+id+'" class="input-text" style="width:100px;" readonly value="" placeholder="0.00" /></td><td><input type="text" name="txtQtyNetFreight_'+id+'" style="width:100px;" id="txtQtyNetFreight_'+id+'" placeholder="0.00" class="input-text" readonly value="" /></td><td><input type="text"  name="txtQtyNetFreightMT_'+id+'" id="txtQtyNetFreightMT_'+id+'" class="input-text" style="width:100px;" readonly value="0.00" /></td></tr>').appendTo("#tblQtyFreight");
		$("#selQtyVendorList_"+id).html($("#selVendor").html());

		$("#txtQTYID").val(id);
		$("[id^=txtQtyAggriedFreight_], [id^=txtFreightQty_], [id^=txtQtyBrokeragePer_], [id^=txtQtyLocalAggriedFreight_], [id^=txtDisExchangeRate_], [id^=txtMarLocalAggriedFreight_], [id^=txtMarExchangeRate_]").numeric();
	}
	else
	{
		jAlert('Please fill All details', 'Alert');
		return false;
	}
	getFinalCalculation();
}


function deleteQtyVendorDetails(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#tbrQtyVRow_"+var1).remove();
					$("#tbrQtyVRow1_"+var1).remove();
					$("#tbrQtyVRow2_"+var1).remove();
					getQtyLocalFreightCal();	
				 }
			else{return false;}
			});
}


function getQtyLocalFreightCal()
{
	var id = $("#txtQTYID").val();
	for(var i=1;i<=id;i++)
	{
		if($("#txtQtyLocalAggriedFreight_"+i).val() == ""){var localfreightcost = 0;}else{var localfreightcost = $("#txtQtyLocalAggriedFreight_"+i).val();}
		if($("#txtDisExchangeRate_"+i).val() == ""){var txtDisExchangeRate = 0;}else{var txtDisExchangeRate = $("#txtDisExchangeRate_"+i).val();}
		var calmul = parseFloat(localfreightcost)/parseFloat(txtDisExchangeRate);
		if(isNaN(calmul) || calmul=="" || calmul=="Infinity"){calmul = 0;}
		$("#txtQtyAggriedFreight_"+i).val(calmul.toFixed(2));
		if($("#txtFreightQty_"+i).val() == ""){var txtFreightQty = 0;}else{var txtFreightQty = $("#txtFreightQty_"+i).val();}
		var calmul2 = parseFloat(calmul)*parseFloat(txtFreightQty);
		if(isNaN(calmul2) || calmul2=="" || calmul=="Infinity"){calmul2 =0;}
		$("#txtQtyFreight_"+i).val(calmul2.toFixed(2));
				
		if($("#txtQtyBrokeragePer_"+i).val() == ""){var brokrage = 0;}else{var brokrage = $("#txtQtyBrokeragePer_"+i).val();}
		var brokerage_usd = (parseFloat(calmul2) * parseFloat(brokrage))/100;
		$("#txtQtyBrokerageAmt_"+i).val(brokerage_usd.toFixed(2));
		var diff = $("#txtQtyFreight_"+i).val() - $("#txtQtyBrokerageAmt_"+i).val();
		if(diff == "" || isNaN(diff) || diff=="Infinity"){var diff = 0;}
		$("#txtQtyNetFreight_"+i).val(diff.toFixed(2));
		var qtyMT = parseFloat(diff) / parseFloat(calmul2);
		if(qtyMT == "" || isNaN(qtyMT) || qtyMT=="Infinity"){var qtyMT = 0;}
		$("#txtQtyNetFreightMT_"+i).val(qtyMT.toFixed(2));
	}
	$("#txtTotalFreightQty").val($("[id^=txtFreightQty_]").sum().toFixed(2));
	$("#txtTotalQtyFreight").val($("[id^=txtQtyFreight_]").sum().toFixed(2));
	$("#txtTotalNetBrokerage").val($("[id^=txtQtyBrokerageAmt_]").sum().toFixed(2));
	$("#txtTotalNetFreight").val($("[id^=txtQtyNetFreight_]").sum().toFixed(2));
	getFinalCalculation();
}

function getMarLocalFreightCal()
{
	if($("#txtMarLocalAggriedFreight").val() == ""){var localfreightcost = 0;}else{var localfreightcost = $("#txtMarLocalAggriedFreight").val();}
	if($("#txtMarExchangeRate").val() == ""){var txtMarExchangeRate = 0;}else{var txtMarExchangeRate = $("#txtMarExchangeRate").val();}
	var calmul = parseFloat(localfreightcost)/parseFloat(txtMarExchangeRate);
	if(isNaN(calmul) || calmul=="" || calmul=="Infinity"){calmul = 0;}
	$("#txtMTCPDRate").val(calmul.toFixed(2));
	getFinalCalculation();
}

function getSubmitForm()
{
	$("#action").val("");
	document.frm1.submit();
}

function showGasMarket(value)
{
	
	if(value == 2)
	{
		$("#txtGASLumpsum").removeAttr('disabled');
		$("#txtAdnlPrenium,#txtBalticRate,#txtBaseRate").attr('disabled',true);
		$("#txtBalticRate,#txtBaseRate,#txtAdnlPrenium").val("");
		$("#txtBalticRate").focus();
		getFinalCalculation();
	}
	else
	{
		$("#txtGASLumpsum").val("");
	}
	if(value == 1)
	{			
		$("#txtGASLumpsum").attr('disabled',true);
		$("#txtAdnlPrenium,#txtBalticRate,#txtBaseRate").removeAttr('disabled');
		$("#txtGASLumpsum").val("");
		$("#txtGASLumpsum").focus();
		getFinalCalculation();
	}
	else
	{
	    $("#txtBalticRate,#txtBaseRate,#txtAdnlPrenium").val("");	
	}
}


function addTankDetails()
{
	var id = $("#txtTankID").val();
	if($("#txtFreightSpecs_"+id).val() != "" )
	{
		$("#tbrQtyVRow_empty,#tbrQtyVRow_empty1,#tbrQtyVRow_empty2").remove();
		id = (id - 1) + 2;
		$('<tr id="tankRow1_'+id+'"><td><a href="#tb'+id+'" onclick="deleteTankerDetails('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td>Freight Specs</td><td><input type="text" name="txtFreightSpecs_'+id+'" id="txtFreightSpecs_'+id+'" class="input-text" autocomplete="off" size="30" value="" placeholder="Freight Specs" onKeyUp="getFinalCalculation();"/></td><td>Flat Rate</td><td>WS</td><td>Distance Leg</td><td>Total Distance</td><td>Amount</td><td>Customer</td></tr><tr id="tankRow2_'+id+'"><td></td><td>Min Cargo Qty</td><td><input type="text" name="txtMinCargoQty_'+id+'" id="txtMinCargoQty_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text" name="txtMinFlatRateQty_'+id+'" id="txtMinFlatRateQty_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text" name="txtMinWSQty_'+id+'" id="txtMinWSQty_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text" name="txtMinDisLeg_'+id+'" id="txtMinDisLeg_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text" name="txtMinTotalDis_'+id+'" id="txtMinTotalDis_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text" name="txtMinAmount_'+id+'" id="txtMinAmount_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><select  name="selTankVendor_'+id+'" class="input-text" style="width:120px;" id="selTankVendor_'+id+'"></select></td></tr><tr id="tankRow3_'+id+'"><td></td><td>Overage Qty</td><td><input type="text" name="txtOveCargoQty_'+id+'" id="txtOveCargoQty_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text" name="txtOveFlatRateQty_'+id+'" id="txtOveFlatRateQty_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text" name="txtOveWSQty_'+id+'" id="txtOveWSQty_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text" name="txtOveDisLeg_'+id+'" id="txtOveDisLeg_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text" name="txtOveTotalDis_'+id+'" id="txtOveTotalDis_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text" name="txtOveAmount_'+id+'" id="txtOveAmount_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" onKeyUp="getFinalCalculation();"/></td><td></td></tr><tr id="tankRow4_'+id+'"><td></td><td>Total Cargo Qty</td><td><input type="text" name="txtTotalTankQty_'+id+'" id="txtTotalTankQty_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" readonly/></td><td></td><td></td><td></td><td></td><td><input type="text" name="txtTotalTankAmount_'+id+'" id="txtTotalTankAmount_'+id+'" class="input-text" autocomplete="off" size="13" value="" placeholder="0.00" readonly/></td><td></td></tr><tr id="tankRow5_'+id+'"><td colspan="9">&nbsp;</td></tr>').appendTo("#tblTankerBody");
		$("#txtMinTotalDis_"+id+",#txtOveTotalDis_"+id).val($("#txtTDist").val());
		$("#txtTankID").val(id);
		$("[id^=txtMinCargoQty_],[id^=txtMinFlatRateQty_],[id^=txtMinWSQty_],[id^=txtMinDisLeg_],[id^=txtMinTotalDis_],[id^=txtMinAmount_],[id^=txtMinCargoQty_],[id^=txtOveFlatRateQty_],[id^=txtOveWSQty_],[id^=txtOveTotalDis_],[id^=txtOveAmount_],[id^=txtTotalTankQty_],[id^=txtOveDisLeg_]").numeric();
		$("#selTankVendor_"+id).html($("#selVendor").html());
	}
	else
	{
		jAlert('Please fill All details', 'Alert');
		return false;
	}
	getFinalCalculation();
}


function deleteTankerDetails(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#tankRow1_"+var1).remove();
					$("#tankRow2_"+var1).remove();
					$("#tankRow3_"+var1).remove();
					$("#tankRow4_"+var1).remove();
					$("#tankRow5_"+var1).remove();
					getQtyLocalFreightCal();	
				 }
			else{return false;}
			});
}

function addOtherInRow()
{
	var id = $("#txtOthInCount").val();
	if($("#txtDesc_"+id).val() != "" && $("#txtOtherAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="OthInRow_'+id+'"><td><a href="#tb1" onClick="removeOtherIncome('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><input type="text" name="txtDesc_'+id+'" id="txtDesc_'+id+'" class="input-text" autocomplete="off" value="" placeholder="Description" size="29"/></td><td><input type="text" name="txtOtherAmt_'+id+'" id="txtOtherAmt_'+id+'" onKeyUp="getFinalCalculation();" class="input-text" autocomplete="off" value="" placeholder="0.00" size="12"/></td><td><input type="text" name="txtOtherAddComm_'+id+'" id="txtOtherAddComm_'+id+'" onKeyUp="getFinalCalculation();" class="input-text" autocomplete="off" value="" placeholder="0.00" size="12"/></td><td><input type="text" name="txtOtherNetAmt_'+id+'" id="txtOtherNetAmt_'+id+'"  class="input-text" autocomplete="off" size="12" value="0.00"  readonly /></td><td><select name="selOtherIncVendor_'+id+'" class="input-text" style="width:100px;" id="selOtherIncVendor_'+id+'"></select></td></tr>').appendTo("#OthInBody");
		$("[id^=txtOtherAmt_],[id^=txtOtherAddComm_]").numeric();	
		$("#selOtherIncVendor_"+id).html($("#selVendor").html());	
		$("#txtOthInCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeOtherIncome(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#OthInRow_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}

function Del_Upload1(var1)
{
	jConfirm('Are you sure you want to remove this attachment permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_file1_"+var1).remove();	
		 }
	else{return false;}
	});
}

function Del_Upload(var1)
{
	jConfirm('Are you sure you want to remove this attachment permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_file_"+var1).remove();	
		 }
	else{return false;}
	});
}


function addBunkerRow()
{
	var id = $("#txtBunkerCount").val();
	if($("#txtBunkerQty_"+id).val() != "" && $("#selBunker_"+id).val() != "" && $("#txtBunkerPrice_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="DivBunkeractual_'+id+'"><td><a href="#tbb'+id+'" onclick="removeBunkerRows('+id+');"><i class="fa fa-times" style="color:red;"></i></a></td><td><select name="selBunker_'+id+'" id="selBunker_'+id+'" style="width:90px;" class="input-text"><?php $obj->getBunkerList();?></select></td><td><input type="text" name="txtBunkerQty_'+id+'" id="txtBunkerQty_'+id+'" style="width:90px;" class="input-text numeric" onkeyup="getBunkerCalculation();" placeholder="0.00" autocomplete="off" value=""></td><td><input type="text" name="txtBunkerPrice_'+id+'" id="txtBunkerPrice_'+id+'" style="width:90px;" class="input-text numeric" onkeyup="getBunkerCalculation();" placeholder="0.00" autocomplete="off" value=""></td><td><input type="text" name="txtBunkerAmt_'+id+'" id="txtBunkerAmt_'+id+'" style="width:90px;" class="input-text" placeholder="0.00" readonly="" autocomplete="off" value=""></td><td><select name="selBunkerPort_'+id+'" id="selBunkerPort_'+id+'" style="width:100px;" class="input-text"></select></td><td><select name="selBunkerVendor_'+id+'" id="selBunkerVendor_'+id+'" style="width:120px;" class="input-text"></select></td></tr>').appendTo("#bunkerTBody");
		$("#txtBunkerCount").val(id);
		$("#selBunkerVendor_"+id).html($("#selVendor").html());
		$("#selBunkerPort_"+id).html($("#selPort").html());
		$(".numeric").numeric();	
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeBunkerRows(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#DivBunkeractual_"+var1).remove();
				getBunkerCalculation();			
			 }
		else{return false;}
		});
}


function getCargoPlanningdata()
{
	if($("#selCargoPlanning").val()!="")
	  {
		  $.post("options.php?id=72",{selCargoPlanning:""+$("#selCargoPlanning").val()+""}, function(data) 
			{
				var vsldata = JSON.parse(data);
				$('#txtCID').val(vsldata['CPID']);
				$('#selShipperCP').val(vsldata['SHIPPER']);
				$('#selChartererCP').val(vsldata['CHARTERER']);
				$('#selOwnerCP').val(vsldata['OWNER']);
				$('#selReceiverCP').val(vsldata['RECEAVER']);
				$('#selCargoCP').val(vsldata['CARGO']);
				$('#txtQtyCP').val(vsldata['CARGOSTEAMSIZE']);
				$('#txtToleranceCP').val(vsldata['TOLERANCE']);
				$('#txtBaseFreightCP').val(vsldata['BASE_FREIGHT']);
				$('#selPlannintTypeCP').val(vsldata['COA_SPOT']);
				$('#txtCOADateCP').val(vsldata['COA_DATE']);
				$('#selBaseInCP').val(vsldata['BASINID']);
				$('#selBunkerHedgeCP').val(vsldata['BUNKER_HEDGE']);
				$('#selLPortCP').val(vsldata['LOAD_PORT']);
				$('#selDPortCP').val(vsldata['DISCHARGE_PORT']);
				$('#txtLCSDateCP').val(vsldata['LAYCAN_SDATE']);
				$('#txtLCFDateCP').val(vsldata['LAYCAN_EDATE']);
				$('#selCargoReletCP').val(vsldata['CARGO_RELET']);
				$('#txtNomClauseCP').val(vsldata['NOMCLAUSE']);
				$('#txtRemarksCP').val(vsldata['REMARKS']);
			});  
	  }
	  else
	  {
		  $('#txtCID, #selShipperCP, #selChartererCP, #selOwnerCP, #selReceiverCP, #selCargoCP, #txtQtyCP, #txtToleranceCP, #selPlannintTypeCP, #txtCOADateCP, #selBaseInCP, #selBunkerHedgeCP, #selLPortCP, #selDPortCP, #txtLCSDateCP, #txtLCFDateCP, #selCargoReletCP, #txtNomClauseCP, #txtRemarksCP').val("");
	  }
}


function getIntakeCalculation()
{
	var txtAllawedDraftM  = parseFloat($("#txtAllawedDraftM").val());if(isNaN(txtAllawedDraftM)){txtAllawedDraftM = 0;}
	var txtSDraftM        = parseFloat($("#txtSDraftM").val());if(isNaN(txtSDraftM)){txtSDraftM = 0;}
	var txtSDWTMT         = parseFloat($("#txtSDWTMT").val());if(isNaN(txtSDWTMT)){txtSDWTMT = 0;}
	var txtTPCMT          = parseFloat($("#txtTPCMT").val());if(isNaN(txtTPCMT)){txtTPCMT = 0;}
	var txtConstantsMT    = parseFloat($("#txtConstantsMT").val());if(isNaN(txtConstantsMT)){txtConstantsMT = 0;}
	var txtBunkerRobMT    = parseFloat($("#txtBunkerRobMT").val());if(isNaN(txtBunkerRobMT)){txtBunkerRobMT = 0;}
	
	var diffdrafts = parseFloat(txtSDraftM) - parseFloat(txtAllawedDraftM);if(isNaN(diffdrafts)){diffdrafts = 0;}
	var diffdraftsmt = parseFloat(diffdrafts)*100;if(isNaN(diffdraftsmt)){diffdraftsmt = 0;}
	
	var DWTdiffmt = parseFloat(diffdraftsmt)*parseFloat(txtTPCMT);if(isNaN(DWTdiffmt)){DWTdiffmt = 0;}
	
	var cargointakemt = parseFloat(txtSDWTMT) - parseFloat(DWTdiffmt) - parseFloat(txtBunkerRobMT) - parseFloat(txtConstantsMT);if(isNaN(cargointakemt)){cargointakemt = 0;}
	if(txtAllawedDraftM == 0 || txtAllawedDraftM=='')
	{
		$("#txtCargoIntakeMT").val(0);
		$("#txtCQMT").val($('#txtQtyCP').val());
		
	}
	else
	{
		$("#txtCargoIntakeMT,#txtCQMT").val(cargointakemt.toFixed(2));
	}
}
</script>
    </body>
</html>