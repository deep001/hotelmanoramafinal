<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
if (isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$selBType = $_REQUEST['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	$selBType = 1;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(7,2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-bar-chart-o"></i>&nbsp;Reports&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li>Reports</li>
						<li>Operations</li>
						<li class="active"> Daily Position Report</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div class="box box-primary">
					<div class="row invoice-info" style="margin-top:10px;margin-left:0px;">
                        <div class="col-xs-3">
                             Business Type
                             <select name="selBType" id="selBType" class="form-control" >
                               <?php echo $obj->getBusinessTypeList1($selBType);?>
                              </select>                  
                        </div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							&nbsp;
                            <address>
								<button class="btn btn-info btn-flat" id="inner-login-button" type="button" onclick="getData()">Search</button>
							</address>
						</div>
                        <div class="col-sm-6 invoice-col">
                        <br/>
                            <div style="text-align:right">
                                <!--<a href="#" title="Pdf" onClick="getExcel();"><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i>Generate Excel</button></a>-->
                                <a href="#" title="Pdf" onClick="getPdf();"><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i>Generate PDF</button></a>
                            </div><!-- /.col -->
                        </div>
					</div>
					

					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                    	<div id="COA_report">
						
                        </div>
					</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#DailyReport").dataTable();
    getData();
});

function getPdf()
{
	location.href='allPdf.php?id=55&selBType='+$("#selBType").val();
}

function getExcel()
{
	location.href='allExcel.php?id=8&selBType='+$("#selBType").val();
}

function getData()
{
	$("#COA_report").html("");
	$("#COA_report").html('<span style="text-align:center;display:block;"><img src="../../img/loading.gif" /></span>');
	$.post("options.php?id=57",{selBType:""+$("#selBType").val()+""}, function(html) {
	$("#COA_report").html("");
	$("#COA_report").html(html);
	});
}
</script>
		
</body>
</html>