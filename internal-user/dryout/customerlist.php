<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
//$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

/* get customer list from data base code start here */
$getcustomerlist = $obj->getCustomerDetail();
/* customer list code end here */







/*if($_POST['search'] == 'search')
{
    $name = $_POST['name'];
	$phoneno = $_POST['phoneno'];
	$myquery = mysql_query("SELECT * FROM `customerlist` WHERE  `name` LIKE '$name%' OR contactno = '$phoneno' order by customerid DESC");
	$num1 = mysql_num_rows($myquery);

		
} */
$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
$rigts    = explode(',',$obj->getUserRights($_SESSION['uid'],$_SESSION['moduleid'],18)); 
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

<?php $display->css(); ?>
<?php $display->js(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(18); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Customer list</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
	-				<?php  if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Cargo Tonnage added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating Cargo Tonnage.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }}  ?>
					
				<div class="box box-primary">
				<h3 style=" text-align:center;">Customer list</h3>
					<form name="frmsearch" id="frmsearch" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>
					  <div class="row invoice-info">
						  <div class="col-sm-3 invoice-col">
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							  &nbsp;&nbsp;&nbsp;&nbsp;
							  &nbsp;&nbsp;
							  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							  &nbsp;&nbsp;&nbsp;&nbsp;
							  &nbsp;&nbsp;
							  &nbsp;<b>Search By:</b>
                           
                        </div>
                        <div class="col-sm-3 invoice-col">
                         Name
                            <address>
                           <input type="text"  name="name"  class="form-control required" id="name" value="" placeholder="customer name" />
							</address>
                        </div>
						  <div class="col-sm-3 invoice-col">
                        Phone No
                            <address>
                           <input type="text"  name="phoneno"  class="form-control required" id="phoneno" value="" placeholder="phone no" />
							</address>
                        </div>
						  <div class="col-sm-3 invoice-col">
							  </br>
                            <address>
                           <input type="submit" name="search" id="search" value="search" class="btn btn-primary btn-flat"/>
							</address>
                        </div>
					</div>
					</form>
				<?php if(in_array(2, $rigts)){?><div align="right"><a href="addCustomers.php" title="Add New"><button class="btn btn-info btn-flat">Add New</button></a>&nbsp;&nbsp;&nbsp;&nbsp;</div><?php }?>
			
                 
			<!--	<div style="height:10px;">
				<input type="hidden" name="action" value="submit" /><input type="hidden" name="txtCargoID" id="txtCargoID" value="" /><input type="hidden" name="txtStatus" id="txtStatus" value="" />
				</div>		
                -->
                <div class="box-body table-responsive panel-body" style="overflow:auto;">
                     <table id="cargo_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th align="left" valign="top" width="9%"><input type="checkbox" name="allcustomer" class="allcustomer" id="allcustomer" value="" ></th>
								<th align="left" valign="top" width="9%">Sno</th>
                                <th align="left" valign="top" width="10%">Customer Name</th>
                                <th align="left" valign="top" width="10%">Fathername</th>
                                <th align="left" valign="top" width="8%">Mothername</th>
                                <th align="left" valign="top" width="8%">DOB</th>
                                <th align="left" valign="top" width="8%">Date of Visit</th>
                                <th align="left" valign="top" width="8%">Check in Time</th>
                                <th align="left" valign="top" width="10%">Checkout Time</th>
                                <th align="left" valign="top" width="10%">Aadhar <br/>Card</th>
                                <th align="left" valign="top" width="7%">Voter<br/>Card</th>
                                <th align="left" valign="top" width="8%">Pen<br/>Card</th>
                                <th align="left" valign="top" style="min-width:50px;">Profile<br>Photo</th>
                                <th align="left" valign="top" width="8%">View Detail</th>
                              

                            </tr>
                        </thead>
						<tbody>
                        <?php  /* if($num1>0){
	                     $sn=1;while($searchlist = mysql_fetch_assoc($myquery))
						{ 
                        echo  '<tr class="rowbackground_'.$searchlist['customerid'].'"><td align="left" valign="top" width="10%"><input type="checkbox" name="customer[]" class="myChecknew" id="myChecknew_'.$searchlist['customerid'].'" value="'.$searchlist['customerid'].'" onClick="checkhighlight('.$searchlist['customerid'].');"></td>
						 <td align="left" valign="top" width="9%">'.$sn++.'</td>
						 <td align="left" valign="top" width="10%">'.$searchlist['name'].'</td>
						 <td align="left" valign="top" width="10%">'.$searchlist['fathername'].'</td>
						 <td align="left" valign="top" width="8%">'.$searchlist['motherame'].'</td>
						 <td align="left" valign="top" width="8%">'.$searchlist['DOB'].'</td>
						 <td align="left" valign="top" width="8%">'.$searchlist['Dateofvisit'].'</td>
						 <td align="left" valign="top" width="8%">'.date("g:i a",$searchlist['checkintime']).'</td>
						 <td align="left" valign="top" width="10%">'.date("g:i a",$searchlist['checkouttime']).'</td>
						 <td align="left" valign="top" width="10%"><a href="../../attachment/aadhar/'.$searchlist['aadharcard'].'" target="_blank"><img src="../../attachment/aadhar/'.$searchlist['aadharcard'].'" style="width:50px;height:50px;"></a></td>
						 <td align="left" valign="top" width="7%"><a href="../../attachment/aadhar/'.$searchlist['voterid'].'" target="_blank"><img src="../../attachment/aadhar/'.$searchlist['voterid'].'" style="width:50px;height:50px;"></a></td>
						 <td align="left" valign="top" width="8%"><a href="../../attachment/aadhar/'.$searchlist['pencard'].'" target="_blank"><img src="../../attachment/aadhar/'.$searchlist['pencard'].'" style="width:50px;height:50px;"></a></td>
						 <td align="left" valign="top" style="min-width:50px;"><a href="../../attachment/aadhar/'.$searchlist['profilephoto'].'" target="_blank"><img src="../../attachment/aadhar/'.$searchlist['profilephoto'].'" style="width:50px;height:50px;"></a></td>
						 <td align="left" valign="top" width="8%"><a href="customercompletedetail.php?customerid='.base64_encode($searchlist['customerid']).'" target="_blank">View Detail</a></td>
						';
						}
                            }
							else
							{ */
						
						$sn=1;while($customerdetaillist = mysql_fetch_assoc($getcustomerlist))
						{ 
                        echo  '<tr class="rowbackground_'.$customerdetaillist['customerid'].'"><td align="left" valign="top" width="10%"><input type="checkbox" name="customer[]" class="myChecknew" id="myChecknew_'.$customerdetaillist['customerid'].'" value="'.$customerdetaillist['customerid'].'" onClick="checkhighlight('.$customerdetaillist['customerid'].');"></td>
						 <td align="left" valign="top" width="9%">'.$sn++.'</td>
						 <td align="left" valign="top" width="10%">'.$customerdetaillist['name'].'</td>
						 <td align="left" valign="top" width="10%">'.$customerdetaillist['fathername'].'</td>
						 <td align="left" valign="top" width="8%">'.$customerdetaillist['motherame'].'</td>
						 <td align="left" valign="top" width="8%">'.$customerdetaillist['DOB'].'</td>
						 <td align="left" valign="top" width="8%">'.$customerdetaillist['Dateofvisit'].'</td>
						 <td align="left" valign="top" width="8%">'.date("g:i a",$customerdetaillist['checkintime']).'</td>
						 <td align="left" valign="top" width="10%">'.date("g:i a",$customerdetaillist['checkouttime']).'</td>
						 <td align="left" valign="top" width="10%"><a href="../../attachment/aadhar/'.$customerdetaillist['aadharcard'].'" target="_blank"><img src="../../attachment/aadhar/'.$customerdetaillist['aadharcard'].'" style="width:50px;height:50px;"></a></td>
						 <td align="left" valign="top" width="7%"><a href="../../attachment/aadhar/'.$customerdetaillist['voterid'].'" target="_blank"><img src="../../attachment/aadhar/'.$customerdetaillist['voterid'].'" style="width:50px;height:50px;"></a></td>
						 <td align="left" valign="top" width="8%"><a href="../../attachment/aadhar/'.$customerdetaillist['pencard'].'" target="_blank"><img src="../../attachment/aadhar/'.$customerdetaillist['pencard'].'" style="width:50px;height:50px;"></a></td>
						 <td align="left" valign="top" style="min-width:50px;"><a href="../../attachment/aadhar/'.$customerdetaillist['profilephoto'].'" target="_blank"><img src="../../attachment/aadhar/'.$customerdetaillist['profilephoto'].'" style="width:50px;height:50px;"></a></td>
						 <td align="left" valign="top" width="8%"><a href="customercompletedetail.php?customerid='.base64_encode($customerdetaillist['customerid']).'" target="_blank">View Detail</a></td>
						 ';
						}
						/*	} */
						?>
						</tbody>
                    </table>
            	</div>
			<!-- dtuu start here -->
				<div id="tabs-2" style="display:none;">
					
				</div>
			<!-- dtuu end here -->
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
	  <script src="../../js/jquery-3.4.1.min.js" type="text/javascript"></script>
<!--	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
<script type="text/javascript">
	
$(document).ready(function(){
	$("#search").click(function(){
		var name = $("#name").val();
		var phoneno = $("#phoneno").val();
	//	alert("serching suggestion name is"+name);

$.ajax({
        type:"POST",
       url:"searchsubmit.php",
         data:$("#frmsearch").serialize(),
         success: function(result){
            $( ".panel-body" ).hide(result);
            $( "#tabs-2" ).html(result).show();   
        }
    });

return false;

	});
});
$(document).ready(function(){
	
	$('.allcustomer').click(function(){
		
		if ($('.allcustomer').is(':checked')) {
			$('.myChecknew').prop('checked', true);
		} else {
			$('.myChecknew').prop('checked', false);
		}
		
	
	});
});
	
	
	function checkhighlight(var1)
	{   
		if($("#myChecknew_"+var1).is(':checked')){
	    $(".rowbackground_"+var1).css("background", "#f0ad4e");
		}else {
			$(".rowbackground_"+var1).css("background", "white");
		}
	}
</script>
	
</body>
</html>