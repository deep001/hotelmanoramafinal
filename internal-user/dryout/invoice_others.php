<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$idd       = $_REQUEST['id'];
$id        = explode(",",$_REQUEST['id']);
$comid     = $id[0];
$vendorid  = $id[2];
$fcaid     = $id[1];
$name      = $_REQUEST['name'];
$amounttitle      = $_REQUEST['amounttitle'];
$page      = $_REQUEST['page'];
if($page == 1){$page_link = "payment_grid.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "payment_grid.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "payment_grid.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "payment_gridcoa.php"; $page_bar="In Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else if($page == 5){$page_link = "payment_gridcoa.php"; $page_bar="In Post Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else{$page_link = "payment_gridcoa.php"; $page_bar="Vessels in History - COA";$nav=16;$subtitle = 'COA';}

if(@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertOtherInvoiceDetails();
	$msg = explode(",",$msg);
	header('Location:./invoice_others.php?msg='.$msg[0].'&comid='.$msg[1].'&page='.$page.'&id='.$_REQUEST['id'].'&name='.$_REQUEST['name'].'&amounttitle='.$_REQUEST['amounttitle']);
}

if (@$_REQUEST['action2'] == 'submit1')
{
	$msg = $obj->insertOtherInvoiceReceivedDetails();
	$msg = explode("_",$msg);
	header('Location:./invoice_others.php?msg='.$msg[0].'&comid='.$msg[1].'&page='.$page.'&id='.$_REQUEST['id'].'&name='.$_REQUEST['name'].'&amounttitle='.$_REQUEST['amounttitle']);
}

if (@$_REQUEST['action1'] == 'submit2')
{
	$msg = $obj->deletefreightInvoiceRecords();
	$msg = explode(",",$msg);
	header('Location:./invoice_others.php?msg='.$msg[0].'&comid='.$msg[1].'&page='.$page.'&id='.$_REQUEST['id'].'&name='.$_REQUEST['name'].'&amounttitle='.$_REQUEST['amounttitle']);
}

$fcaid = $obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($fcaid);
$arr       = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$fcaid);

$lp_name = array(); 
$dp_name = array();
$discharge_port = "";

for($j=0;$j<count($arr);$j++)
{   
    $arr_explode = explode('@@',$arr[$j]);
	if($arr_explode[0] == "LP"){$lp_name[]= $obj->getPortNameBasedOnID($arr_explode[1]);}
	else{$dp_name[]= $obj->getPortNameBasedOnID($arr_explode[1]);}
}


$lp_name  = implode(', ',$lp_name);
$dp_name  = implode(', ',$dp_name);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&name=".$name."&page=".$page."&id=".$idd.'&amounttitle='.$_REQUEST['amounttitle'];

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];

if($obj->getFun175()!='')
{
	$sqlcoa = "select * from coa_master where COAID='".$obj->getFun175()."'";
	$resultcoa = mysql_query($sqlcoa) or die($sqlcoa);
	$rowscoa   = mysql_fetch_assoc($resultcoa);
	$currency = $rowscoa['CURRENCY'];
}else
{
	$currency = 'USD';
}

$rights = $obj->getUserRights($uid,$moduleid,$linkid);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
	$(".autosize").autosize({append: "\n"});
	$("#frm1").validate({
		rules: {
	    selFromOwner: {required: true},
		selIType: {required: true},
		txtDNote: {required: true},
		txtDate: {required: true},
		txtDueDate:{required: true},
		txtExchangeRate:{required: true},
		selExchangeCurrency:{required: true},
		txtPaymentTerms:{required: true},
		selNOB:{required: true},
		txtDesc:{required: true}
		},
	messages: {
		selIType: {required: "*"},
		txtDNote: {required: true},
		txtDate: {required: true},
		txtDueDate:{required: "*"},
		txtExchangeRate:{required: "*"},
		selExchangeCurrency:{required: "*"},
		txtPaymentTerms:{required: "*"},
		selNOB:{required: "*"}
		},
	submitHandler: function(form)  {
		    if($('#txtStatus').val() == 0)
			{
				jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
				$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
				$("#popup_content").css({"background":"none","text-align":"center"});
				$("#popup_ok,#popup_title").hide();  
				form.submit();
			}
			else
			{
				jConfirm('Are you sure you want to "Submit & Close"?', 'Confirmation', function(r) {
					if(r)
					{
						jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
						$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
						$("#popup_content").css({"background":"none","text-align":"center"});
						$("#popup_ok,#popup_title").hide();  
						form.submit();
					}
				});
			}
		}
	});
$("#txtTO,[id^=txtAddDesAmt_],[id^=txtSubDesAmt_],#txtAddComm,#txtExchangeRate,#txtBrokerage").numeric();

$('#txtRemarks').autosize({append: "\n"});

$('#txtDate,#txtDueDate,#txtExchangeDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose:true
});



getBankingDetailsData();
getAmountThereOff();
});


function AddNewAddRow()
{
	var id = $("#txtAddID").val();
	if($("#txtAddDes_"+id).val() != "" && $("#txtAddDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="add_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtAddDes_'+id+'" id="txtAddDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtAddDesAmt_'+id+'" id="txtAddDesAmt_'+id+'" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getAmountThereOff();"/></td></tr>').appendTo("#tblAdd");
		$("#txtAddID").val(id);
		$("[id^=txtAddDesAmt_]").numeric();	
	}
}


function removeAddRow(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#add_Row_"+var1).remove();
					getAmountThereOff();
				 }
			else{return false;}
			});
}



function SubNewSubRow()
{
	var id = $("#txtSubID").val();
	if($("#txtSubDes_"+id).val() != "" && $("#txtSubDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="Sub_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtSubDes_'+id+'" id="txtSubDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtSubDesAmt_'+id+'" id="txtSubDesAmt_'+id+'" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getAmountThereOff();"/></td></tr>').appendTo("#tblSub");
		$("#txtSubID").val(id);
		$("[id^=txtSubDesAmt_]").numeric();	
	}
}


function removeSubRow(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#Sub_Row_"+var1).remove();
				getAmountThereOff();
			 }
		else{return false;}
		});
}

function getBankingDetailsData()
{   
    var1 = $('#selNOB').val();
	if($("#selNOB").val()!="")
	{
		$.post("options.php?id=43",{selNOB:""+$("#selNOB").val()+""}, function(html) {
			if(html!="")
			{
				$("#bankingrow").show();
				var res = html.split("@@@@");
				$('#span_1_0').text(res[0]);
				$('#span_1_1').text(res[1]);
				$('#span_1_2').text(res[2]);
				$('#span_1_3').text(res[3]);
				$('#span_1_4').text(res[4]);
				$('#span_1_5').text(res[5]);
				$('#span_1_6').text(res[6]);
				$('#span_1_7').text(res[7]);
				$('#span_1_8').text(res[8]);
				$('#span_1_9').text(res[9]);
				$('#span_1_10').text(res[10]);
			}
		});
	}
	else
	{
	    $("#bankingrow").hide();	
		$('#span_1_0').text("");
	    $('#span_1_1').text("");
		$('#span_1_2').text("");
		$('#span_1_3').text("");
		$('#span_1_4').text("");
		$('#span_1_5').text("");
		$('#span_1_6').text("");
		$('#span_1_7').text("");
		$('#span_1_8').text("");
		$('#span_1_9').text("");
		$('#span_1_10').text("");
	}
}




function getAmountThereOff()
{ 
    var add1 = parseFloat($("[id^=txtAddDesAmt_]").sum());
	if(isNaN(add1)){add1 = 0;}
	var sub1 = parseFloat($("[id^=txtSubDesAmt_]").sum());
	if(isNaN(sub1)){sub1 = 0;}	
	if($('#txtInvAmount').val()=="" || isNaN($('#txtInvAmount').val())){var invamount = 0;}else{var invamount = $("#txtInvAmount").val();}
	var var3 = parseFloat(invamount);
	if(isNaN(var3)){var3 = 0;}
	
	var var2 = parseFloat(var3) + parseFloat(add1) - parseFloat(sub1);
	
	if(isNaN(var2)){var2 = 0;}
	$('#txtNetAmtPayable').val(parseFloat(var2).toFixed(2));
	
}

function getValidate(var1)
{
	$('#txtStatus').val(var1);
}

function getDelete(var1)
{
	jConfirm('Are you sure you want to Delete this Entry?', 'Confirmation', function(r) {
	if(r)
	{
		$('#action1').val('submit2');
		$('#txtDEL_ID').val(var1);
		document.frm2.submit();
	}
	else
	{
		return false;
	}
	});
}

function openWin(var1)
{
	$("#divPayment").empty();
	$("#divPayment").html('<div><img src="../../img/ajax-loader.gif" /><br><b>Loading...</b></div>'); 
	$.post("options.php?id=46",{invoiceid:""+var1+""}, function(html) {
		$("#divPayment").empty();
		$("#divPayment").append(html);
		
		$("#txtP_PR").numeric();
        $('#txtP_Remarks').autosize({append: "\n"});
		$('#txtP_Date').datepicker({
			format: 'dd-mm-yyyy',
			autoclose:true
		});
	});
}

function onkeyUp1()
{
	if(parseFloat($('#txtP_PR').val()) > parseFloat($('#txtP_Amt').val()))
	{
		jAlert('Payment Received is more than Amount', 'Alert');
		$('#txtP_PR').val(0.00);
	}
}


function getValid()
{
	if($('#txtP_PR').val() == '' || $('#txtP_Date').val() =='' || $('#txtP_Remarks').val()=='')
	{
		jAlert('Please fill the Payment Received & Date & Remarks', 'Alert');
		return false;
	}
	else
	{
		var file_temp_name = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
	    $('#txtCRMFILE1').val(file_temp_name);
	    var file_actual_name = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
	    $('#txtCRMNAME1').val(file_actual_name);
		return true;
	}
}

function getPdf(var1)
{
	location.href='allPdf.php?id=52&im_id='+var1;
}

function Del_Upload1(var2)
{
	jConfirm('Are you sure you want to delete this upload permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file1_'+var2).remove();
	}
	});
}


</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?>&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>
                <?php
				$sql = "SELECT * from other_invoice_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and VENDOR='".$vendorid."' and P_TYPE='".$name."' ";
				
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				?>	
                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right">
                <?php if($rec>0){?><a href="#" title="Pdf" onClick="getPdf(<?php echo $rows['INVOICEID'];?>);"><button class="btn btn-default" type="button" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a>&nbsp;&nbsp;&nbsp;<?php }?><a href="<?php echo $page_link;?>?comid=<?php echo $comid;?>&page=<?php echo $page;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
							
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12" style="text-align:center;">
								<h2 class="page-header">
								<?php echo strtoupper($name);?>
								</h2>                            
							</div><!-- /.col -->
						</div>
                        
                        <div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								<strong>Invoicing Company :</strong>
								<address>
								   <select  name="selFromOwner" class="select form-control" id="selFromOwner">
										<?php 
										$obj->getVendorListNewForCOA(11);
										?>
									</select>
									<script>$('#selFromOwner').val('<?php echo $rows['SHIP_OWNER'];?>');</script>
								</address>
							</div><!-- /.col -->                     
							<div class="col-sm-4 invoice-col">
								 <strong>To :</strong>
								<address>
								   <?php if($rec > 0){?>
                                    <?php echo $obj->getVendorListNewBasedOnID($rows['VENDOR']);?><br>
                                    <?php echo stripslashes($obj->getVendorDataBasedOnID($rows['VENDOR'],'STREET_1'));?>
                                    <?php echo stripslashes($obj->getVendorDataBasedOnID($rows['VENDOR'],'CITY'));?>,
                                    <?php echo stripslashes($obj->getVendorDataBasedOnID($rows['VENDOR'],'COUNTRY'));?>,
                                    <?php echo stripslashes($obj->getVendorDataBasedOnID($rows['VENDOR'],'CITY_POSTAL_CODE'));?>
                                    <input type="hidden" name="txtVendor" id="txtVendor" value="<?php echo $rows['VENDOR'];?>" />
                                    <input type="hidden" name="txtFCAID" id="txtFCAID" value="<?php echo $rows['FCAID'];?>" />
                                    <?php }else{?>
                                    <?php echo $obj->getVendorListNewBasedOnID($vendorid);?><br>
                                    <?php echo stripslashes($obj->getVendorDataBasedOnID($vendorid,'STREET_1'));?>
                                    <?php echo stripslashes($obj->getVendorDataBasedOnID($vendorid,'CITY'));?>,
                                    <?php echo stripslashes($obj->getVendorDataBasedOnID($vendorid,'COUNTRY'));?>,
                                    <?php echo stripslashes($obj->getVendorDataBasedOnID($vendorid,'CITY_POSTAL_CODE'));?>
                                    <input type="hidden" name="txtVendor" id="txtVendor" value="<?php echo $vendorid;?>" />
                                    <input type="hidden" name="txtFCAID" id="txtFCAID" value="<?php echo $obj->getFun1();?>" />
                                    <?php }?>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								<strong>Freight Details</strong>
								<address>
								   Fixture Ref.: <?php echo $obj->getCompareEstimateData($comid,"VOYAGE_NO");?><br/>
                                   Vessel: <?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?><br/>
                                   CP Date: <?php echo date("d-m-Y",strtotime($obj->getOpenCargoPositionData($obj->getCompareEstimateData($comid,"OPEN_CARGOID"),'COA_DATE'))); ?><br/>
                                   Port of Loading: <?php echo $lp_name;?><br/>
                                   Port of Discharging: <?php echo $dp_name; ?>
								</address>
							</div><!-- /.col -->
						</div>
					    
                        <div class="row invoice-info">
						   <div class="col-sm-6 invoice-col">
                               <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;">Invoice Type :</th>
                                            <td><select  name="selIType" class="select form-control" id="selIType"><?php $obj->getInvoiceType();?></select></td>
                                        </tr>
                                        <script>$("#selIType").val('<?php echo $rows['I_TYPE'];?>');</script>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;">Attn :</th>
                                            <td><input type="text" name="txtAttenName" id="txtAttenName" class="form-control"  placeholder="Attn" autocomplete="off" value="<?php echo $rows['ATTEN'];?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Invoice Number :</th>
                                            <td>
											<?php if($rec == 0){?>
                                            <input type="text" name="txtDNote" id="txtDNote" class="form-control" placeholder="Invoice No." value=""/>
                                            <input type="hidden" name="txtDNote1" id="txtDNote1" value="" readonly/>
                                            <?php }else{?>
                                            <input type="text" name="txtDNote" id="txtDNote" class="form-control" placeholder="Invoice No." value="<?php echo $rows['MESSAGE'];?>"/>
                                            <input type="hidden" name="txtInvoiceid" id="txtInvoiceid" value="<?php echo $rows['INVOICEID'];?>" readonly/>
                                            <input type="hidden" name="txtDNote1" id="txtDNote1" value="<?php echo $rows['MESSAGE'];?>" readonly/>
                                            <?php }?></td>
                                        </tr>
                                        <?php
										$invdate = date('d-m-Y',strtotime($rows['DATE']));
										if($invdate=='01-01-1970'){$invdate = '';}
										$duedate = date('d-m-Y',strtotime($rows['DUE_DATE']));
										if($duedate=='01-01-1970'){$duedate = '';}
										$exchangeDate = date('d-m-Y',strtotime($rows['EXCHANGE_DATE']));
										if($exchangeDate=='01-01-1970'){$exchangeDate = '';}
										?>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Invoice Date :</th>
                                            <td><input type="text" name="txtDate" id="txtDate"  autocomplete="off" class="form-control" value="<?php echo $invdate;?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Due Date :</th>
                                            <td><input type="text" name="txtDueDate" id="txtDueDate" class="form-control datepicker"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $duedate;?>"/></td>
                                        </tr>
                                        
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Exchange Rate :</th>
                                            <td><input type="text" name="txtExchangeRate" id="txtExchangeRate" class="form-control numeric"  placeholder="Exchange Rate" autocomplete="off" value="<?php echo $rows['EXCHANGE_RATE'];?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Exchange Date :</th>
                                            <td><input type="text" name="txtExchangeDate" id="txtExchangeDate" class="form-control datepicker"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $exchangeDate;?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;">Exchange To Currency :</th>
                                            <td><select  name="selExchangeCurrency" class="select form-control" id="selExchangeCurrency"><?php $obj->getCurrencyList();?></select></td>
                                        </tr>
                                        <script>$("#selExchangeCurrency").val('<?php echo $rows['EXCHANGE_CURRENCY'];?>');</script>
                                        
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Payment Terms :</th>
                                            <td><input type="text" name="txtPaymentTerms" id="txtPaymentTerms" class="form-control"  placeholder="Payment Terms" autocomplete="off" value="<?php echo $rows['PAYMENT_TERMS'];?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Description :</th>
                                            <td><textarea name="txtDesc" id="txtDesc" class="form-control autosize" rows="3" placeholder="Description"><?php echo $rows['REMARKS'];?></textarea></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Banking Details :</th>
                                            <td><select  name="selNOB" class="select form-control" id="selNOB" onChange="getBankingDetailsData()"><?php $obj->getBankingDetails();?></select></td>
                                            <script>$('#selNOB').val('<?php echo $rows['NOB'];?>');</script>
                                        </tr>
                                     </tbody>
                                     <tbody id="bankingrow" style="display:none;">
                                        <tr>
											<td>Address</td>
											<td><span id="span_1_0"></span></td>
										</tr>
										<tr>
											<td>Beneficiary A/C No.</td>
											<td><span id="span_1_1"></span></td>
										</tr>
										<tr>
											<td>Beneficiary Bank</td>
											<td><span id="span_1_2"></span></td>
										</tr>
										<tr>
											<td>Beneficiary Bank Address</td>
											<td><span id="span_1_3"></span></td>
										</tr>
										<tr>
											<td>Beneficiary Bank Swift Code</td>
											<td><span id="span_1_4"></span></td>
										</tr>
										<tr>
											<td>IBAN No.</td>
											<td><span id="span_1_5"></span></td>
										</tr>
										<tr>
											<td>FED ABA</td>
											<td><span id="span_1_6"></span></td>
										</tr>
										<tr>
											<td colspan="4" style="background-color:grey; color:white;">CORRESPONDENT DETAILS</td>
										</tr>
										<tr>
											<td>Correspondent Bank Name</td>
											<td><span id="span_1_7"></span></td>
										</tr>
										
                                        <tr>
                                            <td>Correspondent Bank Address</td>
											<td><span id="span_1_9"></span></td>
										</tr>
                                        <tr>
											<td>Account Number</td>
											<td><span id="span_1_10"></span></td>
										</tr>
                                        <tr>
											<td>Swift Code</td>
											<td><span id="span_1_8"></span></td>
										</tr>
                                        
                                        <tr>
											<td>Receive Payment</td>
											<td>
                                            <?php if($rows['INVOICEID'] > 0){?>
												<a href="#Payment" title="Payment Received" class="btn btn-default btn-sm" data-toggle="modal" data-target="#compose-modal" onClick="openWin(<?php echo $rows['INVOICEID'];?>,<?php echo $rows['NET_AMOUNT'];?>);">Payment Received</a>
										<?php }?>
                                            </td>
										</tr>
                                     </tbody>
                                   </table>
                               </div>
						   </div><!-- /.col -->
						   <div class="col-sm-6 invoice-col">
							   <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;" colspan="2"><?php echo $amounttitle;?>(<?php echo $currency;?>) :</th>
                                            <td><input type="text" name="txtInvAmount" id="txtInvAmount" class="form-control" readonly value="<?php echo $id[3];?>"></td>
                                        </tr>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;" colspan="3">Other Add</th>
                                        </tr>
                                     </tbody>
                                     <?php $sql2 = "select * from other_invoice_slave where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='ADD'";
										   $res2 = mysql_query($sql2);
										   $num2 = mysql_num_rows($res2);
										   if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									       ?>
                                      <tbody id="tblAdd">
                                  <?php if($num2 >0)
									    {$i = 0;
										  while($rows2 = mysql_fetch_assoc($res2))
										  {$i++;
										  ?>
										    <tr id="add_Row_<?php echo $i;?>">
										  	    <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeAddRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
										     	<td><textarea class="form-control areasize" name="txtAddDes_<?php echo $i;?>" id="txtAddDes_<?php echo $i;?>" rows="2" placeholder="Description..."><?php echo $rows2['DESCRIPTION'];?></textarea></td>
										   	    <td><input type="text" name="txtAddDesAmt_<?php echo $i;?>" id="txtAddDesAmt_<?php echo $i;?>" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="<?php echo $rows2['AMOUNT'];?>" onKeyUp="getAmountThereOff();"/></td>
										    </tr>
										<?php }}else{?>
										   <tr id="add_Row_1">
										       <td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
										       <td><textarea class="form-control areasize" name="txtAddDes_1" id="txtAddDes_1" rows="2" placeholder="Description..."></textarea></td>
										       <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getAmountThereOff();"/></td>
										   </tr>
								   <?php }?>
                                       </tbody>
                                       <tbody>
                                           <tr>
                                               <td align="left" colspan="3"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewAddRow();">Add</button><input type="hidden" name="txtAddID" id="txtAddID" value="<?php echo $num21;?>"/></td>
                                           </tr>
                                           
                                           <tr>
                                               <th colspan="3">Other Less</th>
                                           </tr>
                                           
                                      <?php $sql2 = "select * from other_invoice_slave where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='SUB'";
											$res2 = mysql_query($sql2);
											$num2 = mysql_num_rows($res2);
											if($num2 >0){$num21 = $num2;}else{$num21 = 1;}?>
                                       </tbody>
                                       <tbody id="tblSub">
                                     <?php if($num2 >0)
										   {$i = 0;
											while($rows2 = mysql_fetch_assoc($res2))
											{$i++;
											?>
											<tr id="Sub_Row_<?php echo $i;?>">
											   <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeSubRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
											   <td><textarea class="form-control areasize" name="txtSubDes_<?php echo $i;?>" id="txtSubDes_<?php echo $i;?>" rows="2" placeholder="Description..."><?php echo $rows2['DESCRIPTION'];?></textarea></td>
											   <td><input type="text" name="txtSubDesAmt_<?php echo $i;?>" id="txtSubDesAmt_<?php echo $i;?>" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="<?php echo $rows2['AMOUNT'];?>" onKeyUp="getAmountThereOff();"/></td>
											</tr>
											<?php }}else{?>
											<tr id="Sub_Row_1">
											   <td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
											   <td><textarea class="form-control areasize" name="txtSubDes_1" id="txtSubDes_1" rows="2" placeholder="Description..."></textarea></td>
											   <td><input type="text" name="txtSubDesAmt_1" id="txtSubDesAmt_1" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getAmountThereOff();"/></td>
											</tr>
									<?php }?>
                                       </tbody>
                                       <tbody>
                                           <tr>
                                               <td align="left" colspan="3"><button type="button" class="btn btn-primary btn-flat" onClick="SubNewSubRow();">Add</button><input type="hidden" name="txtSubID" id="txtSubID" value="<?php echo $num21;?>"/></td>
                                           </tr>
                                           <tr>
                                                <td colspan="2"><strong>Amount Payable</strong></td>
                                                
                                                <td><input type="text"  name="txtNetAmtPayable" id="txtNetAmtPayable" class="form-control" readonly value="<?php echo $rows['NET_PAYABLE'];?>" /></td>
                                            </tr>
                                            
                                       </tbody>
                                   </table>
                               </div>
						   </div><!-- /.col -->
						</div>
                        
                        <div class="box-footer" align="right">
                        <?php if($rows['STATUS']!=1){?>
                            <input type="hidden" id="action" name="action" value="submit" />
                            <input type="hidden" name="txtStatus" id="txtStatus" value="0"/>
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0)">Submit to edit</button>
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(1)">Submit & Close</button>
                            
							<input type="hidden" name="txtFreightAmtLocal" id="txtFreightAmtLocal" value="<?php echo $id[7];?>" />	
                        <?php }?>    						
						</div>
						<div class="box-footer" align="right">&nbsp;</div>
					</form>	
                    
                    
					<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								</div>
								<div class="modal-body">
									<form name="frm3" id="frm3" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>
                                        <div id="divPayment">
                                            
                                        </div>
                                        <div class="box-footer" align="right">
                                            <button type="submit" id="btnhide" class="btn btn-primary btn-flat" onClick="return getValid();" >Submit</button>
                                            <input type="hidden" id="action2" name="action2" value="submit1" />
                                            <input type="hidden" name="txtCRMFILE1" id="txtCRMFILE1" value="" />
                                            <input type="hidden" name="txtCRMNAME1" id="txtCRMNAME1" value="" />
                                        </div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
					
					
					
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>