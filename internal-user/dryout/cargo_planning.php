<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

if (@$_REQUEST['action'] == 'submit')
 {
 	if($_REQUEST['txtStatus'] == "1")
	{
 		$msg = $obj->deleteCargoEntry($_REQUEST['txtCargoID']);
	}
	
	header('Location:./cargo_planning.php?msg='.$msg);
 }

$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
$rigts    = explode(',',$obj->getUserRights($_SESSION['uid'],$_SESSION['moduleid'],18));
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(18); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Cargo Tonnage Book</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Cargo Tonnage added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating Cargo Tonnage.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
				<h3 style=" text-align:center;">Cargo Tonnage Book</h3>
				<?php if(in_array(2, $rigts)){?><div align="right"><a href="addcargoplanning.php" title="Add New"><button class="btn btn-info btn-flat">Add New</button></a>&nbsp;&nbsp;&nbsp;&nbsp;</div><?php }?>
				<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                 
				<div style="height:10px;">
				<input type="hidden" name="action" value="submit" /><input type="hidden" name="txtCargoID" id="txtCargoID" value="" /><input type="hidden" name="txtStatus" id="txtStatus" value="" />
				</div>		
                
                <div class="box-body table-responsive" style="overflow:auto;">
                     <table id="cargo_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th align="left" valign="top" width="9%">CP ID</th>
                                <th align="left" valign="top" width="10%">COA Date</th>
                                <th align="left" valign="top" width="10%">Cargo</th>
                                <th align="left" valign="top" width="8%">Cargo Stem Size(MT)</th>
                                <th align="left" valign="top" width="8%">Load Port</th>
                                <th align="left" valign="top" width="8%">Dis Port</th>
                                <th align="left" valign="top" width="8%">Frt<br/>(USD/MT)</th>
                                <th align="left" valign="top" width="10%">Charterer</th>
                                <th align="left" valign="top" width="10%">Owner</th>
                                <th align="left" valign="top" width="7%">COA/<br/>Spot</th>
                                <th align="left" valign="top" width="8%">Cargo Relet<br/>/Voyage</th>
                                <th align="left" valign="top" style="min-width:50px;">Laycan Fm/To</th>
                                <th align="left" valign="top" width="8%">Approval Status</th>
                                <th align="center" valign="top" width="5%">Details</th>
                            </tr>
                        </thead>
                    </table>
            	</div>
				
				</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
   getDefaultData();
});

function getDefaultData()
{
	var table = $('#cargo_list').dataTable();
	if(table != null)table.fnDestroy();
  
	table = $("#cargo_list").DataTable( {
		"processing": true,
		"serverSide": true,
		"stateSave": true,
		"order": [[1, 'desc']],
		"ajax": {
			"url": "options.php?id=73",
			"type": "POST",
			"dataSrc": function ( d ) {
				return d.records;
			},
			"data": function ( d ) {},
		},
		"columns": [
			{ "data": "col1" },
			{ "data": "col2" },
			{ "data": "col3" },
			{ "data": "col4" },
			{ "data": "col5" },
			{ "data": "col6" },
			{ "data": "col7" },
			{ "data": "col8" },
			{ "data": "col9" },
			{ "data": "col10" },
			{ "data": "col11" },
			{ "data": "col12" },
			{ "data": "col13" },
			{ "data": "col14" },
			
		],
		"language": {
			  "zeroRecords": "SORRY CURRENTLY THERE ARE ZERO(0) RECORDS"
			},
		 "fnDrawCallback": function(oSettings, json) {
			  $('[data-toggle="tooltip"]').tooltip();	
			},	
		"columnDefs": [ 
				{
					"targets": 11,
					"orderable": false
				},
				{
					"targets": 12,
					"orderable": false
				},
				{
					"targets": 13,
					"orderable": false
				}
			]
				
	});
	
}

function getDelete(cargoid)
{
	jConfirm('Are you sure to remove this cargo planning entry permanently?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtCargoID").val(cargoid);
			$("#txtStatus").val("1");
			document.frm1.submit();
		}
		else{return false;}
		});	
}


function getNominate(cargoid,open_vessel_id)
{
	if(open_vessel_id == 0)
	{
		jAlert('Please attach open vessel entry (not TBN).','Alert');
	}
	else
	{
		jConfirm('Are you sure to nominate this cargo entry ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtCargoID").val(cargoid);
				$("#txtStatus").val("2");
			document.frm1.submit();
		}
		else{return false;}
		});	
	}
}
</script>
</body>
</html>