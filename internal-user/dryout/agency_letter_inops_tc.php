<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];

if($page == 1){$page_link = "in_ops_tc.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}

if(@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertGenerateAgencyLetterDetailsTC();
	header('Location:./agency_letter_inops_tc.php?msg='.$msg.'&comid='.$_REQUEST['comid'].'&page='.$_REQUEST['page']);
}
 
$cost_sheet_id 	= 	$obj->getLatestCostSheetIDTC($comid);
$obj->viewTCEstimationTempleteRecordsNew($cost_sheet_id);

$uid	  = $_SESSION['uid']; 
$moduleid = $_SESSION['moduleid'];

if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}

$rights = $obj->getUserRights($uid,$moduleid,$linkid);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;
$msg = NULL;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>

<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}
form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(21); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops TC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops TC&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance TC</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content invoice">
					<?php 
							if(isset($_REQUEST['msg'])){
								$msg = $_REQUEST['msg'];
								if($msg == 0){?>
									<div class="alert alert-success alert-dismissable">
										<i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<b>Congratulations!</b> Agency Letter Generation added/updated successfully.
									</div>
								<?php }?>
								<?php if($msg == 1){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Sorry!</b> this agent is already exists for this port.
								</div>
								<?php }?>
								<?php if($msg == 2){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Congratulations!</b> Agency Letter deleted successfully.
								</div>
								<?php }?>
						<?php }?>
						<div align="right"><a href="in_ops_tc.php"><button class="btn btn-info btn-flat">Back</button></a></div>
						<div style="height:10px;">&nbsp;</div>
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header" style="text-align:center;">
								 TC - GENERATE AGENCY RELATED LETTERS
								</h2>                            
							</div><!-- /.col -->
						</div>			
						<div class="row invoice-info">
							<div class="col-md-12">
						       <form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>
							   <?php
										$sql 		= "select * from generate_agency_letter_tc where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS = 1 ";
										$res 	 	=  mysql_query($sql) or die($sql);
										$num        = mysql_num_rows($res); 
										$rows 		= mysql_fetch_assoc($res);
										$i = 0;
								?>
								<input type="hidden" name="genAgencyId" id="genAgencyId" value="<?php echo $rows['GEN_AGENCY_TC_ID'];?>" />		
								<div class="row invoice-info">
									<div class="col-sm-6 invoice-col">&nbsp;</div>
										<div class="col-sm-3 invoice-col">
											TC No.
											<address>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<strong><?php echo $obj->getCharteringEstimateTCData($comid,'TC_NO');?></strong>
											</address>
										</div>
										<div class="col-sm-3 invoice-col">
											Nom ID
											<address>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<strong><?php echo $obj->getCompareTableDataTC($comid,"MESSAGE");?></strong>
											</address>
										</div>
								</div>
								<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Date
											<address>
												<input type="text" name="txtDate" id="txtDate" autocomplete="off" class="form-control" placeholder="dd-mm-yyyy" value="<?php if($num>0){echo date("d-m-Y",strtotime($rows['DATE'])) ;}?>" />
											</address>
										</div>
										<div class="col-sm-3 invoice-col">
											Vessel
											<address>
												<input type="text" name="txtVName" id="txtVName" class="form-control" placeholder="Vessel" readonly value="<?php echo $obj->getVesselIMOData($obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?>" />
											</address>
										</div>
										<div class="col-sm-3 invoice-col">
											Agent Name
											<address>
												<select name="selAList" class="form-control" id="selAList" onChange="getVendorName();">
												<?php 
												$obj->getVendorListNewUpdate();
												?>
												</select>
												<script>$("#selAList").val('<?php echo $rows['VENDORID'];?>');</script>
											</address>
										</div>
								</div>
								<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Port of Call
											<address>
											<select name="selPortOfCall" id="selPortOfCall" class="form-control">
												<?php echo $obj->getPortList();?>
											</select>
											<script>$("#selPortOfCall").val('<?php echo $rows['PORT_OF_CALL'];?>');</script>
											</address>
										</div>
										<div class="col-sm-3 invoice-col">
											Purpose of Call
											<address>
											<select name="selPurpOfCall" id="selPurpOfCall" class="form-control">
												<?php echo $obj->getPurposeList();?>
											</select>
											<script>$("#selPurpOfCall").val('<?php echo $rows['PURPOSE_OF_CALL'];?>');</script>
											</address>
										</div>
										<div class="col-sm-3 invoice-col">
											Master's Name
											<address>
												<input type="text" name="txtTOfTolerance" id="txtTOfTolerance" class="form-control" placeholder="Master's Name"  value="<?php if($num>0){echo $rows['TERMO_OF_TOLERANCE'];} else {echo $obj->getCharteringEstimateTCData($comid,'MASTERS_NAME');} ?>" autocomplete="off" onKeyUp="getMasterName();" />
											</address>
										</div>
								</div>
								<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Ship Owner
											<address>
											<select name="selShipOwner" id="selShipOwner" class="form-control" onChange="getShipOwnerdetail();">
												<?php $obj->getVendorListNewForCOA(11);?>
											</select>
											<script> $("#selShipOwner").val('<?php echo $rows['SHIP_OWNER'];?>'); </script>
											</address>
										</div>
								</div>
								<div class="row invoice-info">
										<div class="col-sm-12 invoice-col">
											Main Description<br><br>
											<!--<span id="mainDesc1">Please copy :&nbsp;&nbsp;opex@socatra.com</span><br>-->
							To :&nbsp;&nbsp;<span id="mainDesc2"></span><br>
							Cc:&nbsp;&nbsp;Master mt :&nbsp;<?php echo $obj->getVesselIMOData($obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME")." /att. " ?><span id="mainDesc3"></span><br>
							Cc:&nbsp;&nbsp;<span id="mainDesc4"></span><br>
										   <!--<span id="mainDesc5">Cc:&nbsp;&nbsp;Socatra Crewing/QHSE/IT/Purchasing/Technical</span><br>
											<span id="mainDesc6">From :&nbsp;&nbsp;Socatra Comops, Bordeaux</span>-->
											<address>
											<textarea name="txtMainDescription" id="txtMainDescription" class="form-control" placeholder="Enter Main Description Here"><?php echo $rows['MAIN_DESCRIPTION']; ?></textarea>
											</address>
										</div>
								</div>
								
								<?php if($rights == 1 && $rows['STATUS'] !=2){ ?>
										<div class="box-footer" align="right">
											<button class="btn btn-primary btn-flat" id="inner-login-button" type="submit" onClick="return getSubmit(1);" >Submit</button>&nbsp;&nbsp;
											<button class="btn btn-primary btn-flat" id="inner-login-button" type="submit" onClick="return getSubmit(2);"  >Submit & Close</button>
											<input type="hidden" name="action" id="action" value="submit" />
											<input type="hidden" name="update_status" id="update_status" value="" />
										</div>
										
								<?php } ?>
								
								<div class="box-body no-padding" style="overflow:auto;">
										<table class="table table-striped">
											<thead>
												<tr>
													<th width="3%">#</th>
													<th width="10%">Date</th>
													<th width="10%">Port</th>
													<th width="10%">Purpose</th>
													<th width="10%">Agent Name</th>
													<th width="10%">Letter</th>
													<th width="3%">Edit/Cancel</th>
												</tr>
											</thead>
											<tbody id="tbl_txtID">
											<?php 
											$sql = "select * from generate_agency_letter_tc where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS >=1";
											$res = mysql_query($sql);
											$rec = mysql_num_rows($res);
											if($rec == 0){
											?>
											<tr id="tpRow"><td valign="top" align="center" colspan="9" style="color:red;">Sorry , currently zero(0) records added.</td></tr>
											<?php }else{
											$m=1;
											while($rows = mysql_fetch_assoc($res))
											{
											?>
											<tr id="tpRow_<?php echo $m;?>">
											<td align="center"><?php echo $m.".";?></td>
											<td align="left"><?php echo date("d-M-Y",strtotime($rows['DATE']));?></td>
											<td align="left"><?php echo $obj->getPortNameBasedOnID($rows['PORT_OF_CALL']);?></td>
											<td align="left"><?php echo $obj->getPurposeListBasedOnID($rows["PURPOSE_OF_CALL"]);?></td>
											<td align="left"><?php echo $obj->getVendorListNewBasedOnID($rows['VENDORID']);?></td>
											<td align="left"><p>
											<a href="allPdf.php?id=66&gen_agency_id=<?php echo $rows['GEN_AGENCY_TC_ID'];?>&comid=<?php echo $comid; ?>&tc_no=<?php echo $obj->getCharteringEstimateTCData($comid,'TC_NO');?>" title="Pdf" ><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i> PDA Request Letter</button></a></p></td>
											<td align="center">
											<a href="#?" title="Delete Entry" onClick="getDelete(<?php echo $rows['GEN_AGENCY_TC_ID'];?>,<?php echo $comid;?>,<?php echo $m;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
											</tr>
											<?php $m++;}}?>
											</tbody>
											
										</table>
								</div>
							   </form>
							</div>
						</div>
                <!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
	<?php $display->footer(); ?>
    </body>
</html>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

$('#txtMainDescription').autosize({append: "\n"});
$("#txtDate").datepicker({ 
	format: 'dd-mm-yyyy',
	autoclose: true	});
	
	$("#frm1").validate({
		rules: {
			txtDate:{required:true},
			selAList:{required:true},
			selPortOfCall:{required:true},
			selPurpOfCall:{required:true},
			txtTOfTolerance:{required:true},
			selShipOwner:{required:true},
			txtMainDescription:{required:true},
			},
		messages: {
			selVName:"*",
			},
	    submitHandler: function(form)  {
		jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
				if(r){
					jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
					$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
					$("#popup_content").css({"background":"none","text-align":"center"});
					$("#popup_ok,#popup_title").hide();  
					frm1.submit();
					}
					else{return false;}
				});
		}
	});
	
	if($("#selAList").val()!='')
	{
		$.post("options.php?id=64",{selAList:""+$("#selAList").val()+""}, function(data) 
			{
					$('#mainDesc2').text(data);

			});
	}
	else
	{
		$('#mainDesc2').text('');
	}
	
	if($("#txtTOfTolerance").val()!='')
	{
		
		$('#mainDesc3').text($("#txtTOfTolerance").val());
	}
	else
	{
		$('#mainDesc3').text('');
	}
	
	if($("#selShipOwner").val()!='')
	{
		$.post("options.php?id=65",{selShipOwner:""+$("#selShipOwner").val()+""}, function(data) 
			{
					$('#mainDesc4').text(data);

			});
	}
	else
	{
		$('#mainDesc4').text('');
	}
	
});

function getDelete(agencytcid,comid,var1)
{ 
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
	if(r){
			$.post("options.php?id=60",{agencytcid:""+agencytcid+"",comid:""+comid+""}, function(html) {
				if(html==1)
				{
					$("#tpRow"+"_"+var1).remove();
					window.location.replace("agency_letter_inops_tc.php?msg=2&comid=<?php echo $comid;?>&page=<?php echo $page;?>");
				}
			});
		}
		else
		{
			return false;
		}
	});	
}

function getSubmit(id)
{
	$("#update_status").val(id);
	return true;
} 

function getVendorName()
{
	if($("#selAList").val()!='')
	{
		$.post("options.php?id=64",{selAList:""+$("#selAList").val()+""}, function(data) 
			{
					$('#mainDesc2').text(data);

			});
	}
	else
	{
		$('#mainDesc2').text('');
	}
}

function getShipOwnerdetail()
{
	if($("#selShipOwner").val()!='')
	{
		$.post("options.php?id=65",{selShipOwner:""+$("#selShipOwner").val()+""}, function(data) 
			{
					$('#mainDesc4').text(data);

			});
	}
	else
	{
		$('#mainDesc4').text('');
	}
}

function getMasterName()
{
	if($("#txtTOfTolerance").val()!='')
	{
		
		$('#mainDesc3').text($("#txtTOfTolerance").val());
	}
	else
	{
		$('#mainDesc3').text('');
	}
}

</script>