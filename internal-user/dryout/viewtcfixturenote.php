<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid 		= $_REQUEST['comid'];
$page  		= $_REQUEST['page'];
$id 		= $_REQUEST['id'];

if($page == 1){$page_link = "in_ops_tc.php";}else if($page == 2){$page_link = "vessel_in_post_tc.php";}else {$page_link = "vessel_in_history_tc.php";}
if(@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->updateTCinOpsFixtureNoteData($id);
	header('Location:./'.$page_link.'?msg='.$msg);
}

$id = $obj->getLatestCostSheetIDTC($comid);
$obj->viewTCEstimationTempleteRecordsNew($id);
$pagename = basename($_SERVER['PHP_SELF'])."?&id=".$id."&page=".$page;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
table {border-collapse:separate;}
select{height:19px;}
.select {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-weight: bold;
    color: #333333;
    /* color: #FBC763; */
    text-decoration: none;
    line-height: 13px;
	height:13px;
}

.input-text {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #333333;
	text-decoration: none;
	/*text-transform:uppercase;
	line-height: 15px;*/
}

.input-textsmall {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-weight: normal;
	color: #333333;
	text-decoration:none;
	text-transform:uppercase;
}


.input 
{
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 10px;
font-weight: normal;
color: #333333;
text-decoration: none;
}
</style>
</head>
	<div id="basic-modal-content" style="display:none;" align="center">
	<table cellpadding="1" cellspacing="4" border="0" align="center">
	<tr><td align="center"><img src="../../img/loading.gif" /><br /></td></tr>
	</table>
	</div>
	
    <body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onClick="getFinalCalculation();">Calculate</a>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(21); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-leaf"></i>&nbsp;Ops TC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops at a glance - TC</li>
                    </ol>
                </section><br>
                
                <div align="right" style="margin-top:5px;margin-bottom:5px;margin-right:5px;">
				<a href="allPdf.php?id=70&comid=<?php echo $comid; ?>" title="CHeck List Pdf">
                        <button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                    </a>&nbsp;&nbsp;
				<a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a>
				</div><br>
                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                	<table width="100%" style="margin-bottom:5px; padding:0px 5px 0px 5px;">
                       <tr>
                           <td width="12%" ><strong >Fixture&nbsp;Type&nbsp;:</strong>&nbsp;TC&nbsp;Out&nbsp;&nbsp;<input name="selFType" type="hidden" id="selFType" value="1"></td>
                           <td>
                           <strong>Vessel&nbsp;:</strong>
						   <select name="selVName" id="selVName" disabled class="input-text" style="width:120px;color:#175c84;background-color:#E1E1E1;" onChange="getData();getValue();">
                           <?php
                                $obj->getVesselTypeListMemberWise($_SESSION['selBType']);
                                ?>
                           </select>
                           </td>
                           <td><strong>Vessel&nbsp;Type&nbsp;:</strong><input type="text" name="txtVType" id="txtVType" style="width:95px;color:#175c84;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $obj->getFun4();?>" />
                            <select  name="selBunker" style="display:none;" id="selBunker">
                                <?php $obj->getBunkerList(); ?>
                            </select>
                           </td>
                           <td><strong>Flag&nbsp;:</strong><input type="text" name="txtFlag" id="txtFlag" style="width:100px;color:#175c84;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $obj->getFun5();?>" /></td>
                            <td><strong>Date&nbsp;:</strong><input type="text" name="txtDate" id="txtDate" style="width:90px;color:#175c84;background-color:#E1E1E1;" class="input-text" value="<?php echo date("d-m-Y",strtotime($obj->getFun6()));?>" placeholder"dd-mm-yy" readonly="true" /></td>
                            <td ><strong>TC&nbsp;No.&nbsp;:</strong><input type="text" name="txtTCNo" id="txtTCNo" style="width:115px;color:#175c84;background-color:#E1E1E1;" class="input-text" value="<?php echo $obj->getFun7();?>" readonly/></td>
                       </tr>
                   </table>
                <div id="tabs" class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
						<li class="active"><a href="#tabs_1" data-toggle="tab">TC Fixture Note</a></li>
                        <li><a href="#tabs_2" data-toggle="tab">Commercial Parameters</a></li>
                        <li><a href="#tabs_3" data-toggle="tab">TC/CP Terms</a></li>
                    </ul>
                    <div class="tab-content">
                    
    <!---------------START---------------------------Tab_1->TC Fixture Note-------------------------------------------->
	   
                    <div id="tabs_1" class="tab-pane  active" style="overflow:auto;">
                       <table width="100%">
                          <tr>
                           <td colspan="3">
                               <table width="100%">
							   <tr>
							   <div><strong style="font-size:13px;">Charter Party</strong></div>
							   </tr>
                               <tr class="input-text">
                                  <td>CP Date&nbsp;</td>
								  <td><input type="text" name="txtCPdate" id="txtCPdate" class="input-text" value="<?php echo date("d-m-Y", strtotime($obj->getFun9())); ?>" style="width:130px;color:#175c84;background-color:#E1E1E1;" placeholder="dd-mm-yy" readonly /></td>
								 
								  <td >CP Type&nbsp;
								  <select name="selCPType" disabled id="selCPType" class="input-text" style="width:132px;background-color:#E1E1E1;">
								  <?php echo $obj->getContractTypeList();?>
								  </select></td>
								  <td >Charterers&nbsp;
								  <select name="selCharterers" disabled id="selCharterers" class="input-text" style="width:132px;background-color:#E1E1E1;">
								  <?php $obj->getVendorListNewForCOA('7,11,10');?>
								  </select></td>
								  <td >Charterers' Operations&nbsp;
								  <select name="selCharOperation" disabled id="selCharOperation" class="input-text" style="width:135px;background-color:#E1E1E1;" onChange="getChartererData();">
								  <?php $obj->getVendorListNewForCOA('7,11,10,12');?>
								  </select></td>
                               </tr>
							   <tr class="input-text">
								  <td >Law/Arbitration&nbsp;</td>
								  <td><select name="selLawArbit" disabled id="selLawArbit" class="input-text" style="width:130px;background-color:#E1E1E1;">
								  <?php $obj->getLawArbitrationList(); ?>
								  </select></td>
								  <td></td>
								  <td colspan="2"><textarea name="txtAddress" disabled id="txtAddress" rows="3" cols="85" placeholder="Address" readonly style="background-color:#E1E1E1;"><?php echo $obj->getFun14();?></textarea></td>
								  <td></td>
								  <td></td>
				               </tr>
                              </table>
                           </td>
                       	  </tr>
		<!-------------------------------------------LEFT SIDE PART--------------------------------------------------->	
                       
					   <tr>
                           <td width="28%" style="vertical-align:top;">
						   <div><strong style="font-size:13px;">Vessel Details: <span id="lblVesName"></span></strong></div>
                             <table width="100%">
                                <tr class="input-text">
                                	<td >Build/Yard</td>
									<td width="50%"><input type="text" name="txtBuildYard" id="txtBuildYard" class="input-text" style="width:120px; background-color:#E1E1E1;" autocomplete="off" value="<?php echo $obj->getFun15();?>" readonly/></td>
                                </tr>
                                <tr class="input-text">
                                	<td >Year Build</td>
									<td width="50%"><input type="text" name="txtYearBuild" id="txtYearBuild" class="input-text" style="width:120px; background-color:#E1E1E1;" autocomplete="off" value="<?php echo $obj->getFun16();?>" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Flag</td>
									<td width="50%"><input type="text" name="txtFlag1" id="txtFlag1" class="input-text" style="width:120px; background-color:#E1E1E1;" autocomplete="off" value="<?php echo $obj->getFun17();?>" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Port of Registry</td>
									<td width="50%"><input type="text" name="txtPortOfRegis" id="txtPortOfRegis" class="input-text" style="width:120px; background-color:#E1E1E1;" autocomplete="off" value="<?php echo $obj->getFun18();?>" readonly>
									</td>
                                </tr>
								<tr class="input-text">
                                	<td >IMO Number</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtIMOnum" id="txtIMOnum" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun19();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Class/Class ID</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtClassID" id="txtClassID" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun20();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Last Special Survey</td>
                                    <?php if($obj->getFun21()=="" || $obj->getFun21()=="0000-00-00"  || $obj->getFun21()=="1970-01-01"){$txtLastSpSurvey = "";}else{$txtLastSpSurvey = date('d-m-Y',strtotime($obj->getFun21()));}?>
									<td width="50%"><input autocomplete="off" type="text" name="txtLastSpSurvey" id="txtLastSpSurvey" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $txtLastSpSurvey;?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Last DD</td>
                                    <?php if($obj->getFun22()=="" || $obj->getFun22()=="0000-00-00"  || $obj->getFun22()=="1970-01-01"){$txtLastDD = "";}else{$txtLastDD = date('d-m-Y',strtotime($obj->getFun22()));}?>
									<td width="50%"><input autocomplete="off" type="text" name="txtLastDD" id="txtLastDD" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $txtLastDD;?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Owners’ P & I</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtOwnerPandI" id="txtOwnerPandI" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun23();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Master’s Name</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtMastersName" id="txtMastersName" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun24();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Call Sign</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtCallSign" id="txtCallSign" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun25();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Inmarsat Tel</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtInmarsatTel" id="txtInmarsatTel" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun26();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Inmarsat Email</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtInmarsatEmail" id="txtInmarsatEmail" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun27();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >LOA</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtLOA1" id="txtLOA1" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun28();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Breadth</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtBreadth" id="txtBreadth" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun29();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Summer DWT</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtSummerDWT" id="txtSummerDWT" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun30();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Summer Draft</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtSummerDraft" id="txtSummerDraft" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun31();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >TPC</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtTPC1" id="txtTPC1" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun32();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Gross Tonnage</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtGrossTonnage" id="txtGrossTonnage" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun33();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Net Tonnage</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtNetTonnage" id="txtNetTonnage" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun34();?>" readonly></td>
                                </tr>
								<?php if($_SESSION['selBType'] == 3)
								{ ?>	
								<tr class="input-text">
                                	<td >Suez GRT</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtSuezGRT" id="txtSuezGRT" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun35();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Suez NRT</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtSuezNRT" id="txtSuezNRT" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun36();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Panama NRT</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtPanamaNRT" id="txtPanamaNRT" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun37();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Grain Cap</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtGrainCap" id="txtGrainCap" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun38();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Bale Cap</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtBaleCap" id="txtBaleCap" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun39();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Cranes</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtCranes" id="txtCranes" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun40();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Grabs</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtGrabs" id="txtGrabs" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun41();?>" readonly></td>
                                </tr>
								<?php } elseif($_SESSION['selBType'] == 2) {  ?>
								<tr class="input-text">
                                	<td>Cargo Tank Capacity(CBM)</td>
									<td width="50%"><input type="text" name="txtCTankCap" id="txtCTankCap" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun111();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td>No. of Grades(Double V/V Seg)</td>
									<td width="50%"><input type="text" name="txtCTankNoOfGrades" id="txtCTankNoOfGrades" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun112();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td>Cargo Pump Capacity(CBM/Hr)</td>
									<td width="50%"><input type="text" name="txtCPumpCap" id="txtCPumpCap" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun113();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td>Total SBT Capacity(CBM)</td>
									<td width="50%"><input type="text" name="txtCTotalSBTCap" id="txtCTotalSBTCap" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun114();?>" readonly></td>
                                </tr>
								<?php } ?>
								<tr class="input-text">
                                	<td >Keel to top of Mast</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtKeelTopMast" id="txtKeelTopMast" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun42();?>" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Waterline to Top of Mast full Ballast</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtWaterlineTopMast" id="txtWaterlineTopMast" class="input-text" style="width:120px; background-color:#E1E1E1;" value="<?php echo $obj->getFun43();?>" readonly></td>
                                </tr>
                             </table>
                           </td>
						   
	<!---------------------/////////----------------RIGHT SIDE PART-----------------/////////--------------------------->					   
                           <td width="28%" style="padding-top:10px; vertical-align:top;">  
								<div><strong style="font-size:13px;">Charter Party Main Terms</strong></div>
                                <table width="100%">
                                <tr class="input-text">
                                	<td >Delivery Range / Port</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtDelRangePort" id="txtDelRangePort" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun44();?>" placeholder="Delivery Range / Port" readonly></td>
                                </tr>
                                <tr class="input-text">
								  <td >Duration Fixed Period&nbsp;</td>
								  <td><select name="selDurFixPeriod" id="selDurFixPeriod" class="input-text" style="width:120px;background-color:#E1E1E1;" disabled="disabled">
								  <?php $obj->getDurationFixedPeriod(0); ?>
								  </select></td>
                                </tr>
								<tr class="input-text" id="tripspan_1">
								  <td >Trip TC&nbsp;</td>
								  <td><input autocomplete="off" type="text" name="txtTripTC" id="txtTripTC" class="input-text" style="width:120px;background-color:#E1E1E1;" placeholder="Trip TC" value="<?php echo $obj->getFun74();?>" readonly/></td>
                                </tr>
								
								<tr class="input-text" id="periodspan_1">
								  <td >Period&nbsp;</td>
								  <td><input autocomplete="off" type="text" name="txtPeriod" id="txtPeriod" style="width:120px;background-color:#E1E1E1;" class="input-text" placeholder="Period"/ value="<?php echo $obj->getFun75();?>"></td>
                                </tr>
								<tr class="input-text" id="noOfTripspan_1">
								  <td >No. Of Trips&nbsp;</td>
								  <td><input autocomplete="off" type="text" name="txtNoOfTrips" id="txtNoOfTrips" class="input-text" style="width:120px;background-color:#E1E1E1;" placeholder="No. Of Trips" value="<?php echo $obj->getFun76();?>"/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Delivery Date/Time</td>
                                    <?php if($obj->getFun46()=="" || $obj->getFun46()=="0000-00-00 00:00:00"  || $obj->getFun46()=="1970-01-01 08:00:00"){$txtDeliveryDate = "";}else{$txtDeliveryDate = date('d-m-Y H:i',strtotime($obj->getFun46()));}?>
									<td width="50%"><input autocomplete="off" type="text" name="txtDeliveryDate" id="txtDeliveryDate" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $txtDeliveryDate; ?>" placeholder="dd-mm-yy hh:mm" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Redelivery Date/Time</td>
                                    <?php if($obj->getFun47()=="" || $obj->getFun47()=="0000-00-00 00:00:00"  || $obj->getFun47()=="1970-01-01 08:00:00"){$txtReDeliveryDate = "";}else{$txtReDeliveryDate = date('d-m-Y H:i',strtotime($obj->getFun47()));}?>
									<td width="50%"><input autocomplete="off" type="text" name="txtReDeliveryDate" id="txtReDeliveryDate" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $txtReDeliveryDate; ?>" placeholder="dd-mm-yy hh:mm" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Duration Optional Period</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtDurOptPer" id="txtDurOptPer" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun48();?>" placeholder="Duration Optional Period" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Commencement Optional Period</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtCommOptPer" id="txtCommOptPer" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun49();?>" placeholder="Commencement Optional Period" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Laycan From</td>
									<?php if($obj->getFun50()=="" || $obj->getFun50()=="0000-00-00 00:00:00"  || $obj->getFun50()=="1970-01-01 08:00:00"){$txtLaycanFromDate = "";}else{$txtLaycanFromDate = date('d-m-Y H:i',strtotime($obj->getFun50()));}?>
									<td width="50%"><input autocomplete="off" type="text" name="txtLaycanFrom" id="txtLaycanFrom" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $txtLaycanFromDate; ?>" placeholder="dd-mm-yy" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Laycan To</td>
									<?php if($obj->getFun51()=="" || $obj->getFun51()=="0000-00-00 00:00:00"  || $obj->getFun51()=="1970-01-01 08:00:00"){$txtLaycanToDate = "";}else{$txtLaycanToDate = date('d-m-Y H:i',strtotime($obj->getFun51()));}?>
									<td width="50%"><input autocomplete="off" type="text" name="txtLaycanTo" id="txtLaycanTo" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $txtLaycanToDate; ?>" placeholder="dd-mm-yy" readonly/></td>
                                </tr>
								<?php if($_SESSION['selBType'] == 3)
								{ ?>
								<tr class="input-text">
                                	<td >Laycan Narrowing</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtLaycanNarr" id="txtLaycanNarr" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun52();?>" placeholder="Laycan Narrowing" readonly/></td>
                                </tr>
								<?php } ?>
								<tr class="input-text">
                                	<td >Redelivery Range</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtRedeliveryRange" id="txtRedeliveryRange" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun53();?>" placeholder="Redelivery Range" readonly /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Hire PDPR Currency</td>
									<td ><select  name="selExchangeCurrency" id="selExchangeCurrency" class="input-text" disabled style="width:120px;background-color:#E1E1E1;" onChange="getCurrencySpan();"><?php $obj->getCurrencyList();?></select></td><script>$("#selExchangeCurrency").val('<?php echo $obj->getFun137(); ?>');</script>
                                </tr>
                                <tr class="input-text">
                                	<td >Exchange Rate To USD</td>
									<td ><input type="text" name="txtExchangeRate" id="txtExchangeRate" class="input-text numeric" readonly style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun138(); ?>" placeholder="Exchange Rate To USD" autocomplete="off" onKeyUp="getDailyGrossHire();" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Hire Fixed Period PDPR <span id="currencyspan"></span></td>
									<td ><input type="text" name="txtHireFixPerUSD" id="txtHireFixPerUSD" class="input-text numeric" readonly style="width:120px;background-color:#E1E1E1;" placeholder="Hire Fixed Period PDPR" onKeyUp="getDailyGrossHire();" value="<?php echo $obj->getFun54();?>" autocomplete="off" /></td>
                                </tr>
                                <tr class="input-text">
                                	<td >Hire Fixed Period PDPR (USD)</td>
									<td ><input type="text" name="txtHireFixPerUSD1" id="txtHireFixPerUSD1" class="input-text numeric" readonly style="width:120px;background-color:#E1E1E1;" placeholder="Hire Fixed Period PDPR (USD)" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Hire Optional Period</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtHireOptPer" id="txtHireOptPer" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun55();?>" placeholder="Hire Optional Period" readonly/></td>
                                </tr>
								
								<tr class="input-text">
                                	<td >Fuel Specs</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtFuelSpecs" id="txtFuelSpecs" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun62();?>" placeholder="Fuel Specs" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >CVE/Month(USD)</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtCVEmonth" id="txtCVEmonth" class="input-text numeric" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun63();?>" placeholder="CVE/Month(USD)" readonly="true" /></td>
                                </tr>
								<?php if($_SESSION['selBType'] == 3)
								{ ?>
								<tr class="input-text">
                                	<td >Supercargo and meals(USD)</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtSupercargoMeals" id="txtSupercargoMeals" class="input-text numeric" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun73();?>" placeholder="Supercargo and meals(USD)" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Hold Cleaning Intermediate(USD)</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtHoldCleanInterm" id="txtHoldCleanInterm" class="input-text numeric" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun64();?>" placeholder="Hold Cleaning Intermediate - USD" readonly /></td>
                                </tr>
								<tr class="input-text">
                                	<td >ILOHC(USD)</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtILOHCtcFix" id="txtILOHCtcFix" class="input-text numeric" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun65();?>" placeholder="ILOHC - USD" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >ILOHC - Remarks from CP</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtILOCremarksCP" id="txtILOCremarksCP" class="input-text" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun66();?>" placeholder="ILOHC - Remarks from CP" readonly/></td>
                                </tr>
								<?php } ?>
								<tr class="input-text">
                                	<td >Brokerage Comm. payable by</td>
									<td><select name="txtBrokCommPayableBy" disabled id="txtBrokCommPayableBy" class="input-text" style="width:120px;background-color:#E1E1E1;"><?php $obj->getPayableByList(); ?></select></td>
                                </tr>
								<tr class="input-text">
                                	<td >Add. Comm %</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtAddComm" id="txtAddComm" class="input-text numeric" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun68();?>" placeholder="Add. Comm %" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Broker’s Comm. %</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtBrokerCommTCfix" id="txtBrokerCommTCfix" class="input-text numeric" style="width:120px;background-color:#E1E1E1;" value="<?php echo $obj->getFun69();?>"  placeholder="Broker’s Comm. %" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Owner’s Banking Details</td>
									<td><select name="txtBankingDetails" disabled id="txtBankingDetails" class="input-text" style="width:120px;background-color:#E1E1E1;"><?php $obj->getBankingDetails();?></select>
									</td>
                                </tr>
								<tr class="input-text">
                                	<td >Document created by</td>
									<td width="50%"><input autocomplete="off" type="text" name="txtDocCreatBy" id="txtDocCreatBy" class="input-text" style="width:120px; height:20px; background-color:#E1E1E1;" value="<?php echo $obj->getFun71();?>"  readonly></td>
                                </tr>
								<tr class="input-text">
									<td >Additional Information</td>
									<td><textarea name="txtAddInformation" disabled id="txtAddInformation" rows="3" cols="20" placeholder="Additional Information"><?php echo $obj->getFun72();?> </textarea>
									</td>
								</tr>
                            </table>
                           </td>
						   
						   <td width="44%" style="padding-top:10px; vertical-align:top;">  
								<div><strong style="font-size:13px;">Bunker Details</strong></div>
                                <table width="100%">
                                <tr class="input-text">
                                	<td colspan="4">&nbsp;</td>
                                </tr>
                                <tr class="input-text">
                                	<td colspan="4"><strong>Delivery</strong></td>
                                </tr>
                                <tr class="input-text">
                                   
                                   <td>Bunker Grade</td>
                                   <td>Qty(MT)</td>
                                   <td>Bunker Date</td>
                                   <td>Price USD/MT)</td>
                                   <td>Amount(USD)</td>
                                </tr>
                                <tbody id="bunkerdelivery">
                                <?php $sql3 = "select * from chartering_estimate_tc_slave4 where TCOUTID='".$id."' and IDENTITY='DEL'";
                                 $result3 = mysql_query($sql3);
                                 $num3 = mysql_num_rows($result3);$k = 0;
                                 if($num3==0){$k++;?>
                                    <tr class="input-text" id="row_del_1">
                                       
                                       <td><select name="selDelBunker_1" class="input-text" style=" width:90px;" id="selDelBunker_1"></select></td>
                                       <td><input type="text" name="txtDelQty_1" id="txtDelQty_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtDelBunDate_1" id="txtDelBunDate_1" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                       <td><input type="text" name="txtDelPrice_1" id="txtDelPrice_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtDelAmount_1" id="txtDelAmount_1" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                    </tr>
                                    <script>$("#selDelBunker_1").html($("#selBunker").html());</script>
                               <?php }else{
								   while($rows3 = mysql_fetch_assoc($result3))
								   {$k++;
								   if($rows3['BUNKER_DATE']=="" || $rows3['BUNKER_DATE']=="0000-00-00"  || $rows3['BUNKER_DATE']=="1970-01-01"){$txtDelBunDate = "";}else{$txtDelBunDate = date('d-m-Y',strtotime($rows3['BUNKER_DATE']));}
								   ?>
                                     <tr class="input-text" id="row_del_<?php echo $k;?>">
                                       
                                       <td><select name="selDelBunker_<?php echo $k;?>" class="input-text" style=" width:90px;" id="selDelBunker_<?php echo $k;?>"></select></td>
                                       <td><input type="text" name="txtDelQty_<?php echo $k;?>" id="txtDelQty_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['QTY'];?>" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtDelBunDate_<?php echo $k;?>" id="txtDelBunDate_<?php echo $k;?>" class="input-text" style="width:85px;" value="<?php echo $txtDelBunDate;?>" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                       <td><input type="text" name="txtDelPrice_<?php echo $k;?>" id="txtDelPrice_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['PRICE'];?>" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtDelAmount_<?php echo $k;?>" id="txtDelAmount_<?php echo $k;?>" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="<?php echo $rows3['AMOUNT'];?>" placeholder="0.00" autocomplete="off"/></td>
                                    </tr>
                                   <script>$("#selDelBunker_<?php echo $k;?>").html($("#selBunker").html());$("#selDelBunker_<?php echo $k;?>").val(<?php echo $rows3['BUNKERID'];?>)</script>
                               <?php }}?>
                                </tbody>
                                <tbody>		
                                <tr >
                                    <td><input type="hidden" name="txtDELID" id="txtDELID" value="<?php echo $k;?>"/></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ><input type="text" name="txtTotalDelAmount" id="txtTotalDelAmount" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                    
                                </tr>
                                <tr class="input-text">
                                	<td colspan="4">&nbsp;</td>
                                </tr>
                                </tbody>
                                <tr class="input-text">
                                	<td colspan="4"><strong>Re-Delivery</strong></td>
                                </tr>
                                <tr class="input-text">
                                   
                                   <td>Bunker Grade</td>
                                   <td>Qty(MT)</td>
                                   <td>Bunker Date</td>
                                   <td>Price USD/MT)</td>
                                   <td>Amount(USD)</td>
                                </tr>
                                <tbody id="bunkerRedelivery">
                                <?php $sql3 = "select * from chartering_estimate_tc_slave4 where TCOUTID='".$id."' and IDENTITY='REDEL'";
                                 $result3 = mysql_query($sql3);
                                 $num3 = mysql_num_rows($result3);$k = 0;
                                 if($num3==0){$k++;?>
                                    <tr class="input-text" id="row_Redel_1">
                                       
                                       <td><select name="selReDelBunker_1" class="input-text" style=" width:90px;" id="selReDelBunker_1"></select></td>
                                       <td><input type="text" name="txtReDelQty_1" id="txtReDelQty_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtReDelBunDate_1" id="txtReDelBunDate_1" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                       <td><input type="text" name="txtReDelPrice_1" id="txtReDelPrice_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtReDelAmount_1" id="txtReDelAmount_1" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                    </tr>
                                    <script>$("#selReDelBunker_1").html($("#selBunker").html());</script>
                                    <?php }else{
								   while($rows3 = mysql_fetch_assoc($result3))
								   {$k++;
								   if($rows3['BUNKER_DATE']=="" || $rows3['BUNKER_DATE']=="0000-00-00"  || $rows3['BUNKER_DATE']=="1970-01-01"){$txtReDelBunDate = "";}else{$txtReDelBunDate = date('d-m-Y',strtotime($rows3['BUNKER_DATE']));}
								   ?>
                                     <tr class="input-text" id="row_Redel_<?php echo $k;?>">
                                       
                                       <td><select name="selReDelBunker_<?php echo $k;?>" class="input-text" style=" width:90px;" id="selReDelBunker_<?php echo $k;?>"></select></td>
                                       <td><input type="text" name="txtReDelQty_<?php echo $k;?>" id="txtReDelQty_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['QTY'];?>" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtReDelBunDate_<?php echo $k;?>" id="txtReDelBunDate_<?php echo $k;?>" class="input-text" style="width:85px;" value="<?php echo $txtReDelBunDate;?>" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                       <td><input type="text" name="txtReDelPrice_<?php echo $k;?>" id="txtReDelPrice_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['PRICE'];?>" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtReDelAmount_<?php echo $k;?>" id="txtReDelAmount_<?php echo $k;?>" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="<?php echo $rows3['AMOUNT'];?>" placeholder="0.00" autocomplete="off"/></td>
                                    </tr>
                                   <script>$("#selReDelBunker_<?php echo $k;?>").html($("#selBunker").html());$("#selReDelBunker_<?php echo $k;?>").val(<?php echo $rows3['BUNKERID'];?>)</script>
                               <?php }}?>
                                </tbody>
                                <tbody>		
                                <tr >
                                    <td><input type="hidden" name="txtREDELID" id="txtREDELID" value="<?php echo $k;?>"/></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ><input type="text" name="txtTotalReDelAmount" id="txtTotalReDelAmount" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="margin-top:25px;"><strong style="font-size:13px;">Baltic Route Details</strong></div>
                            <table width="100%" style="margin-top:2px;">
                               
                               <tr>
                                   <td class="input-text">Baltic Route&nbsp;:&nbsp;</td>
                                   <td class="input-text"><select name="selBalticRoute" class="input-text" style=" width:90px;" id="selBalticRoute"><?php echo $obj->getBalticRouteList();?></select></td>
                                   <script>$("#selBalticRoute").val(<?php echo $obj->getFun140();?>);</script>
                                    <td class="input-text">Baltic Route Date&nbsp;:&nbsp;</td>
                                    <?php if(date('d-m-Y',strtotime($obj->getFun141()))!='01-01-1970'){$txtBalticDate = date('d-m-Y',strtotime($obj->getFun141()));}else{$txtBalticDate = '';};?>
                                    <td class="input-text"><input type="text" name="txtBalticDate" id="txtBalticDate" style="width:90px;" class="input-text" autocomplete="off" placeholder="dd-mm-yyyy" value="<?php echo $txtBalticDate;?>"/></td>
                                    <td class="input-text">Baltic Route Value&nbsp;:&nbsp;</td>
                                    <td class="input-text"><input type="text" name="txtBalticValue" id="txtBalticValue" autocomplete="off" style="width:90px;" class="input-text numeric" value="<?php echo $obj->getFun142();?>" /></td>
                               </tr>
                             </table>
                           </td>
                       </tr>
                       </table>
                    </div>
					
	<!---------------ENDD---------------ENDD--------Tab_1->TC Fixture Note-------------------ENDD--------------------->

	<!-----------------START------------------Tab_2->COMMERCIAL PARAMETERS--------------------------------------------->				
					
                    <!-- /.tab_1-pane -->
                    <div id="tabs_2" class="tab-pane">
                         <table width="100%" style="margin-top:2px;">
                           <tr>
                               <td class="input-text">DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>&nbsp;:&nbsp;<input autocomplete="off" type="text" name="txtDWTS" id="txtDWTS" style="width:90px;background-color:#E1E1E1;" class="input-text"  readonly value="<?php echo $obj->getFun77();?>" placeholder="0.00" />
                               </td>
                               <td class="input-text">&nbsp;DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>&nbsp;:&nbsp;<input autocomplete="off" type="text" name="txtDWTT" id="txtDWTT" style="width:90px;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $obj->getFun78();?>" placeholder="0.00"/></td>
                                <td class="input-text">&nbsp;Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span><input autocomplete="off" type="text" name="txtGCap" id="txtGCap" style="width:90px;background-color:#E1E1E1;" class="input-text"  readonly value="<?php echo $obj->getFun79();?>" /></td>
                                <td class="input-text">&nbsp;Bale&nbsp;Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span><input autocomplete="off" type="text" name="txtBCap" id="txtBCap" style="width:90px;background-color:#E1E1E1;" class="input-text"  readonly value="<?php echo $obj->getFun80();?>"/></td>
                                <td class="input-text">SF<span style="font-size:10px; font-style:italic;">(ft3/lt)</span><input autocomplete="off" type="text" name="txtSF" id="txtSF"  style="width:90px;background-color:#E1E1E1;" class="input-text" value="<?php echo $obj->getFun81();?>" readonly  /></td>
                                <td class="input-text">Loadable<span style="font-size:10px; font-style:italic;">(MT)</span><input autocomplete="off" type="text" name="txtLoadable" id="txtLoadable" style="width:90px;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $obj->getFun82();?>" /></td>
                           </tr>
                       </table>
                         <table cellpadding="1" cellspacing="1" border="0" width="100%"  class='tablesorter' >
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
                        <tr>
						<td width="25%" align="left" class="text">GRT/NRT&nbsp;:&nbsp;<input autocomplete="off" type="text" name="txtGNRT" id="txtGNRT" style="width:190px;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $obj->getFun83();?>" /></td>
						<td width="25%" align="left" valign="top" class="text">LOA&nbsp;:&nbsp;<input autocomplete="off" type="text" name="txtLOA" id="txtLOA" style="width:190px;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $obj->getFun84();?>" /></td>
						<td width="25%" align="left" valign="top" class="text">Gear&nbsp;:&nbsp;<input autocomplete="off" type="text" name="txtGear" id="txtGear" style="width:190px;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $obj->getFun85();?>" /></td>
						<td width="25%" align="left" valign="top" class="text">Built&nbsp;Year&nbsp;:&nbsp;<input autocomplete="off" type="text" name="txtBuiltYear" id="txtBuiltYear" style="width:190px;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $obj->getFun86();?>" /></td>
						</tr>
                        <tr>
						<td width="25%" align="left" class="text">B.E.A.M.(m)&nbsp;:&nbsp;<input autocomplete="off" type="text" name="txtBeam" id="txtBeam" style="width:190px;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $obj->getFun87();?>" /></td>
						<td width="25%" align="left" valign="top" class="text">TPC&nbsp;:&nbsp;<input autocomplete="off" type="text" name="txtTPC" id="txtTPC" style="width:190px;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $obj->getFun88();?>" /></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						</tr>
						<tr>
						<td width="25%" align="left" class="text" style="color:#dc631e;">Speed Data</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="25%" align="left" class="input-text">Ballast Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="25%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtBFullSpeed" id="txtBFullSpeed" style="width:190px;background-color:#E1E1E1;" class="input-text" value="<?php echo $obj->getFun89();?>"  placeholder="0.00" readonly/></td>
						<td width="25%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtBEcoSpeed1" id="txtBEcoSpeed1" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun90();?>"  placeholder="0.00" readonly/></td>
						<td width="25%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtBEcoSpeed2" id="txtBEcoSpeed2" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun91();?>" placeholder="0.00" readonly/></td>
						</tr>
						<tr>
						<td width="25%" align="left" class="input-text">Laden Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="25%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtLFullSpeed" id="txtLFullSpeed" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun92();?>" placeholder="0.00" readonly/></td>
						<td width="25%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtLEcoSpeed1" id="txtLEcoSpeed1" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun93();?>" placeholder="0.00" readonly/></td>
						<td width="25%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtLEcoSpeed2" id="txtLEcoSpeed2" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun94();?>" placeholder="0.00" readonly/></td>
						</tr>
						</tbody>
						</table>
						</td></tr>
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">FO Consumption MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtBFOFullSpeed" id="txtBFOFullSpeed" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun95();?>" placeholder="0.00" readonly/></td>
						<td width="17%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtBFOEcoSpeed1" id="txtBFOEcoSpeed1" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun96();?>" placeholder="0.00" readonly/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtBFOEcoSpeed2" id="txtBFOEcoSpeed2" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun97();?>" placeholder="0.00" readonly/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtLFOFullSpeed" id="txtLFOFullSpeed" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun98();?>" placeholder="0.00" readonly/></td>
						<td width="17%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtLFOEcoSpeed1" id="txtLFOEcoSpeed1" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun99();?>" placeholder="0.00" readonly/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtLFOEcoSpeed2" id="txtLFOEcoSpeed2" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun100();?>" placeholder="0.00" readonly/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtPIFOFullSpeed" id="txtPIFOFullSpeed" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun101();?>" placeholder="0.00" readonly/></td>
						<td width="17%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtPWFOFullSpeed" id="txtPWFOFullSpeed" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun102();?>" placeholder="0.00" readonly/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						
						</tbody>
						</table>
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">DO Consumption per MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtBDOFullSpeed" id="txtBDOFullSpeed" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun103();?>"  placeholder="0.00" readonly/></td>
						<td width="17%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtBDOEcoSpeed1" id="txtBDOEcoSpeed1" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun104();?>"  placeholder="0.00" readonly/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtBDOEcoSpeed2" id="txtBDOEcoSpeed2" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun105();?>"  placeholder="0.00" readonly/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtLDOFullSpeed" id="txtLDOFullSpeed" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun106();?>"  placeholder="0.00" readonly/></td>
						<td width="17%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtLDOEcoSpeed1" id="txtLDOEcoSpeed1" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun107();?>"  placeholder="0.00" readonly/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtLDOEcoSpeed2" id="txtLDOEcoSpeed2" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun108();?>"  placeholder="0.00" readonly/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtPIDOFullSpeed" id="txtPIDOFullSpeed" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun109();?>"  placeholder="0.00" readonly/></td>
						<td width="17%" align="right" valign="top" class="text"><input autocomplete="off" type="text" name="txtPWDOFullSpeed" id="txtPWDOFullSpeed" style="width:190px;background-color:#E1E1E1;" class="input-text"  value="<?php echo $obj->getFun110();?>"  placeholder="0.00" readonly/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						</tbody>
						</table>
						
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						</table>
                    </div>
					
	<!-----------------ENDD------------ENDD------Tab_2->COMMERCIAL PARAMETERS----------------------ENDD---------------->
	
	<!-----------------START-----------------Tab_3->TC/CP TERMS-------------------------------------------------------->
					

                    <div id="tabs_3" class="tab-pane" style="overflow:auto;">
                        <table width="100%">
                        <tr >
                            <td colspan="8"><div><strong style="font-size:13px;">TC CP Terms : Sea Passage</strong></div>  </td>
                        </tr>
                        <tr class="input-text">
                            <td>Wind Force : </td>
                            <td><input autocomplete="off" type="text" name="txtWindForce" id="txtWindForce" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun121();?>"/></td>
                            <td>Speed Laden(Kts) : </td>
                            <td><input autocomplete="off" type="text" name="txtSpeedLaden" id="txtSpeedLaden" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun122();?>"/></td>
                            <td>Speed Ballast(Kts) : </td>
                            <td><input autocomplete="off" type="text" name="txtSpeedBallast" id="txtSpeedBallast" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun123();?>"/></td>
                            <td>CP Speed</td>
                            <td><input autocomplete="off" type="text" name="txtCPSpeed" id="txtCPSpeed" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun124();?>"/></td>
                        </tr>
                        <tr class="input-text">
                        	<td>FO Cons Laden(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtFOConsLaden" id="txtFOConsLaden" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun125();?>"/></td>
                            <td>DO Cons Laden(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtDOConsLaden" id="txtDOConsLaden" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun126();?>"/></td>
                            <td>FO Cons Ballast(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtFOConsBallast" id="txtFOConsBallast" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun127();?>"/></td>
                            <td>DO Cons Ballast(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtDOConsBallast" id="txtDOConsBallast" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun128();?>"/></td>
                        </tr>
                        <tr >
                            <td colspan="8"><div><strong style="font-size:13px;">TC CP Terms : Port</strong></div>  </td>
                        </tr>
                        <tr class="input-text">
                        	<td>FO Cons Ldg(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtFOConsLdg" id="txtFOConsLdg" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun129();?>"/></td>
                            <td>DO Cons Ldg(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtDOConsLdg" id="txtDOConsLdg" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun130();?>" /></td>
                            <td>FO Cons Disch(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtFOConsDisch" id="txtFOConsDisch" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun131();?>" /></td>
                            <td>DO Cons Disch(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtDOConsDisch" id="txtDOConsDisch" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun132();?>" /></td>
                        </tr>
                        <tr class="input-text">
                        	<td>FO Cons Idle(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtFOConsIdle" id="txtFOConsIdle" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun133();?>" /></td>
                            <td>DO Cons Idle(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtDOConsIdle" id="txtDOConsIdle" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun134();?>" /></td>
                            <td>Load Rate(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtLoadRate" id="txtLoadRate" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun135();?>" /></td>
                            <td>Disch Rate(MT/Day)</td>
                            <td><input autocomplete="off" type="text" name="txtDischRate" id="txtDischRate" class="input-text" size="21" placeholder="0.00" value="<?php echo $obj->getFun136();?>" /></td>
                        </tr>
                        </table>
                    </div>
                    
	<!-----------------ENDD------------ENDD------Tab_2->TC/CP TERMS--------------------------------ENDD---------------->
	
                    </div><!-- /.tab-content -->
                </div>
                <div style="text-align:center; margin-bottom:50px;">
				
					<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
					<input type="hidden" name="action" id="action" value="submit" />
					<input type="hidden" name="vesselrec" id="vesselrec" class="input-text" value="" />
				</div>
                <!-- Main content -->
                </form>
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/sort.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />
<script type="text/javascript">

$(document).ready(function(){
$("#selVName").val(<?php echo $obj->getFun3();?>);
$("#selCPType").val(<?php echo $obj->getFun10();?>);
$("#selCharterers").val('<?php echo $obj->getFun11();?>');
$("#selCharOperation").val('<?php echo $obj->getFun12();?>');
$("#selLawArbit").val(<?php echo $obj->getFun13();?>);
$("#selDurFixPeriod").val(<?php echo $obj->getFun45();?>);
$("#txtBrokCommPayableBy").val('<?php echo $obj->getFun67();?>');
$("#txtBankingDetails").val(<?php echo $obj->getFun70();?>);


	$('#txtDelLtTimeGMT,#txtReDelLtTimeGMT').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		todayBtn: true,
		minuteStep: 1,
		autoclose:true
	});
	
	$('#txtOffHireFrom,#txtOffHireTo').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
		}).on('changeDate', function(){
		
		calculate_days();
		});
//	$("input[type=text]").attr('disabled', true);
//	$("input[type=select]").attr('disabled', true);
	$('#tripspan_1,#tripspan_2,#tripspan_3,#periodspan_1,#periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').hide();
		
	if($("#selDurFixPeriod").val() == 1)
	{
		$('#tripspan_1,#tripspan_2,#tripspan_3').show();
		$('#periodspan_1,periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').hide();
	}
	else if($("#selDurFixPeriod").val() == 2)
	{
		$('#periodspan_1,#periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').show();
		$('#tripspan_1,#tripspan_2,#tripspan_3').hide();
	}
	else
	{
		$('#tripspan_1,#tripspan_2,#tripspan_3,#periodspan_1,#periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').hide();
	}
	
	if($("#txtDailyGrossHireUSD").val()!="")
	{
		var addComm 		= $("#txtAddCommPerct").val();
		var dailyGrossHire  = $("#txtDailyGrossHireUSD").val();
		var addComm1		= ((addComm*dailyGrossHire)/100);
		$("#txtAddComm1").val(parseFloat(addComm1).toFixed(2));	
		
		var brokerComm		= $("#txtChartererAcc").val();
		var calBrokerComm	= ((brokerComm*dailyGrossHire)/100);
		$("#txtBrokerComm").val(parseFloat(calBrokerComm).toFixed(2));	
	}
	
	if($("#txtBrokCommPayableBy").val() == 'Charterer')
	{
		var dailyGrossHire1  = $("#txtDailyGrossHireUSD").val();
		var addComm2		 = $("#txtAddComm1").val();
		var brokerComm1		 = $("#txtBrokerComm").val();
		var cal1			 = parseFloat(addComm2) + parseFloat(brokerComm1);
	  	var cal2			 = parseFloat(dailyGrossHire1) - parseFloat(cal1);
		if(isNaN(cal2)){cal2 = 0.00;}
		$("#txtNettHire").val(parseFloat(cal2).toFixed(2));
	}
	
	if($("#txtBrokCommPayableBy").val() == 'Operator')
	{
		var dailyGrossHire2  = $("#txtDailyGrossHireUSD").val();
		var addComm3		 = $("#txtAddComm1").val();
		var cal1			 = parseFloat(dailyGrossHire2) - parseFloat(addComm3);
		if(isNaN(cal1)){cal1 = 0.00;}
		$("#txtNettHire").val(parseFloat(cal1).toFixed(2));
	}
	
	
	
	$("#txtVoyageEarnings,#txtDeliveryHFO,#txtDeliveryMGO,#txtPriceDelHFO,#txtPriceDelMGO,#txtSupercargoMeals,#txtHoldCleanInterm,#txtILOHCtcFix,#txtAddComm,#txtBrokerCommTCfix,#txtTCdays,#txtOffHireDays,#txtUtilisationDays,#txtOffHireRate,#txtNoOfTrips,#txtHireFixPerUSD,#txtOtherIncome,#txtDelHFOmt,#txtDelHFOmt2,#txtDelHFOusdmt,#txtDelHFOusdmt2,#txtDelMGOmt,#txtDelMGOmt2,#txtDelMGOusdmt,#txtDelMGOusdmt2,[id^=txtOtherIncome_],[id^=txtOtherExpense_],#txtTankCleaningExp,#txtADDExpLineItem,#txtWindForce,#txtSpeedLaden,#txtSpeedBallast,#txtCPSpeed,#txtFOConsLaden,#txtDOConsLaden,#txtFOConsBallast,#txtDOConsBallast,#txtFOConsLdg,#txtDOConsLdg,#txtFOConsDisch,#txtDOConsDisch,#txtFOConsIdle,#txtDOConsIdle,#txtLoadRate,#txtDischRate").numeric();
	
	$("#txtDate,#txtCPdate").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
	
	$("#txtDeldate,#txtDeldate2").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
		}).on('changeDate', function(){
		getTCDays();
	});
	 
	$('#txtDeliveryDate').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
	 }).on('changeDate', function(){
		$('#txtReDeliveryDate').datepicker('setStartDate', new Date(getString($(this).val())));
	 });
	 
	$('#txtReDeliveryDate').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
	 }).on('changeDate', function(){   });
	 
	 
	 $('#txtLaycanFrom').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
	 }).on('changeDate', function(){
		$('#txtLaycanTo').datepicker('setStartDate', new Date(getString($(this).val())));
	 });
	 
	$('#txtLaycanTo').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
	 }).on('changeDate', function(){   });
	
	$("#frm1").validate({
		rules: {
			selVName:"required",
			txtTCNo:"required",
			txtWindForce:"required",
			txtSpeedLaden:"required",
			txtSpeedBallast:"required",
			txtCPSpeed:"required",
			txtFOConsLdg:"required",
			txtDOConsLdg:"required",
			txtFOConsDisch:"required",
			txtDOConsDisch:"required",
			txtFOConsLaden:"required",
			txtDOConsLaden:"required",
			txtFOConsBallast:"required",
			txtDOConsBallast:"required",
			txtFOConsIdle:"required",
			txtDOConsIdle:"required",
			txtLoadRate:"required",
			txtDischRate:"required"
			},
		messages: {
			selVName:"*",
			},
	submitHandler: function(form)  {
		jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
				if(r){
					jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
					$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
					$("#popup_content").css({"background":"none","text-align":"center"});
					$("#popup_ok,#popup_title").hide();  
					frm1.submit();
					}
					else{return false;}
				});
		}
	});
getDelBunkerCalculation();
	getReDelBunkerCalculation();
	getFinalCalculation();
	getValue();
	getCurrencySpan();
	getDailyGrossHire();
});

function getCurrencySpan()
{
	if($("#selExchangeCurrency").val()!="")
	{
		$("#currencyspan").text("("+$("#selExchangeCurrency").val()+")");
	}
	else
	{
		$("#currencyspan").text('');
	}
}

function getDailyGrossHire()
{
	if($("#txtHireFixPerUSD").val()!='' && $("#txtExchangeRate").val()!='')
	{
		$('#txtHireFixPerUSD1').val(parseFloat($("#txtHireFixPerUSD").val()*$("#txtExchangeRate").val()).toFixed(2));
	}
	else
	{
		$('#txtHireFixPerUSD1').val(0);
	}
}

function getValue()
{
	if($("#selVName").val()!="")
	{
		$("#lblVesName").text($("#selVName option:selected").text());
	}
	else
	{
		$("#lblVesName").text('');
	}
}

function getString(var1)
{
   var var2 = var1.split('-');  
   return var2[2]+'/'+var2[1]+'/'+var2[0];
}

function getData()
{
	  if($("#selVName").val()!="")
	  {
		  $.post("options.php?id=42",{vessel1d:""+$("#selVName").val()+""}, function(data) 
			{
				var vsldata = JSON.parse(data);
				$.each(vsldata[$("#selVName").val()], function(index, array) {
				
					$('#txtVType').val(array['type']);
					$('#txtDWTS').val(array['dwtsum']);
					$('#txtDWTT').val(array['dwttrop']);
					$('#txtGCap').val(array['grain']);
					$('#txtBCap').val(array['bale']);
					
					$('#txtBFullSpeed').val(array['bfs']);
					$('#txtBEcoSpeed1').val(array['bes1']);
					$('#txtBEcoSpeed2').val(array['bes2']);
					$('#txtBFOFullSpeed').val(array['fobfs']);
					$('#txtBFOEcoSpeed1').val(array['fobes1']);
					$('#txtBFOEcoSpeed2').val(array['fobes2']);
					$('#txtBDOFullSpeed').val(array['dobfs']);
					$('#txtBDOEcoSpeed1').val(array['dobes1']);
					$('#txtBDOEcoSpeed2').val(array['dobes2']);
					
					$('#txtLFullSpeed').val(array['lfs']);
					$('#txtLEcoSpeed1').val(array['les1']);
					$('#txtLEcoSpeed2').val(array['les2']);
					$('#txtLFOFullSpeed').val(array['folfs']);
					$('#txtLFOEcoSpeed1').val(array['foles1']);
					$('#txtLFOEcoSpeed2').val(array['foles2']);
					$('#txtLDOFullSpeed').val(array['dolfs']);
					$('#txtLDOEcoSpeed1').val(array['doles1']);
					$('#txtLDOEcoSpeed2').val(array['doles2']);
					
					$('#txtPIFOFullSpeed').val(array['foidle']);
					$('#txtPWFOFullSpeed').val(array['fwking']);
					$('#txtPIDOFullSpeed').val(array['ddle']);
					$('#txtPWDOFullSpeed').val(array['dwking']);
					$('#vesselrec').val(array['num']);
					$('#txtFlag').val(array['flag']);
					$('#txtBuiltYear').val(array['year']);
					$('#txtGNRT').val(array['gnrt']);
					$('#txtLOA').val(array['loa']);
					$('#txtGear').val(array['gear']);
					$('#txtBeam').val(array['beam']);
					$('#txtTPC').val(array['tpc']);
					
					$('#txtBuildYard').val(array['BuildYard']);
					$('#txtYearBuild').val(array['year']);
					$('#txtFlag1').val(array['flag']);
					$('#txtPortOfRegis').val(array['txtPortOfRegis']);
					$('#txtIMOnum').val(array['txtIMOnum']);
					$('#txtOwnerPandI').val(array['txtOwnerPandI']);
					$('#txtCallSign').val(array['txtCallSign']);
					$('#txtInmarsatTel').val(array['txtInmarsatTel']);
					$('#txtInmarsatEmail').val(array['txtInmarsatEmail']);
					$('#txtLOA1').val(array['loa']);
					$('#txtBreadth').val(array['beam']);
					$('#txtSummerDWT').val(array['dwtsum']);
					$('#txtSummerDraft').val(array['txtSummerDraft']);
					$('#txtTPC1').val(array['tpc']);
					$('#txtClassID').val(array['txtClassID']);
					$('#txtLastSpSurvey').val(array['txtLastSpSurvey']);
					$('#txtLastDD').val(array['txtLastDD']);
					$('#txtGrossTonnage').val(array['GrossTonnage']);
					$('#txtNetTonnage').val(array['NetTonnage']);
					$('#txtSuezGRT').val(array['txtSuezGRT']);
					$('#txtSuezNRT').val(array['txtSuezNRT']);
					$('#txtPanamaNRT').val(array['PanamaNRT']);
					$('#txtGrainCap').val(array['txtGrainCap']);
					$('#txtBaleCap').val(array['txtBaleCap']);
					$('#txtCranes').val(array['txtCranes']);
					$('#txtGrabs').val(array['txtGrabs']);
					$('#txtKeelTopMast').val(array['KeelTopMast']);
					$('#txtWaterlineTopMast').val(array['WaterlineTopMast']);
					$('#txtCTankCap').val(array['CTankCap']);
					$('#txtCTankNoOfGrades').val(array['CTankNoOfGrades']);
					$('#txtCPumpCap').val(array['CPumpCap']);
					$('#txtCTotalSBTCap').val(array['CTotalSBTCap']);
				});
			});  
	  }
	  else
	  {
		  $('#txtVType,#txtDWTS,#txtDWTT,#txtGCap,#txtBCap,#txtBFullSpeed,#txtBEcoSpeed1,#txtBEcoSpeed2,#txtBFOFullSpeed,#txtBFOEcoSpeed1,#txtBFOEcoSpeed2,#txtBDOFullSpeed,#txtBDOEcoSpeed1,#txtBDOEcoSpeed2,#txtLFullSpeed,#txtLEcoSpeed1,#txtLEcoSpeed2,#txtLFOFullSpeed,#txtLFOEcoSpeed1,#txtLFOEcoSpeed2,#txtLDOFullSpeed,#txtLDOEcoSpeed1,#txtLDOEcoSpeed2,#txtPIFOFullSpeed,#txtPWFOFullSpeed,#txtPIDOFullSpeed,#txtPWDOFullSpeed,#vesselrec,#txtFlag,#txtGear,#txtBuiltYear,#txtGNRT,#txtLOA,#txtBuildYard,#txtYearBuild,#txtFlag1,#txtPortOfRegis,#txtIMOnum,#txtOwnerPandI,#txtCallSign,#txtInmarsatTel,#txtInmarsatEmail,#txtLOA1,#txtBreadth,#txtSummerDWT,#txtSummerDraft,#txtTPC1,#txtClassID,#txtLastSpSurvey,#txtLastDD,#txtGrossTonnage,#txtNetTonnage,#txtSuezGRT,#txtSuezNRT,#txtPanamaNRT,#txtGrainCap,#txtBaleCap,#txtCranes,#txtGrabs,#txtKeelTopMast,#txtWaterlineTopMast').val("");
	  }
	  getFinalCalculation();
}


function getValidate()
{
	getFinalCalculation();
	
}


function getChartererData()
{
	if($("#selCharOperation").val()!='')
	{
		$.post("options.php?id=59",{selCharOperation:""+$("#selCharOperation").val()+""}, function(data) 
			{
					$('#txtAddress').val(data);

			});
	}
	else
	{
		$('#txtAddress').val('');
	}
}

function getTCDays()
{
    if($("#txtDeldate").val()!='' && $("#txtDeldate2").val()!='')
	{
		var start= $("#txtDeldate").datepicker("getDate");
		var end= $("#txtDeldate2").datepicker("getDate");
		var days = (end- start) / (1000 * 60 * 60 * 24);
	}
	else
	{
	    var days = 0;
	}
 	$("#txtTCdays").val(parseInt(days));
	getFinalCalculation();
}


function calculate_days()
{
	if($('#txtOffHireFrom').val() != '' && $('#txtOffHireTo').val() != '')
	{
		var start = new Date(getString1($('#txtOffHireFrom').val()));
		var	end = new Date(getString1($('#txtOffHireTo').val()));
		var	diff = new Date(end - start);
		
		var diffSeconds = diff/1000;
		var HH = Math.floor(diffSeconds/3600);
		var MM = Math.floor(diffSeconds%3600)/60;
		var offhiretime = ((HH < 10)?("0" + HH):HH) + ":" + ((MM < 10)?("0" + MM):MM);
		var offhiredays = parseFloat(offhiretime)/24;
		if(isNaN(offhiredays)){offhiredays = 0.00;}
		$('#txtOffHireDays').val(parseFloat(offhiredays).toFixed(2));
	}
	else
	{
		$('#txtOffHireDays').val('0.00');
	}
	
	getFinalCalculation();
}

function getString1(var1)
{
	var var2 = var1.split(' ');
	var var3 = var2[0].split('-');	 
	return var3[2]+'/'+var3[1]+'/'+var3[0]+' '+var2[1]; 
}

function getFinalCalculation()
{
    //-----------------------
    var tcdays 		     = parseFloat($("#txtTCdays").val());if(isNaN(tcdays)){tcdays = 0.00;}
	var offHireDays  	 = parseFloat($("#txtOffHireDays").val());if(isNaN(offHireDays)){offHireDays = 0.00;}
	
	var cal1 = parseFloat(tcdays) - parseFloat(offHireDays);
	if(isNaN(cal1)){cal1 = 0.00;}
	$("#txtUtilisationDays").val(parseFloat(cal1).toFixed(2));
	
	var utilisationDays  = parseFloat($("#txtUtilisationDays").val());if(isNaN(utilisationDays)){utilisationDays = 0.00;}
	var nettHire		 = parseFloat($("#txtNettHire").val());if(isNaN(nettHire)){nettHire = 0.00;}
	var cal1			 = parseFloat(utilisationDays) * parseFloat(nettHire);
	if(isNaN(cal1)){cal1 = 0.00;}
	$("#txtNettRev").val(parseFloat(cal1).toFixed(2));
	
	var cVEmonth  			= parseFloat($("#txtCVEmonth").val());if(isNaN(cVEmonth)){cVEmonth = 0.00;}
	var utilisationDays1    = parseFloat($("#txtUtilisationDays").val());if(isNaN(utilisationDays1)){utilisationDays1 = 0.00;}
	var cal1			    = (parseFloat(cVEmonth) / 30);
	var cal2				= cal1 * parseFloat(utilisationDays1);
	if(isNaN(cal2)){cal2 = 0.00;}
	$("#txtCVE").val(parseFloat(cal2).toFixed(2));	
	
    //------------------lessoffhire------------------------
	var offHireRate		 = parseFloat($("#txtOffHireRate").val());if(isNaN(offHireRate)){offHireRate = 0.00;}
	var cal1			 = parseFloat(offHireDays) * parseFloat(offHireRate);
	if(isNaN(cal1)){cal1 = 0.00;}
	$("#txtLessOffHire").val(parseFloat(cal1).toFixed(2));
	
	//-----------------otherIncome-----------
	var nettRev  	 = parseFloat($("#txtNettRev").val());if(isNaN(nettRev)){nettRev = 0.00;}
	var lessOffHire  = parseFloat($("#txtLessOffHire").val());if(isNaN(lessOffHire)){lessOffHire = 0.00;}
	var cVE  		 = parseFloat($("#txtCVE").val());if(isNaN(cVE)){cVE = 0.00;}
	var otherIncome  = parseFloat($('[id^=txtOtherIncome_]').sum());if(isNaN(otherIncome)){otherIncome = 0.00;}
	var cal1		 = parseFloat(nettRev) - parseFloat(lessOffHire) + parseFloat(cVE) + parseFloat(otherIncome);
	if(isNaN(cal1)){cal1 = 0.00;}
	$("#txtTotalRev").val(parseFloat(cal1).toFixed(2));
	//--------------------------------------------
	
	//---------------otherExpense------------------
	var dailyOpen  	 		= parseFloat($("#txtDailyOpen").val());if(isNaN(dailyOpen)){dailyOpen = 0.00;}
	var principalVessel  	= parseFloat($("#txtPrincipalVessel").val());if(isNaN(principalVessel)){principalVessel = 0.00;}
	var interestVessel  	= parseFloat($("#txtInterestVessel").val());if(isNaN(interestVessel)){interestVessel = 0.00;}
	var surveyExpenses  	= parseFloat($("#txtSurveyExpenses").val());if(isNaN(surveyExpenses)){surveyExpenses = 0.00;}
	var holdCleanInter  	= parseFloat($("#txtHoldCleanInter").val());if(isNaN(holdCleanInter)){holdCleanInter = 0.00;}
	var TankCleaningExp2  	= parseFloat($("#txtTankCleaningExp2").val());if(isNaN(TankCleaningExp2)){TankCleaningExp2 = 0.00;}
	var ADDExpLineItem2  	= parseFloat($("#txtADDExpLineItem2").val());if(isNaN(ADDExpLineItem2)){ADDExpLineItem2 = 0.00;}
	var iLOHC1			  	= parseFloat($("#txtILOHC1").val());if(isNaN(iLOHC1)){iLOHC1 = 0.00;}
	var otherExpenses  		= parseFloat($('[id^=txtOtherExpense_]').sum());if(isNaN(otherExpenses)){otherExpenses = 0.00;}
	
	var cal2		 		= parseFloat(dailyOpen) + parseFloat(principalVessel) + parseFloat(interestVessel) + parseFloat(surveyExpenses) + parseFloat(holdCleanInter) + parseFloat(iLOHC1) + parseFloat(otherExpenses) + parseFloat(TankCleaningExp2) + parseFloat(ADDExpLineItem2);
	if(isNaN(cal2)){cal2 = 0.00;}
	$("#txtTotalExpenses").val(parseFloat(cal2).toFixed(2));
	
	var totalRev1  	 	= parseFloat($("#txtTotalRev").val());if(isNaN(totalRev1)){totalRev1 = 0.00;}
	var totalExpenses  	= parseFloat($("#txtTotalExpenses").val());if(isNaN(totalExpenses)){totalExpenses = 0.00;}
	var cal2		 	= parseFloat(totalRev1) - parseFloat(totalExpenses);
	if(isNaN(cal2)){cal2 = 0.00;}
	$("#txtVoyageEarnings").val(parseFloat(cal2).toFixed(2));
	
	
	var voyageEarnings  	= parseFloat($("#txtVoyageEarnings").val());if(isNaN(voyageEarnings)){voyageEarnings = 0.00;}
	var utilisationDays  	= parseFloat($("#txtUtilisationDays").val());if(isNaN(utilisationDays)){utilisationDays = 0.00;}
	var cal2		 		= parseFloat(voyageEarnings) / parseFloat(utilisationDays);
	if(isNaN(cal2) || cal2 == "-Infinity" || cal2 == "Infinity"){cal2 = 0.00;}
	$("#txtProfitPerDay").val(parseFloat(cal2).toFixed(2));
	//------------------------------------------------------------
}

function addOtherIncomeRow()
{
    var idd = $("#hddnOtherID").val();
	if($("#txtOtherIncomeText_"+idd).val()!='' && $("#txtOtherIncome_"+idd).val()!='')
	{
	    idd = parseInt(idd) + 1;
	    $('<tr class="input-text" id="row_otherincome_'+idd+'"><td><a href="#tb1" onClick="removeOtherIncome('+idd+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><input autocomplete="off" type="text" name="txtOtherIncomeText_'+idd+'" id="txtOtherIncomeText_'+idd+'" class="input-text" style="width:150px;" placeholder="Description" value="" /></td><td><input autocomplete="off" type="text" name="txtOtherIncome_'+idd+'" id="txtOtherIncome_'+idd+'" class="input-text" style="width:120px;" onKeyUp="getFinalCalculation();" placeholder="Other Income(USD)" value="" /></td></tr>').appendTo("#otherIncomebody");
		$("#hddnOtherID").val(idd);
		$("[id^=txtOtherIncome_]").numeric();
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeOtherIncome(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#row_otherincome_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}


function addOtherExpenseRow()
{
    var idd = $("#hddnOtherExID").val();
	if($("#txtOtherExpenseText_"+idd).val()!='' && $("#txtOtherExpense_"+idd).val()!='')
	{
	    idd = parseInt(idd) + 1;
	    $('<tr class="input-text" id="row_otherExpense_'+idd+'"><td><a href="#tb1" onClick="removeOtherExpense('+idd+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><input autocomplete="off" type="text" name="txtOtherExpenseText_'+idd+'" id="txtOtherExpenseText_'+idd+'" class="input-text" style="width:150px;" placeholder="Description" value="" /></td><td><input autocomplete="off" type="text" name="txtOtherExpense_'+idd+'" id="txtOtherExpense_'+idd+'" class="input-text" style="width:120px;" onKeyUp="getFinalCalculation();" placeholder="Other Expense(USD)" value="" /></td></tr>').appendTo("#otherExpensebody");
		$("#hddnOtherExID").val(idd);
		$("[id^=txtOtherExpense_]").numeric();
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function getDelBunkerCalculation()
{
	var txtDELID = $("#txtDELID").val();
	for(var i =1;i <=txtDELID;i++)
	{
		var txtDelQty      = parseFloat($("#txtDelQty_"+i).val());if(isNaN(txtDelQty)){txtDelQty = 0.00;}
		var txtDelPrice    = parseFloat($("#txtDelPrice_"+i).val());if(isNaN(txtDelPrice)){txtDelPrice = 0.00;}
		var amount          =   parseFloat(parseFloat(txtDelQty)*parseFloat(txtDelPrice));if(isNaN(amount)){amount = 0.00;}
		$('#txtDelAmount_'+i).val(parseFloat(amount).toFixed(2));
	}
	$("#txtTotalDelAmount").val($('[id^=txtDelAmount_]').sum());
}

function getReDelBunkerCalculation()
{
	var txtREDELID = $("#txtREDELID").val();
	for(var i =1;i <=txtREDELID;i++)
	{
		var txtReDelQty      = parseFloat($("#txtReDelQty_"+i).val());if(isNaN(txtReDelQty)){txtReDelQty = 0.00;}
		var txtReDelPrice    = parseFloat($("#txtReDelPrice_"+i).val());if(isNaN(txtReDelPrice)){txtReDelPrice = 0.00;}
		var amount          =   parseFloat(parseFloat(txtReDelPrice)*parseFloat(txtReDelQty));if(isNaN(amount)){amount = 0.00;}
		$('#txtReDelAmount_'+i).val(parseFloat(amount).toFixed(2));
	}
	$("#txtTotalReDelAmount").val($('[id^=txtReDelAmount_]').sum());
}


function removeOtherExpense(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#row_otherExpense_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}
 
</script>
</body>
</html>