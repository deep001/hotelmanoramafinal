<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];
if(@$_REQUEST['action'] == 'submit')
{
 	$msg = $obj->updateVesselMainMasterOne();
	header('Location:./fleet.php?msg='.$msg);
	exit();
 }
 
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-anchor"></i>&nbsp;Fleet &nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Fleet</li>
						<li class="active"><?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME")); ?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="fleet.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
					<div class="row">
						<div class="col-xs-12">
							<h2 style=" text-align:center;">
								Vessel Particulars ( <?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME")); ?> )                              
							</h2>                            
						</div><!-- /.col -->
					</div>
					<div class="row" style="height:20px">&nbsp;</div>	
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								Vessel Description                           
							</h2>                            
						</div><!-- /.col -->
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Vessel’s Name 
							<address>
								<input name="txtVName" type="text" class="form-control" id="txtVName" autocomplete="off" value="<?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME"));?>" readonly placeholder="Vessel’s Name"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Vessel’s Previous Name(S)
							<address>
								<input name="txtVPName" type="text" class="form-control" id="txtVPName" autocomplete="off" value="<?php echo $obj->getVesselParticularData('VP_NAME','vessel_master_1',$_REQUEST['id']);?>" placeholder="Vessel’s Previous Name(S)"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Date (S) Of Change
							<address>
								<input name="txtDOC" type="text" class="form-control datepiker" id="txtDOC" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_OF_CHANGE','vessel_master_1',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATE_OF_CHANGE','vessel_master_1',$_REQUEST['id']) == "1970-01-01" || $obj->checkVesselDate('DATE_OF_CHANGE','vessel_master_1',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_OF_CHANGE','vessel_master_1',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Flag
							<address>
								<select  name="selFlag" class="select form-control" id="selFlag" onChange="getData_F(1)" disabled="disabled">
									<?php $obj->getCountryNameList(); ?>
								</select>
							</address>
						</div><!-- /.col -->
						
					</div>
					<hr>
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Month/Year And Where Built
							<address>
								<input name="txtBuiltDate" type="text" class="form-control" id="txtBuiltDate"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"YEARBUILT");?>" readonly placeholder="Month/Year And Where Built"/>
								
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							&nbsp;
							<address>
								<select  name="selCountry" class="select form-control" id="selCountry">
									<?php $obj->getCountryNameList(); ?>
								</select>
							</address>
						</div><!-- /.col -->
					</div>
					<hr>
				
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Yard Name And Number
							<address>
								<input name="txtYName" type="text" class="form-control" id="txtYName"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('YARD_NAME','vessel_master_1',$_REQUEST['id']);?>" placeholder="Yard Name And Number"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Class Registration Number
							<address>
								<input name="txtCRNo" type="text" class="form-control" id="txtCRNo"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CLASS_R_NO','vessel_master_1',$_REQUEST['id']);?>" placeholder="Class Registration Number"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Official Number
							<address>
								<input name="txtOfficialNo" type="text" class="form-control" id="txtOfficialNo"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('OFFICIAL_NO','vessel_master_1',$_REQUEST['id']);?>" placeholder="Official Number"/>
							</address>
						</div><!-- /.col -->
						
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							IMO Number
							<address>
								<input name="txtIMONo" type="text" class="form-control" id="txtIMONo"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"IMO_NO");?>" readonly placeholder="IMO Number"/>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Port Of Registry
							<address>
								<select  name="selPort" class="select form-control" id="selPort">
									<?php $obj->getPortList(); ?>
								</select>
							</address>
						</div><!-- /.col -->
						
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							Owner Full Style And Contact Number For Operational Purposes , If Appropriate 
							<address>
								<textarea name="txtOwner" id="txtOwner" class="form-control areasize" placeholder="Owner Full Style And Contact Number For Operational Purposes , If Appropriate "><?php echo $obj->getVesselParticularData('OWNERS_FULL','vessel_master_1',$_REQUEST['id']);?></textarea>
							</address>
						</div><!-- /.col -->
					</div>
				
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							Managers Full Style And Contact Number For Operational Purpose, If Appropriate
							<address>
								<textarea name="txtManager" id="txtManager" class="form-control areasize" placeholder="Managers Full Style And Contact Number For Operational Purpose, If Appropriate"><?php echo $obj->getVesselParticularData('MANAGERS_FULL','vessel_master_1',$_REQUEST['id']);?></textarea>
							</address>
						</div><!-- /.col -->
					</div>
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							If Contracting Party Are Disponent Owners State 
							<address>
								<input name="txtICPADOS" type="text" class="form-control" id="txtICPADOS"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('OWNERS_STATE','vessel_master_1',$_REQUEST['id']);?>" placeholder=""/>
							</address>
						</div><!-- /.col -->
					</div>
				
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							Full Style And Contact numbers For Operational Purpose
							<address>
								<textarea name="txtICPADOS_1" id="txtICPADOS_1" class="form-control areasize" placeholder="Full Style And Contact numbers For Operational Purpose"><?php echo $obj->getVesselParticularData('OPERATION_PURPOSE','vessel_master_1',$_REQUEST['id']);?></textarea>
							</address>
						</div><!-- /.col -->
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<br/>If Vessel On Time Charter Or Bareboat
							<address>
								<input name="txtICPADOS_2" type="text" class="form-control" id="txtICPADOS_2"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('BAREBOAT','vessel_master_1',$_REQUEST['id']);?>" placeholder="If Vessel On Time Charter Or Bareboat"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							When Vessel Delivered To Disponent Owners
							<address>
                            
								<input name="txtICPADOS_3" type="text" class="form-control datepiker" id="txtICPADOS_3"  autocomplete="off" value="<?php if($obj->checkVesselDate('DISPONENT_OWNER','vessel_master_1',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DISPONENT_OWNER','vessel_master_1',$_REQUEST['id']) == "1970-01-01" || $obj->checkVesselDate('DISPONENT_OWNER','vessel_master_1',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DISPONENT_OWNER','vessel_master_1',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy"/>
								
							</address>
						</div><!-- /.col -->
					</div>
				
					
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								Particulars Of Vessel                           
							</h2>                            
						</div><!-- /.col -->
					</div>
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Type Of Vessel
							<address>
								<input type="text" class="form-control" name="txtVType" id="txtVType"  autocomplete="off" value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_TYPE")); ?>" placeholder="Type Of Vessel" readonly />
							</address>
						</div><!-- /.col -->
					</div>
				
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Deadweight</h3>
						</div><!-- /.box-header -->					
						
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody>
									<tr>
										<th width="10%">&nbsp;</th>
										<th width="10%">DWT(MT)</th>
                                        <th width="10%">DWT(LT)</th>
										<th width="10%">Draft(M)</th>
                                        <th width="10%">Draft(FT)</th>
                                        <th width="10%">TPI(MT/Inch)</th>
                                        <th width="10%">TPI(LT/Inch)</th>
                                        <th width="10%">TPC(MT)</th>
                                        <th width="10%">TPC(LT)</th>
									</tr>
									<tr>
										<td>Summer</td>
										<td><input name="txtSumid_1" type="text" class="form-control numeric" id="txtSumid_1" autocomplete="off" onKeyUp="getCalculateDWT();" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"DWT");?>" readonly/></td>
                                        <td><input name="txtSumid_2" type="text" class="form-control" id="txtSumid_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DWT_LT','vessel_master_1',$_REQUEST['id']);?>" readonly/></td>
										<td><input name="txtSumid_3" type="text" class="form-control numeric" id="txtSumid_3" readonly autocomplete="off" onKeyUp="getCalculateDWT();" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"DRAFTM");?>"/></td>
                                        <td><input name="txtSumid_4" type="text" class="form-control" id="txtSumid_4" autocomplete="off" readonly value="<?php echo $obj->getVesselParticularData('DRAFT_FT','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtSumid_5" type="text" class="form-control numeric" id="txtSumid_5" autocomplete="off" onKeyUp="getCalculateDWT();" value="<?php echo $obj->getVesselParticularData('TPI_MT','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtSumid_6" type="text" class="form-control" id="txtSumid_6" readonly autocomplete="off" value="<?php echo $obj->getVesselParticularData('TPI_LT','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtSumid_7" type="text" class="form-control numeric" id="txtSumid_7" autocomplete="off" onKeyUp="getCalculateDWT();" value="<?php echo $obj->getVesselParticularData('TPC_MT','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtSumid_8" type="text" class="form-control" id="txtSumid_8" readonly autocomplete="off" value="<?php echo $obj->getVesselParticularData('TPC_LT','vessel_master_1',$_REQUEST['id']);?>" /></td>
									</tr>
									<tr>
										<td>Winter</td>
										<td><input name="txtWinid_1" type="text" class="form-control numeric" id="txtWinid_1" size="5" autocomplete="off" onKeyUp="getCalculateDWT();" value="<?php echo $obj->getVesselParticularData('WINTER_1','vessel_master_1',$_REQUEST['id']);?>"/></td>
										<td><input name="txtWinid_2" type="text" class="form-control" id="txtWinid_2" readonly size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_2','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtWinid_3" type="text" class="form-control numeric" id="txtWinid_3" size="5" autocomplete="off" onKeyUp="getCalculateDWT();" value="<?php echo $obj->getVesselParticularData('WINTER_3','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtWinid_4" type="text" class="form-control" id="txtWinid_4" size="5" readonly autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_4','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtWinid_5" type="text" class="form-control numeric" id="txtWinid_5" size="5" autocomplete="off" onKeyUp="getCalculateDWT();" value="<?php echo $obj->getVesselParticularData('WINTER_5','vessel_master_1',$_REQUEST['id']);?>"/></td>
										<td><input name="txtWinid_6" type="text" class="form-control" id="txtWinid_6" readonly size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_6','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtWinid_7" type="text" class="form-control numeric" id="txtWinid_7" size="5" autocomplete="off" onKeyUp="getCalculateDWT();" value="<?php echo $obj->getVesselParticularData('WINTER_7','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtWinid_8" type="text" class="form-control" id="txtWinid_8" readonly size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_8','vessel_master_1',$_REQUEST['id']);?>" /></td>
									</tr>
									
									<tr>
										<td>Tropical</td>
										<td><input name="txtTropicalid_1" type="text" class="form-control numeric" id="txtTropicalid_1" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_1','vessel_master_1',$_REQUEST['id']);?>"/></td>
										<td><input name="txtTropicalid_2" type="text" class="form-control" readonly id="txtTropicalid_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_2','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtTropicalid_3" type="text" class="form-control numeric" id="txtTropicalid_3" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_3','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtTropicalid_4" type="text" class="form-control" id="txtTropicalid_4" size="5" readonly autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_4','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtTropicalid_5" type="text" class="form-control numeric" id="txtTropicalid_5" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_5','vessel_master_1',$_REQUEST['id']);?>"/></td>
										<td><input name="txtTropicalid_6" type="text" class="form-control" readonly id="txtTropicalid_6" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_6','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtTropicalid_7" type="text" class="form-control numeric" id="txtTropicalid_7" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_7','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtTropicalid_8" type="text" class="form-control" readonly id="txtTropicalid_8" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_8','vessel_master_1',$_REQUEST['id']);?>" /></td>
									</tr>
									
									<tr>
										<td>Summer Timber</td>
										<td><input name="txtSumTimid_1" type="text" class="form-control numeric" id="txtSumTimid_1" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SUMMER_TIM_1','vessel_master_1',$_REQUEST['id']);?>"/></td>
										<td><input name="txtSumTimid_2" type="text" class="form-control" readonly id="txtSumTimid_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SUMMER_TIM_2','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtSumTimid_3" type="text" class="form-control numeric" id="txtSumTimid_3" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SUMMER_TIM_3','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtSumTimid_4" type="text" class="form-control" id="txtSumTimid_4" size="5" readonly autocomplete="off" value="<?php echo $obj->getVesselParticularData('SUMMER_TIM_4','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtSumTimid_5" type="text" class="form-control numeric" id="txtSumTimid_5" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SUMMER_TIM_5','vessel_master_1',$_REQUEST['id']);?>"/></td>
										<td><input name="txtSumTimid_6" type="text" class="form-control" readonly id="txtSumTimid_6" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SUMMER_TIM_6','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtSumTimid_7" type="text" class="form-control numeric" id="txtSumTimid_7" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SUMMER_TIM_7','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtSumTimid_8" type="text" class="form-control" readonly id="txtSumTimid_8" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SUMMER_TIM_8','vessel_master_1',$_REQUEST['id']);?>" /></td>
									</tr>
									
									<tr>
										<td>Winter Timber</td>
										<td><input name="txtWinTimid_1" type="text" class="form-control numeric" id="txtWinTimid_1" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_TIM_1','vessel_master_1',$_REQUEST['id']);?>"/></td>
										<td><input name="txtWinTimid_2" type="text" class="form-control" readonly id="txtWinTimid_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_TIM_2','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtWinTimid_3" type="text" class="form-control numeric" id="txtWinTimid_3" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_TIM_3','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtWinTimid_4" type="text" class="form-control" id="txtWinTimid_4" size="5" readonly autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_TIM_4','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtWinTimid_5" type="text" class="form-control numeric" id="txtWinTimid_5" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_TIM_5','vessel_master_1',$_REQUEST['id']);?>"/></td>
										<td><input name="txtWinTimid_6" type="text" class="form-control" readonly id="txtWinTimid_6" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_TIM_6','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtWinTimid_7" type="text" class="form-control numeric" id="txtWinTimid_7" size="5" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_TIM_7','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtWinTimid_8" type="text" class="form-control" readonly id="txtWinTimid_8" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WINTER_TIM_8','vessel_master_1',$_REQUEST['id']);?>" /></td>
									</tr>
									
									<tr>
										<td>Tropical Timber</td>
										<td><input name="txtTropicalTimid_1" type="text" class="form-control numeric" id="txtTropicalTimid_1" onKeyUp="getCalculateDWT();" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_TIM_1','vessel_master_1',$_REQUEST['id']);?>"/></td>
										<td><input name="txtTropicalTimid_2" type="text" class="form-control" readonly id="txtTropicalTimid_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_TIM_2','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtTropicalTimid_3" type="text" class="form-control numeric" id="txtTropicalTimid_3" onKeyUp="getCalculateDWT();" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_TIM_3','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtTropicalTimid_4" type="text" class="form-control" id="txtTropicalTimid_4" readonly size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_TIM_4','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtTropicalTimid_5" type="text" class="form-control numeric" id="txtTropicalTimid_5" onKeyUp="getCalculateDWT();" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_TIM_5','vessel_master_1',$_REQUEST['id']);?>"/></td>
										<td><input name="txtTropicalTimid_6" type="text" class="form-control" readonly id="txtTropicalTimid_6" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_TIM_6','vessel_master_1',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtTropicalTimid_7" type="text" class="form-control numeric" id="txtTropicalTimid_7" onKeyUp="getCalculateDWT();" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_TIM_7','vessel_master_1',$_REQUEST['id']);?>" /></td>
										<td><input name="txtTropicalTimid_8" type="text" class="form-control" readonly id="txtTropicalTimid_8" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_TIM_8','vessel_master_1',$_REQUEST['id']);?>" /></td>
									</tr>
								</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
				    
                    
                    <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Constants(MT)
							<address>
								<input name="txtConstantMT" id="txtConstantMT" type="text" class="form-control numeric" onKeyUp="getCalculateDWT();" autocomplete="off" value="<?php echo $obj->getVesselParticularData('CONSTANTMT','vessel_master_1',$_REQUEST['id']);?>"/>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Constants(LT)
							<address>
								<input name="txtConstantLT" id="txtConstantLT" type="text" readonly class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CONSTANTLT','vessel_master_1',$_REQUEST['id']);?>"/>
							</address>
						</div><!-- /.col -->
                    </div>
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Is Vessel Field For Transit Of : Panama Canal ?<br/><br/>
							<address>
								<select  name="selIVFFTO_1" class="select form-control" id="selIVFFTO_1">
									<?php $obj->getStatusTypeList();?>
								</select>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Is Vessel Field For Transit Of : Suez Canal ?<br/><br/>
							<address>
								<select  name="selIVFFTO_2" class="select form-control" id="selIVFFTO_2" >
									<?php $obj->getStatusTypeList(); ?>
								</select>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Is Vessel Field For Transit Of : ST Lawrence Seaway ?
							<address>
								<select  name="selIVFFTO_3" class="select form-control" id="selIVFFTO_3">
									<?php $obj->getStatusTypeList();?>
								</select>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							For Panama Canal Suitable Vessel State Deadweight All Told (Metric Tons) On 39ft 6ins (12.039M) (SG 0.9954)
							<address>
								<input name="txtPanama" id="txtPanama" type="text" class="form-control"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('PANAMA_CANAL_1','vessel_master_1',$_REQUEST['id']);?>"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Is Panama Deadweight All Told Affected By Vessel’S Bilge Turn Radius ?
							<address>
								<br/><select  name="selPanama" class="select form-control" id="selPanama">
									<?php $obj->getStatusTypeList(); ?>
								</select>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							For ST Lawrence Seaway Size Vessel State Deadweight All Told (Metric Tons) Basis 26ft (7.92M) Fresh Water
							<address>
								<input name="txtPanama_1" type="text" class="form-control" id="txtPanama_1"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('SEAWAY_1','vessel_master_1',$_REQUEST['id']);?>"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							GT/NT : International
							<address>
								<input name="txtGT_1" type="text" class="form-control" id="txtGT_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"GRT_NRT");?>/<?php echo $obj->getVesselIMOData($_REQUEST['id'],"NRT");?>" readonly placeholder="GT/NT : International"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							GT/NT : Suez
							<address>
								<input name="txtGT_2" type="text" class="form-control" id="txtGT_2"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('GT_SUEZ','vessel_master_1',$_REQUEST['id']);?>" placeholder="GT/NT : Suez"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							GT/NT : Panama
							<address>
								<input name="txtGT_3" type="text" class="form-control" id="txtGT_3"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('GT_PANAMA','vessel_master_1',$_REQUEST['id']);?>" placeholder="GT/NT : Panama"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							GT/NT : British
							<address>
								<input name="txtGT_4" type="text" class="form-control" id="txtGT_4"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('GT_BRITISH','vessel_master_1',$_REQUEST['id']);?>" placeholder="GT/NT : British"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Length Overall(METRES)
							<address>
								<input name="txtLOA" type="text" class="form-control" id="txtLOA"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"LOA");?>" readonly placeholder="Length Overall(METRES)"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Length Between Perpendiculars (METRES)
							<address>
								<input name="txtLBP" type="text" class="form-control" id="txtLBP"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('LBW','vessel_master_1',$_REQUEST['id']);?>" placeholder="Length Between Perpendiculars (METRES)"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Extreme Breadth(METRES)
							<address>
								<input name="txtEB" type="text" class="form-control" id="txtEB"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"EXT_BREADTH");?>" readonly placeholder="Extreme Breadth(METRES)"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Depth moulded(METRES)
							<address>
								<input name="txtDM" type="text" class="form-control" id="txtDM"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('DEPTH_MODULE','vessel_master_1',$_REQUEST['id']);?>" placeholder="Depth moulded(METRES)"/>
							</address>
						</div><!-- /.col -->
					</div>
					
					
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Distance (METRES) From Waterline to Top Of Hatch Coamings Basis 50 PCT Bunker - No Holds Flooded</h3>
						</div><!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody><tr>
									<th width="25%">&nbsp;</th>
									<th width="25%"> Ballast Condition (Ballast Holds Not Flooded)</th>
									<th width="25%"> Heavy Ballast Condition</th>
									<th width="25%"> Light Condition </th>
								</tr>
								<tr>
									<td>No 1 Hatch</td>
									<td><input name="txtNOH_1" type="text" class="form-control" id="txtNOH_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOH_1','vessel_master_1',$_REQUEST['id']);?>"/></td>
									<td><input name="txtNOH_2" type="text" class="form-control" id="txtNOH_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOH_2','vessel_master_1',$_REQUEST['id']);?>" /></td>
									<td><input name="txtNOH_3" type="text" class="form-control" id="txtNOH_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOH_3','vessel_master_1',$_REQUEST['id']);?>" /></td>
									
								</tr>
								<tr>
									<td>Midships</td>
									<td><input name="txtMS_1" type="text" class="form-control" id="txtMS_1" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MIDSHIPS_1','vessel_master_1',$_REQUEST['id']);?>"/></td>
									<td><input name="txtMS_2" type="text" class="form-control" id="txtMS_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MIDSHIPS_2','vessel_master_1',$_REQUEST['id']);?>"/></td>
									<td><input name="txtMS_3" type="text" class="form-control" id="txtMS_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MIDSHIPS_3','vessel_master_1',$_REQUEST['id']);?>"/></td>
								</tr>
								<tr>
									<td>Last Hatch</td>
									<td><input name="txtLH_1" type="text" class="form-control" id="txtLH_1" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('LAST_HATCH_1','vessel_master_1',$_REQUEST['id']);?>"/></td>
									<td><input name="txtLH_2" type="text" class="form-control" id="txtLH_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('LAST_HATCH_2','vessel_master_1',$_REQUEST['id']);?>"/></td>
									<td><input name="txtLH_3" type="text" class="form-control" id="txtLH_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('LAST_HATCH_3','vessel_master_1',$_REQUEST['id']);?>"/></td>
								</tr>
								
							</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
				
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Distance (METRES) From Keel to Top Of Hatch Coamings (Or Top Of Hatch Covers If Side-Rolling)</h3>
						</div><!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody><tr>
									<th width="50%">&nbsp;</th>
									<th width="50%">&nbsp;</th>
								</tr>
								<tr>
									<td>AT</td>
									<td><input name="txtAT_1" type="text" class="form-control" id="txtAT_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('AT_1','vessel_master_1',$_REQUEST['id']);?>"/ placeholder="At"></td>
									
								</tr>
								<tr>
									<td>No 1 Hatch</td>
									<td><input name="txtNOH_1_1" type="text" class="form-control" id="txtNOH_1_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOH_1_1','vessel_master_1',$_REQUEST['id']);?>" placeholder="No 1 Hatch"/></td>
								</tr>
								
								<tr>
									<td>Midships</td>
									<td><input name="txtMS_1_1" type="text" class="form-control" id="txtMS_1_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MIDSHIPS_1_1','vessel_master_1',$_REQUEST['id']);?>" placeholder="Midships"/></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td><input name="txtLH_1_1" type="text" class="form-control" id="txtLH_1_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('LAST_HATCH_1_1','vessel_master_1',$_REQUEST['id']);?>"/></td>
								</tr>
								
							</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Vessel’s ballasting And Deballasting Time (Metric Tons Per Hour)
							<address>
								<input name="txtVBDT" type="text" class="form-control" id="txtVBDT"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('VESSEL_BALLAST','vessel_master_1',$_REQUEST['id']);?>" placeholder=""/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Distance (METRES) From Keel To Highest Points Of Vessel (INM Antenna)
							<address>
								<input name="txtKeel_Distance" type="text" class="form-control" id="txtKeel_Distance"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('DISTANCE_KEEL','vessel_master_1',$_REQUEST['id']);?>" placeholder=""/>
							</address>
						</div><!-- /.col -->
						
					</div>
				
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Capacity Of : Ballast Tanks </h3>
						</div><!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody><tr>
									<th width="50%">&nbsp;</th>
									<th width="50%">&nbsp;</th>
								</tr>
								<tr>
									<td>Full Ballast</td>
									<td><input name="txtFB_1" type="text" class="form-control" id="txtFB_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('FULL_BALLAST_1','vessel_master_1',$_REQUEST['id']);?>" placeholder="Full Ballast"/></td>
									
								</tr>
								<tr>
									<td>Light Ballast</td>
									<td><input name="txtLB_1" type="text" class="form-control" id="txtLB_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('LIGHT_BALLAST_1','vessel_master_1',$_REQUEST['id']);?>" placeholder="Light Ballast"/></td>
								</tr>
								
								<tr>
									<td>Top Side Tank Empty</td>
									<td><input name="txtTSTE_1" type="text" class="form-control" id="txtTSTE_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TSTE_1','vessel_master_1',$_REQUEST['id']);?>" placeholder="Top Side Tank Empty"/></td>
								</tr>
								
								<tr>
									<td>&nbsp;</td>
									<td><textarea name="txtCOBT" id="txtCOBT" class="form-control areasize" ><?php echo $obj->getVesselParticularData('COBT','vessel_master_1',$_REQUEST['id']);?></textarea></td>
								</tr>
								
							</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							Capacity Of : Ballast Holds Capacity (State Which Hold (S))
							<address>
								<textarea name="txtCOBC" id="txtCOBC" class="form-control areasize" placeholder="Capacity Of : Ballast Holds Capacity (State Which Hold (S))"><?php echo $obj->getVesselParticularData('COBC','vessel_master_1',$_REQUEST['id']);?></textarea>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Constants Excluding Freshwater
							<address>
								<input name="txtCEF" type="text" class="form-control" id="txtCEF"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CEF','vessel_master_1',$_REQUEST['id']);?>" placeholder="Constants Excluding Freshwater"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Daily Freshwater Consumption
							<address>
								<input name="txtDFC" type="text" class="form-control" id="txtDFC"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('DFC','vessel_master_1',$_REQUEST['id']);?>" placeholder="Daily Freshwater Consumption"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Freshwater Capacity 
							<address>
								<input name="txtFWC" type="text" class="form-control" id="txtFWC"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('FWC','vessel_master_1',$_REQUEST['id']);?>" placeholder="Freshwater Capacity" />
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							State Capacity And Daily
							<address>
								<input name="txtSCAD" type="text" class="form-control" id="txtSCAD"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('SCAD','vessel_master_1',$_REQUEST['id']);?>" placeholder="State Capacity And Daily"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Production Of Evaporator
							<address>
								<input name="txtPOE" type="text" class="form-control" id="txtPOE"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('POE','vessel_master_1',$_REQUEST['id']);?>" placeholder="Production Of Evaporator"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Normal Fresh Water Reserve 
							<address>
								<input name="txtNFWR" type="text" class="form-control" id="txtNFWR"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('NFWR','vessel_master_1',$_REQUEST['id']);?>" placeholder="Normal Fresh Water Reserve"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Vessel is Fitted With Shaft Generator 
							<address>
								<select  name="selSHAFT_GEN" class="select form-control" id="selSHAFT_GEN" >
									<?php $obj->getStatusTypeList(); ?>
								</select>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Vessel’s On board Electrical Supply (V/Hz)
							<address>
								<input name="txtVOES" type="text" class="form-control" id="txtVOES"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('VOES','vessel_master_1',$_REQUEST['id']);?>" placeholder="Vessel’s On board Electrical Supply (V/Hz)"/>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-4 invoice-col">
							Details Of Alternative Supply, If Any
							<address>
								<input name="txtDOAS" type="text" class="form-control" id="txtDOAS"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('DOAS','vessel_master_1',$_REQUEST['id']);?>" placeholder="Details Of Alternative Supply, If Any"/>
							</address>
						</div><!-- /.col -->
					</div>
				
				<!----------------------------------------------------------------------------------------------------------->
				<!------------------------------------------End Main Form---------------------------------------------------->
				<!----------------------------------------------------------------------------------------------------------->
				
				<div class="row" style="height:20px"></div>
				<div class="row invoice-info">
					<div class="col-md-12">
						<!-- Custom Tabs -->
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#tab_1">CHAPTER 1</a></li>
								<li class=""><a data-toggle="tab" href="#tab_2">CHAPTER 2</a></li>
								<li class=""><a data-toggle="tab" href="#tab_3">CHAPTER 3</a></li>
								<li class=""><a data-toggle="tab" href="#tab_4">CHAPTER 4</a></li>
								<li class=""><a data-toggle="tab" href="#tab_5">CHAPTER 5</a></li>
								<li class=""><a data-toggle="tab" href="#tab_6">CHAPTER 6</a></li>
								<li class=""><a data-toggle="tab" href="#tab_7">CHAPTER 7</a></li>
								<!--<li class=""><a data-toggle="tab" href="#tab_8">CHAPTER 8</a></li>-->
							</ul>
                            <div class="tab-content">
								<div id="tab_1" class="tab-pane active">
									
									
									
										
									
								
								</div>
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab One Form------------------------------------------------->
								<!----------------------------------------------------------------------------------------------------------->
                                <div id="tab_2" class="tab-pane">
									
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Classification Society, Surveys And Certificates
												</h2>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Name Of Classification Society 
												<address>
													<select  name="selCLASS_SOC" class="select form-control" id="selCLASS_SOC" onChange="getData_C(1)" disabled="disabled" >
														<?php $obj->getClaSocList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Class Notation  
												<address>
													<textarea name="txtCLASS_NOT" id="txtCLASS_NOT" class="form-control areasize" placeholder="Class Notation"><?php echo $obj->getVesselParticularData('CLASS_NOTATION','vessel_master_2',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Date Of Last Special Survey<br/><br/>
												<address>
													<input name="txtDOLSS" type="text" class="form-control datepiker" id="txtDOLSS" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_LAST_1','vessel_master_2',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATE_LAST_1','vessel_master_2',$_REQUEST['id']) == "1970-01-01" || $obj->checkVesselDate('DATE_LAST_1','vessel_master_2',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_LAST_1','vessel_master_2',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
													
													
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Date Of Last Annual Survey<br/><br/>
												<address>
													<input name="txtDOLAS" type="text" class="form-control datepiker" id="txtDOLAS" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_LAST_2','vessel_master_2',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATE_LAST_2','vessel_master_2',$_REQUEST['id']) == "1970-01-01" || $obj->checkVesselDate('DATE_LAST_2','vessel_master_2',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_LAST_2','vessel_master_2',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Vessel Entered In Classification Approved Enhanced Survey Programme
												<address>
													<select  name="selCLASS_1" class="select form-control" id="selCLASS_1" >
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/><br/>Date Of Last Inspection
												<address>
													<input name="txtDOLI" type="text" class="form-control datepiker" id="txtDOLI" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_INS_1','vessel_master_2',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATE_INS_1','vessel_master_2',$_REQUEST['id']) == "1970-01-01" || $obj->checkVesselDate('DATE_INS_1','vessel_master_2',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_INS_1','vessel_master_2',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />													
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/><br/>Date Of Next Inspection
												<address>
													<input name="txtDONI" type="text" class="form-control datepiker" id="txtDONI" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_INS_2','vessel_master_2',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATE_INS_2','vessel_master_2',$_REQUEST['id']) == "1970-01-01" || $obj->checkVesselDate('DATE_INS_2','vessel_master_2',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_INS_2','vessel_master_2',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Dose Vessel Comply With IACS Unified Requirements Regarding Number 1 Cargo Hold And Double Bottom Tank Steel Structure ?
												<address>
													<select  name="selSTR" class="select form-control" id="selSTR">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Has This Compliance Been Verified By The Classification Society? 
												<address>
													<select  name="selSTR_1" class="selectc form-control" id="selSTR_1">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="box">
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody><tr>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
													</tr>
													<tr>
														<td>Date And Place Of Last DryDock</td>
														<td>
															<input name="txtDate_Dry" type="text" class="form-control datepiker" id="txtDate_Dry" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_DRYDOCK','vessel_master_2',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATE_DRYDOCK','vessel_master_2',$_REQUEST['id']) == "1970-01-01" || $obj->checkVesselDate('DATE_DRYDOCK','vessel_master_2',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_DRYDOCK','vessel_master_2',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
														
														</td>
														<td>
															<select  name="selPOLD" class="select form-control" id="selPOLD">
																<?php $obj->getPortList(); ?>
															</select>
														</td>
													</tr>
												</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												Has Vessel Been Involved in any Groundings Or Collision in last 12 Month? If So Give Full Details
												<address>
													<textarea name="txtGROUND" id="txtGROUND" class="form-control areasize" placeholder="Has Vessel Been Involved in any Groundings Or Collision in last 12 Month? If So Give Full Details"><?php echo $obj->getVesselParticularData('GROUND','vessel_master_2',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>
												Is Vessel ISM Certified?
												<address>
													<select  name="selISM" class="select form-control" id="selISM">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												State - DOC (DOCUMENT OF COMPLIANCE) Certificate Number/ISsuing Authority
												<address>
													<input name="txtSTATE_DOC" type="text" class="form-control" id="txtSTATE_DOC"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('STATE_DOC','vessel_master_2',$_REQUEST['id']);?>" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												State - SMC (SAFETY MANAGEMENT) Certificate Number/Issuing Authority
												<address>
													<input name="txtSTATE_SMC" type="text" class="form-control" id="txtSTATE_SMC"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('STATE_SMC','vessel_master_2',$_REQUEST['id']);?>"  />
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="box">
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody><tr>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
													</tr>
													<tr>
														<td>Give Date Of Last And Next Audit</td>
														<td>
															<input name="txtLAST_AUDIT" type="text" class="form-control datepiker" id="txtLAST_AUDIT" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_LAST_AUDIT','vessel_master_2',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATE_LAST_AUDIT','vessel_master_2',$_REQUEST['id']) == "1970-01-01" || $obj->checkVesselDate('DATE_LAST_AUDIT','vessel_master_2',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_LAST_AUDIT','vessel_master_2',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
														</td>
														<td>
															<input name="txtNEXT_AUDIT" type="text" class="form-control datepiker" id="txtNEXT_AUDIT" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_NEXT_AUDIT','vessel_master_2',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATE_NEXT_AUDIT','vessel_master_2',$_REQUEST['id']) == "1970-01-01" || $obj->checkVesselDate('DATE_NEXT_AUDIT','vessel_master_2',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_NEXT_AUDIT','vessel_master_2',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
														</td>
													</tr>
												</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										
										
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												State Outstanding Recommendations, If Any
												<address>
													<textarea name="txtSTATE_OUTS" id="txtSTATE_OUTS" class="form-control areasize" placeholder="State Outstanding Recommendations, If Any"><?php echo $obj->getVesselParticularData('STATE_OUTS','vessel_master_2',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="box">
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody><tr>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
														<th width="33%">&nbsp;</th>
													</tr>
													<tr>
														<td>Advise Date And Place Of Last Port State Control</td>
														<td>
															<input name="txtADVISE_DATE" type="text" class="form-control datepiker" id="txtADVISE_DATE" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_ADVISE','vessel_master_2',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATE_ADVISE','vessel_master_2',$_REQUEST['id']) == "1970-01-01" || $obj->checkVesselDate('DATE_ADVISE','vessel_master_2',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_ADVISE','vessel_master_2',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
														</td>
														<td>
															<select  name="selADVISE_PORT" class="select form-control" id="selADVISE_PORT">
																<?php $obj->getPortList(); ?>
															</select>
														</td>
													</tr>
												</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Did Vessel Pass Most Recent Port State Control Inspection Without Detention
												<address>
													<select  name="selDETEN" class="select form-control" id="selDETEN">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
										
											<div class="col-sm-12 invoice-col">
												State Outstanding Recommendations , If Any
												<address>
													<textarea name="txtSTATE_OUTS_1" id="txtSTATE_OUTS_1" class="form-control areasize" placeholder="State Outstanding Recommendations , If Any"><?php echo $obj->getVesselParticularData('STATE_OUTS_1','vessel_master_2',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Is Vessel’s Crew Covered By Full ITF Or Bona Fide Trade Union Agreement Acceptable To ITF?
												<address>
													<textarea name="txtITF" id="txtITF" class="form-control areasize" placeholder="Is Vessel’s Crew Covered By Full ITF Or Bona Fide Trade Union Agreement Acceptable To ITF?"><?php echo $obj->getVesselParticularData('ITF','vessel_master_2',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												If Vessel Has ITF Agreement State Number, Date Of Issue And Expiry Date 
												<address>
													<textarea name="txtITF_AGREE" id="txtITF_AGREE" class="form-control areasize" placeholder="If Vessel Has ITF Agreement State Number, Date Of Issue And Expiry Date"><?php echo $obj->getVesselParticularData('ITF_AGREE','vessel_master_2',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
										</div>
										<div class="box">
											<div class="box-header">
												<h3 class="box-title">Certificates </h3>
											</div><!-- /.box-header -->
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody id="tb2">
														<tr>
															<th width="20%">Certificate Name</th>
															<th width="20%">Date Of Issue</th>
															<th width="20%">Date Of Last Annual Endorsement</th>
															<th width="20%">Date Of Expiry</th>
															<th width="15%">Upload</th>
														</tr>
														
														<?php
														$sql = "select * from vessel_master_slave where VESSEL_IMO_ID=".$_REQUEST['id'];
														$res = mysql_query($sql);
														$rec = mysql_num_rows($res);
														
														if($rec == 0)
														{
														?>
														<tr>
															<td>
																<select  name="selCert_1" class="select form-control" id="selCert_1">
																	<?php $obj->getCertificateList(); ?>
																</select>
															</td>
															<td>
																<input name="txtIDate_1" type="text" class="form-control datepiker" id="txtIDate_1" autocomplete="off" placeholder="dd-mm-yyyy"/>
															</td>
															<td>
																<input name="txtLAIDate_1" type="text" class="form-control datepiker" id="txtLAIDate_1" autocomplete="off" placeholder="dd-mm-yyyy"/>
															</td>
															<td>
																<input name="txtEDate_1" type="text" class="form-control datepiker" id="txtEDate_1" autocomplete="off" placeholder="dd-mm-yyyy"/>
															</td>
															<td>
																<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="">
																	<i class="fa fa-paperclip"></i> Attachment
																	<input type="file" class="form-control" name="txtfile_1" id="txtfile_1" title="" data-widget="Add Inspector's Report" data-toggle="tooltip" data-original-title="Add Inspector's Report" />
																</div>											
															</td>
														</tr>
														<?php }else{ ?>
														<?php 
															$i =1;
															while($rows = mysql_fetch_assoc($res))
															{
														?>
															<tr>
																<td>
																	<select  name="selCert_<?php echo $i;?>" class="select form-control" id="selCert_<?php echo $i;?>" >
																		<?php $obj->getCertificateUpdateList($rows['CERTIFICATE_ID']); ?>
																	</select>
																</td>
																<td>
																	<input name="txtIDate_<?php echo $i;?>" type="text" class="form-control datepiker" id="txtIDate_<?php echo $i;?>" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_ISSUE','vessel_master_slave',$_REQUEST['id']) == "0000-00-00" || $rows['DATE_ISSUE'] == "1970-01-01"  || $rows['DATE_ISSUE'] == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_ISSUE','vessel_master_slave',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
																</td>
																<td>
																	<input name="txtLAIDate_<?php echo $i;?>" type="text" class="form-control datepiker" id="txtLAIDate_<?php echo $i;?>" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_LAST','vessel_master_slave',$_REQUEST['id']) == "0000-00-00" || $rows['DATE_LAST'] == "1970-01-01" || $rows['DATE_LAST'] == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_LAST','vessel_master_slave',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
																	
																</td>
																<td>
																	<input name="txtEDate_<?php echo $i;?>" type="text" class="form-control datepiker" id="txtEDate_<?php echo $i;?>" autocomplete="off" value="<?php if($obj->checkVesselDate('DATE_EXPIRY','vessel_master_slave',$_REQUEST['id']) == "0000-00-00" || $rows['DATE_EXPIRY'] == "1970-01-01" || $rows['DATE_EXPIRY'] == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATE_EXPIRY','vessel_master_slave',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
																</td>
																<?php if($obj->getVesselParticularData("UPLOAD","vessel_master_slave",$_REQUEST['id']) == ""){ ?>
																<td>
																											
																</td>
																<?php }else{ ?>
																<td>
																	<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="">
																		<i class="fa fa-paperclip"></i> Attachment
																		<input type="file" class="form-control" name="txtfile_<?php echo $i;?>" id="txtfile_<?php echo $i;?>" title="" data-widget="Add Inspector's Report" data-toggle="tooltip" data-original-title="Add Inspector's Report" /><br/><br/><br/>
																	</div>
																	&nbsp;&nbsp;<a href="../../attachment/<?php echo $rows['UPLOAD'];?>" target="_blank" data-toggle="tooltip" data-original-title="View Previous Upload"><i class="" style="font-size:15px; margin-left:36px;"></i>View</a>
																	
																</td>
																<?php } ?>
															</tr>
														<?php } ?>
														<?php } ?>
														<?php 
															if($rec == 0){$value = 1;}
															else{$value = $rec;}
														?>
													</tbody>
														<tr>
															<td colspan="2"></td>
															<td colspan="1">
																<input type="hidden" class="input" name="txtCID" id="txtCID" value="<?php echo $value; ?>"/>
																<button type="button" onClick="addCertificateRow();" name="button" id="button" class="form-control btn btn-success">Add</button>
															</td>
															<td colspan="2"></td>
														</tr>
												</table>
											</div><!-- /.box-body -->
										</div>
										
								</div><!-- /.tab_2-pane -->
								
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Two Form------------------------------------------------->
								<!----------------------------------------------------------------------------------------------------------->
								
								<div id="tab_3" class="tab-pane">
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Crew                   
												</h2>
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Number Of Crew
												<address>
													<input name="txtNOC" type="text" class="form-control" id="txtNOC"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOC','vessel_master_3',$_REQUEST['id']);?>" placeholder="Number Of Crew"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Name And Nationality Of Master 
												<address>
													<input name="txtNOM" type="text" class="form-control" id="txtNOM"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOM','vessel_master_3',$_REQUEST['id']);?>" placeholder="Name And Nationality Of Master" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Nationality Of Officers
												<address>
													<input name="txtNOO" type="text" class="form-control" id="txtNOO"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOO','vessel_master_3',$_REQUEST['id']);?>" placeholder="Nationality Of Officers"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Nationality Of Crew
												<address>
													<input name="txtNOCR" type="text" class="form-control" id="txtNOCR"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOCR','vessel_master_3',$_REQUEST['id']);?>" placeholder="Nationality Of Crew"/>
												</address>
											</div><!-- /.col -->
											
										</div>
										
										
										
								</div><!-- /.tab_3-pane -->
								
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Three Form----------------------------------------------->
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_4" class="tab-pane">
								
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Cargo Arrangements Holds
												</h1>
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												<br/>Number Of Holds
												<address>
													<input name="txtNOH" type="text" class="form-control" id="txtNOH"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"NOH");?>" readonly placeholder="Number Of Holds"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Are Vessel’s Holds Clear And Free Of Any Obstructions?
												<address>
													<select  name="selHOLDS_CLEAR" class="select form-control" id="selHOLDS_CLEAR">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
										
											<div class="col-sm-12 invoice-col">
												GRAIN/BALE Capacity In Holds
												<address>
													<textarea name="txtGRAIN" id="txtGRAIN" class="form-control areasize" placeholder="GRAIN/BALE Capacity In Holds"><?php echo $obj->getVesselParticularData('GRAIN','vessel_master_4',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
										
											<div class="col-sm-12 invoice-col">
												Is Vessel Strengthened For The Carriage Of Heavy Cargoes? If Yes State Witch Holds May Be Left Empty
												<address>
													<textarea name="txtCARRIAGE" id="txtCARRIAGE" class="form-control areasize" placeholder="Is Vessel Strengthened For The Carriage Of Heavy Cargoes? If Yes State Witch Holds May Be Left Empty"><?php echo $obj->getVesselParticularData('CARRIAGE','vessel_master_4',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Tanktop Steel And Suitable For Grab Discharge?
												<address>
												<address>
													<select  name="selTANKTOP" class="select form-control" id="selTANKTOP">
														<?php $obj->getStatusTypeList();?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												State Whether Bulkhead Corrugations Vertical Or Horizontal
												<address>
												<address>
													<input name="txtBULKHEAD" type="text" class="form-control" id="txtBULKHEAD"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('BULKHEAD','vessel_master_4',$_REQUEST['id']);?>" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Tanktop Strength (METRIC TONS PER SQM)
												<address>
													<textarea name="txtSQM" id="txtSQM" class="form-control" placeholder="Tanktop Strength (METRIC TONS PER SQM)"><?php echo $obj->getVesselParticularData('SQM','vessel_master_4',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Are Holds CO2 Fitted?<br/><br/>
												<address>
													<select  name="selCO2" class="select form-control" id="selCO2">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Are Holds Fitted With Smoke Detection System?
												<address>
													<select  name="selDET_SYS" class="select form-control" id="selDET_SYS">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Vessel Fitted With Australian Type Approved Holds Ladders
												<address>
													<select  name="selLADDER" class="select form-control" id="selLADDER">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Has Vessel A functioning Class Certified LOADMASTER/LOADICATOR Or Similar Calculator
												<address>
													<select  name="selCALC" class="select form-control" id="selCALC">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Are Holds Hoppered At? : Hold Side <br/><br/>
												<address>
													<select  name="selHOLD_SIDE" class="select form-control" id="selHOLD_SIDE">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Are Holds Hoppered AT? : Forward Bulkhead <br/><br/>
												<address>
													<select  name="selHOLD_SIDE" class="select form-control" id="selHOLD_SIDE">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Are Holds Hoppered At?  : AFT Bulkhead <br/><br/>
												<address>
													<select  name="selAFT_BULKHEAD" class="select form-control" id="selAFT_BULKHEAD">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Are Holds Hoppered At? : Can Vessel’s Holds Be Described As Box Shaped<br/><br/>
												<address>
													<select  name="selSHAPED" class="select form-control" id="selSHAPED" >
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Measurement Of Any Tank SLOPES/HOPPERING(Height And Distance From Vessel’s Side At Tank Top) (METRES)
												<address>
													<input name="txtMOATS" type="text" class="form-control" id="txtMOATS"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOATS','vessel_master_4',$_REQUEST['id']);?>" />
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="box">
											<div class="box-header">
												<h3 class="box-title"> Flat Floor Measurement Of Cargo Holds At Tank Top (METRES)</h3>
											</div><!-- /.box-header -->
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody>
													<tr>
														<th width="25%">&nbsp;</th>
														<th width="25%">Length- mtrs</th>
														<th width="25%">Width(Fwd)-mtrs</th>
														<th width="25%">Width(Aft)- mtrs</th>
													</tr>
													<?php
														$arr = array("H1","H2","H3","H4","H5","H6","H7");
														$j = 1;
														for($i=0;$i<sizeof($arr);$i++)
														{
													?>
													<tr>
														<td><?php echo $arr[$i];?></td>
														<td><input name="txtHid_<?php echo $j;?>_1" type="text" class="input-text" id="txtHid_<?php echo $j;?>_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData($arr[$i].'_1','vessel_master_4',$_REQUEST['id']);?>"/></td>
														<td><input name="txtHid_<?php echo $j;?>_2" type="text" class="input-text" id="txtHid_<?php echo $j;?>_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData($arr[$i].'_2','vessel_master_4',$_REQUEST['id']);?>" /></td>
														<td><input name="txtHid_<?php echo $j;?>_3" type="text" class="input-text" id="txtHid_<?php echo $j;?>_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData($arr[$i].'_3','vessel_master_4',$_REQUEST['id']);?>" /></td>
													</tr>
													<?php $j++;
													}?>
												</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												Are Vessel’s Holds electrically Ventilated?If Yes State Number Of Airchanges Per Hour Basis Empty Holds 
												<address>
													<textarea name="txtVENT" id="txtVENT" class="form-control areasize" placeholder="Are Vessel’s Holds electrically Ventilated?If Yes State Number Of Airchanges Per Hour Basis Empty Holds " ><?php echo $obj->getVesselParticularData('VENT','vessel_master_4',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Deck And Hatches
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Number Of Hatches
												<address>
													<input name="txtNOHA" type="text" class="form-control" id="txtNOHA"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"NOHA");?>" readonly placeholder="Number Of Hatches"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Make And Type Of Hatch Covers 
												<address>
													<input name="txtNOHA_COV" type="text" class="form-control" id="txtNOHA_COV"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOHA_COV','vessel_master_4',$_REQUEST['id']);?>" placeholder="Make And Type Of Hatch Covers"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Hatch Size (METRES)
												<address>
													<textarea name="txtHATCH_SIZE" id="txtHATCH_SIZE" class="form-control areasize" readonly placeholder="Hatch Size (METRES)"><?php echo $obj->getVesselIMOData($_REQUEST['id'],"HATCH_SIZE");?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Strength Of Hatch Cover (METRIC TONS PER SQM)
												<address>
													<textarea name="txtHATCH_SQM" id="txtHATCH_SQM" class="form-control areasize" placeholder="Strength Of Hatch Cover (METRIC TONS PER SQM)"><?php echo $obj->getVesselParticularData('HATCH_SQM','vessel_master_4',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Distance From Ship’s Rail TO Near And Far Edge Of Hatch COVERS/COAMING Near And Far (METRES)
												<address>
													<textarea name="txtFAR" id="txtFAR" class="form-control areasize" placeholder="Distance From Ship’s Rail TO Near And Far Edge Of Hatch COVERS/COAMING Near And Far (METRES)"><?php echo $obj->getVesselParticularData('FAR','vessel_master_4',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Distance From Bow To Fore Of 1st Hold Opening (METRES)
												<address>
													<input name="txtOPEN" type="text" class="form-control" id="txtOPEN"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('OPEN_1','vessel_master_4',$_REQUEST['id']);?>" placeholder=""/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Distance From Stern To AFT Of Last Hold Opening (METRES)
												<address>
													<input name="txtLAST_OPEN" type="text" class="form-control" id="txtLAST_OPEN"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('LAST_OPEN','vessel_master_4',$_REQUEST['id']);?>" placeholder=""/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												State Deck Strength (METRIC TONS PER SQM)<br/><br/>
												<address>
													<input name="txtSTRENGTH_SQM" type="text" class="form-control" id="txtSTRENGTH_SQM"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('STR_SQM','vessel_master_4',$_REQUEST['id']);?>" placeholder=""/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Cement Hole Numbers Per Hold
												<address>
													<input name="txtCHNPH" type="text" class="form-control" id="txtCHNPH"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CHNPH','vessel_master_4',$_REQUEST['id']);?>" placeholder="Cement Hole Numbers Per Hold"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Diameter Of Holes
												<address>
													<input name="txtDOH" type="text" class="form-control" id="txtDOH"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('DOH','vessel_master_4',$_REQUEST['id']);?>" placeholder="Diameter Of Holes"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Position Of Holes
												<address>
													<input name="txtPOH" type="text" class="form-control" id="txtPOH"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('POH','vessel_master_4',$_REQUEST['id']);?>" placeholder="Position Of Holes"/>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Cargo Gear
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												<br/>If Geared State Make And Type
												<address>
													<input name="txtMAKE_TYPE" type="text" class="form-control" id="txtMAKE_TYPE"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"CARGO_GEAR");?>" readonly placeholder="If Geared State Make And Type"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Number Of Cranes And Where Situated
												<address>
													<input name="txtNOCS" type="text" class="form-control" id="txtNOCS"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"CRANESIZE");?>" readonly placeholder=""/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Outreach (METRES) Of Gear - Beyond Ship’s Rail
												<address>
													<input name="txtBSR" type="text" class="form-control" id="txtBSR"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('BSR','vessel_master_4',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Outreach (METRES) Of Gear - Beyond Ship’s Rail With Maximum Cargo Left On Hook<br/><br/>
												<address>
													<input name="txtHOOK" type="text" class="form-control" id="txtHOOK"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('HOOK','vessel_master_4',$_REQUEST['id']);?>" placeholder=""/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												If Gantry CRANES/HORIZONTAL Slewing Cranes state Minimum Clearance Distance Crane Hook To Top Of Hatch Coaming (METRES)
												<address>
													<input name="txtHATCH_COAM" type="text" class="form-control" id="txtHATCH_COAM"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('HATCH_COAM','vessel_master_4',$_REQUEST['id']);?>" placeholder=""/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Time Needed For Full Cycle With Maximum Cargo Lift On Hook<br/><br/>
												<address>
													<input name="txtHOOK_CARGO" type="text" class="form-control" id="txtHOOK_CARGO"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('HOOK_CARGO','vessel_master_4',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Is Gear Combinable For Heavy Life
												<address>
													<select  name="selH_LIFT" class="select form-control" id="selH_LIFT">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Are Winches ELECTRO-HYDRAULIC?
												<address>
													<select  name="selE_HYDRA" class="select form-control" id="selE_HYDRA">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												If Vessel Has Grabs on Board State Type And Capacity
												<address>
													<input name="txtTYPE_CAP" type="text" class="form-control" id="txtTYPE_CAP"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"GRABSIZE");?>" readonly/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Vessel Fitted With Sufficient Lights At Each Hatch For Night Work?
												<address>
													<select  name="selN_WORK" class="select form-control" id="selN_WORK">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Is Vessel Logs Fitted? If Yes State Number And Type Of STANCHIONS/SOCKETS, If On Board
												<address>
													<textarea name="txtBOARD" id="txtBOARD" class="form-control areasize" placeholder="Is Vessel Logs Fitted? If Yes State Number And Type Of STANCHIONS/SOCKETS, If On Board"><?php echo $obj->getVesselParticularData('BOARD','vessel_master_4',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
										</div>
										
								</div><!-- /.tab_4-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Four Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_5" class="tab-pane">
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Speed/Consumption/Fuel Engine
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="box">
											<div class="box-header">
												<h3 class="box-title">
													State Vessel’s Consumption At About 13,5 Knots (Up To Beaufort Scale Force 4/Douglas Sea State 3,No Adverse Currents) As Follows:
												</h3>
											</div><!-- /.box-header -->
											<div class="box-body no-padding">
												<table class="table table-striped">
													<tbody><tr>
														<th width="33%">&nbsp;</th>
														<th width="33%">About Metric Tons (MAIN ENGINE)</th>
														<th width="33%">About Metric Tons(AUXILIARIES)</th>
													</tr>
													<?php
														$arr_1 = array("Laden","Ballast");
														$j = 1;
														for($i=0;$i<sizeof($arr_1);$i++)
														{
														?>
														<tr>
															<td><?php echo $arr_1[$i];?></td>
															<td><input name="txtADid_<?php echo $j;?>_1" type="text" class="form-control" id="txtADid_<?php echo $j;?>_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData($arr_1[$i].'_1','vessel_master_5',$_REQUEST['id']);?>"/></td>
															<td><input name="txtADid_<?php echo $j;?>_2" type="text" class="form-control" id="txtADid_<?php echo $j;?>_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData($arr_1[$i].'_2','vessel_master_5',$_REQUEST['id']);?>" /></td>
														</tr>
													<?php $j++;}?>
												</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Bunker Grades
												<address>
													<input name="txtBUNKER_G" type="text" class="form-control" id="txtBUNKER_G"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('BUNKER_G','vessel_master_5',$_REQUEST['id']);?>" placeholder="Bunker Grades"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Bunker Capacities 100 .PCT Capacity
												<address>
													<input name="txtBUNKER_C" type="text" class="form-control" id="txtBUNKER_C"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('BUNKER_C','vessel_master_5',$_REQUEST['id']);?>" placeholder="Bunker Capacities 100 .PCT Capacity"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Port Consumption Per 24 Hours IDLE/WORKING (METRIC TONS)
												<address>
													<textarea name="txtP_WORK" id="txtP_WORK" class="form-control areasize" placeholder="Port Consumption Per 24 Hours IDLE/WORKING (METRIC TONS)"><?php echo $obj->getVesselParticularData('P_WORK','vessel_master_5',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Engine Make And Type
												<address>
													<input name="txtEMAT" type="text" class="form-control" id="txtEMAT"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('EMAT','vessel_master_5',$_REQUEST['id']);?>" placeholder="Engine Make And Type" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Max Output BHP/RPM
												<address>
													<input name="txtMOB" type="text" class="form-control" id="txtMOB"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOB','vessel_master_5',$_REQUEST['id']);?>" placeholder="Max Output BHP/RPM"/>
												</address>
											</div><!-- /.col -->
											
											
										</div>
								</div><!-- /.tab_5-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Five Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_6" class="tab-pane">
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Communications
												</h1>                            
											</div><!-- /.col -->
										</div>
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Name Of Radio Station Which Vessel Monitoring
												<address>
													<input name="txtNOR" type="text" class="form-control" id="txtNOR"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOR','vessel_master_6',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Specify Vessel’s Satellite Communication System
												<address>
													<input name="txtCOMM_SYS" type="text" class="form-control" id="txtCOMM_SYS"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('COMM_SYS','vessel_master_6',$_REQUEST['id']);?>" placeholder=""/>
												</address>
											</div><!-- /.col -->
										</div>
                                        
                                        <div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Communication Details
												</h1>                            
											</div><!-- /.col -->
										</div>
                                        
                                        <div class="row invoice-info">
											
                                            <div class="col-sm-4 invoice-col">
												Call Sign
												<address>
													<input name="txtCALL_SIGN" type="text" class="form-control" id="txtCALL_SIGN"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CALL_SIGN','vessel_master_6',$_REQUEST['id']);?>" placeholder="Call Sign"/>
												</address>
											</div><!-- /.col -->
                                            
                                            <div class="col-sm-4 invoice-col">
												INM-C
												<address>
													<input name="txtINMARSAT_Number" type="text" class="form-control" id="txtINMARSAT_Number"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('INMARSAT_NUMBER','vessel_master_6',$_REQUEST['id']);?>" placeholder="INMARSAT Number"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												INM-F TEL
												<address>
													<input name="txtTelex_Number" type="text" class="form-control" id="txtTelex_Number"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('TELEX_NUMBER','vessel_master_6',$_REQUEST['id']);?>" placeholder="Telex Number" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												INM-F FAX 
												<address>
													<input name="txtFax_Number" type="text" class="form-control" id="txtFax_Number"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('FAX_NUMBER','vessel_master_6',$_REQUEST['id']);?>" placeholder="Fax Number"/>
												</address>
											</div><!-- /.col -->
                                            
                                            <div class="col-sm-4 invoice-col">
												Email Address
												<address>
													<input name="txtEmail_Address" type="text" class="form-control" id="txtEmail_Address"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('EMAIL_ADDRESS','vessel_master_6',$_REQUEST['id']);?>" placeholder="Email Address"/>
												</address>
											</div><!-- /.col -->
                                            
                                            <div class="col-sm-4 invoice-col">
												MMSI No
												<address>
													<input name="txtMMSI_No" type="text" class="form-control" id="txtMMSI_No"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('MMSI_NUMBER','vessel_master_6',$_REQUEST['id']);?>" placeholder="MMSI No"/>
												</address>
											</div><!-- /.col -->
                                            
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Insurances
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												<br/>Hull And Machinery Insured value
												<address>
													<input name="txtHAMIV" type="text" class="form-control" id="txtHAMIV"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('HAMIV','vessel_master_6',$_REQUEST['id']);?>" placeholder="Hull And Machinery Insured value"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Name Of Owners P And I Insures 
												<address>
													<input name="txtPI" type="text" class="form-control" id="txtPI"  autocomplete="off" value="<?php echo $obj->getVendorNameBasedOnID($obj->getVesselIMOData($_REQUEST['id'],"P_I"));?>" style="width:98%;" readonly placeholder="Name Of Owners P And I Insures "/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Where is Owners Hull And Machinery Placed?<br/><br/>
												<address>
													<input name="txtPLACED" type="text" class="form-control" id="txtPLACED"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('PLACED','vessel_master_6',$_REQUEST['id']);?>" placeholder="Where is Owners Hull And Machinery Placed?"/>
												</address>
											</div><!-- /.col -->
										</div>
										
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Miscellaneous
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												State Last 5 (Five) Cargoes Carried And Load And Discharge Port(S) With Most Recent First
												<address>
													<textarea name="txtCARGO" id="txtCARGO" class="form-control areasize" placeholder="State Last 5 (Five) Cargoes Carried And Load And Discharge Port(S) With Most Recent First"><?php echo $obj->getVesselParticularData('CARGO','vessel_master_6',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Vessel Fitted For Carriage Of Grain in Accordance With Chapter V1 Of Solas 1974 And Amendments Without Requiring Bagging, Strapping And Securing When Loading A Full Cargo (DEADWEIGHT) Of Heavy Grain in Bulk (STOWAGE FACTOR 42 CUFT) With Ends Untrimmed?
												<address>
													<select  name="selCARGO_1" class="select form-control" id="selCARGO_1">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/><br/><br/><br/>State Number Of Holds Which May Be Left Slack Without Requiring Bagging, Strapping And Securing 
												<address>
													<input name="txtCARGO_2" type="text" class="form-control" id="txtCARGO_2"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CARGO_2','vessel_master_6',$_REQUEST['id']);?>" placeholder=""/>
												</address>
											</div><!-- /.col -->
											
										</div>
								</div><!-- /.tab_6-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Six Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
								
								
								<div id="tab_7" class="tab-pane">
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Container Bulkers/ Multi Purpose (Only To Be Completed If Applicable)               
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Capacity In Direct Stow Of TUE/FEU Basis : Empty
												<address>
													<input name="txtEMPTY" type="text" class="form-control" id="txtEMPTY"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('EMPTY','vessel_master_7',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Capacity In Direct Stow Of TEU/FEU Basis : Tons Homogeneous Weight
												<address>
													<input name="txtTHW" type="text" class="form-control" id="txtTHW"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('THW','vessel_master_7',$_REQUEST['id']);?>" placeholder=""/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-12 invoice-col">
												Are All Containers Within Reach Of Vessel’s GEAR:(YES/NO) If No State Self Sustained Capacity
												<address>
													<textarea name="txtGEAR" id="txtGEAR" class="form-control areasize" placeholder="Are All Containers Within Reach Of Vessel’s GEAR:(YES/NO) If No State Self Sustained Capacity"><?php echo $obj->getVesselParticularData('GEAR','vessel_master_7',$_REQUEST['id']);?></textarea>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												If Vessel Fitted With All Permanent And Loose FITTINGS/LASHING Materials For Above Number Of TEU/FEU?
												<address>
													<select  name="selFEU" class="select form-control" id="selFEU">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Vessel Fitted With Recent HOLES/SHOES On Tanktop And Container Shoes On Weatherdeck And Hatch Covers?
												<address>
													<select  name="selCOVERS" class="select form-control" id="selCOVERS">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Advise Stack Weight And Number Of Tiers ON/UNDERDECK - PER TEU
												<address>
													<input name="txtPER_TEU" type="text" class="form-control" id="txtPER_TEU"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('PER_TEU','vessel_master_7',$_REQUEST['id']);?>" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Advise Stack Weight And Number Of Tiers ON/UNDERDECK - PER FEU
												<address>
													<input name="txtPER_FEU" type="text" class="form-control" id="txtPER_FEU"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('PER_FEU','vessel_master_7',$_REQUEST['id']);?>"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Has Vessel A Container Spreader On Board?<br/><br/>
												<address>
													<input name="txtSPREAD_BOARD" type="text" class="form-control" id="txtSPREAD_BOARD"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('SPREAD_BOARD','vessel_master_7',$_REQUEST['id']);?>" placeholder="Has Vessel A Container Spreader On Board?"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Number And Type Of Reefer Plugs
												<address>
													<input name="txtREEFER_PLUGS" type="text" class="form-control" id="txtREEFER_PLUGS"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('REEFER_PLUGS','vessel_master_7',$_REQUEST['id']);?>" placeholder="Number And Type Of Reefer Plugs" />
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Tweendeckers               
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Has Vessel Folding Tweens?
												<address>
													<select  name="selTWEENS" class="select form-control" id="selTWEENS">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Number Of HOLDS/HATCHES
												<address>
													<input name="txtNOHH" type="text" class="form-control" id="txtNOHH"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('NOHH','vessel_master_7',$_REQUEST['id']);?>" placeholder="Number Of HOLDS/HATCHES"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Type Of Hatches
												<address>
													<input name="txtTOH" type="text" class="form-control" id="txtTOH"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('TOH','vessel_master_7',$_REQUEST['id']);?>" placeholder="Type Of Hatches"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Hatch Size (METRES) : WEATHERDECK
												<address>
													<input name="txtWEATHERDECK" type="text" class="form-control" id="txtWEATHERDECK"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('WEATHER_DECK','vessel_master_7',$_REQUEST['id']);?>" placeholder="Hatch Size (METRES) : WEATHERDECK"/>
												</address>
											</div><!-- /.col -->

											<div class="col-sm-4 invoice-col">
												Hatch Size (METRES) : TWEENDECK
												<address>
													<input name="txtTWEENDECK" type="text" class="form-control" id="txtTWEENDECK"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('TWEEN_DECK','vessel_master_7',$_REQUEST['id']);?>" placeholder="Hatch Size (METRES) : TWEENDECK"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Are Tweendeckers Flush ?
												<address>
													<select  name="selFLUSH" class="select form-control" id="selFLUSH">
														<?php $obj->getStatusTypeList();?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Strengths (METRIC TONS PER SQM) : TANKTOP<br/><br/>
												<address>
													<input name="txtTANKTOP" type="text" class="form-control" id="txtTANKTOP"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('TANKTOP','vessel_master_7',$_REQUEST['id']);?>" placeholder="Strengths (METRIC TONS PER SQM) : TANKTOP"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Strengths (METRIC TONS PER SQM) : WEATHERDECK
												<address>
													<input name="txtSQM_WD" type="text" class="form-control" id="txtSQM_WD"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('SQM_WD','vessel_master_7',$_REQUEST['id']);?>"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Strengths (METRIC TONS PER SQM) : HATCHCOVERS
												<address>
													<input name="txtSQM_HC" type="text" class="form-control" id="txtSQM_HC"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('SQM_HC','vessel_master_7',$_REQUEST['id']);?>"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												<br/>Is Vessel Fully Cargo Batten Fitted?
												<address>
													<select  name="selB_FITTED" class="select form-control" id="selB_FITTED" >
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Is Vessel CO2 FITTED/ELECTRICALLY VENTILATED?
												<address>
													<select  name="selCO2_FITT" class="select form-control" id="selCO2_FITT">
														<?php $obj->getStatusTypeList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h1 class="page-header">
													Supplementary Information For Specific COMMODITIES/TRAD
												</h1>                            
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-12 invoice-col">
												Remark
												<address>
													<textarea name="txtREMARK" id="txtREMARK" class="form-control areasize" readonly><?php echo $obj->getVesselIMOData($_REQUEST['id'],"REMARKS");?></textarea>
												</address>
											</div><!-- /.col -->
										</div>
										
								</div><!-- /.tab_7-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Seven Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_8" class="tab-pane">
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												Name
												<address>
													<input name="txtVName_1" type="text" class="form-control" id="txtVName_1" autocomplete="off" value="<?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME"));?>" readonly placeholder="Name"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Type
												<address>
													<input name="txtVType_1" type="text" class="form-control" id="txtVType_1"  autocomplete="off" value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_TYPE")); ?>" readonly placeholder="Type"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Built Date
												<address>
													<input name="txtBuiltDate_1" type="text" class="form-control" id="txtBuiltDate_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"YEARBUILT");?>" readonly placeholder="Built Date"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Flag
												<address>
													<select  name="selFlag_1" class="select form-control" id="selFlag_1" onChange="getData_F(0)" disabled="disabled">
														<?php $obj->getCountryNameList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Summer Dwat
												<address>
													<input name="txtSumid_1_1" type="text" class="form-control" id="txtSumid_1_1" autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"DWT");?>" readonly placeholder="Summer Dwat"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Summer Draft
												<address>
													<input name="txtSumid_2_1" type="text" class="form-control" id="txtSumid_2_1" style="width:98%;" autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"DRAFTM");?>" readonly placeholder="Summer Draft"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												LOA (M)
												<address>
													<input name="txtLOA_1" type="text" class="form-control" id="txtLOA_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"LOA");?>" readonly placeholder="LOA (M)"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Extreme Breadth(M)
												<address>
													<input name="txtEB_1" type="text" class="form-control" id="txtEB_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"EXT_BREADTH");?>" style="width:98%;" readonly placeholder="Extreme Breadth(M)"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												GRT 
												<address>
													<input name="txtGT_7" type="text" class="form-control" id="txtGT_7"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"GRT_NRT");?>" readonly placeholder="GRT / NRT : International"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Grain (M³)
												<address>
													<input name="txtGRAIN_1" type="text" class="form-control" id="txtGRAIN_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"GRAIN");?>" readonly placeholder="Grain (M³)"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Bale (M³)
												<address>
													<input name="txtBALE_1" type="text" class="form-control" id="txtBALE_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"BALE");?>" readonly placeholder="Bale (M³)"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Number Of Holds 
												<address>
													<input name="txtNOH_7" type="text" class="form-control" id="txtNOH_7"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"NOH");?>" readonly placeholder="Number Of Holds"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Number Of Hatches
												<address>
													<input name="txtNOHA_1" type="text" class="form-control" id="txtNOHA_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"NOHA");?>" readonly placeholder="Number Of Hatches"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Cargo Gear (MAIN DETAILS)
												<address>
													<input name="txtMAKE_TYPE_1" type="text" class="form-control" id="txtMAKE_TYPE_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"CARGO_GEAR");?>" readonly placeholder="Cargo Gear (MAIN DETAILS)"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Crane Size
												<address>
													<input name="txtNOCS_1" type="text" class="form-control" id="txtNOCS_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"CRANESIZE");?>" readonly placeholder="Crane Size"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Grab (MAIN DETAILS)
												<address>
													<input name="txtTYPE_CAP_1" type="text" class="form-control" id="txtTYPE_CAP_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"GRABSIZE");?>" readonly placeholder="Grab (MAIN DETAILS)"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												IMO Number
												<address>
													<input name="txtIMONo_1" type="text" class="form-control" id="txtIMONo_1"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"IMO_NO");?>" readonly placeholder="IMO Number"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Owners P&I
												<address>
													<input name="txtPI_1" type="text" class="form-control" id="txtPI_1"  autocomplete="off" value="<?php echo $obj->getVendorOnlyNameBasedOnID($obj->getVesselIMOData($_REQUEST['id'],"P_I"),"P_I");?>" readonly placeholder="Name Of Owners P And I Insurers"/>
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-4 invoice-col">
												Name Of Classification Society 
												<address>
													<select  name="selCLASS_SOC_1" class="select form-control" id="selCLASS_SOC_1" onChange="getData_C(0)" disabled="disabled">
														<?php $obj->getClaSocList(); ?>
													</select>
												</address>
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
                     					 <?php if($obj->getVesselMaster8Data($id,'ATTACHMENT') != ''){ $file = explode(",",$obj->getVesselMaster8Data($id,'ATTACHMENT'));if($obj->getVesselMaster8Data($id,'ATTACHMENT_NAME')==""){$filename = explode(",",$obj->getVesselMaster8Data($id,'ATTACHMENT')); }else{$filename = explode(",",$obj->getVesselMaster8Data($id,'ATTACHMENT_NAME'));}?>
											<div class="col-sm-6 invoice-col">
											  Previous Attachments
												<address>
														<table cellpadding="1" cellspacing="1" border="0" width="100%" align="left">
														<?php
														$j =1;
														for($i=0;$i<sizeof($file);$i++)
														{
														?>
														<tr height="20" id="row_file_<?php echo $j;?>">
															<td width="50%" align="left" class="input-text"  valign="top"><a href="../../attachment/<?php echo $file[$i]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="Click to view file"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $filename[$i];?></a><input type="hidden" name="file_<?php echo $j;?>" id="file_<?php echo $j;?>" value="<?php echo $file[$i]; ?>" /><input type="hidden" name="name_<?php echo $j;?>" id="name_<?php echo $j;?>" value="<?php echo $filename[$i];?>" /></td>
															<td align="left" class="input-text"  valign="top"><a href="#1" onClick="Del_Upload(<?php echo $j;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
															
														</tr>
														<?php $j++;}?>
													</table>
												</address>
											</div><!-- /.col -->
											<?php }?>
											
											<div class="col-sm-6 invoice-col">
											 &nbsp;
												<address>
													<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Vessel Particular">
														<i class="fa fa-paperclip"></i> Attachment
														<input type="file" class="form-control" multiple name="mul_file[]" id="mul_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                                        <input type="hidden" name="txtCRMFILE" id="txtCRMFILE" value="" />
                                                        <input type="hidden" name="txtCRMNAME" id="txtCRMNAME" value="" />
													</div>
												</address>
										   </div><!-- /.col -->
										 </div>
										 
										 <div class="col-sm-4 invoice-col"><input type="hidden" name="txtCRM" id="txtCRM" value="" /></div>
								</div><!-- /.tab_8-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Eight Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
								
							</div><!-- /.tab-content -->
						</div><!-- nav-tabs-custom -->
					</div>
				</div>
                
                    <div class="box-footer" align="right">
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate1();">Submit</button>
						<input type="hidden" name="action" value="submit">
					</div>
                  </form>
				<!--  content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
$(".numeric").numeric();
$(".areasize").autosize({append: "\n"});

$('.datepiker').datepicker({
    format: 'dd-mm-yyyy',
	autoclose: true
});

$("#frm1").validate({
	rules: {
		
	},
	messages: {
		
	},
	submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

$('#selFlag').val(<?php echo $obj->getVesselIMOData($_REQUEST['id'],"FLAG"); ?>);
$('#selFlag_1').val(<?php echo $obj->getVesselIMOData($_REQUEST['id'],"FLAG"); ?>);
$('#selCountry').val(<?php echo $obj->getVesselParticularData('COUNTRY_ID','vessel_master_1',$_REQUEST['id']); ?>);
$('#selPort').val(<?php echo $obj->getVesselParticularData('PORT_ID','vessel_master_1',$_REQUEST['id']); ?>);
$('#selIVFFTO_1').val(<?php echo $obj->getVesselParticularData('PANAMA_CANAL','vessel_master_1',$_REQUEST['id']); ?>);
$('#selIVFFTO_2').val(<?php echo $obj->getVesselParticularData('SUEZ_CANAL','vessel_master_1',$_REQUEST['id']); ?>);
$('#selIVFFTO_3').val(<?php echo $obj->getVesselParticularData('SEAWAY','vessel_master_1',$_REQUEST['id']); ?>);
$('#selPanama').val(<?php echo $obj->getVesselParticularData('PANAMA_RADIUS','vessel_master_1',$_REQUEST['id']); ?>);
$('#selSHAFT_GEN').val(<?php echo $obj->getVesselParticularData('SHAFT_GEN','vessel_master_1',$_REQUEST['id']); ?>);

$('#selCLASS_SOC').val(<?php echo $obj->getVesselIMOData($_REQUEST['id'],"CLA_SOC_ID"); ?>);
$('#selCLASS_1').val(<?php echo $obj->getVesselParticularData('CLASS_1','vessel_master_2',$_REQUEST['id']); ?>);
$('#selSTR').val(<?php echo $obj->getVesselParticularData('STR','vessel_master_2',$_REQUEST['id']); ?>);
$('#selSTR_1').val(<?php echo $obj->getVesselParticularData('STR_1','vessel_master_2',$_REQUEST['id']); ?>);
$('#selPOLD').val(<?php echo $obj->getVesselParticularData('PORT_DRYDOCK','vessel_master_2',$_REQUEST['id']); ?>);
$('#selISM').val(<?php echo $obj->getVesselParticularData('ISM','vessel_master_2',$_REQUEST['id']); ?>);
$('#selADVISE_PORT').val(<?php echo $obj->getVesselParticularData('PORT_ADVISE','vessel_master_2',$_REQUEST['id']); ?>);
$('#selDETEN').val(<?php echo $obj->getVesselParticularData('DETEN','vessel_master_2',$_REQUEST['id']); ?>); 

$('#selHOLDS_CLEAR').val(<?php echo $obj->getVesselParticularData('HOLDS_CLEAR','vessel_master_4',$_REQUEST['id']); ?>);
$('#selTANKTOP').val(<?php echo $obj->getVesselParticularData('TANKTOP','vessel_master_4',$_REQUEST['id']); ?>);
$('#selCO2').val(<?php echo $obj->getVesselParticularData('CO2','vessel_master_4',$_REQUEST['id']); ?>);
$('#selDET_SYS').val(<?php echo $obj->getVesselParticularData('DET_SYS','vessel_master_4',$_REQUEST['id']); ?>);
$('#selLADDER').val(<?php echo $obj->getVesselParticularData('LADDER','vessel_master_4',$_REQUEST['id']); ?>);
$('#selCALC').val(<?php echo $obj->getVesselParticularData('CALC','vessel_master_4',$_REQUEST['id']); ?>);
$('#selHOLD_SIDE').val(<?php echo $obj->getVesselParticularData('HOLD_SIDE','vessel_master_4',$_REQUEST['id']); ?>);
$('#selF_BULKHEAD').val(<?php echo $obj->getVesselParticularData('F_BULKHEAD','vessel_master_4',$_REQUEST['id']); ?>);
$('#selAFT_BULKHEAD').val(<?php echo $obj->getVesselParticularData('AFT_BULKHEAD','vessel_master_4',$_REQUEST['id']); ?>);
$('#selSHAPED').val(<?php echo $obj->getVesselParticularData('SHAPED','vessel_master_4',$_REQUEST['id']); ?>);
$('#selH_LIFT').val(<?php echo $obj->getVesselParticularData('H_LIFT','vessel_master_4',$_REQUEST['id']); ?>);
$('#selE_HYDRA').val(<?php echo $obj->getVesselParticularData('E_HYDRA','vessel_master_4',$_REQUEST['id']); ?>);
$('#selN_WORK').val(<?php echo $obj->getVesselParticularData('N_WORK','vessel_master_4',$_REQUEST['id']); ?>); 

$('#selCARGO_1').val(<?php echo $obj->getVesselParticularData('CARGO_1','vessel_master_6',$_REQUEST['id']); ?>); 

$('#selFEU').val(<?php echo $obj->getVesselParticularData('FEU','vessel_master_7',$_REQUEST['id']); ?>);
$('#selCOVERS').val(<?php echo $obj->getVesselParticularData('COVERS','vessel_master_7',$_REQUEST['id']); ?>);
$('#selTWEENS').val(<?php echo $obj->getVesselParticularData('TWEENS','vessel_master_7',$_REQUEST['id']); ?>);
$('#selFLUSH').val(<?php echo $obj->getVesselParticularData('FLUSH','vessel_master_7',$_REQUEST['id']); ?>);
$('#selB_FITTED').val(<?php echo $obj->getVesselParticularData('B_FITTED','vessel_master_7',$_REQUEST['id']); ?>);
$('#selCO2_FITT').val(<?php echo $obj->getVesselParticularData('CO2_FITT','vessel_master_7',$_REQUEST['id']); ?>);

$('#selCLASS_SOC_1').val(<?php echo $obj->getVesselIMOData($_REQUEST['id'],"CLA_SOC_ID"); ?>);
getCalculateDWT();
});

function addCertificateRow()
{
	var id = $("#txtCID").val();
	if($("#selCert_"+id).val() == 0)
	{
		jAlert('Please Select Certificate Name', 'Alert Dialog');
		$("#selCert_"+id).focus();
	}
	else
	{
		id  = (id - 1 )+ 2;
		$('<tr height="10"><td><select name="selCert_'+id+'" type="text" class="select form-control" id="selCert_'+id+'" autocomplete="off" value="" onchange="getCertificate('+id+');"><?php $obj->getCertificateList();?></select></td><td><input name="txtIDate_'+id+'" type="text" class="form-control datepiker" id="txtIDate_'+id+'" autocomplete="off" value="" placeholder="dd-mm-yyyy" /></td><td><input name="txtLAIDate_'+id+'" type="text" class="form-control datepiker" id="txtLAIDate_'+id+'" autocomplete="off" value="" placeholder="dd-mm-yyyy" /></td><td><input name="txtEDate_'+id+'" type="text" class="form-control datepiker" id="txtEDate_'+id+'" autocomplete="off" value="" placeholder="dd-mm-yyyy" /></td><td><div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title=""><i class="fa fa-paperclip"></i> Attachment<input type="file" class="form-control" name="txtfile_'+id+'" id="txtfile_'+id+'" title="" data-widget="Add Inspectors Report" data-toggle="tooltip" data-original-title="Add Inspectors Report" /></div</td></tr>').appendTo("#tb2");
		$("#txtCID").val(id);
		$('.datepiker').datepicker({
			format: 'dd-mm-yyyy'
		});
	}
}

function Del_Upload(var1)
{
	jConfirm('Are you sure you want to delete this attachment permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file_'+var1).remove();
	}
	else{return false;}
	});
}

function getValidate1()
{
	var var6 = $("[id^=file_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRMFILE').val(var6);
	
	var var7 = $("[id^=name_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRMNAME').val(var7);
}

function getCalculateDWT()
{
	var dwtlt = parseFloat($("#txtSumid_1").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtSumid_2").val(dwtlt.toFixed(5));
	
	var draftft = parseFloat($("#txtSumid_3").val())*3.28084;if(isNaN(draftft)){draftft = 0;}
	$("#txtSumid_4").val(draftft.toFixed(5));
	
	var dwtlt = parseFloat($("#txtSumid_5").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtSumid_6").val(dwtlt.toFixed(5));
	
	var dwtlt = parseFloat($("#txtSumid_7").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtSumid_8").val(dwtlt.toFixed(5));
	
	//------------------
	var dwtlt = parseFloat($("#txtWinid_1").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtWinid_2").val(dwtlt.toFixed(5));
	
	var draftft = parseFloat($("#txtWinid_3").val())*3.28084;if(isNaN(draftft)){draftft = 0;}
	$("#txtWinid_4").val(draftft.toFixed(5));
	
	var dwtlt = parseFloat($("#txtWinid_5").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtWinid_6").val(dwtlt.toFixed(5));
	
	var dwtlt = parseFloat($("#txtWinid_7").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtWinid_8").val(dwtlt.toFixed(5));
	
	//------------------
	var dwtlt = parseFloat($("#txtTropicalid_1").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtTropicalid_2").val(dwtlt.toFixed(5));
	
	var draftft = parseFloat($("#txtTropicalid_3").val())*3.28084;if(isNaN(draftft)){draftft = 0;}
	$("#txtTropicalid_4").val(draftft.toFixed(5));
	
	var dwtlt = parseFloat($("#txtTropicalid_5").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtTropicalid_6").val(dwtlt.toFixed(5));
	
	var dwtlt = parseFloat($("#txtTropicalid_7").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtTropicalid_8").val(dwtlt.toFixed(5));
	
	//------------------
	var dwtlt = parseFloat($("#txtSumTimid_1").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtSumTimid_2").val(dwtlt.toFixed(5));
	
	var draftft = parseFloat($("#txtSumTimid_3").val())*3.28084;if(isNaN(draftft)){draftft = 0;}
	$("#txtSumTimid_4").val(draftft.toFixed(5));
	
	var dwtlt = parseFloat($("#txtSumTimid_5").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtSumTimid_6").val(dwtlt.toFixed(5));
	
	var dwtlt = parseFloat($("#txtSumTimid_7").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtSumTimid_8").val(dwtlt.toFixed(5));
	
	//------------------
	var dwtlt = parseFloat($("#txtWinTimid_1").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtWinTimid_2").val(dwtlt.toFixed(5));
	
	var draftft = parseFloat($("#txtWinTimid_3").val())*3.28084;if(isNaN(draftft)){draftft = 0;}
	$("#txtWinTimid_4").val(draftft.toFixed(5));
	
	var dwtlt = parseFloat($("#txtWinTimid_5").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtWinTimid_6").val(dwtlt.toFixed(5));
	
	var dwtlt = parseFloat($("#txtWinTimid_7").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtWinTimid_8").val(dwtlt.toFixed(5));
	
	//------------------
	var dwtlt = parseFloat($("#txtTropicalTimid_1").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtTropicalTimid_2").val(dwtlt.toFixed(5));
	
	var draftft = parseFloat($("#txtTropicalTimid_3").val())*3.28084;if(isNaN(draftft)){draftft = 0;}
	$("#txtTropicalTimid_4").val(draftft.toFixed(5));
	
	var dwtlt = parseFloat($("#txtTropicalTimid_5").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtTropicalTimid_6").val(dwtlt.toFixed(5));
	
	var dwtlt = parseFloat($("#txtTropicalTimid_7").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtTropicalTimid_8").val(dwtlt.toFixed(5));
	

	var dwtlt = parseFloat($("#txtConstantMT").val())*0.984207;if(isNaN(dwtlt)){dwtlt = 0;}
	$("#txtConstantLT").val(dwtlt.toFixed(5));
}

</script>
	</body>
</html>