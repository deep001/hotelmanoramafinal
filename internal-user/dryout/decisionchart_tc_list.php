<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(20); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-leaf"></i>&nbsp;TC Estimates&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Decision Chart</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
					if($msg == 1){?>
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Congratulations!</b> Decision Chart added successfully.
                    </div>
				<?php }}?>
				<div class="box box-primary">
				<h3 style=" text-align:center;">Decision Chart List</h3>
				<div align="right"><a href="allPdf.php?id=57" title="Pdf"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a></div>
				<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                    
					<div style="height:10px;">
						<input name="txttblid" id="txttblid" type="hidden" value="" />
						<input name="txttblstatus" id="txttblstatus" type="hidden" value="" />
						<input name="delId" id="delId" type="hidden" value="" />
						<input type="hidden" name="action" value="submit" />
					</div>				
					<?php $obj->displayTCDecisionChartList();?>
                    <div style="clear:both;"></div>
				</form>
				</div>
                
              <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" style="width:95%">
					 <div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Decision Chart</h4>
						</div>
						
						<div class="modal-body" id="div_3" style="overflow:auto;">
						</div>
					 </div>
				</div>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<script src='../../js/jquery.autosize.js'></script>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
 
$(function() {
	$("#fce_list").dataTable();

});

function getListToCompare(val,val2)
{
	$("#div_3").html('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />');
	$.post("options.php?id=53",{val:""+val+"",val2:""+val2+""}, function(html) {
	$("#div_3").html("");
	$("#div_3").html(html);
	     $("input[type='radio']").iCheck({
				checkboxClass: 'icheckbox_minimal',
				radioClass: 'iradio_minimal'
			    });
	});
}


</script>
		
</body>
</html>