<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$_SESSION['moduleid'] =6;
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-anchor"></i>&nbsp;Fleet&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Fleet</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b>  Fleet added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating Fleet.
				</div>
				<?php }}?>
				<div class="box box-primary">
				<h3 style=" text-align:center;">FLEET</h3>
                <!--<div align="right" style="float:left; margin-left:18px;"><a href="fleet_allocation.php" title="Add New"><button class="btn btn-info btn-flat">Fleet Allocation</button></a>&nbsp;&nbsp;&nbsp;&nbsp;</div>-->
				<div align="right"><a href="#" onClick="getListToCompare();"><button type="button" class="btn btn-info btn-flat" id="comparebutton">Compare Vessels</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button style="display:none;" class="btn btn-info btn-flat" data-toggle="modal" id="buttonPop1" data-target="#compose-modal">Compare Voyage Est.</button><a href="addVessel.php" title="Add New"><button class="btn btn-info btn-flat">Add New</button></a>&nbsp;&nbsp;&nbsp;&nbsp;</div>
				
				<?php $obj->displayFleetlList();?>
				</div>
                
                 <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" style="width:95%">
					 <div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Compare Vessels</h4>
						</div>
						<form name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
						<div class="modal-body" id="div_3" style="overflow:auto;">
						</div>
                        <input type="hidden" name="action1" value="submit" />
                        </form>
					 </div>
				</div>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(function() {
	$("#fleet_list").dataTable({
    "iDisplayLength": 50
    });
});

function getListToCompare()
{
	var anyBoxesChecked = false;
	var chkArr = Array();
	var j = 0;
    $('[id^=chkVSL_]').each(function() {
        if ($(this).is(":checked")) {
			chkArr[j] = $(this).val();
            anyBoxesChecked = true;
			j = j + 1;
        }
    });
 
    if (anyBoxesChecked == false) {
       jAlert('Please select at least one checkbox', 'alert');
      return false;
    } 
	else
	{  
	    $("#buttonPop1").click();
	    $("#div_3").html('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />');
		$.post("options.php?id=48",{chkArr:""+chkArr+""}, function(html) {
		$("#div_3").html("");
		$("#div_3").html(html);
		$("input[type='radio']").iCheck({
				checkboxClass: 'icheckbox_minimal',
				radioClass: 'iradio_minimal'
			    });
		$('[id^=txtDesc_]').autosize({append: "\n"});
	});
	}
}
</script>
    </body>
</html>