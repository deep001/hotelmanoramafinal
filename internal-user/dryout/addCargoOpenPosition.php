<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
{
 	$msg = $obj->insertCargoOpenPositionDetails();
	header('Location:./open_cargo_position.php?msg='.$msg);
}
$pagename = basename($_SERVER['PHP_SELF']);

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rigts    = $obj->getUserRights($uid,$moduleid,2);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Open Cargo Position</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="open_cargo_position.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             ADD CARGO OPEN ENTRY
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           Cargo ID
                            <address>
                              <input type="text" name="txtCID" id="txtCID" class="form-control"  placeholder="Cargo ID" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                          Shipper
                            <address>
                                <select  name="selShipper" class="form-control" id="selShipper" >
									<?php 
                                    $obj->getVendorListNewForCOA(15);
                                    ?>
                                    </select>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Material
                            <address>
                               <select  name="selMType" onChange="getMaterialCode();" class="form-control" id="selMType">
								<?php 
                                $obj->getMaterialType();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Material Code
                            <address>
                               <input type="text"  name="txtMCode"  class="form-control" id="txtMCode" placeholder="Material Code" readonly autocomplete="off" value=""/>
								
                             </address>
                        </div><!-- /.col -->
					</div>
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                          Material Quantity (MT)
                            <address>
                                <input type="text"  name="txtQty"  class="form-control" id="txtQty" placeholder="Material Quantity (MT)" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                         Tolerance (+/- %)
                            <address>
                                <input type="text"  name="txtTolerance"  class="form-control" id="txtTolerance" placeholder="Tolerance (%)" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                       
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                          Freight (USD/MT)
                            <address>
                                <input type="text"  name="txtGrossFreight"  class="form-control" id="txtGrossFreight" placeholder="Freight (USD/MT)" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                         Term Tolerance
                            <address>
                                <input type="text"  name="txtTermTolerance"  class="form-control" id="txtTermTolerance" placeholder="Term Tolerance" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Load Port
                            <address>
                               <select  name="selLPort" class="form-control" id="selLPort">
								<?php 
                                $obj->getPortListNew();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Discharge Port
                            <address>
                                <select  name="selDPort" class="form-control" id="selDPort">
								<?php 
                                $obj->getPortListNew();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
                     
					</div>
					<div class="row invoice-info" >
							
						<div class="col-sm-6 invoice-col">
							LayCan Start Date
							<address>
								<input type="text" name="txtFDate" id="txtFDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value=""/>
							 </address>
						</div><!-- /.col -->
						
						 <div class="col-sm-6 invoice-col">
						   LayCan Finish Date
							<address>
								<input type="text" name="txtTDate" id="txtTDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value=""/>
							 </address>
						</div><!-- /.col -->
							
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                          Vessel Category
                            <address>
                               <select  name="selVCType" class="form-control" id="selVCType">
								<?php 
                                $obj->getVesselCategoryList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Open Vessel
                            <address>
                                <select  name="selVOPID" class="form-control" id="selVOPID">
								<?php 
                                $obj->getOpenVesselPositionList();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
                        
                    </div>
					<?php 					 	
					 	if($rigts == 1)
						{
					 ?>
					<div class="box-footer" align="right">
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
						<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>
					<?php } ?>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#txtQty,#txtLINo,#txtTolerance,#txtGrossFreight").numeric();

$('#txtFDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#txtTDate').datepicker('setStartDate', new Date(getString($(this).val())));
 });
 
$('#txtTDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){   });

function getString(var1)
{
  var var2 = var1.split('-');  
  return var2[2]+'/'+var2[1]+'/'+var2[0];
}


$(".areasize").autosize({append: "\n"});
$("[id^=ui-datepicker-div]").hide();
$("#selVOPID").val(" ");


$("#frm1").validate({
	rules: {
		txtCID:{required: true,remote:'options.php?id=6'},
		selMType:"required",
		txtMCode:"required",
		selLPort:"required",
		selDPort:"required",
		txtFDate:"required",
		txtTDate:"required",
		selVOPID:"required",
		selVCType:"required",
		txtQty : {required:true ,number : true}
		},
	messages: {
		txtCID:"*",
		selMType:"*",
		txtMCode:"*",
		selLPort:"*",
		selDPort:"*",
		txtFDate:"*",
		txtTDate:"*",
		selVOPID:"*",
		selVCType:"*",
		txtQty : {required:"*" ,number : "*"}
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

});


function getMaterialCode()
{
	if($("#selMType").val() != "")
	{
		$("#txtMCode").val($("#selMType").val().split("@@@")[1]);
	}
	else
	{
		$("#txtMCode").val("");
	}
}


function getValidate()
{
	if($("#selLPort").val() == $("#selDPort").val() && $("#selLPort").val()!="" && $("#selDPort").val()!="" )
	{
		jAlert("Sorry, Load port and Discharge Port can not be same.", 'Alert');
		return false;
	}
	else
	{
		return true;
	}
}

</script>
    </body>
</html>