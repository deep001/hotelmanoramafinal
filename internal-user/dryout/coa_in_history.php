<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);

if (isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$selBType = $_SESSION['selBType'] = $_REQUEST['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	unset($_SESSION['selBType']);
	$selBType = '';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(17); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops COA&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops COA&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Vessels in History COA</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Vessels in History added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Vessels in History sheet successfully create.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
					<h3 style=" text-align:center;">Vessels in History - COA</h3>
				
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                        <div class="col-xs-9">
                        </div>
                        <div class="col-xs-3">
                             <select name="selBType" id="selBType" class="form-control" onChange="getSubmit();">
                             <option value="">---Select Business Type---</option>
                               <?php echo $obj->getBusinessTypeList1($selBType);?>
                              </select>                  
                        </div><!-- /.col -->
						<div style="height:10px;">
							<input type="hidden" name="txtMappingid2" id="txtMappingid2" value="" />
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<input type="hidden" name="txtFileName" id="txtFileName" value="" />
							<input type="hidden" name="action" id="action" value="submit" />
						</div>
                
						<div class="box-body table-responsive" style="overflow:auto;">
							<table class="table table-bordered table-striped" id='vessel_in_histroy'>
								<thead>
									<tr>
										<th align="left">VF View</th>
										<th align="left">Business Type / Nom ID</th>
										<th align="left">Material Name</th>
										<th align="left">Vessel</th>
                                        <th align="left">Check List</th>
										<th align="left">Fixture Date/ Voyage No.</th>
										<th align="center">Voyage Financials</th>
										<th align="left">PDA Request</th>	
										<th align="left">Appoint Agent</th>
										<th align="left">SOF</th>	
										<th align="left">Laytime</th>
                                        <th align="center">Payment / Invoice</th>
										<th align="center">Status</th>
									</tr>
								</thead>
									<?php 
										$sql = "select *, (select CARGO from coa_master where coa_master.COAID=freight_cost_estimate_compare.COAAID) as CARGOIDD from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS in (3,4) and freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' and COAAID is not NULL order by date(freight_cost_estimete_master.FINAL_DATETIME) desc"; 

										$res = mysql_query($sql);
										$rec = mysql_num_rows($res);
										$i=1;
										$submit = 0;
									   if($rec == 0)
										{
										echo '<tbody>';
								
										echo '</tbody>';
									}else{
									?>
								<tbody>
								<?php while($rows = mysql_fetch_assoc($res)) { ?>
								<tr style="font-size:11px;">
									<td>
										<a href="viewcoaestimates.php?id=<?php echo $rows['FCAID'];?>&rttype=4" style="color:green;" title="Click me" >FVF</a><br/><br/>
                                            <a href="documents.php?comid=<?php echo $rows['COMID'];?>&page=6" style="color:green;" title="Click me" >Docs</a>
									</td>
									<td><?php echo $obj->getBusinessTypeBasedOnID($obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"BUSINESSTYPEID"))."<br><br>".$rows['MESSAGE'];?></td>
									<td>
                                        <?php echo $obj->getMaterialCodeDesBasedOnCode($rows['CARGOIDD']);?>
                                    </td>
                                    <td>
                                        <?php echo $obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME");?>/<br/> <?php echo $obj->getCompareEstimateData($rows['COMID'],"VESSEL_TYPE");?>
                                    </td>
                                   
			                        <td>
										<?php echo $obj->getFreightFixtureBasedOnID($rows['FIXTURETYPEID']);?><br/><br/>
                                       
                                            <a href="check_list.php?comid=<?php echo $rows['COMID'];?>&page=6" style="color:red;" title="Click me" >Check List</a>	
                                        
                                    </td>
                                    
                                    <td>
                                        <?php $fixture_date = $obj->getCompareEstimateData($rows['COMID'],"TRANS_DATE");
											if($fixture_date!="0000-00-00" && $fixture_date!=""){$fixture_date = date('d-m-Y',strtotime($fixture_date));}else{$fixture_date = "";}?>
                                            <?php echo $fixture_date;?>/<?php echo $obj->getCompareEstimateData($rows['COMID'],"VOYAGE_NO");?>
                                    </td>
									<td style="text-align:center;">
                                      
                                        <?php
                                            $sql1 = "select * from cost_sheet_name_master where COMID='".$rows['COMID']."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";

                                            $res1 = mysql_query($sql1);
                                            $rec1 = mysql_num_rows($res1);
                                            if($rec1 > 0)
                                            {
                                                while($rows1 = mysql_fetch_assoc($res1))
                                                {
                                                    if($rows1['PROCESS'] != "EST")
                                                    {
                                                        $href = "./cost_sheet_coa.php?comid=".$rows['COMID']."&cost_sheet_id=".$rows1['COST_SHEETID']."&page=3";
                                                       
                                                        echo "<a href='".$href."'>".str_replace(' ','&nbsp;',$rows1['SHEET_NAME'])."</a><br/>";
                                                        $curr_cst = $rows1['COST_SHEETID'];
                                                    }
                                                    else
                                                    {
                                                        $curr_cst = $obj->getCostSheetFixtureID($rows['COMID'],"SHEET_NO");
                                                    }
                                                }
                                            }	
                                            ?>
                                    </td>
                                    <td>
                                        <a href="agency_letter_generation.php?comid=<?php echo $rows['COMID'];?>&tab=0&page=6">Generate Agency Letter</a>
                                    </td>
                                    <td>
                                        <a href="nominate_agent.php?comid=<?php echo $rows['COMID'];?>&page=6">Nominate Agent</a>
                                    </td>			
                                    <td>
                                        <a href="sof.php?comid=<?php echo $rows['COMID'];?>&page=6">SOF</a>		
                                    </td>
                                    <td>
                                        <?php if($obj->getFreightEstimationStatus($rows['COMID']) == 2 )
                                            {?>
                                        <a href="laytime_calculation.php?comid=<?php echo $rows['COMID'];?>&page=6" style="color:blue;" title="Click me" >Calculations</a>
                                        <?php }?>
                                    </td>
			
									<td>
                                        <a href="payment_gridcoa.php?comid=<?php echo $rows['COMID'];?>&page=6" >View</a><br><br>
                                        <!--<a href="profit_head.php?comid=<?php echo $rows['COMID'];?>&page=3" >Headwise Detail</a>
                                        <br><br>-->
                                        <a href="piclubdeclaration.php?comid=<?php echo $rows['COMID'];?>&page=6" >P & I Club Declaration</a>
                                        
                                    </td>
                                    
                                    <?php if($rights == 1){ ?>
                                    <td style="text-align:center;">
                                        <a href="#A" title="Deactivate entry" onClick="getValidate2(<?php echo $rows['COMID'];?>,<?php echo $i;?>);">
                                            <i class="fa fa-times" style="color:red;"></i>
                                        </a>
                                    </td>
                                    <?php } ?>
                                    
									<td>
										History
									</td>
                                    
								</tr>
								<?php $i++;}}?>
								</tbody>
							</table>
						</div>
					</form>	
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>

<script type="text/javascript">
$(document).ready(function(){ 

$("#vessel_in_histroy").dataTable();

function getPdfForSea(mappingid,vesselid)
{
	location.href='allPdf.php?id=12&mappingid='+mappingid+'&vesselid='+vesselid;
}
});

function getSubmit()
{ 
    $("#action").val("");
	document.frm1.submit();
}
</script>
		
</body>
</html>