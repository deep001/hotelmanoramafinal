<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$_SESSION['moduleid'] =6;
$pagename = basename($_SERVER['PHP_SELF']);
if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	$_SESSION['selBType'] = 3;
	$selBType = 3;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
#chartdiv {
	
	font-size	: 11px;
}	
</style>

</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(1); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
               <section class="content-header">
                    <h1>
                        <i class="fa fa-dashboard"></i>&nbsp;Dashboard&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
               <!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../..//js/lib/3/amcharts.js"></script>
<script src="../../js/lib/3/serial.js"></script>
<script src="../../js/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script>
$(document).ready(function(){ 
   $("#txtFromDate,#txtToDate,#txtFromDate1,#txtToDate1,#txtFromDate2,#txtToDate2").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
	$("#selBType").val('<?php echo $selBType;?>');
	$("#selBType1").val('<?php echo $selBType;?>');
	getData();
	getData1();
	getDefaultData();
	$("#in_ops_at_glance_3, #TC_ops_at_glance_3,#in_ops_at_glance_4, #TC_ops_at_glance_4,#in_ops_at_glance_5, #TC_ops_at_glance_5,#in_ops_at_glance_6, #TC_ops_at_glance_6").dataTable({
    "iDisplayLength": 10
    });
});

function getData()
{
	$('#reportdataDiv').html('<tr><td align="center" colspan="8"><img src="../../img/loading.gif" /></td></tr>');
		$.post("options.php?id=56",{txtFromDate:""+$("#txtFromDate").val()+"",txtToDate:""+$("#txtToDate").val()+"",selBType:""+$("#selBType").val()+""}, function(html) {
			$('#reportdataDiv,#dummetbody,#dummerageamtdiv,#freightamtdiv,#freighttbody').empty();
			$('<div class="box-body table-responsive"><table id="reporttable" class="table table-striped display" style="width:100%"><thead><tr><th align="left" width="55%">Vessels</th><th align="left" width="15%">Fixture</th><th align="left" width="15%">Interim</th><th align="left" width="15%">Completion</th></tr></thead><tbody id="reporttbody"></tbody></table></div>').appendTo("#reportdataDiv");
			var jsondata = html.split('@@@');
			var jsondata1 = JSON.parse(jsondata[0]);
			$.each(jsondata1, function(index, array){
				if(array['fixture']==null){array['fixture']='';}
				if(array['Interim']==null){array['Interim']='';}
				if(array['completion']==null){array['completion']='';}
				$('<tr><td>'+array['vessel']+'</td><td>'+array['fixture']+'</td><td>'+array['Interim']+'</td><td>'+array['completion']+'</td></tr>').appendTo("#reporttbody");
			});
			$('#reporttable').dataTable();
			
			
			var jsondata2 = JSON.parse(jsondata[2]);
			$.each(jsondata2, function(index, array){
				$('<tr><td>'+array['vessel']+'</td><td>'+array['dummerage']+'</td></tr>').appendTo("#dummetbody");
			});
			$('<tr style="color:#ff5e00;"><td><strong>Total</strong></td><td><strong>'+jsondata[6]+'</strong></td></tr>').appendTo("#dummetbody");
			$("#dummerageamtdiv").html(jsondata[3]);
			
			
			var jsondata2 = JSON.parse(jsondata[4]);
			$.each(jsondata2, function(index, array){
				$('<tr><td>'+array['vessel']+'</td><td>'+array['freight']+'</td></tr>').appendTo("#freighttbody");
			});
			$('<tr style="color:#ff5e00;"><td><strong>Total</strong></td><td><strong>'+jsondata[7]+'</strong></td></tr>').appendTo("#freighttbody");
			$("#freightamtdiv").html(jsondata[5]);
			
			$("#chartDiv").empty();
			$("#chartDiv").css("width",(parseInt(jsondata[1])*55 + 30));
			var chart1 = AmCharts.makeChart("chartDiv", {
				"type": "serial",
				 "theme": "none",
				"categoryField": "vessel",
				"rotate": false,
				"startDuration": 1,
				"categoryAxis": {
					"gridPosition": "start",
					"position": "bottom",
					"labelRotation": 60,
				},
				"legend": {
						"valueWidth":0,
						"verticalGap":0,
						"useGraphSettings": false,
						"data":[{title: "Fixture", color: "#86a948"},{title: "Interim", color: "#999898"},{title: "Completion", color: "#367fa9"}]
					},
				"trendLines": [],
				"graphs": [
					{
						"balloonText": "<span style='font-size:13px;'>Fixture :[[value]]</span>",
						"fillAlphas": 1,
						"lineAlpha": 0.2,
						"id": "AmGraph-1",
						"title": "Fixture",
						"type": "column",
						"valueField": "fixture",
						"fillColorsField": "fixcolor",
						"fixedColumnWidth":12,
					},
					{
						"balloonText": "<span style='font-size:13px;'>Interim :[[value]]</span>",
						"id": "AmGraph-2",
						"fillAlphas": 1,
						"lineAlpha": 0.2,
						"title": "Interim",
						"type": "column",
						"valueField": "Interim",
						"fillColorsField": "intrimgrey",
						"fixedColumnWidth":12,
					},
					{
						"balloonText": "<span style='font-size:13px;'>Completion :[[value]]</span>",
						"id": "AmGraph-3",
						"fillAlphas": 1,
						"lineAlpha": 0.2,
						"title": "Completion",
						"type": "column",
						"valueField": "completion",
						"fillColorsField": "completeblue",
						"fixedColumnWidth":12,
					}
				],
				"guides": [],
				"valueAxes": [
					{
						"id": "ValueAxis-1",
						"position": "bottom",
						"axisAlpha": 0,
						"title":"USD '000",
						"minimum": 0,
					}
				],
				"allLabels": [],
				"balloon": {},
				"dataProvider":jsondata1,
				"export": {
					"enabled": true
				 }
			
			});
	});
}


function getData1()
{
	$('#reportdataDiv1').html('<tr><td align="center" colspan="8"><img src="../../img/loading.gif" /></td></tr>');
		$.post("options.php?id=66",{txtFromDate:""+$("#txtFromDate1").val()+"",txtToDate:""+$("#txtToDate1").val()+"",selBType:""+$("#selBType1").val()+""}, function(html) {
			$('#reportdataDiv1,#otherinvtbody,#hiretbody').empty();
			$('<div class="box-body table-responsive"><table id="reporttable1" class="table table-striped display" style="width:100%"><thead><tr><th align="left" width="55%">TC No.</th><th align="left" width="55%">Vessels</th><th align="left" width="15%">Fixture</th><th align="left" width="15%">Interim</th><th align="left" width="15%">Completion</th></tr></thead><tbody id="reporttbody1"></tbody></table></div>').appendTo("#reportdataDiv1");
			var jsondata = html.split('@@@');
			var jsondata1 = JSON.parse(jsondata[0]);
			$.each(jsondata1, function(index, array){
				if(array['fixture']==null){array['fixture']='';}
				if(array['Interim']==null){array['Interim']='';}
				if(array['completion']==null){array['completion']='';}
				$('<tr><td>'+array['TCno']+'</td><td>'+array['vessel']+'</td><td>'+array['fixture']+'</td><td>'+array['Interim']+'</td><td>'+array['completion']+'</td></tr>').appendTo("#reporttbody1");
			});
			$('#reporttable1').dataTable();
			
			var jsondata2 = JSON.parse(jsondata[2]);
			$.each(jsondata2, function(index, array){
				$('<tr><td>'+array['TCno']+'</td><td>'+array['vessel']+'</td><td>'+array['shortdesc']+'</td><td>'+array['amount']+'</td></tr>').appendTo("#otherinvtbody");
			});
			$('<tr style="color:#ff5e00;"><td colspan="3"><strong>Total</strong></td><td><strong>'+jsondata[6]+'</strong></td></tr>').appendTo("#otherinvtbody");
			
			
			var jsondata2 = JSON.parse(jsondata[4]);
			$.each(jsondata2, function(index, array){
				$('<tr><td>'+array['TCno']+'</td><td>'+array['vessel']+'</td><td>'+array['customer']+'</td><td>'+array['amount']+'</td></tr>').appendTo("#hiretbody");
			});
			$('<tr style="color:#ff5e00;"><td colspan="3"><strong>Total</strong></td><td><strong>'+jsondata[7]+'</strong></td></tr>').appendTo("#hiretbody");
			
			
			$("#chartDiv1").empty();
			$("#chartDiv1").css("width",(parseInt(jsondata[1])*55 + 30));
			var chart1 = AmCharts.makeChart("chartDiv1", {
				"type": "serial",
				 "theme": "none",
				"categoryField": "vessel",
				"rotate": false,
				"startDuration": 1,
				"categoryAxis": {
					"gridPosition": "start",
					"position": "bottom",
					"labelRotation": 60,
				},
				"legend": {
						"valueWidth":0,
						"verticalGap":0,
						"useGraphSettings": false,
						"data":[{title: "Fixture", color: "#86a948"},{title: "Interim", color: "#999898"},{title: "Completion", color: "#367fa9"}]
					},
				"trendLines": [],
				"graphs": [
					{
						"balloonText": "<span style='font-size:13px;'>Fixture :[[value]]</span>",
						"fillAlphas": 1,
						"lineAlpha": 0.2,
						"id": "AmGraph-1",
						"title": "Fixture",
						"type": "column",
						"valueField": "fixture",
						"fillColorsField": "fixcolor",
						"fixedColumnWidth":12,
					},
					{
						"balloonText": "<span style='font-size:13px;'>Interim :[[value]]</span>",
						"id": "AmGraph-2",
						"fillAlphas": 1,
						"lineAlpha": 0.2,
						"title": "Interim",
						"type": "column",
						"valueField": "Interim",
						"fillColorsField": "intrimgrey",
						"fixedColumnWidth":12,
					},
					{
						"balloonText": "<span style='font-size:13px;'>Completion :[[value]]</span>",
						"id": "AmGraph-3",
						"fillAlphas": 1,
						"lineAlpha": 0.2,
						"title": "Completion",
						"type": "column",
						"valueField": "completion",
						"fillColorsField": "completeblue",
						"fixedColumnWidth":12,
					}
				],
				"guides": [],
				"valueAxes": [
					{
						"id": "ValueAxis-1",
						"position": "bottom",
						"axisAlpha": 0,
						"title":"USD '000",
						"minimum": 0,
					}
				],
				"allLabels": [],
				"balloon": {},
				"dataProvider":jsondata1,
				"export": {
					"enabled": true
				 }
			
			});
	});
}


function getDefaultData()
{
		var table = $('#coa_list').dataTable();
		if(table != null)table.fnDestroy();
	  
		table = $("#coa_list").DataTable( {
			"processing": true,
			"serverSide": true,
			"stateSave": true,
			"order": [[1, 'desc']],
			"ajax": {
				"url": "options.php?id=69",
				"type": "POST",
				"dataSrc": function ( d ) {
					return d.records;
				},
				"data": function ( d ) {d.txtFromDate=$("#txtFromDate2").val();d.txtToDate=$("#txtToDate2").val();d.gridtype=1;d.businesstype=$("#selBType3").val()},
			},
			"columns": [
				{ "data": "col1" },
				{ "data": "col2" },
				{ "data": "col3" },
				{ "data": "col4" },
				{ "data": "col5" },
				{ "data": "col6" },
				{ "data": "col7" },
				{ "data": "col8" },
				{ "data": "col9" },
				{ "data": "col10" },
				{ "data": "col11" },
				{ "data": "col12" },
				{ "data": "col13" },
				{ "data": "col14" },
				
			],
			"language": {
				  "zeroRecords": "SORRY CURRENTLY THERE ARE ZERO(0) RECORDS"
				},
			 "fnDrawCallback": function(oSettings, json) {
				  $('[data-toggle="tooltip"]').tooltip();	
				},	
			"columnDefs": [ 
			        {
						"targets": 9,
						"orderable": false
					},
			        {
						"targets": 10,
						"orderable": false
					},
					{
						"targets": 11,
						"orderable": false
					},
					{
						"targets": 12,
						"orderable": false
					},
					{
						"targets": 13,
						"orderable": false
					}
				]
					
		});
}

function getListOFEstimate(coaid,rowsid)
{
	$("#div_3").html('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />');
	$.post("options.php?id=70",{coaid:""+coaid+""}, function(html) {
	$("#div_3").html("");
	$("#div_3").html(html);
	$("#fce_list").dataTable();
	$("#coaidship").html($("#coaid_"+rowsid).html());
	});
}
</script>
    </body>
</html>