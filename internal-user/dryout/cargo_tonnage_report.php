<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;

if (isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$selBType = $_REQUEST['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	$selBType = '';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(7,1); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-bar-chart-o"></i>&nbsp;Reports&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                         <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                         <li>Reports</li>
						 <li>Chartering</li>
						 <li class="active">Cargo Tonnage Book Report</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div class="box box-primary">
				<h3 style=" text-align:center;">Cargo Tonnage Book Report</h3>
				<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
					<div class="row invoice-info" style="margin-left:7px;margin-right:7px;">
                        <div class="col-sm-2">
						    Date From
							<address>
								<input type="text" name="txtFrom_Date" id="txtFrom_Date" class="form-control" value="" autocomplete="off" placeholder="dd-mm-yyyy">
							 </address>
						</div>
						
						<div class="col-sm-2">
						    Date To
							<address>
								<input type="text" name="txtTo_Date" id="txtTo_Date" class="form-control" value="" autocomplete="off" placeholder="dd-mm-yyyy">
							 </address>
						</div>
                        <div class="col-sm-2">
						    Month
							<address>
								<select name="selMonth" id="selMonth" class="form-control" onChange="getData();">
                                <option value="">---Select Month---</option>
                                <?php echo $obj->getMonthList();?>
                                </select> 
							 </address>
						</div>
                        <div class="col-sm-2">
						    Year
							<address>
								<select name="selYear" id="selYear" class="form-control" onChange="getData();">
                                <option value="">---Select Year---</option>
                                <?php echo $obj->getYearListForCargoPlanning();?>
                                </select> 
							 </address>
						</div>
                        <div class="col-sm-2">
						    Qtr
							<address>
								<select name="selQtr" id="selQtr" class="form-control" onChange="getData();">
                                <option value="">---Select Qtr.---</option>
                                <?php echo $obj->getQuaterList();?>
                                </select> 
							 </address>
						</div>
                        
					</div>
					<div align="right">
						<a href="#" title="Pdf" onClick="getPdf();"><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i>Generate PDF</button></a>
					</div>
					<div style="height:10px;">
						<input name="txttblid" id="txttblid" type="hidden" value="" />
						<input name="txttblstatus" id="txttblstatus" type="hidden" value="" />
						<input type="hidden" name="action" value="submit" />
					</div>
                    
                    <div style="overflow:auto;">
                        <div class="box-body table-responsive" style="overflow:auto;">
                     		<table id="cargo_list" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th align="left" valign="top" width="3%">#</th>
                                    <th align="left" valign="top" width="8%">Charter</th>
                                    <th align="left" valign="top" width="8%">Owner</th>	
                                    <th align="left" valign="top" width="6%">Basin</th>
                                    <th align="left" valign="top" width="5%">COA/Spot</th>
                                    <th align="left" valign="top" width="5%" colspan="2">Shipment</th>
                                    <th align="left" valign="top" width="6%">Laycan From</th>
                                    <th align="left" valign="top" width="6%">Laycan To</th>
                                    <th align="left" valign="top" width="6%">LP</th>
                                    <th align="left" valign="top" width="6%">DP</th>
                                    <th align="left" valign="top" width="6%">Cargo Name</th>
                                    <th align="left" valign="top" width="6%">Stem Size(MT)</th>
                                    <th align="left" valign="top" width="6%">Tolerance /Terms</th>
                                    <th align="left" valign="top" width="5%">B Hedge</th>
                                    <th align="left" valign="top" width="5%">Biz Type</th>
                                    <th align="left" valign="top" width="4%">Vessel Type</th>
                                    <th align="left" valign="top" width="13%">Comments</th>
                                    <th align="left" valign="top" width="6%">Status</th>
                                </tr>
                                </thead>
                                <tbody id="tbody1">
                                    <tr><td colspan="19"></td></tr>
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
				</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$('#txtFrom_Date').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
		}).on('changeDate', function(){
		$('#txtTo_Date').datepicker('setStartDate', new Date(getString($(this).val())));
	});
	
	$('#txtTo_Date').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
	}).on('changeDate', function(){  getData(); });
	
});

function getString(var1)
{
  var var2 = var1.split('-');  
  return var2[2]+'/'+var2[1]+'/'+var2[0];
}

function getData()
{
	var table = $('#sf_list').dataTable();
	if(table != null)table.fnDestroy();
	$('#tbody1').html("");
	$('#tbody1').html('<tr><td align="center" colspan="19"><img src="../../img/loading.gif" /></span></td></tr>');
	$.post("options.php?id=74",{txtFrom_Date:""+$("#txtFrom_Date").val()+"",txtTo_Date:""+$("#txtTo_Date").val()+"",selMonth:""+$("#selMonth").val()+"",selYear:""+$("#selYear").val()+"",selQtr:""+$("#selQtr").val()+""}, function(html) {
		$('#tbody1').html("");
		$('#tbody1').html(html);
		$("#sf_list").dataTable();
	});
}

function getPdf()
{
	location.href='allPdf.php?id=72&txtFrom_Date='+$("#txtFrom_Date").val()+'&txtTo_Date='+$("#txtTo_Date").val()+'&selMonth='+$("#selMonth").val()+'&selYear='+$("#selYear").val()+'&selQtr='+$("#selQtr").val();
}
</script>
		
</body>
</html>