<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->InsertOtherShippingCostMasterRecords();
	header('Location:./other_shipping_cost_master.php?msg='.$msg);
 }
$rdoStatus = 1;
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Other Shipping Cost</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div align="right"><a href="other_shipping_cost_master.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				
				<!--<div class="col-md-6">-->
				<div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">ADD NEW OTHER SHIPPING COST</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                                    <div class="box-body">
										<div class="form-group">
                                            <label for="txtName">Name</label>
                                            <input type="text" class="form-control" id="txtName" name="txtName" placeholder="Name" autocomplete="off">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="txtName">Expense Class Group</label>
                                            <input type="text" name="txtECG" id="txtECG" class="form-control" autocomplete="off" value=""  />
                                        </div>
										<div class="form-group">
                                            <label for="selStatus" >Expense Class</label>
											  <input type="text" name="txtEC" id="txtEC" class="form-control" autocomplete="off" value=""  />
                                        </div>
                                              
                                              
                                        <div class="form-group">
                                            <label for="txtName">Accounting Type for Expenses</label>
                                            <input type="text" name="txtATE" id="txtATE" class="form-control" autocomplete="off" value=""  />
                                        </div>
										<div class="form-group">
                                            <label for="selStatus" >Posting Type for Expenses</label>
											  <input type="text" name="txtPTE" id="txtPTE" class="form-control" autocomplete="off" value=""  />                                        </div>
                                              
                                        <div class="form-group">
                                            <label for="txtName">Condition Type</label>
                                            <input type="text" name="txtCT" id="txtCT" class="form-control" autocomplete="off" value=""  />
                                        </div>
										<div class="form-group">
                                            <label for="selStatus" >Partner Number</label>
											  <input type="text" name="txtPN" id="txtPN" class="form-control" autocomplete="off" value=""  />                                        </div>
                                              
                                        <div class="form-group">
                                            <label for="txtName">Currency Key</label>
                                            <input type="text" name="txtCK" id="txtCK" class="form-control" autocomplete="off" value=""  />
                                        </div>
										<div class="form-group">
                                            <label for="selStatus" >Tax Code</label>
											  <input type="text" name="txtTaxCode" id="txtTaxCode" class="form-control" autocomplete="off" value=""  />                                        </div>
										
                                    </div><!-- /.box-body -->

                                    <div class="box-footer" align="center">
                                        <button type="submit" class="btn btn-primary btn-flat">Submit</button><input type="hidden" name="action" value="submit" />
                                    </div>
                                </form>
                            </div>
					<!--</div>			-->
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#frm1").validate({
	rules: {
	txtName:"required"
	},
messages: {	
	txtName:"*"
	},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
});

</script>
    </body>
</html>