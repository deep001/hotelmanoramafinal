<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if(@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertTCITemplateDetails();
	header('Location:./fce.php?msg='.$msg);
}
  
$pagename = basename($_SERVER['PHP_SELF'])."?selFType=2";
$rdoMarket = $rdoMMarket = $rdoCap = $rdoDWT = $rdoQty = 1;
?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}

</style>
</head>
	<div id="basic-modal-content" style="display:none;" align="center">
	<table cellpadding="1" cellspacing="4" border="0" align="center">
	<tr><td align="center"><img src="../../img/loading.gif" /><br /></td></tr>
	</table>
	</div>
	
    <body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onClick="getFinalCalculation();">Calculate</a>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(8); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-leaf"></i>&nbsp;Voyage Estimate&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Freight Cost Estimates</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
					<!--   content put here..................-->
					<div align="right"><a href="fce.php"><button class="btn btn-info btn-flat">Back</button></a></div>
					<div style="height:10px;">&nbsp;</div>
						
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Voyage Financials : Estimate
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
					
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Fixture Type
								<address>
								<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFreightFixtureOutBasedOnID(2);?></strong>
								</address>
							</div><!-- /.col -->
						</div>
                            
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								 Main Particulars
								</h2>                            
							</div><!-- /.col -->
						</div>
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								 Vessel Name
								<address>
								  <select name="selVName" class="form-control" id="selVName" onChange="getData();">
									<?php 
									$obj->getVesselTypeListMemberWise();
									?>
									</select>
								</address>
							</div><!-- /.col -->
							 <div class="col-sm-4 invoice-col">
								 Vessel Type
								<address>
								    <input type="text" name="txtVType" id="txtVType" class="form-control" readonly value="" />
								</address>
							</div><!-- /.col -->
						</div>
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Date
								<address>
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo date("d-m-Y",time());?>" placeholder"dd-mm-yy" />
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Voyage No.
								<address>
									<input type="text" name="txtVNo" id="txtVNo" class="form-control" autocomplete="off" placeholder="Voyage No."/>
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								 Voyage Financials Name
								<address>
									<input type="text" name="txtENo" style="color:red;" id="txtENo" class="form-control" value="" placeholder=" Voyage Financials Name" />
								</address>
							</div><!-- /.col -->
							
						 </div>
                            
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoDWT" class="checkbox" id="rdoDWT1" type="radio" value="1"  <?php if($rdoDWT == 1) echo "checked"; ?> onClick="showDWTField();" />
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
								<input name="rdoDWT" class="checkbox" id="rdoDWT2" type="radio" value="2"  <?php if($rdoDWT == 2) echo "checked"; ?> onClick="showDWTField();" />
								</address>
							</div><!-- /.col -->
						   
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>
								<address>
									<input type="text" name="txtDWTS" id="txtDWTS" class="form-control" autocomplete="off" readonly value="" placeholder="DWT (Summer)" />
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>
								<address>
									<input type="text" name="txtDWTT" id="txtDWTT" class="form-control" autocomplete="off" readonly value="" placeholder="DWT (Tropical)" disabled="disabled" />
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoCap" id="rdoCap1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoCap == 1) echo "checked"; ?> onClick="showCapField();"  />
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoCap" id="rdoCap2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoCap == 2) echo "checked"; ?> onClick="showCapField();" />
								</address>
							</div><!-- /.col -->
							
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
								<address>
									<input type="text" name="txtGCap" id="txtGCap" class="form-control" autocomplete="off" readonly value="" />
								</address>
							</div>
							
							<div class="col-sm-4 invoice-col">
								Bale Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
								<address>
									<input type="text" name="txtBCap" id="txtBCap" class="form-control" autocomplete="off" readonly value="" disabled="disabled" />
								</address>
							</div>
							<div class="col-sm-4 invoice-col">
								SF <span style="font-size:10px; font-style:italic;">(CBM/MT)</span>
								<address>
									<input type="text" name="txtSF" id="txtSF"  autocomplete="off" class="form-control" value=""  onkeyup="getTotalDWT(),getTotalDWT1()" />
								</address>
							</div>
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-8 invoice-col">
								&nbsp;
								<address>
									&nbsp;
								</address>
							</div>
							
							<div class="col-sm-4 invoice-col">
								Loadable <span style="font-size:10px; font-style:italic;">(MT)</span>
								<address>
									<input type="text" name="txtLoadable" id="txtLoadable" autocomplete="off" class="form-control" readonly value="" />
								</address>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								 COMMERCIAL - PARAMETERS &nbsp;&nbsp;&nbsp;&nbsp;<img src="../../img/close.png" onClick="getShowHide1();" id="BID1" name="BID1" style="cursor:pointer;" title="Close Panel" /><input type="hidden" id="txtbid1" value="0" />
								</h2>                            
							</div><!-- /.col -->
						</div>			
	
						<div id="bunker_adj1" >
						<table cellpadding="1" cellspacing="1" border="0" width="100%">
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">Speed Data</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 1</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 2</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBFullSpeed" id="txtBFullSpeed" class="form-control" readonly value="" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBEcoSpeed1" id="txtBEcoSpeed1" class="form-control" readonly value="" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBEcoSpeed2" id="txtBEcoSpeed2" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLFullSpeed" id="txtLFullSpeed" class="form-control" readonly value="" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLEcoSpeed1" id="txtLEcoSpeed1" class="form-control" readonly value="" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLEcoSpeed2" id="txtLEcoSpeed2" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td></tr>
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">FO Consumption MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 1</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 2</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBFOFullSpeed" id="txtBFOFullSpeed" class="form-control" readonly value="" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBFOEcoSpeed1" id="txtBFOEcoSpeed1" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtBFOEcoSpeed2" id="txtBFOEcoSpeed2" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLFOFullSpeed" id="txtLFOFullSpeed" class="form-control" readonly value="" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLFOEcoSpeed1" id="txtLFOEcoSpeed1" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtLFOEcoSpeed2" id="txtLFOEcoSpeed2" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtPIFOFullSpeed" id="txtPIFOFullSpeed" class="form-control" readonly value="" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtPWFOFullSpeed" id="txtPWFOFullSpeed" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						
						</tbody>
						</table>
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">DO Consumption per MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 1</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 2</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBDOFullSpeed" id="txtBDOFullSpeed" class="form-control" readonly value="" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBDOEcoSpeed1" id="txtBDOEcoSpeed1" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtBDOEcoSpeed2" id="txtBDOEcoSpeed2" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLDOFullSpeed" id="txtLDOFullSpeed" class="form-control" readonly value="" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLDOEcoSpeed1" id="txtLDOEcoSpeed1" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtLDOEcoSpeed2" id="txtLDOEcoSpeed2" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtPIDOFullSpeed" id="txtPIDOFullSpeed" class="form-control" readonly value="" /></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtPWDOFullSpeed" id="txtPWDOFullSpeed" class="form-control" readonly value="" /></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						</tbody>
						</table>
						
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						</table>
						</div>
                             
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">Market</h2>                            
							</div><!-- /.col -->
						</div>
						 
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								<address>
								  <input name="rdoMMarket" class="checkbox" id="rdoMMarket1" type="radio" value="1"  <?php if($rdoMMarket == 1) echo "checked"; ?>  onclick="showMMarketField();"  />
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								<address>
									<input name="rdoMMarket" class="checkbox" id="rdoMMarket2" type="radio" value="2" <?php if($rdoMMarket == 2) echo "checked"; ?> onClick="showMMarketField();" />
								</address>
							</div><!-- /.col -->
						</div>
                        
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
								Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
								<address>
									<input type="text" name="txtMTCPDRate" id="txtMTCPDRate" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" placeholder="Agreed Gross Freight (USD/MT)" />
								</address>
                            </div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Lumpsum <span style="font-size:10px; font-style:italic;">(USD)</span>
								<address>
									<input type="text" name="txtMLumpsum" id="txtMLumpsum" class="form-control" autocomplete="off" disabled="disabled" onKeyUp="getFinalCalculation();" placeholder="Lumpsum (USD)"/>
								</address>
						   </div><!-- /.col -->
                               <div class="col-sm-4 invoice-col">
									Addnl Cargo Rate <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
										<input type="text"  name="txtAddnlCRate" id="txtAddnlCRate" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" placeholder="Addnl Cargo Rate (USD/MT)"/>
                                    </address>
                               </div><!-- /.col -->
                         </div>
                        
                        <div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								 Cargo
								</h2>                            
							</div><!-- /.col -->
						</div>
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                                Quantity <span style="font-size:10px; font-style:italic;">(MT)</span>
                                <address>
									<input type="text" name="txtCQMT" id="txtCQMT" class="form-control" autocomplete="off" value=""  onkeyup="getFinalCalculation();" placeholder="Quantity (MT)"/>
                                </address>
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                               Cargo Type
                                <address>
									<select  name="selCType" class="form-control" id="selCType">
										<?php $obj->getCargoTypeList();	?>
									</select>
                                </address>
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                Cargo Name
                                <address>
                                   <select  name="selCName" class="form-control" id="selCName">
									<?php 
									$obj->getCargoNameList();
									?>
									</select>
                                </address>
                            </div><!-- /.col -->
                            
                        </div>
                            
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                              <span style="font-size:13px; font-style:italic; color:#dc631e">( Please put dead freight quantity / addnl quantity separately )</span>  
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <address>
									<input name="rdoQty" class="checkbox" id="rdoQty1" type="radio" value="1"  <?php if($rdoQty == 1) echo "checked"; ?>  onclick="showQtyField();"  />
                                </address>
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <address>
									<input name="rdoQty" class="checkbox" id="rdoQty2" type="radio" value="2" <?php if($rdoQty == 2) echo "checked"; ?> onClick="showQtyField();" />
                                </address>
                            </div><!-- /.col -->
                         </div>
                            
                            
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                              &nbsp;
                                <address>
									<input type="hidden" name="txtAQMT" id="txtAQMT" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();"/>
                                </address>
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                DF Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                <address>
									<input type="text" name="txtDFQMT" id="txtDFQMT" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" placeholder="DF Qty (MT)"/>
                                </address>
                            </div><!-- /.col -->
                             <div class="col-sm-4 invoice-col">
                                Addnl Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                <address>
                                    <input type="text" name="txtAddnlQMT" id="txtAddnlQMT" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" disabled placeholder="Addnl Qty (MT)"/>
                                </address>
                            </div><!-- /.col -->
                        </div>
							
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								Sea Passage
								</h2>                            
							</div><!-- /.col -->
						</div>
							
							 <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  From Port
                                    <address>
										<select  name="selFPort" class="select form-control" id="selFPort" onChange="getDistance();">
											<?php $obj->getPortList(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    To Port
                                    <address>
										<select  name="selTPort" class="select form-control" id="selTPort" onChange="getDistance();">
											<?php //$obj->getPortList(); ?>
										</select>
										<script>$("#selTPort").html($("#selFPort").html());</script>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Distance Type
                                    <address>
										<select  name="selDType" class="select form-control" id="selDType" onChange="getDistance();">
											<?php $obj->getPortDistanceType(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Speed Adj.
                                    <address>
										<input type="text" name="txtWeather" id="txtWeather" class="form-control" autocomplete="off" placeholder="Speed Adj." />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Margin <span style="font-size:10px; font-style:italic;">(days)</span>
                                    <address>
										<input type="text" name="txtMargin" id="txtMargin" class="form-control" autocomplete="off" placeholder="Margin (days)" />
										<input type="hidden" name="txtVoyageTime" id="txtVoyageTime" class="form-control" autocomplete="off" value="0.00" />
										<input type="hidden" name="txtTTLVoyageDays" id="txtTTLVoyageDays" class="form-control" autocomplete="off" value="0.00" />
										<input type="hidden" name="txtTTLFoConsp" id="txtTTLFoConsp" class="form-control" readonly value="0.00"  />
										<input type="hidden" name="txtTTLDoConsp" id="txtTTLDoConsp" class="form-control" readonly value="0.00"  />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Distance
                                    <address>
										<input type="text" name="txtDistance" id="txtDistance" class="form-control"  value="" placeholder="distance"/>
										<span id="loader1" style="display:none;" ><img src="../../img/ajax-loader2.gif" /></span>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Passage Type
                                    <address>
										<select  name="selPType" class="select form-control" id="selPType" >
											<?php $obj->getPassageType(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Select Speed
                                    <address>
										<select  name="selSSpeed" class="select form-control" id="selSSpeed" >
											<?php $obj->getSelectSpeedList(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
										<button type="button" class="btn btn-primary btn-flat" onClick="addPortRotationDetails()" style="margin-left:100px;">Add</button>
                                    </address>
                                </div><!-- /.col -->
								
                            </div>
							
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>From Port</th>
												<th>To Port</th>
												<th>Passage Type</th>
												<th>Distance</th>
												<th>Speed Adj.</th>
												<th>Margin<span style="font-size:10px; font-style:italic;">(days)</span></th>
												<input type="hidden" name="p_rotationID" id="p_rotationID" class="input" value="0" /></th>
											</tr>
										</thead>
										<tbody id="tblPortRotation">
											<tr id="PRrow_Empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
									Load Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							<div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Load Port
                                    <address>
										<select  name="selLoadPort" class="select form-control" id="selLoadPort">
											<option value="">--Select from list--</option>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Cargo
                                    <address>
										<select  name="selLPCName" class="select form-control" id="selLPCName" onChange="getLOadPortQty();" >
											<?php 
											$obj->getCargoNameList();
											?>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
										<input type="text" name="txtPCosts" id="txtPCosts" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)"/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Qty MT
                                    <address>
										<input type="text" name="txtQMT" id="txtQMT" class="form-control" onKeyUp="getLoadPortCalculation();" autocomplete="off" value="" placeholder="Qty MT" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                                    <address>
										<input type="text" name="txtRate" id="txtRate" class="form-control" onKeyUp="getLoadPortCalculation();" autocomplete="off" value="" placeholder="Rate (MT/Day)" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Work Days
                                    <address>
										<input type="text" name="txtWDays" id="txtWDays" class="form-control" readonly value="" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Idle Days
                                    <address>
										<input type="text" name="txtIDays" id="txtIDays" class="form-control" autocomplete="off" value="" placeholder="Idle Days" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
										<button type="button" class="btn btn-primary btn-flat" onClick="addLoadPortDetails()" style="margin-left:100px;">Add</button>
                                    </address>
                                </div><!-- /.col -->
							</div>
							
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Load Port</th>
												<th>Cargo Name</th>
												<th>Qty MT</th>
												<th>Rate<span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
												<th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
												<th>Idle Days</th>
												<th>Work Days</th>
												<input type="hidden" name="load_portID" id="load_portID" class="form-control" value="0" />
											</tr>
										</thead>
										<tbody id="tblLoadPort">
											<tr id="LProw_Empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Discharge Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Discharge Port
                                    <address>
										<select  name="selDisPort" class="select form-control" id="selDisPort">
											<option value="">--Select from list--</option>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Cargo
                                    <address>
										<select  name="selDPCName" class="select form-control" id="selDPCName" onChange="getDisPortQty();" >
											<?php 
												$obj->getCargoNameList();
											?>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
										<input type="text" name="txtDPCosts" id="txtDPCosts" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)"/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Qty MT
                                    <address>
										<input type="text"  name="txtDQMT" id="txtDQMT" class="form-control" onKeyUp="getDisPortCalculation();" autocomplete="off" value="" placeholder="Qty MT" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                                    <address>
										<input type="text" name="txtDRate" id="txtDRate" class="form-control" onKeyUp="getDisPortCalculation();" autocomplete="off" value="" placeholder="Rate (MT/Day)" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Work Days
                                    <address>
										<input type="text"name="txtDWDays" id="txtDWDays" class="form-control" readonly value="" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Idle Days
                                    <address>
										<input type="text" name="txtDIDays" id="txtDIDays" class="form-control" autocomplete="off" value="" placeholder="Idle Days" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
										<button type="button" class="btn btn-primary btn-flat" onClick="addDisPortDetails()" style="margin-left:100px;">Add</button>
                                    </address>
                                </div><!-- /.col -->
							</div>
							
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Discharge Port</th>
												<th>Cargo Name</th>
												<th>Qty MT</th>
												<th>Rate<span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
												<th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
												<th>Idle Days</th>
												<th>Work Days</th>
												<input type="hidden" name="dis_portID" id="dis_portID" class="form-control" value="0" />
											</tr>
										</thead>
										<tbody id="tblDisPort">
											<tr id="DProw_Empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Transit Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Transit Port
                                    <address>
										<select  name="selTLoadPort" id="selTLoadPort" class="select form-control" >
											<option value="">--Select from list--</option>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
										<input type="text" name="txtTLPCosts" id="txtTLPCosts" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)"/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Idle Days
                                    <address>
										<input type="text" name="txtTLIDays" id="txtTLIDays" class="form-control" autocomplete="off" value="" placeholder="Idle Days" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
										<button type="button" class="btn btn-primary btn-flat" onClick="addTransitPortDetails()" style="margin-left:100px;">Add</button>
                                    </address>
                                </div><!-- /.col -->
								
							</div>
							
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Transit Port</th>
												<th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
												<th>Idle Days</th>
												<input type="hidden" name="transit_portID" id="transit_portID" class="form-control" value="0" />
											</tr>
										</thead>
										<tbody id="tblTransitPort">
											<tr id="TProw_Empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>											
										</tbody>
									</table>
								</div>
							</div>
							
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Totals
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Laden Dist
                                    <address>
										<input type="text" name="txtLDist" id="txtLDist" class="form-control" readonly value="0" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ballast Dist
                                    <address>
										<input type="text" name="txtBDist" id="txtBDist" class="form-control" readonly value="0" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Dist
                                    <address>
										<input type="text" name="txtTDist" id="txtTDist" class="form-control" readonly value="0" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Laden Days
                                    <address>
										<input type="text" name="txtLDays" id="txtLDays" class="form-control" readonly value="0" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ballast Days
                                    <address>
										<input type="text" name="txtBDays" id="txtBDays" class="form-control" readonly value="0" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Sea Days
                                    <address>
										<input type="text" name="txtTSDays" id="txtTSDays" class="form-control" readonly value="0" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl Port Idle Days
                                    <address>
										<input type="text" name="txtTtPIDays" id="txtTtPIDays" class="form-control" readonly value="0" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl Port Work Days
                                    <address>
										<input type="text" name="txtTtPWDays" id="txtTtPWDays" class="form-control" readonly value="0" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Days
                                    <address>
										<input type="text" name="txtTDays" id="txtTDays" class="form-control" readonly value="0" />
                                    </address>
                                </div><!-- /.col -->
							</div>
							<div class="row invoice-info">	
								<div class="col-sm-4 invoice-col">
                                  Ttl FO Consp MT
                                    <address>
										<input type="text" name="txtTFUMT" id="txtTFUMT" class="form-control" readonly value="" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl DO Consp MT
                                    <address>
										<input type="text" name="txtTDUMT" id="txtTDUMT" class="form-control" readonly value="" />
                                    </address>
                                </div><!-- /.col -->
							</div>
							<div class="row invoice-info">	
								<div class="col-sm-4 invoice-col">
                                  Ttl FO Consp Off Hire
                                    <address>
										<input type="text" name="txtTFOCOffHire" id="txtTFOCOffHire" class="form-control" autocomplete="off" value="0.00"  />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl DO Consp Off Hire
                                    <address>
										<input type="text" name="txtTDOCOffHire" id="txtTDOCOffHire" class="form-control" autocomplete="off" value="0.00" />
                                    </address>
                                </div><!-- /.col -->
								
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Freight Adjustment</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="25%">&nbsp;&nbsp;</th>
												<th width="25%">Percent</th>
												<th width="25%">USD</th>
												<th width="25%">Per MT</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Gross Freight</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdGF" id="txtFrAdjUsdGF" class="form-control" readonly value="0.00" /></td>
												<td><input type="text"  name="txtFrAdjUsdGFMT" id="txtFrAdjUsdGFMT" class="form-control" readonly value="0.00" /></td>
											</tr>
											
											<tr>
												<td>Dead Freight</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdDF" id="txtFrAdjUsdDF" class="form-control" readonly value="0.00" /></td>
												<td><input type="text"  name="txtFrAdjUsdDFMT" id="txtFrAdjUsdDFMT" class="form-control" readonly value="0.00"/></td>
											</tr>
											
											<tr>
												<td>Addnl Freight</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdAF" id="txtFrAdjUsdAF" class="form-control" readonly value="0.00" /></td>
												<td><input type="text"  name="txtFrAdjUsdAFMT" id="txtFrAdjUsdAFMT" class="form-control" readonly value="0.00" /></td>
											</tr>
											
											<tr>
												<td>Total Freight</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdTF" id="txtFrAdjUsdTF" class="form-control" readonly value="0.00" /></td>
												<td><input type="text"  name="txtFrAdjUsdTFMT" id="txtFrAdjUsdTFMT" class="form-control" readonly value="0.00" /></td>
											</tr>
											
											<tr>
												<td>Address Commission</td>
												<td><input type="text"  name="txtFrAdjPerAC" id="txtFrAdjPerAC" class="form-control" autocomplete="off"  onkeyup="getFinalCalculation();" value="0.00"/></td>
												<td><input type="text"  name="txtFrAdjUsdAC" id="txtFrAdjUsdAC" class="form-control" readonly value="0.00"  /></td>
												<td><input type="text"  name="txtFrAdjUsdACMT" id="txtFrAdjUsdACMT" class="form-control" readonly value="0.00"  /></td>
											</tr>
											
											<tr>
												<td>Brokerage</td>
												<td><input type="text"  name="txtFrAdjPerAgC" id="txtFrAdjPerAgC"  onkeyup="getFinalCalculation(),getValue()" class="form-control" autocomplete="off" value="0.00" ></td>
												<td><input type="text"  name="txtFrAdjUsdAgC" id="txtFrAdjUsdAgC" class="form-control" readonly value="0.00"   /></td>
												<td></td>
											</tr>
											
											<tr>
												<td>Net Freight Payable</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdFP" id="txtFrAdjUsdFP" class="form-control" readonly value="0.00"   /></td>
												<td></td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Freight Details</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="25%;"></th>
												<th width="25%;"></th>
												<th width="25%;"></th>
												<th width="25%;"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Final Nett Freight</td>
												<td></td>
												<td><input type="text"  name="txtFGFFD" id="txtFGFFD" class="form-control" onKeyUp="getFinalCalculation();" readonly value="0.00" /></td>
												<td><input type="text"  name="txtFGFFDMT" id="txtFGFFDMT" class="form-control" readonly value="0.00" /></td>
											</tr>											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Bunker Adjustment &nbsp;&nbsp;<img src="../../img/close.png" onClick="getShowHide();" id="BID" name="BID" style="cursor:pointer;" title="Close Panel" /><input type="hidden" id="txtbid" value="0" /></h3>
								</div>
								<div class="box-body no-padding table-responsive" style="overflow:auto;" id="bunker_adj">
									<table class="table table-striped">
										<thead>
											<tr class="GridviewScrollHeader">
												<th colspan="1">Select Bunker grade</th>
												<?php
													$sql = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
													$res = mysql_query($sql);
													$rec = mysql_num_rows($res);
													if($rec == 0)
													{
												?>
												<tr>
													<td valign="top" align="center" colspan="6" style="color:red;">First fill the Bunker Grade Master Data.</td>
												</tr>
											<?php	
												}else{
											?>
												<?php $m=0; while($rows = mysql_fetch_assoc($res)){
													$ttl_bg[] = $rows['NAME'];
													$ttl_bg1[] = $rows['BUNKERGRADEID'];
												?>
													 <th colspan="3" style="text-align:center;"><?php echo $rows['NAME'];?>&nbsp;&nbsp;
													 <input type="hidden" name="txtBHID_<?php echo $rows['BUNKERGRADEID'];?>" id="txtBHID_<?php echo $rows['BUNKERGRADEID'];?>" value="<?php echo $rows['NAME'];?>" class="form-control" autocomplete="off" />
													 <input type="hidden" name="txtBHID1[]" id="txtBHID1_<?php echo $rows['BUNKERGRADEID'];?>" value="<?php echo $rows['BUNKERGRADEID'];?>" class="form-control" autocomplete="off" />
													<input type="hidden" name="txtTTLEst_<?php echo $rows['BUNKERGRADEID'];?>" id="txtTTLEst_<?php echo $rows['BUNKERGRADEID'];?>" class="form-control" readonly value="0" autocomplete="off"  />
													 </th>
												<?php $m++; ?>
											<?php } ?>
											</tr>
										</thead>
										<tbody>
											<tr class="GridviewScrollHeader">
												<td>&nbsp;</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){?>
												<td style="text-align:center;">MT</td>
												<td style="text-align:center;">Price</td>
												<td style="text-align:center;">Cost</td>
												<?php }?>
											</tr>
		
											<tr class="GridviewScrollItem">
												<td>&nbsp;</td>
												<?php $j=$k=0; for($i=0;$i<count($ttl_bg);$i++){?>
												<td></td>
												<td style="text-align:center;">
												<input type="checkbox" name="checkIFO_<?php echo $ttl_bg1[$i];?>" id="checkIFO_<?php echo $ttl_bg1[$i];?>" class="regular-checkbox small-checkbox" value="<?php echo $ttl_bg1[$i];?>" onClick="getEnabled(<?php echo $ttl_bg1[$i];?>,'<?php echo $ttl_bg[$i];?>');" /><label for="checkIFO_<?php echo $ttl_bg1[$i];?>" />
												<input type="hidden" name="txtBunkerRec" id="txtBunkerRec" class="form-control" readonly value="<?php echo $rec;?>"  />
												</td>
												<td></td>
												<?php }?>
											</tr>
		
											<tr class="GridviewScrollItem">
												<td>Estimated</td>
												<?php $j=$k=0; for($i=0;$i<count($ttl_bg);$i++){?>
												<td>
												<input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_1" id="txt<?php echo $ttl_bg[$i];?>_1_1" class="form-control" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',1,this.value);" autocomplete="off" disabled style="width:150px;" readonly />
												</td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_2" id="txt<?php echo $ttl_bg[$i];?>_1_2" class="form-control" autocomplete="off" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',1,this.value,<?php echo $ttl_bg1[$i];?>);" disabled style="width:150px;" /></td>
												<td><input type="text" name="txt<?php echo $ttl_bg[$i];?>_1_3" id="txt<?php echo $ttl_bg[$i];?>_1_3" class="form-control" disabled style="width:150px;" readonly /></td>
												
												<?php }?>
											</tr>
											
											<tr class="GridviewScrollItem">
												<td>Supply</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){?>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_1" id="txt<?php echo $ttl_bg[$i];?>_8_1" class="form-control" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',8,this.value);" disabled style="width:150px;" autocomplete="off" readonly /></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_2" id="txt<?php echo $ttl_bg[$i];?>_8_2" class="form-control" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',8,this.value,<?php echo $ttl_bg1[$i];?>);" disabled style="width:150px;" autocomplete="off" readonly /></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_3" id="txt<?php echo $ttl_bg[$i];?>_8_3" class="form-control" disabled style="width:150px;" autocomplete="off" readonly /></td>
												<?php }?>
											</tr>
											<tr class="GridviewScrollItem">
												<td>Supplier Adjustment</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){?>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_1" id="txt<?php echo $ttl_bg[$i];?>_9_1" class="form-control" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',9,this.value);" disabled style="width:150px;" autocomplete="off" readonly/></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_2" id="txt<?php echo $ttl_bg[$i];?>_9_2" class="form-control"onkeyup="getCalculate('<?php echo $ttl_bg[$i];?>',9,this.value,<?php echo $ttl_bg1[$i];?>);" disabled style="width:150px;" autocomplete="off" readonly/></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_3" id="txt<?php echo $ttl_bg[$i];?>_9_3" class="form-control" disabled style="width:150px;" autocomplete="off" readonly /></td>
												<?php }?>
											</tr>
											<tr class="GridviewScrollItem">
												<td>Nett Supply </td>
												<?php for($i=0;$i<count($ttl_bg);$i++){?>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_1" id="txt<?php echo $ttl_bg[$i];?>_10_1" class="form-control" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',10,this.value);" disabled style="width:150px;" autocomplete="off" readonly/></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_2" id="txt<?php echo $ttl_bg[$i];?>_10_2" class="form-control"onkeyup="getCalculate('<?php echo $ttl_bg[$i];?>',10,this.value,<?php echo $ttl_bg1[$i];?>);" disabled style="width:150px;" autocomplete="off" readonly/></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_3" id="txt<?php echo $ttl_bg[$i];?>_10_3" class="form-control" disabled style="width:150px;" autocomplete="off" readonly /></td>
												<?php }?>
											</tr>
											<tr class="GridviewScrollItem">
												<td>Actual Consumption</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){?>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_1" id="txt<?php echo $ttl_bg[$i];?>_11_1" class="form-control" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',11,this.value);" disabled style="width:150px;" autocomplete="off" readonly/></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_2" id="txt<?php echo $ttl_bg[$i];?>_11_2" class="form-control" disabled style="width:150px;" autocomplete="off" readonly/></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_3" id="txt<?php echo $ttl_bg[$i];?>_11_3" class="form-control" disabled style="width:150px;" autocomplete="off" readonly /></td>
												<?php }?>
		
										<?php }?>									
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Bunkers Nett Supply</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="20%">Cost</th>
												<th width="20%">Value/MT</th>
											</tr>
										</thead>
										<tbody id="tbbunkers">
											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Owner Related Costs (Others)</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
											</tr>
										</thead>
										<tbody id="tbodyBrokerage">
										 <tr id="tbrRow_1">
											<td><a href="#tb1'" onClick="removeBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
											<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
											
											<td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td>
											<td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="form-control" readonly value="0.00" /></td>
											<td></td>
										</tr>
										</tbody>
										
										<tbody>
										<tr>
											<td><button type="button" class="btn btn-primary btn-flat" onClick="addBrokerageRow()">Add</button><input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="1"/></td>
											<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
											
											<td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();" readonly/></td>
											<td><input type="text" name="txtBrComm" id="txtBrComm" class="form-control" readonly value="0.00" /></td>
											<td>
											</td>
										</tr>
									   </tbody>
									   
									   <tbody>
											<?php 
											$sql1 = "select * from owner_related_cost_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
											$res1 = mysql_query($sql1);
											$rec1 = mysql_num_rows($res1);
											$i=1;
											while($rows1 = mysql_fetch_assoc($res1))
											{
											?>
											<tr>
												<td><?php echo $rows1['NAME'];?><input type="hidden" name="txtHidORCID_<?php echo $i;?>" id="txtHidORCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows1['OWNER_RCOSTID'];?>" /></td>
												<td></td>
												<td></td>												
												<td>
												<?php if($rows1['RDO_STATUS'] == 1){?>
												<input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>" onKeyUp="getORCCalculate(<?php echo $rows1['RDO_STATUS'];?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="" placeholder="0.00"  />
												<?php }else if($rows1['RDO_STATUS'] == 2){?>
												<input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>"  onkeyup="getORCCalculate(<?php echo $rows1['RDO_STATUS'];?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="" placeholder="0.00" style="color:red;"  />
												<?php }?>
												</td>
												<td>
												<input type="hidden" name="txtHidORCAmt_<?php echo $i;?>" id="txtHidORCAmt_<?php echo $i;?>" autocomplete="off" value="0.00" />	
												</td>
											</tr>								
											<?php $i++;}?>
												
											<tr>
												<td>Total Shipowner Expenses <input type="hidden" name="txtORC_id" id="txtORC_id" value="<?php echo $rec1;?>" /></td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtTTLORCAmt" id="txtTTLORCAmt" class="form-control" readonly placeholder="0.00"/></td>
												<td></td>												
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td style="font-weight:bold;">Value/MT</td>
												<td><input type="text" name="txtTTLORCCostMT" id="txtTTLORCCostMT" class="form-control"  readonly="true" placeholder="0.00" /></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Charterers' Costs</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="20%">Cost</th>
												<th width="20%">Value/MT</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$sql2 = "select * from charterers_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
											$res2 = mysql_query($sql2);
											$rec2 = mysql_num_rows($res2);
											$i=1;
											while($rows2 = mysql_fetch_assoc($res2))
											{
											?>
											<tr>
												<td>
												<?php echo $rows2['NAME'];?><input type="hidden" name="txtHidCCID_<?php echo $i;?>" id="txtHidCCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows2['CHARTERER_COSTID'];?>" />
												</td>
												<td></td>
												<td>
													<input type="text"  name="txtCCAbs_<?php echo $i;?>" id="txtCCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" onKeyUp="getCharterersCostCalculate(<?php echo $i;?>);" placeholder="0.00" />
												</td>
												<td>
													<input type="text" name="txtCCostMT_<?php echo $i;?>" id="txtCCostMT_<?php echo $i;?>" class="form-control" readonly placeholder="0.00" />
												</td>
												
											</tr>
											<?php $i++;}?>
											<tr><td colspan="5"><input type="hidden" name="txtCC_id" id="txtCC_id" value="<?php echo $rec2;?>" /></td></tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Port Costs</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="20%">Cost</th>
												<th width="20%">Value/MT</th>
											</tr>
										</thead>
										<tbody id="tbPortCosts">
											
										</tbody>
										<tfoot>
											<tr>
												<td><b>Total Port Costs</b></td>
												<td></td>
												<td><input type="text"  name="txtTTLPortCosts" id="txtTTLPortCosts" class="form-control" readonly /></td>
												<td><input type="text" name="txtTTLPortCostsMT" id="txtTTLPortCostsMT" class="form-control" readonly placeholder="0.00"  /></td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
							
							<!--<div class="box">
								<div class="box-header">
									<h3 class="box-title">Demurrage Despatch Ship Owner <br/><span style="font-size:15px;font-style:italic;">(Enter Despatch in Negative)</span></h3>
								</div>
								
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="20%">Cost</th>
												<th width="20%">Value/MT</th>
											</tr>
										</thead>
										<tbody id="tbDDSW">
										
										</tbody>
										<tfoot>
											<tr><td colspan="5"><input type="hidden" name="txtDemurrageDispatchID" id="txtDemurrageDispatchID" value="<?php echo $myrec2; ?>"></td></tr>
										</tfoot>
									</table>
								</div>
							</div>-->
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Other Misc. Income</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="20%">Cost</th>
												<th width="20%">Nett Value</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$sql3 = "select * from other_misc_cost_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
											$res3 = mysql_query($sql3);
											$rec3 = mysql_num_rows($res3);
											$i=1;
											while($rows3 = mysql_fetch_assoc($res3))
											{
											?>
											<tr>
												<td><?php echo $rows3['NAME'];?><input type="hidden" name="txtHidOMCID_<?php echo $i;?>" id="txtHidOMCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows3['OTHER_MCOSTID'];?>" /></td>
												<td></td>
												<td><input type="text"  name="txtOMCAbs_<?php echo $i;?>" id="txtOMCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="" onKeyUp="getOMCCalculate(<?php echo $i;?>);" placeholder="0.00" />
												</td>
												<td><input type="text"  name="txtOMCCostMT_<?php echo $i;?>" id="txtOMCCostMT_<?php echo $i;?>" class="form-control" readonly value="" placeholder="0.00" /></td>
											</tr>
											<?php $i++;}?>
											<tr><td colspan="5" align="left" valign="top" ><input type="hidden" name="txtAOMC_id" id="txtAOMC_id" value="<?php echo $rec3;?>" />
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Results</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="35%"></th>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="25%"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Revenue (Final Nett Freight)</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtRevenue" id="txtRevenue" class="form-control" readonly placeholder="0.00" /></td>
											</tr>
											<tr>
												<td>Total Owners Expenses</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtTTLOwnersExpenses" id="txtTTLOwnersExpenses" class="form-control" readonly  placeholder="0.00" /></td>
											</tr>	
											<tr>
												<td>Total Charterers' Expenses</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtTTLCharterersExpenses" id="txtTTLCharterersExpenses" class="form-control" readonly value="" placeholder="0.00" /></td>
											</tr>
											<tr>
												<td>Voyage Earnings</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtVoyageEarnings" id="txtVoyageEarnings" class="form-control" readonly placeholder="0.00" /></td>
											</tr>
											<tr>
												<td style="color:#dc631e;" >Daily Earnings / TCE</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtDailyEarnings" id="txtDailyEarnings" class="form-control" readonly placeholder="0.00" /></td>
											</tr>
											<tr>
												<td>Daily Time Charter (USD/Day)</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtDailyVesselOperatingExpenses" id="txtDailyVesselOperatingExpenses" class="form-control" autocomplete="off" value="" placeholder="0.00" onKeyUp="getFinalCalculation();" /></td>
											</tr>
											<tr>
												<td style="color:#dc631e;" >G Total Voyage Earnings&nbsp;&nbsp;&nbsp;<span style="font-size:10px; font-style:italic;">(Voyage Earnings + Demurrage)</span></td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtGTTLVoyageEarnings" id="txtGTTLVoyageEarnings" class="form-control" readonly  placeholder="0.00" /></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">Nett Daily Earnings</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtNettDailyEarnings" id="txtNettDailyEarnings" class="form-control" readonly  placeholder="0.00" /></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">Nett Daily Profit</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtNettDailyProfit" id="txtNettDailyProfit" class="form-control" readonly placeholder="0.00" /></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">P/L</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtPL" id="txtPL" class="form-control" readonly placeholder="0.00" /></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							 
                        <div class="box-footer" align="right">
							<button type="button" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
							<input type="hidden" name="action" id="action" value="submit" /><input type="hidden" name="vesselrec" id="vesselrec" class="form-control" value="" />
				        </div>
					
                  </form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />

<script type="text/javascript">
$(document).ready(function(){ 

$("#TCI_18,#TCI_19").show();

$("#txtMTCPDRate,#txtMLumpsum ,#txtWSFR, #txtWSFD ,#txtCQMT ,#txtMinQMT ,#txtAddnlQMT ,#txtWeather, #txtMargin ,#txtDistance ,#txtPCosts ,#txtQMT ,#txtRate ,#txtWDays1 ,#txtIDays ,#txtDPCosts ,#txtDQMT ,#txtDRate ,#txtDWDays1 ,#txtDIDays ,#txtTLPCosts ,#txtTLIDays ,#txtTFOCOffHire ,#txtTDOCOffHire ,#txtFrAdjPerAC ,#txtDiversionCost ,#txtDemCost ,#txtExtraDues ,#txtBrCommPercent ,[id^=txtORCAmt_],[id^=txtCCAbs_] ,#txtDailyVesselOperatingExpenses ,[id^=txtMCWS_],[id^=txtMCDistanceLeg_],[id^=txtMCTotalDistance_],[id^=txtOvrWS_],[id^=txtOvrDistanceLeg_],[id^=txtOvrTotalDistance_],[id^=txtAdditionAmt_],[id^=txtDeductionAmt_],#txtFrAdjUsdGF,#txtFrAdjPerACTF,#txtFrAdjPerACGF,[id^=txtOMCAbs_], [id^=txtOvrWS_],[id^=txtOvrDistanceLeg_],[id^=txtOvrTotalDistance_],[id^=txtAdditionAmt_],[id^=txtDeductionAmt_],#txtFrAdjUsdGF,#txtFrAdjPerACTF,#txtFrAdjPerACGF,#txtddswAddComm,#txtAddnlCRate,#txtDFQMT,#txtFrAdjPerAgC,#txtDWTS,#txtDWTT,#txtSF").numeric();
$("#txtDate").datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
});

});

function getValidate()
{
	if($("#txtENo").val() != "")
	{
		/*var arr = new Array(); var arr1 = new Array();
		var i=0;
		$('[id^=txtFreightSpecs_]').each(function(index) {
					var j = i+1;
					if($("#txtFreightSpecs_"+j).val() == "" || $("#txtMinCargo_"+j).val() == "" || $("#txtMCWSFlatRate_"+j).val() == "" || $("#txtMCWS_"+j).val() == "" || $("#txtMCAmount_"+j).val() == "" || parseFloat($("#txtMCAmount_"+j).val()) == "0.00")
					{
						arr[i] = j;		
					}			
					i++;
			});
		if(arr.length == 0)
		{*/
		jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
			if(r){
					getFinalCalculation();
					document.frm1.submit();
				 }
			else{return false;}
			});
		/*}
		else
		{
			jAlert('Please fill mandatory fields', 'Alert');
			return false;
		}*/
	}
	else
	{
		jAlert('Please fill sheet name.', 'Alert');
		return false;
	}
}

function getData()
{
	if($("#selVName").val() != "")
	{
		$('#basic-modal-content').modal();
		$("#simplemodal-container").css({"height":"15%"});
		$("#simplemodal-container a").hide();
		$.post("options.php?id=32",{selVName:""+$("#selVName").val()+""}, function(data) {
				
				if(data.split("#")[27] == 0)
				{
					jAlert('Please ensure commercial parameters are completed before proceeding', 'Alert', function(r) {
					if(r){ 
						window.location="commercial_parameter.php?id="+$("#selVName").val();
					}
					else{return false;}
					});	
				}
				else
				{
					$('#txtVType').val(data.split("#")[0]);
					$('#txtDWTS').val(data.split("#")[1]);
					$('#txtDWTT').val(data.split("#")[2]);
					$('#txtGCap').val(data.split("#")[3]);
					$('#txtBCap').val(data.split("#")[4]);
					
					$('#txtBFullSpeed').val(data.split("#")[5]);
					$('#txtBEcoSpeed1').val(data.split("#")[6]);
					$('#txtBEcoSpeed2').val(data.split("#")[7]);
					$('#txtBFOFullSpeed').val(data.split("#")[8]);
					$('#txtBFOEcoSpeed1').val(data.split("#")[9]);
					$('#txtBFOEcoSpeed2').val(data.split("#")[10]);
					$('#txtBDOFullSpeed').val(data.split("#")[11]);
					$('#txtBDOEcoSpeed1').val(data.split("#")[12]);
					$('#txtBDOEcoSpeed2').val(data.split("#")[13]);
					
					$('#txtLFullSpeed').val(data.split("#")[14]);
					$('#txtLEcoSpeed1').val(data.split("#")[15]);
					$('#txtLEcoSpeed2').val(data.split("#")[16]);
					$('#txtLFOFullSpeed').val(data.split("#")[17]);
					$('#txtLFOEcoSpeed1').val(data.split("#")[18]);
					$('#txtLFOEcoSpeed2').val(data.split("#")[19]);
					$('#txtLDOFullSpeed').val(data.split("#")[20]);
					$('#txtLDOEcoSpeed1').val(data.split("#")[21]);
					$('#txtLDOEcoSpeed2').val(data.split("#")[22]);
					
					$('#txtPIFOFullSpeed').val(data.split("#")[23]);
					$('#txtPWFOFullSpeed').val(data.split("#")[24]);
					$('#txtPIDOFullSpeed').val(data.split("#")[25]);
					$('#txtPWDOFullSpeed').val(data.split("#")[26]);
					//$('#txtGRT').val(data.split("#")[27]);
					$('#vesselrec').val(data.split("#")[27]);
				}
				$.modal.close();
		});
	}
	else
	{
		$("#txtVType,#txtDWTS,#txtDWTT,#txtGCap,#txtBCap,#txtBFullSpeed,#txtBEcoSpeed1,#txtBEcoSpeed2,#txtBFOFullSpeed,#txtBFOEcoSpeed1,#txtBFOEcoSpeed2, #txtBDOFullSpeed,#txtBDOEcoSpeed1,#txtBDOEcoSpeed2,#txtLFullSpeed,#txtLEcoSpeed1,#txtLEcoSpeed2,#txtLFOFullSpeed,#txtLFOEcoSpeed1,#txtLFOEcoSpeed2, #txtLDOFullSpeed,#txtLDOEcoSpeed1,#txtLDOEcoSpeed2,#txtPIFOFullSpeed,#txtPWFOFullSpeed,#txtPIDOFullSpeed,#txtPWDOFullSpeed,#txtGRT").val("");
	}	
}

function showDWTField()
{
	if($("#rdoDWT1").is(":checked"))
	{
		$("#txtDWTS").attr({'disabled':''});
		$("#txtDWTT").attr({'disabled':'disabled'});
	}
	if($("#rdoDWT2").is(":checked"))
	{
		$("#txtDWTS").attr({'disabled':'disabled'});
		$("#txtDWTT").attr({'disabled':''});
	}
}

function showMMarketField()
{
	if($("#rdoMMarket1").is(":checked"))
	{
		$("#txtMTCPDRate").removeAttr('disabled');
		$("#txtMLumpsum").attr('disabled',true);
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtMTCPDRate").focus();
		getFinalCalculation();
	}
	if($("#rdoMMarket2").is(":checked"))
	{
			
		$("#txtMTCPDRate").attr('disabled',true);
		$("#txtMLumpsum").removeAttr('disabled');
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtMLumpsum").focus();
		getFinalCalculation();
	}
}

function showQtyField()
{
	if($("#rdoQty1").is(":checked"))
	{
		$("#txtDFQMT").removeAttr('disabled');
		$("#txtAddnlQMT").attr('disabled',true);
		$("#txtDFQMT,#txtAddnlQMT").val("");
		$("#txtDFQMT").focus();
	}
	if($("#rdoQty2").is(":checked"))
	{
		$("#txtDFQMT").attr('disabled',true);
		$("#txtAddnlQMT").removeAttr('disabled');
		$("#txtDFQMT,#txtAddnlQMT").val("");
		$("#txtAddnlQMT").focus();
	}
}


function getFinalCalculation()
{
	if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}
	//if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
	if(parseFloat(cargo_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	
	$('[id^=txtCCAbs_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				
				if(this.value == ""){var cc_abs = 0;}else{var cc_abs = this.value;}
				if($("#txtCCAddComm_"+lasrvar1).val() == ""){var cc_addcomm = 0;}else{var cc_addcomm = $("#txtCCAddComm_"+lasrvar1).val();}
				var cc_value = "";
				cc_value = parseFloat(cc_abs) - parseFloat((parseFloat(cc_abs) * parseFloat(cc_addcomm))/100);
				$("#txtCCValue_"+lasrvar1).val(cc_value.toFixed(2));
				
				var costMT = this.value / parseFloat(per_mt);				
				$("#txtCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
	
	$('[id^=txtOMCAbs_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				
				if(this.value == ""){var omc_abs = 0;}else{var omc_abs = this.value;}
				if($("#txtOMCAddComm_"+lasrvar1).val() == ""){var omc_addcomm = 0;}else{var omc_addcomm = $("#txtOMCAddComm_"+lasrvar1).val();}
				var omc_value = "";
				omc_value = parseFloat(omc_abs) - parseFloat((parseFloat(omc_abs) * parseFloat(omc_addcomm))/100);
				$("#txtOMCValue_"+lasrvar1).val(omc_value.toFixed(2));
				
				var costMT = this.value / parseFloat(per_mt);				
				$("#txtOMCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
			
	if($("#txtddswCost").val() == ""){var ddsw_cost = 0;}else{var ddsw_cost = $("#txtddswCost").val();}
	if($("#txtddswAddComm").val() == ""){var ddsw_addcomm = 0;}else{var ddsw_addcomm = $("#txtddswAddComm").val();}
	var ddsw_value = parseFloat(ddsw_cost) - parseFloat((parseFloat(ddsw_cost) * parseFloat(ddsw_addcomm))/100);
	$("#txtddswValue").val(ddsw_value.toFixed(2));
	
	
	if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
	if($("#txtWSFR").val() == ""){var ws_flat_rate = 0;}else{var ws_flat_rate = $("#txtWSFR").val();}
	if($("#txtWSFD").val() == ""){var fixed_diff = 0;}else{var fixed_diff = $("#txtWSFD").val();}
	if($("#txtMTCPDRate").val() == ""){var ws_rate = 0;}else{var ws_rate = $("#txtMTCPDRate").val();}
	if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
	
	$("[id^=txtMinCargo_]").val(min_qty);$("[id^=txtOverage_]").val(addnl_qty);
	$("[id^=txtMCWSFlatRate_],[id^=txtOvrWSFlatRate_]").val(ws_flat_rate);
	
	$("[id^=txtTotalCargoQty_]").val(cargo_qty);
	
	<!----------------- Quantity  ------------------->
	var per_amt = $("#txtCQMT").val();
	if($("#rdoMMarket1").is(":checked"))
	{
		<!----------------- Agreed Gross Freight  ------------------->
		if($("#txtMTCPDRate").val() == ""){var agr_gross_fr = 0;}else{var agr_gross_fr = $("#txtMTCPDRate").val();}
		//if($("#txtAQMT").val() == ""){var actual_qty = 0;}else{var actual_qty = $("#txtAQMT").val();}
		
		<!----------------- (Freight Adjustment) Gross Freight  ------------------->
		var gross_fr = parseFloat(agr_gross_fr) * parseFloat(per_amt);
		$("#txtFrAdjUsdGF").val(gross_fr.toFixed(2));
		
		<!----------------- (Freight Adjustment) Gross Freight(MT)  ------------------->
		var gross_fr_mt = parseFloat(gross_fr) / parseFloat(per_amt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		
		<!-----------------  DF Qty (MT)  ------------------->
		if($("#txtDFQMT").val() == "" || $("#txtDFQMT").val() == 0)
		{
			var df_qty = 0;
			$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
			
		}
		else
		{
			<!-----------------  Dead Freight(USD)  ------------------->
			var df_qty = $("#txtDFQMT").val();
			var dead_fr = parseFloat(agr_gross_fr) * parseFloat(df_qty);
			$("#txtFrAdjUsdDF").val(dead_fr.toFixed(2));
			
			<!-----------------  Dead Freight(MT)  ------------------->
			var dead_fr_mt = parseFloat(dead_fr) / parseFloat(df_qty);
			$("#txtFrAdjUsdDFMT").val(dead_fr_mt.toFixed(2));
		}
		
		<!-----------------  Addnl Cargo Rate (USD/MT)  ------------------->
		
		if($("#txtAddnlCRate").val() == ""){var addnl_rate = 0;}else{var addnl_rate = $("#txtAddnlCRate").val();}
		//if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
		
		
		if($("#txtAddnlQMT").val() == "" || $("#txtAddnlQMT").val() == 0 )
		{
			var addnl_qty = 0;
			$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		}
		else
		{
			<!-----------------  Addnl Qty (MT)  ------------------->
			var addnl_qty = $("#txtAddnlQMT").val();
			
			<!-----------------(Freight Adjustment)  Addnl Freight(USD)  ------------------->
			var addnl_fr = parseFloat(addnl_rate) * parseFloat(addnl_qty);
			$("#txtFrAdjUsdAF").val(addnl_fr.toFixed(2));
			
			<!-----------------(Freight Adjustment)  Addnl Freight(MT)  ------------------->
			var addnl_fr_mt = parseFloat(addnl_fr) / parseFloat(addnl_qty);
			$("#txtFrAdjUsdAFMT").val(addnl_fr_mt.toFixed(2));
		}
		
		<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum().toFixed(2));
		
		<!-----------------(Freight Adjustment)  Total Freight(MT)  ------------------->
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / (parseFloat(per_amt) + parseFloat(df_qty) + parseFloat(addnl_qty));
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
	}
	
	if($("#rdoMMarket2").is(":checked"))
	{
		<!-----------------(Freight Adjustment)  Gross Freight(USD)  ------------------->
		$("#txtFrAdjUsdGF").val($("#txtMLumpsum").val());
		
		<!-----------------(Freight Adjustment)  Gross Freight(MT)  ------------------->
		var gross_fr_mt = parseFloat($("#txtFrAdjUsdGF").val()) / parseFloat(per_amt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		
		$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
		$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		
		<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum());
		
		<!-----------------(Freight Adjustment)  Gross Freight(MT)  ------------------->
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / parseFloat(per_amt);
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
	}
	
	<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
	if($("#txtFrAdjUsdTF").val() == ""){var ttl_fr = 0;}else{var ttl_fr = $("#txtFrAdjUsdTF").val();}
	
	<!-----------------(Freight Adjustment) Address Commission(Percent)  ------------------->
	if($("#txtFrAdjPerAC").val() == ""){var ac_per = 0;}else{var ac_per = $("#txtFrAdjPerAC").val();}
	
	<!-----------------(Freight Adjustment) Address Commission(USD)  ------------------->
	var address_commission = (parseFloat(ttl_fr) * parseFloat(ac_per)) / 100;
	$("#txtFrAdjUsdAC").val(address_commission.toFixed(2));
	
	<!-----------------(Freight Adjustment) Address Commission(MT)  ------------------->
	var address_commission_mt = parseFloat($("#txtFrAdjUsdAC").val()) / parseFloat(per_amt);
	$("#txtFrAdjUsdACMT").val(address_commission_mt.toFixed(2));
	
	<!-----------------(Freight Adjustment) Brokerage(Persent)  ------------------->
	if($("#txtFrAdjPerAgC").val() == ""){var ag_per = 0;}else{var ag_per = $("#txtFrAdjPerAgC").val();}
	var agent_commission = (parseFloat(ttl_fr) * parseFloat(ag_per)) / 100;
	
	<!-----------------(Freight Adjustment) Brokerage(USD)  ------------------->
	$("#txtFrAdjUsdAgC").val(agent_commission.toFixed(2));
	
	<!-----------------(Freight Adjustment) Net Freight Payable(USD)  ------------------->
	var net_fr = parseFloat(ttl_fr) - parseFloat($("#txtFrAdjUsdAC,#txtFrAdjUsdAgC").sum());
	$("#txtFrAdjUsdFP,#txtFGFFD").val(net_fr.toFixed(2));
	
	<!-----------------(Freight Adjustment) Final Net Freight Payable(MT)  ------------------->
	var net_fr_mt = parseFloat(net_fr) / parseFloat(per_amt);
	$("#txtFGFFDMT").val(net_fr_mt.toFixed(2));
	
	<!----------------- Revenue (Final Nett Freight) ------------------->
	if($("#txtFGFFD").val() == ""){var final_nett_frt = 0;}else{var final_nett_frt = $("#txtFGFFD").val();}
	var rev = parseFloat($("[id^=txtOMCAbs_]").sum().toFixed(2)) + parseFloat(final_nett_frt);
	$("#txtRevenue").val(rev.toFixed(2));
	
	<!--##########################################-->
	
	/*if($("#txtFrAdjPerACTF").val() == ""){var less_address_comm_tf = 0;}else{var less_address_comm_tf = $("#txtFrAdjPerACTF").val();}
	if($("#txtFrAdjUsdTF").val() == ""){var ttl_freight = 0;}else{var ttl_freight = $("#txtFrAdjUsdTF").val();}
	
	var add_comm_usd_tf = (parseFloat(ttl_freight) * parseFloat(less_address_comm_tf))/100;
	$("#txtFrAdjUsdACTF").val(add_comm_usd_tf.toFixed(2));
	
	
	if($("#txtFrAdjPerACGF").val() == ""){var less_address_comm_gf = 0;}else{var less_address_comm_gf = $("#txtFrAdjPerACGF").val();}
	if($("#txtFrAdjUsdGF").val() == ""){var gross_freight = 0;}else{var gross_freight = $("#txtFrAdjUsdGF").val();}
	
	var add_comm_usd_gf = (parseFloat(gross_freight) * parseFloat(less_address_comm_gf))/100;
	$("#txtFrAdjUsdACGF").val(add_comm_usd_gf.toFixed(2));		
	
	
	if($("#txtFrAdjUsdACTF").val() == ""){var add_comm_usd_tf1 = 0;}else{var add_comm_usd_tf1 = $("#txtFrAdjUsdACTF").val();}
	if($("#txtFrAdjUsdACGF").val() == ""){var add_comm_usd_gf1 = 0;}else{var add_comm_usd_gf1 = $("#txtFrAdjUsdACGF").val();}
	
	
	var balance_freight = parseFloat(gross_freight) - (parseFloat(add_comm_usd_tf1) + parseFloat(add_comm_usd_gf1));
	$("#txtBalanceFreight").val(balance_freight.toFixed(2));
	
	var final_nett_frt = parseFloat(balance_freight.toFixed(2)) + parseFloat($("[id^=txtAdditionAmt_]").sum().toFixed(2)) - parseFloat($("[id^=txtDeductionAmt_]").sum().toFixed(2));
	$("#txtFGFFD").val(final_nett_frt.toFixed(2));
	
	var rev = parseFloat($("[id^=txtOMCValue_]").sum().toFixed(2)) + parseFloat(final_nett_frt.toFixed(2));
	$("#txtRevenue").val(rev.toFixed(2));
	
	if($("#txtFGFFD").val() == ""){var final_nett_freight = 0;}else{var final_nett_freight = $("#txtFGFFD").val();}
	var final_nett_freight_mt = parseFloat(final_nett_freight) / parseFloat(per_mt);
	$("#txtFGFFDMT").val(final_nett_freight_mt.toFixed(2));*/
	
	<!----------------- Brokerage ------------------->
	//if($("#txtBrCommPercent").val() == ""){var brokerage_comm = 0;}else{var brokerage_comm = $("#txtBrCommPercent").val();}
	//if($("#txtFrAdjUsdTF").val() == ""){var t_frt = 0;}else{var t_frt = $("#txtFrAdjUsdTF").val();}
	//var brokerage_comm_usd = (parseFloat(t_frt) * parseFloat(brokerage_comm))/100;
	//$("#txtBrComm").val(brokerage_comm_usd.toFixed(2));
	
	for(var l = 1;l<=$("#txtBRokageCount").val();l++)
	{
	if($("#txtBrCommPercent_"+l).val() == ""){var brokerage_comm = 0;}else{var brokerage_comm = $("#txtBrCommPercent_"+l).val();}
	if($("#txtFrAdjUsdTF").val() == ""){var t_frt = 0;}else{var t_frt = $("#txtFrAdjUsdTF").val();}
	
	var brokerage_comm_usd = (parseFloat(t_frt) * parseFloat(brokerage_comm))/100;
	$("#txtBrComm_"+l).val(brokerage_comm_usd.toFixed(2));
	}
	$("#txtBrCommPercent").val($("[id^=txtBrCommPercent_]").sum().toFixed(2));
	$("#txtBrComm").val($("[id^=txtBrComm_]").sum().toFixed(2));
	
	$("#txtTTLORCAmt").val($("[id^=txtHidORCAmt_],#txtBrComm").sum());
	var calc = parseFloat($("#txtTTLORCAmt").val()) / parseFloat(per_mt);
	$("#txtTTLORCCostMT").val(calc.toFixed(2));
	
	$("#txtTTLOwnersExpenses").val($("[id^=txtTTLEst_],#txtTTLPortCosts,#txtTTLORCAmt").sum().toFixed(2));
	$("#txtTTLCharterersExpenses").val($("[id^=txtCCAbs_]").sum().toFixed(2));
	
	if($("#txtRevenue").val() == ""){var revenue = 0;}else{var revenue = $("#txtRevenue").val();}
	if($("#txtTTLOwnersExpenses").val() == ""){var owners_expenses = 0;}else{var owners_expenses = $("#txtTTLOwnersExpenses").val();}
	var voyage_earning = parseFloat(revenue) - parseFloat(owners_expenses);
	$("#txtVoyageEarnings").val(voyage_earning.toFixed(2));
	
	var g_voyage_earning = parseFloat(voyage_earning.toFixed(2));// + parseFloat($("#txtddswValue").val());
	$("#txtGTTLVoyageEarnings").val(g_voyage_earning.toFixed(2));
	
	if($("#txtTDays").val() == 0){var ttl_days = 1;}else{var ttl_days = $("#txtTDays").val();}
	var daily_earnings = parseFloat(g_voyage_earning) / parseFloat(ttl_days);
	$("#txtDailyEarnings,#txtNettDailyEarnings").val(daily_earnings.toFixed(2));
	
	if($("#txtDailyVesselOperatingExpenses").val() == ""){var daily_vessel_exp = 0;}else{var daily_vessel_exp = $("#txtDailyVesselOperatingExpenses").val();}
	var nett_daily_profit = parseFloat($("#txtNettDailyEarnings").val()) - parseFloat(daily_vessel_exp);
	$("#txtNettDailyProfit").val(nett_daily_profit.toFixed(2));	
	
	$("#txtPL").val( parseFloat(parseFloat($("#txtNettDailyProfit").val()) *  parseFloat($("#txtTDays").val())).toFixed(2));	
	
	$('[id^=txtBunkerCost_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				var costMT = this.value / parseFloat(per_mt);
				$("#txtBunkerCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
	$('[id^=txtLPOSCCost_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				var costMT = this.value / parseFloat(per_mt);
				$("#txtLPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
	$('[id^=txtDPOSCCost_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				var costMT = this.value / parseFloat(per_mt);
				$("#txtDPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
	$('[id^=txtTPOSCCost_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				var costMT = this.value / parseFloat(per_mt);
				$("#txtTPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
}


function getCalculateAddnl()
{
	if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}
	if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
	var diff  = parseFloat(cargo_qty) - parseFloat(min_qty); 
	if(diff.toFixed(2) < 0){$("#txtAddnlQMT").val("0.00");}
	else{$("#txtAddnlQMT").val(diff.toFixed(2));}
	
}

function getDistance()
{
	$("#txtDistance").val("");
	$("#loader1").show();
	if($('#selFPort').val() != "" && $('#selTPort').val() != "" && $('#selDType').val() != "")
	{
		$("#txtDistance").val("");
		$.post("options.php?id=10",{selFPort:""+$("#selFPort").val()+"",selTPort:""+$("#selTPort").val()+"",selDType:""+$("#selDType").val()+""}, function(data) 
		{
				$('#txtDistance').val(data);
				$("#loader1").hide();
		});
	}
	else
	{
		$('#txtDistance').val("");
		$("#loader1").hide();
	}
}

//..............for Port Rotation details...............................................
function addPortRotationDetails()
{
	if($("#selFPort").val() != "" && $("#selTPort").val() != "" && $("#selPType").val() != "" && $("#txtDistance").val() != "" && $("#selSSpeed").val() != "")
	{
			var id = $("#p_rotationID").val();
			var lasrvar1 = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar1 = rowid.split('_')[1];
			});
		if($("#selFPort").val() == $("#txtTPort_"+lasrvar1).val() || id == 0 || lasrvar1 == "")
		{
			getVoyageTime();
			id = (id - 1) + 2;
			$("#PRrow_Empty").remove();
			var frm_port = document.getElementById("selFPort").selectedIndex;
			var frm_port_text = document.getElementById("selFPort").options[frm_port].text;
			var to_port = document.getElementById("selTPort").selectedIndex;
			var to_port_text = document.getElementById("selTPort").options[to_port].text;
			var p_type = document.getElementById("selPType").selectedIndex;
			var p_type_text = document.getElementById("selPType").options[p_type].text;
			
			var d_type = document.getElementById("selDType").selectedIndex;
			var d_type_text = document.getElementById("selDType").options[d_type].text;
			
			var speed_type = document.getElementById("selSSpeed").selectedIndex;
			var speed_type_text = document.getElementById("selSSpeed").options[speed_type].text;
			
			$('<tr id="pr_Row_'+id+'"><td align="center" class="input-text" ><a href="#pr'+id+'" id="spcancel_'+id+'" onclick="removePortRotation('+id+','+$("#selFPort").val()+','+$("#selTPort").val()+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left" class="input-text" >'+frm_port_text+'<input type="hidden" name="txtFPort_'+id+'" id="txtFPort_'+id+'" class="input" size="10" value="'+$("#selFPort").val()+'"/></td><td align="left" class="input-text" >'+to_port_text+'<input type="hidden" name="txtTPort_'+id+'" id="txtTPort_'+id+'" class="input" size="10" value="'+$("#selTPort").val()+'"/></td><td align="left" class="input-text" >'+p_type_text+'('+speed_type_text+')<input type="hidden" name="txtPType_'+id+'" id="txtPType_'+id+'" class="input" size="10" value="'+$("#selPType").val()+'"/><input type="hidden" name="txtSSpeed_'+id+'" id="txtSSpeed_'+id+'" class="input" size="10" value="'+$("#selSSpeed").val()+'"/></td><td align="left" class="input-text" >'+parseFloat($("#txtDistance").val())+'('+d_type_text+')<input type="hidden" name="txtDistance_'+id+'" id="txtDistance_'+id+'" class="input" size="10" value="'+parseFloat($("#txtDistance").val())+'"/><input type="hidden" name="txtDType_'+id+'" id="txtDType_'+id+'" class="input" size="10" value="'+$("#selDType").val()+'"/></td><td align="left" class="input-text" >'+$("#txtWeather").val()+'<input type="hidden" name="txtWeather_'+id+'" id="txtWeather_'+id+'" class="input" size="10" value="'+$("#txtWeather").val()+'"/></td><td align="left" class="input-text" >'+$("#txtMargin").val()+'<input type="hidden" name="txtMargin_'+id+'" id="txtMargin_'+id+'" class="input" size="10" value="'+$("#txtMargin").val()+'"/></td></tr>').appendTo("#tblPortRotation");
			
			//for load port.....................
			$('<option value="'+$("#selFPort").val()+'">'+frm_port_text+'</option>').appendTo("#selLoadPort");
			//.........ends...................
			//for discharge port.....................
			$('<option value="'+$("#selTPort").val()+'">'+to_port_text+'</option>').appendTo("#selDisPort");
			//.........ends...................
			//for transit port.....................
			$('<option value="'+$("#selFPort").val()+'">'+frm_port_text+'</option>').appendTo("#selTLoadPort");
			//.........ends...................
			$("#p_rotationID").val(id);
			$("#selFPort").val($("#selTPort").val());
			$("#selTPort,#selPType,#txtDistance,#txtWeather,#txtMargin,#selSSpeed").val("");
			$("#selDType").val("1");
			
			var lasrvar = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar = rowid.split('_')[1];
			});
			$("[id^=spcancel_]").hide();
			$("#spcancel_"+lasrvar).show();
			
			//getTTLFreight();
		}
		else
		{
			jAlert('Please select in sequence of ports', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Sea Passage', 'Alert');
	}
}

function getVoyageTime()
{
	if($("#txtMargin").val() == ""){var margin = 0;}else{var margin = $("#txtMargin").val();}
	if($("#txtDistance").val() == ""){var distance = 0;}else{var distance = $("#txtDistance").val();}
	if($("#txtWeather").val() == ""){var speed_adj = 0;}else{var speed_adj = $("#txtWeather").val();}
	if($("#selPType").val() == 1)
	{
		if($("#selSSpeed").val() == 1)
		{
			var ballast = $("#txtBFullSpeed").val();
			var fo_ballast = $("#txtBFOFullSpeed").val();
			var do_ballast = $("#txtBDOFullSpeed").val();
		}
		if($("#selSSpeed").val() == 2)
		{
			var ballast = $("#txtBEcoSpeed1").val();
			var fo_ballast = $("#txtBFOEcoSpeed1").val();
			var do_ballast = $("#txtBDOEcoSpeed1").val();
		}
		if($("#selSSpeed").val() == 3)
		{
			var ballast = $("#txtBEcoSpeed2").val();
			var fo_ballast = $("#txtBFOEcoSpeed2").val();
			var do_ballast = $("#txtBDOEcoSpeed2").val();
		}
		
		var calc = ($("#txtDistance").val() /  (parseFloat(ballast) + parseFloat(speed_adj)))/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) + parseFloat(calc) + parseFloat(margin);
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		var ttl_days = parseFloat(calc) + parseFloat($("#txtBDays").val()) + parseFloat(margin);
		$("#txtBDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ballast));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ballast));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat(distance) + parseFloat($("#txtBDist").val());
		$("#txtBDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
	if($("#selPType").val() == 2)
	{
		if($("#selSSpeed").val() == 1)
		{
			var ladan = $("#txtLFullSpeed").val();
			var fo_ladan = $("#txtLFOFullSpeed").val();
			var do_ladan = $("#txtLDOFullSpeed").val();
		}
		if($("#selSSpeed").val() == 2)
		{
			var ladan = $("#txtLEcoSpeed1").val();
			var fo_ladan = $("#txtLFOEcoSpeed1").val();
			var do_ladan = $("#txtLDOEcoSpeed1").val();
		}
		if($("#selSSpeed").val() == 3)
		{
			var ladan = $("#txtLEcoSpeed2").val();
			var fo_ladan = $("#txtLFOEcoSpeed2").val();
			var do_ladan = $("#txtLDOEcoSpeed2").val();
		}
		
		var calc = ($("#txtDistance").val() / (parseFloat(ladan) + parseFloat(speed_adj)) )/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) + parseFloat(calc) + parseFloat(margin);
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		var ttl_days = parseFloat(calc) + parseFloat($("#txtLDays").val()) + parseFloat(margin);
		$("#txtLDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ladan));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ladan));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat(distance) + parseFloat($("#txtLDist").val());
		$("#txtLDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
	//getTTLFreight();
}

function removePortRotation(var1,portid,disportid)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){	
			//for load port.....................
			$("#selLoadPort option[value='"+$("#txtFPort_"+var1).val()+"']").remove();
			$('[id^=txtLoadPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == portid)
				{
					if($("#txtPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtPCosts_"+rowid.split('_')[1]).val();}
					if($("#txtLPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtLPIDays_"+rowid.split('_')[1]).val();}
					if($("#txtLPWDays_"+rowid.split('_')[1]).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtLPWDays_"+rowid.split('_')[1]).val();}
					
					var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
					$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
					$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDays(rowid.split('_')[1]);
					
					$("#lp_Row_"+rowid.split('_')[1]).remove();
					$("#oscLProw_"+rowid.split('_')[1]).remove();
					$("#oscLProw1_"+rowid.split('_')[1]).remove();
					$("#ddswLProw_"+rowid.split('_')[1]).remove();
					$("#ddswLProw1_"+rowid.split('_')[1]).remove();
					
					if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
					$("#txtDDNRLPCost").val($("[id^=txtDDSLPOSCCost_]").sum().toFixed(2));
					var ttlcostMT = parseFloat($("#txtDDNRLPCost").val()) / parseFloat(c_qty);
					$("#txtDDNRLPCostMT").val(ttlcostMT.toFixed(2));
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					//getTTLFreight();
				}
			});
			//.........ends...................	
			//for discharge port.....................
			$("#selDisPort option[value='"+$("#txtTPort_"+var1).val()+"']").remove();
			$('[id^=txtDisPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == disportid)
				{
					if($("#txtDPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtDPCosts_"+rowid.split('_')[1]).val();}
					if($("#txtDPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtDPIDays_"+rowid.split('_')[1]).val();}
					if($("#txtDPWDays_"+rowid.split('_')[1]).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtDPWDays_"+rowid.split('_')[1]).val();}
					
					var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
					$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
					$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDaysDP(rowid.split('_')[1]);
					
					$("#dp_Row_"+rowid.split('_')[1]).remove();
					$("#oscDProw_"+rowid.split('_')[1]).remove();
					$("#oscDProw1_"+rowid.split('_')[1]).remove();			
					//$("#oscDDRDProw_"+rowid.split('_')[1]).remove();
					//$("#oscDDRDProw1_"+rowid.split('_')[1]).remove();
					
					
					if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
					$("#txtDDNRDPCost").val($("[id^=txtDDRDPOSCCost_]").sum().toFixed(2));
					var ttlcostMT = parseFloat($("#txtDDNRDPCost").val()) / parseFloat(c_qty);
					$("#txtDDNRDPCostMT").val(ttlcostMT.toFixed(2));
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					//getTTLFreight();
				}
			});
			//.........ends...................		
			//for transit port.....................
			$("#selTLoadPort option[value='"+$("#txtFPort_"+var1).val()+"']").remove();
			$('[id^=txtTLoadPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == portid)
				{
					if($("#txtTPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtTPCosts_"+rowid.split('_')[1]).val();}
					if($("#txtTPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtTPIDays_"+rowid.split('_')[1]).val();}
								
					var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDaysTP(rowid.split('_')[1]);
					
					$("#tp_Row_"+rowid.split('_')[1]).remove();
					$("#oscTProw_"+rowid.split('_')[1]).remove();
					$("#oscTProw1_"+rowid.split('_')[1]).remove();
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					//getTTLFreight();
				}
			});
			//.........ends...................
			getRemoveVoyageTime(var1);	
			$("#pr_Row_"+var1).remove();
			var lasrvar = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar = rowid.split('_')[1];
			});
			$("#selTPort,#selPType,#txtDistance,#txtWeather,#txtMargin,#selSSpeed").val("");
			$("#selFPort").val($("#txtTPort_"+lasrvar).val());
			$("[id^=spcancel_]").hide();
			$("#spcancel_"+lasrvar).show();
			//getTTLFreight();
		}
		else{return false;}
		});	
}

function getRemoveVoyageTime(var1)
{
	if($("#txtMargin_"+var1).val() == ""){var margin = 0;}else{var margin = $("#txtMargin_"+var1).val();}
	if($("#txtDistance_"+var1).val() == ""){var distance = 0;}else{var distance = $("#txtDistance_"+var1).val();}
	if($("#txtWeather_"+var1).val() == ""){var speed_adj = 0;}else{var speed_adj = $("#txtWeather_"+var1).val();}
	if($("#txtPType_"+var1).val() == 1)
	{
		if($("#txtSSpeed_"+var1).val() == 1)
		{
			var ballast = $("#txtBFullSpeed").val();
			var fo_ballast = $("#txtBFOFullSpeed").val();
			var do_ballast = $("#txtBDOFullSpeed").val();
		}
		if($("#txtSSpeed_"+var1).val() == 2)
		{
			var ballast = $("#txtBEcoSpeed1").val();
			var fo_ballast = $("#txtBFOEcoSpeed1").val();
			var do_ballast = $("#txtBDOEcoSpeed1").val();
		}
		if($("#txtSSpeed_"+var1).val() == 3)
		{
			var ballast = $("#txtBEcoSpeed2").val();
			var fo_ballast = $("#txtBFOEcoSpeed2").val();
			var do_ballast = $("#txtBDOEcoSpeed2").val();
		}
		
		var calc = ($("#txtDistance_"+var1).val() / (parseFloat(ballast) + parseFloat(speed_adj)) )/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));	
		
		var ttl_days = parseFloat($("#txtBDays").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtBDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var diff_fo = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ballast);
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(diff_fo.toFixed(2));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		
		var diff_do = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ballast);
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(diff_do.toFixed(2));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat($("#txtBDist").val()) - parseFloat(distance);
		$("#txtBDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));	
	}
	if($("#txtPType_"+var1).val() == 2)
	{
		if($("#txtSSpeed_"+var1).val() == 1)
		{
			var ladan = $("#txtLFullSpeed").val();
			var fo_ladan = $("#txtLFOFullSpeed").val();
			var do_ladan = $("#txtLDOFullSpeed").val();
		}
		if($("#txtSSpeed_"+var1).val() == 2)
		{
			var ladan = $("#txtLEcoSpeed1").val();
			var fo_ladan = $("#txtLFOEcoSpeed1").val();
			var do_ladan = $("#txtLDOEcoSpeed1").val();
		}
		if($("#txtSSpeed_"+var1).val() == 3)
		{
			var ladan = $("#txtLEcoSpeed2").val();
			var fo_ladan = $("#txtLFOEcoSpeed2").val();
			var do_ladan = $("#txtLDOEcoSpeed2").val();
		}
		
		var calc = ($("#txtDistance_"+var1).val() / (parseFloat(ladan) + parseFloat(speed_adj)) )/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		
		var ttl_days = parseFloat($("#txtLDays").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtLDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var diff_fo = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ladan);
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(diff_fo.toFixed(2));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		
		var diff_do = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ladan);
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(diff_do.toFixed(2));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat($("#txtLDist").val()) - parseFloat(distance);
		$("#txtLDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
	//getTTLFreight();
}
//...................Port Rotation details ends here........................................

function getLOadPortQty()
{
	if($("#selLPCName").val() != "")
	{
		if($("#txtCQMT").val() != "")
		{
			$("#txtQMT").val(parseFloat($("#txtCQMT").val()) - parseFloat($("[id^=txtLpQMT_]").sum()));
			getLoadPortCalculation();
		}
		else
		{
			$("#txtQMT").val(0);
		}
	}
	else
	{
		$("#txtQMT,#txtRate,#txtWDays,#txtWDays1").val("");
	}
}

function getLoadPortCalculation()
{
	if($("#txtQMT").val() != "" && $("#txtRate").val() != "") 
	{
		var value = ($("#txtQMT").val() / $("#txtRate").val());
		$("#txtWDays").val(value.toFixed(2));
	}
	else
	{
		$("#txtWDays").val('0.00');
	}
}

//..............for Load Port details...............................................
function addLoadPortDetails()
{
	if($("#selLoadPort").val() != "" && $("#txtQMT").val() != "" && $("#txtRate").val() != "" && $("#selLPCName").val() != "")
	{
		if(parseFloat($("#txtCQMT").val()) >= parseFloat($("[id^=txtLpQMT_],#txtQMT").sum()))
		{
			getTTLVoyageDays();
			var id = $("#load_portID").val();
			id = (id - 1) + 2;
			$("#LProw_Empty").remove();
			var load_port = document.getElementById("selLoadPort").selectedIndex;
			var load_port_text = document.getElementById("selLoadPort").options[load_port].text;
			
			var cargo = document.getElementById("selLPCName").selectedIndex;
			var cargo_text = document.getElementById("selLPCName").options[cargo].text;
			
			$('<tr id="lp_Row_'+id+'"><td align="center" class="input-text" ><a href="#lp'+id+'" onclick="removeLoadPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left" class="input-text" >'+load_port_text+'<input type="hidden" name="txtLoadPort_'+id+'" id="txtLoadPort_'+id+'" class="input" size="10" value="'+$("#selLoadPort").val()+'"/></td><td align="left" class="input-text" >'+cargo_text+'<input type="hidden" name="txtLPCID_'+id+'" id="txtLPCID_'+id+'" class="input" size="10" value="'+$("#selLPCName").val()+'"/></td><td align="left" class="input-text" >'+$("#txtQMT").val()+'<input type="hidden" name="txtLpQMT_'+id+'" id="txtLpQMT_'+id+'" class="input" size="10" value="'+$("#txtQMT").val()+'"/></td><td align="left" class="input-text" >'+$("#txtRate").val()+'<input type="hidden" name="txtLPRate_'+id+'" id="txtLPRate_'+id+'" class="input" size="10" value="'+$("#txtRate").val()+'"/></td><td align="left" class="input-text" >'+$("#txtPCosts").val()+'<input type="hidden" name="txtPCosts_'+id+'" id="txtPCosts_'+id+'" class="input" size="10" value="'+$("#txtPCosts").val()+'"/></td><td align="left" class="input-text" >'+$("#txtIDays").val()+'<input type="hidden" name="txtLPIDays_'+id+'" id="txtLPIDays_'+id+'" class="input" size="10" value="'+$("#txtIDays").val()+'"/></td><td align="left" class="input-text" >'+$("#txtWDays").val()+'<input type="hidden" name="txtLPWDays_'+id+'" id="txtLPWDays_'+id+'" class="input" size="10" value="'+$("#txtWDays").val()+'"/></td></tr>').appendTo("#tblLoadPort");
			
			if($("#txtPCosts").val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtPCosts").val();}
			var lpcostMT = parseFloat(lp_cost) / parseFloat($("#txtCQMT").val());
			var lp = "'LP'";		
			$('<tr id="oscLProw_'+id+'"><td>Load Port '+load_port_text+'</td><td></td><td><input type="text"  name="txtLPOSCCost_'+id+'" id="txtLPOSCCost_'+id+'" class="form-control" readonly="true" value="'+$("#txtPCosts").val()+'"/></td><td><input type="text" name="txtLPOSCCostMT_'+id+'" id="txtLPOSCCostMT_'+id+'" class="form-control" readonly="true" value="'+lpcostMT.toFixed(2)+'"/></td></tr><tr id="oscLProw1_'+id+'"><td colspan="10"></td></tr>').appendTo("#tbPortCosts");
		
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
			$('<tr id="ddswLProw_'+id+'"><td>Load Port '+load_port_text+'</td><td></td><td><input type="text"  name="txtDDSOLPCost_'+id+'" id="txtDDSOLPCost_'+id+'" class="form-control" onkeyup="getFinalCalculation();" value="0.00"/></td><td><input type="text" name="txtDDSOLPCostMT_'+id+'" id="txtDDSOLPCostMT_'+id+'" class="form-control" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDSW");
			
			$("#txtTtPIDays").val($("#txtTtPIDays,#txtIDays").sum().toFixed(2));
			$("#txtTtPWDays").val($("#txtTtPWDays,#txtWDays").sum().toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			$("[id^=txtDDSLPOSCCost_],[id^=txtDDNRLPOSCCost_]").numeric();
			$("#load_portID").val(id);
			$("#txtQMT,#txtRate,#selLoadPort,#txtPCosts,#txtIDays,#txtWDays,#txtWDays1,#txtLPDraftM,#selLPCName,#selCrType").val("");
			//getTTLFreight();
		}
		else
		{
			jAlert('Loaded quantity is greater than Cargo Qty.', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Load Port', 'Alert');
	}
}

function getShowHide()
{
	if($("#txtbid").val() == 0)
	{
		document.BID.src = "../../img/open.png";
		$('#BID').attr('title', 'Open Panel');
		$("#bunker_adj").hide();
		$("#txtbid").val(1);
	}
	else
	{
		document.BID.src = "../../img/close.png";
		$('#BID').attr('title', 'Close Panel');
		$("#bunker_adj").show();
		$("#txtbid").val(0);
	}
}

function getShowHide1()
{
	if($("#txtbid1").val() == 0)
	{
		document.BID1.src = "../../img/open.png";
		$('#BID1').attr('title', 'Open Panel');
		$("#bunker_adj1").hide();
		$("#txtbid1").val(1);
	}
	else
	{
		document.BID1.src = "../../img/close.png";
		$('#BID1').attr('title', 'Close Panel');
		$("#bunker_adj1").show();
		$("#txtbid1").val(0);
	}
}

function getTTLVoyageDays()
{
	if($("#txtIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtIDays").val();}
	if($("#txtWDays").val() == ""){var w_days = 0;}else{var w_days = $("#txtWDays").val();}	
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
	var wrkingdays_fo = parseFloat(w_days) * parseFloat($("#txtPWFOFullSpeed").val());
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
	var wrkingdays_do = parseFloat(w_days) * parseFloat($("#txtPWDOFullSpeed").val());			
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
	//getTTLFreight();
}

function removeLoadPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){ 
			if($("#txtPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtPCosts_"+var1).val();}
			if($("#txtLPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtLPIDays_"+var1).val();}
			if($("#txtLPWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtLPWDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
			var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			getRemoveTTLVoyageDays(var1);
			
			$("#lp_Row_"+var1).remove();
			$("#oscLProw_"+var1).remove();
			$("#oscLProw1_"+var1).remove();
			$("#ddswLProw_"+var1).remove();
			$("#ddswLProw1_"+var1).remove();
			
			if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
			$("#txtDDNRLPCost").val($("[id^=txtDDSLPOSCCost_]").sum().toFixed(2));
			var ttlcostMT = parseFloat($("#txtDDNRLPCost").val()) / parseFloat(c_qty);
			$("#txtDDNRLPCostMT").val(ttlcostMT.toFixed(2));
			
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			//getTTLFreight();
		}
		else{return false;}
		});	
}

function getRemoveTTLVoyageDays(var1)
{
	if($("#txtLPIDays_"+var1).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtLPIDays_"+var1).val();}
	if($("#txtLPWDays_"+var1).val() == ""){var w_days = 0;}else{var w_days = $("#txtLPWDays_"+var1).val();}
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) -  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
	var wrkingdays_fo = parseFloat(w_days) * parseFloat($("#txtPWFOFullSpeed").val());
	
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2)));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
	var wrkingdays_do = parseFloat(w_days) * parseFloat($("#txtPWDOFullSpeed").val());			
	
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2)));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	//getTTLFreight();
}
function getDisPortQty()
{
	if($("#selDPCName").val() != "")
	{
		if($("#txtCQMT").val() != "")
		{
			$("#txtDQMT").val(parseFloat($("#txtCQMT").val()) - parseFloat($("[id^=txtDpQMT_]").sum()));
			getDisPortCalculation();
		}
		else
		{
			$("#txtDQMT").val(0);
		}
	}
	else
	{
		$("#txtDQMT,#txtDRate,#txtDWDays,#txtDWDays1").val("");
	}
}

function getDisPortCalculation()
{
	if($("#txtDQMT").val() != "" && $("#txtDRate").val() != "") 
	{
		var value = ($("#txtDQMT").val() / $("#txtDRate").val());
		$("#txtDWDays").val(value.toFixed(2));
	}
	else
	{
		$("#txtDWDays,#txtDWDays1").val('0.00');
	}
}

//..............for Dis Port details...............................................
function addDisPortDetails()
{
	if($("#selDisPort").val() != "" && $("#txtDQMT").val() != "" && $("#txtDRate").val() != "" && $("#selDPCName").val() != "" )
	{
		if(parseFloat($("#txtCQMT").val()) >= parseFloat($("[id^=txtDpQMT_],#txtDQMT").sum()))
		{
			getTTLVoyageDaysDP();
			var id = $("#dis_portID").val();
			id = (id - 1) + 2;
			$("#DProw_Empty").remove();
			var dis_port = document.getElementById("selDisPort").selectedIndex;
			var dis_port_text = document.getElementById("selDisPort").options[dis_port].text;
			
			var cargo = document.getElementById("selDPCName").selectedIndex;
			var cargo_text = document.getElementById("selDPCName").options[cargo].text;
						
			$('<tr id="dp_Row_'+id+'"><td align="center" class="input-text" ><a href="#dp'+id+'" onclick="removeDisPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left" class="input-text" >'+dis_port_text+'<input type="hidden" name="txtDisPort_'+id+'" id="txtDisPort_'+id+'" class="input" size="10" value="'+$("#selDisPort").val()+'"/></td><td align="left" class="input-text" >'+cargo_text+'<input type="hidden" name="txtDPCID_'+id+'" id="txtDPCID_'+id+'" class="input" size="10" value="'+$("#selDPCName").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDQMT").val()+'<input type="hidden" name="txtDpQMT_'+id+'" id="txtDpQMT_'+id+'" class="input" size="10" value="'+$("#txtDQMT").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDRate").val()+'<input type="hidden" name="txtDPRate_'+id+'" id="txtDPRate_'+id+'" class="input" size="10" value="'+$("#txtDRate").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDPCosts").val()+'<input type="hidden" name="txtDPCosts_'+id+'" id="txtDPCosts_'+id+'" class="input" size="10" value="'+$("#txtDPCosts").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDIDays").val()+'<input type="hidden" name="txtDPIDays_'+id+'" id="txtDPIDays_'+id+'" class="input" size="10" value="'+$("#txtDIDays").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDWDays").val()+'<input type="hidden" name="txtDPWDays_'+id+'" id="txtDPWDays_'+id+'" class="input" size="10" value="'+$("#txtDWDays").val()+'"/></td></tr>').appendTo("#tblDisPort");
			
		if($("#txtDPCosts").val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPCosts").val();}
		var dpcostMT = parseFloat(dp_cost) / parseFloat($("#txtCQMT").val());
		var dp = "'DP'";
		$('<tr id="oscDProw_'+id+'"><td>Discharge Port '+dis_port_text+'</td><td></td><td><input type="text"  name="txtDPOSCCost_'+id+'" id="txtDPOSCCost_'+id+'" class="form-control" readonly="true" value="'+$("#txtDPCosts").val()+'" /></td><td><input type="text" name="txtDPOSCCostMT_'+id+'" id="txtDPOSCCostMT_'+id+'" class="form-control" readonly="true" value="'+dpcostMT.toFixed(2)+'"  /></td></tr><tr id="oscDProw1_'+id+'"><td colspan="10"></td></tr>').appendTo("#tbPortCosts");
		
		$('<tr id="ddswDProw_'+id+'"><td>Discharge Port '+dis_port_text+'</td><td></td><td><input type="text"  name="txtDDSODPCost_'+id+'" id="txtDDSODPCost_'+id+'" class="form-control" value="0.00" onkeyup="getFinalCalculation();"/></td><td><input type="text" name="txtDDSODPCostMT_'+id+'" id="txtDDSODPCostMT_'+id+'" class="form-control" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDSW");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
			$("#txtTtPIDays").val($("#txtTtPIDays,#txtDIDays").sum().toFixed(2));
			$("#txtTtPWDays").val($("#txtTtPWDays,#txtDWDays").sum().toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			$("#dis_portID").val(id);
			$("#selDisPort,#txtDQMT,#txtDRate,#txtDPDraftM,#txtDPCosts,#txtDIDays,#txtDWDays,#txtDWDays1,#selDPCName,#selDPCrType").val("");
			//getTTLFreight();
		}
		else
		{
			jAlert('Discharged quantity is greater than Cargo Qty.', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Discharge Port', 'Alert');
	}
}

function getTTLVoyageDaysDP()
{
	if($("#txtDIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtDIDays").val();}
	if($("#txtDWDays").val() == ""){var w_days = 0;}else{var w_days = $("#txtDWDays").val();}
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
	var wrkingdays_fo = parseFloat(w_days) * parseFloat($("#txtPWFOFullSpeed").val());
		
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
	var wrkingdays_do = parseFloat(w_days) * parseFloat($("#txtPWDOFullSpeed").val());	
		
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
	
	//getTTLFreight();
}

function removeDisPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){ 
			if($("#txtDPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtDPCosts_"+var1).val();}
			if($("#txtDPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtDPIDays_"+var1).val();}
			if($("#txtDPWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtDPWDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
			var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			getRemoveTTLVoyageDaysDP(var1);
			
			$("#dp_Row_"+var1).remove();
			$("#oscDProw_"+var1).remove();
			$("#oscDProw1_"+var1).remove();			
			//$("#oscDDRDProw_"+var1).remove();
			//$("#oscDDRDProw1_"+var1).remove();
			
			if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
			$("#txtDDNRDPCost").val($("[id^=txtDDRDPOSCCost_]").sum().toFixed(2));
			var ttlcostMT = parseFloat($("#txtDDNRDPCost").val()) / parseFloat(c_qty);
			$("#txtDDNRDPCostMT").val(ttlcostMT.toFixed(2));
			
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			//getTTLFreight();
		}
		else{return false;}
		});	
}

function getRemoveTTLVoyageDaysDP(var1)
{
	if($("#txtDPIDays_"+var1).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtDPIDays_"+var1).val();}
	if($("#txtDPWDays_"+var1).val() == ""){var w_days = 0;}else{var w_days = $("#txtDPWDays_"+var1).val();}
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) -  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
	var wrkingdays_fo = parseFloat(w_days) * parseFloat($("#txtPWFOFullSpeed").val());
		
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2)));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
	var wrkingdays_do = parseFloat(w_days) * parseFloat($("#txtPWDOFullSpeed").val());			
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2)));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	//getTTLFreight();
}

function addTransitPortDetails()
{
	if($("#selTLoadPort").val() != "")
	{
		getTTLVoyageDaysTP();
		var id = $("#transit_portID").val();
		id = (id - 1) + 2;
		$("#TProw_Empty").remove();
		var load_port = document.getElementById("selTLoadPort").selectedIndex;
		var load_port_text = document.getElementById("selTLoadPort").options[load_port].text;
				
		$('<tr id="tp_Row_'+id+'"><td align="center" class="input-text" ><a href="#dp'+id+'" onclick="removeTransitPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left" class="input-text" >'+load_port_text+'<input type="hidden" name="txtTLoadPort_'+id+'" id="txtTLoadPort_'+id+'" class="input" size="10" value="'+$("#selTLoadPort").val()+'"/></td><td align="left" class="input-text" >'+$("#txtTLPCosts").val()+'<input type="hidden" name="txtTPCosts_'+id+'" id="txtTPCosts_'+id+'" class="input" size="10" value="'+$("#txtTLPCosts").val()+'"/></td><td align="left" class="input-text" >'+$("#txtTLIDays").val()+'<input type="hidden" name="txtTPIDays_'+id+'" id="txtTPIDays_'+id+'" class="input" size="10" value="'+$("#txtTLIDays").val()+'"/></td></tr>').appendTo("#tblTransitPort");
		
		if($("#txtTLPCosts").val() == ""){var tp_cost = 0;}else{var tp_cost = $("#txtTLPCosts").val();}
		var tpcostMT = parseFloat(tp_cost) / parseFloat($("#txtCQMT").val());
		var tp = "'TP'";
		$('<tr id="oscTProw_'+id+'"><td>Transit Port   '+load_port_text+'</td><td></td><td><input type="text"  name="txtTPOSCCost_'+id+'" id="txtTPOSCCost_'+id+'" class="form-control" readonly="true" value="'+$("#txtTLPCosts").val()+'" /></td><td><input type="text" name="txtTPOSCCostMT_'+id+'" id="txtTPOSCCostMT_'+id+'" class="form-control" readonly="true" value="'+tpcostMT.toFixed(2)+'" /></td></tr><tr id="oscTProw1_'+id+'"><td colspan="10"></td></tr>').appendTo("#tbPortCosts");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
		$("#txtTtPIDays").val($("#txtTtPIDays,#txtTLIDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		$("#transit_portID").val(id);
		$("#selTLoadPort,#txtTLPCosts,#txtTLIDays").val("");
		//getTTLFreight();
	}
	else
	{
		jAlert('Please fill all the records for Tansit Port', 'Alert');
	}
}

function getTTLVoyageDaysTP()
{
	if($("#txtTLIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtTLIDays").val();}
	var ttl_days1 = parseFloat(idle_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo);
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do);
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	//getTTLFreight();
}

function removeTransitPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){
			if($("#txtTPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtTPCosts_"+var1).val();}
			if($("#txtTPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtTPIDays_"+var1).val();}
						
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			getRemoveTTLVoyageDaysTP(var1);
			 
			$("#tp_Row_"+var1).remove();
			$("#oscTProw_"+var1).remove();
			$("#oscTProw1_"+var1).remove();
			
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			//getTTLFreight();
		}
		else{return false;}
		});	
}

function getRemoveTTLVoyageDaysTP(var1)
{
	if($("#txtTPIDays_"+var1).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtTPIDays_"+var1).val();}
	var ttl_days1 = parseFloat(idle_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) -  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(margin_fo);
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());		
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(margin_do);
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	//getTTLFreight();
}

function getORCCalculate(status,var1)
{
	if(status == 1)
	{
		$("#txtHidORCAmt_"+var1).val($("#txtORCAmt_"+var1).val());
	}
	if(status == 2)
	{
		$("#txtHidORCAmt_"+var1).val("-"+$("#txtORCAmt_"+var1).val());
	}
	getFinalCalculation();
}

function getEnabled(var1,var2)
{
	if($("#checkIFO_"+var1).attr('checked'))
	{
		$("[id^=txt"+$("#txtBHID_"+var1).val()+"_]").removeAttr("disabled","");
		$("[id^=txt"+$("#txtBHID_"+var1).val()+"_]").numeric();
		$("#txtTTLEst_"+var1).val("0");
		
		var str=var2.substr(0,3);
		if(str == 'IFO')
		{
			$("[id^=txt"+var2+"_1_1]").val($("#txtTFUMT").val());
		}
		else if(str == 'MDO')
		{
			$("[id^=txt"+var2+"_1_1]").val($("#txtTDUMT").val());
		}
		else if(str == 'MGO')
		{
			$("[id^=txt"+var2+"_1_1]").val($("#txtTDUMT").val());
		}
		else
		{
			$("[id^=txt"+var2+"_1_1]").val('0.00');
		}
		getBunkerTable(var2);
	}
	else
	{
		$("[id^=txt"+$("#txtBHID_"+var1).val()+"_]").attr("disabled",true);
		$("[id^=txt"+$("#txtBHID_"+var1).val()+"_]").val("");
		$("#txtTTLEst_"+var1).val("0");
		$("[id^=txt"+var2+"_1_1]").val('');
		removeBunkerTable(var2);
	}
	
}

function getBunkerTable(var1)
{	
	$('<tr id="rowB_'+var1+'"><td>'+var1+' Nett</td><td></td><td><input type="text"  name="txtBunkerCost_'+var1+'" id="txtBunkerCost_'+var1+'" class="form-control"  placeholder="0.00" disabled /></td><td><input type="text" name="txtBunkerCostMT_'+var1+'" id="txtBunkerCostMT_'+var1+'" class="form-control" disabled value="" placeholder="0.00" /></td></tr>').appendTo("#tbbunkers");
}

function removeBunkerTable(var1)
{
	$("#rowB_"+var1).remove();
}

function getCalculate1(var1,var2,var3)
{
	$("#txt"+var1+"_"+var2+"_1").val(var3);
	if(var3 == ""){var first = 0;}else{var first = var3;}
	if($("#txt"+var1+"_"+var2+"_2").val() == ""){var second = 0;}else{var second = $("#txt"+var1+"_"+var2+"_2").val();}
	var calc = parseFloat(first) * parseFloat(second);
	$("#txt"+var1+"_"+var2+"_3").val(calc.toFixed(2));
	//$("#txtTTLEst_"+var4).val($("#txt"+var1+"_1_3").val());	
}


function getCalculate(var1,var2,var3,var4)
{
	$("#txt"+var1+"_"+var2+"_2").val(var3);
	if($("#txt"+var1+"_"+var2+"_1").val() == ""){var first = 0;}else{var first = $("#txt"+var1+"_"+var2+"_1").val();}
	if(var3 == ""){var second = 0;}else{var second = var3;}
	var calc = parseFloat(first) * parseFloat(second);
	$("#txt"+var1+"_"+var2+"_3").val(calc.toFixed(2));
	$("#txtTTLEst_"+var4).val($("#txt"+var1+"_1_3").val());	
}

function getCharterersCostCalculate(var1)
{
	if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}
	if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
	if(parseFloat(cargo_qty) >= parseFloat(min_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	else
	{
		var per_mt = parseFloat(min_qty);
	}
	
	if($("#txtCCAbs_"+var1).val() == ""){var cc_abs = 0;}else{var cc_abs = $("#txtCCAbs_"+var1).val();}
	var calc = parseFloat(cc_abs) / parseFloat(per_mt);
	$("#txtCCostMT_"+var1).val(calc.toFixed(2));
	getFinalCalculation();	
}

function getPortCostSum(var1,port)
{
	if(port == 'LP')
	{
		if($("#txtLPOSCCost_"+var1).val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtLPOSCCost_"+var1).val();}
		var lpcostMT = parseFloat(lp_cost) / parseFloat($("#txtCQMT").val());
		$("#txtLPOSCCostMT_"+var1).val(lpcostMT.toFixed(2));
	}
	else if(port == 'DP')
	{
		if($("#txtDPOSCCost_"+var1).val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPOSCCost_"+var1).val();}
		var dpcostMT = parseFloat(dp_cost) / parseFloat($("#txtCQMT").val());
		$("#txtDPOSCCostMT_"+var1).val(dpcostMT.toFixed(2));
	}
	else if(port == 'TP')
	{
		if($("#txtTPOSCCost_"+var1).val() == ""){var tp_cost = 0;}else{var tp_cost = $("#txtTPOSCCost_"+var1).val();}
		var tpcostMT = parseFloat(tp_cost) / parseFloat($("#txtCQMT").val());
		$("#txtTPOSCCostMT_"+var1).val(tpcostMT.toFixed(2));
	}
	getFinalCalculation();			
}

function getOMCCalculate(var1)
{
	if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}
	if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
	if(parseFloat(cargo_qty) >= parseFloat(min_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	else
	{
		var per_mt = parseFloat(min_qty);
	}
	
	if($("#txtOMCAbs_"+var1).val() == ""){var omc_abs = 0;}else{var omc_abs = $("#txtOMCAbs_"+var1).val();}
	var calc = parseFloat(omc_abs) / parseFloat(per_mt);
	$("#txtOMCCostMT_"+var1).val(calc.toFixed(2));
	getFinalCalculation();	
}

function addBrokerageRow()
{
	var id = $("#txtBRokageCount").val();
	if($("#txtBrCommPercent_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="tbrRow_'+id+'"><td><a href="#tb1" onclick="removeBrokerage('+id+');" ><i class="fa fa-times" style="color:red;"></a></td><td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td><td><input type="text" name="txtBrCommPercent_'+id+'" id="txtBrCommPercent_'+id+'" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td><td><input type="text" name="txtBrComm_'+id+'" id="txtBrComm_'+id+'" class="form-control" readonly value="0.00" /></td><td></td></tr>').appendTo("#tbodyBrokerage");
	    $("#selBroVList_"+id).html($("#selVendor").html());$("#selBroVList_"+id).val('');
		$("[id^=txtBrCommPercent_]").numeric();	
		$("#txtBRokageCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeBrokerage(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#tbrRow_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}

</script>
    </body>
</html>