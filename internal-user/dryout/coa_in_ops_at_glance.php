<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	if($_REQUEST['txtStatus'] == "")
	{
		if($_REQUEST['txtComid2'] == "")
		{
			$msg = $obj->insertActualCostSheetName();
		}
		else
		{
			$msg = $obj->deleteInOpsEntry($_REQUEST['txtComid2']);
		}
		header('Location:./coa_in_ops_at_glance.php?msg=3');
	}
	else if($_REQUEST['txtStatus'] == "1")
	{
		$msg = $obj->convertToPostOps($_REQUEST['txtComid2']);
	}
	else if($_REQUEST['txtStatus'] == "2")
	{
		
	}
	
	header('Location:./coa_in_ops_at_glance.php?msg=3');
 }
 
if (@$_REQUEST['action1'] == 'submit1')
{
	if (@$_REQUEST['txtuid'] == 1)
	{
		$msg = $obj->insertPerfAtSeaUploadDetails();
		header('Location:./coa_in_ops_at_glance.php?msg='.$msg);
	}
	else if (@$_REQUEST['txtuid'] == 2)
	{
		$msg = $obj->insertPerfAtPortUploadDetails();
		header('Location:./coa_in_ops_at_glance.php?msg='.$msg);
	}
}

if (isset($_REQUEST['selYear']) && $_REQUEST['selYear']!="")
{
	$selYear = $_REQUEST['selYear'];
}
else
{
	$selYear = date('Y',time());
}

if (isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$selBType = $_SESSION['selBType'] = $_REQUEST['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	unset($_SESSION['selBType']);
	$selBType = '';
}

$pagename = basename($_SERVER['PHP_SELF']);

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rights = $obj->getUserRights($uid,$moduleid,4); 

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(15); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops COA&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops COA&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance COA</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> In Ops at a glance added/updated successfully.
				</div>
				<?php }?>
                <?php if($msg == 4){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> new sheet added successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> In Ops at a glance sheet successfully create.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }?>
				<?php if($msg == 6){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Nomination sent to "Post Ops".
				</div>
				<?php }}?>
				<div class="box box-primary">
					<h3 style=" text-align:center;">In Ops at a glance - COA</h3>
							
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                            <div class="col-xs-6">
                            </div>
                            <div class="col-xs-3">
                                 <select name="selBType" id="selBType" class="form-control" onChange="getSubmit();">
                                 <option value="">---Select Business Type---</option>
								   <?php echo $obj->getBusinessTypeList1($selBType);?>
                                  </select>                  
							</div><!-- /.col -->
						    <div class="col-xs-3">
								  <select name="selYear" id="selYear" class="form-control" onChange="getSubmit();">
                                   <?php echo $obj->getYearList($selYear);?>
                                  </select>                      
							</div><!-- /.col -->
						<div style="height:10px;">
							<input type="hidden" name="txtComid2" id="txtComid2" value="" />
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<input type="hidden" name="txtFileName" id="txtFileName" value="" />
							<input type="hidden" name="action" id="action" value="submit" />
						</div>
                
						<div class="box-body table-responsive" style="overflow:auto;">
							<table class="table table-bordered table-striped" id='in_ops_at_glance'>
								<thead>
									<tr valign="top">
										<th align="left">FVF/ Voyage Docs</th>
										<th align="left">Nom ID</th>
                                        <th align="left">Business Type</th>
										<th align="left">Material Name</th>
                                        <th align="left">Vessel</th>
                                        <th align="left">CP Date</th>
                                        <th align="left">Fixture Date/ (Voyage No.)</th>
										<th align="left">Check List</th>
										<th align="left">Voyage Financials</th>
										<th align="left">PDA Request</th>
										<th align="left">Appoint Agent</th>
										<th align="left">SOF</th>	
										<th align="left">Laytime</th>
										<th align="center">Payment / Invoice</th>
										<?php if($rights == 1){ ?>
										<th align="center">Deactivate / Alerts</th>
										<?php }else{ ?>
                                        <th align="center">Alerts</th>
                                        <?php }?>
										<th align="center">Re&nbsp;-&nbsp;Del</th>
										<?php if($rights == 1){ ?>
										<th align="center">Complete</th>
										<?php }else{ ?>
                                       
                                        <?php }?>
									</tr>
								</thead>
                                <tbody>
									<?php
									if($selBType!='')
									{
									$sql = "select freight_cost_estimate_compare.COMID as COMID, (select CARGO from coa_master where coa_master.COAID=freight_cost_estimate_compare.COAAID) as CARGOIDD, freight_cost_estimate_compare.FCAID as FCAID, freight_cost_estimete_master.VESSEL_IMO_ID as VESSEL_IMO_ID, freight_cost_estimate_compare.MESSAGE as MESSAGE, freight_cost_estimete_master.COA_SPOT as COA_SPOT, freight_cost_estimete_master.FIXTURETYPEID as FIXTURETYPEID from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS=1 and year(freight_cost_estimete_master.ADD_ON_DATE)='".$selYear."' and freight_cost_estimete_master.ESTIMATE_TYPE='".$_SESSION['selBType']."' and COAAID is not NULL order by date(freight_cost_estimete_master.FINAL_DATETIME) desc"; 
                                    $res = mysql_query($sql);
									$rec = mysql_num_rows($res);
									$i=1;
									$submit = 0;
										if($rec == 0)
										{
											echo '<tbody>';
									
											echo '</tbody>';
										}else{
									?>
								
									<?php
										while($rows = mysql_fetch_assoc($res))
										{
											$cp_date = $obj->getCompareEstimateData($rows['COMID'],"CP_DATE");
											if($cp_date!="0000-00-00" && $cp_date!=""){$cp_date = date('d-m-Y',strtotime($cp_date));}else{$cp_date = "";}
											
											$ESTIMATE_TYPE = $obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE");
										?>
										<tr id="in_row_<?php echo $i;?>" style="font-size:11px;">
                                           <td>
												<a href="viewcoaestimates.php?id=<?php echo $rows['FCAID'];?>&rttype=1" style="color:green;" title="Click me" >FVF</a><br/><br/>
												<a href="documents.php?comid=<?php echo $rows['COMID'];?>&page=4" style="color:green;" title="Click me" >Docs</a>
											</td>
                                            
                                            <td>
												<?php echo $rows['MESSAGE'];?>
											</td>
                                            <td>
												<?php echo $obj->getEstimateList($ESTIMATE_TYPE);?>
											</td>
											<td>
												<?php echo $obj->getMaterialCodeDesBasedOnCode($rows['CARGOIDD']);?>
											</td>
                                            
											<td>
												<?php echo $obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME");?>/<br/> <?php echo $obj->getCompareEstimateData($rows['COMID'],"VESSEL_TYPE");?>
											</td>
                                            <td>
												<?php echo $cp_date;?>
											</td>
                                            <?php 
											$fixture_date = $obj->getCompareEstimateData($rows['COMID'],"TRANS_DATE");
											if($fixture_date!="0000-00-00" && $fixture_date!=""){$fixture_date = date('d-m-Y',strtotime($fixture_date));}else{$fixture_date = "";}?>
                                            <td>
												<?php echo $fixture_date;?>/<?php echo $obj->getCompareEstimateData($rows['COMID'],"VOYAGE_NO");?>
											</td>
                                            <td>
												<?php echo $obj->getFreightFixtureBasedOnID($rows['FIXTURETYPEID']);?><br/><br/>
													<a href="check_list.php?comid=<?php echo $rows['COMID'];?>&page=4" style="color:red;" title="Click me" >Check List</a>	
											</td>
                                            
											<td style="text-align:center;">
												<?php
													$sql1 = "select * from cost_sheet_name_master where COMID='".$rows['COMID']."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";

													$res1 = mysql_query($sql1);
													$rec1 = mysql_num_rows($res1);
													if($rec1 > 0)
													{
														while($rows1 = mysql_fetch_assoc($res1))
														{
															if($rows1['PROCESS'] != "EST")
															{
																
																$href = "./cost_sheet_coa.php?comid=".$rows['COMID']."&cost_sheet_id=".$rows1['COST_SHEETID']."&page=1";
																
																echo "<a href='".$href."'>".str_replace(' ','&nbsp;',$rows1['SHEET_NAME'])."</a><br/>";
																$curr_cst = $rows1['COST_SHEETID'];
															}
															else
															{
																$curr_cst = $obj->getCostSheetFixtureID($rows['COMID'],"SHEET_NO");
															}
														}
													}	
													if($rights == 1){		
												   $fcaid = $obj->getLatestCostSheetID($rows['COMID']);	
											       if($obj->getEstimateData($fcaid,"FINAL_STATUS")==1)
													{
													?>
														<br/><button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-modal1" type="button" title="Add New CS" onClick="openWin1(<?php echo $rows['COMID'];?>);">A</button>
													<?php }else{?>
                                                    <br/><button class="btn btn-primary btn-flat" type="button" title="Add New CS" onClick="msg();">A</button>
                                                    <?php }}?>
											</td>
											<td>
												<a href="agency_letter_generation.php?comid=<?php echo $rows['COMID'];?>&tab=1&page=4">Generate Port Related Letters</a>
											</td>
											<td>
												<a href="nominate_agent.php?comid=<?php echo $rows['COMID'];?>&page=4">PDA/FDA</a>
											</td>			
											<td>
												<a href="sof.php?comid=<?php echo $rows['COMID'];?>&page=4">SOF</a>		
											</td>
											<td>
												<?php if($obj->getFreightEstimationStatus($rows['COMID']) == 2 )
													{?>
												<a href="laytime_calculation.php?comid=<?php echo $rows['COMID'];?>&page=4" style="color:blue;" title="Click me" >Calculations</a>
												<?php }?>
											</td>
											<td>
												<a href="payment_gridcoa.php?comid=<?php echo $rows['COMID'];?>&page=4" >View</a><br><br>
												<br>
												<a href="piclubdeclaration.php?comid=<?php echo $rows['COMID'];?>&page=4" >P & I Club Declaration</a>
												
											</td>
                                            
                                            <td style="text-align:center;">
											    <?php if($rights == 1){ ?>
												<a href="#A" title="Deactivate entry" onClick="getValidate2(<?php echo $rows['COMID'];?>,<?php echo $i;?>);">
													<i class="fa fa-times" style="color:red;"></i>
												</a><br/>
												<?php }?>
                                                <button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-modal2" type="button" title="Compare Sheets" onClick="getCompareSheetData(<?php echo $rows['COMID'];?>);">Compare Sheets</button>
                                                
                                                <?php 
												$freightsql = "select * from freight_invoice_master WHERE (MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$rows['COMID']."' and P_AMT is NULL) or (MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$rows['COMID']."' and P_AMT=0)";
												$freightres = mysql_query($freightsql);
												$freightrec = mysql_num_rows($freightres);
												if($freightrec >0)
												{?>
                                                <span style="color:red; font-weight:bold;">Payment not Received</span><br/><br/>
                                                <?php }?>
												
												<?php 
												$hiresql = "select * from invoice_hire_master WHERE (MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$rows['COMID']."' and P_AMT is NULL) or (MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$rows['COMID']."' and P_AMT=0)";
												$hireres = mysql_query($hiresql);
												$hirerec = mysql_num_rows($hireres);
												if($hirerec >0)
												{?>
                                                <span style="color:red; font-weight:bold;">Payment not Paid</span>
                                                <?php }
												?>
											</td>
											<td>
											   <?php 
												$re_del_date = $obj->getReDelDate($rows['COMID']);
												if($re_del_date!="" && $re_del_date!="1970-01-01")
												{
													echo date('d-m-Y',strtotime($re_del_date));
												}
												?>
                                            </td>
											
                                            <?php if($rights == 1){ ?>
											<td>
												<button class="btn btn-primary btn-flat" id="inner-login-button"  type="button" title="Push to Post Ops" onClick="getValidate3(<?php echo $rows['COMID'];?>);">Post Ops</button>
											</td>
											
										<?php }else
											{ ?>
											
                                        <?php }?>
										</tr>	
                                        
									<?php $i = $i + 1; } }}else{?>
                                    
                                    <?php }?>
								</tbody>
							</table>
						</div>
					</form>
										
				</div>
                
                
                			
				</div>
                
                
                <div class="modal fade" id="compose-modal1" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body">
									<div class="box box-primary">
										<div class="box-body no-padding">
											<form name="frm2" id="frm2" method="post" enctype="multipart/form-data" action="<?php echo $pagename;?>">
												<div class="row invoice-info">
													
												</div>
												
												<div class="row invoice-info" style="padding:2%">
													<div class="col-sm-12 invoice-col">
														Please enter Voyage Financials Name and Submit
														<address>
															<input type="text" name="txtFile" id="txtFile" class="form-control" autocomplete="off" value="<?php echo $obj->getNomDetailsData($comid,"FREIGHT_RATE");?>" placeholder="Voyage Financials Name" />
															<input type="hidden" name="txtComid1" id="txtComid1" />
														</address>
													</div><!-- /.col -->
												</div>
												
												
												<div class="row invoice-info">
													<div class="col-sm-12 invoice-col" style="text-align:center;">
														 <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
														 <input type="hidden" name="action" id="action" value="submit" />
													</div><!-- /.col -->
												</div>
												
											</form>
										</div><!-- /.box-body -->
									</div>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
                    
                    
                <div class="modal fade" id="compose-modal2" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" style="width:98%">
					    <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Compare Sheets</h4>
                            </div>
                            <form name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                            <div class="modal-body" id="div_3" style="overflow:auto;">
                            </div>
                            
                            </form>
					    </div>
				    </div><!-- /.modal-dialog -->
				</div>		
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>

<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.validate.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#frm1").validate({
		rules: {
			
		},
		messages: {
			
		},
		submitHandler: function(form)  {
			jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
			$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "110%"});
			$("#popup_content").css({"background":"none","text-align":"center"});
			$("#popup_ok,#popup_title").hide();  
			form.submit();
		}
	});
	$("#frm2").validate({
	rules: {
		
	},
	messages: {
		
	},
	submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "110%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
	//sortDateFormat();
	$("#in_ops_at_glance").dataTable({
    "iDisplayLength": 100
    });
	
//for uploading........
 $('#photoimg').live('change', function(){ 
	$("#cpDiv_"+$("#txtVar").val()).append("<span id='labelDiv_"+$("#txtVar").val()+"_"+$("#txtID_"+$("#txtVar").val()).val()+"'>abc</span>");
	$("#labelDiv_"+$("#txtVar").val()+"_"+$("#txtID_"+$("#txtVar").val()).val()).html('<img src="../../images/loader.gif" alt="Uploading...." width="20" height="10"  />');
	$("#imageform").ajaxForm({
				target: '#labelDiv_'+$("#txtVar").val()+'_'+$("#txtID_"+$("#txtVar").val()).val()
	}).submit();
	var id = $("#txtID_"+$("#txtVar").val()).val();
	id = (id - 1) + 2;
	$("#txtID_"+$("#txtVar").val()).val(id);
	$.modal.close();
});
//....................
}); 
function getData()
{
	$("#in_ops_at_glance tbody").empty();
	$("#in_ops_at_glance tbody").html('<tr><td height="200" colspan="17" align="center" ><img src="../../img/loading.gif" /></td></tr>');
	$.post("options.php?id=42",{selVName:""+$("#selVName").val()+""}, function(html) {
	$("#in_ops_at_glance tbody").empty();
	$("#in_ops_at_glance tbody").append(html);
	});
}
function openWin1(comid)
{
	$('#basic-modal-content').modal();
	$("#txtComid1").val(comid);
	$("#simplemodal-container").css({"height":"200px"});
}

function getValidate()
{
	if($("#txtFile").val() != "")
	{
		document.frm2.submit();
	}
	else
	{
		jAlert('Please fill the file name', 'Alert');
		return false;
	}
}

function msg()
{
	jAlert('Please make sure the last Voyage Financials is Submit to Close', 'Alert');
}

function openWin2(var1,mappinid)
{
	$('#basic-modal-content1').modal();
	$("#txtVar").val(var1);
	$("#txtComid").val(comid);
	$("#simplemodal-container").css({"width":"300px","height":"50px"});
}	

function openWin(comid,vesselid,id)
{
	$('#basic-modal').modal();
	$("#txtComid").val(comid);
	$("#txtVesselid").val(vesselid);
	$("#txtuid").val(id);
	$("#simplemodal-container").css({"width":"300px","height":"50px"});
}

function getValid()
{
	if($('#txtfile').val() == '')
	{
		jAlert('Please choose the file', 'Alert');
		return false;
	}
	else
	{
		var name = $('#txtfile').val();
		name = name.split('.');
		if(name[1] == 'txt')
		{
			return true;
		}
		else
		{
			jAlert('Please choose only .txt file', 'Alert');
			$('#txtfile').val('');
			return false;
		}
	}
}

function getPdfForSea(comid,vesselid)
{
	location.href='allPdf.php?id=12&comid='+comid+'&vesselid='+vesselid;
}

function getValidate2(var1,rowid)
{
	$("#in_row_"+rowid+" td").css({"background-color":"red"});
	jConfirm('Are you sure to de-activate this Nom ID ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtComid2").val(var1); 
			document.frm1.submit();
		}
		else{$("#in_row_"+rowid+" td").css({"background-color":""});return false;}
		});	
}

function getValidate3(var1)
{
	jConfirm('Are you sure to send this Nom ID to post ops ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtComid2").val(var1);
			$("#txtStatus").val(1);
			 
			document.frm1.submit();
		}
		else{return false;}
		});	
}

function getValidate4(var1,file_name)
{
	jConfirm('Are you sure to remove this document permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtComid2").val(var1);
			$("#txtStatus").val(2);
			$("#txtFileName").val(file_name);
			document.frm1.submit();
		}
		else{return false;}
		});	
}

function getSubmit()
{ 
    $("#action").val("");
	 
	document.frm1.submit();
}


function getCompareSheetData(comid)
{
	$("#div_3").empty();
	$("#div_3").html('<div style="height:200px;text-align:center;"><img src="../../img/loading.gif" /></div>');
	$.post("options.php?id=49",{comid:""+comid+""}, function(html) {
		$("#div_3").empty();
		$("#div_3").html(html);
	});
}

</script>
		
</body>
</html>