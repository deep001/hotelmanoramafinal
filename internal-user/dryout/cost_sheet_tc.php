<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid 		= $_REQUEST['comid'];
$cost_sheet_id  = $_REQUEST['cost_sheet_id'];
$page  			= $_REQUEST['page'];

if($page == 1){$page_link = "in_ops_tc.php";}else if($page == 2){$page_link = "vessel_in_post_tc.php";}else {$page_link = "vessel_in_history_tc.php";}

if($obj->getTCCostSheetStatusDetails($comid,$cost_sheet_id)==1)
{
	header('Location:./updateclosedcost_sheet_tc.php?comid='.$comid.'&cost_sheet_id='.$cost_sheet_id."&page=".$page);
}
else if($obj->getTCCostSheetRecDetails($comid,$cost_sheet_id) > 0)
{
	header('Location:./updatecost_sheet_tc.php?comid='.$comid.'&cost_sheet_id='.$cost_sheet_id."&page=".$page);
}

if(@$_REQUEST['action'] == 'submit')
{ 
 	$msg = $obj->insertTCCostSheetDetails();
	header('Location:./'.$page_link.'?msg='.$msg);
}

$id = $obj->getLatestCostSheetIDTC($comid);
$obj->viewTCEstimationTempleteRecordsNew($id);
//echo $obj->getFun1();die;
$pagename   = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&cost_sheet_id=".$cost_sheet_id."&page=".$page;?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
table {border-collapse:separate;}
select{height:19px;}
.select {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-weight: bold;
    color: #333333;
    /* color: #FBC763; */
    text-decoration: none;
    line-height: 13px;
	height:13px;
}

.input-text {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #333333;
	text-decoration: none;
	/*text-transform:uppercase;
	line-height: 15px;*/
}

.input-textsmall {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-weight: normal;
	color: #333333;
	text-decoration:none;
	text-transform:uppercase;
}


.input 
{
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 10px;
font-weight: normal;
color: #333333;
text-decoration: none;
}
</style>
</head>
	<div id="basic-modal-content" style="display:none;" align="center">
	<table cellpadding="1" cellspacing="4" border="0" align="center">
	<tr><td align="center"><img src="../../img/loading.gif" /><br /></td></tr>
	</table>
	</div>
	
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(21); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops TC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops at a glance - TC</li>
                    </ol>
                </section>
                
                <div align="right" style="margin-top:5px;margin-bottom:5px;margin-right:5px;"><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                	<table width="100%" style="padding:0px 5px 0px 5px;">
                       <tr class="input-text">
                           <td width="50px;"><strong>Fixture&nbsp;Type&nbsp;:</strong></td>
                           <td>&nbsp;TC&nbsp;Out&nbsp;<input name="selFType" type="hidden" id="selFType" value="1"></td>
                           <td>
                           <strong>Vessel&nbsp;:</strong></td>
						   <td><select name="selVName" id="selVName" class="input-text" style="width:110px;color:#175c84;background-color:#EAEAEA;" onChange="getData();" disabled="disabled">
                           <?php
                                $obj->getVesselTypeListMemberWise($_SESSION['selBType']);
                                ?>
                           </select>
                           </td>
                           <td><strong>Vessel&nbsp;Type&nbsp;:</strong></td>
                           <td><input type="text" name="txtVType" id="txtVType" style="width:110px;color:#175c84;background-color:#EAEAEA;" class="input-text" readonly value="<?php echo $obj->getFun4();?>" />
                             <select  name="selBunker" style="display:none;" id="selBunker">
                                <?php $obj->getBunkerList(); ?>
                            </select>
                            <select name="selExpenseType" id="selExpenseType" style="display:none;">
                            <?php $obj->getExpenseTypeList();?>
                            </select>
							<select name="selCurrencyList" id="selCurrencyList" style="display:none;">
                            <?php $obj->getCurrencyList();?>
                            </select>
							<select  name="selVendor" style="display:none;"  id="selVendor" >
                            <?php $obj->getVendorListNewUpdate();	?>
                            </select> 
                           </td>
                           <td><strong>Flag&nbsp;:</strong></td>
                           <td><input type="text" name="txtFlag" id="txtFlag" style="width:110px;color:#175c84;background-color:#EAEAEA;" class="input-text" readonly value="<?php echo $obj->getFun5();?>" /></td>
                            <td><strong>Date&nbsp;:</strong></td>
                            <td><input type="text" name="txtDate" id="txtDate" style="width:110px;color:#175c84;background-color:#EAEAEA;" class="input-text" value="<?php echo date("d-m-Y",strtotime($obj->getFun6()));?>" placeholder"dd-mm-yy" readonly="true" /></td>
                            <td ><strong>TC&nbsp;No.&nbsp;:</strong></td>
                            <td><input type="text" name="txtTCNo" id="txtTCNo" style="width:110px;color:#175c84;background-color:#EAEAEA;" class="input-text" value="<?php echo $obj->getFun7();?>" readonly/></td>
                       </tr>
                       <tr class="input-text">
                          <td ><strong>CP&nbsp;Date&nbsp;:&nbsp;</strong></td>
                          <td><input autocomplete="off" type="text" name="txtCPdate2" id="txtCPdate2" class="input-text" value="<?php echo date("d-m-Y", strtotime($obj->getFun9())); ?>" style="width:110px;background-color:#EAEAEA;" placeholder="dd-mm-yy" readonly /></td>
                          <td ><strong>CP&nbsp;Type&nbsp;:&nbsp;</strong></td>
                          <td><input autocomplete="off" type="text" name="txtCPType" id="txtCPType" class="input-text" style="width:110px; background-color:#EAEAEA;" readonly value="<?php echo $obj->getContractTypeMasterData($obj->getFun10(),'CONTRACT_TYPE');?>"/></td>
                          
                          <td ><span id="tripspan_2"><strong>Trip TC&nbsp;:</strong></span><input name="selFType" type="hidden" id="selFType" value="1"></td>
                           <td><span id="tripspan_3"><input autocomplete="off" type="text" name="txtTripTC" id="txtTripTC" class="input-text" style="width:110px;" placeholder="Trip TC" value="<?php echo $obj->getFun74(); ?>"/></span></td>
                           <td><span id="periodspan_2"><strong>Period&nbsp;:</strong></span></td>
                           <td><span id="periodspan_3"><input autocomplete="off" type="text" name="txtPeriod" id="txtPeriod" style="width:110px;" class="input-text" placeholder="Period" value="<?php echo $obj->getFun75(); ?>"/></span></td>
                           <td><span id="noOfTripspan_2"><strong>No. Of Trips&nbsp;:</strong></span></td>
                           <td><span id="noOfTripspan_3"><input autocomplete="off" type="text" name="txtNoOfTrips" id="txtNoOfTrips" class="input-text" style="width:110px;" placeholder="No. Of Trips" value="<?php echo $obj->getFun76(); ?>"/></span></td>
                           <td ><strong>Charterers&nbsp;:&nbsp;</strong></td>
                          <td><input autocomplete="off" type="text" name="txtCharterers" id="txtCharterers" class="input-text" style="width:200px;background-color:#EAEAEA;" readonly value="<?php echo $obj->getVendorListNewData($obj->getFun11(),'NAME');?>"/><input type="hidden" name="hddnDailyGrossHireUSD" id="hddnDailyGrossHireUSD" value="<?php echo $obj->getFun54();?>"/><input type="hidden" name="hddnaddComm" id="hddnaddComm" value="<?php echo $obj->getFun68();?>"/><input type="hidden" name="hddnbrokerComm" id="hddnbrokerComm" value="<?php echo $obj->getFun69();?>"/></td>
                       </tr>
                       <tr class="input-text">
                          
                          <td><strong>Total Days</strong></td>
                          <td><input autocomplete="off" type="text" name="txtTotalUdays" id="txtTotalUdays" class="input-text" style="width:110px;background-color:#EAEAEA;" readonly value="<?php echo $obj->getFun117(); ?>"/></td>
                           <td><strong>Total TC Earnings</strong></td>
                           <td><input autocomplete="off" type="text" name="txtTotalVoyageEarning" id="txtTotalVoyageEarning" class="input-text" style="width:110px;background-color:#EAEAEA;" readonly value="<?php echo $obj->getFun118(); ?>"/></td>
                           <td ></td>
                          <td></td>
                          <td></td>
                          <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                          <td></td>
                       </tr>
                   </table>
		
				       <div id="maintripdiv">
	                   <?php $mx = 0;
						$sql2 = "select * from chartering_tc_estimate_slave1 where TCOUTID = '".$obj->getFun1()."'";
						$res2 = mysql_query($sql2) or die($sql2);
						$num  = mysql_num_rows($res2);
						while($rows = mysql_fetch_assoc($res2))
						{ $mx++; ?>
                           
                           <div id="tripdiv_<?php echo $mx;?>">
                           <div style="margin-bottom:7px; margin-top:8px; height:2px; background-color:#ccc;width:100%;"></div>
                           <div><strong style="font-size:15px;padding:0px 5px 0px 5px;""><a href="#tbl<?php echo $mx;?>" onClick="removeSheet(<?php echo $mx;?>);" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;Trip/Period <?php echo $mx;?></strong></div>
                           <table width="100%" style="margin-bottom:10px; padding:0px 5px 0px 5px;">
                               <tr class="input-text">
                                  <td >Del Date/Time&nbsp;<input type="hidden" name="txtCOUTSlaveRandomID_<?php echo $mx;?>" id="txtCOUTSlaveRandomID_<?php echo $mx;?>" value="<?php echo $rows['RANDOMID'];?>"/><input type="hidden" name="txtCOUTSlaveID_<?php echo $mx;?>" id="txtCOUTSlaveID_<?php echo $mx;?>" value="<?php echo $rows['TC_SLAVE1ID'];?>"/><input type="hidden" name="txtCOUTSlaveIdentify_<?php echo $mx;?>" id="txtCOUTSlaveIdentify_<?php echo $mx;?>" value="update"/></td>
                                  <td><input autocomplete="off" type="text" name="txtDeldate_<?php echo $mx;?>" id="txtDeldate_<?php echo $mx;?>" class="input-text" value="<?php if($num == 0){ echo date("d-m-Y", time());} else { if(date("d-m-Y", strtotime($rows['DEL_DATE_EST']))!='01-01-1970'){echo date("d-m-Y H:i", strtotime($rows['DEL_DATE_EST']));}else{echo '';} }?>" style="width:100px;" placeholder="dd-mm-yy hh:mm" /></td>
                                  <td >Re Del Date/Time&nbsp;</td>
                                  <td><input autocomplete="off" type="text" name="txtReDeldate_<?php echo $mx;?>" id="txtReDeldate_<?php echo $mx;?>" class="input-text" value="<?php if($num==0){ echo date("d-m-Y", time());} else{if(date("d-m-Y", strtotime($rows['REDEL_DATE_EST']))!='01-01-1970'){echo date("d-m-Y H:i", strtotime($rows['REDEL_DATE_EST']));}else{echo '';}} ?>" style="width:100px;" placeholder="dd-mm-yy hh:mm" /></td>
                                  <td >TC Days&nbsp;</td>
                                  <td><input autocomplete="off" type="text" name="txtTCdays_<?php echo $mx;?>" id="txtTCdays_<?php echo $mx;?>" class="input-text" placeholder="TC Days" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo $rows['TC_DAYS_EST'];?>"/></td>
                                  <td></td><td></td><td></td><td></td><td></td><td></td>
                               </tr>
                               <tr class="input-text">
                                  <td colspan="6">
                                     <table width="100%">
                                         <tr class="input-text">
                                            <td colspan="5"><strong>Delivery</strong></td>
                                         </tr>
                                         <tr class="input-text">
                                           <td>#</td>
                                           <td>Bunker Grade</td>
                                           <td>Qty(MT)</td>
                                           <td>Bunker Date</td>
                                           <td>Price USD/MT)</td>
                                           <td>Amount(USD)</td>
                                         </tr>
                                         <tbody id="bunkerdelivery_<?php echo $mx;?>">
                                        <?php 
										 $sql3 = "select * from chartering_estimate_tc_slave5 where TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."' and IDENTITY='DEL'";
										 $result3 = mysql_query($sql3);
										 $num3 = mysql_num_rows($result3);
										 
										 $k = 0;
                                         if($num3==0){$k++;?>
                                            <tr class="input-text" id="row_del_<?php echo $mx;?>_1">
                                               <td><a href="#tbl1" onClick="removeDelBunker(<?php echo $mx;?>,1);" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td>
                                               <td><select name="selDelBunker_<?php echo $mx;?>_1" class="input-text" style=" width:90px;" id="selDelBunker_<?php echo $mx;?>_1"></select></td>
                                               <td><input type="text" name="txtDelQty_<?php echo $mx;?>_1" id="txtDelQty_<?php echo $mx;?>_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelBunDate_<?php echo $mx;?>_1" id="txtDelBunDate_<?php echo $mx;?>_1" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelPrice_<?php echo $mx;?>_1" id="txtDelPrice_<?php echo $mx;?>_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelAmount_<?php echo $mx;?>_1" id="txtDelAmount_<?php echo $mx;?>_1" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                            </tr>
                                            <script>$("#selDelBunker_<?php echo $mx;?>_1").html($("#selBunker").html());</script>
                                       <?php }else{
                                           while($rows3 = mysql_fetch_assoc($result3))
                                           {$k++;
                                           if($rows3['BUNKER_DATE']=="" || $rows3['BUNKER_DATE']=="0000-00-00"  || $rows3['BUNKER_DATE']=="1970-01-01"){$txtDelBunDate = "";}else{$txtDelBunDate = date('d-m-Y',strtotime($rows3['BUNKER_DATE']));}
                                           ?>
                                             <tr class="input-text" id="row_del_<?php echo $mx;?>_<?php echo $k;?>">
                                               <td><a href="#tbl_<?php echo $mx;?>_<?php echo $k;?>" onClick="removeDelBunker(<?php echo $mx;?>,<?php echo $k;?>);" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td>
                                               <td><select name="selDelBunker_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style=" width:90px;" id="selDelBunker_<?php echo $mx;?>_<?php echo $k;?>"></select></td>
                                               <td><input type="text" name="txtDelQty_<?php echo $mx;?>_<?php echo $k;?>" id="txtDelQty_<?php echo $mx;?>_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['QTY'];?>" placeholder="0.00" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelBunDate_<?php echo $mx;?>_<?php echo $k;?>" id="txtDelBunDate_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style="width:85px;" value="<?php echo $txtDelBunDate;?>" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelPrice_<?php echo $mx;?>_<?php echo $k;?>" id="txtDelPrice_<?php echo $mx;?>_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['PRICE'];?>" placeholder="0.00" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" autocomplete="off" /></td>
                                               <td><input type="text" name="txtDelAmount_<?php echo $mx;?>_<?php echo $k;?>" id="txtDelAmount_<?php echo $mx;?>_<?php echo $k;?>" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="<?php echo $rows3['AMOUNT'];?>" placeholder="0.00" autocomplete="off"/></td>
                                            </tr>
                                           <script>$("#selDelBunker_<?php echo $mx;?>_<?php echo $k;?>").html($("#selBunker").html());$("#selDelBunker_<?php echo $mx;?>_<?php echo $k;?>").val(<?php echo $rows3['BUNKERID'];?>)</script>
                                       <?php }}?>
                                        </tbody>
                                        <tbody>		
                                        <tr class="input-text">
                                            <td colspan="2"><button type="button" onClick="AddNewDel(<?php echo $mx;?>);">Add</button><input type="hidden" name="txtDELID_<?php echo $mx;?>" id="txtDELID_<?php echo $mx;?>" value="<?php echo $k;?>"/></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ><input type="text" name="txtTotalDelAmount_<?php echo $mx;?>" id="txtTotalDelAmount_<?php echo $mx;?>" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                            
                                        </tr>
                                     </table>
                                  
                                  </td>
                                  <td colspan="6">
                                     <table width="100%">
                                        <tr class="input-text">
                                            <td colspan="5"><strong>Re-Delivery</strong></td>
                                        </tr>
                                        <tr class="input-text">
                                           <td>#</td>
                                           <td>Bunker Grade</td>
                                           <td>Qty(MT)</td>
                                           <td>Bunker Date</td>
                                           <td>Price USD/MT)</td>
                                           <td>Amount(USD)</td>
                                        </tr>
                                        <tbody id="bunkerRedelivery_<?php echo $mx;?>">
                                        <?php 
										 $sql3 = "select * from chartering_estimate_tc_slave5 where TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."' and IDENTITY='REDEL'";
										 $result3 = mysql_query($sql3);
										 $num3 = mysql_num_rows($result3);
										 
										 $k = 0;
                                         if($num3==0){$k++;?>
                                            <tr class="input-text" id="row_Redel_<?php echo $mx;?>_1">
                                               <td><a href="#tbl1" onClick="removeReDelBunker(<?php echo $mx;?>,1);" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td>
                                               <td><select name="selReDelBunker_<?php echo $mx;?>_1" class="input-text" style=" width:90px;" id="selReDelBunker_<?php echo $mx;?>_1"></select></td>
                                               <td><input type="text" name="txtReDelQty_<?php echo $mx;?>_1" id="txtReDelQty_<?php echo $mx;?>_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelBunDate_<?php echo $mx;?>_1" id="txtReDelBunDate_<?php echo $mx;?>_1" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelPrice_<?php echo $mx;?>_1" id="txtReDelPrice_<?php echo $mx;?>_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelAmount_<?php echo $mx;?>_1" id="txtReDelAmount_<?php echo $mx;?>_1" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                            </tr>
                                            <script>$("#selReDelBunker_<?php echo $mx;?>_1").html($("#selBunker").html());</script>
                                            <?php }else{
                                           while($rows3 = mysql_fetch_assoc($result3))
                                           {$k++;
                                           if($rows3['BUNKER_DATE']=="" || $rows3['BUNKER_DATE']=="0000-00-00"  || $rows3['BUNKER_DATE']=="1970-01-01"){$txtReDelBunDate = "";}else{$txtReDelBunDate = date('d-m-Y',strtotime($rows3['BUNKER_DATE']));}
                                           ?>
                                             <tr class="input-text" id="row_Redel_<?php echo $mx;?>_<?php echo $k;?>">
                                               <td><a href="#tbl_<?php echo $mx;?><?php echo $k;?>" onClick="removeReDelBunker(<?php echo $mx;?>,<?php echo $k;?>);" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td>
                                               <td><select name="selReDelBunker_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style=" width:90px;" id="selReDelBunker_<?php echo $mx;?>_<?php echo $k;?>"></select></td>
                                               <td><input type="text" name="txtReDelQty_<?php echo $mx;?>_<?php echo $k;?>" id="txtReDelQty_<?php echo $mx;?>_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['QTY'];?>" placeholder="0.00" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelBunDate_<?php echo $mx;?>_<?php echo $k;?>" id="txtReDelBunDate_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style="width:85px;" value="<?php echo $txtReDelBunDate;?>" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelPrice_<?php echo $mx;?>_<?php echo $k;?>" id="txtReDelPrice_<?php echo $mx;?>_<?php echo $k;?>" class="input-text numeric" style="width:85px;" value="<?php echo $rows3['PRICE'];?>" placeholder="0.00" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" autocomplete="off" /></td>
                                               <td><input type="text" name="txtReDelAmount_<?php echo $mx;?>_<?php echo $k;?>" id="txtReDelAmount_<?php echo $mx;?>_<?php echo $k;?>" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="<?php echo $rows3['AMOUNT'];?>" placeholder="0.00" autocomplete="off"/></td>
                                            </tr>
                                           <script>$("#selReDelBunker_<?php echo $mx;?>_<?php echo $k;?>").html($("#selBunker").html());$("#selReDelBunker_<?php echo $mx;?>_<?php echo $k;?>").val(<?php echo $rows3['BUNKERID'];?>)</script>
                                       <?php }}?>
                                        </tbody>
                                        <tbody>		
                                        <tr class="input-text">
                                            <td colspan="2"><button type="button" onClick="AddNewReDel(<?php echo $mx;?>);">Add</button><input type="hidden" name="txtREDELID_<?php echo $mx;?>" id="txtREDELID_<?php echo $mx;?>" value="<?php echo $k;?>"/></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ></td>
                                            <td ><input type="text" name="txtTotalReDelAmount_<?php echo $mx;?>" id="txtTotalReDelAmount_<?php echo $mx;?>" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                        </tr>
                                        </tbody>
                                     </table>
                                  
                                  </td>
                               </tr>
                               
                               <tr class="input-text">
                                <td colspan="12">&nbsp;</td>
                               </tr>
                                <tr class="input-text">
                                   <td></th>
                                   <td>Off Hire Reason</td>
                                   <td>Off Hire From</td>
                                   <td>Off Hire To</td>
                                   <td>Off Hire Days</td>
                                   <td>Off Hire Rate/Day(USD)</td>
                                   <td>Off Hire(USD)</td>
                                   <td></td>
                                   <td >Utilisation Days&nbsp;</td>
                                   <td><input autocomplete="off" type="text" name="txtUtilisationDays_<?php echo $mx;?>" id="txtUtilisationDays_<?php echo $mx;?>" class="input-text" placeholder="Utilisation Days" style="width:90px;background-color:#EAEAEA;" readonly value="<?php echo $rows['UTILISATION_DAY_EST'];?>"></td>
                                   <td >HFO/MGO Diff.(USD)&nbsp;</td>
                                   <td><input autocomplete="off" type="text" name="txtBunkerDifference_<?php echo $mx;?>" id="txtBunkerDifference_<?php echo $mx;?>" class="input-text" placeholder="0.00" style="width:90px;background-color:#EAEAEA;" readonly value="<?php echo $rows['BUNKER_DIFF_AMT'];?>"></td>
                                   <td ></td>
                                </tr>
                                <tbody id="offhirebody_<?php echo $mx;?>">
								 <?php $sql3 = "select * from chartering_estimate_tc_slave3 where TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."'";
                                 $result3 = mysql_query($sql3);
                                 $num3 = mysql_num_rows($result3);$k = 0;
                                 if($num3==0){$k++;?>
                                    <tr class="input-text" id="row_offhire_<?php echo $mx;?>_<?php echo $k;?>">
                                        <td><a href="#tb1" onClick="removeOffHire(<?php echo $mx;?>,<?php echo $k;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                        <td valign="top"><textarea class="input-text areasize" name="txtOffHireReason_<?php echo $mx;?>_<?php echo $k;?>" style="width:120px;" id="txtOffHireReason_<?php echo $mx;?>_<?php echo $k;?>" rows="3" placeholder="Off Hire Reason..."></textarea></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireFrom_<?php echo $mx;?>_<?php echo $k;?>" id="txtOffHireFrom_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" value="" style="width:100px;" placeholder="dd-mm-yyyy HH:MM" /></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireTo_<?php echo $mx;?>_<?php echo $k;?>" id="txtOffHireTo_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" value="" style="width:100px;" placeholder="dd-mm-yyyy HH:MM" /></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireDays_<?php echo $mx;?>_<?php echo $k;?>" id="txtOffHireDays_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" placeholder="Off Hire Days" style="width:90px;background-color:#EAEAEA;" readonly value="" ></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireRate_<?php echo $mx;?>_<?php echo $k;?>" id="txtOffHireRate_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" placeholder="Rate/Day(USD)" style="width:100px;" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" value="" ></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireAmt_<?php echo $mx;?>_<?php echo $k;?>" id="txtOffHireAmt_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" placeholder="Off Hire(USD)" style="width:90px;background-color:#EAEAEA;" readonly value="" ></td>
                                        <td></td><td></td><td></td><td></td><td></td>
                                    </tr>
                                <?php }else{
                                   while($rows3 = mysql_fetch_assoc($result3))
                                   {$k++;?>
                                     <tr class="input-text" id="row_offhire_<?php echo $mx;?>_<?php echo $k;?>">
                                        <td><a href="#tb1" onClick="removeOffHire(<?php echo $mx;?>,<?php echo $k;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                        <td valign="top"><textarea class="input-text areasize" name="txtOffHireReason_<?php echo $mx;?>_<?php echo $k;?>" style="width:120px;" id="txtOffHireReason_<?php echo $mx;?>_<?php echo $k;?>" rows="2" placeholder="Off Hire Reason..."><?php echo $rows3['OFF_REASON'];?></textarea></td>
                                        <?php if($rows3['OFF_FROM']=='0000-00-00 00:00:00' || $rows3['OFF_FROM']=='1970-01-01 08:00:00'){$offfrom = '';}else{$offfrom = date('d-m-Y H:i',strtotime($rows3['OFF_FROM']));}?>
                                        <?php if($rows3['OFF_TO']=='0000-00-00 00:00:00' || $rows3['OFF_TO']=='1970-01-01 08:00:00'){$offto = '';}else{$offto = date('d-m-Y H:i',strtotime($rows3['OFF_TO']));}?>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireFrom_<?php echo $mx;?>_<?php echo $k;?>" id="txtOffHireFrom_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" value="<?php echo $offfrom;?>" style="width:100px;" placeholder="dd-mm-yyyy HH:MM" /></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireTo_<?php echo $mx;?>_<?php echo $k;?>" id="txtOffHireTo_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" value="<?php echo $offto;?>" style="width:100px;" placeholder="dd-mm-yyyy HH:MM" /></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireDays_<?php echo $mx;?>_<?php echo $k;?>" id="txtOffHireDays_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" placeholder="Off Hire Days" style="width:90px;background-color:#EAEAEA;" readonly value="<?php echo $rows3['OFF_DAYS'];?>" ></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireRate_<?php echo $mx;?>_<?php echo $k;?>" id="txtOffHireRate_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" placeholder="Rate/Day(USD)" style="width:100px;" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" value="<?php echo $rows3['HIRE_RATE'];?>" ></td>
                                        <td valign="top"><input autocomplete="off" type="text" name="txtOffHireAmt_<?php echo $mx;?>_<?php echo $k;?>" id="txtOffHireAmt_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" placeholder="Off Hire(USD)" style="width:90px;background-color:#EAEAEA;" readonly value="<?php echo $rows3['OFF_HIRE'];?>" ></td>
                                        <td></td><td></td><td></td><td></td><td></td>
                                    </tr>
                                <?php }}?>
                                </tbody>
                                <tbody>		
                                <tr class="input-text">
                                    <td ><button type="button" onClick="AddNewOffHire(<?php echo $mx;?>);">Add</button><input type="hidden" name="txtOFFID_<?php echo $mx;?>" id="txtOFFID_<?php echo $mx;?>" value="<?php echo $k;?>"/></td>
                                    <td colspan="11"></td>
                                     
                                </tr>
                                </tbody>
                             </table>
	<!----------------------/////////------------LEFT SIDE PART------------/////////------------------------------>	
                           <table width="100%" style="margin-bottom:5px; padding:0px 5px 0px 5px;">
                           <tr class="input-text">
                               <td width="50%"><strong>Revenue Calculations - USD</strong></td>
                               <td width="50%"><strong>Expense Calculations - USD</strong></td>
                           </tr>
                           <tr>
                               <td width="50%" style="padding-top:4px; vertical-align:top;">
                                 <table width="100%">
                                    <tbody>
                                    <tr class="input-text">
                                        <td>Hire PDPR Currency</td>
                                        <td><select  name="selExchangeCurrency_<?php echo $mx;?>" id="selExchangeCurrency_<?php echo $mx;?>" onChange="getCurrencySpan(<?php echo $mx;?>);" class="input-text" style="width:100px;" ><?php $obj->getCurrencyList();?></select></td>
										<td>Exchange Rate To USD</td>
                                        <td><input type="text" name="txtExchangeRate_<?php echo $mx;?>" id="txtExchangeRate_<?php echo $mx;?>" class="input-text numeric" style="width:100px;" value="<?php echo $rows['EXCHANGE_RATE']; ?>" placeholder="Exchange Rate" autocomplete="off" onKeyUp="getFinalCalculation(<?php echo $mx;?>);"/></td>
                                    </tr><script>$("#selExchangeCurrency_<?php echo $mx;?>").val('<?php echo $rows['EXCHANGE_CURRENCY'];?>');</script>
                                    <tr class="input-text">
                                        <td>Hire Fixed Period PDPR <span id="currencyspan_<?php echo $mx;?>"></span></td>
										<td></td>
                                        <td><input autocomplete="off" type="text" name="txtDailyGrossHireUSD_<?php echo $mx;?>" id="txtDailyGrossHireUSD_<?php echo $mx;?>" class="input-text" style="width:100px;" value="<?php echo $rows['DAILY_GROSS_HIRE_EST'];?>" onKeyUp="getFinalCalculation(<?php echo $mx;?>);"/></td>
                                        <td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td>Hire Fixed Period PDPR(USD)</td>
										<td></td>
                                        <td><input autocomplete="off" type="text" name="txtDailyGrossHireUSD1_<?php echo $mx;?>" id="txtDailyGrossHireUSD1_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo number_format(($rows['DAILY_GROSS_HIRE_EST']*$rows['EXCHANGE_RATE']),2,'.','');?>" onKeyUp="getFinalCalculation(<?php echo $mx;?>);"/></td>
										<td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td >Add Comm(%)</td>
										<td><input autocomplete="off" type="text" name="txtAddCommPerct_<?php echo $mx;?>" id="txtAddCommPerct_<?php echo $mx;?>" class="input-text" style="width:100px;" value="<?php echo $rows['ADD_COMM_EST']; ?>" onKeyUp="getFinalCalculation(<?php echo $mx;?>);"/></td>
                                        <td><input autocomplete="off" type="text" name="txtAddComm1_<?php echo $mx;?>" id="txtAddComm1_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo $rows['ADD_COMM_CAL_EST']; ?>"/></td>
										<td><select name="selAddCommVendor_<?php echo $mx;?>" class="input-text" style="width:100px;" id="selAddCommVendor_<?php echo $mx;?>" ></select></td>
										<script>$("#selAddCommVendor_<?php echo $mx;?>").html($("#selVendor").html());$("#selAddCommVendor_<?php echo $mx;?>").val('<?php echo $rows['ADD_COMM_VENDOR'];?>');</script>
                                    </tr>
                                    <tr class="input-text">
                                        <td >Broker's Comm.(%)</td>
										<td><input autocomplete="off" type="text" name="txtChartererAcc_<?php echo $mx;?>" id="txtChartererAcc_<?php echo $mx;?>" class="input-text" style="width:100px;" value="<?php echo $rows['BROKER_COMM_EST'];?>" onKeyUp="getFinalCalculation(<?php echo $mx;?>);"/></td>
                                        <td><input autocomplete="off" type="text" name="txtBrokerComm_<?php echo $mx;?>" id="txtBrokerComm_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo $rows['BROKER_COMM_CAL_EST']; ?>"/></td>
										<td><select name="selBroCommVendor_<?php echo $mx;?>" class="input-text" style="width:100px;" id="selBroCommVendor_<?php echo $mx;?>" ></select></td>
										<script>$("#selBroCommVendor_<?php echo $mx;?>").html($("#selVendor").html());$("#selBroCommVendor_<?php echo $mx;?>").val('<?php echo $rows['BROKER_COMM_VENDOR'];?>');</script>
                                    </tr>
                                    <tr class="input-text">
                                        <td >Nett Hire(USD/day)</td>
										<td></td>
                                        <td><input autocomplete="off" type="text" name="txtNettHire_<?php echo $mx;?>" id="txtNettHire_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo $rows['NETT_HIRE_EST']; ?>" ></td>
                                        <td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td >Nett Rev(USD)</td>
										<td></td>
                                        <td><input autocomplete="off" type="text" name="txtNettRev_<?php echo $mx;?>" id="txtNettRev_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo $rows['NETT_REV_EST']; ?>" /></td>
                                        <td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td >Less Off hire</td>
										<td></td>
                                        <td><input autocomplete="off" type="text" name="txtLessOffHire_<?php echo $mx;?>" id="txtLessOffHire_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo $rows['LESS_OFF_HIRE_EST']; ?>" ></td>
                                        <td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td >CVE(USD/Month)</td>
                                        <td><input autocomplete="off" type="text" name="txtCVEM_<?php echo $mx;?>" id="txtCVEM_<?php echo $mx;?>" class="input-text" style="width:100px;" value="<?php echo $rows['CVE_MONTH'];?>" onKeyUp="getFinalCalculation(<?php echo $mx;?>);"/></td>
                                        <td><input autocomplete="off" type="text" name="txtCVE_<?php echo $mx;?>" id="txtCVE_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo $rows['CVE_EST']; ?>"/></td>
                                        <td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td ><strong>Nett Hire to invoice(USD)</strong></td>
										<td></td>
                                        <td><input autocomplete="off" type="text" name="txtNETHire_<?php echo $mx;?>" id="txtNETHire_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo $rows['NET_HIRE_AMT']; ?>"/></td>
                                        <td><select name="selESTtcVendor_<?php echo $mx;?>" class="input-text" style="width:100px;" id="selESTtcVendor_<?php echo $mx;?>" ></select></td>
                                        
                                    </tr>
                                    <script>$("#selESTtcVendor_<?php echo $mx;?>").html($("#selVendor").html());$("#selESTtcVendor_<?php echo $mx;?>").val('<?php echo $rows['TTL_REV_VENDOR'];?>');</script>
                                    <tr class="input-text">
                                        <td ><strong>Other Income</strong></td>
                                        <td></td>
                                        <td></td><td></td>
                                    </tr>
                                </tbody>		
                                <tbody id="otherIncomebody_<?php echo $mx;?>">
                                 <?php $sql3 = "select * from chartering_estimate_tc_slave2 where STATUS=1 and TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."'";
                                 $result3 = mysql_query($sql3);
                                 $num3 = mysql_num_rows($result3);$k = 0;
                                 if($num3==0){$k++;?>
                                    <tr class="input-text" id="row_otherincome_<?php echo $mx;?>_1">
                                        <td><a href="#tb1" onClick="removeOtherIncome(<?php echo $mx;?>,1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                        <td ><input autocomplete="off" type="text" name="txtOtherIncomeText_<?php echo $mx;?>_1" id="txtOtherIncomeText_<?php echo $mx;?>_1" class="input-text" style="width:100px;" placeholder="Description" value="" /></td>
                                        <td><input autocomplete="off" type="text" name="txtOtherIncome_<?php echo $mx;?>_1" id="txtOtherIncome_<?php echo $mx;?>_1" class="input-text" style="width:100px;" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" placeholder="Other Income" value="" /></td>
                                        <td><select name="selOthIncVendor_<?php echo $mx;?>_1" class="input-text" style="width:100px;" id="selOthIncVendor_<?php echo $mx;?>_1" ></select></td>
                                    </tr><script>$("#selOthIncVendor_<?php echo $mx;?>_1").html($("#selVendor").html());</script>
                                <?php }else{
                                   while($rows3 = mysql_fetch_assoc($result3))
                                   {$k++;?>
                                   <tr class="input-text" id="row_otherincome_<?php echo $mx;?>_<?php echo $k;?>">
                                        <td><a href="#tb1" onClick="removeOtherIncome(<?php echo $mx;?>,<?php echo $k;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                        <td ><input autocomplete="off" type="text" name="txtOtherIncomeText_<?php echo $mx;?>_<?php echo $k;?>" id="txtOtherIncomeText_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style="width:100px;" placeholder="Description" value="<?php echo $rows3['DESCRIPTION']; ?>" /></td>
                                        <td><input autocomplete="off" type="text" name="txtOtherIncome_<?php echo $mx;?>_<?php echo $k;?>" id="txtOtherIncome_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style="width:100px;" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" placeholder="Other Income" value="<?php echo $rows3['OTHER_AMT']; ?>" /></td>
                                    
                                    <td><select name="selOthIncVendor_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style="width:100px;" id="selOthIncVendor_<?php echo $mx;?>_<?php echo $k;?>" ></select></td>                
                                    </tr>
									<script>$("#selOthIncVendor_<?php echo $mx;?>_<?php echo $k;?>").html($("#selVendor").html());$("#selOthIncVendor_<?php echo $mx;?>_<?php echo $k;?>").val('<?php echo $rows3['OTHER_EXP_VENDOR']; ?>');</script>
                                <?php }}?>
                                </tbody>
                                <tbody>		
                                    <tr class="input-text">
                                        <td ><button type="button" onClick="addOtherIncomeRow(<?php echo $mx;?>);">Add</button><input type="hidden" name="hddnOtherID_<?php echo $mx;?>" id="hddnOtherID_<?php echo $mx;?>" value="<?php echo $k;?>"/></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td><strong>Total Rev</strong></td>
										<td></td>
                                        <td><input autocomplete="off" type="text" name="txtTotalRev_<?php echo $mx;?>" id="txtTotalRev_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="<?php echo $rows['TOTAL_REV_EST']; ?>" /></td>
										<td></td>
										
                                    </tr>
                                </tbody>
                                 </table>
                               </td>
                               
        <!---------------------------------------------RIGHT SIDE PART----------------------------------------------------->					   
                               <td width="50%" style="padding-top:4px; vertical-align:top;">
                                    <table width="100%">
                                    
                                    <tr class="input-text">
                                        <td colspan="6"><strong>Other Expense</strong></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td></td>
                                        <td>Expense Type</td>
                                        <td>Expense Descp.</td>
                                        <td>Add to TTL</td>
                                        <td>Expense Amt</td>
										<td>Vendor</td>
                                    </tr>
                                   
                                    <tbody id="otherExpensebody_<?php echo $mx;?>">
                                     <?php $sql3 = "select * from chartering_estimate_tc_slave2 where STATUS=2 and TC_SLAVE1ID='".$rows['TC_SLAVE1ID']."'";
                                     $result3 = mysql_query($sql3);
                                     $num3 = mysql_num_rows($result3);$k = 0;
                                     if($num3==0){$k++;?>
                                        <tr class="input-text" id="row_otherExpense_<?php echo $mx;?>_1">
                                            <td><a href="#tb1" onClick="removeOtherExpense(<?php echo $mx;?>,1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                            <td ><select name="selExpenseType_<?php echo $mx;?>_1" id="selExpenseType_<?php echo $mx;?>_1" class="input-text" style="width:100px;"></select></td>
                                            <td><input autocomplete="off" type="text" name="txtExpenseDesc_<?php echo $mx;?>_1" id="txtExpenseDesc_<?php echo $mx;?>_1" class="input-text" style="width:100px;" placeholder="Expense Descp." value="" /></td>
                                            <td><input name="ChkAddToTTL_<?php echo $mx;?>_1" class="checkbox" id="ChkAddToTTL_<?php echo $mx;?>_1" type="checkbox" value="1"/><input type="hidden" name="chkVal_<?php echo $mx;?>_1" id="chkVal_<?php echo $mx;?>_1" value="" /></td>
                                            <td><input autocomplete="off" type="text" name="txtOtherExpense_<?php echo $mx;?>_1" id="txtOtherExpense_<?php echo $mx;?>_1" class="input-text" style="width:100px;" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" placeholder="Other Income(USD)" value="" /></td>
											<td><select name="selOthExpVendor_<?php echo $mx;?>_1" class="input-text" style="width:100px;" id="selOthExpVendor_<?php echo $mx;?>_1" ></select></td>
                                        </tr>
										<script>$("#selOthExpVendor_<?php echo $mx;?>_1").html($("#selVendor").html());</script>
                                        <script>$("#selExpenseType_<?php echo $mx;?>_1").html($("#selExpenseType").html());</script>
                                    <?php }else{
                                       while($rows3 = mysql_fetch_assoc($result3))
                                       {$k++;?>
                                       <tr class="input-text" id="row_otherExpense_<?php echo $mx;?>_<?php echo $k;?>">
                                            <td><a href="#tb1" onClick="removeOtherExpense(<?php echo $mx;?>,<?php echo $k;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                            <td ><select name="selExpenseType_<?php echo $mx;?>_<?php echo $k;?>" id="selExpenseType_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style="width:100px;"></select></td>
                                            <td><input autocomplete="off" type="text" name="txtExpenseDesc_<?php echo $mx;?>_<?php echo $k;?>" id="txtExpenseDesc_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style="width:100px;" placeholder="Expense Descp." value="<?php echo $rows3['DESCRIPTION']; ?>" /></td>
                                            <td><input name="ChkAddToTTL_<?php echo $mx;?>_<?php echo $k;?>" class="checkbox" id="ChkAddToTTL_<?php echo $mx;?>_<?php echo $k;?>" type="checkbox" value="1" <?php if($rows3['CHK_ADDTTL']==1){echo "checked";}?>/><input type="hidden" name="chkVal_<?php echo $mx;?>_<?php echo $k;?>" id="chkVal_<?php echo $mx;?>_<?php echo $k;?>" value="<?php if($rows3['CHK_ADDTTL']==1){echo "1";}?>" /></td>
                                            <td><input autocomplete="off" type="text" name="txtOtherExpense_<?php echo $mx;?>_<?php echo $k;?>" id="txtOtherExpense_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style="width:100px;" onKeyUp="getFinalCalculation(<?php echo $mx;?>);" placeholder="Other Income(USD)" value="<?php echo $rows3['OTHER_AMT']; ?>" /></td>
											<td><select name="selOthExpVendor_<?php echo $mx;?>_<?php echo $k;?>" class="input-text" style="width:100px;" id="selOthExpVendor_<?php echo $mx;?>_<?php echo $k;?>" ></select></td>
                                        </tr>
										<script>$("#selOthExpVendor_<?php echo $mx;?>_<?php echo $k;?>").html($("#selVendor").html());$("#selOthExpVendor_<?php echo $mx;?>_<?php echo $k;?>").val('<?php echo $rows3['OTHER_EXP_VENDOR']; ?>');</script>
                                        <script>$("#selExpenseType_<?php echo $mx;?>_<?php echo $k;?>").html($("#selExpenseType").html());$("#selExpenseType_<?php echo $mx;?>_<?php echo $k;?>").val(<?php echo $rows3['EXPENSETYPEID']; ?>);</script>
                                    <?php }}?>
                                    </tbody>
                                    <tr class="input-text">
                                        <td colspan="5"><button type="button" onClick="addOtherExpenseRow(<?php echo $mx;?>);">Add</button><input type="hidden" name="hddnOtherExID_<?php echo $mx;?>" id="hddnOtherExID_<?php echo $mx;?>" value="<?php echo $k;?>"/></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr class="input-text">
                                        <td colspan="5"><strong>TC In Expenses</strong></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td>TC In (USD/Day)</td>
                                        <td><input autocomplete="off" type="text" name="txtTCPerDay_<?php echo $mx;?>" id="txtTCPerDay_<?php echo $mx;?>" class="input-text" style="width:100px;" placeholder="TC In (USD/Day)" value="<?php echo $rows['TC_RATE']; ?>" onKeyUp="getFinalCalculation(<?php echo $mx;?>);"/></td>
                                        <td>Total Hireage(USD) </td>
                                        <td><input autocomplete="off" type="text" name="txtTotalHireage_<?php echo $mx;?>" id="txtTotalHireage_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="Total Hireage" readonly value="<?php echo $rows['TOTAL_HIREAGE']; ?>"/></td>
                                        <td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td>Add. Comm(%)</td>
                                        <td><input autocomplete="off" type="text" name="txtAddComm_<?php echo $mx;?>" id="txtAddComm_<?php echo $mx;?>" class="input-text" style="width:100px;" placeholder="Add. Comm(%)" value="<?php echo $rows['ADD_COMM']; ?>" onKeyUp="getFinalCalculation(<?php echo $mx;?>);"/></td>
                                        <td>Add. Comm(USD)</td>
                                        <td><input autocomplete="off" type="text" name="txtAddCommAmt_<?php echo $mx;?>" id="txtAddCommAmt_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="Add. Comm(USD)" readonly value="<?php echo $rows['ADD_COMM_AMT']; ?>"/></td>
                                        <td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td></td>
                                        <td></td>
                                        <td>Nett Hireage(USD)</td>
                                        <td><input autocomplete="off" type="text" name="txtNetHireage_<?php echo $mx;?>" id="txtNetHireage_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="Nett Hireage(USD)" readonly value="<?php echo $rows['NET_HIREAGE']; ?>"/></td>
                                        <td><select name="selTcInExpVendor_<?php echo $mx;?>" class="input-text" style="width:100px;" id="selTcInExpVendor_<?php echo $mx;?>" ></select></td>
											<script>$("#selTcInExpVendor_<?php echo $mx;?>").html($("#selVendor").html());$("#selTcInExpVendor_<?php echo $mx;?>").val('<?php echo $rows['NET_HIR_VENDOR'];?>');</script>
                                    </tr>
                                    <tr class="input-text">
                                        <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr class="input-text">
                                        <td colspan="2"><strong>Total Expenses</strong></td>
                                        <td></td>
                                        <td><input autocomplete="off" type="text" name="txtTotalExpenses_<?php echo $mx;?>" id="txtTotalExpenses_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="Total Expenses" readonly value="<?php echo $rows['TOTAL_EXP_EST']; ?>"/></td>
                                        <td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td colspan="2"><strong>TC Earnings</strong></td>
                                        <td></td>
                                        <td><input autocomplete="off" type="text" name="txtVoyageEarnings_<?php echo $mx;?>" id="txtVoyageEarnings_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="TC Earnings" value="<?php echo $rows['VOYAGE_EARN_EST']; ?>"/></td>
                                        <td></td>
                                    </tr>
                                    <tr class="input-text">
                                        <td colspan="2"><strong>Profit / Day&nbsp;</strong></td>
                                        <td></td>
                                        <td><input autocomplete="off" type="text" name="txtProfitPerDay_<?php echo $mx;?>" id="txtProfitPerDay_<?php echo $mx;?>" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="Voy Earnings/Utilisation Days" value="<?php echo $rows['PROFIT_PER_DAY_EST']; ?>"/></td>
                                        <td></td>
                                    </tr>
                                </table>
                               </td>
                           </tr>
                           </table>
                           </div>
                  <?php }?>
                        </div>
                        <hr style="margin-bottom:7px; margin-top:8px; color:#000;">
                        <div style="text-align:center;">
                        <button type="button" class="btn btn-warning btn-flat" style="float:left; margin-left:10px;" onClick="AddNewSheet();">Add New Trip/Period</button><button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(1);">Submit to Close</button>
                        <input type="hidden" name="action" id="action" value="submit" />
                        <input type="hidden" name="update_status" id="update_status" value="" />
                        <input type="hidden" name="txtTripID" id="txtTripID" value="<?php echo $mx;?>" />
                        <input type="hidden" name="vesselrec" id="vesselrec" class="input-text" value="" /></div>
                        <!-- some hidden fields start -->
                        <input type="hidden" name="selDurFixPeriod" id="selDurFixPeriod"/>
                        <input type="hidden" name="txtCVEmonth" id="txtCVEmonth" value="<?php echo $obj->getFun63();?>"/>
                        <input type="hidden" name="txtBrokCommPayableBy" id="txtBrokCommPayableBy"/>
                         </div>
                    
				<!-- some hidden fields ends -->
                </form>
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/sort.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />
<script type="text/javascript">
$(document).ready(function(){
$(".areasize").autosize({append: "\n"});
$("#selVName").val(<?php echo $obj->getFun3();?>);
$("#selDurFixPeriod").val(<?php echo $obj->getFun45();?>);
$("#txtBrokCommPayableBy").val('<?php echo $obj->getFun67();?>');
		
	$('#tripspan_1,#tripspan_2,#tripspan_3,#periodspan_1,#periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').hide();
		
	if($("#selDurFixPeriod").val() == 1)
	{
		$('#tripspan_1,#tripspan_2,#tripspan_3').show();
		$('#periodspan_1,periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').hide();
	}
	else if($("#selDurFixPeriod").val() == 2)
	{
		$('#periodspan_1,#periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').show();
		$('#tripspan_1,#tripspan_2,#tripspan_3').hide();
	}
	else
	{
		$('#tripspan_1,#tripspan_2,#tripspan_3,#periodspan_1,#periodspan_2,#periodspan_3,#noOfTripspan_1,#noOfTripspan_2,#noOfTripspan_3').hide();
	}
	
	$("#frm1").validate({
		rules: {
			selVName:"required",
			txtTCNo:"required"
			},
		messages: {
			selVName:"*",
			},
	submitHandler: function(form)  {
		jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
				if(r){
					jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
					$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
					$("#popup_content").css({"background":"none","text-align":"center"});
					$("#popup_ok,#popup_title").hide();  
					frm1.submit();
					}
					else{return false;}
				});
		}
	});
initialiseControl();
<?php $mx = 0;
$sql2 = "select * from chartering_tc_estimate_slave1 where TCOUTID = '".$obj->getFun1()."'";
$res2 = mysql_query($sql2) or die($sql2);
$num  = mysql_num_rows($res2);
while($rows = mysql_fetch_assoc($res2))
{$mx++;?>
getCurrencySpan(<?php echo $mx;?>);
getFinalCalculation(<?php echo $mx;?>);
<?php }?>
});

function getValue()
{
	
}


function getString(var1)
{
   var var2 = var1.split('-');  
   return var2[2]+'/'+var2[1]+'/'+var2[0];
}


function initialiseControl()
{
	$(".areasize").autosize({append: "\n"});
	$("[id^=txtVoyageEarnings_], [id^=txtDeliveryHFO_], [id^=txtDeliveryMGO_], [id^=txtPriceDelHFO_], [id^=txtPriceDelMGO_], [id^=txtSupercargoMeals_], [id^=txtHoldCleanInterm_], [id^=txtILOHCtcFix_], [id^=txtAddComm_], [id^=txtBrokerCommTCfix_], [id^=txtTCdays_], [id^=txtOffHireDays_], [id^=txtUtilisationDays_], [id^=txtOffHireRate_], [id^=txtNoOfTrips_], [id^=txtHireFixPerUSD_], [id^=txtOtherIncome_], [id^=txtDelHFOmt_], [id^=txtReDelHFOmt_], [id^=txtDelHFOusdmt_], [id^=txtReDelHFOusdmt_], [id^=txtDelMGOmt_], [id^=txtReDelMGOmt_], [id^=txtDelMGOusdmt_], [id^=txtReDelMGOusdmt_], [id^=txtOtherIncome_], [id^=txtOtherExpense_], [id^=txtOffHireRate_], [id^=txtTCPerDay_], [id^=txtAddComm_], [id^=txtDailyGrossHireUSD_], [id^=txtAddCommPerct_], [id^=txtChartererAcc_], [id^=txtCVEM_]").numeric();
	
	$("input[type='checkbox']").iCheck({
		checkboxClass: 'icheckbox_minimal',
		radioClass: 'iradio_minimal'
	});
	
	$('[id^=ChkAddToTTL_]').on('ifChecked', function () { 
	   var idd = $(this).attr('id');
	   var iddd = idd.split("_");
	   $("#chkVal_"+iddd[1]+"_"+iddd[2]).val(1);
	   getFinalCalculation(iddd[1]);
	});
	 
	$('[id^=ChkAddToTTL_]').on('ifUnchecked', function () { 
	   var idd = $(this).attr('id');
	   var iddd = idd.split("_");
	   $("#chkVal_"+iddd[1]+"_"+iddd[2]).val(0);
	   getFinalCalculation(iddd[1]);
	});
    
	$("[id^=txtDelBunDate_],[id^=txtReDelBunDate_]").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
	
	$('[id^=txtOffHireFrom_],[id^=txtOffHireTo_],[id^=txtDeldate_],[id^=txtReDeldate_]').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
		}).on('changeDate', function(){
			var idd = $(this).attr('id');
	        var iddd = idd.split("_");
		    getFinalCalculation(iddd[1]);
	});
}


function getValidate(val1)
{
	$('#update_status').val(val1);
	return true;
}

function getTimeDiff(date2,date1)
{	
	if(date1 != '' &&  date2 != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat(date2);
		start_actual_time    =  getDateWithSpecificFormat(date1);
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000;
		return days.toFixed(5);	
	}
}

function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' '+dateSplit1[1];
	return currentDate;
}


function getFinalCalculation(rval)
{
	var txtDailyGrossHireUSD      = parseFloat($("#txtDailyGrossHireUSD_"+rval).val());if(isNaN(txtDailyGrossHireUSD)){txtDailyGrossHireUSD = 0.00;}
	var txtExchangeRate           = parseFloat($("#txtExchangeRate_"+rval).val());if(isNaN(txtExchangeRate)){txtExchangeRate = 0.00;}
	$("#txtDailyGrossHireUSD1_"+rval).val(parseFloat(parseFloat(txtDailyGrossHireUSD)*parseFloat(txtExchangeRate)).toFixed(2));
	
	var tcdays = parseFloat(getTimeDiff($("#txtReDeldate_"+rval).val(),$("#txtDeldate_"+rval).val()));if(isNaN(tcdays)){tcdays = 0.00;}
	$("#txtTCdays_"+rval).val(parseFloat(tcdays).toFixed(4));
    
	var txtDELID = $("#txtDELID_"+rval).val();
	for(var i =1;i <=txtDELID;i++)
	{
		var txtDelQty      = parseFloat($("#txtDelQty_"+rval+"_"+i).val());if(isNaN(txtDelQty)){txtDelQty = 0.00;}
		var txtDelPrice    = parseFloat($("#txtDelPrice_"+rval+"_"+i).val());if(isNaN(txtDelPrice)){txtDelPrice = 0.00;}
		var amount          =   parseFloat(parseFloat(txtDelQty)*parseFloat(txtDelPrice));if(isNaN(amount)){amount = 0.00;}
		$('#txtDelAmount_'+rval+'_'+i).val(parseFloat(amount).toFixed(2));
	}
	$("#txtTotalDelAmount_"+rval).val($('[id^=txtDelAmount_'+rval+'_]').sum());
	
    var txtREDELID = $("#txtREDELID_"+rval).val();
	for(var i =1;i <=txtREDELID;i++)
	{
		var txtReDelQty      = parseFloat($("#txtReDelQty_"+rval+"_"+i).val());if(isNaN(txtDelQty)){txtDelQty = 0.00;}
		var txtReDelPrice    = parseFloat($("#txtReDelPrice_"+rval+"_"+i).val());if(isNaN(txtDelPrice)){txtDelPrice = 0.00;}
		var amount          =   parseFloat(parseFloat(txtReDelQty)*parseFloat(txtReDelPrice));if(isNaN(amount)){amount = 0.00;}
		$('#txtReDelAmount_'+rval+'_'+i).val(parseFloat(amount).toFixed(2));
	}
	$("#txtTotalReDelAmount_"+rval).val($('[id^=txtReDelAmount_'+rval+'_]').sum());
	
	var delbunker          = $('#txtTotalDelAmount_'+rval).sum();
	var redelbunker        = $('#txtTotalReDelAmount_'+rval).sum();
	
	$("#txtBunkerDifference_"+rval).val(parseFloat(parseFloat(delbunker) - parseFloat(redelbunker)).toFixed(2));
	
	var dailygrosshire = parseFloat($("#txtDailyGrossHireUSD1_"+rval).val());if(isNaN(dailygrosshire)){dailygrosshire = 0.00;}
	var addcomm        = parseFloat($("#txtAddCommPerct_"+rval).val());if(isNaN(addcomm)){addcomm = 0.00;}
	var addcommamt     = parseFloat(parseFloat(dailygrosshire)*parseFloat(addcomm))/100;if(isNaN(addcommamt)){addcommamt = 0.00;}
	$("#txtAddComm1_"+rval).val(parseFloat(addcommamt).toFixed(2));
	
	var brokercomm        = parseFloat($("#txtChartererAcc_"+rval).val());if(isNaN(brokercomm)){brokercomm = 0.00;}
	var brokercommamt     = parseFloat(parseFloat(dailygrosshire)*parseFloat(brokercomm))/100;if(isNaN(brokercommamt)){brokercommamt = 0.00;}
	$("#txtBrokerComm_"+rval).val(parseFloat(brokercommamt).toFixed(2));
	$("#txtNettHire_"+rval).val(parseFloat((parseFloat(dailygrosshire)) - parseFloat(addcommamt) - parseFloat(brokercommamt)).toFixed(2));
	
	var numoff = $("#txtOFFID_"+rval).val();
	var offhiredays = 0;
	for(var i =1;i <=numoff;i++)
	{
		if(typeof($("#txtOffHireFrom_"+rval+"_"+i).val())!='undefined')
		{
			var timediff = parseFloat(getTimeDiff($("#txtOffHireTo_"+rval+"_"+i).val(),$("#txtOffHireFrom_"+rval+"_"+i).val()));
			if(isNaN(timediff)){timediff = 0.00;}
			$("#txtOffHireDays_"+rval+"_"+i).val(parseFloat(timediff).toFixed(4))
			var offrate  = parseFloat($("#txtOffHireRate_"+rval+"_"+i).val());
			if(isNaN(offrate)){offrate = 0.00;}
			offhiredays =   parseFloat(parseFloat(offhiredays) + parseFloat(timediff));
			$("#txtOffHireAmt_"+rval+"_"+i).val(parseFloat(parseFloat(timediff)*parseFloat(offrate)).toFixed(2));
		}
	}	
	
	if(isNaN(offhiredays)){offhiredays = 0.00;}
	$("#txtLessOffHire_"+rval).val($('[id^=txtOffHireAmt_'+rval+'_]').sum());
	
	var cal1 = parseFloat(tcdays) - parseFloat(offhiredays);
	if(isNaN(cal1)){cal1 = 0.00;}
	$("#txtUtilisationDays_"+rval).val(parseFloat(cal1).toFixed(4));
	
	var txtTCdays        = parseFloat($("#txtTCdays_"+rval).val());if(isNaN(txtTCdays)){txtTCdays = 0.00;}
	var nettHire		 = parseFloat($("#txtNettHire_"+rval).val());if(isNaN(nettHire)){nettHire = 0.00;}
	var cal1			 = parseFloat(txtTCdays) * parseFloat(nettHire);
	if(isNaN(cal1)){cal1 = 0.00;}
	$("#txtNettRev_"+rval).val(parseFloat(cal1).toFixed(2));
	
	var utilisationDays1    = parseFloat($("#txtUtilisationDays_"+rval).val());if(isNaN(utilisationDays1)){utilisationDays1 = 0.00;}
	
	if($("#txtCVEM_"+rval).val() == ""){var txtCVE = 0;}else{var txtCVE = $("#txtCVEM_"+rval).val();}
	var calmul = parseFloat(parseFloat(parseFloat(txtCVE)/30)*parseFloat(utilisationDays1));if(isNaN(calmul)){calmul = 0.00;}
	$("#txtCVE_"+rval).val(parseFloat(calmul).toFixed(2));
	
	//-----------------otherIncome-----------
	var nettRev  	             = parseFloat($("#txtNettRev_"+rval).val());if(isNaN(nettRev)){nettRev = 0.00;}
	var txtBunkerDifference  	 = parseFloat($("#txtBunkerDifference_"+rval).val());if(isNaN(txtBunkerDifference)){txtBunkerDifference = 0.00;}
	var lessOffHire  			 = parseFloat($("#txtLessOffHire_"+rval).val());if(isNaN(lessOffHire)){lessOffHire = 0.00;}
	var cVE  		             = parseFloat($("#txtCVE_"+rval).val());if(isNaN(cVE)){cVE = 0.00;}
	var otherIncome              = parseFloat($('[id^=txtOtherIncome_'+rval+'_]').sum());if(isNaN(otherIncome)){otherIncome = 0.00;}
	var cal1		             = parseFloat(nettRev) - parseFloat(lessOffHire) + parseFloat(cVE) + parseFloat(txtBunkerDifference);
	if(isNaN(cal1)){cal1 = 0.00;}
	$("#txtNETHire_"+rval).val(parseFloat(cal1).toFixed(2));
	var cal2		             = parseFloat(cal1) + parseFloat(otherIncome);
	$("#txtTotalRev_"+rval).val(parseFloat(cal2).toFixed(2));
	//--------------------------------------------
	
	//---------------otherExpense------------------
	var hddnOtherExID = $("#hddnOtherExID_"+rval).val();
	var otherexpense = 0;
	for(var i =1;i <=hddnOtherExID;i++)
	{
		if(typeof($("#selExpenseType_"+rval+"_"+i).val())!='undefined')
		{   
		    if($("#chkVal_"+rval+"_"+i).val()==1)
	        {
				var otherexpense1 = parseFloat($("#txtOtherExpense_"+rval+"_"+i).val());
				if(isNaN(otherexpense1)){otherexpense1 = 0.00;}
				otherexpense =   parseFloat(parseFloat(otherexpense) + parseFloat(otherexpense1));
			}
		}
	}
	
	var txtTCPerDay  	 		= parseFloat($("#txtTCPerDay_"+rval).val());if(isNaN(txtTCPerDay)){txtTCPerDay = 0.00;}
	var totalhireage            = parseFloat(parseFloat(utilisationDays1)*parseFloat(txtTCPerDay));if(isNaN(totalhireage)){totalhireage = 0.00;}
	$("#txtTotalHireage_"+rval).val(parseFloat(totalhireage).toFixed(2));
	var txtAddComm  	 		= parseFloat($("#txtAddComm_"+rval).val());if(isNaN(txtAddComm)){txtAddComm = 0.00;}
	var txtAddCommAmt           = parseFloat(parseFloat(parseFloat(totalhireage)*parseFloat(txtAddComm))/100);if(isNaN(txtAddCommAmt)){txtAddCommAmt = 0.00;}
	$("#txtAddCommAmt_"+rval).val(parseFloat(txtAddCommAmt).toFixed(2));
	$("#txtNetHireage_"+rval).val(parseFloat(parseFloat(totalhireage) - parseFloat(txtAddCommAmt)).toFixed(2));
	
	var cal2		 		= parseFloat(otherexpense) + parseFloat($("#txtNetHireage_"+rval).val());
	if(isNaN(cal2)){cal2 = 0.00;}
	
	$("#txtTotalExpenses_"+rval).val(parseFloat(cal2).toFixed(2));
	
	var totalRev1  	 	= parseFloat($("#txtTotalRev_"+rval).val());if(isNaN(totalRev1)){totalRev1 = 0.00;}
	var totalExpenses  	= parseFloat($("#txtTotalExpenses_"+rval).val());if(isNaN(totalExpenses)){totalExpenses = 0.00;}
	var cal2		 	= parseFloat(totalRev1) - parseFloat(totalExpenses);
	if(isNaN(cal2)){cal2 = 0.00;}
	$("#txtVoyageEarnings_"+rval).val(parseFloat(cal2).toFixed(2));
	
	
	var voyageEarnings  	= parseFloat($("#txtVoyageEarnings_"+rval).val());if(isNaN(voyageEarnings)){voyageEarnings = 0.00;}
	var utilisationDays  	= parseFloat($("#txtUtilisationDays_"+rval).val());if(isNaN(utilisationDays)){utilisationDays = 0.00;}
	var cal2		 		= parseFloat(voyageEarnings) / parseFloat(utilisationDays);
	if(isNaN(cal2) || cal2 == "-Infinity" || cal2 == "Infinity"){cal2 = 0.00;}
	$("#txtProfitPerDay_"+rval).val(parseFloat(cal2).toFixed(2));
	//------------------------------------------------------------
	var txtTripID = $("#txtTripID").val();
	var totaldays = totalvoyageearning = 0;
	for(var i =1;i <=txtTripID;i++)
	{
		if($("#tripdiv_"+i).is(':visible'))
		{   
			var days           = parseFloat($("#txtUtilisationDays_"+i).val());if(isNaN(voyageEarnings)){voyageEarnings = 0.00;}
			var earning        = parseFloat($("#txtVoyageEarnings_"+i).val());if(isNaN(earning)){earning = 0.00;}
			totaldays          =   parseFloat(parseFloat(totaldays) + parseFloat(days));
			totalvoyageearning =   parseFloat(parseFloat(totalvoyageearning) + parseFloat(earning));
		}
	}
	$("#txtTotalUdays").val(parseFloat(totaldays).toFixed(4));
	$("#txtTotalVoyageEarning").val(parseFloat(totalvoyageearning).toFixed(2));
}

function addOtherIncomeRow(rval)
{
    var idd = $("#hddnOtherID_"+rval).val();
	if($("#txtOtherIncomeText_"+rval+"_"+idd).val()!='' && $("#txtOtherIncome_"+rval+"_"+idd).val()!='')
	{
	    idd = parseInt(idd) + 1;
	    $('<tr class="input-text" id="row_otherincome_'+rval+'_'+idd+'"><td><a href="#tb1" onClick="removeOtherIncome('+rval+','+idd+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><input autocomplete="off" type="text" name="txtOtherIncomeText_'+rval+'_'+idd+'" id="txtOtherIncomeText_'+rval+'_'+idd+'" class="input-text" style="width:100px;" placeholder="Description" value="" /></td><td><input autocomplete="off" type="text" name="txtOtherIncome_'+rval+'_'+idd+'" id="txtOtherIncome_'+rval+'_'+idd+'" class="input-text" style="width:100px;" onKeyUp="getFinalCalculation('+rval+');" placeholder="Other Income" value="" /></td><td><select name="selOthIncVendor_'+rval+'_'+idd+'" class="input-text" style="width:100px;" id="selOthIncVendor_'+rval+'_'+idd+'" ></select></td></tr>').appendTo("#otherIncomebody_"+rval);
		$("#hddnOtherID_"+rval).val(idd);
		$("#selOthIncVendor_"+rval+"_"+idd).html($("#selVendor").html());
		$("[id^=txtOtherIncome_]").numeric();
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeOtherIncome(rval,var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#row_otherincome_"+rval+"_"+var1).remove();
					getFinalCalculation(rval);			
				 }
			else{return false;}
			});
}


function addOtherExpenseRow(rval)
{
    var idd = $("#hddnOtherExID_"+rval).val();
	if($("#selExpenseType_"+rval+"_"+idd).val()!='' && $("#txtOtherExpense_"+rval+"_"+idd).val()!='')
	{
	    idd = parseInt(idd) + 1;
	    $('<tr class="input-text" id="row_otherExpense_'+rval+'_'+idd+'"><td><a href="#tb1" onClick="removeOtherExpense(_'+rval+','+idd+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td ><select name="selExpenseType_'+rval+'_'+idd+'" id="selExpenseType_'+rval+'_'+idd+'" class="input-text" style="width:100px;"></select></td><td><input autocomplete="off" type="text" name="txtExpenseDesc_'+rval+'_'+idd+'" id="txtExpenseDesc_'+rval+'_'+idd+'" class="input-text" style="width:100px;" placeholder="Expense Descp." value="" /></td><td><input name="ChkAddToTTL_'+rval+'_'+idd+'" class="checkbox" id="ChkAddToTTL_'+rval+'_'+idd+'" type="checkbox" value="1"/><input type="hidden" name="chkVal_'+rval+'_'+idd+'" id="chkVal_'+rval+'_'+idd+'" value="" /></td><td><input autocomplete="off" type="text" name="txtOtherExpense_'+rval+'_'+idd+'" id="txtOtherExpense_'+rval+'_'+idd+'" class="input-text" style="width:100px;" onKeyUp="getFinalCalculation('+rval+');" placeholder="Other Expense(USD)" value="" /></td><td ><select name="selOthExpVendor_'+rval+'_'+idd+'" id="selOthExpVendor_'+rval+'_'+idd+'" class="input-text" style="width:100px;"></select></td></tr>').appendTo("#otherExpensebody_"+rval);
		$("#hddnOtherExID_"+rval).val(idd);
		
		$("#selExpenseType_"+rval+"_"+idd).html($("#selExpenseType").html());
		$("#selOthExpVendor_"+rval+"_"+idd).html($("#selVendor").html());
		$("[id^=txtOtherExpense_"+rval+"_]").numeric();
		
		initialiseControl();
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeOtherExpense(rval, var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#row_otherExpense_"+rval+"_"+var1).remove();
					getFinalCalculation(rval);			
				 }
			else{return false;}
			});
}


function AddNewOffHire(rval)
{
	var id = $("#txtOFFID_"+rval).val();
	if($("#txtOffHireReason_"+rval+"_"+id).val() != "" && $("#txtOffHireFrom_"+rval+"_"+id).val() != "" && $("#txtOffHireTo_"+rval+"_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="row_offhire_'+rval+'_'+id+'" class="input-text"><td><a href="#pr'+id+'" onclick="removeOffHire('+rval+','+id+');"><i class="fa fa-times" style="color:red;"></i></a></td><td valign="top"><textarea class="input-text areasize" name="txtOffHireReason_'+rval+'_'+id+'" style="width:100px;" id="txtOffHireReason_'+rval+'_'+id+'" rows="3" placeholder="Off Hire Reason..."></textarea></td><td valign="top"><input type="text" name="txtOffHireFrom_'+rval+'_'+id+'" id="txtOffHireFrom_'+rval+'_'+id+'" class="input-text" placeholder="dd-mm-yyyy HH:MM" style="width:100px;" autocomplete="off" value=""></td><td valign="top"><input type="text" name="txtOffHireTo_'+rval+'_'+id+'" id="txtOffHireTo_'+rval+'_'+id+'" class="input-text" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="" style="width:100px;"></td><td valign="top"><input type="text" name="txtOffHireDays_'+rval+'_'+id+'" id="txtOffHireDays_'+rval+'_'+id+'" class="input-text"  placeholder="Off Hire Days" style="width:90px;background-color:#EAEAEA;" autocomplete="off" value=""/></td><td valign="top"><input type="text" name="txtOffHireRate_'+rval+'_'+id+'" id="txtOffHireRate_'+rval+'_'+id+'" class="input-text" style="width:100px;"  placeholder="Rate/Day" autocomplete="off" value="" onKeyUp="getFinalCalculation('+rval+');"/></td><td valign="top"><input type="text" name="txtOffHireAmt_'+rval+'_'+id+'" id="txtOffHireAmt_'+rval+'_'+id+'" class="input-text"  placeholder="Off Hire(USD)" style="width:90px;background-color:#EAEAEA;" autocomplete="off" value=""/></td></tr>').appendTo("#offhirebody_"+rval);
		$("#txtOFFID_"+rval).val(id);
		$("[id^=txtOffHireRate_"+rval+"_]").numeric();	
		
		initialiseControl();
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}


function removeOffHire(rval,var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#row_offhire_"+rval+"_"+var1).remove();
				getFinalCalculation(rval);
			 }
		else{return false;}
		});
}

function removeSheet(rval)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#tripdiv_"+rval).hide();
				$("#txtCOUTSlaveIdentify_"+rval).val('delete');
				getFinalCalculation(rval);
				$("#tripdiv_"+rval).remove();
			 }
		else{return false;}
		});
}

function AddNewSheet()
{
	jConfirm('Are you sure you want add new Trip/Period ?', 'Confirmation', function(r) {
	if(r){
		    txtTripID = $("#txtTripID").val();
			var idd = parseInt(txtTripID) + 1;
		    $.post("multipletcsheetload.php",{id:""+idd+"",comid:<?php echo $comid;?>}, function(data) 
			{
				$(data).appendTo("#maintripdiv");
				$("#txtTripID").val(idd);
				$("#txtDailyGrossHireUSD_"+idd).val($("#hddnDailyGrossHireUSD").val());
				$("#txtAddCommPerct_"+idd).val($("#hddnaddComm").val());
				$("#txtChartererAcc_"+idd).val($("#hddnbrokerComm").val());
				$("#selExpenseType_"+idd+"_1").html($("#selExpenseType").html());
				$("#selExchangeCurrency_"+idd).html($("#selCurrencyList").html());
				$("#selReDelBunker_"+idd+"_1,#selDelBunker__"+idd+"_1").html($("#selBunker").html());
		        $("#selOthExpVendor_"+idd+"_1,#selOthIncVendor_"+idd+"_1, #selAddCommVendor_"+idd+", #selBroCommVendor_"+idd+",#selESTtcVendor_"+idd+",#selTcInExpVendor_"+idd+",#selESTtcVendor_"+idd).html($("#selVendor").html());
				
				initialiseControl();
				getFinalCalculation(idd);
			});  
		}
	else{return false;}
	});
}


function removeDelBunker(rval,var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_del_"+rval+"_"+var1).remove();	
			getFinalCalculation(rval);		
		 }
	else{return false;}
	});
}


function AddNewDel(rval)
{
	var id = $("#txtDELID_"+rval).val();
	if($("#selDelBunker_"+rval+"_"+id).val() != "" && $("#txtDelQty_"+rval+"_"+id).val() != "" && $("#txtDelBunDate_"+rval+"_"+id).val() != "" && $("#txtDelPrice_"+rval+"_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr class="input-text" id="row_del_'+rval+'_'+id+'"><td><a href="#tbl_'+rval+''+id+'" onClick="removeDelBunker('+rval+','+id+');" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td><td><select name="selDelBunker_'+rval+'_'+id+'" class="input-text" style=" width:90px;" id="selDelBunker_'+rval+'_'+id+'"></select></td><td><input type="text" name="txtDelQty_'+rval+'_'+id+'" id="txtDelQty_'+rval+'_'+id+'" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation('+rval+');" autocomplete="off" /></td><td><input type="text" name="txtDelBunDate_'+rval+'_'+id+'" id="txtDelBunDate_'+rval+'_'+id+'" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td><td><input type="text" name="txtDelPrice_'+rval+'_'+id+'" id="txtDelPrice_'+rval+'_'+id+'" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation('+rval+');" autocomplete="off" /></td><td><input type="text" name="txtDelAmount_'+rval+'_'+id+'" id="txtDelAmount_'+rval+'_'+id+'" class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" readonly="true" autocomplete="off"/></td></tr>').appendTo("#bunkerdelivery_"+rval);
		$("#txtDELID_"+rval).val(id);
		$(".numeric").numeric();	
		$("#selDelBunker_"+rval+"_"+id).html($("#selBunker").html());
		$("[id^=txtDelBunDate_]").datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true
		});
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}


function removeReDelBunker(rval,var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_Redel_"+rval+"_"+var1).remove();	
			getFinalCalculation(rval);	
		 }
	else{return false;}
	});
}


function AddNewReDel(rval)
{
    var id = $("#txtREDELID_"+rval).val();
	if($("#selReDelBunker_"+rval+"_"+id).val() != "" && $("#txtReDelQty_"+rval+"_"+id).val() != "" && $("#txtReDelBunDate_"+rval+"_"+id).val() != "" && $("#txtReDelPrice_"+rval+"_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr class="input-text" id="row_Redel_'+rval+'_'+id+'"><td><a href="#tbl_'+rval+''+id+'" onClick="removeReDelBunker('+rval+','+id+');" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td><td><select name="selReDelBunker_'+rval+'_'+id+'" class="input-text" style=" width:90px;" id="selReDelBunker_'+rval+'_'+id+'"></select></td><td><input type="text" name="txtReDelQty_'+rval+'_'+id+'" id="txtReDelQty_'+rval+'_'+id+'" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation('+rval+');" autocomplete="off" /></td><td><input type="text" name="txtReDelBunDate_'+rval+'_'+id+'" id="txtReDelBunDate_'+rval+'_'+id+'" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td><td><input type="text" name="txtReDelPrice_'+rval+'_'+id+'" id="txtReDelPrice_'+rval+'_'+id+'" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation('+rval+');" autocomplete="off" /></td><td><input type="text" name="txtReDelAmount_'+rval+'_'+id+'" id="txtReDelAmount_'+rval+'_'+id+'" class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" readonly="true" autocomplete="off"/></td></tr>').appendTo("#bunkerRedelivery_"+rval);
		$("#txtREDELID_"+rval).val(id);
		$(".numeric").numeric();	
		$("#selReDelBunker_"+rval+"_"+id).html($("#selBunker").html());
		$("[id^=txtReDelBunDate_]").datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true
		});
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function getCurrencySpan(rval)
{
	if($("#selExchangeCurrency_"+rval).val()!="")
	{
		$("#currencyspan_"+rval).text("("+$("#selExchangeCurrency_"+rval).val()+")");
	}
	else
	{
		$("#currencyspan_"+rval).text('');
	}
}
 
 
</script>
</body>
</html>