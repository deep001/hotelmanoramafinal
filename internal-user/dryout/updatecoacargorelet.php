<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$page 	= $_REQUEST['page'];
$id 	= $_REQUEST['id'];
$coaid 	= $_REQUEST['coaid'];

if(@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->updateCOACargoReletDetails();
	header('Location:./coa_list.php?msg='.$msg);
}
$obj->viewCargoReletEstimationTempleteRecordsNew($id);
$coaid  = $obj->getFun2();
$estimatetype = $obj->getFun92();
$result = mysql_query("select * from coa_master where COAID='".$coaid."'") or die;
$rows   = mysql_fetch_assoc($result);
$currencttoshow = $rows['CURRENCY'];
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id."&coaid=".$coaid."&page=".$page;
$rigts    = explode(',',$obj->getUserRights($_SESSION['uid'],$_SESSION['moduleid'],14));
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
table {border-collapse:separate;}
select{height:19px;}
.select {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-weight: bold;
    color: #333333;
    /* color: #FBC763; */
    text-decoration: none;
    line-height: 13px;
	height:13px;
}

.input-text {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #333333;
	text-decoration: none;
	/*text-transform:uppercase;
	line-height: 15px;*/
}

.input-textsmall {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-weight: normal;
	color: #333333;
	text-decoration:none;
	text-transform:uppercase;
}


.input 
{
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 10px;
font-weight: normal;
color: #333333;
text-decoration: none;
}
</style>
</head>
	<div id="basic-modal-content" style="display:none;" align="center">
	<table cellpadding="1" cellspacing="4" border="0" align="center">
	<tr><td align="center"><img src="../../img/loading.gif" /><br /></td></tr>
	</table>
	</div>
	<body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onClick="getFinalCalculation();">Calculate</a>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(16); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-leaf"></i>&nbsp;COA&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">COA</li>
                    </ol>
                </section>
                <div align="right" style="margin-top:5px;margin-bottom:5px;margin-right:5px;"><a href="coa_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                <table width="100%" style="margin-bottom:5px; padding:0px 5px 0px 5px;">
                   <tr>
                       <td width="12%" class="input-text" style="color:#175c84;"><strong >Fixture&nbsp;Type&nbsp;:</strong>&nbsp;Cargo Relet&nbsp;<input type="hidden" name="selFType" id="selFType" value="2"><input type="hidden" name="rdoEstimateType" id="rdoEstimateType" value="<?php echo $_SESSION['selBType'];?>"/></td>&nbsp;&nbsp;
                       <td class="input-text" style="color:#175c84;"><strong>COA&nbsp;ID.:</strong><input type="text" name="txtCOAID" id="txtCOAID" style="width:90px;color:#175c84;background-color:#E1E1E1;" class="input-text" readonly value="<?php echo $rows['COA_ID']; ?>" /></td>
                       <td class="input-text" style="color:#175c84;">
                       <strong>Vessel&nbsp;:</strong><select name="selVName" class="input-text" id="selVName" style="width:120px;color:#175c84;" onChange="getData();">
                            <?php 
                            $obj->getVesselTypeListMemberWise($estimatetype);
                            ?>
                            </select>
                       </td>
                       <td class="input-text" style="color:#175c84;"><strong>Vessel&nbsp;Type&nbsp;:</strong><input type="text" name="txtVType" id="txtVType" style="width:95px;color:#175c84;background-color:#E1E1E1;" class="input-text" readonly value="" />
           
                        <select  name="selVendor" style="display:none;"  id="selVendor">
                        <?php $obj->getVendorListNewUpdate();	?>
                        </select> 
                        <select  name="selPort" style="display:none;" id="selPort">
                            <?php $obj->getPortList(); ?>
                        </select>
                        
                       </td>
                       <td class="input-text" style="color:#175c84;"><strong>Date&nbsp;:</strong><input type="text" name="txtDate" id="txtDate" style="width:90px;color:#175c84;" class="input-text" value="<?php echo date("d-m-Y",strtotime($obj->getFun7()));?>" placeholder"dd-mm-yy" /></td>
                        <td class="input-text" style="color:#367fa9;"><strong>Cargo Relet&nbsp;No.&nbsp;:</strong><input type="text" name="txtVNo" id="txtVNo" style="width:90px;color:#175c84;" class="input-text" value="<?php echo $obj->getFun8();?>" autocomplete="off" placeholder"" /></td>
						<td class="input-text" style="color:#367fa9;"><strong>Cargo Relet Sheet&nbsp;Name:</strong><input type="text" name="txtVName" id="txtVName" style="width:90px;color:#175c84;" class="input-text" value="<?php echo $obj->getFun9();?>" autocomplete="off" placeholder"" /></td>
                   </tr>
               </table>
                <div id="tabs" class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tabs_2" data-toggle="tab">Cargo Relet: Estimate</a></li>
                        <li><a href="#tabs_3" data-toggle="tab">Commercial Parameters</a></li>
                        <li><a href="#tabs_4" data-toggle="tab">Planned Cargo/Intake</a></li>
                    </ul>
                    
                    <div class="tab-content">
                    
                    <!----------------------------------------------- Start 1------------------------------------------------->
                    <div id="tabs_2" class="tab-pane active" style="overflow:auto;">
                       <table width="100%">
                        <tr>
                        <td class="input-text" width="10%"><strong>Cargo Type : </strong></td>
                        <td width="10%" class="input-text"><strong><?=$obj->getBusinessTypeBasedOnID1($estimatetype);?></strong></td>
						<td class="input-text" width="10%"><strong>Planned Cargo</strong></td>
                        <td width="25%"><select  name="selCargoPlanning" class="input-text required" style="width:250px;" id="selCargoPlanning" onChange="getCargoPlanningdata();"><?php $obj->getCargoPlanningList($_SESSION['selBType'],2,1);?></select></td>
						<td class="input-text" width="10%"><strong>Cargo : </strong></td>
                        <td width="10%" class="input-text"><strong><span id="cargonamspan"></span></strong></td>
						<td class="input-text" width="10%"><strong>Cargo Qty(MT) : </strong></td>
                        <td width="10%" class="input-text"><strong><input type="text" name="txtActualQty" id="txtActualQty" style="width:130px;" readonly class="input-text"  value="<?php echo $obj->getFun10();?>" placeholder=""/><span id="cargoQtyspan"></span></strong></td>
                        
                       </tr>
                       </table>
					   
					   <table class='tablesorter' width="100%">
					    <tr>
						   <td width="50%" style="border-right:1px solid #0077B0;font-size:13px;padding:3px;"><strong>Cargo IN</strong></td>
						   <td width="50%"><strong style="font-size:13px;padding:3px;">Cargo OUT</strong></td>
						</tr>
                        <tr>
						   <td width="50%" style="border-right:1px solid #0077B0;">
						       <table class='tablesorter' width="100%">
								  <tr><td><strong style="font-size:12px;">Parties/Entities</strong></td></tr>    
							   </table>
						   	   <table class='tablesorter' width="100%">
								 <tbody id="tbodypartis">
									 <tr id="partiesrow1_1">
									   <td>#</td>
									   <td><strong>Charterer</strong></td>
									   <td><strong>Owner</strong></td>
									   <td><strong>Broker</strong></td>
									 </tr>	
                                  <?php 
									$sql1 = "select * from cargo_relet_estimate_slave1 where FCAID='".$obj->getFun1()."' and IDENTIFY='IN'";
									$res1 = mysql_query($sql1);
									$rec1 = mysql_num_rows($res1);
									if($rec1==0)
									{$num = 1;
									?> 
									 <tr id="partiesrow_1">
									   <td width="50px;"></td>
									   <td><select name="selCharterers_1" class="input-text required" style="width:130px;" id="selCharterers_1"></select></td>
									   <td><select name="selOwner_1" class="input-text required" style="width:130px;" id="selOwner_1"></select></td>
									   <td><select name="selBroker_1" class="input-text required" style="width:130px;" id="selBroker_1"></select></td>
									   <script>$("#selCharterers_1,#selOwner_1,#selBroker_1").html($("#selVendor").html());</script>
									 </tr>
                              <?php }else{$i = 0;$num =$rec1;
								while($rows1 = mysql_fetch_assoc($res1)){$i++;?>
                                     <tr id="partiesrow_<?php echo $i;?>">
									   <td width="50px;"><?php if($i>1){?><a href="#tb<?php echo $i;?>" onClick="deletepartiesDetails(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a><?php }?></td>
									   <td><select name="selCharterers_<?php echo $i;?>" class="input-text required" style="width:130px;" id="selCharterers_<?php echo $i;?>"></select></td>
									   <td><select name="selOwner_<?php echo $i;?>" class="input-text required" style="width:130px;" id="selOwner_<?php echo $i;?>"></select></td>
									   <td><select name="selBroker_<?php echo $i;?>" class="input-text required" style="width:130px;" id="selBroker_<?php echo $i;?>"></select></td>
									   <script>$("#selCharterers_<?php echo $i;?>,#selOwner_<?php echo $i;?>,#selBroker_<?php echo $i;?>").html($("#selVendor").html());
                                       $("#selCharterers_<?php echo $i;?>").val('<?php echo $rows1['CHARTERER'];?>');$("#selOwner_<?php echo $i;?>").val('<?php echo $rows1['OWNER'];?>');$("#selBroker_<?php echo $i;?>").val('<?php echo $rows1['BROKER'];?>');
                                       </script>
									 </tr>
                             <?php }}?>
								</tbody>
									 
								<tfoot style="background-color:#fff;">
								     <tr><td colspan="4"><button type="button" onClick="addpartiesDetails();" >Add</button>
								          <input type="hidden" name="txtPartiesID" id="txtPartiesID" value="<?php echo $num;?>" /></td>
									 </tr>
									</tfoot>
								</table>
						   </td>
						   <td width="50%" style="padding:3px;">
						       <table class='tablesorter' width="100%">
								  <tr><td><strong style="font-size:12px;">&nbsp;</strong></td></tr>    
							   </table>
						   	   <table class='tablesorter' width="100%">
								 <tbody id="tbodypartisout">
									 <tr id="partiesrow1out_1">
									   <td>#</td>
									   <td><strong>Charterer</strong></td>
									   <td><strong>Owner</strong></td>
									   <td><strong>Broker</strong></td>
									 </tr>
                                  <?php 
									$sql1 = "select * from cargo_relet_estimate_slave1 where FCAID='".$obj->getFun1()."' and IDENTIFY='OUT'";
									$res1 = mysql_query($sql1);
									$rec1 = mysql_num_rows($res1);
									if($rec1==0)
									{$num = 1;
									?>	 
									 <tr id="partiesrowout_1">
									   <td width="50px;"></td>
									   <td><select name="selCharterersout_1" class="input-text required" style="width:130px;" id="selCharterersout_1"></select></td>
									   <td><select name="selOwnerout_1" class="input-text required" style="width:130px;" id="selOwnerout_1"></select></td>
									   <td><select name="selBrokerout_1" class="input-text required" style="width:130px;" id="selBrokerout_1"></select></td>
									   <script>$("#selCharterersout_1,#selOwnerout_1,#selBrokerout_1").html($("#selVendor").html());</script>
									 </tr>
                                <?php }else{$i = 0;$num =$rec1;
								while($rows1 = mysql_fetch_assoc($res1)){$i++;?>
                                     <tr id="partiesrowout_<?php echo $i;?>">
									   <td width="50px;"><?php if($i>1){?><a href="#tb<?php echo $i;?>" onClick="deletepartiesDetailsout(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a><?php }?></td>
									   <td><select name="selCharterersout_<?php echo $i;?>" class="input-text required" style="width:130px;" id="selCharterersout_<?php echo $i;?>"></select></td>
									   <td><select name="selOwnerout_<?php echo $i;?>" class="input-text required" style="width:130px;" id="selOwnerout_<?php echo $i;?>"></select></td>
									   <td><select name="selBrokerout_<?php echo $i;?>" class="input-text required" style="width:130px;" id="selBrokerout_<?php echo $i;?>"></select></td>
									   <script>$("#selCharterersout_<?php echo $i;?>,#selOwnerout_<?php echo $i;?>,#selBrokerout_<?php echo $i;?>").html($("#selVendor").html());
                                       $("#selCharterersout_<?php echo $i;?>").val('<?php echo $rows1['CHARTERER'];?>');$("#selOwnerout_<?php echo $i;?>").val('<?php echo $rows1['OWNER'];?>');$("#selBrokerout_<?php echo $i;?>").val('<?php echo $rows1['BROKER'];?>');
                                       </script>
									 </tr>
                             <?php }}?>
								</tbody>
									 
								<tfoot style="background-color:#fff;">
								     <tr><td colspan="4"><button type="button" onClick="addpartiesDetailsout();" >Add</button>
								          <input type="hidden" name="txtPartiesoutID" id="txtPartiesoutID" value="1" /></td>
									 </tr>
									</tfoot>
								</table>
						   </td>
						</tr>
						<tr>
						   <td width="50%" style="border-right:1px solid #0077B0;padding:3px;">
						   	   <table class='tablesorter' width="100%">
								  <tr><td><strong style="font-size:12px;">Ports</strong></td></tr>    
							   </table>
						   	   <table class='tablesorter' width="100%">
								 <tbody id="tbodylport">
									 <tr id="portlrow1_1">
									   <td>#</td>
									   <td><strong>Load Port</strong></td>
									   <td><strong>Comments</strong></td>
									 </tr>	 
                                    <?php 
									$sql1 = "select * from cargo_relet_estimate_slave2 where FCAID='".$obj->getFun1()."' and IDENTIFY='IN' and PORT_TYPE='LP'";
									$res1 = mysql_query($sql1);
									$rec1 = mysql_num_rows($res1);
									if($rec1==0)
									{$num = 1;
									?> 
									 <tr id="portlrow_1">
									   <td width="50px;"></td>
									   <td><select name="selLoadPort_1" class="input-text required" style="width:130px;" id="selLoadPort_1"></select></td>
									   <td><textarea class="input-text required" name="txtLComments_1" id="txtLComments_1" rows="2" cols="25" placeholder="Comments"></textarea></td>
									   <script>$("#selLoadPort_1").html($("#selPort").html());</script>
									 </tr>
                                <?php }else{$i = 0;$num =$rec1;
								while($rows1 = mysql_fetch_assoc($res1)){$i++;?>
                                     <tr id="portlrow_<?php echo $i;?>">
									   <td width="50px;"><?php if($i>1){?><a href="#tb<?php echo $i;?>" onClick="deleteLport(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a><?php }?></td>
									   <td><select name="selLoadPort_<?php echo $i;?>" class="input-text required" style="width:130px;" id="selLoadPort_<?php echo $i;?>"></select></td>
									   <td><textarea class="input-text required" name="txtLComments_<?php echo $i;?>" id="txtLComments_<?php echo $i;?>" rows="2" cols="25" placeholder="Comments"><?php echo $rows1['COMMENTS'];?></textarea></td>
									   <script>$("#selLoadPort_<?php echo $i;?>").html($("#selPort").html());$("#selLoadPort_<?php echo $i;?>").val(<?php echo $rows1['PORTID'];?>);</script>
									 </tr>
                             <?php }}?>
								</tbody>
									 
								<tfoot style="background-color:#fff;">
								     <tr><td colspan="4"><button type="button" onClick="addlportDetails();" >Add</button>
								          <input type="hidden" name="txtLPortID" id="txtLPortID" value="<?php echo $num;?>" /></td>
									 </tr>
									</tfoot>
								</table>
                                <table class='tablesorter' width="100%">
								 <tbody id="tbodydport">
									 <tr>
									   <td>#</td>
									   <td><strong>Dis Port</strong></td>
									   <td><strong>Comments</strong></td>
									 </tr>
                                    <?php 
									$sql1 = "select * from cargo_relet_estimate_slave2 where FCAID='".$obj->getFun1()."' and IDENTIFY='IN' and PORT_TYPE='DP'";
									$res1 = mysql_query($sql1);
									$rec1 = mysql_num_rows($res1);
									if($rec1==0)
									{$num = 1;?>
                                     <tr id="portdrow_1">
									   <td width="50px;"></td>
									   <td style="border-bottom:none;"><select name="selDisPort_1" class="input-text required" style="width:130px;" id="selDisPort_1"></select></td>
									   <td><textarea class="input-text required" name="txtDComments_1" id="txtDComments_1" rows="2" cols="25" placeholder="Comments"></textarea></td>
									   <script>$("#selDisPort_1").html($("#selPort").html());</script>
									 </tr>
                                    
                                <?php }else{$i = 0;$num =$rec1;
								while($rows1 = mysql_fetch_assoc($res1)){$i++;?>	 
									 <tr id="portdrow_<?php echo $i;?>">
									   <td width="50px;"><?php if($i>1){?><a href="#tb<?php echo $i;?>" onClick="deleteDport(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a><?php }?></td>
									   <td style="border-bottom:none;"><select name="selDisPort_<?php echo $i;?>" class="input-text required" style="width:130px;" id="selDisPort_<?php echo $i;?>"></select></td>
									   <td><textarea class="input-text required" name="txtDComments_<?php echo $i;?>" id="txtDComments_<?php echo $i;?>" rows="2" cols="25" placeholder="Comments"><?php echo $rows1['COMMENTS'];?></textarea></td>
									   <script>$("#selDisPort_<?php echo $i;?>").html($("#selPort").html());$("#selDisPort_<?php echo $i;?>").val(<?php echo $rows1['PORTID'];?>);</script>
									 </tr>
                             <?php }}?>
								</tbody>
								<tfoot style="background-color:#fff;">
								     <tr><td colspan="3"><button type="button" onClick="adddportDetails();" >Add</button>
								          <input type="hidden" name="txtDPortID" id="txtDPortID" value="<?php echo $num;?>" /></td>
									 </tr>
									</tfoot>
								</table>
						   </td>
						   <td width="50%" style="padding:3px;">
						        <table class='tablesorter' width="100%">
								  <tr><td><strong style="font-size:12px;">Ports</strong></td></tr>    
							   </table>
						   	   <table class='tablesorter' width="100%">
								 <tbody id="tbodylportout">
									 <tr id="portlrowout1_1">
									   <td>#</td>
									   <td><strong>Load Port</strong></td>
									   <td><strong>Comments</strong></td>
									 </tr>	 
                                 <?php 
									$sql1 = "select * from cargo_relet_estimate_slave2 where FCAID='".$obj->getFun1()."' and IDENTIFY='OUT' and PORT_TYPE='LP'";
									$res1 = mysql_query($sql1);
									$rec1 = mysql_num_rows($res1);
									if($rec1==0)
									{$num = 1;
									?> 
									 <tr id="portlrowout_1">
									   <td width="50px;"></td>
									   <td><select name="selLoadPortout_1" class="input-text required" style="width:130px;" id="selLoadPortout_1"></select></td>
									   <td><textarea class="input-text required" name="txtLCommentsout_1" id="txtLCommentsout_1" rows="2" cols="25" placeholder="Comments"></textarea></td>
									   <script>$("#selLoadPortout_1").html($("#selPort").html());</script>
									 </tr>
                                <?php }else{$i = 0;$num =$rec1;
								while($rows1 = mysql_fetch_assoc($res1)){$i++;?>
                                     <tr id="portlrowout_<?php echo $i;?>">
									   <td width="50px;"><?php if($i>1){?><a href="#tb<?php echo $i;?>" onClick="deleteLportout(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a><?php }?></td>
									   <td><select name="selLoadPortout_<?php echo $i;?>" class="input-text required" style="width:130px;" id="selLoadPortout_<?php echo $i;?>"></select></td>
									   <td><textarea class="input-text required" name="txtLCommentsout_<?php echo $i;?>" id="txtLCommentsout_<?php echo $i;?>" rows="2" cols="25" placeholder="Comments"><?php echo $rows1['COMMENTS'];?></textarea></td>
									   <script>$("#selLoadPortout_<?php echo $i;?>").html($("#selPort").html());$("#selLoadPortout_<?php echo $i;?>").val(<?php echo $rows1['PORTID'];?>);</script>
									 </tr>
                             <?php }}?>    
                                </tbody>
								<tfoot style="background-color:#fff;">
								     <tr><td colspan="4"><button type="button" onClick="addlportDetailsout();" >Add</button>
								          <input type="hidden" name="txtLPortoutID" id="txtLPortoutID" value="<?=$num;?>" /></td>
									 </tr>
									</tfoot>
								</table>
                                <table class='tablesorter' width="100%">
								 <tbody id="tbodydportout">
									 <tr id="portdrowout1_1">
									   <td>#</td>
									   <td><strong>Dis Port</strong></td>
									   <td><strong>Comments</strong></td>
									 </tr>
                                 <?php 
									$sql1 = "select * from cargo_relet_estimate_slave2 where FCAID='".$obj->getFun1()."' and IDENTIFY='OUT' and PORT_TYPE='DP'";
									$res1 = mysql_query($sql1);
									$rec1 = mysql_num_rows($res1);
									if($rec1==0)
									{$num = 1;
									?> 
									 <tr id="portdrowout_1">
									   <td width="50px;"></td>
									   <td><select name="selDisPortout_1" class="input-text required" style="width:130px;" id="selDisPortout_1"></select></td>
									   <td><textarea class="input-text required" name="txtDCommentsout_1" id="txtDCommentsout_1" rows="2" cols="25" placeholder="Comments"></textarea></td>
									   <script>$("#selDisPortout_1").html($("#selPort").html());</script>
									 </tr>
                                <?php }else{$i = 0;$num =$rec1;
								while($rows1 = mysql_fetch_assoc($res1)){$i++;?>
                                     <tr id="portdrowout_<?php echo $i;?>">
									   <td width="50px;"><?php if($i>1){?><a href="#tb<?php echo $i;?>" onClick="deleteDportout(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a><?php }?></td>
									   <td><select name="selDisPortout_<?php echo $i;?>" class="input-text required" style="width:130px;" id="selDisPortout_<?php echo $i;?>"></select></td>
									   <td><textarea class="input-text required" name="txtDCommentsout_<?php echo $i;?>" id="txtDCommentsout_<?php echo $i;?>" rows="2" cols="25" placeholder="Comments"><?php echo $rows1['COMMENTS'];?></textarea></td>
									   <script>$("#selDisPortout_<?php echo $i;?>").html($("#selPort").html());$("#selDisPortout_<?php echo $i;?>").val(<?php echo $rows1['PORTID'];?>);</script>
									 </tr>
                             <?php }}?>
								</tbody>
									 
								<tfoot style="background-color:#fff;">
								     <tr><td colspan="3"><button type="button" onClick="adddportDetailsout();" >Add</button>
								          <input type="hidden" name="txtDPortoutID" id="txtDPortoutID" value="<?=$num;?>" /></td>
									 </tr>
									</tfoot>
								</table>
						   </td>
						</tr>
                        <tr>
						   <td width="50%" style="border-right:1px solid #0077B0;padding:3px;">
                              <table class='tablesorter' width="100%">
								  <tr><td><strong style="font-size:12px;">Freight</strong></td></tr>    
							  </table>
                           	  <table class='tablesorter' width="100%">
                                 <tr class="input-text">
                                    <td>Frt Rate(<?php echo $currencttoshow;?>/MT) : </td>
                                    <td><input type="text" name="txtFreightUSD" id="txtFreightUSD" style="width:130px;" readonly class="input-text numeric"  value="<?php echo $obj->getFun11();?>" onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                    <td>BAF : </td>
                                    <td><input type="text" name="txtBAFUSD" id="txtBAFUSD" style="width:130px;" class="input-text numeric"  value="<?php echo $obj->getFun12();?>" onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Frt Applicable From : </td>
                                    <?php if(date('d-m-Y',strtotime($obj->getFun13()))!='01-01-1970'){$datefrom = date('d-m-Y',strtotime($obj->getFun13()));}else{$datefrom = '';}?>
                                    <?php if(date('d-m-Y',strtotime($obj->getFun14()))!='01-01-1970'){$dateto = date('d-m-Y',strtotime($obj->getFun14()));}else{$dateto = '';}?>
                                    <td><input type="text" name="txtApplicableFrom" id="txtApplicableFrom" style="width:130px;" class="input-text"  value="<?php echo $datefrom;?>" placeholder="dd-mm-yyyy"/></td>
                                    <td>Frt Applicable To : </td>
                                    <td><input type="text" name="txtApplicableTo" id="txtApplicableTo" style="width:130px;" class="input-text"  value="<?php echo $dateto;?>" placeholder="dd-mm-yyyy"/></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Add Comm(%) : </td>
                                    <td><input type="text" name="txtAddComm" id="txtAddComm" style="width:130px;" class="input-text numeric"  value="<?php echo $obj->getFun15();?>" onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                    <td>Brokerage(%) : </td>
                                    <td><input type="text" name="txtBrokerage" id="txtBrokerage" style="width:130px;" class="input-text numeric" onKeyUp="getCalculation();" value="<?php echo $obj->getFun16();?>" placeholder="0.00"/></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Dem Rate(<?php echo $currencttoshow;?>/Day) : </td>
                                    <td><input type="text" name="txtDemRate" id="txtDemRate" style="width:130px;" class="input-text numeric"  value="<?php echo $obj->getFun17();?>" onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                    <td>Despatch Rate(<?php echo $currencttoshow;?>/Day) : </td>
                                    <td><input type="text" name="txtDespatchRate" id="txtDespatchRate" style="width:130px;" class="input-text numeric"  value="<?php echo $obj->getFun18();?>" onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Contract FO Price(<?php echo $currencttoshow;?>/MT) : </td>
                                    <td><input type="text" name="txtConFOPrice" id="txtConFOPrice" style="width:130px;" class="input-text numeric" value="<?php echo $obj->getFun19();?>" readonly onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                    <td>Current FO Price(<?php echo $currencttoshow;?>/MT) : </td>
                                    <td><input type="text" name="txtCurFOPrice" id="txtCurFOPrice" style="width:130px;" class="input-text numeric" onKeyUp="getCalculation();" value="<?php echo $obj->getFun20();?>" placeholder="0.00"/></td>
                                 </tr>
                                 <tr>
                                    <td colspan="4">&nbsp;</td>
                                 </tr>
                              </table>
                              <table class='tablesorter' width="100%" >
                                 <tr class="input-text">
                                    <td>Payment Clause : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtPaymentClause" id="txtPaymentClause" rows="2" cols="40" placeholder="Payment Clause"><?php echo $obj->getFun29();?></textarea></td>
                                 </tr>
                                 <tr>
                                    <td>Bunker Clause : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtBunkerClause" id="txtBunkerClause" rows="2" cols="40" placeholder="Bunker Clause"><?php echo $obj->getFun30();?></textarea></td>
                                 </tr>
                                 
                              </table>
                           </td>
                           <td width="50&" style="padding:3px;"> 
                              <table class='tablesorter' width="100%">
								  <tr><td><strong style="font-size:12px;">Freight</strong></td></tr>    
							  </table>
                           	  <table class='tablesorter' width="100%">
                                 <tr class="input-text">
                                    <td>Frt Rate(<?php echo $currencttoshow;?>/MT) : </td>
                                    <td><input type="text" name="txtFreightUSDout" id="txtFreightUSDout" style="width:130px;" class="input-text numeric" value="<?php echo $obj->getFun21();?>" onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                    <td>BAF : </td>
                                    <td><input type="text" name="txtBAFUSDout" id="txtBAFUSDout" style="width:130px;" class="input-text numeric"  value="<?php echo $obj->getFun22();?>" onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                 </tr>
                                 <tr>
                                 <?php if(date('d-m-Y',strtotime($obj->getFun23()))!='01-01-1970'){$datefrom = date('d-m-Y',strtotime($obj->getFun23()));}else{$datefrom = '';}?>
                                 <?php if(date('d-m-Y',strtotime($obj->getFun24()))!='01-01-1970'){$dateto = date('d-m-Y',strtotime($obj->getFun24()));}else{$dateto = '';}?>
                                    <td>Frt Applicable From : </td>
                                    <td><input type="text" name="txtApplicableFromout" id="txtApplicableFromout" style="width:130px;" class="input-text"  value="<?=$datefrom;?>" placeholder="dd-mm-yyyy"/></td>
                                    <td>Frt Applicable To : </td>
                                    <td><input type="text" name="txtApplicableToout" id="txtApplicableToout" style="width:130px;" class="input-text"  value="<?=$dateto;?>" placeholder="dd-mm-yyyy"/></td>
                                 </tr>
                                 <tr>
                                    <td>Add Comm(%) : </td>
                                    <td><input type="text" name="txtAddCommout" id="txtAddCommout" style="width:130px;" class="input-text numeric"  value="<?php echo $obj->getFun25();?>" onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                    <td>Brokerage(%) : </td>
                                    <td><input type="text" name="txtBrokerageout" id="txtBrokerageout" style="width:130px;" class="input-text numeric" onKeyUp="getCalculation();" value="<?php echo $obj->getFun26();?>" placeholder="0.00"/></td>
                                 </tr>
                                 <tr>
                                    <td>Dem Rate(<?php echo $currencttoshow;?>/Day) : </td>
                                    <td><input type="text" name="txtDemRateout" id="txtDemRateout" style="width:130px;" class="input-text numeric"  value="<?php echo $obj->getFun27();?>" onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                    <td>Despatch Rate(<?php echo $currencttoshow;?>/Day) : </td>
                                    <td><input type="text" name="txtDespatchRateout" id="txtDespatchRateout" style="width:130px;" class="input-text numeric"  value="<?php echo $obj->getFun28();?>" onKeyUp="getCalculation();" placeholder="0.00"/></td>
                                 </tr>
                                 
                                 <tr>
                                    <td colspan="4">&nbsp;</td>
                                 </tr>
                              </table>
                              <table class='tablesorter' width="100%">
                                 <tr class="input-text">
                                    <td>Payment Clause : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtPaymentClauseout" id="txtPaymentClauseout" rows="2" cols="40" placeholder="Payment Clause"><?php echo $obj->getFun31();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Bunker Clause : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtBunkerClauseout" id="txtBunkerClauseout" rows="2" cols="40" placeholder="Bunker Clause"><?php echo $obj->getFun32();?></textarea></td>
                                 </tr>
                              </table>
                           </td>
                        </tr> 
                        <tr>
						   <td width="50%" style="border-right:1px solid #0077B0;padding:3px;">
                              <table class='tablesorter' width="100%">
								  <tr><td><strong style="font-size:12px;">Result</strong></td></tr>    
							  </table>
                              <table class='tablesorter' width="100%">
                                 <tr class="input-text">
                                    <td>Freight(<?php echo $currencttoshow;?>) : </td>
                                    <td><input type="text" name="txtFreightAmt" id="txtFreightAmt" style="width:130px;" class="input-text"  value="<?php echo $obj->getFun33();?>" readonly placeholder="0.00"/></td>
                                    <td>Bunker Surcharge(<?php echo $currencttoshow;?>) : </td>
                                    <td><input type="text" name="txtBunkerSurcharge" id="txtBunkerSurcharge" style="width:130px;" class="input-text"  value="<?php echo $obj->getFun34();?>" readonly placeholder="0.00"/></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Dem(<?php echo $currencttoshow;?>): </td>
                                    <td><input type="text" name="txtDemmurageAmt" class="input-text" style="width:130px;" id="txtDemmurageAmt" value="<?php echo $obj->getFun35();?>" readonly placeholder="0.00"/></td>
                                    <td>Despatch(<?php echo $currencttoshow;?>): </td>
                                    <td><input type="text" name="txtDespatchAmt" class="input-text" style="width:130px;" id="txtDespatchAmt" value="<?php echo $obj->getFun36();?>" readonly placeholder="0.00"/></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Add Comm(<?php echo $currencttoshow;?>): </td>
                                    <td><input type="text" name="txtAddCommAmt" class="input-text" style="width:130px;" id="txtAddCommAmt" value="<?php echo $obj->getFun37();?>" readonly placeholder="0.00"/></td>
                                    <td>Brokerage(<?php echo $currencttoshow;?>): </td>
                                    <td><input type="text" name="txtBrokerageAmt" class="input-text" style="width:130px;" id="txtBrokerageAmt" value="<?php echo $obj->getFun38();?>" readonly placeholder="0.00"/></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td><strong>Total(<?php echo $currencttoshow;?>):</strong></td>
                                    <td><input type="text" name="txtTotals" class="input-text" style="width:130px;" id="txtTotals" value="<?php echo $obj->getFun39();?>" readonly placeholder="0.00"/></td>
                                    <td><strong>Profit(<?php echo $currencttoshow;?>):</strong></td>
                                    <td><input type="text" name="txtProfit" class="input-text" style="width:130px;" id="txtProfit" value="<?php echo $obj->getFun40();?>" readonly placeholder="0.00"/></td>
                                 </tr>
                              </table>
                              <table class='tablesorter' width="100%">
								  <tr><td><strong style="font-size:12px;">Various Notes</strong></td></tr>    
							  </table>
                              <table class='tablesorter' width="100%">
                                 <tr class="input-text">
                                    <td>COA ref if applicable : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtCOARef" id="txtCOARef" rows="2" cols="40" placeholder="COA ref if applicable"><?php echo $obj->getFun48();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Load Port Agents : </td>
                                    <td><select name="selLoadPortAgent" class="input-text" style="width:130px;" id="selLoadPortAgent"></select></td>
                                    <td>Remark : </td>
                                    <td><textarea class="input-text" name="txtLAgentRemarks" id="txtLAgentRemarks" rows="2" cols="20" placeholder="Remark"><?php echo $obj->getFun50();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Dis Port Agents : </td>
                                    <td><select name="selDisPortAgent" class="input-text" style="width:130px;" id="selDisPortAgent"></select></td>
                                    <td>Remark : </td>
                                    <td><textarea class="input-text" name="txtDAgentRemarks" id="txtDAgentRemarks" rows="2" cols="20" placeholder="Remark"><?php echo $obj->getFun52();?></textarea></td>
                                 </tr>
                                 <script>$("#selLoadPortAgent,#selDisPortAgent").html($("#selVendor").html());$("#selLoadPortAgent").val('<?php echo $obj->getFun49();?>');$("#selDisPortAgent").val('<?php echo $obj->getFun51();?>');</script>
                                 <tr class="input-text">
                                    <td>Notices : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtNotices" id="txtNotices" rows="2" cols="40" placeholder="Notices"><?php echo $obj->getFun53();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>D/A : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtDA" id="txtDA" rows="2" cols="40" placeholder="D/A"><?php echo $obj->getFun54();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Extra Insurance : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtExtraInsurance" id="txtExtraInsurance" rows="2" cols="40" placeholder="Extra Insurance"><?php echo $obj->getFun55();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Main Terms : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtMinTerms" id="txtMinTerms" rows="2" cols="40" placeholder="Main Terms"><?php echo $obj->getFun56();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Attachments : </td>
                                    <td>
                                    	<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Attachments">
                                            <i class="fa fa-paperclip"></i> Attachments
                                            <input type="file" class="form-control" multiple name="mul_file[]" id="mul_file" title="" data-widget="Add Attachments" data-toggle="tooltip" data-original-title="Add Attachment"/>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                       <table width="100%">
                                    <?php if($obj->getFun57() != '')
										{ 
											$file = explode(",",$obj->getFun57()); 
											$name = explode(",",$obj->getFun58()); ?>										
										<tr>
											<td colspan="2">
										  Previous Attachments
											</td>
										 </tr>
						   
											<?php
											$j =1;
											for($i=0;$i<sizeof($file);$i++)
											{
											?>
											<tr id="row_file_<?php echo $j;?>">
												<td width="90%" align="left" class="input-text"  valign="top"><a href="../../attachment/<?php echo $file[$i]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="<?php echo $name[$i];?>"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $name[$i];?></a>
												<input type="hidden" name="file_<?php echo $j;?>" id="file_<?php echo $j;?>" value="<?php echo $file[$i]; ?>" />
												<input type="hidden" name="name_<?php echo $j;?>" id="name_<?php echo $j;?>" value="<?php echo $name[$i]; ?>" />
												</td>
												<td ><a href="#1" onClick="Del_Upload(<?php echo $j;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
											</tr>
											<?php $j++;}?>
										<?php }?>
                                        </table>
                                    </td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Special Comments : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtSpecialComments" id="txtSpecialComments" rows="2" cols="40" placeholder="Special Comments"><?php echo $obj->getFun59();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Nomination Proc : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtNominationProc" id="txtNominationProc" rows="2" cols="40" placeholder="Nomination Proc"><?php echo $obj->getFun60();?></textarea></td>
                                 </tr>
                              </table>
                           </td>
						   <td width="50%" style="padding:3px;">
                              <table class='tablesorter' width="100%">
								  <tr><td><strong style="font-size:12px;">Result</strong></td></tr>    
							  </table>
                              <table class='tablesorter' width="100%">
                                 <tr class="input-text">
                                    <td>Freight(<?php echo $currencttoshow;?>) : </td>
                                    <td><input type="text" name="txtFreightAmtout" id="txtFreightAmtout" style="width:130px;" class="input-text"  value="<?php echo $obj->getFun41();?>" readonly placeholder="0.00"/></td>
                                    <td>Bunker Surcharge(<?php echo $currencttoshow;?>) : </td>
                                    <td><input type="text" name="txtBunkerSurchargeout" id="txtBunkerSurchargeout" style="width:130px;" class="input-text"  value="<?php echo $obj->getFun42();?>" readonly placeholder="0.00"/></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Dem(<?php echo $currencttoshow;?>): </td>
                                    <td><input type="text" name="txtDemmurageAmtout" class="input-text" style="width:130px;" id="txtDemmurageAmtout" value="<?php echo $obj->getFun43();?>" readonly placeholder="0.00"/></td>
                                    <td>Despatch(<?php echo $currencttoshow;?>): </td>
                                    <td><input type="text" name="txtDespatchAmtout" class="input-text" style="width:130px;" id="txtDespatchAmtout" value="<?php echo $obj->getFun44();?>" readonly placeholder="0.00"/></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Add Comm(<?php echo $currencttoshow;?>): </td>
                                    <td><input type="text" name="txtAddCommAmtout" class="input-text" style="width:130px;" id="txtAddCommAmtout" value="<?php echo $obj->getFun45();?>" readonly placeholder="0.00"/></td>
                                    <td>Brokerage(<?php echo $currencttoshow;?>): </td>
                                    <td><input type="text" name="txtBrokerageAmtout" class="input-text" style="width:130px;" id="txtBrokerageAmtout" value="<?php echo $obj->getFun46();?>" readonly placeholder="0.00"/></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td><strong>Total(<?php echo $currencttoshow;?>):</strong></td>
                                    <td><input type="text" name="txtTotalsout" class="input-text" style="width:130px;" id="txtTotalsout" value="<?php echo $obj->getFun47();?>" readonly placeholder="0.00"/></td>
                                    <td></td>
                                    <td></td>
                                 </tr>
                              </table>
                           	  <table class='tablesorter' width="100%">
								  <tr><td><strong style="font-size:12px;">Various Notes</strong></td></tr>    
							  </table>
                              <table class='tablesorter' width="100%" >
                                 <tr class="input-text">
                                    <td>COA ref if applicable : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtCOARefout" id="txtCOARefout" rows="2" cols="40" placeholder="COA ref if applicable"><?php echo $obj->getFun61();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Load Port Agents : </td>
                                    <td><select name="selLoadPortAgentout" class="input-text" style="width:130px;" id="selLoadPortAgentout"></select></td>
                                    <td>Remark : </td>
                                    <td><textarea class="input-text" name="txtLAgentRemarksout" id="txtLAgentRemarksout" rows="2" cols="20" placeholder="Remark"><?php echo $obj->getFun63();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Dis Port Agents : </td>
                                    <td><select name="selDisPortAgentout" class="input-text" style="width:130px;" id="selDisPortAgentout"></select></td>
                                    <td>Remark : </td>
                                    <td><textarea class="input-text" name="txtDAgentRemarksout" id="txtDAgentRemarksout" rows="2" cols="20" placeholder="Remark"><?php echo $obj->getFun65();?></textarea></td>
                                 </tr>
                                 <script>$("#selLoadPortAgentout,#selDisPortAgentout").html($("#selVendor").html());$("#selLoadPortAgentout").val('<?php echo $obj->getFun62();?>'); $("#selDisPortAgentout").val('<?php echo $obj->getFun64();?>');</script>
                                 <tr class="input-text">
                                    <td>Notices : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtNoticesout" id="txtNoticesout" rows="2" cols="40" placeholder="Notices"><?php echo $obj->getFun63();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>D/A : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtDAout" id="txtDAout" rows="2" cols="40" placeholder="D/A"><?php echo $obj->getFun67();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Extra Insurance : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtExtraInsuranceout" id="txtExtraInsuranceout" rows="2" cols="40" placeholder="Extra Insurance"><?php echo $obj->getFun68();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Main Terms : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtMinTermsout" id="txtMinTermsout" rows="2" cols="40" placeholder="Main Terms"><?php echo $obj->getFun69();?></textarea></td>
                                 </tr>
                                 <tr class="input-text">
                                    <td>Attachments : </td>
                                    <td>
                                    	<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Attachments">
                                            <i class="fa fa-paperclip"></i> Attachments
                                            <input type="file" class="form-control" multiple name="mul_fileout[]" id="mul_fileout" title="" data-widget="Add Attachments" data-toggle="tooltip" data-original-title="Add Attachment"/>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                       <table width="100%">
                                    <?php if($obj->getFun70() != '')
										{ 
											$file = explode(",",$obj->getFun70()); 
											$name = explode(",",$obj->getFun71()); ?>										
										<tr>
											<td colspan="2">
										  Previous Attachments
											</td>
										 </tr>
						   
											<?php
											$j =1;
											for($i=0;$i<sizeof($file);$i++)
											{
											?>
											<tr id="row_file1_<?php echo $j;?>">
												<td width="90%" align="left" class="input-text"  valign="top"><a href="../../attachment/<?php echo $file[$i]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="<?php echo $name[$i];?>"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $name[$i];?></a>
												<input type="hidden" name="file1_<?php echo $j;?>" id="file1_<?php echo $j;?>" value="<?php echo $file[$i]; ?>" />
												<input type="hidden" name="name1_<?php echo $j;?>" id="name1_<?php echo $j;?>" value="<?php echo $name[$i]; ?>" />
												</td>
												<td ><a href="#1" onClick="Del_Upload1(<?php echo $j;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
											</tr>
											<?php $j++;}?>
										<?php }?>
                                        </table>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Special Comments : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtSpecialCommentsout" id="txtSpecialCommentsout" rows="2" cols="40" placeholder="Special Comments"><?php echo $obj->getFun72();?></textarea></td>
                                 </tr>
                                 <tr>
                                    <td>Nomination Proc : </td>
                                    <td colspan="3"><textarea class="input-text" name="txtNominationProcout" id="txtNominationProcout" rows="2" cols="40" placeholder="Nomination Proc"><?php echo $obj->getFun73();?></textarea></td>
                                 </tr>
                              </table>
                           </td>
					   </table>
                       
                       
                       
                    
                    
                    </div>
                    <!-- /.tab_1-pane -->
                    <div id="tabs_3" class="tab-pane">

                         <table width="100%" style="margin-top:2px;">
                           <tr>
                               <td class="input-text">&nbsp;DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>&nbsp;:&nbsp;<input type="text" name="txtDWTS" id="txtDWTS" style="width:90px;" class="input-text" autocomplete="off" readonly value="" placeholder="0.00" />
                               </td>
                               <td class="input-text"></td>
                                <td class="input-text" id="tankgasnot1"><input name="rdoCap" id="rdoCap1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoCap == 1) echo "checked"; ?> onClick="showCapField(1);"  />&nbsp;Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span><input type="text" name="txtGCap" id="txtGCap" style="width:90px;" class="input-text" autocomplete="off" readonly value="" /></td>
                                <td class="input-text" id="tankgasnot2"><input name="rdoCap" id="rdoCap2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoCap == 2) echo "checked"; ?> onClick="showCapField(2);" />&nbsp;Bale&nbsp;Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span><input type="text" name="txtBCap" id="txtBCap" style="width:90px;" class="input-text" autocomplete="off" readonly value="" disabled="disabled" /></td>
                                <td class="input-text" id="tankgasnot3">SF<span style="font-size:10px; font-style:italic;">(ft3/lt)</span><input type="text" name="txtSF" id="txtSF"  autocomplete="off" style="width:90px;" class="input-text" value=""  onkeyup="getTotalDWT(),getTotalDWT1()" /></td>
                                <td class="input-text" id="tankgasnot4">Loadable<span style="font-size:10px; font-style:italic;">(MT)</span><input type="text" name="txtLoadable" id="txtLoadable" autocomplete="off" style="width:90px;" class="input-text" readonly value="" /></td>
                           </tr>
                       </table>
                         <table cellpadding="1" cellspacing="1" border="0" width="100%"  class='tablesorter' >
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
                        <tr>
						<td width="25%" align="left" class="text">GRT&nbsp;:&nbsp;<input type="text" name="txtGNRT" id="txtGNRT" style="width:190px;" class="input-text" readonly value="" /></td>
						<td width="25%" align="left" valign="top" class="text">LOA&nbsp;:&nbsp;<input type="text" name="txtLOA" id="txtLOA" style="width:190px;" class="input-text" readonly value="" /></td>
                        <td width="25%" align="left" valign="top" class="text">Built Year&nbsp;:&nbsp;<input type="text" name="txtBuiltYear" id="txtBuiltYear" class="input-text" readonly value="" /></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						</tr>
                        <tr>
						<td width="25%" align="left" class="text">BEAM(m)<br/><input type="text" name="txtBeam" id="txtBeam" class="input-text" readonly value="" /></td>
						<td width="25%" align="left" valign="top" class="text">TPC&nbsp;:&nbsp;<input type="text" name="txtTPC" id="txtTPC" style="width:190px;" class="input-text" readonly value="" /></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						</tr>
						<tr>
						<td width="25%" align="left" class="text" style="color:#dc631e;">Speed Data</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="25%" align="left" class="input-text">Ballast Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBFullSpeed" id="txtBFullSpeed" style="width:190px;" class="input-text" value="" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBEcoSpeed1" id="txtBEcoSpeed1" style="width:190px;" class="input-text"  value=""  placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBEcoSpeed2" id="txtBEcoSpeed2" style="width:190px;" class="input-text"  value=""  placeholder="0.00"/></td>
						</tr>
						<tr>
						<td width="25%" align="left" class="input-text">Laden Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLFullSpeed" id="txtLFullSpeed" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLEcoSpeed1" id="txtLEcoSpeed1" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLEcoSpeed2" id="txtLEcoSpeed2" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						</tr>
						</tbody>
						</table>
						</td></tr>
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">FO Consumption MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBFOFullSpeed" id="txtBFOFullSpeed" style="width:190px;" class="input-text"  value=""  placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBFOEcoSpeed1" id="txtBFOEcoSpeed1" style="width:190px;" class="input-text"  value=""  placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtBFOEcoSpeed2" id="txtBFOEcoSpeed2" style="width:190px;" class="input-text"  value=""  placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLFOFullSpeed" id="txtLFOFullSpeed" style="width:190px;" class="input-text"  value=""  placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLFOEcoSpeed1" id="txtLFOEcoSpeed1" style="width:190px;" class="input-text"  value=""  placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtLFOEcoSpeed2" id="txtLFOEcoSpeed2" style="width:190px;" class="input-text"  value=""  placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtPIFOFullSpeed" id="txtPIFOFullSpeed" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtPWFOFullSpeed" id="txtPWFOFullSpeed" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						
						</tbody>
						</table>
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">DO Consumption per MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBDOFullSpeed" id="txtBDOFullSpeed" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBDOEcoSpeed1" id="txtBDOEcoSpeed1" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtBDOEcoSpeed2" id="txtBDOEcoSpeed2" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLDOFullSpeed" id="txtLDOFullSpeed" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLDOEcoSpeed1" id="txtLDOEcoSpeed1" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtLDOEcoSpeed2" id="txtLDOEcoSpeed2" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtPIDOFullSpeed" id="txtPIDOFullSpeed" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtPWDOFullSpeed" id="txtPWDOFullSpeed" style="width:190px;" class="input-text"  value="" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						</tbody>
						</table>
						
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						</table>
                    </div>
                    <!-- /.tab_2-pane -->
                    <div id="tabs_4" class="tab-pane">
                        <table class='tablesorter' width="100%">
                            <tr><td><strong style="font-size:12px;">Planned Cargo</strong></td></tr>    
                        </table>
                        <table width="100%">
                        <tr class="input-text">
                            <td>CP ID : </td>
                            <td><input type="text" name="txtCID" id="txtCID" class="input-text" size="21" placeholder="Cargo ID" readonly autocomplete="off" value=""/></td>
                            <td>Shipper : </td>
                            <td><select  name="selShipperCP" class="input-text" style="width:137px;" id="selShipperCP" ><?php $obj->getVendorListNewForCOA(15);?></select></td>
                            <td>Charterer : </td>
                            <td><select  name="selChartererCP" class="input-text" style="width:137px;" id="selChartererCP" ><?php $obj->getVendorListNewForCOA(7);?></select></td>
                            <td>Owner :</td>
                            <td><select  name="selOwnerCP" class="input-text" style="width:137px;" id="selOwnerCP" ><?php $obj->getVendorListNewForCOA(11);?></select></td>
                        </tr>
                        <tr class="input-text">
                        	<td>Receiver : </td>
                            <td><select  name="selReceiverCP" class="input-text" style="width:137px;" id="selReceiverCP" ><?php $obj->getVendorListNewUpdate();?></select></td>
                            <td>Cargo :</td>
                            <td><select  name="selCargoCP" class="input-text" style="width:137px;" id="selCargoCP"><?php $obj->getCargoNameListForMultiple($_SESSION['selBType']);?></select></td>
                            <td>Cargo Stem Size(MT) :</td>
                            <td><input type="text" name="txtQtyCP" id="txtQtyCP" class="input-text" readonly size="21"  placeholder="" autocomplete="off" value=""/></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="input-text">
                            <td>Tolerance (+/- %) : </td>
                            <td><input type="text" name="txtToleranceCP" id="txtToleranceCP" readonly class="input-text" size="21"  placeholder="" autocomplete="off" value=""/></td>
                            <td>Base Freight (<?php echo $currencttoshow;?>/MT) : </td>
                            <td><input type="text" name="txtBaseFreightCP" id="txtBaseFreightCP" readonly class="input-text" size="21"  placeholder="" autocomplete="off" value=""/></td>
                            <td>COA/Spot :</td>
                            <td> <select  name="selPlannintTypeCP" class="input-text" style="width:137px;" id="selPlannintTypeCP"><?php $obj->getCOASpotList();?></select></td>
                            <td>COA date : </td>
                            <td><input type="text" name="txtCOADateCP" id="txtCOADateCP" class="input-text" readonly size="21" placeholder="dd-mm-yyyy" autocomplete="off" value=""/></td>
                        </tr>
                        <tr class="input-text">
                        	<td>Basin : </td>
                            <td><select  name="selBaseInCP" class="input-text" style="width:137px;" id="selBaseInCP" ><?php $obj->getBaseinList();?></select></td>
                            <td>Bunker Hedge :</td>
                            <td><select  name="selBunkerHedgeCP" class="input-text" style="width:137px;" id="selBunkerHedgeCP"><?php $obj->getLoader();?></select></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="input-text">
                            <td>Load Port : </td>
                            <td><select  name="selLPortCP" class="input-text" style="width:137px;" id="selLPortCP"></select></td>
                            <td>Discharge Port : </td>
                            <td><select  name="selDPortCP" class="input-text" style="width:137px;" id="selDPortCP"></select></td>
                            <td>LayCan Start Date :</td>
                            <td><input type="text" name="txtLCSDateCP" id="txtLCSDateCP" readonly class="input-text" size="21" autocomplete="off" value=""/></td>
                            <td>LayCan Finish Date : </td>
                            <td><input type="text" name="txtLCFDateCP" id="txtLCFDateCP" readonly class="input-text" size="21" autocomplete="off" value=""/></td>
                             <script>$("#selLPortCP,#selDPortCP").html($("#selPort").html());</script>
                        </tr>
                        <tr class="input-text">
                            <td>Cargo Relet/Voyage : </td>
                            <td><select  name="selCargoReletCP" class="input-text" style="width:137px;" id="selCargoReletCP"><?php $obj->getBusinessTypeForCargoPlanning('');?></select></td>
                            <td>Nom Clause : </td>
                            <td><input type="text" name="txtNomClauseCP" id="txtNomClauseCP" readonly class="input-text" size="21" autocomplete="off" value=""/></td>
                            <td>Remarks :</td>
                            <td><textarea class="input-text" name="txtRemarksCP" id="txtRemarksCP" rows="2" cols="21" readonly placeholder="Remarks" ></textarea></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </table>
                        <table class='tablesorter' width="100%">
							<tr><td><strong style="font-size:12px;">Cargo Intake Calculations</strong></td></tr>    
					    </table>
                        <table width="100%">
                            <tr class="input-text">
                               <td>Summer DWT(MT):</td>
                               <td><input type="text" name="txtSDWTMT" id="txtSDWTMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun74();?>"/></td>
                               <td>Summer DWT(LT):</td>
                               <td><input type="text" name="txtSDWTLT" id="txtSDWTLT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun75();?>"/></td>
                               <td>Summer Draft(M):</td>
                               <td><input type="text" name="txtSDraftM" id="txtSDraftM" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun76();?>"/></td>
                               <td>Summer Draft(FT):</td>
                               <td><input type="text" name="txtSDraftFT" id="txtSDraftFT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun77();?>"/></td>
                            </tr>
                            <tr class="input-text">
                               <td>TPI(MT/Inch):</td>
                               <td><input type="text" name="txtTPIMT" id="txtTPIMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun78();?>"/></td>
                               <td>TPI(LT/Inch):</td>
                               <td><input type="text" name="txtTPILT" id="txtTPILT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun79();?>"/></td>
                               <td>TPC(MT):</td>
                               <td><input type="text" name="txtTPCMT" id="txtTPCMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun80();?>"/></td>
                               <td>TPC(LT):</td>
                               <td><input type="text" name="txtTPCLT" id="txtTPCLT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun81();?>"/></td>
                            </tr>
                            <tr class="input-text">
                               <td>Constants(MT):</td>
                               <td><input type="text" name="txtConstantsMT" id="txtConstantsMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun82();?>"/></td>
                               <td>Constants(LT):</td>
                               <td><input type="text" name="txtConstantsLT" id="txtConstantsLT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun83();?>"/></td>
                               <td>Grain Cap(CBM):</td>
                               <td><input type="text" name="txtGrainCapCBM" id="txtGrainCapCBM" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun84();?>"/></td>
                               <td>Grain Cap(CFT):</td>
                               <td><input type="text" name="txtGrainCapCFT" id="txtGrainCapCFT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun85();?>"/></td>
                            </tr>
                        
							<tr><td colspan="8"><strong style="font-size:12px;">Basis Max Draft in Port</strong></td></tr>    
					    
                            <tr class="input-text">
                               <td>Allowed Draft(M):</td>
                               <td><input type="text" name="txtAllawedDraftM" id="txtAllawedDraftM" class="input-text numeric" size="21" autocomplete="off" value="<?php echo $obj->getFun86();?>" placeholder="0.00" onKeyUp="getIntakeCalculation();"/></td>
                               <td>Bunker ROB(MT):</td>
                               <td><input type="text" name="txtBunkerRobMT" id="txtBunkerRobMT" class="input-text numeric" size="21" autocomplete="off" value="<?php echo $obj->getFun87();?>" placeholder="0.00" onKeyUp="getIntakeCalculation();"/></td>
                               <td>Cargo Intake(MT) :</td>
                               <td><input type="text" name="txtCargoIntakeMT" id="txtCargoIntakeMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun88();?>"/></td>
                               <td></td>
                               <td></td>
                            </tr>
                        
							<tr><td colspan="8"><strong style="font-size:12px;">Basis Stowage Factor</strong></td></tr>    
					    
                            <tr class="input-text">
                               <td>SF(CBM/MT):</td>
                               <td><input type="text" name="txtSFCBM_MT" id="txtSFCBM_MT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun89();?>"/></td>
                               <td>SF(CBFT/MT):</td>
                               <td><input type="text" name="txtSFCBFT_MT" id="txtSFCBFT_MT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun90();?>"/></td>
                               <td>Cargo Loadable (MT) :</td>
                               <td><input type="text" name="txtCargoLoadableMT" id="txtCargoLoadableMT" readonly class="input-text" size="21" autocomplete="off" value="<?php echo $obj->getFun91();?>"/></td>
                               <td></td>
                               <td></td>
                            </tr>
                        </table>
                    </div>
                    </div><!-- /.tab-content -->
                </div>
                <div style="text-align:center;">
                <?php if($obj->getFun4()==0 && in_array(2, $rigts)){?>
                            <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit to Edit</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-warning btn-flat" onClick="return getValidate(1);">Send to Checker</button>
                <?php }?>
                <?php if($obj->getFun4()==1 && in_array(3, $rigts)){?>
                            <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Send to Creator</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-warning btn-flat" onClick="return getValidate(2);">Send to Ops</button>
                <?php }?>
                            <input type="hidden" name="txtCRMFILE" id="txtCRMFILE" value="" />
                            <input type="hidden" name="txtCRMNAME" id="txtCRMNAME" value="" />
                            <input type="hidden" name="txtCRMFILE1" id="txtCRMFILE1" value="" />
                            <input type="hidden" name="txtCRMNAME1" id="txtCRMNAME1" value="" />
							<input type="hidden" name="action" id="action" value="submit" />
                            <input type="hidden" name="vesselrec" id="vesselrec" class="input-text" value="" />
                            <input type="hidden" name="txtStatus" id="txtStatus" class="input-text" value="0" />
                </div>
                <!-- Main content -->
                
                </form>
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/sort.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />
<script src="../../js/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" href="../../css/chosen.css" rel="stylesheet" />
<script type="text/javascript">
$(document).ready(function()
{
$("#selShipperCP,#selChartererCP,#selOwnerCP,#selReceiverCP").html($("#selVendor").html());
$("#selVName").val(<?php echo $obj->getFun5();?>);
$("#selCargoPlanning").val(<?php echo $obj->getFun3();?>);
$("#frm1").validate({
	rules: {
		txtDate:"required",
		txtENo:"required",
		selCharterers_1:"required",
		txtCargomt_1:"required",
		txtRateUSDmt_1:"required",
		selVName:"required",
		txtVNo:"required"
		},
	messages: {
		selVName:"*",
		},
       submitHandler: function(form)  {
		if($("#txtStatus").val()==1)
		{
		   jConfirm('Are you sure you want to send this estimate to ops ?', 'Confirmation', function(r) {
			if(r){
				jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
				$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
				$("#popup_content").css({"background":"none","text-align":"center"});
				$("#popup_ok,#popup_title").hide();  
				frm1.submit();
				}
				else{return false;}
			});
		}
		else
		{
		   jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
			if(r){
				var var3 = $("[id^=file_]").map(function () {return this.value;}).get().join(",");
				$('#txtCRMFILE').val(var3);
				var var2 = $("[id^=name_]").map(function () {return this.value;}).get().join(",");
				$('#txtCRMNAME').val(var2);
				var var3 = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
				$('#txtCRMFILE1').val(var3);
				var var2 = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
				$('#txtCRMNAME1').val(var2);
				jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
				$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
				$("#popup_content").css({"background":"none","text-align":"center"});
				$("#popup_ok,#popup_title").hide();  
				frm1.submit();
				}
				else{return false;}
			});
		}
	}
});

$('#txtApplicableFrom').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
	}).on('changeDate', function(){
	$('#txtApplicableTo').datepicker('setStartDate', new Date(getString($(this).val())));
});

$('#txtApplicableTo').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
}).on('changeDate', function(){   });

$('#txtApplicableFromout').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
	}).on('changeDate', function(){
	$('#txtApplicableToout').datepicker('setStartDate', new Date(getString($(this).val())));
});

$('#txtApplicableToout').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
}).on('changeDate', function(){   });

$(".numeric").numeric();

$("#txtDate").datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
});
getData();
getCargoPlanningdata();
});

function getValue()
{
	
}

function getString(var1)
{
  var var2 = var1.split('-');  
  return var2[2]+'/'+var2[1]+'/'+var2[0];
}


function getData()
{
	  if($("#selVName").val()!="")
	  {
		  $.post("options.php?id=42",{vessel1d:""+$("#selVName").val()+""}, function(data) 
			{
				    var vsldata = JSON.parse(data);
					$.each(vsldata[$("#selVName").val()], function(index, array) {
					if(array['num'] == "" || array['num']==0)
					{
						jAlert('Please ensure commercial parameters are completed before proceeding', 'Alert', function(r) {
						if(r){ 
							window.location="commercial_parameter.php?id="+$("#selVName").val();
							}
							else{return false;}
							});	
					}
					else
					{
						$('#txtVType').val(array['type']);
						$('#txtDWTS').val(array['dwtsum']);
						$('#txtGCap').val(array['grain']);
						$('#txtBCap').val(array['bale']);
						
						$('#txtBFullSpeed').val(array['bfs']);
						$('#txtBEcoSpeed1').val(array['bes1']);
						$('#txtBEcoSpeed2').val(array['bes2']);
						$('#txtBFOFullSpeed').val(array['fobfs']);
						$('#txtBFOEcoSpeed1').val(array['fobes1']);
						$('#txtBFOEcoSpeed2').val(array['fobes2']);
						$('#txtBDOFullSpeed').val(array['dobfs']);
						$('#txtBDOEcoSpeed1').val(array['dobes1']);
						$('#txtBDOEcoSpeed2').val(array['dobes2']);
						
						$('#txtLFullSpeed').val(array['lfs']);
						$('#txtLEcoSpeed1').val(array['les1']);
						$('#txtLEcoSpeed2').val(array['les2']);
						$('#txtLFOFullSpeed').val(array['folfs']);
						$('#txtLFOEcoSpeed1').val(array['foles1']);
						$('#txtLFOEcoSpeed2').val(array['foles2']);
						$('#txtLDOFullSpeed').val(array['dolfs']);
						$('#txtLDOEcoSpeed1').val(array['doles1']);
						$('#txtLDOEcoSpeed2').val(array['doles2']);
						
						$('#txtPIFOFullSpeed').val(array['foidle']);
						$('#txtPWFOFullSpeed').val(array['fwking']);
						$('#txtPIDOFullSpeed').val(array['ddle']);
						$('#txtPWDOFullSpeed').val(array['dwking']);
						$('#vesselrec').val(array['num']);
						$('#txtFlag').val(array['flag']);
						$('#txtBuiltYear').val(array['year']);
						$('#txtGNRT').val(array['gnrt']);
						$('#txtLOA').val(array['loa']);
						$('#txtGear').val(array['gear']);
						$('#txtBeam').val(array['beam']);
						$('#txtTPC').val(array['tpc']);
						
						$('#txtSDWTMT').val(array['SDWTMT']);
						$('#txtSDWTLT').val(array['SDWTLT']);
						$('#txtSDraftM').val(array['SDraftM']);
						$('#txtSDraftFT').val(array['SDraftFT']);
						$('#txtTPIMT').val(array['TPIMT']);
						$('#txtTPILT').val(array['TPILT']);
						$('#txtTPCMT').val(array['TPCMT']);
						$('#txtTPCLT').val(array['TPCLT']);
						$('#txtConstantsMT').val(array['ConstantsMT']);
						$('#txtConstantsLT').val(array['ConstantsLT']);
						$('#txtGrainCapCBM').val(array['GrainCapCBM']);
						$('#txtGrainCapCFT').val(array['GrainCapCFT']);
						
					}
				});
			});  
	  }
	  else
	  {
		  $('#txtVType, #txtDWTS, #txtGCap, #txtBCap, #txtBFullSpeed, #txtBEcoSpeed1, #txtBEcoSpeed2, #txtBFOFullSpeed, #txtBFOEcoSpeed1, #txtBFOEcoSpeed2, #txtBDOFullSpeed, #txtBDOEcoSpeed1, #txtBDOEcoSpeed2, #txtLFullSpeed, #txtLEcoSpeed1, #txtLEcoSpeed2, #txtLFOFullSpeed, #txtLFOEcoSpeed1, #txtLFOEcoSpeed2, #txtLDOFullSpeed, #txtLDOEcoSpeed1, #txtLDOEcoSpeed2, #txtPIFOFullSpeed, #txtPWFOFullSpeed, #txtPIDOFullSpeed, #txtPWDOFullSpeed, #vesselrec, #txtFlag, #txtGear, #txtBuiltYear, #txtGNRT, #txtLOA, #rdoEstimateType, #txtSDWTMT, #txtSDWTLT, #txtSDraftM, #txtSDraftFT, #txtTPIMT, #txtTPILT, #txtTPCMT, #txtTPCLT, #txtConstantsMT, #txtConstantsLT, #txtGrainCapCBM, #txtGrainCapCFT').val("");
	  }
}


function getValidate(val)
{
	$("#txtStatus").val(val);
	var var3 = $("[id^=file_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRMFILE').val(var3);
	var var2 = $("[id^=name_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRMNAME').val(var2);
	var var3 = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRMFILE1').val(var3);
	var var2 = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRMNAME1').val(var2);
	return true;
}

function getCalculation()
{
	var txtFreightUSD  = $("#txtFreightUSD").val();if(isNaN(txtFreightUSD)){txtFreightUSD = 0;}
	var txtBAFUSD      = $("#txtBAFUSD").val();if(isNaN(txtBAFUSD)){txtBAFUSD = 0;}
	var txtAddComm     = $("#txtAddComm").val();if(isNaN(txtAddComm)){txtAddComm = 0;}
	var txtBrokerage   = $("#txtBrokerage").val();if(isNaN(txtBrokerage)){txtBrokerage = 0;}
	var txtActualQty   = $("#txtActualQty").val();if(isNaN(txtActualQty)){txtActualQty = 0;}
	var txtFreightAmt  = parseFloat(txtFreightUSD)*parseFloat(txtActualQty);if(isNaN(txtFreightAmt)){txtFreightAmt = 0;}
	var txtAddCommAmt  = (parseFloat(txtFreightAmt)*parseFloat(txtAddComm))/100;if(isNaN(txtAddCommAmt)){txtAddCommAmt = 0;}
	var txtBrokerageAmt  = (parseFloat(txtFreightAmt)*parseFloat(txtBrokerage))/100;if(isNaN(txtBrokerageAmt)){txtBrokerageAmt = 0;}
	$("#txtFreightAmt").val(txtFreightAmt.toFixed(2));
	$("#txtAddCommAmt").val(txtAddCommAmt.toFixed(2));
	$("#txtBrokerageAmt").val(txtBrokerageAmt.toFixed(2));
	
	var txtConFOPrice  = $("#txtConFOPrice").val();if(isNaN(txtConFOPrice)){txtConFOPrice = 0;}
	var txtCurFOPrice  = $("#txtCurFOPrice").val();if(isNaN(txtCurFOPrice)){txtCurFOPrice = 0;}
	var fodiff = parseFloat(txtCurFOPrice) - parseFloat(txtConFOPrice);if(isNaN(fodiff)){fodiff = 0;}
	var txtBunkerSurcharge = parseFloat(txtActualQty)*parseFloat(fodiff)*parseFloat(txtBAFUSD); if(isNaN(txtBunkerSurcharge)){txtBunkerSurcharge = 0;}
	$("#txtBunkerSurcharge").val(txtBunkerSurcharge.toFixed(2));
	
	var txtTotals = parseFloat($("#txtFreightAmt").val()) + parseFloat($("#txtBunkerSurcharge").val()) - parseFloat($("#txtAddCommAmt").val()) - parseFloat($("#txtBrokerageAmt").val());if(isNaN(txtTotals)){txtTotals = 0;}
	$("#txtTotals").val(txtTotals.toFixed(2));
	
	var txtFreightUSDout = $("#txtFreightUSDout").val();if(isNaN(txtFreightUSDout)){txtFreightUSDout = 0;}
	var txtAddCommout    = $("#txtAddCommout").val();if(isNaN(txtAddCommout)){txtAddCommout = 0;}
	var txtBrokerageout  = $("#txtBrokerageout").val();if(isNaN(txtBrokerageout)){txtBrokerageout = 0;}
	var txtBAFUSDout     = $("#txtBAFUSDout").val();if(isNaN(txtBAFUSDout)){txtBAFUSDout = 0;}
	var txtFreightAmtout = parseFloat(txtFreightUSDout)*parseFloat(txtActualQty);if(isNaN(txtFreightAmtout)){txtFreightAmtout = 0;}
	var txtAddCommAmtout  = (parseFloat(txtFreightAmtout)*parseFloat(txtAddCommout))/100;if(isNaN(txtAddCommAmtout)){txtAddCommAmtout = 0;}
	var txtBrokerageAmtout  = (parseFloat(txtFreightAmtout)*parseFloat(txtBrokerageout))/100;if(isNaN(txtBrokerageAmtout)){txtBrokerageAmtout = 0;}
	$("#txtFreightAmtout").val(txtFreightAmtout.toFixed(2));
	$("#txtAddCommAmtout").val(txtAddCommAmtout.toFixed(2));
	$("#txtBrokerageAmtout").val(txtBrokerageAmtout.toFixed(2));
	
	var txtTotalsout = parseFloat($("#txtFreightAmtout").val()) - parseFloat($("#txtAddCommAmtout").val()) - parseFloat($("#txtBrokerageAmtout").val());
	if(isNaN(txtTotalsout)){txtTotalsout = 0;} 
	$("#txtTotalsout").val(txtTotalsout.toFixed(2));
	
	
	var txtProfit = parseFloat($("#txtTotals").val()) - parseFloat($("#txtTotalsout").val());if(isNaN(txtProfit)){txtProfit = 0;}
	$("#txtProfit").val(txtProfit.toFixed(2)); 
}

function getIntakeCalculation()
{
	var txtAllawedDraftM  = parseFloat($("#txtAllawedDraftM").val());if(isNaN(txtAllawedDraftM)){txtAllawedDraftM = 0;}
	var txtSDraftM        = parseFloat($("#txtSDraftM").val());if(isNaN(txtSDraftM)){txtSDraftM = 0;}
	var txtSDWTMT         = parseFloat($("#txtSDWTMT").val());if(isNaN(txtSDWTMT)){txtSDWTMT = 0;}
	var txtTPCMT          = parseFloat($("#txtTPCMT").val());if(isNaN(txtTPCMT)){txtTPCMT = 0;}
	var txtConstantsMT    = parseFloat($("#txtConstantsMT").val());if(isNaN(txtConstantsMT)){txtConstantsMT = 0;}
	var txtBunkerRobMT    = parseFloat($("#txtBunkerRobMT").val());if(isNaN(txtBunkerRobMT)){txtBunkerRobMT = 0;}
	
	var diffdrafts = parseFloat(txtSDraftM) - parseFloat(txtAllawedDraftM);if(isNaN(diffdrafts)){diffdrafts = 0;}
	var diffdraftsmt = parseFloat(diffdrafts)*100;if(isNaN(diffdraftsmt)){diffdraftsmt = 0;}
	
	var DWTdiffmt = parseFloat(diffdraftsmt)*parseFloat(txtTPCMT);if(isNaN(DWTdiffmt)){DWTdiffmt = 0;}
	
	var cargointakemt = parseFloat(txtSDWTMT) - parseFloat(DWTdiffmt) - parseFloat(txtBunkerRobMT) - parseFloat(txtConstantsMT);if(isNaN(cargointakemt)){cargointakemt = 0;}
	if(txtAllawedDraftM == 0 || txtAllawedDraftM=='')
	{
		$("#txtCargoIntakeMT").val(0);
		$("#txtActualQty").val($('#txtQtyCP').val());
		
	}
	else
	{
		$("#txtCargoIntakeMT,#txtActualQty").val(cargointakemt.toFixed(4));
	}
	
	getCalculation();
}

function addpartiesDetails()
{
	var id = $("#txtPartiesID").val();
	if($("#selCharterers_"+id).val() != "" && $("#selOwner_"+id).val() != "" && $("#selBroker_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="partiesrow_'+id+'"><td width="50px;"><a href="#tb1" onClick="deletepartiesDetails('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select name="selCharterers_'+id+'" class="input-text" style="width:130px;" id="selCharterers_'+id+'"></select></td><td><select name="selOwner_'+id+'" class="input-text" style="width:130px;" id="selOwner_'+id+'"></select></td><td><select name="selBroker_'+id+'" class="input-text" style="width:130px;" id="selBroker_'+id+'"></select></td></tr>').appendTo("#tbodypartis");
		$("#txtPartiesID").val(id);
		$("#selCharterers_"+id+",#selOwner_"+id+",#selBroker_"+id).html($("#selVendor").html());
	}
	else
	{
		jAlert('Please fill All details', 'Alert');
		return false;
	}
}


function deletepartiesDetails(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#partiesrow_"+var1).remove();
				 }
			else{return false;}
			});
}


function addpartiesDetailsout()
{
	var id = $("#txtPartiesoutID").val();
	if($("#selCharterersout_"+id).val() != "" && $("#selOwnerout_"+id).val() != "" && $("#selBrokerout_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="partiesrowout_'+id+'"><td width="50px;"><a href="#tb1" onClick="deletepartiesDetailsout('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select name="selCharterersout_'+id+'" class="input-text" style="width:130px;" id="selCharterersout_'+id+'"></select></td><td><select name="selOwnerout_'+id+'" class="input-text" style="width:130px;" id="selOwnerout_'+id+'"></select></td><td><select name="selBrokerout_'+id+'" class="input-text" style="width:130px;" id="selBrokerout_'+id+'"></select></td></tr>').appendTo("#tbodypartisout");
		$("#txtPartiesoutID").val(id);
		$("#selCharterersout_"+id+",#selOwnerout_"+id+",#selBrokerout_"+id).html($("#selVendor").html());
	}
	else
	{
		jAlert('Please fill All details', 'Alert');
		return false;
	}
}


function deletepartiesDetailsout(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#partiesrowout_"+var1).remove();
				 }
			else{return false;}
			});
}


function addlportDetails()
{
	var id = $("#txtLPortID").val();
	if($("#selLoadPort_"+id).val() != "" && $("#txtLComments_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="portlrow_'+id+'"><td width="50px;"><a href="#tb1" onClick="deleteLport('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select name="selLoadPort_'+id+'" class="input-text" style="width:130px;" id="selLoadPort_'+id+'"></select></td><td><textarea class="input-text required" name="txtLComments_'+id+'" id="txtLComments_'+id+'" rows="2" cols="25" placeholder="Comments"></textarea></td></tr>').appendTo("#tbodylport");
		$("#txtLPortID").val(id);
		$("#selLoadPort_"+id).html($("#selPort").html());
	}
	else
	{
		jAlert('Please fill All details', 'Alert');
		return false;
	}
}


function deleteLport(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#portlrow_"+var1).remove();
				 }
			else{return false;}
			});
}


function addlportDetailsout()
{
	var id = $("#txtLPortoutID").val();
	if($("#selLoadPortout_"+id).val() != "" && $("#txtLCommentsout_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="portlrowout_'+id+'"><td width="50px;"><a href="#tb1" onClick="deleteLportout('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select name="selLoadPortout_'+id+'" class="input-text" style="width:130px;" id="selLoadPortout_'+id+'"></select></td><td><textarea class="input-text required" name="txtLCommentsout_'+id+'" id="txtLCommentsout_'+id+'" rows="2" cols="25" placeholder="Comments"></textarea></td></tr>').appendTo("#tbodylportout");
		$("#txtLPortoutID").val(id);
		$("#selLoadPortout_"+id).html($("#selPort").html());
	}
	else
	{
		jAlert('Please fill All details', 'Alert');
		return false;
	}
}


function deleteLportout(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#portlrowout_"+var1).remove();
				 }
			else{return false;}
			});
}





function adddportDetails()
{
	var id = $("#txtDPortID").val();
	if($("#selDisPort_"+id).val() != "" && $("#txtDComments_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="portdrow_'+id+'"><td width="50px;"><a href="#tb1" onClick="deleteDport('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select name="selDisPort_'+id+'" class="input-text" style="width:130px;" id="selDisPort_'+id+'"></select></td><td><textarea class="input-text required" name="txtDComments_'+id+'" id="txtDComments_'+id+'" rows="2" cols="25" placeholder="Comments"></textarea></td></tr>').appendTo("#tbodydport");
		$("#txtDPortID").val(id);
		$("#selDisPort_"+id).html($("#selPort").html());
	}
	else
	{
		jAlert('Please fill All details', 'Alert');
		return false;
	}
}


function deleteDport(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#portdrow_"+var1).remove();
				 }
			else{return false;}
			});
}


function adddportDetailsout()
{
	var id = $("#txtDPortoutID").val();
	if($("#selDisPortout_"+id).val() != "" && $("#txtDCommentsout_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="portdrowout_'+id+'"><td width="50px;"><a href="#tb1" onClick="deleteDportout('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select name="selDisPortout_'+id+'" class="input-text" style="width:130px;" id="selDisPortout_'+id+'"></select></td><td><textarea class="input-text required" name="txtDCommentsout_'+id+'" id="txtDCommentsout_'+id+'" rows="2" cols="25" placeholder="Comments"></textarea></td></tr>').appendTo("#tbodydportout");
		$("#txtDPortoutID").val(id);
		$("#selDisPortout_"+id).html($("#selPort").html());
	}
	else
	{
		jAlert('Please fill All details', 'Alert');
		return false;
	}
}


function deleteDportout(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#portdrowout_"+var1).remove();
				 }
			else{return false;}
			});
}


function getCargoPlanningdata()
{
	if($("#selCargoPlanning").val()!="")
	  {
		  $.post("options.php?id=72",{selCargoPlanning:""+$("#selCargoPlanning").val()+""}, function(data) 
			{
				var vsldata = JSON.parse(data);
				$('#txtCID').val(vsldata['CPID']);
				$('#selShipperCP').val(vsldata['SHIPPER']);
				$('#selChartererCP').val(vsldata['CHARTERER']);
				$('#selOwnerCP').val(vsldata['OWNER']);
				$('#selReceiverCP').val(vsldata['RECEAVER']);
				$('#selCargoCP').val(vsldata['CARGO']);
				$('#txtQtyCP').val(vsldata['CARGOSTEAMSIZE']);
				$('#txtToleranceCP').val(vsldata['TOLERANCE']);
				$('#txtBaseFreightCP').val(vsldata['BASE_FREIGHT']);
				$('#selPlannintTypeCP').val(vsldata['COA_SPOT']);
				$('#txtCOADateCP').val(vsldata['COA_DATE']);
				$('#selBaseInCP').val(vsldata['BASINID']);
				$('#selBunkerHedgeCP').val(vsldata['BUNKER_HEDGE']);
				$('#selLPortCP').val(vsldata['LOAD_PORT']);
				$('#selDPortCP').val(vsldata['DISCHARGE_PORT']);
				$('#txtLCSDateCP').val(vsldata['LAYCAN_SDATE']);
				$('#txtLCFDateCP').val(vsldata['LAYCAN_EDATE']);
				$('#selCargoReletCP').val(vsldata['CARGO_RELET']);
				$('#txtNomClauseCP').val(vsldata['NOMCLAUSE']);
				$('#txtRemarksCP').val(vsldata['REMARKS']);
				$("#cargonamspan").text($("#selCargoCP option:selected").text());
				
				getIntakeCalculation();
			});  
	  }
	  else
	  {
		  $('#txtCID, #selShipperCP, #selChartererCP, #selOwnerCP, #selReceiverCP, #selCargoCP, #txtQtyCP, #txtToleranceCP, #selPlannintTypeCP, #txtCOADateCP, #selBaseInCP, #selBunkerHedgeCP, #selLPortCP, #selDPortCP, #txtLCSDateCP, #txtLCFDateCP, #selCargoReletCP, #txtNomClauseCP, #txtRemarksCP').val("");
		  getIntakeCalculation();
	  }
	 
}


function Del_Upload1(var1)
{
	jConfirm('Are you sure you want to remove this attachment permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_file1_"+var1).remove();	
		 }
	else{return false;}
	});
}

function Del_Upload(var1)
{
	jConfirm('Are you sure you want to remove this attachment permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_file_"+var1).remove();	
		 }
	else{return false;}
	});
}

</script>
    </body>
</html>