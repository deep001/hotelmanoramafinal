<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = @$_REQUEST['id'];
if (@$_REQUEST['action'] == 'submit')
 {
 	if (@$_REQUEST['txtStatus'] == '1')
 	{
 		$msg = $obj->deleteVesselOpenPositionsAttachment();
		header('Location:./updateVesselOpenPosition.php?id='.$_REQUEST['id'].'&msg='.$msg);
	}
	else if (@$_REQUEST['txtStatus'] == '2')
 	{
 		$msg = $obj->updateVesselOpenPositionDetails();
		header('Location:./open_vessel_position.php?msg='.$msg);
	}
	
 }
$obj->viewVesselOpenPositionRecords($id);
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
$msg = NULL;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rigts = $obj->getUserRights($uid,$moduleid,1);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Open Vessel Position</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b>Invoice Status List added/updated successfully.
				</div>
				<?php }}?>
				
				<!--   content put here..................-->
				<div align="right"><a href="open_vessel_position.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             UPDATE OPEN VESSEL POSITION    
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           Vessel Name
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselIMOData($obj->getFun1(),"VESSEL_NAME");?></strong>
                            </address>
                        </div><!-- /.col -->
                        <!--<div class="col-sm-6 invoice-col">
                           Business Source Entity
                            <address>
                            <?php
							/*$sql = "SELECT * FROM open_vessel_entry_master where OPENVESSEL_ID='".$id."'";
							$res = mysql_query($sql);
							$rows = mysql_fetch_assoc($res);
							if($rows['LOGINTYPE'] != 'External User')
							{*/
							?>
                                <select  name="selVendor" class="form-control" id="selVendor" >
									<?php 
									/*$_REQUEST['selVendor'] = $obj->getFun2();
                                    $obj->getVendorListNew();*/
                                    ?>
                                    </select>
                            <?php /*}else{
									$sql1 = "select USERNAME from login where LOGINID='".$rows['VENDORID']."'"; 
									$res1 = mysql_query($sql1);
									$rows1 = mysql_fetch_assoc($res1);*/
									?>
                                    <strong>&nbsp;&nbsp;&nbsp;<?php //echo strtoupper($rows1['USERNAME']);?></strong>
	                                <input type="hidden" name="selVendor" id="selVendor" class="input-text" size="12" readonly="true" value="<?php //echo $rows['VENDORID'];?>" />
                                 <?php //}?>
                            </address>
                        </div>  -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Port Open
                            <address>
                               <select  name="selPort" onChange="getZoneData();" class="form-control" id="selPort">
								<?php 
                                $obj->getPortListNew();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Zone Open
                            <address>
                               <select  name="selZone"  class="form-control" id="selZone">
								<?php 
								$_REQUEST['selZone'] = $obj->getFun4();
                                $obj->getZoneList();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                          COA / Spot
                            <address>
                                <select  name="selCOASpot" class="form-control" id="selCOASpot" onChange="getShow();">
								<?php 
								$_REQUEST['selCOASpot'] = $obj->getFun16();
                                $obj->getCOASpotList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                          Broker
                            <address>
                                <select  name="selBroker" class="form-control" id="selBroker">
								<?php 
								$obj->getVendorListNewForCOA(12);
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                       
					</div>
                    
                    <?php if($obj->getFun16() == 2){?>
				   
				   	 <div class="row invoice-info" id="tr_coa" >
                        <div class="col-sm-6 invoice-col">
                          COA Number
                            <address>
                               <select  name="selCOA" class="form-control" id="selCOA" onChange="getTotalShipments();">
								<?php 
								$_REQUEST['selCOA'] = $obj->getFun17();
                                $obj->getCOAList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                       <div class="col-sm-6 invoice-col">
                           Number of Lift
                            <address>
                               <input type="text" name="txtNoLift" id="txtNoLift" class="form-control"  placeholder=" Number of Lift" autocomplete="off" value="<?PHP echo (int)$obj->getFun18();?>"/>
                            </address>
                        </div><!-- /.col -->                        
					</div>
                    <div class="row invoice-info" id="tr_coa1">
                        <div class="col-sm-6 invoice-col">
                            Total No. of Shipments
                            <address>
                                <strong><span id="ttl_shipment" ></span></strong>
                             </address>
                        </div><!-- /.col -->
                    </div>
				   <?php }else{?>
				   
				    <div class="row invoice-info" id="tr_coa" style="display:none;">
                        <div class="col-sm-6 invoice-col">
                          COA Number
                            <address>
                               <select  name="selCOA" class="form-control" id="selCOA" onChange="getTotalShipments();">
								<?php 
								 $obj->getCOAList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                       <div class="col-sm-6 invoice-col">
                           Number of Lift
                            <address>
                               <input type="text" name="txtNoLift" id="txtNoLift" class="form-control"  placeholder=" Number of Lift" autocomplete="off" />
                            </address>
                        </div><!-- /.col -->                        
					</div>
                    
                    <div class="row invoice-info" id="tr_coa1" style="display:none;">
                        <div class="col-sm-6 invoice-col">
                            Total No. of Shipments
                            <address>
                                <strong><span id="ttl_shipment" ></span></strong>
                             </address>
                        </div><!-- /.col -->
                        
                    </div>
				   <?php }?>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Vessel Category
                            <address>
                               <select  name="selVCType" class="form-control" id="selVCType">
								<?php 
                                $obj->getVesselCategoryList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-6 invoice-col">
                             Expected Hire
                            <address>
                               <input type="text" name="txtexpectedhire" id="txtexpectedhire" class="form-control"  placeholder="Expected Hire" autocomplete="off" value="<?php echo $obj->getFun20();?>"/>
                            </address>
                        </div> 
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
								Owner
								<address>
									<select  name="selOwner" class="form-control" id="selOwner">
									<?php 
									$obj->getVendorListNewForCOA(11);
									?>
									</select>
								 </address>
						</div><!-- /.col -->
						<div class="col-sm-6 invoice-col">
								Disponent Owner 1
								<address>
								 <select  name="txtDisponentOwner" class="form-control" id="txtDisponentOwner">
									<?php 
									$obj->getVendorListNewForCOA(11);
									?>
								 </select>
								</address>
						 </div><!-- /.col -->
					</div>
					
					<?php 
					$j=0;
					$sql_disponent  = "select * from open_vessel_entry_disponent_owner where OPENVESSEL_ID='".$id."'";
					$disponent_res = mysql_query($sql_disponent);
					$disponent_rec = mysql_num_rows($disponent_res);
					if($disponent_rec > 0)
					{
						while($disponent_rows = mysql_fetch_assoc($disponent_res))
						{ $j = $j+1; 
					?>
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
						&nbsp;
						</div>
						<div class="col-sm-6 invoice-col">
								Disponent Owner <?php echo $j+1; ?>
								<address>
									<select  name="txtDisponentOwner_<?php echo $j; ?>" class="form-control" id="txtDisponentOwner_<?php echo $j; ?>">
									<?php
									$_REQUEST['txtDisponentOwner_'.$j] = $disponent_rows['DISPONENT_OWNER'];
									$obj->getVendorListNewForDisponentOwner(11,$j);
									?>
									</select>
								</address>
						 </div><!-- /.col -->
					</div>
					
					<?php }} ?>
					
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
						&nbsp;
						</div>
						<div class="col-sm-6 invoice-col" id="sort">
						&nbsp;
						</div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
						&nbsp;
						</div>
						<div class="col-sm-6 invoice-col">
							<address>
								<button type="button" onClick="addUI_Row(<?php echo $i+1;?>);" class="btn btn-primary btn-flat">Add</button><input type="hidden" class="input" name="txtTID_<?php echo $i+1;?>" id="txtTID_<?php echo $i+1;?>" value="<?php echo $j; ?>" /><input type="hidden" name="txttabid[]" id="txttabid_<?php echo $i+1;?>" class="input-text" value="" />
							</address>
						</div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           Laycan Start
                            <address>
                               <input type="text" name="txtFDate" id="txtFDate" class="form-control"  placeholder="Laycan Start" autocomplete="off" value="<?php echo date("d-m-Y",strtotime($obj->getFun5()));?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Laycan Finish
                            <address>
                                <input type="text" name="txtTDate" id="txtTDate" class="form-control"  placeholder="Laycan Finish" autocomplete="off" value="<?php echo date("d-m-Y",strtotime($obj->getFun6()));?>"/>
                             </address>
                        </div><!-- /.col -->
                       
					</div>
                   
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           ETA During Fixture
                            <address>
							<?php if(date("d-m-Y",strtotime($obj->getFun13())) == "01-01-1970"){$eta_date = "";}else{$eta_date = date("d-m-Y",strtotime($obj->getFun13()));}?>
                              <input type="text" name="txtETADate" id="txtETADate" class="form-control"  placeholder="ETA During Fixture" autocomplete="off" value="<?php echo $eta_date;?>"/>
                            </address>
                        </div><!-- /.col -->
						<?php if(date("d-m-Y",strtotime($obj->getFun14())) == "01-01-1970"){$cp_date = "";}else{$cp_date = date("d-m-Y",strtotime($obj->getFun14()));}?>
                        <div class="col-sm-6 invoice-col">
                           CP Date
                            <address>
                               <input type="text" name="txtCPDate" id="txtCPDate" class="form-control"  placeholder="CP Date" autocomplete="off" value="<?php echo $cp_date;?>"/>
                            </address>
                        </div><!-- /.col -->
                        
					</div>
                    
                    
                   
                    
                     <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                            Remarks
                            <address>
                               <textarea class="form-control areasize" name="txtNotes" id="txtNotes" rows="3" placeholder="Remarks ..." ><?php echo $obj->getFun7();?></textarea>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                           TC Description
                            <address>
                               <textarea class="form-control areasize" name="txtDirection" id="txtDirection" rows="3" placeholder="TC Description ..." ><?php echo $obj->getFun8();?></textarea>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    
                     <div class="row invoice-info">
                     
                     <?php if($obj->getFun15() != ''){ $file = explode(",",$obj->getFun15());
					 
					 $file = $name = "";
					 $file = explode(",",$obj->getFun15()); 
					 $name = explode(",",$obj->getFun22());
					 ?>
						<div class="col-sm-6 invoice-col">
                          Previous Attachments
                            <address>
									<table cellpadding="1" cellspacing="1" border="0" width="100%" align="left">
									<?php
									for($j=0;$j<sizeof($file);$j++)
									{
									?>
									<tr height="20" id="row_file_<?php echo $j;?>">
										<td width="50%" align="left" class="input-text"  valign="top"><a href="../../attachment/<?php echo $file[$j]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="Click to view file"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $name[$j];?></a>
                                        
                                        <input type="hidden" name="file_<?php echo $j;?>" id="file_<?php echo $j;?>" value="<?php echo $file[$j]; ?>" />
										<input type="hidden" name="name_<?php echo $j;?>" id="name_<?php echo $j;?>" value="<?php echo $name[$j]; ?>" />
                                        </td>
										<td width="50%" align="left" class="input-text"  valign="top"><a href="#1" onClick="Del_Upload(<?php echo $j;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
									</tr>
									<?php }?>
								</table>
                            </address>
                        </div><!-- /.col -->
						<?php }?>
                        
                        <div class="col-sm-6 invoice-col">
                         &nbsp;
							<address>
								<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip">
									<i class="fa fa-paperclip"></i> Attachment
									<input type="file" class="form-control" multiple name="attach_file[]" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                    
                                 
											<input type="hidden" name="txtCRMFILE" id="txtCRMFILE" value="" />
													<input type="hidden" name="txtCRMNAME" id="txtCRMNAME" value="" />
											<input type="hidden" name="txtUpStatus" id="txtUpStatus" value="0" />
								</div>
							</address>
                       </div><!-- /.col -->
                     </div>
					<?php
					if($rigts == 1)
						{
					 ?>
					<div class="box-footer" align="right">
						<input type="hidden" name="txtCRM1" id="txtCRM1" value="" />
						<input type="hidden" name="txtCRM2" id="txtCRM2" value="" />
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
						<input type="hidden" name="action" value="submit" /><input type="hidden" name="txtStatus" id="txtStatus" value="" />
					</div>
         			<?php } ?>
				</form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#selPort").val('<?php echo $obj->getFun3();?>');
$("#selOwner").val('<?php echo $obj->getFun11();?>');
$("#txtDisponentOwner").val('<?php echo $obj->getFun21();?>');
$("#selBroker").val('<?php echo $obj->getFun12();?>');
$("#selVCType").val('<?php echo $obj->getFun19();?>');
$("#txtNoLift").numeric();

$('#txtTDate,#txtFDate,#txtETADate,#txtCPDate,#txtLayDate').datepicker({
    format: 'dd-mm-yyyy',
	autoclose: true
});
$(".areasize").autosize({append: "\n"});
$("#frm1").validate({
rules: {
	selVName:"required",
	selCOASpot:"required",
	selBroker:"required",
	//txtETADate:"required",
	txtCPDate:"required",
	txtFDate:"required",
	txtTDate:"required",
	selOwner: "required",
	selVCType: "required",
	selCOA: {  required: function(element) {return $("#selCOASpot").val() == 2;}},
	txtNoLift:{  required: function(element) {return $("#selCOASpot").val() == 2;},digits:true}
	},
messages: {
	selVName:"*",
	selCOASpot:"*",
	selBroker:"*",
	//txtETADate:"*",
	txtCPDate:"*",
	txtFDate:"*",
	txtTDate:"*",
	selOwner:"*",
	selVCType: "*",
	selCOA : "*",
	txtNoLift : {  required: "*",digits:"*"}
	}
});	

var coa_list = <?php echo $obj->getCOAMasterDataJson();?>;
if($("#selCOA").val() != ""){
	$.each(coa_list[$("#selCOA").val()], function(index, array) {  
		$("#ttl_shipment").html("");
		$("#ttl_shipment").html(array['NO_OF_SHIPMENT']);
	});
}
}); 

function Del_Upload(var1)
{
	jConfirm('Are you sure you want to delete this upload permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file_'+var1).remove();
	}
	else{return false;}
	});
}

function getTotalShipmentsOnLoad()
{
	if($("#selCOA").val() != "")
	{
		var coa_list = <?php echo $obj->getCOAMasterDataJson();?>;
		$.each(coa_list[$("#selCOA").val()], function(index, array) {
				$("#ttl_shipment").html("");
				$("#selBroker,#selOwner,#txtCPDate").val("");
				$("#ttl_shipment").html(array['NO_OF_SHIPMENT']);
				$("#selBroker").val(array['BROKER']);
				$("#selOwner").val(array['OWNER']);
				$("#txtCPDate").val(array['COA_DATE']);
			});
	}
	else
	{
		$("#ttl_shipment").html("");
		$("#selBroker,#selOwner,#txtCPDate").val("");
	}
}

function getShow()
{
	if($("#selCOASpot").val() == 1 || $("#selCOASpot").val() == 3 || $("#selCOASpot").val() == 4)
	{
		$("#tr_coa,#tr_coa1").hide();
		$("#selOwner,#selCOA,#txtNoLift,#selBroker,#txtCPDate").val("");
		$("#ttl_shipment").html("");
	}
	else if($("#selCOASpot").val() == 2)
	{
		$("#selOwner,#selCOA,#txtNoLift,#selBroker,#txtCPDate").val("");
		$("#ttl_shipment").html("");
		$("#tr_coa,#tr_coa1").show();
	}
}

function getTotalShipments()
{
	if($("#selCOA").val() != "")
	{
		$("#txtNoLift").val("");
		var coa_list = <?php echo $obj->getCOAMasterDataJson();?>;
		$.each(coa_list[$("#selCOA").val()], function(index, array) {
				$("#ttl_shipment").html("");
				$("#selBroker,#selOwner,#txtCPDate").val("");
				$("#ttl_shipment").html(array['NO_OF_SHIPMENT']);
				$("#selBroker").val(array['BROKER']);
				$("#selOwner").val(array['OWNER']);
				$("#txtCPDate").val(array['COA_DATE']);
			});
	}
	else
	{
		$("#txtNoLift").val("");
		$("#ttl_shipment").html("");
		$("#selBroker,#selOwner,#txtCPDate").val("");
	}
}

function getZoneData()
{
	$("#selZone").val("");
	$.post("options.php?id=14",{selPort:""+$("#selPort").val()+""}, function(html) {
	$("#selZone").val(html);
	});
}

function getValidate()
{
	var file_temp_name = $("[id^=file_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRMFILE').val(file_temp_name);
				
	var file_actual_name = $("[id^=name_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRMNAME').val(file_actual_name);
	$("#txtStatus").val(2);
}

function getDelete(file_name)
{
	jConfirm('Are you sure to remove this attachment permanently?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtStatus").val(1);
			$("#txtFileName").val(file_name);
			document.frm1.submit();
		}
		else{return false;}
		});	
}

function addUI_Row(var1)
{
	var id = $("#txtTID_"+var1).val();
	
	if(id == 0)
	{
		if($("#txtDisponentOwner").val() != "")
		{
			id  = (id - 1 )+ 2;
			num = parseFloat(id +1);
			//alert(id);
			$('<div class="row invoice-info"><div class="col-sm-12 invoice-col"> Disponent Owner '+num+' <address><select  name="txtDisponentOwner_'+id+'" class="form-control" id="txtDisponentOwner_'+id+'"><?php $obj->getVendorListNewForCOA(11); ?></select></address></div></div>').appendTo("#sort");
			$("#txtTID_"+var1).val(id);
		}
		else
		{
			jAlert('Please fill the above entries.', 'Alert');
		}
		
	}
	else
	{
		if($("#txtDisponentOwner_"+id).val() != "")
		{
			id  = (id - 1 )+ 2;
			num = parseFloat(id +1);
			$('<div class="row invoice-info"><div class="col-sm-12 invoice-col"> Disponent Owner '+num+' <address><select  name="txtDisponentOwner_'+id+'" class="form-control" id="txtDisponentOwner_'+id+'"><?php $obj->getVendorListNewForCOA(11); ?></select></address></div></div>').appendTo("#sort");
			$("#txtTID_"+var1).val(id);
		}
		else
		{
			jAlert('Please fill the above entries.', 'Alert');
		}
	}	
}


</script>
    </body>
</html>