<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$comid = $_REQUEST['id'];
$page  = $_REQUEST['page'];
$invoiceid  = $_REQUEST['invoiceid'];

$id = $obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($id);
$arr  = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$id);
$vendorid = $obj->getFun140();
$lp_name = ''; 
$dp_name = '';
for($j=0;$j<count($arr);$j++)
{   $arr_explode = explode('@@',$arr[$j]);$port_name = $obj->getPortNameBasedOnID($arr_explode[1]);
	if($arr_explode[0] == "LP"){$lp_name .= $obj->getPortNameBasedOnID($arr_explode[1]).", ";}
	else{$dp_name .= $obj->getPortNameBasedOnID($arr_explode[1]).", ";}
}

$lp_name  = substr($lp_name,0,-1);
$dp_name  = substr($dp_name,0,-1);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}

$bunkergrade = array( 1 =>1 , 2 =>2, 3 =>3 , 4 =>4);
$bunkergrade1= array( 1 =>'HSFO' , 2 =>'LSFO', 3 =>'HSDO' , 4 =>'LSDO');
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 

});





function getTimeDiff(date2,date1)
{	
	if(date1 != '' &&  date2 != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat(date2);
		start_actual_time    =  getDateWithSpecificFormat(date1);
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000 + parseInt(1);
		return days.toFixed(5);
		//$('#txtTotalDays').val(days.toFixed(0));
	}
}


function getString(var1)
{
  var var2 = var1.split(' ');
  var var3 = var2[0].split('-');  
  return var3[2]+'/'+var3[1]+'/'+var3[0]+' '+var2[1];
}
 
function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' '+dateSplit1[1];
	return currentDate;
}

function AddNewAddRow()
{
	var id = $("#txtAddID").val();
	if($("#txtAddDes_"+id).val() != "" && $("#txtAddDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="add_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtAddDes_'+id+'" id="txtAddDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtAddDesAmt_'+id+'" id="txtAddDesAmt_'+id+'" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td></tr>').appendTo("#tblAdd");
		$("#txtAddID").val(id);
		$("[id^=txtAddDesAmt_]").numeric();	
	}
}


function removeAddRow(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#add_Row_"+var1).remove();
					getFullClaculation();
				 }
			else{return false;}
			});
}



function SubNewSubRow()
{
	var id = $("#txtSubID").val();
	if($("#txtSubDes_"+id).val() != "" && $("#txtSubDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="Sub_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtSubDes_'+id+'" id="txtSubDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtSubDesAmt_'+id+'" id="txtSubDesAmt_'+id+'" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td></tr>').appendTo("#tblSub");
		$("#txtSubID").val(id);
		$("[id^=txtSubDesAmt_]").numeric();	
	}
}


function removeSubRow(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#Sub_Row_"+var1).remove();
				getFullClaculation();
			 }
		else{return false;}
		});
}


function AddNewOffHire()
{
	var id = $("#txtOFFID").val();
	if($("#txtOffHireReason_"+id).val() != "" && $("#txtFromOff_"+id).val() != "" && $("#txtToOff_"+id).val() != "" && $("#txtOffPer_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="off_Row_'+id+'"><td align="center"><a href="#pr'+id+'" onclick="removeOffRow('+id+');"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtOffHireReason_'+id+'" id="txtOffHireReason_'+id+'" rows="2" placeholder="Off Hire Reason..."></textarea></td><td><input type="text" name="txtFromOff_'+id+'" id="txtFromOff_'+id+'" class="form-control" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""></td><td><input type="text" name="txtToOff_'+id+'" id="txtToOff_'+id+'" class="form-control" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""></td><td><input type="text" name="txtOffPer_'+id+'" id="txtOffPer_'+id+'" class="form-control"  placeholder="%" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td></tr>').appendTo("#tblOFF");
		$("#txtOFFID").val(id);
		$("[id^=txtOffPer_]").numeric();	
		$('#txtFromOff_'+id+',#txtToOff_'+id).datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
	    autoclose: true,
        todayBtn: true,
        minuteStep: 1
		}).on('changeDate', function(){  getFullClaculation(); });
	}
}


function removeOffRow(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#off_Row_"+var1).remove();
					getFullClaculation();
				 }
			else{return false;}
			});
}


function getFullClaculation()
{
    var delDays1 = parseFloat(getTimeDiff($("#txtReDelLtTimeGMT").val(),$("#txtDelLtTimeGMT").val()));
	if(isNaN(delDays1)){delDays1 = 0.00;}
	$("#txtTripDuration").val(parseFloat(delDays1).toFixed(5));
	var tccost = parseFloat($("#txtTCCost").val());
	if(isNaN(tccost)){tccost = 0.00;}
	var delDays = parseFloat($("#txtReleaseHireDays").val());
    var numoff = $("#txtOFFID").val();
	var offhiredays = 0;
	for(var i =1;i <=numoff;i++)
	{
	    var timediff = parseFloat(getTimeDiff($("#txtToOff_"+i).val(),$("#txtFromOff_"+i).val()));
		if(parseInt($("#txtOffPer_"+i).val())>0)
		{var percent = $("#txtOffPer_"+i).val();}
		else
		{var percent = 100;}
		offhiredays =   parseFloat(parseFloat(offhiredays) + parseFloat(parseFloat(parseFloat(timediff)*parseFloat(percent))/100));
		$("#txtOffPerAmt_"+i).val(parseFloat(parseFloat(parseFloat(timediff)*parseFloat(percent))/100).toFixed(5));
	}
	if(isNaN(offhiredays)){offhiredays = 0.00;}
	$("#txtNettOffHire").val(offhiredays.toFixed(5));
	var nettperiod = parseFloat(delDays)- parseFloat(offhiredays);
	if(isNaN(nettperiod)){nettperiod = 0.00;}
	$("#txtNettRelease").val(parseFloat(nettperiod).toFixed(5));
	var nettHire = parseFloat(tccost) * parseFloat($("#txtNettRelease").val());
	if(isNaN(nettHire)){nettHire = 0.00;}
	$("#txtNettHire").val(parseFloat(nettHire).toFixed(2));
	var netthire =parseFloat($("#txtNettHire").val());
	if(isNaN(netthire)){netthire = 0.00;}
	var ballastbonus =parseFloat($("#txtBallastBonus").val());
	if(isNaN(ballastbonus)){ballastbonus = 0.00;}
	var nettballahire = parseFloat(ballastbonus) + parseFloat(netthire);
	var comission = $("#txtCommission").val();
	if(isNaN(comission)){comission = 0.00;}
	var commamt =   parseFloat(parseFloat(parseFloat(nettballahire)*parseFloat(comission))/100);
	if(isNaN(commamt)){commamt = 0.00;}
	$("#txtCommissionAmt").val(parseFloat(commamt).toFixed(2));
	
	var Brokerage = $("#txtBrokerage").val();
	if(isNaN(Brokerage)){Brokerage = 0.00;}
	var BrokerageAmt =   parseFloat(parseFloat(parseFloat(nettballahire)*parseFloat(Brokerage))/100);
	if(isNaN(BrokerageAmt)){BrokerageAmt = 0.00;}
	$("#txtBrokerageAmt").val(parseFloat(BrokerageAmt).toFixed(2));
	
	var cveamt = parseFloat(parseFloat($("#txtDelCVE").val())/30)*parseFloat($("#txtNettRelease").val());
	if(isNaN(cveamt)){cveamt = 0.00;}
	$("#txtDelCVEAmt").val(cveamt.toFixed(2));
	if($("#txtDelCVEPer").val()=="" || $("#txtDelCVEPer").val()==0){$("#txtDelCVEPerAmt").val($("#txtDelCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtDelCVEAmt").val())*parseFloat($("#txtDelCVEPer").val()))/100);
	if(isNaN(perAmt)){perAmt = 0.00;}$("#txtDelCVEPerAmt").val(perAmt.toFixed(2));}
	
	 var delAmt = parseFloat($("#txtDelConspTotalAmt").val());
	 if(isNaN(delAmt)){delAmt = 0.00;}
	 
	 var cveamt = parseFloat($("#txtDelCVEPerAmt").val());
	 if(isNaN(cveamt)){cveamt = 0.00;}
	 
	 var redelAmt = parseFloat($("#txtReDelConspTotalAmt").val());
	 if(isNaN(redelAmt)){redelAmt = 0.00;}
	 
	  var onRedelAmt = parseFloat($("#txtOnReDelConspTotalAmt").val());
	 if(isNaN(onRedelAmt)){onRedelAmt = 0.00;}
	
	$("#txtAddDesTotalAmt").val(parseFloat($("[id^=txtAddDesAmt_]").sum()).toFixed(2));
	
	$("#txtSubDesTotalAmt").val(parseFloat($("[id^=txtSubDesAmt_]").sum()).toFixed(2));
	
	var lastAmt = parseFloat(nettballahire) + parseFloat(delAmt) + parseFloat(cveamt) + parseFloat($("#txtAddDesTotalAmt").val()) + parseFloat(redelAmt)  - parseFloat(commamt) - parseFloat(BrokerageAmt) - parseFloat(onRedelAmt) - parseFloat($("#txtSubDesTotalAmt").val()) - parseFloat($("[id^=txtPrevInvoiceAmt_]").sum());
	if(isNaN(lastAmt)){lastAmt = 0.00;}//txtPrevInvoiceAmt_
	$("#txtFinalAmt").val(parseFloat(lastAmt).toFixed(2))
}

function getOnReDelBunkerGradeAmt()
{
	var total = $("#txtOnReDelbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtOnReDelConspQtyMT_"+i).val())*parseFloat($("#txtOnReDelPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtOnReDelConspAmt_"+i).val(amt.toFixed(3))
		if($("#txtOnReDelConspPer_"+i).val()=="" || $("#txtOnReDelConspPer_"+i).val()==0)
		{
		   $("#txtOnReDelConspPerAmt_"+i).val($("#txtOnReDelConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtOnReDelConspAmt_"+i).val())*parseFloat($("#txtOnReDelConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtOnReDelConspPerAmt_"+i).val(perAmt.toFixed(3));
		}
	}
	$("#txtOnReDelConspTotalAmt").val(parseFloat($("[id^=txtOnReDelConspPerAmt_]").sum()).toFixed(3));
	getFullClaculation();
}


function getReDelBunkerGradeAmt()
{
	var total = $("#txtReDelbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtReDelConspQtyMT_"+i).val())*parseFloat($("#txtReDelPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtReDelConspAmt_"+i).val(amt.toFixed(3))
		if($("#txtReDelConspPer_"+i).val()=="" || $("#txtReDelConspPer_"+i).val()==0)
		{
		   $("#txtReDelConspPerAmt_"+i).val($("#txtReDelConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtReDelConspAmt_"+i).val())*parseFloat($("#txtReDelConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtReDelConspPerAmt_"+i).val(perAmt.toFixed(3));
		}
	}
	$("#txtReDelConspTotalAmt").val(parseFloat($("[id^=txtReDelConspPerAmt_]").sum()).toFixed(3));
	getFullClaculation();
}

function getDelBunkerGradeAmt()
{
	var total = $("#txtDelbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtDelConspQtyMT_"+i).val())*parseFloat($("#txtDelPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtDelConspAmt_"+i).val(amt.toFixed(3))
		if($("#txtDelConspPer_"+i).val()=="" || $("#txtDelConspPer_"+i).val()==0)
		{
		   $("#txtDelConspPerAmt_"+i).val($("#txtDelConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtDelConspAmt_"+i).val())*parseFloat($("#txtDelConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtDelConspPerAmt_"+i).val(perAmt.toFixed(3));
		}
	}
	$("#txtDelConspTotalAmt").val(parseFloat($("[id^=txtDelConspPerAmt_]").sum()).toFixed(3));
	getFullClaculation();
}


function getValidate(var1)
{
	$("#txtStatus").val(var1);
	//document.frm1.submit();
}

</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;In Ops at a glance&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>
                 <?php
				$sql = "SELECT * from invoice_hire_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and INVOICEID='".$invoiceid."'";
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				?>
                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><?php if($rows['INVOICE_TYPE']!="Adjustment"){?><a href="allPdf.php?id=46&comid=<?php echo $comid; ?>&invoiceid=<?php echo $invoiceid; ?>" title="Pdf"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a><?php }?><a href="hire_invoice_list.php?id=<?php echo $comid;?>&page=<?php echo  $page;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
				<?php
				$sql = "SELECT * from invoice_hire_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and INVOICEID='".$invoiceid."'";
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				if($rows['INV_TYPE']==2)
				{
					$sql4 = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
					$res4 = mysql_query($sql4);
					$rec4 = mysql_num_rows($res4);
					$bunkergrade = array();
					$bunkergrade1 = array();
					$i=1;
					while($rows4 = mysql_fetch_assoc($res4))
					{
						$bunkergrade[$i] = $rows4['BUNKERGRADEID'];
						$bunkergrade1[$i] = $rows4['NAME'];
						$i++;
					}
				}
				?>				
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12" style="text-align:center;">
								<h2 class="page-header">
								Hire Statement
                                <input type="hidden" name="txtFcaid" id="txtFcaid" value="<?php echo $obj->getFun1();?>"/>
                                <input type="hidden" name="txtMapId" id="txtMapId" value="<?php echo $comid;?>"/>
                                <input type="hidden" name="txtInID" id="txtInID" value="<?php echo $rows['INVOICEID'];?>"/>
								</h2>                            
							</div><!-- /.col -->
						</div>
					
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Statement Type
								<address>
                                <label>&nbsp;&nbsp;&nbsp;<?php echo $rows['INVOICE_TYPE'];?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
							   Statement No.
								<address>
									<label>&nbsp;&nbsp;&nbsp;<?php echo $rows['INVOICE_NO'];?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								CP Date
								<address>
								   <label>&nbsp;&nbsp;&nbsp;<?php 
								   $cpdate = $obj->getCompareEstimateData($comid,"CP_DATE");
								   if($cpdate=="" || $cpdate=="0000-00-00"){$cpdate = ""; }
								   else{$cpdate = date("d-m-Y",strtotime($obj->getCompareEstimateData($comid,"CP_DATE"))); } echo $cpdate; ?></label>
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
						  <div class="col-sm-3 invoice-col">
								 Statement Date
								<address>
								  <label>&nbsp;&nbsp;&nbsp;<?php echo date("d-m-Y",strtotime($rows['INVOICE_DATE']));?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-3 invoice-col">
								Nom ID
								<address>
								 <label>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCompareTableData($comid,"MESSAGE");?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-3 invoice-col">
								Vessel
								<address>
								   <label>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></label>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-3 invoice-col">
								Vessel IMO No.
								<address>
								   <label><?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"IMO_NO");?></label>
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Vendor
								<address>
								        <label>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVendorListNewBasedOnID($vendorid);?></label>
										<input type="hidden" name="txtVendor" id="txtVendor" value="<?php echo $vendorid;?>" />
										<input type="hidden" name="txtNameid" id="txtNameid" value="<?php echo $id[0];?>" />
										<input type="hidden" name="txtGradeid" id="txtGradeid" value="<?php echo $id[2];?>" />
										<input type="hidden" name="txtFCAID" id="txtFCAID" value="<?php echo $obj->getFun1();?>" />
								</address>
							</div><!-- /.col -->                    
							<div class="col-sm-4 invoice-col">
								Loading Port
								<address>
								   <label>&nbsp;&nbsp;&nbsp;<?php echo $lp_name;?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Discharging Port
								<address>
								   <label>&nbsp;&nbsp;&nbsp;<?php echo $dp_name;?></label>
								</address>
							</div><!-- /.col -->
						</div>
                        <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Remarks
								<address>
									<?php echo $rows['REMARKS'];?>
								</address>
							</div><!-- /.col -->						
						</div>
					   <?php if($rows['INVOICE_TYPE']!="Adjustment"){?>   
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								Supplier's Account : Statement
								</h2>                            
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Del At.
								<address>
                                    <label>&nbsp;&nbsp;&nbsp;<?php echo $obj->getPortNameBasedOnID($rows['DEL_PORTID']);?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Del Date Time GMT
								<address>
                                <?php if($rec > 0 && $rows['DEL_DATE']!= NULL){$delDate =  date('d-m-Y H:i',strtotime($rows['DEL_DATE']));}else{$delDate ="";}?>
								 <label>&nbsp;&nbsp;&nbsp;<?php echo $delDate;?></label>
								</address>
							</div><!-- /.col -->
						</div>
						
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   ReDel At.
								<address>
                                    <label>&nbsp;&nbsp;&nbsp;<?php echo $obj->getPortNameBasedOnID($rows['REDEL_PORTID']);?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								ReDel Date Time GMT
								<address>
                                 <?php if($rec > 0 && $rows['REDEL_DATE']!= NULL){$delDate =  date('d-m-Y H:i',strtotime($rows['REDEL_DATE']));}else{$delDate ="";}?>
								  <label>&nbsp;&nbsp;&nbsp;<?php echo $delDate;?></label>
								</address>
							</div><!-- /.col -->
						</div>
						
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Total Trip Duration
								<address>
                                
									<label>&nbsp;&nbsp;&nbsp;<?php echo $rows['TRIP_DURATION'];?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
							   Tc Cost/Day (USD)
								<address>
									<label>&nbsp;&nbsp;&nbsp;<?php echo $rows['TC_COST'];?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Release Hire Days
								<address>
								  <label>&nbsp;&nbsp;&nbsp;<?php echo $rows['RELEASE_HIRE_DAYS'];?></label>
								</address>
							</div><!-- /.col -->
						</div>
					 <div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                              		<th>Off Hire Reason</th>
                                    <th>Off Hire From (Days)</th>
                                    <th>Off Hire To (Days)</th>
                                    <?php $sql2 = "select * from invoice_hire_slave3 where INVOICEID='".$rows['INVOICEID']."'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									?>
									<th>Off Hire %<input type="hidden" name="txtOFFID" id="txtOFFID" value="<?php echo $num21;?>"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblOFF">
                            <?php if($num2 >0)
							   {$i = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;
								?>
                                <tr id="off_Row_<?php echo $i;?>">
                                   <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows2['OFFHIRE_REASON'];?></level></td>
                                   <td><label>&nbsp;&nbsp;&nbsp;<?php echo date('d-m-Y H:i',strtotime($rows2['FROM_OFF_DATE']));?></label></td>
                                   <td><label>&nbsp;&nbsp;&nbsp;<?php echo date('d-m-Y H:i',strtotime($rows2['TO_OFF_DATE']));?></label></td>
								   <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows2['OFFHIRE_PERCENT'];?></label></td>
                                </tr>
                                <?php }}?>
                       
                            </tbody>
                   
                        </table>
                    </div>
                  </div>
						
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							  Nett Off Hire
								<address>
                                 <label>&nbsp;&nbsp;&nbsp;<?php echo $rows['NETT_OFFHIRE'];?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Nett Release
								<address>
								  <label>&nbsp;&nbsp;&nbsp;<?php echo $rows['NETT_RELEASE'];?></label>
								</address>
							</div><!-- /.col -->
						</div>
						
						
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								Amount Due To Owners
								</h2>                            
							</div><!-- /.col -->
						</div>
						
						<div class="box">
                         <div class="box-body no-padding">
							<table class="table table-striped">
								<tbody>
									<tr>
										<td width="20%">Nett Hire</td>
										<td width="15%"></td>
										<td width="20%"></td>
										<td width="20%"></td>
                                       
										<td width="25%"><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['NETT_HIRE'];?></label></td>
									</tr>
							 <?php 
									$sql3 = "select * from fca_tci_other_misc_cost where FCAID='".$obj->getFun1()."' and OTHER_MCOSTID='15'";
									$res3 = mysql_query($sql3);
									$rows3 = mysql_fetch_assoc($res3);
									
									
									?>
									<tr>
										<td width="20%">Ballast Bonus</td>
										<td width="15%"></td>
										<td width="20%"></td>
										<td width="20%"></td>
                                        <?php if($rec>0){?>
										<td width="25%"><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['BALLAST_BONUS'];?></label></td>
										<?php }else{?>
                                        <td width="25%"><label>&nbsp;&nbsp;&nbsp;<?php echo $rows3['AMOUNT'];?></label></td>
                                        <?php }?>
									</tr>
								</tbody>
						      </table>
						 </div>
                       </div>
						
						
						
					<?php
					 $numgrade = count($bunkergrade);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Cost Of Bunker On Delivery</h3><input type="hidden" name="txtDelbid" id="txtDelbid" value="<?php echo $numgrade;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php  for($i=1;$i<=$numgrade;$i++)
					             {
								$sql_inv2 = "select * from invoice_hire_slave2 where INVOICEID='".$rows['INVOICEID']."' and BUNKERID='".$i."' and IDENTIFY='DEL'"; 
								$res_inv2 = mysql_query($sql_inv2);
								$rows_inv2 = mysql_fetch_assoc($res_inv2);	 
									 ?>
                                  <tr>
                                    <td><?php echo $bunkergrade1[$i];?><input type="hidden" id="txtDelBunkerID_<?php echo $i;?>" name="txtDelBunkerID_<?php echo $i;?>" value="<?php echo $i;?>" /></td>
								    <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows_inv2['BUNKER_QTY'];?></label></td>
                                    <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows_inv2['BUNKER_PRICE'];?></label></td>
                                    <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows_inv2['BUNKER_AMT'];?></label></td>
									<td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows_inv2['BUNKER_PER'];?></label></td>
                                </tr>
                                <?php }?>
								<tr>
                                    <td colspan="4">Total</td>
									<td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['DEL_BUNKER_COST'];?></label></td>
                                </tr>
                            </tbody>
                            <tfoot>
							<?php 
								$sql_brok = "select * from fca_tci_owner_related_cost where FCAID='".$obj->getFun1()."' and OWNER_RCOSTID='3'";
								$res_brok = mysql_query($sql_brok);
								$row_brok = mysql_fetch_assoc($res_brok);
								?>
                               <tr>
                                    <td>CVE</td>
										<td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['CVE'];?></label></td>
                                        <td></td>
                                        <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['CVE_AMT'];?></label></td>
                                        <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['CVE_PER'];?></label></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
				
				
				
				
				<div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                  
                                    <th>Description</th>
                                    <?php $sql2 = "select * from invoice_hire_slave1 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='ADD'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									?>
                          			<th>Amount<input type="hidden" name="txtAddID" id="txtAddID" value="<?php echo $num21;?>"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblAdd">
                            <?php if($num2 >0)
							   {$i = 0;$sum = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;$sum = $sum + $rows2['AMOUNT'];
								?>
                                <tr id="add_Row_<?php echo $i;?>">
                          
                                   <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows2['DESCRIPTION'];?></label></td>
                                   <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows2['AMOUNT'];?></label></td>
                                </tr>
                                <?php }}?>
                            </tbody>
                            <tfoot>
                                <tr>
                                
									<td>Total</td>
									<td><label>&nbsp;&nbsp;&nbsp;<?php echo $sum;?></label></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
						
						
				
				
				    
						
						
				
				   <div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
							     Less
								</h2>                            
							</div><!-- /.col -->
				    </div>
				
				   <div class="box">
                      <div class="box-body no-padding">
                      <h3>Comission</h3>
                        <table class="table table-striped">
                           <tbody>
							   <tr>
                                    <td><strong>Comission (%)</strong></td>
                                    <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['COMISSION_PER'];?></label></td>
                                    <td></td>
                                    <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['COMISSION_PER_AMT'];?></label></td>
								 </tr>
                            </tbody>
                          </table>
                    </div>
					
					<div class="box-body no-padding">
                      <h3>Brokerage</h3>
                        <table class="table table-striped">
                           <tbody>
							
                               <tr>
                                    <td><strong>Brokerage (%)</strong></td>
                                 
										<td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['BROKERAGE_PER'];?></label></td>
                                    <td></td>
                                    <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['BROKERAGE_PER_AMT'];?></label></td>
									
                                </tr>
                            </tbody>
                          </table>
                    </div>
                  </div>
				  
				  
				  
				  <div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
							     Cost Of Bunker On Redelivery
								</h2>                            
							</div><!-- /.col -->
				    </div>
						<?php
					 $numgrade = count($bunkergrade);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <input type="hidden" name="txtOnReDelbid" id="txtOnReDelbid" value="<?php echo $numgrade;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php  for($i=1;$i<=$numgrade;$i++)
					             {
								$sql_inv2 = "select * from invoice_hire_slave2 where INVOICEID='".$rows['INVOICEID']."' and BUNKERID='".$i."' and IDENTIFY='ONREDEL'"; 
								$res_inv2 = mysql_query($sql_inv2);
								$rows_inv2 = mysql_fetch_assoc($res_inv2);?>
                                  <tr>
                                    <td><?php echo $bunkergrade1[$i];?><input type="hidden" id="txtOnReDelBunkerID_<?php echo $i;?>" name="txtOnReDelBunkerID_<?php echo $i;?>" value="<?php echo $i;?>" /></td>
								    <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows_inv2['BUNKER_QTY'];?></label></td>
                                    <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows_inv2['BUNKER_PRICE'];?></label></td>
                                    <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows_inv2['BUNKER_AMT'];?></label></td>
									<td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows_inv2['BUNKER_PER'];?></label><input type="Hidden" name="txtOnReDelConspPerAmt_<?php echo $i;?>" id="txtOnReDelConspPerAmt_<?php echo $i;?>" value="<?php echo $rows_inv2['BUNKER_PER_AMT'];?>"/></td>
                                </tr>
                                <?php }?>
								<tr>
                                    <td colspan="4">Total</td>
									<td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['ONREDEL_BUNKER_COST'];?></label></td>
                                </tr>
                            </tbody>
                          </table>
                    </div>
                  </div>
				  
				  		
				<div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                     <?php $sql2 = "select * from invoice_hire_slave1 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='SUB'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									?>
                          			<th>Amount<input type="hidden" name="txtSubID" id="txtSubID" value="<?php echo $num21;?>"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblSub">
                            <?php if($num2 >0)
							   {$i = 0;$sum = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;$sum = $sum + $rows2['AMOUNT'];
								?>
                                <tr id="Sub_Row_<?php echo $i;?>">
                                  <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows2['DESCRIPTION'];?></label></td>
                                   <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows2['AMOUNT'];?></label></td>
                                </tr>
                                <?php }}?>
                            </tbody>
                            <tfoot>
                                <tr>
                           
									<td>Total</td>
									<td><label>&nbsp;&nbsp;&nbsp;<?php echo $sum;?></label></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
						
					<div class="box">
							<div class="box-header">
							</div><!-- /.box-header -->
							
						</div>
                    
                    <?php 
							$sql4 = "select * from invoice_hire_master where COMID='".$comid."' and INVOICEID < '".$rows['INVOICEID']."'";
	                        $res4 = mysql_query($sql4);
							$num4 = mysql_num_rows($res4);
							if($num4>0)
							{?>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                   <tbody>
							   <?php  $i=0;
								while($rows4 = mysql_fetch_assoc($res4))
								{$i++;
								?>
                                    <tr>
                                        <td width="30%"><?php echo $rows4['INVOICE_NO'];?></td>
                                        <td width="30%"></td>
                                        <td width="30%"><label>&nbsp;&nbsp;&nbsp;<?php echo $rows4['BALANCE_TO_OWNER'];?></label></td>
                                    </tr>
                         <?php }  ?>    
                               </tbody>
                             </table>
                         </div>
                     <?php } }?>
                    	
				  
				  <div class="box-body no-padding">
                        <table class="table table-striped">
                           <tbody>
							
                               <tr>
                                    <td> <h3>Balance To Owners</h3></td>
                                    <td></td>
                                    <td><label>&nbsp;&nbsp;&nbsp;<?php echo $rows['BALANCE_TO_OWNER'];?></label></td>
                                </tr>
                            </tbody>
                          </table>
                    </div>
				       
                       
                       <div class="box">
							<div class="box-header">
								<h3 class="box-title">Banking Details</h3><br/>
                               
							</div><!-- /.box-header --> &nbsp;<?php echo $rows['BANKINGDATAILS'];?>
							<div class="box-body no-padding"></div>
						</div>
						
				
												
					</form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>