<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
if(isset($_REQUEST['tabs']))
{
	$tabs = $_REQUEST['tabs'];
}else{$tabs = 0;}
if($page == 1){$page_link = "in_ops_at_glance.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "vessel_in_post_ops.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "vessel_in_history.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "coa_in_ops_at_glance.php"; $page_bar="In Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else if($page == 5){$page_link = "coa_in_post_ops.php"; $page_bar="In Post Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else{$page_link = "coa_in_history.php"; $page_bar="Vessels in History - COA";$nav=16;$subtitle = 'COA';}

if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->nominateAgent();
	if($_REQUEST['txtsubmitid_'.$_REQUEST['txttabid'][0]] == 1)
	{
		header('Location:./nominate_agent.php?msg='.$msg.'&tab='.($_REQUEST['txttabid'][0]-1).'&comid='.$_REQUEST['comid'].'&page='.$_REQUEST['page']."&tabs=".$tabs);
	}
	else
	{
		header('Location:./'.$page_link.'?msg=3');
	}
 }
$cost_sheet_id = $obj->getLatestCostSheetID($comid);
$arr  = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$cost_sheet_id);
$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);


$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;
$msg = NULL;
if(isset($_REQUEST['tab']))
{
$tab = $_REQUEST['tab'];
}
else{$tab = 0;}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
//$("#tabs").tabs({selected : <?php echo $tab;?> });
});

function getSubmit(tabid,submitid,port,portid,serialno)
{
	if($("#txtvendorid_"+tabid).val() != "")
	{
		jConfirm('Are you sure you want to submit this data?', 'Confirmation', function(r) 
		{
			if(r)
			{
				$("[id^=txttabid_],[id^=txtsubmitid_],[id^=txtport_],[id^=txtportid_],[id^=txtSerialNo_]").val("");
				$("#txttabid_"+tabid).val(tabid);
				$("#txtsubmitid_"+tabid).val(submitid);
				$("#txtport_"+tabid).val(port);
				$("#txtportid_"+tabid).val(portid);
				$("#txtSerialNo_"+tabid).val(serialno);
				var formname = "frm"+tabid;
				$("#"+formname).submit();
			} 
			else 
			{
				return false;
			}
		});
	}
	else
	{
		jAlert('Please nominate the agent.', 'Alert');
		return false;
	}
}

function getVendorID(tabid,vendorid)
{
	$("#txtvendorid_"+tabid).val(vendorid);
}

function getPdf(var1)
{
	//location.href='allPdf.php?id=3&gen_agency_id='+var1;
}
</script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}
form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?></li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content invoice">
					<!--   content put here..................-->
					<?php 
							if(isset($_REQUEST['msg'])){
								$msg = $_REQUEST['msg'];
								if($msg == 0){?>
									<div class="alert alert-success alert-dismissable">
										<i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<b>Congratulations!</b> Agency Letter Generation added/updated successfully.
									</div>
								<?php }?>
								<?php if($msg == 1){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Sorry!</b> this agent is already exists for this port.
								</div>
								<?php }?>
						<?php }?>
						<div align="right"><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
						<div style="height:10px;">&nbsp;</div>
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header" style="text-align:center;">
								 PDA/FDA
								</h2>                            
							</div><!-- /.col -->
						</div>			
						<div class="row invoice-info">
							<div class="col-md-12">
								<!-- Custom Tabs -->
								<div id="tabs" class="nav-tabs-custom">
									<ul class="nav nav-tabs">
									<?php for($i=0;$i<count($arr);$i++){
										$arr_explode = explode('@@',$arr[$i]);$port_name = $obj->getPortNameBasedOnID($arr_explode[1]);?>
										<li class="<?php if($tabs == $i){ echo 'active';} ?>"><a href="#tabs_<?php echo $i+1;?>" data-toggle="tab"><?php echo $arr_explode[0]."-".$port_name;?></a></li>
									<?php }?>
									</ul>
									<div class="tab-content">
									
									<!----------------------------------------------- Start 1------------------------------------------------->
									<?php for($i=0;$i<count($arr);$i++){ 
									$arr_explode = explode('@@',$arr[$i]);$port_name = $obj->getPortNameBasedOnID($arr_explode[1]);
									?>
									<div id="tabs_<?php echo $i+1;?>" class="tab-pane <?php if($tabs == $i){ echo 'active';} ?>">
									<form name="frm<?php echo $i+1;?>" id="frm<?php echo $i+1;?>" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>
									<div class="box-body table-responsive">
									<?php 
									$mysql = "select * from generate_agency_letter where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_explode[0]."' and PORTID='".$arr_explode[1]."' and STATUS=2 and RANDOMID='".$arr_explode[2]."'";
									$myres = mysql_query($mysql);
									$myrec = mysql_num_rows($myres);
									if($myrec == 1)
									{
										$myrows  = mysql_fetch_assoc($myres);
										?>
                                        <div class="row invoice-info">
                                            <div class="col-sm-12 invoice-col">&nbsp;</div>
                                            <div class="col-sm-3 invoice-col">
                                                Nom ID : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?php echo $obj->getCompareTableData($comid,"MESSAGE");?></strong>
                                            </div>
                                        </div>
                                        <div class="row invoice-info" style="height:20%;">&nbsp;</div>
                                        <table cellpadding="1" cellspacing="1" border="0" width="100%" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                        <th width="10%" align="left" valign="top" style="background-color:#3c8dbc;color:#fff;font-size:12px;">Vessel</th>
                                        <th width="10%" align="left" valign="top" style="background-color:#3c8dbc;color:#fff;font-size:12px;">Agent</th>
                                        <th width="10%" align="left" valign="top" style="background-color:#3c8dbc;color:#fff;font-size:12px;">Username</th>
                                        <th width="10%" align="left" valign="top" style="background-color:#3c8dbc;color:#fff;font-size:12px;">Password</th>
                                        <th width="10%" align="left" valign="top" style="background-color:#3c8dbc;color:#fff;font-size:12px;">Port Cost</th>
                                        
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                        <td align="left" valign="middle"><?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></td>
                                        <td align="left" valign="middle"><?php echo $obj->getVendorListNewBasedOnID($myrows['VENDORID']);?></td>
                                        <td align="left" valign="middle"><?php echo $myrows['USERNAME'];?></td>
                                        <td align="left" valign="middle"><?php echo $myrows['PASSWORD'];?></td>
                                        <td align="left" valign="middle"><a href="port_costs.php?comid=<?php echo $comid;?>&gen_agency_id=<?php echo $myrows['GEN_AGENCY_ID'];?>&page=<?php echo $page;?>&vendorid=<?php echo $myrows['VENDORID'];?>&tab=<?php echo $i;?>" title="Port Costs" >Port Costs</a></td>
                                        
                                        </tr>
                                        </tbody>
                                        </table>
							  <?php } 
							   else { ?>
                                        
										
										<?php } ?>
                                        </div>
                                        </form>
                                        </div>
									<?php } ?>
									
									<!-- /.tab_1-pane -->
									
									</div><!-- /.tab-content -->
								</div><!-- nav-tabs-custom -->
							</div>
						</div>
					<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
    </body>
</html>