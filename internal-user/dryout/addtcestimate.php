<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$obj->viewVesselRecords($id);

if(@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertTCEstimate();
	header('Location:./estimate_tc_list.php?msg='.$msg);
}

$estimatetype = $_REQUEST['estimatetype'];
$_SESSION['selBType'] = $estimatetype;

$pagename = basename($_SERVER['PHP_SELF'])."?estimatetype=".$_SESSION['selBType'];
?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>

<style>
.animated
{
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error
{
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}

table{border-collapse:separate;}
select{height:19px;}
.select
{
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-weight: bold;
    color: #333333;
    /* color: #FBC763; */
    text-decoration: none;
    line-height: 13px;
	height:13px;
}

.input-text
{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #333333;
	text-decoration: none;
	/*text-transform:uppercase;
	line-height: 15px;*/
}

.input-textsmall
{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-weight: normal;
	color: #333333;
	text-decoration:none;
	text-transform:uppercase;
}

.input 
{
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 10px;
font-weight: normal;
color: #333333;
text-decoration: none;
}
</style>
</head>

	<div id="basic-modal-content" style="display:none;" align="center">
	<table cellpadding="1" cellspacing="4" border="0" align="center">
	<tr><td align="center"><img src="../../img/loading.gif" /><br /></td></tr>
	</table>
	</div>
	
    <body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onClick="getFinalCalculation();">Calculate</a>
     <!----------------------------- header logo: style can be found in header.less --------------------------------->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(8); ?>
			<aside class="right-side">                
     <!---------------------------------------- Content Header (Page header)----------------------------------------->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-leaf"></i>&nbsp;TC Estimates&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">TC Estimates</li>
                    </ol>
                </section>
                <div align="right" style="margin-top:5px;margin-bottom:5px;margin-right:5px;"><a href="estimate_tc_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                    <table width="100%" style="margin-bottom:5px; padding:0px 5px 0px 5px;">
                       <tr>
                           <td width="12%" ><strong >Fixture&nbsp;Type&nbsp;:</strong>&nbsp;TC&nbsp;Out&nbsp;&nbsp;<input name="selFType" type="hidden" id="selFType" value="1"/></td>
                           <td >
                           <strong>Vessel&nbsp;:</strong><select name="selVName" class="input-text" id="selVName" style="width:120px;color:#175c84;" onChange="getData();getValue();">
                                <?php
                                $obj->getVesselTypeListMemberWise($_SESSION['selBType']);
                                ?>
                                </select>
                           </td>
                           <td ><strong>Vessel&nbsp;Type&nbsp;:</strong><input type="text" name="txtVType" id="txtVType" style="width:95px;color:#175c84;background-color:#E1E1E1;" class="input-text" readonly value="" />
                           
                            <select  name="selBunker" style="display:none;" id="selBunker">
                                <?php $obj->getBunkerList(); ?>
                            </select>
                           
                           </td>
                           <td ><strong>Flag&nbsp;:</strong><input type="text" name="txtFlag" id="txtFlag" style="width:100px;color:#175c84;background-color:#E1E1E1;" class="input-text" readonly value="" /></td>
                            <td ><strong>Date&nbsp;:</strong><input type="text" name="txtDate" id="txtDate" style="width:90px;color:#175c84;" class="input-text" value="<?php echo date("d-m-Y",time());?>" placeholder"dd-mm-yy" /></td>							
							<?php
								$copyYear = 2017; // Set your website start date
								$curYear = date('y'); // Keeps the second year updated
							?>
                            <td ><strong>TC&nbsp;No.&nbsp;:</strong><input type="text" name="txtTCNo" id="txtTCNo" style="width:90px;color:#175c84;" class="input-text" autocomplete="off" placeholder="TC NO." /></td>
                       </tr>
                   </table>
                <div id="tabs" class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
						<li class="active"><a href="#tabs_1" data-toggle="tab">TC Fixture Note</a></li>
                        <li><a href="#tabs_2" data-toggle="tab">Commercial Parameters</a></li>
                        <li><a href="#tabs_3" data-toggle="tab">TC/CP Terms</a></li>
                    </ul>
                    <div class="tab-content">
                    
    <!-----------------START-------------------------Tab_1->TC Fixture Note-------------------------------------------->

					<div id="tabs_1" class="tab-pane  active" style="overflow:auto;">
                       <table width="100%">
                          <tr>
                           <td colspan="3">
                               <table width="100%">
							   <tr>
							   <div><strong style="font-size:13px;">Charter Party</strong></div>
							   </tr>
                               <tr class="input-text">
                                  <td>CP Date&nbsp;</td>
								  <td><input type="text" name="txtCPdate" id="txtCPdate" class="input-text" value="<?php echo date("d-m-Y", time()); ?>" style="width:130px;color:#175c84;" placeholder="dd-mm-yy" /></td>
								  <td >CP Type&nbsp;
								  <select name="selCPType" id="selCPType" class="input-text" style="width:132px;" onChange="getCPTypeOther();">
								  <?php echo $obj->getContractTypeList();?>
								  </select></td>
								  <td >Charterers&nbsp;
								  <select name="selCharterers" id="selCharterers" class="input-text" style="width:132px;" onChange="getCharterersOther();">
								  <?php $obj->getVendorListNewForCOA('7,11,10');?>
								  </select></td>
								  <td >Charterers' Operations&nbsp;
								  <select name="selCharOperation" id="selCharOperation" class="input-text" style="width:135px;" onChange="getChartererData();">
								  <?php $obj->getVendorListNewForCOA('7,11,10,12');?>
								  </select></td>
                               </tr>
							   <tr class="input-text">
								  <td >Law/Arbitration&nbsp;</td>
								  <td><select name="selLawArbit" id="selLawArbit" class="input-text" style="width:130px;">
								  <?php $obj->getLawArbitrationList(); ?>
								  </select></td>
								  <td></td>
								  <td colspan="2"><textarea name="txtAddress" id="txtAddress" rows="3" cols="85" placeholder="Address" readonly style="background-color:#E1E1E1;"></textarea></td>
								  <td></td>
								  <td></td>
				               </tr>
                              </table>
                           </td>
                       	  </tr>
		<!-------------------------------------------LEFT SIDE PART--------------------------------------------------->	
                       
					   <tr>
                           <td style="width:28%;vertical-align:top;">
						   <div><strong style="font-size:13px;">Vessel Details: <span id="lblVesName"></span></strong></div>
                             <table width="100%">
                                <tr class="input-text">
                                	<td >Build/Yard</td>
									<td ><input type="text" name="txtBuildYard" id="txtBuildYard" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly/></td>
                                </tr>
                                <tr class="input-text">
                                	<td >Year Build</td>
									<td ><input type="text" name="txtYearBuild" id="txtYearBuild" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Flag</td>
									<td ><input type="text" name="txtFlag1" id="txtFlag1" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Port of Registry</td>
									<td ><input type="text" name="txtPortOfRegis" id="txtPortOfRegis" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly>
									</td>
                                </tr>
								<tr class="input-text">
                                	<td >IMO Number</td>
									<td ><input type="text" name="txtIMOnum" id="txtIMOnum" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Class/Class ID</td>
									<td ><input type="text" name="txtClassID" id="txtClassID" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Last Special Survey</td>
									<td ><input type="text" name="txtLastSpSurvey" id="txtLastSpSurvey" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Last DD</td>
									<td ><input type="text" name="txtLastDD" id="txtLastDD" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Owners’ P & I</td>
									<td ><input type="text" name="txtOwnerPandI" id="txtOwnerPandI" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Master’s Name</td>
									<td ><input type="text" name="txtMastersName" id="txtMastersName" class="input-text" style="width:140px;" placeholder="Master’s Name" autocomplete="off"></td>
                                </tr>
								<tr class="input-text">
                                	<td >Call Sign</td>
									<td ><input type="text" name="txtCallSign" id="txtCallSign" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Inmarsat Tel</td>
									<td ><input type="text" name="txtInmarsatTel" id="txtInmarsatTel" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Inmarsat Email</td>
									<td ><input type="text" name="txtInmarsatEmail" id="txtInmarsatEmail" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >LOA</td>
									<td ><input type="text" name="txtLOA1" id="txtLOA1" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Breadth</td>
									<td ><input type="text" name="txtBreadth" id="txtBreadth" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Summer DWT</td>
									<td ><input type="text" name="txtSummerDWT" id="txtSummerDWT" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Summer Draft</td>
									<td ><input type="text" name="txtSummerDraft" id="txtSummerDraft" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >TPC</td>
									<td ><input type="text" name="txtTPC1" id="txtTPC1" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Gross Tonnage</td>
									<td ><input type="text" name="txtGrossTonnage" id="txtGrossTonnage" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Net Tonnage</td>
									<td ><input type="text" name="txtNetTonnage" id="txtNetTonnage" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<?php if($_SESSION['selBType'] == 3)
								{ ?>								
								<tr class="input-text">
                                	<td >Suez GRT</td>
									<td ><input type="text" name="txtSuezGRT" id="txtSuezGRT" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Suez NRT</td>
									<td ><input type="text" name="txtSuezNRT" id="txtSuezNRT" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Panama NRT</td>
									<td ><input type="text" name="txtPanamaNRT" id="txtPanamaNRT" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Grain Cap(CBM)</td>
									<td ><input type="text" name="txtGrainCap" id="txtGrainCap" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Bale Cap(CBM)</td>
									<td ><input type="text" name="txtBaleCap" id="txtBaleCap" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Cranes</td>
									<td ><input type="text" name="txtCranes" id="txtCranes" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Grabs</td>
									<td ><input type="text" name="txtGrabs" id="txtGrabs" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>	
								<tr class="input-text">
                                	<td >Keel to top of Mast(M)</td>
									<td ><input type="text" name="txtKeelTopMast" id="txtKeelTopMast" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Waterline to Top of Mast full Ballast(M)</td>
									<td ><input type="text" name="txtWaterlineTopMast" id="txtWaterlineTopMast" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>							
								<?php } elseif($_SESSION['selBType'] == 2) {  ?>
								<tr class="input-text">
                                	<td>Cargo Tank Capacity(CBM)</td>
									<td ><input type="text" name="txtCTankCap" id="txtCTankCap" class="input-text" style="width:140px;background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td>No. of Grades(Double V/V Seg)</td>
									<td ><input type="text" name="txtCTankNoOfGrades" id="txtCTankNoOfGrades" class="input-text" style="width:140px;background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td>Cargo Pump Capacity(CBM/Hr)</td>
									<td ><input type="text" name="txtCPumpCap" id="txtCPumpCap" class="input-text" style="width:140px;background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td>Total SBT Capacity(CBM)</td>
									<td ><input type="text" name="txtCTotalSBTCap" id="txtCTotalSBTCap" class="input-text" style="width:140px;background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Keel to top of Mast</td>
									<td ><input type="text" name="txtKeelTopMast" id="txtKeelTopMast" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<tr class="input-text">
                                	<td >Waterline to Top of Mast full Ballast</td>
									<td ><input type="text" name="txtWaterlineTopMast" id="txtWaterlineTopMast" class="input-text" style="width:140px; background-color:#E1E1E1;" readonly></td>
                                </tr>
								<?php } ?>
								
								
                             </table>
                           </td>
						   
	<!---------------------/////////----------------RIGHT SIDE PART-----------------/////////--------------------------->					   
                           <td style="padding-top:10px; width:28%; vertical-align:top;">  
								<div><strong style="font-size:13px;">Charter Party Main Terms</strong></div>
                                <table width="100%">
                                <tr class="input-text">
                                	<td >Delivery Range / Port</td>
									<td ><input type="text" name="txtDelRangePort" id="txtDelRangePort" class="input-text" style="width:140px;" placeholder="Delivery Range / Port" autocomplete="off"></td>
                                </tr>
                                <tr class="input-text">
								  <td >Duration Fixed Period&nbsp;</td>
								  <td><select name="selDurFixPeriod" id="selDurFixPeriod" class="input-text" style="width:140px;" onChange="getShowHideTextBox();">
								  <?php $obj->getDurationFixedPeriod(0); ?>
								  </select></td>
                                </tr>
								<tr class="input-text" id="tripspan_1">
								  <td >Trip TC&nbsp;</td>
								  <td><input type="text" name="txtTripTC" id="txtTripTC" class="input-text" style="width:140px;" placeholder="Trip TC" autocomplete="off"/></td>
                                </tr>
								
								<tr class="input-text" id="periodspan_1">
								  <td >Period&nbsp;</td>
								  <td><input type="text" name="txtPeriod" id="txtPeriod" style="width:140px;" class="input-text" placeholder="Period" autocomplete="off"/></td>
                                </tr>
								<tr class="input-text" id="noOfTripspan_1">
								  <td >No. Of Trips&nbsp;</td>
								  <td><input type="text" name="txtNoOfTrips" id="txtNoOfTrips" class="input-text" style="width:140px;" placeholder="No. Of Trips" autocomplete="off"/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Delivery Date/Time</td>
									<td ><input type="text" name="txtDeliveryDate" id="txtDeliveryDate" class="input-text" style="width:140px;" value="<?php echo date("d-m-Y H:i", time()); ?>" placeholder="dd-mm-yy hh:mm"/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Redelivery Date/Time</td>
									<td ><input type="text" name="txtReDeliveryDate" id="txtReDeliveryDate" class="input-text" style="width:140px;" value="<?php echo date("d-m-Y H:i", time()); ?>" placeholder="dd-mm-yy hh:mm"/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Duration Optional Period</td>
									<td ><input type="text" name="txtDurOptPer" id="txtDurOptPer" class="input-text" style="width:140px;" placeholder="Duration Optional Period" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Commencement Optional Period</td>
									<td ><input type="text" name="txtCommOptPer" id="txtCommOptPer" class="input-text" style="width:140px;" placeholder="Commencement Optional Period" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Laycan From</td>
									<td ><input type="text" name="txtLaycanFrom" id="txtLaycanFrom" class="input-text" style="width:140px;" value="<?php echo date("d-m-Y H:i", time()); ?>" placeholder="dd-mm-yy"/></td>
                                </tr>
								<tr class="input-text">
                                	<td >Laycan To</td>
									<td ><input type="text" name="txtLaycanTo" id="txtLaycanTo" class="input-text" style="width:140px;" value="<?php echo date("d-m-Y H:i", time()); ?>" placeholder="dd-mm-yy"/></td>
                                </tr>
								<?php if($_SESSION['selBType'] == 3)
								{ ?>
								<tr class="input-text">
                                	<td >Laycan Narrowing</td>
									<td ><input type="text" name="txtLaycanNarr" id="txtLaycanNarr" class="input-text" style="width:140px;" placeholder="Laycan Narrowing" autocomplete="off" /></td>
                                </tr>
								<?php } ?>
								<tr class="input-text">
                                	<td >Redelivery Range</td>
									<td ><input type="text" name="txtRedeliveryRange" id="txtRedeliveryRange" class="input-text" style="width:140px;" placeholder="Redelivery Range" autocomplete="off" /></td>
                                </tr>
                                <tr class="input-text">
                                	<td >Hire PDPR Currency</td>
									<td ><select  name="selExchangeCurrency" id="selExchangeCurrency" class="input-text" style="width:140px;" onChange="getCurrencySpan();"><?php $obj->getCurrencyList();?></select></td>
                                </tr>
                                <tr class="input-text">
                                	<td >Exchange Rate To USD</td>
									<td ><input type="text" name="txtExchangeRate" id="txtExchangeRate" class="input-text numeric" style="width:140px;" value="1" placeholder="Exchange Rate To USD" autocomplete="off" onKeyUp="getDailyGrossHire();" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Hire Fixed Period PDPR <span id="currencyspan"></span></td>
									<td ><input type="text" name="txtHireFixPerUSD" id="txtHireFixPerUSD" class="input-text numeric" style="width:140px;" placeholder="Hire Fixed Period PDPR" onKeyUp="getDailyGrossHire();" autocomplete="off" /></td>
                                </tr>
                                <tr class="input-text">
                                	<td >Hire Fixed Period PDPR (USD)</td>
									<td ><input type="text" name="txtHireFixPerUSD1" id="txtHireFixPerUSD1" class="input-text numeric" readonly style="width:140px;background-color:#E1E1E1;" placeholder="Hire Fixed Period PDPR (USD)" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Hire Optional Period</td>
									<td ><input type="text" name="txtHireOptPer" id="txtHireOptPer" class="input-text" style="width:140px;" placeholder="Hire Optional Period" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Fuel Specs</td>
									<td ><input type="text" name="txtFuelSpecs" id="txtFuelSpecs" class="input-text" style="width:140px;" placeholder="Fuel Specs" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >CVE/Month(USD)</td>
									<td ><input type="text" name="txtCVEmonth" id="txtCVEmonth" class="input-text numeric" style="width:140px;" value="1250" placeholder="CVE/Month(USD)" onKeyUp="getCVE();" autocomplete="off" /></td>
                                </tr>
								<?php if($_SESSION['selBType'] == 3)
								{ ?>
								<tr class="input-text">
                                	<td >Supercargo and meals (USD)</td>
									<td><input type="text" name="txtSupercargoMeals" id="txtSupercargoMeals" class="input-text numeric" style="width:140px;" placeholder="Supercargo and meals(USD)" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Hold Cleaning Intermediate (USD)</td>
									<td><input type="text" name="txtHoldCleanInterm" id="txtHoldCleanInterm" class="input-text numeric" style="width:140px;" placeholder="Hold Cleaning Intermediate - USD" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >ILOHC (USD)</td>
									<td ><input type="text" name="txtILOHCtcFix" id="txtILOHCtcFix" class="input-text numeric" style="width:140px;" placeholder="ILOHC - USD" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td>ILOHC - Remarks from CP</td>
									<td ><input type="text" name="txtILOCremarksCP" id="txtILOCremarksCP" class="input-text" style="width:140px;" placeholder="ILOHC - Remarks from CP" autocomplete="off" /></td>
                                </tr>
								<?php }   ?>
								<tr class="input-text">
                                	<td >Brokerage Comm. payable by</td>
									<td><select name="txtBrokCommPayableBy" id="txtBrokCommPayableBy" class="input-text " style="width:140px;"><?php $obj->getPayableByList(); ?></select></td>
                                </tr>
								<tr class="input-text">
                                	<td >Add. Comm %</td>
									<td ><input type="text" name="txtAddComm" id="txtAddComm" class="input-text numeric" style="width:140px;" value="3.75" placeholder="Add. Comm %" onKeyUp="getAddComm();" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Broker’s Comm. %</td>
									<td ><input type="text" name="txtBrokerCommTCfix" id="txtBrokerCommTCfix numeric" class="input-text" style="width:140px;" value="1.25" placeholder="Broker’s Comm. %" onKeyUp="getBrokerComm();" autocomplete="off" /></td>
                                </tr>
								<tr class="input-text">
                                	<td >Owner’s Banking Details</td>
									
									<td><select name="txtBankingDetails" id="txtBankingDetails" class="input-text" style="width:140px;"><?php $obj->getBankingDetails();?></select>
									</td>
                                </tr>
								<tr class="input-text">
                                	<td >Document created by</td>
									<td >
									<input type="text" name="txtDocCreatBy" id="txtDocCreatBy" class="input-text" style="width:140px; height:20px; background-color:#E1E1E1;" value="<?php echo $_SESSION['display']; ?>" readonly></td>
                                </tr>
								<tr class="input-text">
									<td >Additional Information</td>
									<td><textarea name="txtAddInformation" id="txtAddInformation" rows="3" cols="23" placeholder="Additional Information"></textarea>
									</td>
								</tr>
                            </table>
                           </td>
                           
                           <td width="44%" style="padding-top:10px; vertical-align:top;">  
								<div><strong style="font-size:13px;">Bunker Details</strong></div>
                                <table width="100%">
                                <tr class="input-text">
                                	<td colspan="5">&nbsp;</td>
                                </tr>
                                <tr class="input-text">
                                	<td colspan="5"><strong>Delivery</strong></td>
                                </tr>
                                <tr class="input-text">
                                   <td>#</td>
                                   <td>Bunker Grade</td>
                                   <td>Qty(MT)</td>
                                   <td>Bunker Date</td>
                                   <td>Price USD/MT)</td>
                                   <td>Amount(USD)</td>
                                </tr>
                                <tbody id="bunkerdelivery">
                                    <tr class="input-text" id="row_del_1">
                                       <td><a href="#tbl1" onClick="removeDelBunker(1);" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td>
                                       <td><select name="selDelBunker_1" class="input-text" style=" width:90px;" id="selDelBunker_1"></select></td>
                                       <td><input type="text" name="txtDelQty_1" id="txtDelQty_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtDelBunDate_1" id="txtDelBunDate_1" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                       <td><input type="text" name="txtDelPrice_1" id="txtDelPrice_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtDelAmount_1" id="txtDelAmount_1" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                    </tr>
                                    <script>$("#selDelBunker_1").html($("#selBunker").html());</script>
                                </tbody>
                                <tbody>		
                                <tr class="input-text">
                                    <td colspan="2"><button type="button" onClick="AddNewDel();">Add</button><input type="hidden" name="txtDELID" id="txtDELID" value="1"/></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ><input type="text" name="txtTotalDelAmount" id="txtTotalDelAmount" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                    
                                </tr>
                                <tr class="input-text">
                                	<td colspan="5">&nbsp;</td>
                                </tr>
                                </tbody>
                                <tr class="input-text">
                                	<td colspan="5"><strong>Re-Delivery</strong></td>
                                </tr>
                                <tr class="input-text">
                                   <td>#</td>
                                   <td>Bunker Grade</td>
                                   <td>Qty(MT)</td>
                                   <td>Bunker Date</td>
                                   <td>Price USD/MT)</td>
                                   <td>Amount(USD)</td>
                                </tr>
                                <tbody id="bunkerRedelivery">
                                    <tr class="input-text" id="row_Redel_1">
                                       <td><a href="#tbl1" onClick="removeReDelBunker(1);" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td>
                                       <td><select name="selReDelBunker_1" class="input-text" style=" width:90px;" id="selReDelBunker_1"></select></td>
                                       <td><input type="text" name="txtReDelQty_1" id="txtReDelQty_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtReDelBunDate_1" id="txtReDelBunDate_1" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
                                       <td><input type="text" name="txtReDelPrice_1" id="txtReDelPrice_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td>
                                       <td><input type="text" name="txtReDelAmount_1" id="txtReDelAmount_1" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                    </tr>
                                    <script>$("#selReDelBunker_1").html($("#selBunker").html());</script>
                                </tbody>
                                <tbody>		
                                <tr class="input-text">
                                    <td colspan="2"><button type="button" onClick="AddNewReDel();">Add</button><input type="hidden" name="txtREDELID" id="txtREDELID" value="1"/></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ><input type="text" name="txtTotalReDelAmount" id="txtTotalReDelAmount" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="margin-top:25px;"><strong style="font-size:13px;">Baltic Route Details</strong></div>
                            <table width="100%" style="margin-top:2px;">
                               <tr>
                                   <td class="input-text">Baltic Route&nbsp;:&nbsp;</td>
                                   <td class="input-text"><select name="selBalticRoute" class="input-text" style=" width:90px;" id="selBalticRoute"><?php echo $obj->getBalticRouteList();?></select></td>
                                    <td class="input-text">Baltic Route Date&nbsp;:&nbsp;</td>
                                    <td class="input-text"><input type="text" name="txtBalticDate" id="txtBalticDate" style="width:90px;" class="input-text" autocomplete="off" placeholder="dd-mm-yyyy" value=""/></td>
                                    <td class="input-text">Baltic Route Value&nbsp;:&nbsp;</td>
                                    <td class="input-text"><input type="text" name="txtBalticValue" id="txtBalticValue" autocomplete="off" style="width:90px;" class="input-text numeric" value="" /></td>
                               </tr>
                             </table>
                           </td>
                       </tr>
                       </table>
                    </div>
					
	<!-----------------ENDD-------------------------Tab_1->TC Fixture Note-------------------------------------------->

    <!-----------------START------------------Tab_2->Commercial Parameters------------------------------------------->
                    
                    <div id="tabs_2" class="tab-pane">
                         <table width="100%" style="margin-top:2px;">
                           <tr>
                               <td class="input-text">DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>&nbsp;:&nbsp;<input type="text" name="txtDWTS" id="txtDWTS" style="width:90px;" class="input-text" autocomplete="off" readonly placeholder="0.00" />
                               </td>
                               <td class="input-text">&nbsp;DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>&nbsp;:&nbsp;<input type="text" name="txtDWTT" id="txtDWTT" style="width:90px;" class="input-text" autocomplete="off" readonly value="" placeholder="0.00"/></td>
                                <td class="input-text">&nbsp;Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span><input type="text" name="txtGCap" id="txtGCap" style="width:90px;" class="input-text" autocomplete="off" readonly value="" /></td>
                                <td class="input-text">&nbsp;Bale&nbsp;Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span><input type="text" name="txtBCap" id="txtBCap" style="width:90px;" class="input-text" autocomplete="off" readonly value=""/></td>
                                <td class="input-text">SF<span style="font-size:10px; font-style:italic;">(ft3/lt)</span><input type="text" name="txtSF" id="txtSF"  autocomplete="off" style="width:90px;" class="input-text" value=""  onkeyup="getTotalDWT(),getTotalDWT1()" /></td>
                                <td class="input-text">Loadable<span style="font-size:10px; font-style:italic;">(MT)</span><input type="text" name="txtLoadable" id="txtLoadable" autocomplete="off" style="width:90px;" class="input-text" readonly value="" /></td>
                           </tr>
                         </table>
					   
                     <table cellpadding="1" cellspacing="1" border="0" width="100%"  class='tablesorter' >
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
                        <tr>
						<td width="25%" align="left" class="text">GRT/NRT&nbsp;:&nbsp;<input type="text" name="txtGNRT" id="txtGNRT" style="width:190px;" class="input-text" readonly value="" /></td>
						<td width="25%" align="left" valign="top" class="text">LOA&nbsp;:&nbsp;<input type="text" name="txtLOA" id="txtLOA" style="width:190px;" class="input-text" readonly value="" /></td>
						<td width="25%" align="left" valign="top" class="text">Gear&nbsp;:&nbsp;<input type="text" name="txtGear" id="txtGear" style="width:190px;" class="input-text" readonly value="" /></td>
						<td width="25%" align="left" valign="top" class="text">Built&nbsp;Year&nbsp;:&nbsp;<input type="text" name="txtBuiltYear" id="txtBuiltYear" style="width:190px;" class="input-text" readonly value="" /></td>
						</tr>
                        <tr>
						<td width="25%" align="left" class="text">B.E.A.M.(m)&nbsp;:&nbsp;<input type="text" name="txtBeam" id="txtBeam" style="width:190px;" class="input-text" readonly value="" /></td>
						<td width="25%" align="left" valign="top" class="text">TPC&nbsp;:&nbsp;<input type="text" name="txtTPC" id="txtTPC" style="width:190px;" class="input-text" readonly value="" /></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						</tr>
						<tr>
						<td width="25%" align="left" class="text" style="color:#dc631e;">Speed Data</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="25%" align="left" class="input-text">Ballast Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBFullSpeed" id="txtBFullSpeed" style="width:190px;" class="input-text" value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBEcoSpeed1" id="txtBEcoSpeed1" style="width:190px;" class="input-text"  value=""  onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBEcoSpeed2" id="txtBEcoSpeed2" style="width:190px;" class="input-text"  value=""  onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						</tr>
						<tr>
						<td width="25%" align="left" class="input-text">Laden Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLFullSpeed" id="txtLFullSpeed" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLEcoSpeed1" id="txtLEcoSpeed1" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLEcoSpeed2" id="txtLEcoSpeed2" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						</tr>
						</tbody>
						</table>
						</td></tr>
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">FO Consumption MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBFOFullSpeed" id="txtBFOFullSpeed" style="width:190px;" class="input-text"  value=""  onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBFOEcoSpeed1" id="txtBFOEcoSpeed1" style="width:190px;" class="input-text"  value=""  onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtBFOEcoSpeed2" id="txtBFOEcoSpeed2" style="width:190px;" class="input-text"  value=""  onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLFOFullSpeed" id="txtLFOFullSpeed" style="width:190px;" class="input-text"  value=""  onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLFOEcoSpeed1" id="txtLFOEcoSpeed1" style="width:190px;" class="input-text"  value=""  onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtLFOEcoSpeed2" id="txtLFOEcoSpeed2" style="width:190px;" class="input-text"  value=""  onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtPIFOFullSpeed" id="txtPIFOFullSpeed" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtPWFOFullSpeed" id="txtPWFOFullSpeed" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						
						</tbody>
						</table>
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">DO Consumption per MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Service Speed</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Most Eco Speed</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBDOFullSpeed" id="txtBDOFullSpeed" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBDOEcoSpeed1" id="txtBDOEcoSpeed1" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtBDOEcoSpeed2" id="txtBDOEcoSpeed2" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLDOFullSpeed" id="txtLDOFullSpeed" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLDOEcoSpeed1" id="txtLDOEcoSpeed1" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtLDOEcoSpeed2" id="txtLDOEcoSpeed2" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtPIDOFullSpeed" id="txtPIDOFullSpeed" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtPWDOFullSpeed" id="txtPWDOFullSpeed" style="width:190px;" class="input-text"  value="" onKeyUp="getVoyageTime();" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						</tbody>
						</table>
						
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						</table>
                    </div>
					
	<!-----------------ENDD------------ENDD------Tab_2->Commercial Parameters-------------------ENDD---------------->
    
	<!-----------------START-----------------Tab_3->TC/CP TERMS-------------------------------------------------------->
					

                    <div id="tabs_3" class="tab-pane" style="overflow:auto;">
                        <table width="100%">
                        <tr >
                            <td colspan="8"><div><strong style="font-size:13px;">TC CP Terms : Sea Passage</strong></div>  </td>
                        </tr>
                        <tr class="input-text">
                            <td>Wind Force : </td>
                            <td><input autocomplete="off" type="text" name="txtWindForce" id="txtWindForce" class="input-text" size="21" placeholder="0.00" value=""/></td>
                            <td>Speed Laden(Kts) : </td>
                            <td><input autocomplete="off" type="text" name="txtSpeedLaden" id="txtSpeedLaden" class="input-text" size="21" placeholder="0.00" value=""/></td>
                            <td>Speed Ballast(Kts) : </td>
                            <td><input autocomplete="off" type="text" name="txtSpeedBallast" id="txtSpeedBallast" class="input-text" size="21" placeholder="0.00" value=""/></td>
                            <td>CP Speed</td>
                            <td><input autocomplete="off" type="text" name="txtCPSpeed" id="txtCPSpeed" class="input-text" size="21" placeholder="0.00" value=""/></td>
                        </tr>
                        <tr class="input-text">
                        	<td>FO Cons Laden(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtFOConsLaden" id="txtFOConsLaden" class="input-text" size="21" placeholder="0.00" value=""/></td>
                            <td>DO Cons Laden(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtDOConsLaden" id="txtDOConsLaden" class="input-text" size="21" placeholder="0.00" value=""/></td>
                            <td>FO Cons Ballast(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtFOConsBallast" id="txtFOConsBallast" class="input-text" size="21" placeholder="0.00" value=""/></td>
                            <td>DO Cons Ballast(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtDOConsBallast" id="txtDOConsBallast" class="input-text" size="21" placeholder="0.00" value=""/></td>
                        </tr>
                        <tr >
                            <td colspan="8"><div><strong style="font-size:13px;">TC CP Terms : Port</strong></div>  </td>
                        </tr>
                        <tr class="input-text">
                        	<td>FO Cons Ldg(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtFOConsLdg" id="txtFOConsLdg" class="input-text" size="21" placeholder="0.00" value=""/></td>
                            <td>DO Cons Ldg(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtDOConsLdg" id="txtDOConsLdg" class="input-text" size="21" placeholder="0.00" value="" /></td>
                            <td>FO Cons Disch(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtFOConsDisch" id="txtFOConsDisch" class="input-text" size="21" placeholder="0.00" value="" /></td>
                            <td>DO Cons Disch(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtDOConsDisch" id="txtDOConsDisch" class="input-text" size="21" placeholder="0.00" value="" /></td>
                        </tr>
                        <tr class="input-text">
                        	<td>FO Cons Idle(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtFOConsIdle" id="txtFOConsIdle" class="input-text" size="21" placeholder="0.00" value="" /></td>
                            <td>DO Cons Idle(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtDOConsIdle" id="txtDOConsIdle" class="input-text" size="21" placeholder="0.00"value="" /></td>
                            <td>Load Rate(MT/Day) : </td>
                            <td><input autocomplete="off" type="text" name="txtLoadRate" id="txtLoadRate" class="input-text" size="21" placeholder="0.00" value="" /></td>
                            <td>Disch Rate(MT/Day)</td>
                            <td><input autocomplete="off" type="text" name="txtDischRate" id="txtDischRate" class="input-text" size="21" placeholder="0.00"value="" /></td>
                        </tr>
                        </table>
                    </div>
                    
	<!-----------------ENDD------------ENDD------Tab_2->TC/CP TERMS--------------------------------ENDD---------------->
                    
                    </div><!-- /.tab-content -->
                </div>
                <div style="text-align:center; margin-bottom:50px;">
                	<input type="hidden" name="txtCRMFILE" id="txtCRMFILE" value="" />
					<input type="hidden" name="txtCRMNAME" id="txtCRMNAME" value="" />
					<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
					<input type="hidden" name="action" id="action" value="submit" />
					<input type="hidden" name="vesselrec" id="vesselrec" class="input-text" value="" />
				</div>
                <!-- Main content -->
                </form>
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/sort.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />
<script type="text/javascript">
$(document).ready(function(){ 
	$('#tripspan_1,#periodspan_1,#noOfTripspan_1').hide();
	$(".numeric").numeric();	
	$("#txtSupercargoMeals,#txtHoldCleanInterm,#txtILOHCtcFix,#txtAddComm,#txtBrokerCommTCfix,#txtNoOfTrips,#txtHireFixPerUSD,#txtWindForce,#txtSpeedLaden,#txtSpeedBallast,#txtCPSpeed,#txtFOConsLaden,#txtDOConsLaden,#txtFOConsBallast,#txtDOConsBallast,#txtFOConsLdg,#txtDOConsLdg,#txtFOConsDisch,#txtDOConsDisch,#txtFOConsIdle,#txtDOConsIdle,#txtLoadRate,#txtDischRate").numeric();
	
	$("#txtDate,#txtDeldate,#txtDeldate2,[id^=txtDelBunDate_],[id^=txtReDelBunDate_],#txtBalticDate").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
	
	$("#txtCPdate").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
		}).on('changeDate', function(){ $("#txtCPdate2").val($(this).val());
	});
	
	
	$('#txtDeliveryDate,#txtReDeliveryDate').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
	});
	 
	 
	 $('#txtLaycanFrom').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
	 }).on('changeDateTime', function(){
		$('#txtLaycanTo').datetimepicker('setStartDateTime', new Date(getString($(this).val())));
	 });
	 
	$('#txtLaycanTo').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
	 }).on('changeDateTime', function(){   });
	
	function getString(var1)
	{
	  var var2 = var1.split('-');
	  return var2[2]+'/'+var2[1]+'/'+var2[0];
	}
	
	$("#frm1").validate({
		rules: {
			selVName:"required",
			txtTCNo:"required",
			selDurFixPeriod:"required"
			},
		messages: {
			selVName:"*",
			},
	submitHandler: function(form)  {
		jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
				if(r){
					jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
					$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
					$("#popup_content").css({"background":"none","text-align":"center"});
					$("#popup_ok,#popup_title").hide();  
					frm1.submit();
					}
					else{return false;}
				});
		}
	});
getCurrencySpan();
});

function getCurrencySpan()
{
	if($("#selExchangeCurrency").val()!="")
	{
		$("#currencyspan").text("("+$("#selExchangeCurrency").val()+")");
	}
	else
	{
		$("#currencyspan").text('');
	}
}

function getValue()
{
	if($("#selVName").val()!="")
	{
		$("#lblVesName").text($("#selVName option:selected").text());
	}
	else
	{
		$("#lblVesName").text('');
	}
}


function getData()
{
	  if($("#selVName").val()!="")
	  {
		  $.post("options.php?id=42",{vessel1d:""+$("#selVName").val()+""}, function(data) 
			{
				var vsldata = JSON.parse(data);
				$.each(vsldata[$("#selVName").val()], function(index, array) {
				
					$('#txtVType').val(array['type']);
					$('#txtDWTS').val(array['dwtsum']);
					$('#txtDWTT').val(array['dwttrop']);
					$('#txtGCap').val(array['grain']);
					$('#txtBCap').val(array['bale']);
					
					$('#txtBFullSpeed').val(array['bfs']);
					$('#txtBEcoSpeed1').val(array['bes1']);
					$('#txtBEcoSpeed2').val(array['bes2']);
					$('#txtBFOFullSpeed').val(array['fobfs']);
					$('#txtBFOEcoSpeed1').val(array['fobes1']);
					$('#txtBFOEcoSpeed2').val(array['fobes2']);
					$('#txtBDOFullSpeed').val(array['dobfs']);
					$('#txtBDOEcoSpeed1').val(array['dobes1']);
					$('#txtBDOEcoSpeed2').val(array['dobes2']);
					
					$('#txtLFullSpeed').val(array['lfs']);
					$('#txtLEcoSpeed1').val(array['les1']);
					$('#txtLEcoSpeed2').val(array['les2']);
					$('#txtLFOFullSpeed').val(array['folfs']);
					$('#txtLFOEcoSpeed1').val(array['foles1']);
					$('#txtLFOEcoSpeed2').val(array['foles2']);
					$('#txtLDOFullSpeed').val(array['dolfs']);
					$('#txtLDOEcoSpeed1').val(array['doles1']);
					$('#txtLDOEcoSpeed2').val(array['doles2']);
					
					$('#txtPIFOFullSpeed').val(array['foidle']);
					$('#txtPWFOFullSpeed').val(array['fwking']);
					$('#txtPIDOFullSpeed').val(array['ddle']);
					$('#txtPWDOFullSpeed').val(array['dwking']);
					$('#vesselrec').val(array['num']);
					$('#txtFlag').val(array['flag']);
					$('#txtBuiltYear').val(array['year']);
					$('#txtGNRT').val(array['gnrt']);
					$('#txtLOA').val(array['loa']);
					$('#txtGear').val(array['gear']);
					$('#txtBeam').val(array['beam']);
					$('#txtTPC').val(array['tpc']);
					
					$('#txtBuildYard').val(array['BuildYard']);
					$('#txtYearBuild').val(array['year']);
					$('#txtFlag1').val(array['flag']);
					$('#txtPortOfRegis').val(array['txtPortOfRegis']);
					$('#txtIMOnum').val(array['txtIMOnum']);
					$('#txtOwnerPandI').val(array['txtOwnerPandI']);
					$('#txtCallSign').val(array['txtCallSign']);
					$('#txtInmarsatTel').val(array['txtInmarsatTel']);
					$('#txtInmarsatEmail').val(array['txtInmarsatEmail']);
					$('#txtLOA1').val(array['loa']);
					$('#txtBreadth').val(array['beam']);
					$('#txtSummerDWT').val(array['dwtsum']);
					$('#txtSummerDraft').val(array['txtSummerDraft']);
					$('#txtTPC1').val(array['tpc']);
					$('#txtClassID').val(array['txtClassID']);
					$('#txtLastSpSurvey').val(array['txtLastSpSurvey']);
					$('#txtLastDD').val(array['txtLastDD']);
					$('#txtGrossTonnage').val(array['GrossTonnage']);
					$('#txtNetTonnage').val(array['NetTonnage']);
					$('#txtSuezGRT').val(array['txtSuezGRT']);
					$('#txtSuezNRT').val(array['txtSuezNRT']);
					$('#txtPanamaNRT').val(array['PanamaNRT']);
					$('#txtGrainCap').val(array['txtGrainCap']);
					$('#txtBaleCap').val(array['txtBaleCap']);
					$('#txtCranes').val(array['txtCranes']);
					$('#txtGrabs').val(array['txtGrabs']);
					$('#txtKeelTopMast').val(array['KeelTopMast']);
					$('#txtWaterlineTopMast').val(array['WaterlineTopMast']);
					$('#txtCTankCap').val(array['CTankCap']);
					$('#txtCTankNoOfGrades').val(array['CTankNoOfGrades']);
					$('#txtCPumpCap').val(array['CPumpCap']);
					$('#txtCTotalSBTCap').val(array['CTotalSBTCap']);
				});
			});  
	  }
	  else
	  {
		  $('#txtVType,#txtDWTS,#txtDWTT,#txtGCap,#txtBCap,#txtBFullSpeed,#txtBEcoSpeed1,#txtBEcoSpeed2,#txtBFOFullSpeed,#txtBFOEcoSpeed1,#txtBFOEcoSpeed2,#txtBDOFullSpeed,#txtBDOEcoSpeed1,#txtBDOEcoSpeed2,#txtLFullSpeed,#txtLEcoSpeed1,#txtLEcoSpeed2,#txtLFOFullSpeed,#txtLFOEcoSpeed1,#txtLFOEcoSpeed2,#txtLDOFullSpeed,#txtLDOEcoSpeed1,#txtLDOEcoSpeed2,#txtPIFOFullSpeed,#txtPWFOFullSpeed,#txtPIDOFullSpeed,#txtPWDOFullSpeed,#vesselrec,#txtFlag,#txtGear,#txtBuiltYear,#txtGNRT,#txtLOA,#txtBuildYard,#txtYearBuild,#txtFlag1,#txtPortOfRegis,#txtIMOnum,#txtOwnerPandI,#txtCallSign,#txtInmarsatTel,#txtInmarsatEmail,#txtLOA1,#txtBreadth,#txtSummerDWT,#txtSummerDraft,#txtTPC1,#txtClassID,#txtLastSpSurvey,#txtLastDD,#txtGrossTonnage,#txtNetTonnage,#txtSuezGRT,#txtSuezNRT,#txtPanamaNRT,#txtGrainCap,#txtBaleCap,#txtCranes,#txtGrabs,#txtKeelTopMast,#txtWaterlineTopMast').val("");
	  }
	//  getFinalCalculation();
}

function getDistance(i)
{
	
		$("#txtDistance_"+i).val("");
		$("#ploader_"+i).show();
		var loadporttext = disporttext = "";
		if($("#selFPort_"+i).val()!=""){loadporttext = $("#selFPort_"+i+" option:selected").text();}else{loadporttext = "";}
		if($("#selTPort_"+i).val()!=""){disporttext = $("#selTPort_"+i+" option:selected").text();}else{disporttext = "";}
		$("#spanLoadPort_"+i).html(loadporttext);
		$("#spanDisPort_"+i).html(disporttext);
		$("#TranDisPort_"+i).html(loadporttext);
		$("#spanDDCLPort_"+i).html(loadporttext);
		$("#spanDDCDPort_"+i).html(disporttext);
		if($('#selFPort_'+i).val() != "" && $('#selTPort_'+i).val() != "" && $('#selDType_'+i).val() != "")
		{
			$("#txtDistance_"+i).val(0);
			$.post("options.php?id=10",{selFPort:""+$("#selFPort_"+i).val()+"",selTPort:""+$("#selTPort_"+i).val()+"",selDType:""+$("#selDType_"+i).val()+""}, function(data) 
			{
					$('#txtDistance_'+i).val(data);
					$("#ploader_"+i).hide();
					getVoyageTime();
			});
		}
		else
		{
			$('#txtDistance_'+i).val(0);
			$("#ploader_"+i).hide();
			getVoyageTime();
		}
}


function getValidate()
{
	getFinalCalculation();
	
}


function getTimeDiff(date2,date1)
{	
	if(date1 != '' &&  date2 != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat(date2);
		start_actual_time    =  getDateWithSpecificFormat(date1);
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000;
		return days.toFixed(5);	
	}
}
 
function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' '+dateSplit1[1];
	return currentDate;
}

function getChartererData()
{
	if($("#selCharOperation").val()!='')
	{
		$.post("options.php?id=59",{selCharOperation:""+$("#selCharOperation").val()+""}, function(data) 
			{
					$('#txtAddress').val(data);

			});
	}
	else
	{
		$('#txtAddress').val('');
	}
}

function getCPTypeOther()
{
	if($("#selCPType").val()!='')
	{
		$('#txtCPType').val($("#selCPType option:selected").text());
	}
	else
	{
		$('#txtCPType').val('');
	}
}

function getCharterersOther()
{
	if($("#selCharterers").val()!='')
	{
		$('#txtCharterers').val($("#selCharterers option:selected").text());
	}
	else
	{
		$('#txtCharterers').val('');
	}
}

function getShowHideTextBox()
{
	if($("#selDurFixPeriod").val() == 1)
	{
		$('#tripspan_1').show();
		$('#periodspan_1,#noOfTripspan_1').hide();
	}
	else if($("#selDurFixPeriod").val() == 2)
	{
		$('#periodspan_1,#noOfTripspan_1').show();
		$('#tripspan_1').hide();
	}
	else
	{
		$('#tripspan_1,#periodspan_1,#noOfTripspan_1').hide();
	}
}

function getDailyGrossHire()
{
	if($("#txtHireFixPerUSD").val()!='' && $("#txtExchangeRate").val()!='')
	{
		$('#txtHireFixPerUSD1').val(parseFloat($("#txtHireFixPerUSD").val()*$("#txtExchangeRate").val()).toFixed(2));
	}
	else
	{
		$('#txtHireFixPerUSD1').val(0);
	}
}

function getAddComm()
{
	if($("#txtAddComm").val()!='')
	{
		$('#txtAddCommPerct').val($("#txtAddComm").val());
	}
	else
	{
		$('#txtAddCommPerct').val('');
	}
}

function getBrokerComm()
{
	if($("#txtBrokerCommTCfix").val()!='')
	{
		$('#txtBrokerComm').val($("#txtBrokerCommTCfix").val());
	}
	else
	{
		$('#txtBrokerComm').val('');
	}
}

function getCVE()
{
	if($("#txtCVEmonth").val()!='')
	{
		$('#txtCVE').val($("#txtCVEmonth").val());
	}
	else
	{
		$('#txtCVE').val('');
	}
}


function removeDelBunker(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_del_"+var1).remove();			
		 }
	else{return false;}
	});
}


function AddNewDel()
{
	var id = $("#txtDELID").val();
	if($("#selDelBunker_"+id).val() != "" && $("#txtDelQty_"+id).val() != "" && $("#txtDelBunDate_"+id).val() != "" && $("#txtDelPrice_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr class="input-text" id="row_del_'+id+'"><td><a href="#tbl'+id+'" onClick="removeDelBunker('+id+');" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td><td><select name="selDelBunker_'+id+'" class="input-text" style=" width:90px;" id="selDelBunker_'+id+'"></select></td><td><input type="text" name="txtDelQty_'+id+'" id="txtDelQty_'+id+'" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td><td><input type="text" name="txtDelBunDate_'+id+'" id="txtDelBunDate_'+id+'" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td><td><input type="text" name="txtDelPrice_'+id+'" id="txtDelPrice_'+id+'" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getDelBunkerCalculation();" autocomplete="off" /></td><td><input type="text" name="txtDelAmount_'+id+'" id="txtDelAmount_'+id+'" class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" readonly="true" autocomplete="off"/></td></tr>').appendTo("#bunkerdelivery");
		$("#txtDELID").val(id);
		$(".numeric").numeric();	
		$("#selDelBunker_"+id).html($("#selBunker").html());
		$("[id^=txtDelBunDate_]").datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true
		});
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function getDelBunkerCalculation()
{
	var txtDELID = $("#txtDELID").val();
	for(var i =1;i <=txtDELID;i++)
	{
		var txtDelQty      = parseFloat($("#txtDelQty_"+i).val());if(isNaN(txtDelQty)){txtDelQty = 0.00;}
		var txtDelPrice    = parseFloat($("#txtDelPrice_"+i).val());if(isNaN(txtDelPrice)){txtDelPrice = 0.00;}
		var amount          =   parseFloat(parseFloat(txtDelQty)*parseFloat(txtDelPrice));if(isNaN(amount)){amount = 0.00;}
		$('#txtDelAmount_'+i).val(parseFloat(amount).toFixed(2));
	}
	$("#txtTotalDelAmount").val($('[id^=txtDelAmount_]').sum());
}

function getReDelBunkerCalculation()
{
	var txtREDELID = $("#txtREDELID").val();
	for(var i =1;i <=txtREDELID;i++)
	{
		var txtReDelQty      = parseFloat($("#txtReDelQty_"+i).val());if(isNaN(txtReDelQty)){txtReDelQty = 0.00;}
		var txtReDelPrice    = parseFloat($("#txtReDelPrice_"+i).val());if(isNaN(txtReDelPrice)){txtReDelPrice = 0.00;}
		var amount          =   parseFloat(parseFloat(txtReDelPrice)*parseFloat(txtReDelQty));if(isNaN(amount)){amount = 0.00;}
		$('#txtReDelAmount_'+i).val(parseFloat(amount).toFixed(2));
	}
	$("#txtTotalReDelAmount").val($('[id^=txtReDelAmount_]').sum());
}


function removeReDelBunker(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_Redel_"+var1).remove();		
		 }
	else{return false;}
	});
}


function AddNewReDel()
{
	var id = $("#txtREDELID").val();
	if($("#selReDelBunker_"+id).val() != "" && $("#txtReDelQty_"+id).val() != "" && $("#txtReDelBunDate_"+id).val() != "" && $("#txtReDelPrice_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr class="input-text" id="row_Redel_'+id+'"><td><a href="#tbl'+id+'" onClick="removeReDelBunker('+id+');" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td><td><select name="selReDelBunker_'+id+'" class="input-text" style=" width:90px;" id="selReDelBunker_'+id+'"></select></td><td><input type="text" name="txtReDelQty_'+id+'" id="txtReDelQty_'+id+'" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td><td><input type="text" name="txtReDelBunDate_'+id+'" id="txtReDelBunDate_'+id+'" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td><td><input type="text" name="txtReDelPrice_'+id+'" id="txtReDelPrice_'+id+'" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getReDelBunkerCalculation();" autocomplete="off" /></td><td><input type="text" name="txtReDelAmount_'+id+'" id="txtReDelAmount_'+id+'" class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" readonly="true" autocomplete="off"/></td></tr>').appendTo("#bunkerRedelivery");
		$("#txtREDELID").val(id);
		$(".numeric").numeric();	
		$("#selReDelBunker_"+id).html($("#selBunker").html());
		$("[id^=txtReDelBunDate_]").datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true
		});
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

</script>
</body>
</html>