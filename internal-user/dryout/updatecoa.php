<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$id = $_REQUEST['id'];
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->updateCOADetails($id);
	header('Location:./coa_list.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
$rigts    = explode(',',$obj->getUserRights($_SESSION['uid'],$_SESSION['moduleid'],14));
$obj->viewCOARecords($id);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
<?php $display->js(); ?>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(14); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-truck"></i>&nbsp;Contract of Affreightment&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Contract of Affreightment</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right">
				<!--<a href="allPdf.php?id=50&coaid=<?php echo $id; ?>" title="Pdf"><button class="btn btn-default"><i class="fa fa-download"></i> Generate PDF</button></a>&nbsp;&nbsp;&nbsp;-->
				<a href="coa_list.php"><button class="btn btn-info btn-flat">Back</button></a>
				</div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">	
				<select id="hiddenSel1" name="hiddenSel1" style='display:none;'/>	
				<?php $obj->getPortList(); ?>
				</select>			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            UPDATE COA
                            </h2>                            
                        </div><!-- /.col -->
                </div>
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                            COA ID
                            <address>
                               <input type="text" name="txtCOAid" id="txtCOAid" style="background-color:#E1E1E1;" class="form-control"  placeholder="COA ID" autocomplete="off" value="<?php echo $obj->getFun1(); ?>" readonly/>
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
                            COA No.
                            <address>
                               <input type="text" name="txtCOANo" id="txtCOANo" class="form-control"  placeholder="COA No." autocomplete="off" value="<?php echo $obj->getFun7(); ?>"/>
                            </address>
                        </div>
						<?php if($obj->getFun4()=="" || $obj->getFun4()=="0000-00-00"  || $obj->getFun4()=="1970-01-01"){$txtCOADate = "";}else{$txtCOADate = date('d-m-Y',strtotime($obj->getFun4()));}?>
						<div class="col-sm-3 invoice-col">
						    COA Date
                            <address>
                               <input type="text" name="txtCOADate" id="txtCOADate" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy" value="<?php echo $txtCOADate; ?>" />
                            </address>
                        </div>
						<div class="col-sm-3 invoice-col">
                            COA Route
                            <address>
                                <select  name="selCOAroute" class="form-control" id="selCOAroute">
								<?php 
                                $obj->getCOArouteNamesList();
                                ?>
                                </select>
                            </address>
                        </div>
					</div>
                    <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                          Charterer
                            <address>
                                <select  name="selCharterer" class="form-control" id="selCharterer">
								<?php 
								$obj->getVendorListNewForCOA(7);
								?>
                                </select>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                          Owner
                            <address>
                                <select  name="selOwner" class="form-control" id="selOwner">
								<?php 
                                $obj->getVendorListNewForCOA(11);
                                ?>
                                </select>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                          Broker
                            <address>
                                <select  name="selBroker" class="form-control" id="selBroker">
								<?php 
								$obj->getVendorListNewForCOA(12);
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
					</div>
                    <div class="row invoice-info">	
                        <div class="col-sm-3 invoice-col">
                           Business Type
                            <address>
                               <select  name="selBType" class="form-control required" id="selBType" onChange="getVesselTypeList(),getCargoTypList();">
                               <option value="">----Select from list----</option>
								<?php 
                                $obj->getBusinessTypeList1();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->					
                        <div class="col-sm-3 invoice-col">
                          Vessel Type
                            <address>
                                <select  name="selVesselType" class="form-control" id="selVesselType">
								<?php 
								$obj->getVesselTypeList();
								?>
                                </select>
                            </address>
                        </div>
						<div class="col-sm-3 invoice-col">
                          Load Options
                            <address>
                                <select  name="selLoadOpt" class="form-control" id="selLoadOpt">
								<?php 
								$obj->getLoadOptionsNameList();
								?>
                                </select>
                            </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
                            Total Shipments(No.'s)
                            <address>
                               <input type="text" name="txtNoofShipment" id="txtNoofShipment" class="form-control"  placeholder="Total Shipments(No.'s)" autocomplete="off" value="<?php echo $obj->getFun9(); ?>"/>
                            </address>
                        </div>
					</div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Cargo
                            <address>
                                <select  name="selCargo" class="form-control" id="selCargo">
								<?php 
								$obj->getCargoNameList();
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Tolerance ( % )
                            <address>
                               <input type="text" name="txtTolerance" id="txtTolerance" class="form-control" autocomplete="off" placeholder="Tolerance ( % )" value="<?php echo $obj->getFun14(); ?>" />
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
                            COA Notice(Days)
                            <address>
                                <input type="text" name="txtCOAnoticeDays" id="txtCOAnoticeDays" class="form-control"  placeholder="COA Notice(Days)" autocomplete="off" value="<?php echo $obj->getFun15(); ?>" />
                             </address>
                        </div>
                    </div>
					 
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                            Min Guaranteed Qty(MT)
                            <address>
                                <input type="text" name="txtMinGuarQty" id="txtMinGuarQty" class="form-control"  placeholder="Min Guaranteed Qty(MT)" autocomplete="off" value="<?php echo $obj->getFun16(); ?>" />
                             </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                           Load Port ETA Notices
                            <address>
                                <input type="text" name="txtLdPrtETAnot" class="form-control" id="txtLdPrtETAnot" placeholder="Load Port ETA Notices" autocomplete="off" value="<?php echo $obj->getFun17(); ?>"/>
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
                           Vessel Substitutions
                            <address>
                                <select  name="selVslSubs" class="form-control" id="selVslSubs">
								<?php 
								$obj->getVesselSubstitutionList();
								?>
                                </select>
                            </address>
                        </div>
                    </div>
					<div class="box-body table-responsive">	
						<table width="100%" class="table table-bordered table-striped">
							
							<tr>
								<td></td>
								<td>Min Guaranteed Qty(CBM or MT)</td>
								<td>Ex - Port</td>
								
							</tr>
						
						<tbody id="minGuarenteedBody">
						<?php $sql1 	= "SELECT * FROM coa_master_s1 WHERE STATUS=1 AND COAID='".$obj->getFun32()."'";
							  $result1  = mysql_query($sql1);
							  $num1 	= mysql_num_rows($result1);
							  $k 		= 0;
							  if($num1==0){$k++;
						?>
						<tr id="minGuarenteedBodyRow_1">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;<a href="#tb1" onClick="removeMinGuarenteedBody(1);" ><i class="fa fa-times" style="color:red;"></i></a>
							</td>
							<td>
								<input type="text" name="txtminGuarenteed_1" id="txtminGuarenteed_1" class="form-control" autocomplete="off" value="" placeholder="Min Guaranteed Vol/Voyage(CBM)" />
							</td>
							<td>
								<select name="selExPort_1" id="selExPort_1" class="form-control">
								<?php 
								$obj->getPortList();
								?>
								</select>
							</td>
						</tr>	
						
						<?php }else{
							while($rows1 = mysql_fetch_assoc($result1))
							{$k++;
						?>
						<tr id="minGuarenteedBodyRow_<?php echo $k;?>" style="margin-top:10px;">
							<td>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#tb1" onClick="removeMinGuarenteedBody(<?php echo $k;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
							<td><input type="text" name="txtminGuarenteed_<?php echo $k;?>" id="txtminGuarenteed_<?php echo $k;?>" class="form-control" autocomplete="off" value="<?php echo $rows1['MIN_GUARANTEED']; ?>" placeholder="Min Guaranteed Vol/Voyage(CBM)" /></td>
							<td><select name="selExPort_<?php echo $k;?>" id="selExPort_<?php echo $k;?>" class="form-control"><?php $obj->getPortList(); ?> </select>
							<script>$("#selExPort_<?php echo $k;?>").val('<?php echo $rows1['EX_PORT']; ?>');</script>
							</td>
							
						</tr>
						<?php }}?>
						</tbody>
						
						<tfoot>	
						<tr>
							<td colspan="3" style="padding-top:10px;">
									<button type="button" class="btn btn-primary btn-flat" onClick="addOthMinGuarenteedBody();">Add</button>
									<input type="hidden" name="hddnOtherID" id="hddnOtherID" value="<?php echo $k;?>"/>
								
							</td>
						</tr>
						</tfoot>
					  </table>
					</div>
                    <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							COA Duration
							<address>
							   <textarea class="form-control areasize" name="txtCOAduration" id="txtCOAduration" rows="1" placeholder="COA Duration" ><?php echo $obj->getFun19();?></textarea>
							</address>
						</div><!-- /.col -->
						<?php if($obj->getFun20()=="" || $obj->getFun20()=="0000-00-00"  || $obj->getFun20()=="1970-01-01"){$txtStartDate = "";}else{$txtStartDate = date('d-m-Y',strtotime($obj->getFun20()));}?>
						<div class="col-sm-3 invoice-col">
                           Start Date
                            <address>
                               <input type="text" name="txtStartDate" id="txtStartDate" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy" value="<?php echo $txtStartDate; ?>" />
                            </address>
                        </div><!-- /.col -->
						<?php if($obj->getFun21()=="" || $obj->getFun21()=="0000-00-00"  || $obj->getFun21()=="1970-01-01"){$txtEndDate = "";}else{$txtEndDate = date('d-m-Y',strtotime($obj->getFun21()));}?>
						<div class="col-sm-3 invoice-col">
                           	End Date
                            <address>
                               <input type="text" name="txtEndDate" id="txtEndDate" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy" value="<?php echo $txtEndDate; ?>" />
                            </address>
                        </div>
                        <div class="col-sm-2 invoice-col">
                           	Working Currency
                            <address>
                               <select  name="selCurrency" class="select form-control" id="selCurrency" onChange="getCurrency();"><?php $obj->getCurrencyList();?></select>
                            </address>
                        </div>
					</div>
                    <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Freight Details
							<address>
							   <textarea class="form-control areasize" name="txtFreightDetails" id="txtFreightDetails" rows="2" placeholder="Freight Details"><?php echo $obj->getFun22();?></textarea>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Load Port Details
							<address>
							   <input type="text" class="form-control" name="txtLPDetails" id="txtLPDetails" placeholder="Load Port Details" autocomplete="off" value="<?php echo $obj->getFun23();?>" />
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							Discharge Port Details
							<address>
							   <input type="text" class="form-control" name="txtDPDetails" id="txtDPDetails" placeholder="Discharge Port Details" autocomplete="off" value="<?php echo $obj->getFun24();?>"/>
							</address>
						</div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Contract FO Price(<span id="curr_1">USD</span>/MT)
							<address>
							   <input type="text" class="form-control numeric" name="txtFOPrice" id="txtFOPrice" value="<?php echo $obj->getFun35();?>" placeholder="0.00" autocomplete="off" />
							</address>
						</div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
							BAF
							<address>
							   <input type="text" class="form-control numeric" name="txtBAF" id="txtBAF" value="<?php echo $obj->getFun36();?>" placeholder="0.00" autocomplete="off" />
							</address>
						</div><!-- /.col -->
						
					</div>
                    
                    <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							Demurrage & Laytime
							<address>
                               <textarea class="form-control areasize" name="txtDemLaytime" id="txtDemLaytime" rows="2" placeholder="Demurrage & Laytime"><?php echo $obj->getFun25();?></textarea>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-8 invoice-col">
						Overall Remarks
						<address>
						   <textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" rows="3" placeholder="Overall Remarks..." ><?php echo $obj->getFun26();?></textarea>
						</address>
						</div><!-- /.col -->
					</div>
                     <div class="row invoice-info" style="width:100%;float:left;padding-left:1.5%;">
						Attach COA recap<br>
						<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" style="width:23%;">
                        <i class="fa fa-paperclip"></i> Attachment
                        <input type="file" class="form-control" multiple name="attach_file[]" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                        </div>
				    </div>
					<?php if($obj->getFun28() != '')
                            { 
                                $file = explode(",",$obj->getFun28()); 
                                $name = explode(",",$obj->getFun29()); ?>
						<table>									
                            <tr><td colspan="4" align="left">Previous Attachments:</td></tr>
                                <?php
                                $j =1;
                                for($i=0;$i<sizeof($file);$i++)
                                {
                                ?>
                                <tr id="row_file_<?php echo $j;?>">
                                    <td width="40%" align="left" class="input-text"  valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../../attachment/<?php echo $file[$i]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="<?php echo $name[$i];?>"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $name[$i]; ?></a>
                                    <input type="hidden" name="file_<?php echo $j;?>" id="file_<?php echo $j;?>" value="<?php echo $file[$i]; ?>" />
                                    <input type="hidden" name="name_<?php echo $j;?>" id="name_<?php echo $j;?>" value="<?php echo $name[$i]; ?>" />
                                    </td>
                                    <td ><a href="#1" onClick="Del_Upload(<?php echo $j;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
                                </tr>
                                <?php $j++;}?>
						</table>
					
                     	<?php }?>
						
						<div class="box-footer" align="right">
						<input type="hidden" name="txtCRMFILE" id="txtCRMFILE" value="" />
						<input type="hidden" name="txtCRMNAME" id="txtCRMNAME" value="" />
						<?php if($obj->getFun27()==0 && in_array(2, $rigts)){?>
                            <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Save as Draft</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-success btn-flat" onClick="return getValidate(1);">Send to Checker</button> 
                        <?php }?>
                        <?php if($obj->getFun27()==1 && in_array(3, $rigts)){?>
                            <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(1);">Send to Creator</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-success btn-flat" onClick="return getValidate(2);">Submit to Close</button>
                        <?php }?>
						 <input type="hidden" name="action" value="submit" />
						 <input type="hidden" name="upstatus" id="upstatus" value="" />
						</div>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#selCOAroute").val('<?php echo $obj->getFun8();?>');
$("#selCharterer").val('<?php echo $obj->getFun5();?>');
$("#selOwner").val('<?php echo $obj->getFun6();?>');
$("#selBroker").val('<?php echo $obj->getFun10();?>');
$("#selLoadOpt").val('<?php echo $obj->getFun12();?>');
$("#selVslSubs").val('<?php echo $obj->getFun18();?>');
$("#selCurrency").val('<?php echo $obj->getFun33();?>');
$("#selBType").val('<?php echo $obj->getFun34();?>');
$("#txtCQty,#txtMinGuarQty,#txtSQty,#txtTolerance,#txtNoofShipment,#txtPeriodDays,#txtCOAnoticeDays").numeric();

	$('#txtEndDate,#txtStartDate,#txtCOADate').datepicker({
		format: 'dd-mm-yyyy',
		autoclose:true
	});
	
	$(".areasize").autosize({append: "\n"});

	$("#frm1").validate({
		rules: {
			txtCOADate:{required:true},
			txtCOAid:{required:true},
			selCharterer:{required:true},
			txtSQty:{required:true},
			txtCOANo:{required:true},
			selOwner:{required:true},
			txtNoofShipment:{required:true},
			selBroker:{required:true},
			txtCQty:{required:true},
			selVesselType:{required:true},
			txtPeriodDays:{required:true},
			selCargo:{required:true},
			txtStartDate:{required:true},
			txtEndDate:{required:true},
			selCurrency:{required:true}
			},
		messages: {
			
			},
			submitHandler: function(form)  {
					jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
				if(r){
					jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
					$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
					$("#popup_content").css({"background":"none","text-align":"center"});
					$("#popup_ok,#popup_title").hide();  
					frm1.submit();
					}
					else{return false;}
				});
			}
		});
getVesselTypeList();
getCargoTypList();
getCurrency();
$("#selVesselType").val('<?php echo $obj->getFun11();?>');
$("#selCargo").val('<?php echo $obj->getFun13();?>');
});


function getVesselTypeList()
{
    var vesseltypelist = <?php echo $obj->getVesselTypeListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selVesselType');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selVesselType");
	$.each(vesseltypelist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selVesselType");
	});
}


function getCargoTypList()
{
    var cargolist = <?php echo $obj->getCargoListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selCargo');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selCargo");
	$.each(cargolist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selCargo");
	});
}

function getValidate(val)
{
	var var1 = $("[id^=file_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRMFILE').val(var1);
	
	var var2 = $("[id^=name_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRMNAME').val(var2);
	$("#upstatus").val(val);
	return true;
}

function addOthMinGuarenteedBody()
{
    var idd = $("#hddnOtherID").val();
	if($("#txtminGuarenteed_"+idd).val()!='' && $("#selExPort_"+idd).val()!='')
	{
	    idd = parseInt(idd) + 1;
	    $('<tr id="minGuarenteedBodyRow_'+idd+'"><td>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#tb1" onClick="removeMinGuarenteedBody('+idd+');"><i class="fa fa-times" style="color:red;"></i></a></td><td><input type="text" name="txtminGuarenteed_'+idd+'" id="txtminGuarenteed_'+idd+'" class="form-control" autocomplete="off" value="" placeholder="Min Guaranteed Vol/Voyage(CBM)" /></td><td><select name="selExPort_'+idd+'" id="selExPort_'+idd+'" class="form-control"></select></td></tr>').appendTo("#minGuarenteedBody");
		$("#hddnOtherID").val(idd);
		$("#selExPort_"+idd).html($("#hiddenSel1").html());
	}
	else
	{
		jAlert('Please fill previous fields', 'Alert');
		return false;
	}
}

function removeMinGuarenteedBody(var1)
{
		jConfirm('Are you sure you want to remove this row permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#minGuarenteedBodyRow_"+var1).remove();		
				 }
			else{return false;}
			});
}

function Del_Upload(var1)
{
	jConfirm('Are you sure you want to remove this attachment permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_file_"+var1).remove();	
		 }
	else{return false;}
	});
}

function getCurrency()
{
	if($("#selCurrency").val()!='')
	{
		$("#curr_1").text($("#selCurrency").val());
	}
	else
	{
		$("#curr_1").text('USD');
	}
}

</script>
    </body>
</html>