<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
if(isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$selBType = $_REQUEST['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	$selBType = '';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(7,3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-bar-chart-o"></i>&nbsp;Reports&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                         <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                         <li>Reports</li>
						 <li>Accounts</li>
						 <li class="active">Vendor Wise Payment Report</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div class="box box-primary">
					<h3 style=" text-align:center;">Vendor Wise Payment Report</h3>
					<div class="row invoice-info" style="margin-left:10px;">
						<div class="col-sm-2 invoice-col">
                           Date Type
                            <address>
								<select  name="selDate_Type" class="select form-control" id="selDate_Type" onchange="getData_1()">
									<?php $obj->getShipmentDateType();?>
								</select>
							</address>
						</div>
						<div class="col-sm-2 invoice-col" style="font-size:13px;">
                           <span id="span_1"></span>&nbsp;Date From
                            <address>
								<input type="text" name="txtFrom_Date" id="txtFrom_Date" class="form-control" value="" disabled="disabled" placeholder="dd-mm-yyyy"/>
							</address>
						</div>
						<div class="col-sm-2 invoice-col" style="font-size:13px;">
                           <span id="span_2"></span>&nbsp;Date To
                            <address>
								<input type="text" name="txtTo_Date" id="txtTo_Date" class="form-control" value="" disabled="disabled" placeholder="dd-mm-yyyy"/>
							</address>
						</div>
                        <div class="col-sm-2 invoice-col">
                             Business Type
                             <select name="selBType" id="selBType" class="form-control" >
                               <?php echo $obj->getBusinessTypeList1($selBType);?>
                              </select>                  
                        </div><!-- /.col -->
						<div class="col-sm-2 invoice-col">
							&nbsp;
                            <address>
								<button class="btn btn-info btn-flat" id="inner-login-button" type="button" onclick="getData()">Search</button>
							</address>
						</div>
					</div>
					<div align="right">
						<a href="#" title="Pdf" onClick="getExcel();"><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i>Generate Excel</button></a>
                        <a href="#" title="Pdf" onClick="getPdf();"><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i>Generate PDF</button></a>
					</div>
					<div style="height:10px;"></div>
					<div id="DivOwner" class="box-body table-responsive" style="overflow:auto;" >
						<table id="shipment_registor" class="table table-bordered table-striped dataTable">
							<thead>
								<tr>
									<th valign="top">Nom ID</th>
									<th valign="top">Vessel</th>
									<th valign="top">CP Date</th>
									<th valign="top">Owner</th>
									<th valign="top">Broker</th>
									<th valign="top">Customer</th>
									<th valign="top">Material Desc</th>
									<th valign="top">Last Updated Freight</th>
									<th valign="top">From Port</th>
									<th valign="top">To Port</th>
									<th valign="top">BL Date</th>
									<th valign="top">Qty</th>
									<th valign="top">Addn Qty</th>
									<th valign="top">Dead Freight Qty</th>
									<th valign="top">BAF</th>
									<th valign="top">Bunkers</th>
									<th valign="top">Owner Related Cost</th>
									<th valign="top">Other Shipping Cost</th>
									<th valign="top">Port Cost LP</th>
									<th valign="top">Port Cost DP</th>
									<th valign="top">Dem / Dis Shipowner</th>
									<th valign="top">DA Deduction</th>
									<th valign="top">Actual DA</th>
									<th valign="top">Miscellaneous  Cost</th>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
						</table>
					</div>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="../../js/jquery.numeric.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$("#shipment_registor").dataTable();
	$("#txtDays").numeric();
	
	$("#txtFrom_Date,#txtTo_Date").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
});


function getData()
{
	$("#row_1").hide(); 
	$("#row_2").hide();
	$('#shipment_registor tbody').show();
	
	if($("#txtFrom_Date").val() != "" && $("#selDate_Type").val() != "" && $("#txtTo_Date").val() != "" && $("#selMat_Type").val() != "")
	{
		$('#shipment_registor tbody').html('<tr><td align="center" colspan="26"><img src="../../img/loading.gif" /></td></tr>');
		$.post("options.php?id=27",{txtFrom_Date:""+$("#txtFrom_Date").val()+"",txtTo_Date:""+$("#txtTo_Date").val()+"",selDate_Type:""+$("#selDate_Type").val()+"",selBType:""+$("#selBType").val()+""}, function(html) {
			$('#shipment_registor tbody').empty();
			$('#shipment_registor tbody').append(html);	   
		
		});
	}
	else
	{
		jAlert('Please select Material Type, Date Type, Date From & Date To', 'Alert');
	}
}

function getPdf()
{
	location.href='allPdf.php?id=14&txtFrom_Date='+$("#txtFrom_Date").val()+'&txtTo_Date='+$("#txtTo_Date").val()+'&selDate_Type='+$("#selDate_Type").val()+'&selBType='+$("#selBType").val();
}

function getExcel()
{
	location.href='allExcel.php?id=2&txtFrom_Date='+$("#txtFrom_Date").val()+'&txtTo_Date='+$("#txtTo_Date").val()+'&selDate_Type='+$("#selDate_Type").val()+'&selBType='+$("#selBType").val();
}

function getData_1()
{
	if($('#selDate_Type').val() == '')
	{
		$('#txtFrom_Date').val('');
		$('#txtTo_Date').val('');
		$('#txtFrom_Date').attr('disabled','disabled');
		$('#txtTo_Date').attr('disabled','disabled');
		$('#tbd').hide();
		$("#row_1").hide(); 
		$("#row_2").hide();
		$('#span_1').text(''); 
		$('#span_2').text('');
	}
	else
	{
		$('#txtFrom_Date').removeAttr('disabled');
		$('#txtTo_Date').removeAttr('disabled');
		if($('#selDate_Type').val() == 1){
			$('#span_1').text('BL'); 
			$('#span_2').text('BL');
		}else{
			$('#span_1').text('Financial Year'); 
			$('#span_2').text('Financial Year');
		}
	}
}

</script>
</body>
</html>