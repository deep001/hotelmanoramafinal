<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$id = explode(",",$_REQUEST['id']);
$comid = $id[0];
$vendorid  = $id[2];
$fcaid  = $id[1];
$page      = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "vessel_in_post_ops.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "vessel_in_history.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "coa_in_ops_at_glance.php"; $page_bar="In Ops at a glance - COA";$nav=20;$subtitle = 'COA';}else if($page == 5){$page_link = "coa_in_post_ops.php"; $page_bar="In Post Ops at a glance - COA";$nav=20;$subtitle = 'COA';}else{$page_link = "coa_in_history.php"; $page_bar="Vessels in History - COA";$nav=20;$subtitle = 'COA';}

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertDumerageInvoiceDetails($comid);
	header('Location:./payment_gridcoacoa.php?msg='.$msg.'&comid='.$comid.'&page='.$page);
}

if (@$_REQUEST['action1'] == 'submit1')
{
	$msg = $obj->insertDumeragePaymentReceivedDetails($comid);
	header('Location:./payment_gridcoa.php?msg='.$msg.'&comid='.$comid.'&page='.$page);
}

if (@$_REQUEST['action'] == 'submit2')
{
	$msg = $obj->deleteDumerageInvoiceRecords();
	$msg = explode(",",$msg);
	header('Location:./payment_gridcoa.php?msg='.$msg.'&comid'.$comid.'&page='.$page);
}

$fcaid = $obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($fcaid);

$pagename = basename($_SERVER['PHP_SELF'])."?id=".$_REQUEST['id']."&page=".$page."&name=".$_REQUEST['name'];

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);
$sqlcoa = "select CARGO from coa_master where COAID='".$obj->getFun175()."'";
$resultcoa = mysql_query($sqlcoa) or die($sqlcoa);
$rowscoa   = mysql_fetch_assoc($resultcoa);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 

$("#txtTO,#txtP_PR,[id^=txtAddDesAmt_],[id^=txtSubDesAmt_]").numeric();

$('#txtP_Remarks, #txtRemarks').autosize({append: "\n"});

$('#txtDate, #txtP_Date').datepicker({
	format: 'dd-mm-yyyy',
	autoclose:true
});

$('#txtP_PR').keyup(function(){
	if($('#txtP_PR').val() > $('#txtP_Amt').val())
	{
		jAlert('Payment Received is more than Amount', 'Alert');
		$('#txtP_PR').val(0.00);
	}
});

getBankingDetailsData();
getAmountThereOff();
});


function AddNewAddRow()
{
	var id = $("#txtAddID").val();
	if($("#txtAddDes_"+id).val() != "" && $("#txtAddDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="add_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtAddDes_'+id+'" id="txtAddDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtAddDesAmt_'+id+'" id="txtAddDesAmt_'+id+'" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getAmountThereOff();"/></td></tr>').appendTo("#tblAdd");
		$("#txtAddID").val(id);
		$("[id^=txtAddDesAmt_]").numeric();	
	}
}


function removeAddRow(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#add_Row_"+var1).remove();
					getAmountThereOff();
				 }
			else{return false;}
			});
}



function SubNewSubRow()
{
	var id = $("#txtSubID").val();
	if($("#txtSubDes_"+id).val() != "" && $("#txtSubDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="Sub_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtSubDes_'+id+'" id="txtSubDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtSubDesAmt_'+id+'" id="txtSubDesAmt_'+id+'" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getAmountThereOff();"/></td></tr>').appendTo("#tblSub");
		$("#txtSubID").val(id);
		$("[id^=txtSubDesAmt_]").numeric();	
	}
}


function removeSubRow(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#Sub_Row_"+var1).remove();
				getAmountThereOff();
			 }
		else{return false;}
		});
}



function getBankingDetailsData()
{   
	var1 = $('#selNOB').val();
	if($("#selNOB").val()!="")
	{
		$.post("options.php?id=43",{selNOB:""+$("#selNOB").val()+""}, function(html) {
			if(html!="")
			{
				$("#row").show();
				var res = html.split("@@@@");
				$('#span_1_0').text(res[0]);
				$('#span_1_1').text(res[1]);
				$('#span_1_2').text(res[2]);
				$('#span_1_3').text(res[3]);
				$('#span_1_4').text(res[4]);
				$('#span_1_5').text(res[5]);
				$('#span_1_6').text(res[6]);
				$('#span_1_7').text(res[7]);
				$('#span_1_8').text(res[8]);
				$('#span_1_9').text(res[9]);
				$('#span_1_10').text(res[10]);
			}
		});
	}
	else
	{
	    $("#row").hide();	
		$('#span_1_0').text("");
	    $('#span_1_1').text("");
		$('#span_1_2').text("");
		$('#span_1_3').text("");
		$('#span_1_4').text("");
		$('#span_1_5').text("");
		$('#span_1_6').text("");
		$('#span_1_7').text("");
		$('#span_1_8').text("");
		$('#span_1_9').text("");
		$('#span_1_10').text("");
	}
}


function getAmountThereOff()
{ 
    $("#txtAddDesTotalAmt").val(parseFloat($("[id^=txtAddDesAmt_]").sum()).toFixed(2));
	
	$("#txtSubDesTotalAmt").val(parseFloat($("[id^=txtSubDesAmt_]").sum()).toFixed(2));
	
	var add1 = parseFloat($("#txtAddDesTotalAmt").val());
	if(isNaN(add1)){add1 = 0;}
	var sub1 = parseFloat($("#txtSubDesTotalAmt").val());
	if(isNaN(sub1)){sub1 = 0;}
	if($('#txtNetAmount').val()=="" || isNaN($('#txtNetAmount').val())){var final_freight = 0;}else{var final_freight = $("#txtNetAmount").val();}
	var var1 = parseFloat($('#txtTO').val());
	var var3 = (parseFloat(parseFloat(parseFloat(final_freight)*parseFloat(var1))/100)).toFixed(2);
	if(isNaN(var3)){var3 = 0;}
	$('#txtNet').val(parseFloat(var3).toFixed(2));
	
	var var2 = parseFloat(var3) + parseFloat(add1) - parseFloat(sub1);
	
	var var4 = (parseFloat(parseFloat(parseFloat(var2)*parseFloat(var1))/100)).toFixed(2);
	if(isNaN(var4)){var4 = 0;}
	$('#txtNetAmtPayable').val(parseFloat(var4).toFixed(2));
	getAmountNetDue();
}

function toWords(s) {
	var th = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];

	var dg = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];

	var tn = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];

	var tw = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

    s = s.toString();
    s = s.replace(/[\, ]/g, '');
    if (s != parseFloat(s)) return 'not a number';
    var x = s.indexOf('.');
    if (x == -1) x = s.length;
    if (x > 15) return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++)
	{
    	if ((x - i) % 3 == 2) 
		{
        	if (n[i] == '1') 
			{
                str += tn[Number(n[i + 1])] + ' ';
                i++;
                sk = 1;
            } 
			else if (n[i] != 0) 
			{
                str += tw[n[i] - 2] + ' ';
                sk = 1;
            }
        } 
		else if (n[i] != 0) 
		{
            str += dg[n[i]] + ' ';
            if ((x - i) % 3 == 0) str += 'Hundred ';
            sk = 1;
        }
        if ((x - i) % 3 == 1) {
            if (sk) str += th[(x - i - 1) / 3] + ' ';
            sk = 0;
        }
    }
    return str.replace(/\s+/g, ' ');
}

function getAmountNetDue()
{
	var var3 = $('#txtNetAmtPayable').val();
	
	if(var3 > 0)
	{
		var4 = var3.split('.');
		
		if(var4[1] > 0)
		{ var5 = toWords(var4[0])+'Dollars And '+toWords(var4[1])+'Cents';}
		else {var5 = toWords(var4[0])+'Dollars';}
		$('#span_1').text('Net Payable: '+var5);
		$('#txtNet_Name').val(var5);
	}
	else if(var3 < 0)
	{
		var var3 = (parseFloat(-(var3))).toFixed(2);
		var4 = var3.split('.');
		
		if(var4[1] > 0)
		{ var5 = toWords(var4[0])+'Dollars And '+toWords(var4[1])+'Cents';}
		else {var5 = toWords(var4[0])+'Dollars';}
		$('#span_1').text('Net Payable: '+var5);
		$('#txtNet_Name').val(var5);
	}
	else
	{ $('#span_1').text('Net Payable: Zero Dollars');}
}

function getSubmit(var1)
{
	if(var1 == 0)
	{
		if($('#selIType').val() != '' && $('#txtTO').val() != '' && $('#selNOB').val() != '')
		{
			$('#txtStatus').val(var1);
			document.frm1.submit();
		}
		else
		{
			jAlert('Please select Invoice Type & fill % There Off & Banking Details', 'Alert');
			return false;
		}
	}
	else
	{
		if($('#selIType').val() != '' && $('#txtTO').val() != '' && $('#selNOB').val() != '')
		{
			jConfirm('Are you sure you want to "Submit & Close"?', 'Confirmation', function(r) {
			if(r)
			{
				$('#txtStatus').val(var1);
				document.frm1.submit();
			}
			});
		}
		else
		{
			jAlert('Please select Invoice Type & fill % There Off & Banking Details ', 'Alert');
			return false;
		}
	}
	
}

function getDelete(var1)
{
	jConfirm('Are you sure you want to Delete this Entry?', 'Confirmation', function(r) {
	if(r)
	{
		$('#action').val('submit2');
		$('#txtDEL_ID').val(var1);
		document.frm1.submit();
	}
	else
	{
		return false;
	}
	});
}



function getValid()
{
	if($('#txtP_PR').val() == '' || $('#txtP_Date').val() =='' || $('#txtP_Remarks').val()=='')
	{
		jAlert('Please fill the Payment Received & Date & Remarks', 'Alert');
		return false;
	}
	else
	{
		return true;
	}
}

function getPdf(var1)
{
	location.href='allPdf.php?id=52&im_id='+var1;
}

function getValid()
{
	if($('#txtP_PR').val() == '' || $('#txtP_Date').val() =='' || $('#txtP_Remarks').val()=='')
	{
		jAlert('Please fill the Payment Received & Date & Remarks', 'Alert');
		return false;
	}
	else
	{
		var file_temp_name = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
	    $('#txtCRMFILE1').val(file_temp_name);
	    var file_actual_name = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
	    $('#txtCRMNAME1').val(file_actual_name);
		document.frm1.submit();
		return true;
	}
}

function Del_Upload1(var2)
{
	jConfirm('Are you sure you want to delete this upload permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file1_'+var2).remove();
	}
	});
}

function onkeyUp1()
{
	if(parseFloat($('#txtP_PR').val()) > parseFloat($('#txtP_Amt').val()))
	{
		jAlert('Payment Received is more than Amount', 'Alert');
		$('#txtP_PR').val(0.00);
	}
}


function openWin(var1)
{
	$("#divPayment").empty();
	$("#divPayment").html('<div><img src="../../img/ajax-loader.gif" /><br><b>Loading...</b></div>'); 
	$.post("options.php?id=46",{invoiceid:""+var1+""}, function(html) {
		$("#divPayment").empty();
		$("#divPayment").append(html);
		
		$("#txtP_PR").numeric();
        $('#txtP_Remarks').autosize({append: "\n"});
		$('#txtP_Date').datepicker({
			format: 'dd-mm-yyyy',
			autoclose:true
		});
	});
}
</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?>&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>
                <?php
				$sql = "SELECT * from dumerage_invoice_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and VENDOR='".$vendorid."' and PORTID='".$id[6]."' and PORT_TYPE='".$id[5]."' and STATUS=0 LIMIT 1";
				
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				?>	
                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right">
                <?php if($rec>0){?><a href="#" title="Pdf" onClick="getPdf(<?php echo $rows['INVOICEID'];?>);"><button class="btn btn-default" type="button" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a>&nbsp;&nbsp;&nbsp;<?php }?><a href="payment_gridcoa.php?comid=<?php echo $comid;?>&page=<?php echo  $page;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
							
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								Invoice
								</h2>                            
							</div><!-- /.col -->
						</div>
					
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Invoice Type
								<address>
									<select  name="selIType" class="select form-control" id="selIType" >
										<?php 
										$obj->getInvoiceType();
										?>
									</select>
									<script>$('#selIType').val('<?php echo $rows['I_TYPE'];?>');</script>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
							   Invoice No.
								<address>
									<?php if($rec == 0){?>
									<?php echo $obj->getMessageNumberForDemurrageInvoice($obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME"));?>
									<input type="hidden" name="txtDNote" id="txtDNote" value="<?php echo $obj->getMessageNumberForDemurrageInvoice($obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME"));?>" readonly/>
									<?php }else{?>
									<?php echo $rows['MESSAGE'];?><input type="hidden" name="txtDNote" id="txtDNote" value="<?php echo $rows['MESSAGE'];?>" readonly/>
                                    <input type="hidden" name="txtInvoiceid" id="txtInvoiceid" value="<?php echo $rows['INVOICEID'];?>" readonly/>
									<?php }?>
								</address>
							</div><!-- /.col -->
						</div>
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Charterer
								<address>
								   <?php if($rec > 0){?>
										<label>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVendorListNewBasedOnID($rows['VENDOR']);?></label>
										<input type="hidden" name="txtVendor" id="txtVendor" value="<?php echo $rows['VENDOR'];?>" />
                                        <input type="hidden" name="txtPort" id="txtPort" value="<?php echo $id[6];?>" />
                                        <input type="hidden" name="txtPortType" id="txtPortType" value="<?php echo $id[5];?>" />
										<input type="hidden" name="txtFCAID" id="txtFCAID" value="<?php echo $rows['FCAID'];?>" />
										<?php }else{?>
										<label>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVendorListNewBasedOnID($vendorid);?></label>
										<input type="hidden" name="txtVendor" id="txtVendor" value="<?php echo $vendorid;?>" />
										<input type="hidden" name="txtFCAID" id="txtFCAID" value="<?php echo $obj->getFun1();?>" />
                                        <input type="hidden" name="txtPort" id="txtPort" value="<?php echo $id[6];?>" />
                                        <input type="hidden" name="txtPortType" id="txtPortType" value="<?php echo $id[5];?>" />
										<?php }?>
								</address>
							</div><!-- /.col -->                     
							<div class="col-sm-4 invoice-col">
								 Invoice Date
								<address>
								   <?php if($rec == 0){?>
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo date("d-m-Y",time());?>"/>
									<?php }else{?>
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo date("d-m-Y",strtotime($rows['DATE']));?>"/>
								<?php }?>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								 Fixture Ref.
								<address>
								   <label>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCompareEstimateData($comid,"VOYAGE_NO");?></label>
								</address>
							</div><!-- /.col -->
						</div>
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Vessel
								<address>
								   <label>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Cargo
								<address>
								   <label>&nbsp;&nbsp;&nbsp;<?php echo $obj->getMaterialCodeDesBasedOnCode($rowscoa['CARGO']);?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								CP Date
								<address>
                                <?php $cpdate = date("d-m-Y",strtotime($obj->getCompareEstimateData($comid,"CP_DATE"))); 
								if($cpdate=='01-01-1970'){$cpdate='';}
								?>
								   <label>&nbsp;&nbsp;&nbsp;<?php echo $cpdate; ?></label>
								   <input type="hidden" name="txtCP_Date" id="txtCP_Date" class="input-text" size="10" value="<?php echo $cpdate; ?>" autocomplete="off" readonly/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
                            Invoice Description
								
								<address>
								   <label>&nbsp;&nbsp;&nbsp;<?php echo $id[5]." - ".$_REQUEST['name'];?></label>
								</address>
                               
							</div><!-- /.col -->
						</div>
                        
                        <div class="box-header">
								<h3 class="box-title">&nbsp;</h3>
							</div><!-- /.box-header -->
                        <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								<strong>Terms Of Payment</strong>
								<address>
									<textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" rows="3" placeholder="Remarks ..." ><?php echo $rows['REMARKS'];?></textarea>
								</address>
							</div><!-- /.col -->
						</div>
						
						
						
					
				<div class="box">
                    <div class="box-header">
                        <h3 class="box-title">&nbsp;</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                       <table class="table table-striped">
                			<tbody>
                                <tr>
                                    <td>Amount</td>												
                                    <td><input type="text" name="txtAmount" id="txtAmount" class="form-control" readonly value="<?php echo $id[3];?>"></td>
                                </tr>
                                <?php $commamt = ($id[3]*$id[4])/100;?>
                                <tr>
                                    <td>Add Comm (<?php echo $id[4];?> %)  </td>
                                    <td><input type="text" name="txtComm" id="txtComm" class="form-control" readonly value="<?php echo $id[4];?>"><input type="hidden" name="txtCommAMT" id="txtCommAMT" class="form-control" value="<?php echo number_format($commamt,2,'.','');?>"></td>
                                </tr>
                                
                                
                                <?php
								$dumerage_amt = $id[3] - ($id[3]*$id[4])/100;
								$sql1 = "select * from dumerage_invoice_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and VENDOR = '".$vendorid."' and STATUS=1";
								$res1 = mysql_query($sql1);
								$net = 0;
								while($rows1 = mysql_fetch_assoc($res1))
								{ 
									$net = $net+$rows1['NET_AMOUNT'];
								?>
								<tr>
									
									<td><?php echo $rows1['I_TYPE'].'('.$rows1['MESSAGE'].')';?></td>
									<td><input type="text"  name="txtInv" id="txtInv" class="form-control" readonly value="<?php echo $rows1['NET_AMOUNT'];?>" /></td>	
								
								</tr>
								<?php }
								 $net_paid = $dumerage_amt-$net;?>
                                 <tr>
                                    <td>Net Amount</td>
                                    <td><input type="text" name="txtNetAmount" id="txtNetAmount" class="form-control" readonly value="<?php echo $net_paid;?>"></td>
                                 </tr>
                              </tbody>
                        </table>
                      </div>
					</div>
                        
                        <div class="box">
							
							<div class="box-body no-padding">
								<table class="table table-striped">
								   <tbody>
										<tr>
											<td>% There Off</td>
											<td><input type="text" name="txtTO" id="txtTO" class="form-control" value="<?php echo $rows['TO_1'];?>" style="text-align:right;" onKeyUp="getAmountThereOff()" autocomplete="off"/></td>
											<td></td>											
											<?php if($rec == 0){?>
											<td><input type="text"  name="txtNet" id="txtNet" class="form-control" readonly value="0" /></td>
											<?php }else{?>
											<td><input type="text"  name="txtNet" id="txtNet" class="form-control" readonly value="<?php echo $rows['NET_AMOUNT'];?>" /></td>
											<?php }?>
											<td></td>
										</tr>
                                       
										
									</tbody>
								</table>
							</div>
						</div>
						
						
								 
					
						
					<div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Description</th>
                                    <?php $sql2 = "select * from dumerage_invoice_slave where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='ADD'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									?>
                          			<th>Amount<input type="hidden" name="txtAddID" id="txtAddID" value="<?php echo $num21;?>"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblAdd">
                            <?php if($num2 >0)
							   {$i = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;
								?>
                                <tr id="add_Row_<?php echo $i;?>">
                                   <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeAddRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtAddDes_<?php echo $i;?>" id="txtAddDes_<?php echo $i;?>" rows="2" placeholder="Description..."><?php echo $rows2['DESCRIPTION'];?></textarea></td>
                                   <td><input type="text" name="txtAddDesAmt_<?php echo $i;?>" id="txtAddDesAmt_<?php echo $i;?>" class="form-control"  placeholder="Amount" autocomplete="off" value="<?php echo $rows2['AMOUNT'];?>" onKeyUp="getAmountThereOff();"/></td>
                                </tr>
                                <?php }}else{?>
                                <tr id="add_Row_1">
                                   <td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtAddDes_1" id="txtAddDes_1" rows="2" placeholder="Description..."></textarea></td>
                                   <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getAmountThereOff();"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="left"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewAddRow();">Add</button></td>
									<td>Total</td>
									<td><input type="text" name="txtAddDesTotalAmt" id="txtAddDesTotalAmt" class="form-control" readonly  placeholder="0.00" autocomplete="off" value=""/></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
				  
				  
				 <div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
							     Less
								</h2>                            
							</div><!-- /.col -->
				    </div>	
                  
				  <div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Description</th>
                                     <?php $sql2 = "select * from dumerage_invoice_slave where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='SUB'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									?>
                          			<th>Amount<input type="hidden" name="txtSubID" id="txtSubID" value="<?php echo $num21;?>"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblSub">
                            <?php if($num2 >0)
							   {$i = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;
								?>
                                <tr id="Sub_Row_<?php echo $i;?>">
                                   <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeSubRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtSubDes_<?php echo $i;?>" id="txtSubDes_<?php echo $i;?>" rows="2" placeholder="Description..."><?php echo $rows2['DESCRIPTION'];?></textarea></td>
                                   <td><input type="text" name="txtSubDesAmt_<?php echo $i;?>" id="txtSubDesAmt_<?php echo $i;?>" class="form-control"  placeholder="Amount" autocomplete="off" value="<?php echo $rows2['AMOUNT'];?>" onKeyUp="getAmountThereOff();"/></td>
                                </tr>
                                <?php }}else{?>
                                <tr id="Sub_Row_1">
                                   <td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtSubDes_1" id="txtSubDes_1" rows="2" placeholder="Description..."></textarea></td>
                                   <td><input type="text" name="txtSubDesAmt_1" id="txtSubDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getAmountThereOff();"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="left"><button type="button" class="btn btn-primary btn-flat" onClick="SubNewSubRow();">Add</button></td>
									<td>Total</td>
									<td><input type="text" name="txtSubDesTotalAmt" id="txtSubDesTotalAmt" class="form-control" readonly  placeholder="0.00" autocomplete="off" value=""/></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
								
                                
                       <div class="box">
							
							<div class="box-body no-padding">
								<table class="table table-striped">
								   <tbody>
										<tr>
											<td>Amount Payable		</td>
											<td></td>
											<td></td>											
											
											<td><input type="text"  name="txtNetAmtPayable" id="txtNetAmtPayable" class="form-control" readonly value="<?php echo $rows['NET_PAYABLE'];?>" /></td>
											<td></td>
										</tr>
                                       
										<tr>
											<td style="background-color:grey; color:white;" colspan="5" align="center">
											<?php if($rec == 0){?>
											<span id="span_1">Zero Dollars</span>
                                            <input type="hidden" class="form-control" name="txtNet_Name" id="txtNet_Name" value="Zero Dollars" readonly />
											<?php }else{?>
											<span id="span_1"><?php echo $rows['NETNAME'];?></span>
                                            <input type="hidden" class="form-control" name="txtNet_Name" id="txtNet_Name" value="<?php echo $rows['NETNAME'];?>" readonly />
											<?php }?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div> 
											
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Banking Details</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<td width="5%"></td>
											<td width="30%">Name Of Beneficiary</td>
											<td width="30%">
												<select  name="selNOB" class="select form-control" id="selNOB" onChange="getBankingDetailsData()">
													<?php 
													$obj->getBankingDetails();
													?>
												</select>
												<script>$('#selNOB').val('<?php echo $rows['NOB'];?>');</script>
											</td>
											<td width="35%"></td>
										</tr>
									</thead>
									<tbody id="row" style="display:none;">										
										<tr>
											<td></td>
											<td>Address</td>
											<td>
												<span id="span_1_0"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>Beneficiary A/C No.</td>
											<td>
												<span id="span_1_1"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>Beneficiary Bank</td>
											<td>
												<span id="span_1_2"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>Beneficiary Bank Address</td>
											<td>
												<span id="span_1_3"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>Beneficiary Bank Swift Code</td>
											<td>
												<span id="span_1_4"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>IBAN No.</td>
											<td>
												<span id="span_1_5"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>FED ABA</td>
											<td>
												<span id="span_1_6"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td colspan="4" style="background-color:grey; color:white;">CORRESPONDENT DETAILS</td>
										</tr>
										<tr>
											<td></td>
											<td>Correspondent Bank Name</td>
											<td>
												<span id="span_1_7"></span>
											</td>
											<td></td>
										</tr>
										
                                        <tr>
											<td></td>
											<td>Correspondent Bank Address</td>
											<td>
												<span id="span_1_9"></span>
											</td>
											<td></td>
										</tr>
                                        <tr>
											<td></td>
											<td>Account Number</td>
											<td>
												<span id="span_1_10"></span>
											</td>
											<td></td>
										</tr>
                                        <tr>
											<td></td>
											<td>Swift Code</td>
											<td>
												<span id="span_1_8"></span>
											</td>
											<td></td>
										</tr>
										
									</tbody>
								</table>
							</div>
						</div>
						<?php if($rights == 1){ ?>
						<div class="box-footer" align="right">
							<button type="button" class="btn btn-primary btn-flat" onClick="return getSubmit(0)">Submit</button>
							<button type="button" class="btn btn-primary btn-flat" onClick="return getSubmit(1)">Submit & Close</button>
                            <input type="hidden" name="txtCurrency" id="txtCurrency" value="<?php echo $id[6];?>" />
							<input type="hidden" name="txtFreightAmtLocal" id="txtFreightAmtLocal" value="<?php echo $id[7];?>" />	
                            <input type="hidden" name="txtExchangeRate" id="txtExchangeRate" value="<?php echo $id[8];?>" />						
						</div>
						<?php } ?>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">&nbsp;</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Fixture Ref.</th>
											<th>Vessel</th>
											<th>Invoice Type</th>
											<th>Invoice No.</th>
											<th>Charterer</th>
											<th>Details</th>
											<th>Payment Received</th>
										</tr>
									</thead>
									<tbody>
									<?php
									$sql = "select * from dumerage_invoice_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and VENDOR = '".$vendorid."' and PORTID='".$id[6]."' and PORT_TYPE='".$id[5]."' and STATUS=1";
									$res = mysql_query($sql);
									$rec = mysql_num_rows($res);
									
									if($rec == 0)
									{
									?>
									<tr>
										<td colspan="7" align="center" valign="middle" style="color:red;letter-spacing:1px;">SORRY CURRENTLY THERE ARE ZERO(0) RECORDS</td>
									</tr>
									<?php }else{while($rows = mysql_fetch_assoc($res)){?>
									<tr>
										<td align="left" valign="middle"><?php echo $obj->getCompareEstimateData($comid,"VOYAGE_NO");?></td>
										<td align="left" valign="middle"><?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></td>
										<td align="left" valign="middle"><?php echo $rows['I_TYPE'];?></td>
										<td align="left" valign="middle"><?php echo $rows['MESSAGE'];?></td>
										<td align="left" valign="middle"><?php echo $obj->getVendorListNewBasedOnID($rows['VENDOR']);?></td>
										<td align="center" valign="middle">
                                        <a href="#" title="Pdf" onClick="getPdf(<?php echo $rows['INVOICEID'];?>);"><button class="btn btn-default" type="button" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a>
									    &nbsp;|&nbsp;
										<a title="Delete Link" href="#1" onClick="return getDelete(<?php echo $rows['INVOICEID'];?>);" >
											<img src="../../img/icon_delete.gif" border="0" />
										</a>
										</td>
										<td align="left" valign="middle">
										<?php if($rows['STATUS'] == 1){?>
										<a href="#Payment" title="Payment Received" class="btn btn-default btn-sm" data-toggle="modal" data-target="#compose-modal" onClick="openWin(<?php echo $rows['INVOICEID'];?>);">Payment Received</a>
										<?php }else{?>
										Payment Received
										<?php }?>
										</td>
									</tr>
									<?php }}?>
									</tbody>
								</table>
							</div>
						</div>
						<input type="hidden" id="action" name="action" value="submit" />
						<input type="hidden" name="txtStatus" id="txtStatus" value="0"/>
						<input type="hidden" name="txtDEL_ID" id="txtDEL_ID" value="0"/>						
					</form>
					<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								</div>
								<div class="modal-body">
									<form name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>
                                        <div id="divPayment">
                                            
                                        </div>
                                        <div class="box-footer" align="right">
                                            <button type="submit" id="btnhide" class="btn btn-primary btn-flat" onClick="return getValid();" >Submit</button>
                                            <input type="hidden" id="action1" name="action1" value="submit1" />
                                            <input type="hidden" name="txtCRMFILE1" id="txtCRMFILE1" value="" />
                                            <input type="hidden" name="txtCRMNAME1" id="txtCRMNAME1" value="" />
                                        </div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
					
					
					
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>