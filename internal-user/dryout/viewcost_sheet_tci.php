<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid 		= $_REQUEST['comid'];
$cost_sheet_id  = $_REQUEST['cost_sheet_id'];
$page  			= $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}
if (@$_REQUEST['action'] == 'submit')
 { 
 	$msg = $obj->updateTCICostSheetDetails();
	header('Location:./'.$page_link.'?msg='.$msg);
 }
$id = $obj->getTCICostSheetID($comid,$cost_sheet_id);
$obj->viewFreightCostEstimationTempleteRecordsNew($id);
$pagename   = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&cost_sheet_id=".$cost_sheet_id."&page=".$page;
$rdoCap = $obj->getFun12();
$rdoMMarket= $obj->getFun45();
$rdoDWT =$obj->getFun46();
$rdoQty = $obj->getFun96();
$rdoQtyType = $obj->getFun132();
if($rdoQtyType ==""){$rdoQtyType = 1;}
?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}

</style>
</head>
	<div id="basic-modal-content" style="display:none;" align="center">
	<table cellpadding="1" cellspacing="4" border="0" align="center">
	<tr><td align="center"><img src="../../img/loading.gif" /><br /></td></tr>
	</table>
	</div>
	
    <body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onClick="getFinalCalculation();">Calculate</a>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-leaf"></i>&nbsp;Voyage Estimates(Chartering)&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Voyage Estimates(Chartering)</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
					<!--   content put here..................-->
					<div align="right"><a href="allPdf.php?id=43&comid=<?php echo $comid; ?>&sheetid=<?php echo $id;?>" title="Pdf"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
					<div style="height:10px;">&nbsp;</div>
						
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Voyage Financials : Estimate
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
					<div class="col-sm-4 invoice-col" style="display:none;" >
						<address>
						   <select  name="selVendor" class="select form-control" id="selVendor" >
								<?php $obj->getVendorListNewUpdate("");	?>
							</select> 
                            <select  name="selPort" class="select form-control" id="selPort">
								<?php $obj->getPortList(); ?>
							</select>
						</address>
					</div><!-- /.col -->
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Fixture Type <input type="hidden" name="selFType" id="selFType" value="<?php echo $obj->getFun2();?>"/>
								<address>
								<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFreightFixtureOutBasedOnID(2);?></strong>
								</address>
							</div><!-- /.col -->
						</div>
                            
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								 Main Particulars
								</h2>                            
							</div><!-- /.col -->
						</div>
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								 Vessel Name
								<address>
                                <input type="hidden" name="selVName" id="selVName" value="<?php echo $obj->getFun5();?>"/>
								  <strong><?php echo $obj->getVesselIMOData($obj->getFun5(),"VESSEL_NAME");?></strong>
                                </address>
							</div><!-- /.col -->
							 <div class="col-sm-4 invoice-col">
								 Vessel Type
								<address>
								    <input type="text" name="txtVType" id="txtVType" class="form-control" readonly value="<?php echo $obj->getFun6();?>" />
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
								 Flag
								<address>
								    <input type="text" name="txtFlag" id="txtFlag" class="form-control" readonly value="<?php echo $obj->getFun7();?>" />
								</address>
							</div><!-- /.col -->
						</div>
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Date
								<address>
                                <?php if($obj->getFun3()!=""){$date = date('d-m-Y',strtotime($obj->getFun3()));}else{$date = "";}?>
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo $date ;?>" placeholder"dd-mm-yy" />
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Voyage No.
								<address>
									<input type="text" name="txtVNo" id="txtVNo" class="form-control" autocomplete="off" placeholder="Voyage No." value="<?php echo $obj->getFun8();?>"/>
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								 Voyage Financials Name
								<address>
									<input type="text" name="txtENo" style="color:red;" id="txtENo" class="form-control" value="<?php echo $obj->getFun9();?>" placeholder=" Voyage Financials Name" />
								</address>
							</div><!-- /.col -->
							
						 </div>
                            
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoDWT" class="checkbox" id="rdoDWT1" type="radio" value="1"  <?php if($rdoDWT == 1) echo "checked"; ?> onClick="showDWTField();" />
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
								<input name="rdoDWT" class="checkbox" id="rdoDWT2" type="radio" value="2"  <?php if($rdoDWT == 2) echo "checked"; ?> onClick="showDWTField();" />
								</address>
							</div><!-- /.col -->
						   
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>
								<address>
									<input type="text" name="txtDWTS" id="txtDWTS" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFun10();?>" placeholder="DWT (Summer)" />
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>
								<address>
									<input type="text" name="txtDWTT" id="txtDWTT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFun11();?>" placeholder="DWT (Tropical)" disabled="disabled" />
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoCap" id="rdoCap1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoCap == 1) echo "checked"; ?> onClick="showCapField();"  />
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoCap" id="rdoCap2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoCap == 2) echo "checked"; ?> onClick="showCapField();" />
								</address>
							</div><!-- /.col -->
							
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
								<address>
									<input type="text" name="txtGCap" id="txtGCap" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFun13();?>" />
								</address>
							</div>
							
							<div class="col-sm-4 invoice-col">
								Bale Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
								<address>
									<input type="text" name="txtBCap" id="txtBCap" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFun14();?>" disabled="disabled" />
								</address>
							</div>
							<div class="col-sm-4 invoice-col">
								SF <span style="font-size:10px; font-style:italic;">(CBM/MT)</span>
								<address>
									<input type="text" name="txtSF" id="txtSF"  autocomplete="off" class="form-control" value="<?php echo $obj->getFun15();?>"  onkeyup="getTotalDWT(),getTotalDWT1()" />
								</address>
							</div>
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-8 invoice-col">
								&nbsp;
								<address>
									&nbsp;
								</address>
							</div>
							
							<div class="col-sm-4 invoice-col">
								Loadable <span style="font-size:10px; font-style:italic;">(MT)</span>
								<address>
									<input type="text" name="txtLoadable" id="txtLoadable" autocomplete="off" class="form-control" readonly value="<?php echo $obj->getFun16();?>" />
								</address>
							</div>
						</div>
                        
						
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								 COMMERCIAL - PARAMETERS &nbsp;&nbsp;&nbsp;&nbsp;<img src="../../img/close.png" onClick="getShowHide1();" id="BID1" name="BID1" style="cursor:pointer;" title="Close Panel" /><input type="hidden" id="txtbid1" value="0" />
								</h2>                            
							</div><!-- /.col -->
						</div>			
	
						<div id="bunker_adj1" >
						<table cellpadding="1" cellspacing="1" border="0" width="100%">
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
                        <tr>
						<td width="25%" align="left" class="text">GRT/NRT<br/><input type="text" name="txtGNRT" id="txtGNRT" class="form-control" readonly value="<?php echo $obj->getFun17();?>" autocomplete="off" placeholder="0.00"/></td>
						<td width="25%" align="left" valign="top" class="text">LOA<br/><input type="text" name="txtLOA" id="txtLOA" class="form-control" readonly value="<?php echo $obj->getFun18();?>" autocomplete="off" placeholder="0.00"/></td>
						<td width="25%" align="left" valign="top" class="text">Gear<br/><input type="text" name="txtGear" id="txtGear" class="form-control" readonly value="<?php echo $obj->getFun19();?>" autocomplete="off" placeholder="0.00"/></td>
						<td width="25%" align="left" valign="top" class="text">Built Year<br/><input type="text" name="txtBuiltYear" id="txtBuiltYear" class="form-control" readonly value="<?php echo $obj->getFun20();?>" autocomplete="off" placeholder="0.00"/></td>
						</tr>
                        <tr>
						<td width="25%" align="left" class="text">B.E.A.M.(m)<br/><input type="text" name="txtBeam" id="txtBeam" class="form-control" readonly value="<?php echo $obj->getFun21();?>" autocomplete="off" placeholder="0.00"/></td>
						<td width="25%" align="left" valign="top" class="text">TPC<br/><input type="text" name="txtTPC" id="txtTPC" class="form-control" readonly value="<?php echo $obj->getFun22();?>" autocomplete="off" placeholder="0.00"/></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						<td width="25%" align="left" valign="top" class="text"></td>
						</tr>
						<tr>
						<td width="25%" align="left" class="text" style="color:#dc631e;">Speed Data</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 1</td>
						<td width="25%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 2</td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="25%" align="left" class="input-text">Ballast Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBFullSpeed" id="txtBFullSpeed" class="form-control" value="<?php echo $obj->getFun23();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBEcoSpeed1" id="txtBEcoSpeed1" class="form-control" value="<?php echo $obj->getFun24();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtBEcoSpeed2" id="txtBEcoSpeed2" class="form-control" value="<?php echo $obj->getFun25();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						</tr>
						<tr>
						<td width="25%" align="left" class="input-text">Laden Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLFullSpeed" id="txtLFullSpeed" class="form-control" value="<?php echo $obj->getFun26();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLEcoSpeed1" id="txtLEcoSpeed1" class="form-control" value="<?php echo $obj->getFun27();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="25%" align="right" valign="top" class="text"><input type="text" name="txtLEcoSpeed2" id="txtLEcoSpeed2" class="form-control" value="<?php echo $obj->getFun28();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						</tr>
						</tbody>
						</table>
						</td></tr>
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">FO Consumption MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 1</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 2</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBFOFullSpeed" id="txtBFOFullSpeed" class="form-control" value="<?php echo $obj->getFun29();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBFOEcoSpeed1" id="txtBFOEcoSpeed1" class="form-control" value="<?php echo $obj->getFun30();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtBFOEcoSpeed2" id="txtBFOEcoSpeed2" class="form-control" value="<?php echo $obj->getFun31();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLFOFullSpeed" id="txtLFOFullSpeed" class="form-control" value="<?php echo $obj->getFun32();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLFOEcoSpeed1" id="txtLFOEcoSpeed1" class="form-control" value="<?php echo $obj->getFun33();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtLFOEcoSpeed2" id="txtLFOEcoSpeed2" class="form-control" value="<?php echo $obj->getFun34();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtPIFOFullSpeed" id="txtPIFOFullSpeed" class="form-control" value="<?php echo $obj->getFun35();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtPWFOFullSpeed" id="txtPWFOFullSpeed" class="form-control" value="<?php echo $obj->getFun36();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						
						</tbody>
						</table>
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" >
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text" style="color:#dc631e;">DO Consumption per MT/Day</td>
						<td width="20%" align="right" valign="top" class="text" style="color:#1b77a6;">Full Speed</td>
						<td width="17%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 1</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text" style="color:#1b77a6;">Eco Speed 2</td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="input-text">Ballast Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtBDOFullSpeed" id="txtBDOFullSpeed" class="form-control" value="<?php echo $obj->getFun37();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtBDOEcoSpeed1" id="txtBDOEcoSpeed1" class="form-control" value="<?php echo $obj->getFun38();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtBDOEcoSpeed2" id="txtBDOEcoSpeed2" class="form-control" value="<?php echo $obj->getFun39();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						<tr>
						<td width="45%" align="left" class="input-text">Laden Passage</td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtLDOFullSpeed" id="txtLDOFullSpeed" class="form-control" value="<?php echo $obj->getFun40();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtLDOEcoSpeed1" id="txtLDOEcoSpeed1" class="form-control" value="<?php echo $obj->getFun41();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"><input type="text" name="txtLDOEcoSpeed2" id="txtLDOEcoSpeed2" class="form-control" value="<?php echo $obj->getFun42();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						
						<tr>
						<td colspan="7" align="left" class="text">
						<table width="100%" cellpadding="1" cellspacing="1" border="0">
						<thead>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text">In Port - Idle</td>
						<td width="17%" align="right" valign="top" class="text">In Port - Working</td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td width="45%" align="left" class="text"></td>
						<td width="20%" align="right" valign="top" class="text"><input type="text" name="txtPIDOFullSpeed" id="txtPIDOFullSpeed" class="form-control" value="<?php echo $obj->getFun43();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="17%" align="right" valign="top" class="text"><input type="text" name="txtPWDOFullSpeed" id="txtPWDOFullSpeed" class="form-control" value="<?php echo $obj->getFun44();?>" onKeyUp="getVoyageTime();" autocomplete="off" placeholder="0.00"/></td>
						<td width="1%" align="left" class="text"></td>
						<td width="16%" align="right" valign="top" class="text"></td>
						<td width="1%" align="left" class="text"></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						</tbody>
						</table>
						
						</td></tr>	
						
						<tr height="10"><td colspan="10" align="left" class="text" valign="top" ></td></tr>	
						</table>
						</div>
                        
                        
                        
                         <div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								   OPEN VESSEL DETAILS &nbsp;&nbsp;&nbsp;&nbsp;<img src="../../img/close.png" onClick="getShowHide2();" id="BID2" name="BID2" style="cursor:pointer;" title="Close Panel" /><input type="hidden" id="txtbid2" value="0" />
								</h2>                            
							</div><!-- /.col -->
						</div>
                        
                        <div id="openvessel">
                           <div class="row invoice-info">
                               <div class="col-sm-4 invoice-col">
                                   Port Open
                                    <address>
                                        <select  name="selOpenPort" onChange="getZoneData();" class="form-control" id="selOpenPort">
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Zone Open
                                    <address>
                                      	<select  name="selZone"  class="form-control" id="selZone">
										<?php 
                                        $obj->getZoneList();
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                  Broker
                                    <address>
                                        <select  name="selBroker" class="form-control" id="selBroker">
                                        <?php 
                                        $obj->getVendorListNewForCOA(12);
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    COA / Spot
                                    <address>
                                      	<select  name="selCOASpot" class="form-control" id="selCOASpot" onChange="getShow();">
										<?php 
                                        $obj->getCOASpotList();
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info" id="tr_coa" style="display:none;">
                                <div class="col-sm-4 invoice-col">
                                  COA Number
                                    <address>
                                       <select  name="selCOA" class="form-control" id="selCOA" onChange="getTotalShipments();">
                                        <?php 
                                        $obj->getCOAList();
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                   Number of Lift
                                    <address>
                                       <input type="text" name="txtNoLift" id="txtNoLift" class="form-control"  placeholder=" Number of Lift" autocomplete="off" value="<?php echo $obj->getFun103();?>"/>
                                    </address>
                                </div><!-- /.col -->                        
                                <div class="col-sm-4 invoice-col">
                                    Total No. of Shipments
                                    <address>
                                        <strong><span id="ttl_shipment" ></span></strong>
                                     </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Vessel Category
                                    <address>
                                       <select  name="selVCType" class="form-control" id="selVCType">
                                        <?php 
                                        $obj->getVesselCategoryList();
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                     Expected Hire
                                    <address>
                                       <input type="text" name="txtexpectedhire" id="txtexpectedhire" class="form-control"  placeholder="Expected Hire" autocomplete="off" value="<?php echo $obj->getFun106();?>"/>
                                    </address>
                                </div>  
                            
                                <div class="col-sm-4 invoice-col">
                                    Owner
                                    <address>
                                        <select  name="selOwner" class="form-control" id="selOwner">
                                        <?php 
                                        $obj->getVendorListNewForCOA(11);
                                        ?>
                                        </select>
                                     </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info" id="sort">
                                <div class="col-sm-4 invoice-col">
                                        Disponent Owner 1
                                        <address>
                                            <select  name="txtDisponentOwner" class="form-control" id="txtDisponentOwner">
                                            <?php 
                                            $obj->getVendorListNewForCOA(11);
                                            ?>
                                            </select>
                                         </address>
                                 </div><!-- /.col -->
                                 <?php $sql5 = "select * from freight_cost_estimete_slave5 where FCAID='".$id."'";
								       $result5 = mysql_query($sql5);
									   $k =0;
									   while($rows5 = mysql_fetch_assoc($result5))
									   {$k = $k + 1;?>
										   <div class="col-sm-4 invoice-col"> Disponent Owner <?php echo $k+1;?> <address><select  name="txtDisponentOwner_<?php echo $k;?>" class="form-control" id="txtDisponentOwner_<?php echo $k;?>"><?php $obj->getVendorListNewForCOA(11); ?></select></address></div>
									  <?php }?>
                             </div>
                             <div class="row invoice-info">
                                 <div class="col-sm-4 invoice-col">
                                    <address>
                                    <button type="button" onClick="addUI_Row();" class="btn btn-primary btn-flat">Add</button><input type="hidden" class="input" name="txtTID" id="txtTID" value="<?php echo $k;?>" /><input type="hidden" name="txttabid[]" id="txttabid_<?php echo $i+1;?>" class="input-text" value="" />
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            
                            
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                <?php if($obj->getFun109()=="" || $obj->getFun109()=="0000-00-00"){$txtFDate = "";}else{$txtFDate = date('d-m-Y',strtotime($obj->getFun109()));}?>
                                   Laycan Start
                                    <address>
                                       <input type="text" name="txtFDate" id="txtFDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtFDate;?>"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                <?php if($obj->getFun110()=="" || $obj->getFun110()=="0000-00-00"){$txtTDate = "";}else{$txtTDate = date('d-m-Y',strtotime($obj->getFun110()));}?>
                                    Laycan Finish
                                    <address>
                                        <input type="text" name="txtTDate" id="txtTDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtTDate;?>"/>
                                     </address>
                                </div><!-- /.col -->
                       			<div class="col-sm-4 invoice-col">
                                <?php if($obj->getFun111()=="" || $obj->getFun111()=="0000-00-00"){$txtETADate = "";}else{$txtETADate = date('d-m-Y',strtotime($obj->getFun111()));}?>
                                   ETA During Fixture
                                    <address>
                                       <input type="text" name="txtETADate" id="txtETADate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtETADate;?>"/>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                            
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                <?php if($obj->getFun112()=="" || $obj->getFun112()=="0000-00-00"){$txtCPDate = "";}else{$txtCPDate = date('d-m-Y',strtotime($obj->getFun112()));}?>
                                   CP Date
                                    <address>
                                       <input type="text" name="txtCPDate" id="txtCPDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtCPDate;?>"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Remarks
                                    <address>
                                       <textarea class="form-control areasize" name="txtNotes" id="txtNotes" rows="3" placeholder="Remarks ..." ><?php echo $obj->getFun113();?></textarea>
                                    </address>
                                </div><!-- /.col -->
                           		<div class="col-sm-4 invoice-col">
                                   TC Description
                                    <address>
                                       <textarea class="form-control areasize" name="txtDirection" id="txtDirection" rows="3" placeholder="TC Description ..." ><?php echo $obj->getFun114();?></textarea>
                                    </address>
                                </div><!-- /.col -->
                             
                            </div>
                            
                            <div class="row invoice-info">
                                <div class="col-sm-6 invoice-col">
                                     &nbsp;
                                        <address>
                                        <div class="btn btn-success btn-file btn-flat" data-toggle="tooltip">
                                            <i class="fa fa-paperclip"></i> Attachment
                                            <input type="file" class="form-control" multiple name="attach_file[]" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                        </div>
                                    </address>
                                 </div><!-- /.col -->
                                 <?php if($obj->getFun115() != '')
									{ 
										$file = explode(",",$obj->getFun115()); 
										$name = explode(",",$obj->getFun116()); ?>										
									<div class="col-sm-6 invoice-col">
									  Previous Attachments
										<address>
												<table cellpadding="1" cellspacing="1" border="0" width="100%" align="left">
												<?php
												$j =1;
												for($i=0;$i<sizeof($file);$i++)
												{
												?>
												<tr height="20" id="row_file_<?php echo $j;?>">
													<td width="40%" align="left" class="input-text"  valign="top"><a href="../../attachment/<?php echo $file[$i]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="<?php echo $name[$i];?>"><i class="fa fa-external-link"></i>&nbsp;&nbsp;Attachment<?php echo $j;?></a>
													<input type="hidden" name="file_<?php echo $j;?>" id="file_<?php echo $j;?>" value="<?php echo $file[$i]; ?>" />
													<input type="hidden" name="name_<?php echo $j;?>" id="name_<?php echo $j;?>" value="<?php echo $name[$i]; ?>" />
													</td>
													<td width="30%" align="left" class="input-text"  valign="top"><a href="#1" onClick="Del_Upload(<?php echo $j;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
												</tr>
												<?php $j++;}?>
											</table>
										</address>
									</div><!-- /.col -->
                                <?php }?>
                             </div>
                         </div>
                        
                        
                        
                        <div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								   CARGO OPEN DETAILS &nbsp;&nbsp;&nbsp;&nbsp;<img src="../../img/close.png" onClick="getShowHide3();" id="BID3" name="BID3" style="cursor:pointer;" title="Close Panel" /><input type="hidden" id="txtbid3" value="0" />
								</h2>                            
							</div><!-- /.col -->
						</div>
                        
                         
                       <div id="opencargo">
                           <div class="row invoice-info">
                               <div class="col-sm-4 invoice-col">
                                   Cargo ID
                                    <address>
                                      <input type="text" name="txtCID" id="txtCID" class="form-control"  placeholder="Cargo ID" autocomplete="off" value="<?php echo $obj->getFun128();?>"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                  Shipper
                                    <address>
                                        <select  name="selShipper" class="form-control" id="selShipper" >
                                            <?php 
                                            $obj->getVendorListNewForCOA(15);
                                            ?>
                                            </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                 Tolerance (+/- %)
                                    <address>
                                        <input type="text"  name="txtTolerance"  class="form-control" id="txtTolerance" placeholder="Tolerance (%)" autocomplete="off" value="<?php echo $obj->getFun118();?>"/>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Material
                                    <address>
                                       <select  name="selMType" onChange="getMaterialCode();" class="form-control" id="selMType">
                                        <?php 
                                        $obj->getMaterialType();
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Material Code
                                    <address>
                                       <input type="text"  name="txtMCode"  class="form-control" id="txtMCode" placeholder="Material Code" readonly autocomplete="off" value="<?php echo $obj->getFun120();?>"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                 Term Tolerance
                                    <address>
                                        <input type="text"  name="txtTermTolerance"  class="form-control" id="txtTermTolerance" placeholder="Term Tolerance" autocomplete="off" value="<?php echo $obj->getFun121();?>"/>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Load Port
                                    <address>
                                       <select  name="selLPort" class="form-control" id="selLPort">
                                        <?php 
                                        $obj->getPortListNew();
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Discharge Port
                                    <address>
                                        <select  name="selDPort" class="form-control" id="selDPort">
                                        <?php 
                                        $obj->getPortListNew();
                                        ?>
                                        </select>
                                     </address>
                                </div><!-- /.col -->
                                
                             </div>
                             <div class="row invoice-info">
                                 <div class="col-sm-4 invoice-col">
                                 <?php if($obj->getFun124()=="" || $obj->getFun124()=="0000-00-00"){$txtLCSDate = "";}else{$txtLCSDate = date('d-m-Y',strtotime($obj->getFun124()));}?>
                                    LayCan Start Date
                                    <address>
                                        <input type="text" name="txtLCSDate" id="txtLCSDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtLCSDate;?>"/>
                                     </address>
                                </div><!-- /.col -->
                                
                                 <div class="col-sm-4 invoice-col">
                                 <?php if($obj->getFun125()=="" || $obj->getFun125()=="0000-00-00"){$txtLCFDate = "";}else{$txtLCFDate = date('d-m-Y',strtotime($obj->getFun125()));}?>
                                   LayCan Finish Date
                                    <address>
                                        <input type="text" name="txtLCFDate" id="txtLCFDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtLCFDate;?>"/>
                                     </address>
                                </div><!-- /.col -->
                            </div>
                         </div>
                         
                        
                        
                        
                        
                        <div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								<address>
									<input name="rdoQtyType" id="rdoQtyType1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoQtyType == 1) echo "checked"; ?> onClick="showHideQtyVendorDiv(2);"  /> <strong>Single</strong>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								<address>
									<input name="rdoQtyType" id="rdoQtyType2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoQtyType == 2) echo "checked"; ?> onClick="showHideQtyVendorDiv(2);" /> <strong>Distributed</strong>
								</address>
							</div><!-- /.col -->
						</div>
                        
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">Market</h2>                            
							</div><!-- /.col -->
						</div>
                        
                        <div class="box" id="divQty1">
                            <div class="box-body no-padding" style="overflow:auto;">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th width="25%">&nbsp;&nbsp;&nbsp;Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <th>Agreed&nbsp;Gross Freight <span style="font-size:10px; font-style:italic;">(Local Currency/MT)</span></th>
                                            <th>Currency&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <th>Exchange&nbsp;Rate</th>
                                            <th>Agreed&nbsp;Gross&nbsp;Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span></th>
                                            <th>Quantity&nbsp;<span style="font-size:10px; font-style:italic;">(MT)</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <?php 
                                            $sql12 = "select * from freight_cost_estimete_slave7 where FCAID='".$obj->getFun1()."'";
                                            $res12 = mysql_query($sql12);
                                            $rec12 = mysql_num_rows($res12);
                                            ?>
                                            
                                            <th>Gross&nbsp;Freight&nbsp;<span style="font-size:10px; font-style:italic;">(USD)</span><input type="hidden" name="txtQTYID" id="txtQTYID" value="<?php echo $rec12;?>" /></th>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tblQtyFreight">	
                                    <?php if($rec12==0)
                                    {?>
                                            <tr id="tbrQtyVRow_empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
                                       
                                        <?php }
                                        else
                                        {$i = 0;
                                        while($rows2 = mysql_fetch_assoc($res12))
                                        {$i = $i + 1;
                                        ?>
                                        <tr id="tbrQtyVRow_<?php echo $i;?>">
                                            <td><a href="#tb<?php echo $i;?>" onclick="deleteQtyVendorDetails(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                            <td>
                                                <select name="selQtyVendorList_<?php echo $i;?>" class="select form-control" id="selQtyVendorList_<?php echo $i;?>" onChange="showVendorName();"></select>
                                                <script>
                                                $("#selQtyVendorList_<?php echo $i;?>").html($("#selVendor").html());
												$("#selQtyVendorList_<?php echo $i;?>").val('<?php echo $rows2['QTY_VENDORID'];?>');
                                                </script>
                                            </td>
                                            <td><input type="text" name="txtQtyLocalAggriedFreight_<?php echo $i;?>" id="txtQtyLocalAggriedFreight_<?php echo $i;?>" class="form-control"  autocomplete="off" value="<?php echo $rows2['QTY_VENDORID'];?>" placeholder="Agreed Gross Freight (Local Currency)" onKeyUp="getQtyLocalFreightCal();"/></td>
                                            <td>
                                                <select  name="selCurrencyDisList_<?php echo $i;?>" class="select form-control" id="selCurrencyDisList_<?php echo $i;?>">
                                                <?php $obj->getCurrencyList(); ?>
                                                </select>
                                                <script>$("#selCurrencyDisList_<?php echo $i;?>").val(<?php echo $rows2['CURRENCYID'];?>);</script>
                                            </td>
                                            <td>
                                                <input type="text" name="txtDisExchangeRate_<?php echo $i;?>" id="txtDisExchangeRate_<?php echo $i;?>" class="form-control"   value="<?php echo $rows2[''];?>" autocomplete="off" placeholder="Exchange Rate" onKeyUp="getQtyLocalFreightCal();"/>
                                            </td>
                                            <td>
                                                <input type="text" name="txtQtyAggriedFreight_<?php echo $i;?>" id="txtQtyAggriedFreight_<?php echo $i;?>" class="form-control"  autocomplete="off" value="<?php echo $rows2['AGREED_GROSS_FREIGHT'];?>" placeholder="Agreed Gross Freight (USD/MT)" onKeyUp="getQtyLocalFreightCal();"/>
                                            </td>
                                            <td>
                                                <input type="text" name="txtFreightQty_<?php echo $i;?>" id="txtFreightQty_<?php echo $i;?>" class="form-control"  value="<?php echo $rows2['QUANTITY'];?>" placeholder="Quantity" onKeyUp="getQtyLocalFreightCal();"/>
                                            </td>
                                            <td>
                                                <input type="text" name="txtQtyFreight_<?php echo $i;?>" id="txtQtyFreight_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['GROSS_FREIGHT'];?>" readonly/>
                                            </td>
                                        </tr>
                                        
                                        <?php }}?>
                                    </tbody>
                                    <tfoot>
                                      <tr><td><button type="button" class="btn btn-primary btn-flat" onClick="addQtyVendorDetails()" >Add</button></td><td></td><td></td><td></td><td></td><td></td><td><input type="text" name="txtTotalFreightQty" id="txtTotalFreightQty" class="form-control" readonly value=""/></td><td><input type="text" name="txtTotalQtyFreight" id="txtTotalQtyFreight" class="form-control" readonly value=""/></td></tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        
                        <div class="row invoice-info" id="divMarket1">
							<div class="col-sm-4 invoice-col">
								<address>
								  <input name="rdoMMarket" class="checkbox" id="rdoMMarket1" type="radio" value="1"  <?php if($rdoMMarket == 1) echo "checked"; ?>  onclick="showMMarketField();"  />
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								<address>
									<input name="rdoMMarket" class="checkbox" id="rdoMMarket2" type="radio" value="2" <?php if($rdoMMarket == 2) echo "checked"; ?> onClick="showMMarketField();" />
								</address>
							</div><!-- /.col -->
						</div>
                        
                        <div class="row invoice-info" id="divMarket2">
                            <div class="col-sm-4 invoice-col">
								Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(Local Currency/MT)</span>
                                <address>
                                    <input type="text" name="txtMarLocalAggriedFreight" id="txtMarLocalAggriedFreight" class="form-control"  autocomplete="off" value="<?php echo $obj->getFun133();?>" placeholder="Agreed Gross Freight (Local Currency)" onKeyUp="getMarLocalFreightCal();"/>
                                </address>
								</address>
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                              Currency
                                <address>
                                    <select  name="selCurrencyMarList" class="select form-control" id="selCurrencyMarList">
                                    <?php $obj->getCurrencyList(); ?>
                                    </select>
                                </address>
                            </div><!-- /.col -->
                            <script>$("#selCurrencyMarList").val(<?php echo $obj->getFun134();?>);</script>
                            <div class="col-sm-4 invoice-col">
                               Exchange Rate
                                <address>
                                    <input type="text" name="txtMarExchangeRate" id="txtMarExchangeRate" class="form-control"  autocomplete="off" value="<?php echo $obj->getFun135();?>" placeholder="Exchange Rate" onKeyUp="getMarLocalFreightCal();"/>
                                </address>
                            </div><!-- /.col -->
                             <div style="clear:both;"></div>
                            <div class="col-sm-4 invoice-col">
								Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
								<address>
									<input type="text" name="txtMTCPDRate" id="txtMTCPDRate" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" placeholder="Agreed Gross Freight (USD/MT)" value="<?php echo $obj->getFun47();?>" />
								</address>
                            </div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Lumpsum <span style="font-size:10px; font-style:italic;">(USD)</span>
								<address>
									<input type="text" name="txtMLumpsum" id="txtMLumpsum" class="form-control" autocomplete="off" disabled="disabled" onKeyUp="getFinalCalculation();" placeholder="Lumpsum (USD)" value="<?php echo $obj->getFun48();?>" />
								</address>
						   </div><!-- /.col -->
                               <div class="col-sm-4 invoice-col">
									Addnl Cargo Rate <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
										<input type="text"  name="txtAddnlCRate" id="txtAddnlCRate" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" placeholder="Addnl Cargo Rate (USD/MT)" value="<?php echo $obj->getFun49();?>" />
                                    </address>
                               </div><!-- /.col -->
                         </div>
                        
                        <div class="row" id="divMarket3">
							<div class="col-xs-12">
								<h2 class="page-header">
								 Cargo
								</h2>                            
							</div><!-- /.col -->
						</div>
                        <div class="row invoice-info" id="divMarket4">
                            <div class="col-sm-4 invoice-col">
                                Quantity <span style="font-size:10px; font-style:italic;">(MT)</span>
                                <address>
									<input type="text" name="txtCQMT" id="txtCQMT" class="form-control" autocomplete="off"  onkeyup="getFinalCalculation();" placeholder="Quantity (MT)" value="<?php echo $obj->getFun50();?>" />
                                </address>
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                               Cargo Type
                                <address>
									<select  name="selCType" class="form-control" id="selCType">
										<?php $obj->getCargoTypeList();	?>
									</select>
                                </address>
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                Cargo Name
                                <address>
                                   <select  name="selCName" class="form-control" id="selCName">
									<?php 
									$obj->getCargoNameList();
									?>
									</select>
                                </address>
                            </div><!-- /.col -->
                            
                        </div>
                            
                        <div class="row invoice-info" id="divMarket5">
                            <div class="col-sm-4 invoice-col">
                              <span style="font-size:13px; font-style:italic; color:#dc631e">( Please put dead freight quantity / addnl quantity separately )</span>  
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <address>
									<input name="rdoQty" class="checkbox" id="rdoQty1" type="radio" value="1"  <?php if($rdoQty == 1) echo "checked"; ?>  onclick="showQtyField();"  />
                                </address>
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <address>
									<input name="rdoQty" class="checkbox" id="rdoQty2" type="radio" value="2" <?php if($rdoQty == 2) echo "checked"; ?> onClick="showQtyField();" />
                                </address>
                            </div><!-- /.col -->
                         </div>
                            
                            
                        <div class="row invoice-info" id="divMarket6">
                            <div class="col-sm-4 invoice-col">
                              &nbsp;
                                <address>
									<input type="hidden" name="txtAQMT" id="txtAQMT" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" value=""/>
                                </address>
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                DF Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                <address>
									<input type="text" name="txtDFQMT" id="txtDFQMT" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" placeholder="DF Qty (MT)" value="<?php echo $obj->getFun53();?>"/>
                                </address>
                            </div><!-- /.col -->
                             <div class="col-sm-4 invoice-col">
                                Addnl Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                <address>
                                    <input type="text" name="txtAddnlQMT" id="txtAddnlQMT" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" disabled placeholder="Addnl Qty (MT)" value="<?php echo $obj->getFun54();?>"/>
                                </address>
                            </div><!-- /.col -->
                        </div>
                        
                        
                        <div class="row">
                            <div class="col-xs-12">
                                <h2 class="page-header">
                                Passage Locations
                                </h2>                            
                            </div><!-- /.col -->
					    </div>
                        
                        <div class="box">
								<div class="box-body no-padding" style="overflow:auto;">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Location From</th>
												<th>Location To</th>
												<th>Passage Type</th>
                                                <th>Speed Type</th>
												<?php 
												$sqll = "select * from freight_cost_estimete_slave6 where FCAID='".$id."'";
												$resl = mysql_query($sqll);
												$recl = mysql_num_rows($resl);
												
										        ?>
												<th>Distance</th>
												</th>
											</tr>
										</thead>
										<tbody id="tblPortLotation">
											<?php if($recl == 0){ $i=1; ?>	
                                   			<tr id="prl_Row_<?php echo $i;?>">
                                                <td><a href="#pr1" onClick="removePortLocation(1);" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                <td><input type="text" name="txtFromLocation_1" id="txtFromLocation_1" class="form-control" value="" autocomplete="off" placeholder="Location From"/></td>
                                                <td><input type="text" class="form-control" name="txtToLocation_1" id="txtToLocation_1" value="" autocomplete="off" placeholder="Location To"/></td>
                                                <td>
                                                <select  name="selPLocationType_1" class="select form-control" id="selPLocationType_1" onChange="getVoyageTime();">
													<?php $obj->getPassageType(); ?>
                                                </select>
                                                </td>
                                                <td>
												<select  name="selSLocationSpeed_1" class="select form-control" id="selSLocationSpeed_1" onChange="getVoyageTime();">
													<?php $obj->getSelectSpeedList(); ?>
                                                </select>
                                                </td>
                                                <td>
                                                <input type="text" name="txtLocationDistance_1" id="txtLocationDistance_1" class="form-control"  value="" placeholder="distance" onKeyUp="getVoyageTime();"/>
                                                </td>
                                            </tr>
                                            <?php }else{
                                            $i=0;
                                            while($rowsl = mysql_fetch_assoc($resl))
											{ $i = $i + 1;?>
                                            <tr id="prl_Row_<?php echo $i;?>">
                                                <td><a href="#pr<?php echo $i;?>" onClick="removePortLocation(<?php echo $i;?>);" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                <td><input type="text" name="txtFromLocation_<?php echo $i;?>" class="form-control" id="txtFromLocation_<?php echo $i;?>" value="<?php echo $rowsl['LOCATION_FROM'];?>" autocomplete="off" placeholder="Location From"/></td>
                                                <td><input type="text" name="txtToLocation_<?php echo $i;?>" class="form-control" id="txtToLocation_<?php echo $i;?>" value="<?php echo $rowsl['LOCATION_TO'];?>" autocomplete="off" placeholder="Location To"/></td>
                                                <td>
                                                <select  name="selPLocationType_<?php echo $i;?>" class="select form-control" id="selPLocationType_<?php echo $i;?>" onChange="getVoyageTime();">
													<?php $obj->getPassageType(); ?>
                                                </select>
                                                </td>
                                                <td>
												<select  name="selSLocationSpeed_<?php echo $i;?>" class="select form-control" id="selSLocationSpeed_<?php echo $i;?>" onChange="getVoyageTime();">
													<?php $obj->getSelectSpeedList(); ?>
                                                </select>
                                                </td>
                                                <td>
                                                <input type="text" name="txtLocationDistance_<?php echo $i;?>" id="txtLocationDistance_<?php echo $i;?>" class="form-control"  value="<?php echo $rowsl['DISTANCE'];?>" placeholder="distance" onKeyUp="getVoyageTime();"/>
                                                <script>$("#selPLocationType_<?php echo $i;?>").val(<?php echo $rowsl['PASSAGE_TYPE'];?>);$("#selSLocationSpeed_<?php echo $i;?>").val(<?php echo $rowsl['SPEED_TYPE'];?>);</script>
                                                </td>
                                            </tr>
										<?php }}?>
                            
										</tbody>
                                        <tfoot>
                                        <tr>
                                        <td colspan="6" align="left"><button type="button" class="btn btn-primary btn-flat" onClick="addPortRotationLocationDetails()">Add</button><input type="hidden" name="p_locationID" id="p_locationID" value="<?php echo $i; ?>" /></td>
                                        </tr>
                                        </tfoot>
									</table>
								</div>
							</div>
                        
                        
							
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								Sea Passage
								</h2>                            
							</div><!-- /.col -->
						</div>
							
							<div class="box">
								<div class="box-body no-padding" style="overflow:auto;">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>From Port</th>
												<th>To Port</th>
                                                <th>Distance Type</th>
												<th>&nbsp;&nbsp;Passage&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Type</th>
												<th>Distance</th>
												<th>&nbsp;&nbsp;&nbsp;Speed&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Type</th>
                                                <th>Speed Adj.</th>
                                                <th>Margin Distance <span style="font-size:10px; font-style:italic;">(%)</span></th>
												
											</tr>
										</thead>
										<tbody id="tblPortRotation">
											<?php 
                                            $sql1 = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."'";
                                            
                                            $res1 = mysql_query($sql1);
                                            $num1 = mysql_num_rows($res1);
                                            if($num1==0)
                                            {$num1 =1;
                                            ?>
											<tr id="pr_Row_1">
                                                <td align="center" class="input-text" >
                                                  <a href="#pr1" id="spcancel_1" onclick="removePortRotation(1);" ><i class="fa fa-times" style="color:red;"></i></a>
                                                  
                                                </td>
                                                <td align="left" class="input-text" >
                                                   <select  name="selFPort_1" class="select form-control" id="selFPort_1" onChange="getDistance(1);">
														<?php $obj->getPortList(); ?>
                                                   </select> 
                                                </td>
                                                <td align="left" class="input-text" >
                                                   <select  name="selTPort_1" class="select form-control" id="selTPort_1" onChange="getDistance(1);">
														
                                                    </select>
                                                    <script>$("#selFPort_1,#selTPort_1").html($("#selPort").html());</script>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selDType_1" class="select form-control" id="selDType_1" onChange="getDistance(1);">
														<?php $obj->getPortDistanceType(); ?>
                                                    </select>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selPType_1" class="select form-control" id="selPType_1" onChange="getVoyageTime()"; >
														<?php $obj->getPassageType(); ?>
                                                    </select>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDistance_1" id="txtDistance_1" class="form-control"  value="" placeholder="distance" onKeyUp="getVoyageTime()"; /><span id="ploader_1" style="display:none;"><img src="../../img/ajax-loader2.gif" /></span>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selSSpeed_1" class="select form-control" id="selSSpeed_1" onChange="getVoyageTime()"; >
														<?php $obj->getSelectSpeedList(); ?>
                                                    </select>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtWeather_1" id="txtWeather_1" class="form-control" autocomplete="off" placeholder="Speed Adj."  onKeyUp="getVoyageTime()"; />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtMargin_1" id="txtMargin_1" class="form-control" autocomplete="off" placeholder="Margin (%)"  onKeyUp="getVoyageTime()"; />
                                                    <input type="hidden" name="txtVoyageTime_1" id="txtVoyageTime_1" class="form-control" autocomplete="off" value="0.00" />
                                                    <input type="hidden" name="txtTTLVoyageDays_1" id="txtTTLVoyageDays_1" class="form-control" autocomplete="off" value="0.00" />
                                                    <input type="hidden" name="txtTTLFoConsp_1" id="txtTTLFoConsp_1" class="form-control" readonly value="0.00"  />
                                                    <input type="hidden" name="txtTTLDoConsp_1" id="txtTTLDoConsp_1" class="form-control" readonly value="0.00"  />
                                                </td>
                                            </tr>
                                            <?php }
									     else
									      {$i=0;$num1 = $num1;
										  while($rows1 = mysql_fetch_assoc($res1))
										  {$i = $i + 1;
										  ?>
                                          <tr id="pr_Row_<?php echo $i;?>">
                                                <td align="center" class="input-text" >
                                                  <a href="#pr1" id="spcancel_<?php echo $i;?>" onclick="removePortRotation(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a><input type="hidden" name="txtFCASlaveID_<?php echo $i;?>" id="txtFCASlaveID_<?php echo $i;?>" value="<?php echo $rows1['FCA_SLAVEID'];?>"/><input type="hidden" name="txtFCASlaveRandomID_<?php echo $i;?>" id="txtFCASlaveRandomID_<?php echo $i;?>" value="<?php echo $rows1['RANDOMID'];?>"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                   <select  name="selFPort_<?php echo $i;?>" class="select form-control" id="selFPort_<?php echo $i;?>" onChange="getDistance(<?php echo $i;?>);">
														<?php $obj->getPortList(); ?>
                                                   </select> 
                                                </td>
                                                <td align="left" class="input-text" >
                                                   <select  name="selTPort_<?php echo $i;?>" class="select form-control" id="selTPort_<?php echo $i;?>" onChange="getDistance(<?php echo $i;?>);">
													</select>
                                                    <script>$("#selFPort_<?php echo $i;?>,#selTPort_<?php echo $i;?>").html($("#selPort").html());$("#selFPort_<?php echo $i;?>").val(<?php echo $rows1['FROM_PORT'];?>);$("#selTPort_<?php echo $i;?>").val(<?php echo $rows1['TO_PORT'];?>);</script>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selDType_<?php echo $i;?>" class="select form-control" id="selDType_<?php echo $i;?>" onChange="getDistance(<?php echo $i;?>);">
														<?php $obj->getPortDistanceType(); ?>
                                                    </select>
                                                </td>
                                                <script>$("#selDType_<?php echo $i;?>").val(<?php echo $rows1['DIS_TYPE'];?>);</script>
                                                <td align="left" class="input-text" >
                                                    <select  name="selPType_<?php echo $i;?>" class="select form-control" id="selPType_<?php echo $i;?>" onChange="getVoyageTime()"; >
														<?php $obj->getPassageType(); ?>
                                                    </select>
                                                </td>
                                                <script>$("#selPType_<?php echo $i;?>").val(<?php echo $rows1['PASSAGE_TYPE'];?>);</script>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDistance_<?php echo $i;?>" id="txtDistance_<?php echo $i;?>" class="form-control"  value="<?php echo $rows1['DISTANCE'];?>" placeholder="distance" onKeyUp="getVoyageTime()"; /><span id="ploader_<?php echo $i;?>" style="display:none;"><img src="../../img/ajax-loader2.gif" /></span>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selSSpeed_<?php echo $i;?>" class="select form-control" id="selSSpeed_<?php echo $i;?>" onChange="getVoyageTime()"; >
														<?php $obj->getSelectSpeedList(); ?>
                                                    </select>
                                                </td>
                                                <script>$("#selSSpeed_<?php echo $i;?>").val(<?php echo $rows1['SPEED_TYPE'];?>);</script>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtWeather_<?php echo $i;?>" id="txtWeather_<?php echo $i;?>" class="form-control" autocomplete="off" placeholder="Speed Adj." value="<?php echo $rows1['SPEED_ADJ'];?>"  onKeyUp="getVoyageTime()"; />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtMargin_<?php echo $i;?>" id="txtMargin_<?php echo $i;?>" class="form-control" autocomplete="off" placeholder="Margin (%)"  onKeyUp="getVoyageTime()"; value="<?php echo $rows1['MARGIN_DISTANCE'];?>" />
                                                    <input type="hidden" name="txtVoyageTime_<?php echo $i;?>" id="txtVoyageTime_<?php echo $i;?>" class="form-control" autocomplete="off" value="0.00" />
                                                    <input type="hidden" name="txtTTLVoyageDays_<?php echo $i;?>" id="txtTTLVoyageDays_<?php echo $i;?>" class="form-control" autocomplete="off" value="0.00" />
                                                    <input type="hidden" name="txtTTLFoConsp_<?php echo $i;?>" id="txtTTLFoConsp_<?php echo $i;?>" class="form-control" readonly value="0.00"  />
                                                    <input type="hidden" name="txtTTLDoConsp_<?php echo $i;?>" id="txtTTLDoConsp_<?php echo $i;?>" class="form-control" readonly value="0.00"  />
                                                </td>
                                            </tr>
                                          <?php }}?>										
										</tbody>
                                        <tfoot>
                                        <tr>
                                        <td colspan="9"><button type="button" class="btn btn-primary btn-flat" onClick="addPortRotationDetails()" >Add</button><input type="hidden" name="p_rotationID" id="p_rotationID" class="input" value="<?php echo $num1;?>" /></td>
                                        </tr>
                                        </tfoot>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
									Load Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							
							<div class="box">
								<div class="box-body no-padding" style="overflow:auto;">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Load Port(s)</th>
												
												<th>Port&nbsp;Costs<span style="font-size:10px; font-style:italic;">(USD)</span>&nbsp;</th>
												<th>Qty&nbsp;MT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
												<th>Rate&nbsp;<span style="font-size:10px; font-style:italic;">(MT/Day)</span>&nbsp;&nbsp;&nbsp;</th>
                                                <th>LP/Terms&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
												<th>Work&nbsp;Days&nbsp;&nbsp;</th>
												<th>Idle&nbsp;Days&nbsp;</th>
                                                <th>Port&nbsp;Cost&nbsp;Vendor&nbsp;</th>
                                                <th>Hide in Demurrage Dispatch</th>
                                            </tr>
										</thead>
										<tbody id="tblLoadPort">
                                        <?php 
                                            $sql1 = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."'";
                                            
                                            $res1 = mysql_query($sql1);
                                            $num1 = mysql_num_rows($res1);
                                            if($num1==0)
                                            {$num1 =1;
                                            ?>
											<tr id="lp_Row_1">
                                                <td align="center" class="input-text" >
                                                    <span id="spanLoadPort_1"></span>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtPCosts_1" id="txtPCosts_1" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)" onKeyUp="getFinalCalculation();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtQMT_1" id="txtQMT_1" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="Qty MT" />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtRate_1" id="txtRate_1" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="Rate (MT/Day)" />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selLPTerms_1" class="select form-control" id="selLPTerms_1" onChange="getLPRemoveDaysAttr(1),getPortCalculation();"><?php $obj->getLPTermsList(0);?></select>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtWDays_1" id="txtWDays_1" class="form-control"  autocomplete="off" readonly value="" onKeyUp= "getVoyageTime();" />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtIDays_1" id="txtIDays_1" class="form-control" autocomplete="off" value="" placeholder="Idle Days" onKeyUp="getVoyageTime();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selLPVendor_1" class="select form-control" id="selLPVendor_1">
                                                    </select>
                                                </td>
                                                <script>$("#selLPVendor_1").html($("#selVendor").html());</script>
                                                <td align="left" class="input-text" >
                                                    <input name="ChkShowDDCLP_1" class="checkbox" id="ChkShowDDCLP_1" type="checkbox" value="1" onClick="ShowDDCDPDP();" />
                                                </td>
                                            </tr>	
                                            <?php }
											 else
											  {$i=0;$num1 = $num1;
											  while($rows1 = mysql_fetch_assoc($res1))
											  {$i = $i + 1;
											  ?>	
                                              <tr id="lp_Row_<?php echo $i;?>">
                                                <td align="center" class="input-text" >
                                                    <span id="spanLoadPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($rows1['FROM_PORT']);?></span>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtPCosts_<?php echo $i;?>" id="txtPCosts_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows1['LOAD_PORT_COST'];?>" placeholder="Port Costs (USD)" onKeyUp="getFinalCalculation();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtQMT_<?php echo $i;?>" id="txtQMT_<?php echo $i;?>" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="<?php echo $rows1['LOAD_PORT_QTY'];?>" placeholder="Qty MT" />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtRate_<?php echo $i;?>" id="txtRate_<?php echo $i;?>" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="<?php echo $rows1['LOAD_PORT_RATE'];?>" placeholder="Rate (MT/Day)" />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selLPTerms_<?php echo $i;?>" class="select form-control" id="selLPTerms_<?php echo $i;?>" onChange="getLPRemoveDaysAttr(<?php echo $i;?>),getPortCalculation();"><?php $obj->getLPTermsList($rows1['LOAD_PORT_TERMS']);?></select>
                                                </td>
                                                <?php if($rows1['LOAD_PORT_TERMS']==4){$readonly="";}else{$readonly="readonly";}?>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtWDays_<?php echo $i;?>" id="txtWDays_<?php echo $i;?>" class="form-control"  autocomplete="off" <?php echo $readonly;?> value="<?php echo $rows1['LOAD_PORT_WORK_DAYS'];?>" onKeyUp= "getVoyageTime();" />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtIDays_<?php echo $i;?>" id="txtIDays_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows1['LOAD_PORT_IDEAL_DAYS'];?>" placeholder="Idle Days" onKeyUp="getVoyageTime();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selLPVendor_<?php echo $i;?>" class="select form-control" id="selLPVendor_<?php echo $i;?>">
                                                    </select>
                                                </td>
                                                <script>$("#selLPVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selLPVendor_<?php echo $i;?>").val('<?php echo $rows1['PORT_COSTLP_VENDOR'];?>');</script>
                                                <td align="left" class="input-text" >
                                                    <input name="ChkShowDDCLP_<?php echo $i;?>" class="checkbox" id="ChkShowDDCLP_<?php echo $i;?>" type="checkbox" value="1"  <?php if($rows1['IS_SHOW_DDCLP'] == 1) echo "checked"; ?> onClick="ShowDDCDPDP();" />
                                                </td>
                                            </tr>	 
                                              <?php }}?>								
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Discharge Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							
							
							<div class="box">
								<div class="box-body no-padding" style="overflow:auto;">
									<table class="table table-striped">
										<thead>
											<tr>
												
												<th>Discharge Port</th>
												
												<th>Port&nbsp;Costs<span style="font-size:10px; font-style:italic;">(USD)</span>&nbsp;</th>
												<th>Qty&nbsp;MT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
												<th>Rate&nbsp;<span style="font-size:10px; font-style:italic;">(MT/Day)</span>&nbsp;&nbsp;&nbsp;</th>
                                                <th>&nbsp;LP/Terms&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
												<th>Work&nbsp;Days&nbsp;&nbsp;</th>
												<th>Idle&nbsp;Days&nbsp;</th>
                                                <th>Port&nbsp;Cost&nbsp;Vendor&nbsp;</th>
                                                <th>Hide in Demurrage Dispatch</th>
											</tr>
										</thead>
										<tbody id="tblDisPort">
                                         <?php 
                                            $sql1 = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."'";
                                            
                                            $res1 = mysql_query($sql1);
                                            $num1 = mysql_num_rows($res1);
                                            if($num1==0)
                                            {$num1 =1;
                                            ?>
											<tr id="dp_Row_1">
                                                <td align="center" class="input-text" >
                                                    <span id="spanDisPort_1"></span>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDCosts_1" id="txtDCosts_1" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)" onKeyUp="getFinalCalculation();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDQMT_1" id="txtDQMT_1" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="Qty MT" />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDRate_1" id="txtDRate_1" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="Rate (MT/Day)" />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selDPTerms_1" class="select form-control" id="selDPTerms_1" onChange="getDPRemoveDaysAttr(1),getPortCalculation();"><?php $obj->getLPTermsList(0);?></select>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDWDays_1" id="txtDWDays_1" class="form-control" autocomplete="off" readonly value="" onKeyUp= "getVoyageTime();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDIDays_1" id="txtDIDays_1" class="form-control" autocomplete="off" value="" placeholder="Idle Days"  onKeyUp="getVoyageTime();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selDPVendor_1" class="select form-control" id="selDPVendor_1">
                                                    </select>
                                                </td>
                                                <script>$("#selDPVendor_1").html($("#selVendor").html());</script>
                                                <td align="left" class="input-text" >
                                                    <input name="ChkShowDDCDP_1" class="checkbox" id="ChkShowDDCDP_1" type="checkbox" value="1" onClick="ShowDDCDPDP();" />
                                                </td>
                                            </tr>	
                                            <?php }
											 else
											  {$i=0;$num1 = $num1;
											  while($rows1 = mysql_fetch_assoc($res1))
											  {$i = $i + 1;
											 ?>	
                                              <tr id="dp_Row_<?php echo $i;?>">
                                                <td align="center" class="input-text" >
                                                    <span id="spanDisPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($rows1['TO_PORT']);?></span>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDCosts_<?php echo $i;?>" id="txtDCosts_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows1['DISC_PORT_COST'];?>" placeholder="Port Costs (USD)" onKeyUp="getFinalCalculation();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDQMT_<?php echo $i;?>" id="txtDQMT_<?php echo $i;?>" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="<?php echo $rows1['DISC_PORT_QTY'];?>" placeholder="Qty MT" />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDRate_<?php echo $i;?>" id="txtDRate_<?php echo $i;?>" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="<?php echo $rows1['DISC_PORT_RATE'];?>" placeholder="Rate (MT/Day)" />
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selDPTerms_<?php echo $i;?>" class="select form-control" id="selDPTerms_<?php echo $i;?>" onChange="getDPRemoveDaysAttr(<?php echo $i;?>),getPortCalculation();"><?php $obj->getLPTermsList($rows1['DISC_PORT_TERMS']);?></select>
                                                </td>
                                               <?php if($rows1['DISC_PORT_TERMS']==4){$readonly="";}else{$readonly="readonly";}?>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDWDays_<?php echo $i;?>" id="txtDWDays_<?php echo $i;?>" class="form-control" autocomplete="off" <?php echo $readonly;?> value="<?php echo $rows1['DISC_PORT_WORK_DAYS'];?>" onKeyUp= "getVoyageTime();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtDIDays_<?php echo $i;?>" id="txtDIDays_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows1['DISC_PORT_IDEAL_DATE'];?>" placeholder="Idle Days"  onKeyUp="getVoyageTime();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <select  name="selDPVendor_<?php echo $i;?>" class="select form-control" id="selDPVendor_<?php echo $i;?>">
                                                    </select>
                                                </td>
                                                <script>$("#selDPVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selDPVendor_<?php echo $i;?>").val('<?php echo $rows1['PORT_COSTDP_VENDOR'];?>');</script>
                                                <td align="left" class="input-text" >
                                                    <input name="ChkShowDDCDP_<?php echo $i;?>" class="checkbox" id="ChkShowDDCDP_<?php echo $i;?>" type="checkbox" value="1"  <?php if($rows1['IS_SHOW_DDCDP'] == 1) echo "checked"; ?> onClick="ShowDDCDPDP();" />
                                                </td>
                                            </tr>	 
                                              <?php }}?>										
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Transit Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							
							
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Transit Port</th>
												<th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
												<th>Idle Days</th>
											</tr>
										</thead>
										<tbody id="tblTransitPort">
                                         <?php 
                                            $sql1 = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."'";
                                            
                                            $res1 = mysql_query($sql1);
                                            $num1 = mysql_num_rows($res1);
                                            if($num1==0)
                                            {$num1 =1;
                                            ?>
											<tr id="tp_Row_1">
                                                <td align="center" class="input-text" >
                                                    <span id="TranDisPort_1"></span>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtTLPCosts_1" id="txtTLPCosts_1" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)" onKeyUp="getPortCalculation();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtTLIDays_1" id="txtTLIDays_1" class="form-control" autocomplete="off" value="" placeholder="Idle Days" onkeyup="getPortCalculation();"/>
                                                </td>
                                            </tr>	
                                             <?php }
											 else
											  {$i=0;$num1 = $num1;
											  while($rows1 = mysql_fetch_assoc($res1))
											  {$i = $i + 1;
											
											  ?>	
                                              <tr id="tp_Row_<?php echo $i;?>">
                                                <td align="center" class="input-text" >
                                                    <span id="TranDisPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($rows1['FROM_PORT']);?></span>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtTLPCosts_<?php echo $i;?>" id="txtTLPCosts_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows1['TRANSIT_PORT_COST'];?>" placeholder="Port Costs (USD)" onKeyUp="getPortCalculation();"/>
                                                </td>
                                                <td align="left" class="input-text" >
                                                    <input type="text" name="txtTLIDays_<?php echo $i;?>" id="txtTLIDays_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows1['TRANSIT_PORT_IDLE_DAYS'];?>" placeholder="Idle Days" onkeyup="getPortCalculation();"/>
                                                </td>
                                            </tr>	
                                              <?php }}?>												
										</tbody>
									</table>
								</div>
							</div>
							
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Totals
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Laden Dist
                                    <address>
										<input type="text" name="txtLDist" id="txtLDist" class="form-control" readonly value="<?php echo $obj->getFun55();?>" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ballast Dist
                                    <address>
										<input type="text" name="txtBDist" id="txtBDist" class="form-control" readonly value="<?php echo $obj->getFun56();?>" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Dist
                                    <address>
										<input type="text" name="txtTDist" id="txtTDist" class="form-control" readonly value="<?php echo $obj->getFun63();?>" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Laden Days
                                    <address>
										<input type="text" name="txtLDays" id="txtLDays" class="form-control" readonly value="<?php echo $obj->getFun57();?>" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ballast Days
                                    <address>
										<input type="text" name="txtBDays" id="txtBDays" class="form-control" readonly value="<?php echo $obj->getFun58();?>" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Sea Days
                                    <address>
										<input type="text" name="txtTSDays" id="txtTSDays" class="form-control" readonly value="<?php echo $obj->getFun59();?>" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl Port Idle Days
                                    <address>
										<input type="text" name="txtTtPIDays" id="txtTtPIDays" class="form-control" readonly value="<?php echo $obj->getFun60();?>" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl Port Work Days
                                    <address>
										<input type="text" name="txtTtPWDays" id="txtTtPWDays" class="form-control" readonly value="<?php echo $obj->getFun61();?>" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Days
                                    <address>
										<input type="text" name="txtTDays" id="txtTDays" class="form-control" readonly value="<?php echo $obj->getFun62();?>" />
                                    </address>
                                </div><!-- /.col -->
							</div>
							<div class="row invoice-info">	
								<div class="col-sm-4 invoice-col">
                                  Ttl FO Consp MT
                                    <address>
										<input type="text" name="txtTFUMT" id="txtTFUMT" class="form-control" readonly value="<?php echo $obj->getFun64();?>" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl DO Consp MT
                                    <address>
										<input type="text" name="txtTDUMT" id="txtTDUMT" class="form-control" readonly value="<?php echo $obj->getFun65();?>" />
                                    </address>
                                </div><!-- /.col -->
							</div>
                            
                            <div class="row invoice-info">	
								<div class="col-sm-4 invoice-col">
                                  Ttl FO Consp MT (Manual)
                                    <address>
										<input type="text" name="txtTFUMTManual" id="txtTFUMTManual" class="form-control" value="<?php echo $obj->getFun136();?>"  onKeyUp="getBunkerCalculation();" autocomplete="off"/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl DO Consp MT (Manual)
                                    <address>
										<input type="text" name="txtTDUMTManual" id="txtTDUMTManual" class="form-control" value="<?php echo $obj->getFun137();?>"  onKeyUp="getBunkerCalculation();" autocomplete="off"/>
                                    </address>
                                </div><!-- /.col -->
							</div>
							<div class="row invoice-info">	
								<div class="col-sm-4 invoice-col">
                                  Ttl FO Consp Off Hire
                                    <address>
										<input type="text" name="txtTFOCOffHire" id="txtTFOCOffHire" class="form-control" autocomplete="off" value="<?php echo $obj->getFun138();?>" autocomplete="off"/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl DO Consp Off Hire
                                    <address>
										<input type="text" name="txtTDOCOffHire" id="txtTDOCOffHire" class="form-control" autocomplete="off" value="<?php echo $obj->getFun139();?>" autocomplete="off"/>
                                    </address>
                                </div><!-- /.col -->
								
							</div>
							
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Freight Adjustment</h3>
								</div>
								<div class="box-body no-padding">
                                    <table class="table table-striped" id="divQty3">
                                        <thead>
                                            <tr>
                                                <th width="24%">Customer</th>
                                                <th width="19%">Gross Freight <span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                                <th width="19%">Brokerage(%)</th>
                                                <th width="19%">Net Brokerage</th>
                                                <th width="19%">Net Freight <span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                            </tr>
                                        </thead>
                                        <tbody id="tblQtyFreight1">	
                                        <?php 
                                        $sql12 = "select * from freight_cost_estimete_slave7 where FCAID='".$obj->getFun1()."'";
                                        $res12 = mysql_query($sql12);
                                        $rec12 = mysql_num_rows($res12);
                                        ?>
                                          <?php if($rec12==0)
                                            {?>
                                                <tr id="tbrQtyVRow_empty1">
                                                    <td valign="top" align="center" colspan="5" style="color:red;">Sorry , currently zero(0) records added.</td>
                                                </tr>
                                                <?php }
                                                else
                                                {$i = 0;
                                                while($rows2 = mysql_fetch_assoc($res12))
                                                {$i = $i + 1;
                                                ?>
                                         
                                                <tr id="tbrQtyVRow1_<?php echo $i;?>">
                                                    <td><span id="tbrqtyvrowvendor1_<?php echo $i;?>"><?php echo $obj->getVendorListNewBasedOnID($rows2['QTY_VENDORID']);?></span></td>
                                                    <td><input type="text" name="txtFreightQty1_<?php echo $i;?>" id="txtFreightQty1_<?php echo $i;?>" autocomplete="off" class="form-control" readonly value="<?php echo $rows2['GROSS_FREIGHT'];?>" /></td>
                                                    <td><input type="text" name="txtQtyBrokeragePer_<?php echo $i;?>" id="txtQtyBrokeragePer_<?php echo $i;?>" onKeyUp="getFinalCalculation();" class="form-control" value="<?php echo $rows2['BROKERAGE'];?>" /></td>
                                                    <td><input type="text" name="txtQtyBrokerageAmt_<?php echo $i;?>" id="txtQtyBrokerageAmt_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows2['NET_BROKERAGE'];?>" /></td>
                                                    <td><input type="text" name="txtQtyNetFreight_<?php echo $i;?>" id="txtQtyNetFreight_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows2['NET_FREIGHT'];?>" /></td>
                                                </tr>
                                                
                                                <?php }}?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>Total Net</td>
                                                <td></td>
                                                <td></td>
                                                <td><input type="text"  name="txtTotalNetBrokerage" id="txtTotalNetBrokerage" autocomplete="off" class="form-control" readonly value="0.00" /></td>
                                                <td><input type="text"  name="txtTotalNetFreight" id="txtTotalNetFreight" autocomplete="off" class="form-control" readonly value="0.00" /></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    
                                    <table class="table table-striped" id="divMarket7">
										<thead>
											<tr>
												<th width="25%">&nbsp;&nbsp;</th>
												<th width="25%">Percent</th>
												<th width="25%">USD</th>
												<th width="25%">Per MT</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Gross Freight</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdGF" id="txtFrAdjUsdGF" class="form-control" readonly value="<?php echo $obj->getFun66();?>" /></td>
												<td><input type="text"  name="txtFrAdjUsdGFMT" id="txtFrAdjUsdGFMT" class="form-control" readonly value="<?php echo $obj->getFun70();?>" /></td>
											</tr>
											
											<tr>
												<td>Dead Freight</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdDF" id="txtFrAdjUsdDF" class="form-control" readonly value="<?php echo $obj->getFun67();?>" /></td>
												<td><input type="text"  name="txtFrAdjUsdDFMT" id="txtFrAdjUsdDFMT" class="form-control" readonly value="<?php echo $obj->getFun71();?>"/></td>
											</tr>
											
											<tr>
												<td>Addnl Freight</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdAF" id="txtFrAdjUsdAF" class="form-control" readonly value="<?php echo $obj->getFun68();?>" /></td>
												<td><input type="text"  name="txtFrAdjUsdAFMT" id="txtFrAdjUsdAFMT" class="form-control" readonly value="<?php echo $obj->getFun72();?>" /></td>
											</tr>
											
											<tr>
												<td>Total Freight</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdTF" id="txtFrAdjUsdTF" class="form-control" readonly value="<?php echo $obj->getFun69();?>" /></td>
												<td><input type="text"  name="txtFrAdjUsdTFMT" id="txtFrAdjUsdTFMT" class="form-control" readonly value="<?php echo $obj->getFun73();?>" /></td>
											</tr>
											
											<tr>
												<td>Brokerage</td>
												<td><input type="text"  name="txtFrAdjPerAgC" id="txtFrAdjPerAgC"  onkeyup="getFinalCalculation(),getValue()" class="form-control" autocomplete="off" value="<?php echo $obj->getFun77();?>" ></td>
												<td><input type="text"  name="txtFrAdjUsdAgC" id="txtFrAdjUsdAgC" class="form-control" readonly value="<?php echo $obj->getFun78();?>"   /></td>
												<td></td>
											</tr>
											
											<tr>
												<td>Net Freight Payable</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdFP" id="txtFrAdjUsdFP" class="form-control" readonly value="<?php echo $obj->getFun80();?>"   /></td>
												<td></td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
                            
                            
							
							<div class="box" >
								<div class="box-header">
									<h3 class="box-title">Freight Details</h3>
								</div>
								<div class="box-body no-padding">
                                	<table class="table table-striped" id="divQty4">
										<tbody id="tblQtyFreight2">
                                        <?php 
										$sql12 = "select * from freight_cost_estimete_slave7 where FCAID='".$obj->getFun1()."'";
										$res12 = mysql_query($sql12);
										$rec12 = mysql_num_rows($res12);
										?>
										<?php if($rec12==0)
										{?>
											<tr id="tbrQtyVRow_empty2">
												<td valign="top" align="center" colspan="4" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
                                            <?php }
											else
											{$i = 0;
											while($rows2 = mysql_fetch_assoc($res12))
											{$i = $i + 1;
											?>
                                        
                                            <tr id="tbrQtyVRow2_<?php echo $i;?>">
                                                <td>Final Nett Freight</td>
                                                <td><span id="tbrqtyvrowvendor2_<?php echo $i;?>"><?php echo $obj->getVendorListNewBasedOnID($rows2['QTY_VENDORID']);?></span></td>
                                                <td><input type="text" name="txtQtyNetFreight1_<?php echo $i;?>" id="txtQtyNetFreight1_<?php echo $i;?>" autocomplete="off" class="form-control" readonly value="<?php echo $rows2['NET_FREIGHT'];?>" /></td>
                                                <td><input type="text" name="txtQtyNetFreightMT1_<?php echo $i;?>" id="txtQtyNetFreightMT1_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['NET_FREIGHT_PERMT'];?>" readonly /></td>
                                            </tr>
                                            <?php }}?>								
										</tbody>
									</table>
                                
									<table class="table table-striped" id="divMarket8">
										<thead>
											<tr>
												<th width="25%;"></th>
												<th width="25%;"></th>
												<th width="25%;"></th>
												<th width="25%;"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Final Nett Freight</td>
											    <td><input type="text"  name="txtFGFFD" id="txtFGFFD" class="form-control" onKeyUp="getFinalCalculation();" readonly value="<?php echo $obj->getFun80();?>" /></td>
												<td><input type="text"  name="txtFGFFDMT" id="txtFGFFDMT" class="form-control" readonly value="<?php echo $obj->getFun81();?>" /></td>
                                                <td><select  name="txtFGFFVendor" class="select form-control" id="txtFGFFVendor" ></select></td>
                                                <script>$("#txtFGFFVendor").html($("#selVendor").html());$("#txtFGFFVendor").val('<?php echo $obj->getFun141();?>');</script></td>
											</tr>											
										</tbody>
									</table>
								</div>
							</div>
							
                            
                            <div class="box">
								<div class="box-header">
									<h3 class="box-title">Bunker Adjustment</h3>
									<a href="javascript:void(0)" onClick="getShowHide();" id="BID" name="BID"><img src="../../img/close.png" title="Show Hide" /></a>
									<input type="hidden" id="txtbid" value="0" />
								</div>
								<div class="box-body no-padding table-responsive" style="overflow:auto;" id="bunker_adj">
									<table class="table table-striped">
										<thead>
											<tr class="GridviewScrollHeader">
												<th colspan="1">Select Bunker grade</th>
												<?php
													
													$sql = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
													$res = mysql_query($sql);
													$rec = mysql_num_rows($res);
													
													if($rec == 0)
													{
												?>
												<tr>
													<td valign="top" align="center" colspan="6" style="color:red;">First fill the Bunker Grade Master Data.</td>
												</tr>
												<?php	
												}else{
												?>
												<?php $m=0; while($rows = mysql_fetch_assoc($res)){
													$ttl_bg[]  = $rows['NAME'];
													$ttl_bg1[] = $rows['BUNKERGRADEID'];
												?>
													 <th colspan="3" style="text-align:center;"><?php echo $rows['NAME'];?>&nbsp;&nbsp;</th>
												<?php $m++; ?>
											<?php } ?>
											</tr>
										</thead>
										<tbody>
											<tr class="GridviewScrollHeader">
												<td>&nbsp;</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){?>
												<td style="text-align:center;">MT</td>
												<td style="text-align:center;">Price</td>
												<td style="text-align:center;">Cost<input type="hidden" name="txtBunkerRec" id="txtBunkerRec" class="form-control" readonly value="<?php echo $rec;?>"/>			 
												<input type="hidden" name="txtBunkerGradeName_<?php echo $i+1;?>" id="txtBunkerGradeName_<?php echo $i+1;?>" class="form-control" readonly value="<?php echo $ttl_bg[$i];?>"/>	
												<input type="hidden" name="txtBunkerGradeID_<?php echo $i+1;?>" id="txtBunkerGradeID_<?php echo $i+1;?>" class="form-control" readonly value="<?php echo $ttl_bg1[$i];?>"/>
												 <input type="hidden" name="txtBHID_<?php echo $ttl_bg1[$i];?>" id="txtBHID_<?php echo $ttl_bg1[$i];?>" value="<?php echo $ttl_bg[$i];?>" class="form-control"/>
												 <input type="hidden" name="txtBHID1[]" id="txtBHID1_<?php echo $ttl_bg1[$i];?>" value="<?php echo $ttl_bg1[$i];?>" class="form-control"/>
												 <input type="hidden" name="txtTTLBAA_<?php echo $ttl_bg1[$i];?>" id="txtTTLBAA_<?php echo $ttl_bg1[$i];?>" class="form-control" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"OWNER_ADJ_COST");?>" />
												 <input type="hidden" name="txtTTLEst_<?php echo $ttl_bg1[$i];?>" id="txtTTLEst_<?php echo $ttl_bg1[$i];?>" class="form-control" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"EST_COST");?>"  /></td>
												<?php }?>
											</tr>
		
											<tr class="GridviewScrollItem">
												<td class="fixed-column">Estimated</td>
												<?php $j=$k=0; for($i=0;$i<count($ttl_bg);$i++){			
												?>
												<td>
												<input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_1" id="txt<?php echo $ttl_bg[$i];?>_1_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"EST_MT");?>" class="form-control" autocomplete="off" style="width:150px;"/>
												</td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_2" id="txt<?php echo $ttl_bg[$i];?>_1_2" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"EST_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;" /></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_3" id="txt<?php echo $ttl_bg[$i];?>_1_3" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"EST_COST");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;" /></td>
												<?php }?>
											</tr>
		
											<tr class="GridviewScrollItem">
												<td class="fixed-column">Supply</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_1" id="txt<?php echo $ttl_bg[$i];?>_8_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"SUPPLY_MT");?>" class="form-control" autocomplete="off" style="width:150px;" /></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_2" id="txt<?php echo $ttl_bg[$i];?>_8_2" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"SUPPLY_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_3" id="txt<?php echo $ttl_bg[$i];?>_8_3" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"SUPPLY_COST");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;"/></td>
												<?php }?>
											</tr>
											<tr class="GridviewScrollItem">
												<td class="fixed-column">Supplier Adjustment</td>
													<?php for($i=0;$i<count($ttl_bg);$i++){
													
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_1" id="txt<?php echo $ttl_bg[$i];?>_9_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"SUPP_ADJ_MT");?>" class="form-control" autocomplete="off" style="width:150px;"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_2" id="txt<?php echo $ttl_bg[$i];?>_9_2" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"SUPP_ADJ_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_3" id="txt<?php echo $ttl_bg[$i];?>_9_3" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"SUPP_ADJ_COST");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;"/></td>
												<?php }?>
											</tr>
											<tr class="GridviewScrollItem">
												<td class="fixed-column">Nett Supply </td>
													<?php for($i=0;$i<count($ttl_bg);$i++){
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_1" id="txt<?php echo $ttl_bg[$i];?>_10_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"NETT_MT");?>" class="form-control" autocomplete="off" style="width:150px;"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_2" id="txt<?php echo $ttl_bg[$i];?>_10_2" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"NETT_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_3" id="txt<?php echo $ttl_bg[$i];?>_10_3" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"NETT_COST");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;"/></td>
												<?php }?>
											</tr>
											<tr class="GridviewScrollItem">
												<td class="fixed-column">Actual Consumption</td>
													<?php for($i=0;$i<count($ttl_bg);$i++){
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_1" id="txt<?php echo $ttl_bg[$i];?>_11_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"ACTUAL_MT");?>" class="form-control" autocomplete="off" style="width:150px;"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_2" id="txt<?php echo $ttl_bg[$i];?>_11_2" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"ACTUAL_PRICE");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;"/></td>

													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_3" id="txt<?php echo $ttl_bg[$i];?>_11_3" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$ttl_bg1[$i],"ACTUAL_COST");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;"/></td>
												    <?php }?>
											</tr>
										<?php }?>									
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Bunkers Nett Supply</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="20%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="20%">Value/MT</th>
											</tr>
										</thead>
										<tbody id="tbbunkers">
											<?php 
											for($i=0;$i<count($ttl_bg);$i++){
											$query = "select * from freight_cost_estimete_slave8 where BUNKERGRADEID='".$ttl_bg1[$i]."' and FCAID='".$obj->getFun1()."'";
											$qres = mysql_query($query) or die($query);
											$qrec = mysql_num_rows($qres);
											$qrows = mysql_fetch_assoc($qres);
											?>
											<tr id="rowB_<?php echo $ttl_bg[$i];?>">
												<td><?php echo $ttl_bg[$i];?> Nett</td>
												<td></td>
												<td><input type="text"  name="txtBunkerCost_<?php echo $ttl_bg[$i];?>" id="txtBunkerCost_<?php echo $ttl_bg[$i];?>" class="form-control" readonly value="<?php echo $qrows['COST'];?>"/></td>
												<td><select  name="selBFOVList_<?php echo $ttl_bg[$i];?>" class="select form-control" id="selBFOVList_<?php echo $ttl_bg[$i];?>"></select>
												<script>
												$("#selBFOVList_<?php echo $ttl_bg[$i];?>").html($("#selVendor").html());
												$("#selBFOVList_<?php echo $ttl_bg[$i];?>").val('<?php echo $qrows['VENDORID'];?>')
												</script>
												</td>
												<td><input type="text" name="txtBunkerCostMT_<?php echo $ttl_bg[$i];?>" id="txtBunkerCostMT_<?php echo $ttl_bg[$i];?>" class="form-control" readonly value="<?php echo $qrows['COST_MT'];?>"/></td>
											</tr>
										<?php }?>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Owner Related Costs (Others)</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
											</tr>
										</thead>
										<tbody id="tbodyBrokerage">
                                        <tr>
                                            <td></td>
                                            <td>Address Commission</td>
                                            <td><input type="text"  name="txtFrAdjPerAC" id="txtFrAdjPerAC" class="form-control" autocomplete="off"  onkeyup="getFinalCalculation();" value="<?php echo $obj->getFun74();?>"/></td>
                                            <td><input type="text"  name="txtFrAdjUsdAC" id="txtFrAdjUsdAC" class="form-control" readonly value="<?php echo $obj->getFun75();?>"  /></td>
                                            <td><input type="text"  name="txtFrAdjUsdACMT" id="txtFrAdjUsdACMT" class="form-control" readonly value="<?php echo $obj->getFun76();?>"  /></td>
										</tr>
										<?php 
										$sql_brok = "select * from freight_cost_estimete_slave4 where FCAID='".$obj->getFun1()."'";
										
										$res_brok = mysql_query($sql_brok);
										$num_brok = mysql_num_rows($res_brok);
										if($num_brok==0)
										{$num_brok =1;
										?>
                                             <tr id="tbrRow_1">
                                                <td><a href="#tb1'" onClick="removeBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td>
												<td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="form-control" readonly value="0.00" /></td>
												<td><select  name="selBrCommVendor_1" class="select form-control" id="selBrCommVendor_1"></select></td>
                                                <script>$("#selBrCommVendor_1").html($("#selVendor").html());</script>
											</tr>
									<?php }
									     else
									      {$i=0;$num_brok = $num_brok;
										  while($rows_brok = mysql_fetch_assoc($res_brok))
										  {$i = $i + 1;?>
										  
										      <tr id="tbrRow_<?php echo $i;?>">
                                                <td><a href="#tb1'" onClick="removeBrokerage(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_<?php echo $i;?>" id="txtBrCommPercent_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows_brok['BROKAGE_PERCENT'];?>" onKeyUp="getFinalCalculation();"  /></td>
												<td><input type="text" name="txtBrComm_<?php echo $i;?>" id="txtBrComm_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows_brok['BROKAGE_AMT'];?>" /></td>
												<td><select name="selBrCommVendor_<?php echo $i;?>" class="select form-control" id="selBrCommVendor_<?php echo $i;?>"></select></td>
                                                <script>$("#selBrCommVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selBrCommVendor_<?php echo $i;?>").val('<?php echo $rows_brok['VENDORID'];?>');</script>
											</tr>
										  <?php }} ?>
										</tbody>
										
										<tbody>
										<tr>
											<td><button type="button" class="btn btn-primary btn-flat" onClick="addBrokerageRow()">Add</button><input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="1"/></td>
											<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
											
											<td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="form-control" autocomplete="off" value="<?php echo $obj->getFun82();?>" onKeyUp="getFinalCalculation();" readonly/></td>
											<td><input type="text" name="txtBrComm" id="txtBrComm" class="form-control" readonly value="<?php echo $obj->getFun83();?>" /></td>
											<td></td>
										</tr>
									   </tbody>
                                       <tbody>
                                           <tr>
												<td>CVE</td>
												<td></td>
                                                <td><input type="text" name="txtCVE" id="txtCVE" onkeyup="getFinalCalculation();" class="form-control" autocomplete="off" value="<?php echo $obj->getFun142();?>" placeholder="0.00"></td>
                                                <td>
                                                <input type="text" name="txtCVEAmt" id="txtCVEAmt" class="form-control" autocomplete="off" value="<?php echo $obj->getFun143();?>" placeholder="0.00" readonly>
												</td>
												<td>
                                                <select name="selCVEVendor" class="select form-control" id="selCVEVendor">
                                                </select>
												<input type="hidden" name="txtHidCVEAmt" id="txtHidCVEAmt" autocomplete="off" value="<?php echo $obj->getFun143();?>">
                                                </td>
												<script>$("#selCVEVendor").html($("#selVendor").html());$("#selCVEVendor").val('<?php echo $obj->getFun144();?>')</script>
											</tr>
                                       </tbody>
									   <tbody id="OWCBody">
                                      <?php $sql1 = "select * from freight_cost_estimete_slave3 where FCAID='".$obj->getFun1()."' and IDENTIFY='ORC' ";
											$res1 = mysql_query($sql1) or die($sql1);
											$rec1 = mysql_num_rows($res1);
											$num1 = 0;
											if($rec1 == 0)
											{$num1 = 1;?>
											<tr id="OWCRow_1">
                                                <td><a href="#tb1'" onClick="removeOWC(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                <td></td>
                                                <td><input type="text" name="txtHidORCID_1" id="txtHidORCID_1" class="form-control" placeholder="Owner Related Costs" autocomplete="off" value="" /></td>
                                                <td><input type="text" name="txtORCAmt_1" id="txtORCAmt_1" onKeyUp="getORCCalculate(1);" class="form-control" autocomplete="off" value="" placeholder="0.00"  /></td>
                                                <td><select  name="selORCVendor_1" class="select form-control" id="selORCVendor_1"></select><input type="hidden" name="txtHidORCAmt_1" id="txtHidORCAmt_1"  class="form-control" autocomplete="off" value="0.00"  readonly /></td>
                                                <td></td>
                                                <script>$("#selORCVendor_1").html($("#selVendor").html());</script>
                                            </tr>	
											<?php 
											}
											else
											{$i = 0;$num1 = $rec1;
											while($rows1 = mysql_fetch_assoc($res1))
											{$i = $i + 1;//FCA_SLAVE3ID, FCAID, IDENTY_ID, IDENTIFY, COST, COST_MT
											?>
										 <tr id="OWCRow_<?php echo $i;?>">
											<td><a href="#tb<?php echo $i;?>" onClick="removeOWC(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                            <td></td>
											<td><input type="text" name="txtHidORCID_<?php echo $i;?>" id="txtHidORCID_<?php echo $i;?>" class="form-control" placeholder="Owner Related Costs" autocomplete="off" value="<?php echo $rows1['IDENTY_ID'];?>" /></td>
											<td><input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>" onKeyUp="getORCCalculate(<?php echo $i;?>);" class="form-control" autocomplete="off" value="<?php echo $rows1['COST'];?>" placeholder="0.00"  /></td>
											<td><select  name="selORCVendor_<?php echo $i;?>" class="select form-control" id="selORCVendor_<?php echo $i;?>"></select><input type="hidden" name="txtHidORCAmt_<?php echo $i;?>" id="txtHidORCAmt_<?php echo $i;?>"  class="form-control" autocomplete="off" value="<?php echo $rows1['COST_MT'];?>"  readonly />	</td>
											<script>$("#selORCVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selORCVendor_<?php echo $i;?>").val('<?php echo $rows1['VENDORID'];?>');</script>
                                         </tr>
                                         <?php }}?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                        <td align="left"><button type="button" class="btn btn-primary btn-flat" onClick="addOWCRow()">Add</button><input type="hidden" name="txtORCCount" id="txtORCCount" value="<?php echo $num1;?>"/></td>
                                        <td></td>
                                        <td>Total Shipowner Expenses</td>
                                        <td><input type="text"  name="txtTTLORCAmt" id="txtTTLORCAmt" class="form-control" readonly placeholder="0.00" value="<?php echo $obj->getFun84();?>"/></td>
                                        <td></td>												
										</tr>
										<tr>
                                            <td></td>
                                            <td></td>
                                            <td style="font-weight:bold;">Value/MT</td>
                                            <td><input type="text" name="txtTTLORCCostMT" id="txtTTLORCCostMT" class="form-control"  readonly="true" placeholder="0.00" value="<?php echo $obj->getFun85();?>" /></td>
                                            <td></td>
                                        </tr>
                                        </tfoot>
									 </table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Charterers' Costs</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="5%"></th>
												<th width="19%"></th>
                                                <th width="19%">Add Comm (%)</th>
												<th width="19%">Cost</th>
                                                <th width="19%">Vendor</th>
												<th width="19%">Nett Value</th>
											</tr>
										</thead>
                                        <tbody id="CCBody">
                                        <?php $sql2 = "select * from freight_cost_estimete_slave3 where FCAID='".$obj->getFun1()."' and IDENTIFY='CC' ";
											$res2 = mysql_query($sql2) or die($sql2);
											$rec2 = mysql_num_rows($res2);
											$num2 = 0;
											if($rec2 == 0)
											{$num2 = 1;?>
                                            <tr id="CCRow_1">
                                                <td><a href="#tb1'" onClick="removeCC(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                
                                                <td><input type="text" name="txtHidCCID_1" id="txtHidCCID_1" class="form-control" placeholder="Charterers' Costs" autocomplete="off" value="" /></td>
                                                <td><input type="text" name="txtCCAddComm_1" id="txtCCAddComm_1" onKeyUp="getCharterersCostCalculate();" class="form-control" autocomplete="off" value="" placeholder="0.00"  /></td>
                                                <td><input type="text" name="txtCCAmount_1" id="txtCCAmount_1" onKeyUp="getCharterersCostCalculate();" class="form-control" autocomplete="off" value="" placeholder="0.00"  /></td>
                                                <td><select name="selCCVendor_1" class="select form-control" id="selCCVendor_1" ></select></td>
                                                <script>$("#selCCVendor_1").html($("#selVendor").html());</script>
                                                <td><input type="text" name="txtCCAbs_1" id="txtCCAbs_1" class="form-control" autocomplete="off" value="" placeholder="0.00"  /><input type="hidden" name="txtCCostMT_1" id="txtCCostMT_1" class="form-control" readonly placeholder="0.00" /></td>
                                            </tr>
                                        <?php 
											}
											else
											{$i = 0;$num2 = $rec2;
											while($rows2 = mysql_fetch_assoc($res2))
											{$i = $i + 1;
											?>
                                             <tr id="CCRow_<?php echo $i;?>">
                                                <td><a href="#tb<?php echo $i;?>'" onClick="removeCC(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                <td><input type="text" name="txtHidCCID_<?php echo $i;?>" id="txtHidCCID_<?php echo $i;?>" class="form-control" placeholder="Charterers' Costs" autocomplete="off" value="<?php echo $rows2['IDENTY_ID'];?>" /></td>
                                                <td><input type="text" name="txtCCAddComm_<?php echo $i;?>" id="txtCCAddComm_<?php echo $i;?>" onKeyUp="getCharterersCostCalculate();" class="form-control" autocomplete="off" value="<?php echo $rows2['ADDCOMM'];?>" placeholder="0.00"  /></td>
                                                <td><input type="text" name="txtCCAmount_<?php echo $i;?>" id="txtCCAmount_<?php echo $i;?>" onKeyUp="getCharterersCostCalculate();" class="form-control" autocomplete="off" value="<?php echo $rows2['COST'];?>" placeholder="0.00"  /></td>
                                                <td><select name="selCCVendor_<?php echo $i;?>" class="select form-control" id="selCCVendor_<?php echo $i;?>" ></select></td>
                                                <script>$("#selCCVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selCCVendor_<?php echo $i;?>").val('<?php echo $rows2['VENDORID'];?>');</script>
                                                <td><input type="text" name="txtCCAbs_<?php echo $i;?>" id="txtCCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows2['NET_AMOUNT'];?>" placeholder="0.00"  /><input type="hidden" name="txtCCostMT_<?php echo $i;?>" id="txtCCostMT_<?php echo $i;?>" class="form-control" readonly placeholder="0.00"  value="<?php echo $rows2['COST_MT'];?>"/></td>
                                            </tr>
                                       <?php }}?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                        <td colspan="5" align="left"><button type="button" class="btn btn-primary btn-flat" onClick="addCCRow()">Add</button><input type="hidden" name="txtCCCount" id="txtCCCount" value="<?php echo $num2;?>"/></td>
                                        </tr>
                                        </tfoot>
										
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Demurrage Dispatch Customer</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="15%">Add Comm (%)</th>
												<th width="15%">Estimated Cost</th>
                                                <th width="15%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="15%">Nett Value</th>
											</tr>
										</thead>
										<tbody id="tbDDCBody">
										<?php 
											$mysql2 = "select * from freight_cost_estimete_slave1 where FCAID='".$obj->getFun1()."'";
											$myres2 = mysql_query($mysql2);
											$myrec2 = mysql_num_rows($myres2);
											if($myrec2 ==0)
											{?>
												<tr id="DDCLProw_1"> 
												<td>Load Port <span id="spanDDCLPort_1"></span></td>
												<td><input type="text"  name="txtDDCLPComm_1" id="txtDDCLPComm_1" class="form-control" autocomplete="off" value="" onKeyUp="getFinalCalculation();" placeholder="0.00" /></td>
												
												<td><input type="text"  name="txtEstDDCLPCost_1" id="txtEstDDCLPCost_1" class="form-control" value="0" autocomplete="off" onKeyUp="getFinalCalculation();" /><input type="hidden" name="txtLPLayTimeIs_1" id="txtLPLayTimeIs_1" value="" /></td>
												
												<td><input type="text"  name="txtDDCLPCost_1" id="txtDDCLPCost_1" class="form-control" value="0" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td>
                                                <td><select name="selDDCLPVendor_1" class="select form-control" id="selDDCLPVendor_1" ></select></td>
												<td><input type="text" name="txtDDCLPNetCostMT_1" id="txtDDCLPNetCostMT_1" class="form-control" readonly value="" /><input type="hidden" name="txtDDCLPCostMT_1" id="txtDDCLPCostMT_1" class="form-control" readonly value="0" /></td>
                                                <script>$("#selDDCLPVendor_1").html($("#selVendor").html());</script>
											  </tr>	
                                              <tr id="DDCDProw_1"> 
												<td>Discharge Port <span id="spanDDCDPort_1"></span></td>
												<td><input type="text"  name="txtDDCDPComm_1" id="txtDDCDPComm_1" class="form-control" autocomplete="off" value="" onKeyUp="getFinalCalculation();" placeholder="0.00" /></td>
												
												<td><input type="text"  name="txtEstDDCDPCost_1" id="txtEstDDCDPCost_1" class="form-control" value="0" autocomplete="off" onKeyUp="getFinalCalculation();" /><input type="hidden" name="txtDPLayTimeIs_1" id="txtDPLayTimeIs_1" value="" /></td>
												
												<td><input type="text"  name="txtDDCDPCost_1" id="txtDDCDPCost_1" class="form-control" value="0" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td>
                                                <td><select name="selDDCDPVendor_1" class="select form-control" id="selDDCDPVendor_1" ></select></td>
												<td><input type="text" name="txtDDCDPNetCostMT_1" id="txtDDCDPNetCostMT_1" class="form-control" readonly value="" /><input type="hidden" name="txtDDCDPCostMT_1" id="txtDDCDPCostMT_1" class="form-control" readonly value="0" /></td>
                                                <script>$("#selDDCDPVendor_1").html($("#selVendor").html());</script>
											  </tr>	
									 <?php }
											else
											{$i=1;
											while($myrows2 = mysql_fetch_assoc($myres2))
											{
											$laytime_valLP = $obj->getLayTimeValuesBasedOnComID($comid,$myrows2['FROM_PORT'],"LP",$myrows2['RANDOMID']);
											$laytime_valLP = explode('@#@',$laytime_valLP);
											if($laytime_valLP[1]== 0){$laytimeLP = $laytime_valLP[2];}else{$laytimeLP = $laytime_valLP[1];}
											if($laytime_valLP[0]== 0){$readonlyLP = "";}else{$readonlyLP = "readonly";}
											
											$laytime_valDP = $obj->getLayTimeValuesBasedOnComID($comid,$myrows2['TO_PORT'],"DP",$myrows2['RANDOMID']);
											$laytime_valDP = explode('@#@',$laytime_valDP);
											if($laytime_valDP[1]== 0){$laytimeDP = $laytime_valDP[2];}else{$laytimeDP = $laytime_valDP[1];}
											if($laytime_valDP[0]== 0){$readonlyDP = "";}else{$readonlyDP = "readonly";}
											?>
											<tr id="DDCLProw_<?php echo $i;?>"> 
												<td>Load Port <span id="spanDDCLPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($myrows2['FROM_PORT']);?></span></td>
												<td><input type="text"  name="txtDDCLPComm_<?php echo $i;?>" id="txtDDCLPComm_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $myrows2['DDCLP_COMM'];?>" onKeyUp="getFinalCalculation();" placeholder="0.00" /></td>
												
												<td ><input type="text"  name="txtEstDDCLPCost_<?php echo $i;?>" id="txtEstDDCLPCost_<?php echo $i;?>" class="form-control" value="<?php echo $myrows2['DDCLP_ESTCOST'];?>" autocomplete="off" onKeyUp="getFinalCalculation();" <?php echo $readonlyLP;?> <?php echo $backgroundLP;?>/><input type="hidden" name="txtLPLayTimeIs_<?php echo $i;?>" id="txtLPLayTimeIs_<?php echo $i;?>" value="<?php echo $laytime_valLP[0];?>" /></td>
												
												<td><input type="text"  name="txtDDCLPCost_<?php echo $i;?>" id="txtDDCLPCost_<?php echo $i;?>" class="form-control" value="<?php echo $laytimeLP;?>" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td>
                                                <td><select name="selDDCLPVendor_<?php echo $i;?>" class="select form-control" id="selDDCLPVendor_<?php echo $i;?>" ></select></td>
                                                <?php $omcvalueLP = $myrows2['DDCLP_ESTCOST'] - (($myrows2['DDCLP_ESTCOST'] * $myrows2['DDCLP_COMM'])/100);?>
												<td><input type="text" name="txtDDCLPNetCostMT_<?php echo $i;?>" id="txtDDCLPNetCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $omcvalueLP;?>" /><input type="hidden" name="txtDDCLPCostMT_<?php echo $i;?>" id="txtDDCLPCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $myrows2['DDCLP_NETCOST'];?>" /></td>
                                        <script>$("#selDDCLPVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selDDCLPVendor_<?php echo $i;?>").val('<?php echo $myrows2['DDCLP_VENDOR'];?>');</script>
											  </tr>	
                                              <tr id="DDCDProw_<?php echo $i;?>"> 
												<td>Discharge Port <span id="spanDDCDPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($myrows2['TO_PORT']);?></span></td>
												<td><input type="text"  name="txtDDCDPComm_<?php echo $i;?>" id="txtDDCDPComm_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $myrows2['DDCDP_COMM'];?>" onKeyUp="getFinalCalculation();" placeholder="0.00" /></td>
												
												<td ><input type="text"  name="txtEstDDCDPCost_<?php echo $i;?>" id="txtEstDDCDPCost_<?php echo $i;?>" class="form-control" value="<?php echo $myrows2['DDCDP_ESTCOST'];?>" autocomplete="off" onKeyUp="getFinalCalculation();" <?php echo $readonlyDP;?> <?php echo $backgroundDP;?>/><input type="hidden" name="txtDPLayTimeIs_<?php echo $i;?>" id="txtDPLayTimeIs_<?php echo $i;?>" value="<?php echo $laytime_valDP[0];?>" /></td>
												
												<td><input type="text"  name="txtDDCDPCost_<?php echo $i;?>" id="txtDDCDPCost_<?php echo $i;?>" class="form-control" value="<?php echo $laytimeDP;?>" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td>
                                                <td><select name="selDDCDPVendor_<?php echo $i;?>" class="select form-control" id="selDDCDPVendor_<?php echo $i;?>" ></select></td>
                                                <?php $omcvalueDP = $myrows2['DDCDP_ESTCOST'] - (($myrows2['DDCDP_ESTCOST'] * $myrows2['DDCDP_COMM'])/100);?>
												<td><input type="text" name="txtDDCDPNetCostMT_<?php echo $i;?>" id="txtDDCDPNetCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $omcvalueDP;?>" /><input type="hidden" name="txtDDCDPCostMT_<?php echo $i;?>" id="txtDDCDPCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $myrows2['DDCDP_NETCOST'];?>" /></td>
                                                <script>$("#selDDCDPVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selDDCDPVendor_<?php echo $i;?>").val('<?php echo $myrows2['DDCDP_VENDOR'];?>');</script>
											 </tr>		
									<?php $i++;}} ?>
                                         </tbody>
									</table>
								</div>
							</div>
							
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Other Misc. Income</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="5%"></th>
                                                <th width="19%"></th>
												<th width="19%">Add Comm (%)</th>
												<th width="19%">Cost</th>
												<th width="19%">Vendor</th>
												<th width="19%">Nett Value</th>
											</tr>
										</thead>
                                        <tbody id="OMIBody">
                                        <?php $sql3 = "select * from freight_cost_estimete_slave3 where FCAID='".$obj->getFun1()."' and IDENTIFY='OMI' ";
											$res3 = mysql_query($sql3) or die($sql3);
											$rec3 = mysql_num_rows($res3);
											$num3 = 0;
											if($rec3 == 0)
											{$num3 = 1;?>
                                            <tr id="OMIRow_1">
                                                <td><a href="#OMI1'" onClick="removeOMI(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                
                                                <td><input type="text" name="txtHidOMCID_1" id="txtHidOMCID_1" class="form-control" placeholder="Other Misc. Income" autocomplete="off" value="" /></td>
                                                <td><input type="text" name="txtOMCAddComm_1" id="txtOMCAddComm_1" onKeyUp="getOMCCalculate();" class="form-control" autocomplete="off" value="" placeholder="0.00" /></td>
                                                <td><input type="text" name="txtOMCAmount_1" id="txtOMCAmount_1" onKeyUp="getOMCCalculate();" class="form-control" autocomplete="off" value="" placeholder="0.00" readonly/></td>
                                                <td><select name="selOMCVendor_1" class="select form-control" id="selOMCVendor_1" ></select></td>
                                                <td><input type="text" name="txtOMCAbs_1" id="txtOMCAbs_1" class="form-control" autocomplete="off" value="" placeholder="0.00"  /><input type="hidden" name="txtOMCCostMT_1" id="txtOMCCostMT_1" class="form-control" readonly placeholder="0.00" /></td>
                                            </tr>
                                             <script>$("#selOMCVendor_1").html($("#selVendor").html());</script>
                                        <?php 
											}
											else
											{$i = 0;$num3 = $rec3;
											while($rows3 = mysql_fetch_assoc($res3))
											{$i = $i + 1;
											?>
                                              <tr id="OMIRow_<?php echo $i;?>">
                                                <td><a href="#OMI<?php echo $i;?>'" onClick="removeOMI(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                <td><input type="text" name="txtHidOMCID_<?php echo $i;?>" id="txtHidOMCID_<?php echo $i;?>" class="form-control" placeholder="Other Misc. Income" autocomplete="off" value="<?php echo $rows3['IDENTY_ID'];?>" /></td>
                                                <td><input type="text" name="txtOMCAddComm_<?php echo $i;?>" id="txtOMCAddComm_<?php echo $i;?>" onKeyUp="getOMCCalculate();" class="form-control" autocomplete="off" value="<?php echo $rows3['ADDCOMM'];?>" placeholder="0.00"  /></td>
                                                <td><input type="text" name="txtOMCAmount_<?php echo $i;?>" id="txtOMCAmount_<?php echo $i;?>" onKeyUp="getOMCCalculate();" class="form-control" autocomplete="off" value="<?php echo $rows3['RAW_AMOUNT'];?>" placeholder="0.00"  /></td>
                                                <td><select name="selOMCVendor_<?php echo $i;?>" class="select form-control" id="selOMCVendor_<?php echo $i;?>" ></select></td>
                                                <script>$("#selOMCVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selOMCVendor_<?php echo $i;?>").val('<?php echo $rows3['VENDORID'];?>');</script>
                                                <td><input type="text" name="txtOMCAbs_<?php echo $i;?>" id="txtOMCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows3['COST'];?>" placeholder="0.00"  /><input type="hidden" name="txtOMCCostMT_<?php echo $i;?>" id="txtOMCCostMT_<?php echo $i;?>" class="form-control" readonly placeholder="0.00"  value="<?php echo $rows3['COST_MT'];?>"/></td>
                                              </tr>
                                         <?php }}?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                        <td colspan="5" align="left"><button type="button" class="btn btn-primary btn-flat" onClick="addOMIRow()">Add</button><input type="hidden" name="txtOMICount" id="txtOMICount" value="<?php echo $num3;?>"/></td>
                                        </tr>
                                        </tfoot>
                                        
                                        
										
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Results</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="35%"></th>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="25%"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Revenue (Final Nett Freight)</td>
												<td></td>
												<td><input type="text"  name="txtRevenue" id="txtRevenue" class="form-control" readonly placeholder="0.00" value="<?php echo $obj->getFun86();?>" /></td>
                                                <td></td>
											</tr>
											<tr>
												<td>Total Owners Expenses</td>
												<td></td>
												<td><input type="text"  name="txtTTLOwnersExpenses" id="txtTTLOwnersExpenses" class="form-control" readonly  placeholder="0.00" value="<?php echo $obj->getFun87();?>" /></td>
                                                <td></td>
											</tr>	
											<tr>
												<td>Total Charterers' Expenses</td>
												<td></td>
												<td><input type="text"  name="txtTTLCharterersExpenses" id="txtTTLCharterersExpenses" class="form-control" readonly value="<?php echo $obj->getFun88();?>" placeholder="0.00" /></td>
                                                <td></td>
											</tr>
											<tr>
												<td>Voyage Earnings</td>
												<td></td>
												<td><input type="text"  name="txtVoyageEarnings" id="txtVoyageEarnings" class="form-control" readonly placeholder="0.00"  value="<?php echo $obj->getFun89();?>"/></td>
                                                <td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;" >Daily Earnings / TCE</td>
												<td></td>
												<td><input type="text"  name="txtDailyEarnings" id="txtDailyEarnings" class="form-control" readonly placeholder="0.00" value="<?php echo $obj->getFun90();?>" /></td>
                                                <td></td>
											</tr>
											<tr>
												<td>Daily Time Charter (USD/Day)</td>
												<td></td>
												<td><input type="text"  name="txtDailyVesselOperatingExpenses" id="txtDailyVesselOperatingExpenses" class="form-control" autocomplete="off" value="<?php echo $obj->getFun91();?>" placeholder="0.00" onKeyUp="getFinalCalculation();" /></td>
                                                <td><select name="selDTCVendor" class="select form-control" id="selDTCVendor" ></select></td>
                                                <script>$("#selDTCVendor").html($("#selVendor").html());$("#selDTCVendor").val('<?php echo $obj->getFun140();?>');</script>
											</tr>
                                            <tr>
												<td>Hireage </td>
												<td></td>
                                                <td><input type="text"  name="txtHireargeAmt" id="txtHireargeAmt" class="form-control" value="<?php echo $obj->getFun132();?>" placeholder="0.00" readonly /></td>
                                                <td></td>
												
											</tr>
                                            <tr>
												<td>Add Comm (%)</td>
                                                <td><input type="text"  name="txtHireargePercent" id="txtHireargePercent" class="form-control" value="<?php echo $obj->getFun129();?>" placeholder="%" onKeyUp="getFinalCalculation();"/></td>
												<td><input type="text"  name="txtHireargePercentAmt" id="txtHireargePercentAmt" class="form-control" value="<?php echo $obj->getFun130();?>" placeholder="0.00" readonly /></td>
                                                <td></td>
												
											</tr>
                                            
											<tr>
												<td style="color:#dc631e;" >G Total Voyage Earnings&nbsp;&nbsp;&nbsp;<span style="font-size:10px; font-style:italic;">(Voyage Earnings + Demurrage)</span></td>
												<td></td>
												<td><input type="text"  name="txtGTTLVoyageEarnings" id="txtGTTLVoyageEarnings" class="form-control" readonly  placeholder="0.00" value="<?php echo $obj->getFun92();?>" /></td>
                                                <td></td>
											</tr>
                                            <tr>
												<td>Nett Hireage </td>
												<td></td>
                                                <td><input type="text"  name="txtNettHireargeAmt" id="txtNettHireargeAmt" class="form-control" value="<?php echo $obj->getFun131();?>" placeholder="0.00" readonly /></td>
                                                <td></td>
												
											</tr>
											<tr>
												<td style="color:#dc631e;">Nett Daily Earnings</td>
												<td></td>
												<td><input type="text"  name="txtNettDailyEarnings" id="txtNettDailyEarnings" class="form-control" readonly  placeholder="0.00" value="<?php echo $obj->getFun94();?>" /></td>
                                                <td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">Nett Daily Profit</td>
												<td></td>
												<td><input type="text"  name="txtNettDailyProfit" id="txtNettDailyProfit" class="form-control" readonly placeholder="0.00" value="<?php echo $obj->getFun93();?>" /></td>
                                                <td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">P/L</td>
												<td></td>
												<td><input type="text"  name="txtPL" id="txtPL" class="form-control" readonly placeholder="0.00" value="<?php echo $obj->getFun95();?>" /></td>
                                                <td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							 
                        <div class="box-footer" align="right">
                            <input type="hidden" name="txtCRMFILE" id="txtCRMFILE" value="" />
							<input type="hidden" name="txtCRMNAME" id="txtCRMNAME" value="" />
							<input type="hidden" name="update_status" id="update_status" class="form-control" value="" />
				        </div>
					
                  </form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />

<script type="text/javascript">
$(document).ready(function(){ 
$("#frm1").validate({
	rules: {
		txtENo:"required",
		selVName:"required",
		selCOASpot:"required",
		selBroker:"required",
		txtCPDate:"required",
		txtFDate:"required",
		txtTDate:"required",
		selOwner: "required",
		selVCType: "required",
		selCOA: {  required: function(element) {return $("#selCOASpot").val() == 2;}},
		txtNoLift:{  required: function(element) {return $("#selCOASpot").val() == 2;},digits:true},
		txtCID:{required: true},
		selMType:"required",
		txtMCode:"required",
		selLPort:"required",
		selDPort:"required",
		txtLCSDate:"required",
		txtLCFDate:"required",
		txtQty : {required:true ,number : true}
		},
	messages: {
		selVName:"*",
		selCOASpot:"*",
		selBroker:"*",
		txtCPDate:"*",
		txtFDate:"*",
		txtTDate:"*",
		selOwner:"*",
		selVCType: "*",
		selCOA : "*",
		txtNoLift : {  required: "*",digits:"*"}
		},
submitHandler: function(form)  {
	    if($("#selLPort").val() != $("#selDPort").val())
		{
			var file_temp_name1 = $("[id^=file_]").map(function () {return this.value;}).get().join(",");
			$('#txtCRMFILE').val(file_temp_name1);
			var file_actual_name1 = $("[id^=name_]").map(function () {return this.value;}).get().join(",");
			$('#txtCRMNAME').val(file_actual_name1);
			jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
				if(r){
						jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
						$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
						$("#popup_content").css({"background":"none","text-align":"center"});
						$("#popup_ok,#popup_title").hide();  
						frm1.submit();
					 }
				else{return false;}
				});
		}
		else
		{
			jAlert('Load port and discharge port can not be same in cargo open details.', 'Alert');
			return false;
		}
		
	}
});
$("#selCType").val(<?php echo $obj->getFun51();?>);
$("#selCName").val(<?php echo $obj->getFun52();?>);
$("[id^=txtQtyAggriedFreight_],[id^=txtFreightQty_],#txtMTCPDRate,#txtAddnlCRate,#txtCQMT,#txtDFQMT,[id^=txtMCWS_],[id^=txtMCDistanceLeg_], [id^=txtMCTotalDistance_],[id^=txtOvrWS_],[id^=txtOvrDistanceLeg_],[id^=txtddswDPCost_],[id^=txtddswLPCost_],[id^=txtOvrTotalDistance_], [id^=txtAdditionAmt_],[id^=txtDeductionAmt_],#txtFrAdjUsdGF,#txtFrAdjPerACTF,#txtFrAdjPerACGF,[id^=txtOMCAbs_],#txtDWTS,#txtDWTT,#txtSF,#txtTFUMTManual, #txtTDUMTManual,#txtQtyAggriedFreight, #txtFreightQty,#txtQtyLocalAggriedFreight,#txtDisExchangeRate,#txtMarLocalAggriedFreight, #txtMarExchangeRate,[id^=txtORCAmt_],[id^=txtDistance_],[id^=txtWeather_],[id^=txtMargin_],[id^=txtPCosts_],[id^=txtQMT_],[id^=txtRate_],[id^=txtIDays_], [id^=txtDWDays_],[id^=txtWDays_],[id^=txtTLPCosts_],[id^=txtTLIDays_],#txtFrAdjPerAC,#txtFrAdjPerAgC,[id^=txtBrCommPercent_],[id^=txtORCAmt_],[id^=txtCCAbs_],#txtDailyVesselOperatingExpenses,#txtTolerance,#txtexpectedhire, #txtDWTS, #txtDWTT, #txtBFullSpeed, #txtBEcoSpeed1, #txtBEcoSpeed2, #txtBFOFullSpeed, #txtBFOEcoSpeed1, #txtBFOEcoSpeed2, #txtBDOFullSpeed, #txtBDOEcoSpeed1, #txtBDOEcoSpeed2, #txtLFullSpeed, #txtLEcoSpeed1, #txtLEcoSpeed2, #txtLFOFullSpeed, #txtLFOEcoSpeed1, #txtLFOEcoSpeed2, #txtLDOFullSpeed, #txtLDOEcoSpeed1, #txtLDOEcoSpeed2, #txtPIFOFullSpeed, #txtPWFOFullSpeed, #txtPIDOFullSpeed, #txtPWDOFullSpeed, [id^=txtLocationDistance_],[id^=txtQtyAggriedFreight_],[id^=txtFreightQty_],[id^=txtQtyBrokeragePer_],[id^=txtQtyLocalAggriedFreight_],[id^=txtDisExchangeRate_],[id^=txtMarLocalAggriedFreight_],[id^=txtMarExchangeRate_],[id^=txtHireargePercent_],[id^=txtHireargeAmt_],[id^=txtOMCAddComm_],[id^=txtOMCAmount_],[id^=txtCCAddComm_],[id^=txtCCAmount_],[id^=txtCCAbs_],[id^=txtOMCAbs_],#txtTFUMTManual,#txtTDUMTManual,#txtTFOCOffHire,#txtTDOCOffHire").numeric();

$('#txtLCSDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#txtLCFDate').datepicker('setStartDate', new Date(getString($(this).val())));
 });
 
$('#txtLCFDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){   });
 
$("#txtDate,#txtETADate,#txtCPDate").datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
});

$('#txtFDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#txtTDate').datepicker('setStartDate', new Date(getString($(this).val())));
 });
 
$('#txtTDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){   });

function getString(var1)
{
  var var2 = var1.split('-');  
  return var2[2]+'/'+var2[1]+'/'+var2[0];
}

$("#selOpenPort,#selLPort,#selDPort").html($("#selPort").html());
$("#selZone").val(<?php echo $obj->getFun101();?>);
$("#selCOASpot").val(<?php echo $obj->getFun126();?>);
//getShow();
if($("#selCOASpot").val()==2)
{
	$("#tr_coa,#tr_coa1").show();
}
$("#selShipper").val('<?php echo $obj->getFun117();?>');
$("#selBroker").val('<?php echo $obj->getFun127();?>');
$("#selOwner").val('<?php echo $obj->getFun107();?>');
$("#selMType").val('<?php echo $obj->getFun119();?>');
$("#selOpenPort").val(<?php echo $obj->getFun100();?>);
$("#selLPort").val(<?php echo $obj->getFun122();?>);
$("#selDPort").val(<?php echo $obj->getFun123();?>);
$("#selCOA").val(<?php echo $obj->getFun102();?>);
$("#selVCType").val(<?php echo $obj->getFun105();?>);
$("#txtDisponentOwner").val('<?php echo $obj->getFun108();?>');
$("#selCOASpot").val(<?php echo $obj->getFun103();?>);
//$("#txtCPDate").val(<?php echo $txtCPDate;?>);
<?php $sql5 = "select * from freight_cost_estimete_slave5 where FCAID='".$obj->getFun1()."'";
$result5 = mysql_query($sql5);
$k =0;
while($rows5 = mysql_fetch_assoc($result5))
{$k = $k + 1;?>
$("#txtDisponentOwner_<?php echo $k;?>").val('<?php echo $rows5['DISPONENT_OWNER'];?>');
<?php }?>
<?php
$sql = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
$res = mysql_query($sql);
$rec = mysql_num_rows($res);
$m=0; 
while($rows = mysql_fetch_assoc($res)){
?>
$("#txt<?php echo $rows['NAME'];?>_1_2").numeric();
<?php } ?>
showCapField();
getShowHide2();
getShowHide3();
showHideQtyVendorDiv(1);
getCharterersCostCalculate();
getOMCCalculate();
ShowDDCDPDP();
});

function getValue()
{
	
}

/*function getData()
{
	  var vsl_data = <?php //echo $obj->getVesselRelatedData();?>;
	  if($("#selVName").val()!="")
	  {
			  $.each(vsl_data[$("#selVName").val()], function(index, array) {
				if(array['num'] == "" || array['num']==0)
				{
					jAlert('Please ensure commercial parameters are completed before proceeding', 'Alert', function(r) {
					if(r){ 
						window.location="commercial_parameter.php?id="+$("#selVName").val();
						}
						else{return false;}
						});	
				}
				else
				{
					$('#txtVType').val(array['type']);
					$('#txtDWTS').val(array['dwtsum']);
					$('#txtDWTT').val(array['dwttrop']);
					$('#txtGCap').val(array['grain']);
					$('#txtBCap').val(array['bale']);
					
					$('#txtBFullSpeed').val(array['bfs']);
					$('#txtBEcoSpeed1').val(array['bes1']);
					$('#txtBEcoSpeed2').val(array['bes2']);
					$('#txtBFOFullSpeed').val(array['fobfs']);
					$('#txtBFOEcoSpeed1').val(array['fobes1']);
					$('#txtBFOEcoSpeed2').val(array['fobes2']);
					$('#txtBDOFullSpeed').val(array['dobfs']);
					$('#txtBDOEcoSpeed1').val(array['dobes1']);
					$('#txtBDOEcoSpeed2').val(array['dobes2']);
					
					$('#txtLFullSpeed').val(array['lfs']);
					$('#txtLEcoSpeed1').val(array['les1']);
					$('#txtLEcoSpeed2').val(array['les2']);
					$('#txtLFOFullSpeed').val(array['folfs']);
					$('#txtLFOEcoSpeed1').val(array['foles1']);
					$('#txtLFOEcoSpeed2').val(array['foles2']);
					$('#txtLDOFullSpeed').val(array['dolfs']);
					$('#txtLDOEcoSpeed1').val(array['doles1']);
					$('#txtLDOEcoSpeed2').val(array['doles2']);
					
					$('#txtPIFOFullSpeed').val(array['foidle']);
					$('#txtPWFOFullSpeed').val(array['fwking']);
					$('#txtPIDOFullSpeed').val(array['ddle']);
					$('#txtPWDOFullSpeed').val(array['dwking']);
					$('#vesselrec').val(array['num']);
					$('#txtFlag').val(array['flag']);
					$('#txtBuiltYear').val(array['year']);
					$('#txtGNRT').val(array['gnrt']);
					$('#txtLOA').val(array['loa']);
					$('#txtGear').val(array['gear']);
					$('#txtBeam').val(array['beam']);
					$('#txtTPC').val(array['tpc']);
					getVoyageTime();
				}
			});
	  }
	  else
	  {$('#txtVType,#txtDWTS,#txtDWTT,#txtGCap,#txtBCap,#txtBFullSpeed,#txtBEcoSpeed1,#txtBEcoSpeed2,#txtBFOFullSpeed,#txtBFOEcoSpeed1,#txtBFOEcoSpeed2,#txtBDOFullSpeed,#txtBDOEcoSpeed1,#txtBDOEcoSpeed2,#txtLFullSpeed,#txtLEcoSpeed1,#txtLEcoSpeed2,#txtLFOFullSpeed,#txtLFOEcoSpeed1,#txtLFOEcoSpeed2,#txtLDOFullSpeed,#txtLDOEcoSpeed1,#txtLDOEcoSpeed2,#txtPIFOFullSpeed,#txtPWFOFullSpeed,#txtPIDOFullSpeed,#txtPWDOFullSpeed,#vesselrec,#txtFlag,#txtGear,#txtBuiltYear,#txtGNRT,#txtLOA').val("");}
}*/

function getShowHide3()
{
	if($("#txtbid3").val() == 0)
	{
		document.BID3.src = "../../img/open.png";
		$('#BID3').attr('title', 'Open Panel');
		$("#opencargo").hide();
		$("#txtbid3").val(1);
	}
	else
	{
		document.BID3.src = "../../img/close.png";
		$('#BID3').attr('title', 'Close Panel');
		$("#opencargo").show();
		$("#txtbid3").val(0);
	}
}


function getShowHide2()
{
	if($("#txtbid2").val() == 0)
	{
		document.BID2.src = "../../img/open.png";
		$('#BID2').attr('title', 'Open Panel');
		$("#openvessel").hide();
		$("#txtbid2").val(1);
	}
	else
	{
		document.BID2.src = "../../img/close.png";
		$('#BID2').attr('title', 'Close Panel');
		$("#openvessel").show();
		$("#txtbid2").val(0);
	}
}


function getShowHide1()
{
	if($("#txtbid1").val() == 0)
	{
		document.BID1.src = "../../img/open.png";
		$('#BID1').attr('title', 'Open Panel');
		$("#bunker_adj1").hide();
		$("#txtbid1").val(1);
	}
	else
	{
		document.BID1.src = "../../img/close.png";
		$('#BID1').attr('title', 'Close Panel');
		$("#bunker_adj1").show();
		$("#txtbid1").val(0);
	}
}


function showDWTField()
{
	if($("#rdoDWT1").is(":checked"))
	{
		$("#txtDWTS").attr({'disabled':''});
		$("#txtDWTT").attr({'disabled':'disabled'});
	}
	if($("#rdoDWT2").is(":checked"))
	{
		$("#txtDWTS").attr({'disabled':'disabled'});
		$("#txtDWTT").attr({'disabled':''});
	}
}


function showMMarketField()
{
	if($("#rdoMMarket1").is(":checked"))
	{
		$("#txtMTCPDRate").removeAttr('disabled');
		$("#txtMLumpsum").attr('disabled',true);
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtMTCPDRate").focus();
		getFinalCalculation();
	}
	if($("#rdoMMarket2").is(":checked"))
	{
			
		$("#txtMTCPDRate").attr('disabled',true);
		$("#txtMLumpsum").removeAttr('disabled');
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtMLumpsum").focus();
		getFinalCalculation();
	}
}

function showQtyField()
{

	if($("#rdoQty1").is(":checked"))
	{
		$("#txtDFQMT").removeAttr('disabled');
		$("#txtAddnlQMT").attr('disabled',true);
		$("#txtDFQMT,#txtAddnlQMT").val("");
		$("#txtDFQMT").focus();
	}
	if($("#rdoQty2").is(":checked"))
	{
		$("#txtDFQMT").attr('disabled',true);
		$("#txtAddnlQMT").removeAttr('disabled');
		$("#txtDFQMT,#txtAddnlQMT").val("");
		$("#txtAddnlQMT").focus();
	}
}


function getBunkerCalculation()
{
	    var noOfBunker = $("#txtBunkerRec").val();
		var sum = "";
		for(var i=1;i<=noOfBunker;i++)
		{
		   var bunkergrade = $("#txtBunkerGradeName_"+i).val();
		   var str = bunkergrade.substr(0,3);
		   if(str == 'IFO')
			{
				if($("#txtTFUMTManual").val()>0)
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTFUMTManual").val());
				}
				else
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTFUMT").val());
				}
			}
			else if(str == 'MDO')
			{
				if($("#txtTDUMTManual").val()>0)
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMTManual").val());
				}
				else
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMT").val());
				}
			}
			else if(str == 'MGO')
			{
				if($("#txtTDUMTManual").val()>0)
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMTManual").val());
				}
				else
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMT").val());
				}
			}
			else
			{
				$("#txt"+bunkergrade+"_1_1").val('0.00');
			}
			
			var var3 = $("#txt"+bunkergrade+"_1_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_1_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_1_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_1_3").val(calc.toFixed(2));
			
			var var3 = $("#txt"+bunkergrade+"_8_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_8_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_8_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_8_3").val(calc.toFixed(2));
			
			var var3 = $("#txt"+bunkergrade+"_9_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_9_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_9_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_9_3").val(calc.toFixed(2));
			
			var var3 = $("#txt"+bunkergrade+"_10_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_10_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_10_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_10_3").val(calc.toFixed(2));
			
			var var3 = $("#txt"+bunkergrade+"_11_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_11_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_11_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_11_3").val(calc.toFixed(2));
			
			if($("#txt"+bunkergrade+"_10_3").val() > 0)
			{
			  $("#txtBunkerCost_"+bunkergrade).val($("#txt"+bunkergrade+"_10_3").val());
			}
			else
			{
			  $("#txtBunkerCost_"+bunkergrade).val($("#txt"+bunkergrade+"_1_3").val());
			}
			
		}
	getFinalCalculation();
}


function getORCCalculate(var1)
{
	
	$("#txtHidORCAmt_"+var1).val($("#txtORCAmt_"+var1).val());
	getFinalCalculation();
}


function getDistance(i)
{
	
		$("#txtDistance_"+i).val("");
		$("#ploader_"+i).show();
		var loadporttext = disporttext = ""
		if($("#selFPort_"+i).val()!=""){loadporttext = $("#selFPort_"+i+" option:selected").text();}else{loadporttext = "";}
		if($("#selFPort_"+i).val()!=""){disporttext = $("#selTPort_"+i+" option:selected").text();}else{disporttext = "";}
		$("#spanLoadPort_"+i).html(loadporttext);
		$("#spanDisPort_"+i).html(disporttext);
		$("#TranDisPort_"+i).html(loadporttext);
		$("#spanDDCLPort_"+i).html(loadporttext);
		$("#spanDDCDPort_"+i).html(disporttext);
		if($('#selFPort_'+i).val() != "" && $('#selTPort_'+i).val() != "" && $('#selDType_'+i).val() != "")
		{
			$("#txtDistance_"+i).val(0);
			$.post("options.php?id=10",{selFPort:""+$("#selFPort_"+i).val()+"",selTPort:""+$("#selTPort_"+i).val()+"",selDType:""+$("#selDType_"+i).val()+""}, function(data) 
			{
					$('#txtDistance_'+i).val(data);
					$("#ploader_"+i).hide();
					getVoyageTime();
			});
		}
		else
		{
			$('#txtDistance_'+i).val(0);
			$("#ploader_"+i).hide();
			getVoyageTime();
		}
}





function addPortRotationDetails()
{
	var id = idd = $("#p_rotationID").val();
	if($("#selFPort_"+id).val() != "" && $("#selTPort_"+id).val() != "" && $("#selPType_"+id).val() != "" && $("#txtDistance_"+id).val() != "" && $("#selSSpeed_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="pr_Row_'+id+'"><td align="center" class="input-text" ><a href="#pr'+id+'" id="spcancel_'+id+'" onclick="removePortRotation('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left" class="input-text" ><select name="selFPort_'+id+'" class="select form-control" id="selFPort_'+id+'" onChange="getDistance('+id+');"></select></td><td align="left" class="input-text" ><select  name="selTPort_'+id+'" class="select form-control" id="selTPort_'+id+'" onChange="getDistance('+id+');"></select></td><td align="left" class="input-text" ><select  name="selDType_'+id+'" class="select form-control" id="selDType_'+id+'" onChange="getDistance('+id+');"><?php $obj->getPortDistanceType(); ?></select></td><td align="left" class="input-text" ><select name="selPType_'+id+'" class="select form-control" id="selPType_'+id+'" onChange="getVoyageTime()"; ><?php $obj->getPassageType(); ?></select></td><td align="left" class="input-text" ><input type="text" name="txtDistance_'+id+'" id="txtDistance_'+id+'" class="form-control"  value="" placeholder="distance" onKeyUp="getVoyageTime()"; /><span id="ploader_'+id+'" style="display:none;"><img src="../../img/ajax-loader2.gif" /></span></td><td align="left" class="input-text" ><select  name="selSSpeed_'+id+'" class="select form-control" id="selSSpeed_'+id+'" onChange="getVoyageTime()"; ><?php $obj->getSelectSpeedList(); ?></select></td><td align="left" class="input-text" ><input type="text" name="txtWeather_'+id+'" id="txtWeather_'+id+'" class="form-control" autocomplete="off" placeholder="Speed Adj." onKeyUp="getVoyageTime()"; /></td><td align="left" class="input-text" ><input type="text" name="txtMargin_'+id+'" id="txtMargin_'+id+'" class="form-control" autocomplete="off" placeholder="Margin (%)" onKeyUp="getVoyageTime()"; /><input type="hidden" name="txtVoyageTime_'+id+'" id="txtVoyageTime_'+id+'" class="form-control" autocomplete="off" value="0.00" /><input type="hidden" name="txtTTLVoyageDays_'+id+'" id="txtTTLVoyageDays_'+id+'" class="form-control" autocomplete="off" value="0.00" /><input type="hidden" name="txtTTLFoConsp_'+id+'" id="txtTTLFoConsp_'+id+'" class="form-control" readonly value="0.00"  /><input type="hidden" name="txtTTLDoConsp_'+id+'" id="txtTTLDoConsp_'+id+'" class="form-control" readonly value="0.00"  /></td></tr>').appendTo("#tblPortRotation");
		
		$('<tr id="lp_Row_'+id+'"><td align="center" class="input-text" ><span id="spanLoadPort_'+id+'"></span></td><td align="left" class="input-text" ><input type="text" name="txtPCosts_'+id+'" id="txtPCosts_'+id+'" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)" onKeyUp="getFinalCalculation();"/></td><td align="left" class="input-text" ><input type="text" name="txtQMT_'+id+'" id="txtQMT_'+id+'" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="Qty MT" /></td><td align="left" class="input-text" ><input type="text" name="txtRate_'+id+'" id="txtRate_'+id+'" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="Rate (MT/Day)" /></td><td align="left" class="input-text" ><select  name="selLPTerms_'+id+'" class="select form-control" id="selLPTerms_'+id+'" onChange="getLPRemoveDaysAttr('+id+'),getPortCalculation();"><?php $obj->getLPTermsList(0);?></select></td><td align="left" class="input-text" ><input type="text" name="txtWDays_'+id+'" id="txtWDays_'+id+'" class="form-control" autocomplete="off" readonly value="" onKeyUp= "getVoyageTime();"/></td><td align="left" class="input-text" ><input type="text" name="txtIDays_'+id+'" id="txtIDays_'+id+'" class="form-control" autocomplete="off" value="" placeholder="Idle Days" onKeyUp="getVoyageTime();"/></td><td align="left" class="input-text" ><select  name="selLPVendor_'+id+'" class="select form-control" id="selLPVendor_'+id+'"></select></td><td align="left" class="input-text" ><input name="ChkShowDDCLP_'+id+'" class="checkbox" id="ChkShowDDCLP_'+id+'" type="checkbox" value="1" onClick="ShowDDCDPDP();" /></td></tr>').appendTo("#tblLoadPort");
		$("#selLPVendor_'+id+'").html($("#selVendor").html());
		
		$('<tr id="dp_Row_'+id+'"><td align="center" class="input-text" ><span id="spanDisPort_'+id+'"></span></td><td align="left" class="input-text" ><input type="text" name="txtDCosts_'+id+'" id="txtDCosts_'+id+'" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)" onKeyUp="getFinalCalculation();"/></td><td align="left" class="input-text" ><input type="text" name="txtDQMT_'+id+'" id="txtDQMT_'+id+'" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="Qty MT" /></td><td align="left" class="input-text" ><input type="text" name="txtDRate_'+id+'" id="txtDRate_'+id+'" class="form-control" onKeyUp="getPortCalculation();" autocomplete="off" value="" placeholder="Rate (MT/Day)" /></td><td align="left" class="input-text" ><select  name="selDPTerms_'+id+'" class="select form-control" id="selDPTerms_'+id+'" onChange="getDPRemoveDaysAttr('+id+'),getPortCalculation();"><?php $obj->getLPTermsList(0);?></select></td><td align="left" class="input-text" ><input type="text" name="txtDWDays_'+id+'" id="txtDWDays_'+id+'" class="form-control" autocomplete="off" readonly value="" onKeyUp= "getVoyageTime();"/></td><td align="left" class="input-text" ><input type="text" name="txtDIDays_'+id+'" id="txtDIDays_'+id+'" class="form-control" autocomplete="off" value="" placeholder="Idle Days" onKeyUp="getVoyageTime();"/></td><td align="left" class="input-text" ><select  name="selDPVendor_'+id+'" class="select form-control" id="selDPVendor_'+id+'"></select></td><td align="left" class="input-text" ><input name="ChkShowDDCDP_'+id+'" class="checkbox" id="ChkShowDDCDP_'+id+'" type="checkbox" value="1" onClick="ShowDDCDPDP();" /></td></tr>').appendTo("#tblDisPort");
		$("#selDPVendor_'+id+'").html($("#selVendor").html());
		
		$('<tr id="tp_Row_'+id+'"><td align="center" class="input-text" ><span id="TranDisPort_'+id+'"></span></td><td align="left" class="input-text" ><input type="text" name="txtTLPCosts_'+id+'" id="txtTLPCosts_'+id+'" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)" onKeyUp= "getPortCalculation();"/></td><td align="left" class="input-text" ><input type="text" name="txtTLIDays_'+id+'" id="txtTLIDays_'+id+'" class="form-control" autocomplete="off" value="" placeholder="Idle Days" onKeyUp="getVoyageTime();"/></td></tr>').appendTo("#tblTransitPort");
		
		$('<tr id="DDCLProw_'+id+'"><td>Load Port <span id="spanDDCLPort_'+id+'"></span></td><td><input type="text"  name="txtDDCLPComm_'+id+'" id="txtDDCLPComm_'+id+'" class="form-control" autocomplete="off" value="" onKeyUp="getFinalCalculation();" placeholder="0.00" /></td><td><input type="text"  name="txtEstDDCLPCost_'+id+'" id="txtEstDDCLPCost_'+id+'" class="form-control" value="0" autocomplete="off" onKeyUp="getFinalCalculation();" /><input type="hidden" name="txtLPLayTimeIs_'+id+'" id="txtLPLayTimeIs_'+id+'" value="" /></td><td><input type="text"  name="txtDDCLPCost_'+id+'" id="txtDDCLPCost_'+id+'" class="form-control" value="0" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td><td><select name="selDDCLPVendor_'+id+'" class="select form-control" id="selDDCLPVendor_'+id+'" ></select></td><td><input type="text" name="txtDDCLPNetCostMT_'+id+'" id="txtDDCLPNetCostMT_'+id+'" class="form-control" readonly value="" /><input type="hidden" name="txtDDCLPCostMT_'+id+'" id="txtDDCLPCostMT_'+id+'" class="form-control" readonly value="0" /></td></tr><tr id="DDCDProw_'+id+'"><td>Discharge Port <span id="spanDDCDPort_'+id+'"></span></td><td><input type="text"  name="txtDDCDPComm_'+id+'" id="txtDDCDPComm_'+id+'" class="form-control" autocomplete="off" value="" onKeyUp="getFinalCalculation();" placeholder="0.00" /></td><td><input type="text"  name="txtEstDDCDPCost_'+id+'" id="txtEstDDCDPCost_'+id+'" class="form-control" value="0" autocomplete="off" onKeyUp="getFinalCalculation();" /><input type="hidden" name="txtDPLayTimeIs_'+id+'" id="txtDPLayTimeIs_'+id+'" value="" /></td><td><input type="text"  name="txtDDCDPCost_'+id+'" id="txtDDCDPCost_'+id+'" class="form-control" value="0" onKeyUp="getFinalCalculation();" readonly autocomplete="off"/></td><td><select name="selDDCDPVendor_'+id+'" class="select form-control" id="selDDCDPVendor_'+id+'" ></select></td><td><input type="text" name="txtDDCDPNetCostMT_'+id+'" id="txtDDCDPNetCostMT_'+id+'" class="form-control" readonly value="" /><input type="hidden" name="txtDDCDPCostMT_'+id+'" id="txtDDCDPCostMT_'+id+'" class="form-control" readonly value="0" /></td></tr>').appendTo("#tbDDCBody");
		
		$("input[type='checkbox']").iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
		});
		$("[id^=txtDistance_],[id^=txtWeather_],[id^=txtMargin_],[id^=txtPCosts_],[id^=txtQMT_],[id^=txtRate_],[id^=txtIDays_],[id^=txtDWDays_],[id^=txtWDays_],[id^=txtTLPCosts_],[id^=txtTLIDays_],[id^=txtDDCLPComm_],[id^=txtEstDDCLPCost_],[id^=txtDDCDPComm_],[id^=txtEstDDCDPCost_]").numeric();
		$("#selDDCLPVendor_'+id+'").html($("#selVendor").html());
		$("#selDDCDPVendor_'+id+'").html($("#selVendor").html());
		
		$("#TranDisPort_"+id).html($("#selFPort_"+id+" option:selected").text());
		$("#selFPort_"+id+",#selTPort_"+id).html($("#selPort").html());
		$("#selFPort_"+id).val($("#selTPort_"+idd).val());
		//.........ends...................
		$("#p_rotationID").val(id);
		$("#spanLoadPort_"+id).html($("#selFPort_"+id+" option:selected").text());
		$("#spanDisPort_"+id).html($("#selTPort_"+id+" option:selected").text());
	}
	else
	{
		jAlert('Please fill all the records for Sea Passage', 'Alert');
	}
}

function removePortRotation(var1)
{
	if($("#txtLPLayTimeIs_"+var1).val()!=1 && $("#txtDPLayTimeIs_"+var1).val()!=1)
	{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#pr_Row_"+var1).remove();
					$("#lp_Row_"+var1).remove();
					$("#dp_Row_"+var1).remove();
					$("#tp_Row_"+var1).remove();
					$("#DDCDProw_"+var1).remove();
					$("#DDCLProw_"+var1).remove();
					getPortCalculation();		
				 }
			else{return false;}
			});
	 }
	else
	 {
		jAlert('Sea Passage can not be deleted because laytime already created for Load/Discharge Port.', 'Alert');
	 }
}


function getPortCalculation()
{
	var id = $("#p_rotationID").val();
	
	for(var i=1;i<=id;i++)
	{   $("#txtDWDays_"+i).val(0);$("#txtWDays_"+i).val(0);
		if($("#txtQMT_"+i).val() != "" && $("#txtRate_"+i).val() != "") 
		{
			var value = 0;
			if($("#selLPTerms_"+i).val() == 1)
			{
				value = ($("#txtQMT_"+i).val() / $("#txtRate_"+i).val());
			}
			else if($("#selLPTerms_"+i).val() == 2)
			{
				value = parseFloat($("#txtQMT_"+i).val() / $("#txtRate_"+i).val()) * 1.262 ;
			}
			else if($("#selLPTerms_"+i).val() == 3)
			{
				value = parseFloat($("#txtQMT_"+i).val() / $("#txtRate_"+i).val()) * 1.405 ;
			}
			else
			{
				value = parseFloat($("#txtQMT_"+i).val() / $("#txtRate_"+i).val());
			}
			
			if($("#selLPTerms_"+i).val() != 4)
			{
				if(isNaN(value) || value=="" || value=="Infinity")
				{
					value =0;
				}
				$("#txtWDays_"+i).val(value.toFixed(2));
			}
		}
		else
		{
			$("#txtWDays_"+i).val('0.00');
		}
		
		if($("#txtDQMT_"+i).val() != "" && $("#txtDRate_"+i).val() != "") 
		{
			var value = 0;
			if($("#selDPTerms_"+i).val() == 1)
			{
				value = ($("#txtDQMT_"+i).val() / $("#txtDRate_"+i).val());
			}
			else if($("#selDPTerms_"+i).val() == 2)
			{
				value = parseFloat($("#txtDQMT_"+i).val() / $("#txtDRate_"+i).val()) * 1.262 ;
			}
			else if($("#selDPTerms_"+i).val() == 3)
			{
				value = parseFloat($("#txtDQMT_"+i).val() / $("#txtDRate_"+i).val()) * 1.405 ;
			}
			else
			{
				value = parseFloat($("#txtDQMT_"+i).val() / $("#txtDRate_"+i).val());
			}
			
			if($("#selDPTerms_"+i).val() != 4)
			{
				if(isNaN(value) || value=="" || value=="Infinity")
				{
					value =0;
				}
				$("#txtDWDays_"+i).val(value.toFixed(2));
			}
		}
		else
		{
			$("#txtDWDays"+i+",#txtDWDays1_"+i).val('0.00');
		}
	}
	getVoyageTime();
}


function getLPRemoveDaysAttr(i)
{
	if($("#selLPTerms_"+i).val() == 4)
	{
		$("#txtWDays_"+i).removeAttr('readonly');
		$("#txtWDays_"+i).val(0);
	}
	else
	{
		$("#txtWDays_"+i).attr('readonly',true);
	}
}

function getDPRemoveDaysAttr(i)
{
	if($("#selDPTerms_"+i).val() == 4)
	{
		$("#txtDWDays_"+i).removeAttr('readonly');
		$("#txtDWDays_"+i).val(0);
	}
	else
	{
		$("#txtDWDays_"+i).attr('readonly',true);
	}	
}


function getFinalCalculation()
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	
	if(parseFloat(cargo_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	
	$('[id^=txtCCAbs_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				
				if(this.value == ""){var cc_abs = 0;}else{var cc_abs = this.value;}
				if($("#txtCCAddComm_"+lasrvar1).val() == ""){var cc_addcomm = 0;}else{var cc_addcomm = $("#txtCCAddComm_"+lasrvar1).val();}
				var cc_value = "";
				cc_value = parseFloat(cc_abs) - parseFloat((parseFloat(cc_abs) * parseFloat(cc_addcomm))/100);
				$("#txtCCValue_"+lasrvar1).val(cc_value.toFixed(2));
				
				var costMT = this.value / parseFloat(per_mt);				
				$("#txtCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
	
	$('[id^=txtOMCAbs_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				
				if(this.value == ""){var omc_abs = 0;}else{var omc_abs = this.value;}
				if($("#txtOMCAddComm_"+lasrvar1).val() == ""){var omc_addcomm = 0;}else{var omc_addcomm = $("#txtOMCAddComm_"+lasrvar1).val();}
				var omc_value = "";
				omc_value = parseFloat(omc_abs) - parseFloat((parseFloat(omc_abs) * parseFloat(omc_addcomm))/100);
				$("#txtOMCValue_"+lasrvar1).val(omc_value.toFixed(2));
				
				var costMT = this.value / parseFloat(per_mt);				
				$("#txtOMCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
			
	if($("#txtddswCost").val() == ""){var ddsw_cost = 0;}else{var ddsw_cost = $("#txtddswCost").val();}
	if($("#txtddswAddComm").val() == ""){var ddsw_addcomm = 0;}else{var ddsw_addcomm = $("#txtddswAddComm").val();}
	var ddsw_value = parseFloat(ddsw_cost) - parseFloat((parseFloat(ddsw_cost) * parseFloat(ddsw_addcomm))/100);
	$("#txtddswValue").val(ddsw_value.toFixed(2));
	
	
	if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
	if($("#txtWSFR").val() == ""){var ws_flat_rate = 0;}else{var ws_flat_rate = $("#txtWSFR").val();}
	if($("#txtWSFD").val() == ""){var fixed_diff = 0;}else{var fixed_diff = $("#txtWSFD").val();}
	if($("#txtMTCPDRate").val() == ""){var ws_rate = 0;}else{var ws_rate = $("#txtMTCPDRate").val();}
	if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
	
	//$("[id^=txtMinCargo_]").val(min_qty);$("[id^=txtOverage_]").val(addnl_qty);
	//$("[id^=txtMCWSFlatRate_],[id^=txtOvrWSFlatRate_]").val(ws_flat_rate);
	
	//$("[id^=txtTotalCargoQty_]").val(cargo_qty);
	
	<!----------------- Quantity  ------------------->
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var per_amt = 0;}else{var per_amt = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var per_amt = 0;}else{var per_amt = $("#txtTotalFreightQty").val();}}
	
	if($("#rdoMMarket1").is(":checked"))
	{
		<!----------------- Agreed Gross Freight  ------------------->
		if($("#txtMTCPDRate").val() == ""){var agr_gross_fr = 0;}else{var agr_gross_fr = $("#txtMTCPDRate").val();}
		
		
		<!----------------- (Freight Adjustment) Gross Freight  ------------------->
		var gross_fr = parseFloat(agr_gross_fr) * parseFloat(per_amt);
		$("#txtFrAdjUsdGF").val(gross_fr.toFixed(2));
		
		<!----------------- (Freight Adjustment) Gross Freight(MT)  ------------------->
		var gross_fr_mt = parseFloat(gross_fr) / parseFloat(per_amt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		
		<!-----------------  DF Qty (MT)  ------------------->
		if($("#txtDFQMT").val() == "" || $("#txtDFQMT").val() == 0)
		{
			var df_qty = 0;
			$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
			
		}
		else
		{
			<!-----------------  Dead Freight(USD)  ------------------->
			var df_qty = $("#txtDFQMT").val();
			var dead_fr = parseFloat(agr_gross_fr) * parseFloat(df_qty);
			$("#txtFrAdjUsdDF").val(dead_fr.toFixed(2));
			
			<!-----------------  Dead Freight(MT)  ------------------->
			var dead_fr_mt = parseFloat(dead_fr) / parseFloat(df_qty);
			$("#txtFrAdjUsdDFMT").val(dead_fr_mt.toFixed(2));
		}
		
		<!-----------------  Addnl Cargo Rate (USD/MT)  ------------------->
		
		if($("#txtAddnlCRate").val() == ""){var addnl_rate = 0;}else{var addnl_rate = $("#txtAddnlCRate").val();}
		//if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
		
		
		if($("#txtAddnlQMT").val() == "" || $("#txtAddnlQMT").val() == 0 )
		{
			var addnl_qty = 0;
			$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		}
		else
		{
			<!-----------------  Addnl Qty (MT)  ------------------->
			var addnl_qty = $("#txtAddnlQMT").val();
			
			<!-----------------(Freight Adjustment)  Addnl Freight(USD)  ------------------->
			var addnl_fr = parseFloat(addnl_rate) * parseFloat(addnl_qty);
			$("#txtFrAdjUsdAF").val(addnl_fr.toFixed(2));
			
			<!-----------------(Freight Adjustment)  Addnl Freight(MT)  ------------------->
			var addnl_fr_mt = parseFloat(addnl_fr) / parseFloat(addnl_qty);
			$("#txtFrAdjUsdAFMT").val(addnl_fr_mt.toFixed(2));
		}
		
		<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum().toFixed(2));
		
		<!-----------------(Freight Adjustment)  Total Freight(MT)  ------------------->
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / (parseFloat(per_amt) + parseFloat(df_qty) + parseFloat(addnl_qty));
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
	}
	
	if($("#rdoMMarket2").is(":checked"))
	{
		<!-----------------(Freight Adjustment)  Gross Freight(USD)  ------------------->
		$("#txtFrAdjUsdGF").val($("#txtMLumpsum").val());
		
		<!-----------------(Freight Adjustment)  Gross Freight(MT)  ------------------->
		var gross_fr_mt = parseFloat($("#txtFrAdjUsdGF").val()) / parseFloat(per_amt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		
		$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
		$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		
		<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum());
		
		<!-----------------(Freight Adjustment)  Gross Freight(MT)  ------------------->
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / parseFloat(per_amt);
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
	}
	
	<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtFrAdjUsdTF").val() == ""){var ttl_fr = 0;}else{var ttl_fr = $("#txtFrAdjUsdTF").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{var ttl_fr = parseFloat($("[id^=txtFreightQty1_]").sum()); if(ttl_fr == ""){var ttl_fr = 0;} }
	
	//if($("#txtFrAdjUsdTF").val() == ""){var ttl_fr = 0;}else{var ttl_fr = $("#txtFrAdjUsdTF").val();}
	
	<!-----------------(Freight Adjustment) Address Commission(Percent)  ------------------->
	if($("#txtFrAdjPerAC").val() == ""){var ac_per = 0;}else{var ac_per = $("#txtFrAdjPerAC").val();}
	
	<!-----------------(Freight Adjustment) Address Commission(USD)  ------------------->
	var address_commission = (parseFloat(ttl_fr) * parseFloat(ac_per)) / 100;
	$("#txtFrAdjUsdAC").val(address_commission.toFixed(2));
	
	<!-----------------(Freight Adjustment) Address Commission(MT)  ------------------->
	var address_commission_mt = parseFloat($("#txtFrAdjUsdAC").val()) / parseFloat(per_amt);
	$("#txtFrAdjUsdACMT").val(address_commission_mt.toFixed(2));
	
	<!-----------------(Freight Adjustment) Brokerage(Persent)  ------------------->
	if($("#txtFrAdjPerAgC").val() == ""){var ag_per = 0;}else{var ag_per = $("#txtFrAdjPerAgC").val();}
	var agent_commission = (parseFloat(ttl_fr) * parseFloat(ag_per)) / 100;
	
	<!-----------------(Freight Adjustment) Brokerage(USD)  ------------------->
	$("#txtFrAdjUsdAgC").val(agent_commission.toFixed(2));
	
	<!-----------------(Freight Adjustment) Net Freight Payable(USD)  ------------------->
	var net_fr = parseFloat(ttl_fr) - parseFloat($("#txtFrAdjUsdAgC").sum());
	$("#txtFrAdjUsdFP,#txtFGFFD").val(net_fr.toFixed(2));
	
	<!-----------------(Freight Adjustment) Final Net Freight Payable(MT)  ------------------->
	var net_fr_mt = parseFloat(net_fr) / parseFloat(per_amt);
	$("#txtFGFFDMT").val(net_fr_mt.toFixed(2));
	
	<!----------------- Revenue (Final Nett Freight) ------------------->
	if($("#txtFGFFD").val() == ""){var final_nett_frt = 0;}else{var final_nett_frt = $("#txtFGFFD").val();}
	var rev = parseFloat($("[id^=txtOMCAbs_]").sum().toFixed(2)) + parseFloat(final_nett_frt);
	$("#txtRevenue").val(rev.toFixed(2));
	
	
	
	for(var l = 1;l<=$("#txtBRokageCount").val();l++)
	{
	if($("#txtBrCommPercent_"+l).val() == ""){var brokerage_comm = 0;}else{var brokerage_comm = $("#txtBrCommPercent_"+l).val();}
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtFrAdjUsdTF").val() == ""){var t_frt = 0;}else{var t_frt = $("#txtFrAdjUsdTF").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{var t_frt = parseFloat($("[id^=txtFreightQty1_]").sum()); if(t_frt == ""){var t_frt = 0;} }
	//if($("#txtFrAdjUsdTF").val() == ""){var t_frt = 0;}else{var t_frt = $("#txtFrAdjUsdTF").val();}
	
	var brokerage_comm_usd = (parseFloat(t_frt) * parseFloat(brokerage_comm))/100;
	$("#txtBrComm_"+l).val(brokerage_comm_usd.toFixed(2));
	}
	$("#txtBrCommPercent").val($("[id^=txtBrCommPercent_]").sum().toFixed(2));
	$("#txtBrComm").val($("[id^=txtBrComm_]").sum().toFixed(2));
	
	if($("#txtCVE").val() == ""){var txtCVE = 0;}else{var txtCVE = $("#txtCVE").val();}
	if($("#txtTDays").val() == ""){var txtTDays = 0;}else{var txtTDays = $("#txtTDays").val();}
	var calmul = parseFloat(parseFloat(parseFloat(txtCVE)/30)*parseFloat(txtTDays));
	if(isNaN(calmul) || calmul=="" || calmul=="Infinity"){calmul = 0;}
	$("#txtCVEAmt").val(calmul.toFixed(2));
	$("#txtHidCVEAmt").val(calmul.toFixed(2));
	
	$("#txtTTLORCAmt").val($("[id^=txtHidORCAmt_],#txtBrComm,#txtHidCVEAmt,#txtFrAdjUsdAC").sum());
	var calc = parseFloat($("#txtTTLORCAmt").val()) / parseFloat(per_mt);
	$("#txtTTLORCCostMT").val(calc.toFixed(2));
	
	$("#txtTTLOwnersExpenses").val($("[id^=txtBunkerCost_],#txtTTLPortCosts,#txtTTLORCAmt,[id^=txtDCosts_],[id^=txtPCosts_],[id^=txtTLPCosts_]").sum().toFixed(2));
	$("#txtTTLCharterersExpenses").val($("[id^=txtCCAbs_]").sum().toFixed(2));
	
	if($("#txtRevenue").val() == ""){var revenue = 0;}else{var revenue = $("#txtRevenue").val();}
	if($("#txtTTLOwnersExpenses").val() == ""){var owners_expenses = 0;}else{var owners_expenses = $("#txtTTLOwnersExpenses").val();}
	
	var costbeforebamarage = parseFloat(revenue) - parseFloat(owners_expenses);
	var voyage_earning = parseFloat(revenue) - parseFloat(owners_expenses) + parseFloat(getDDCOwnerCalculation());
	
	$("#txtVoyageEarnings").val(costbeforebamarage.toFixed(2));
	$("#txtGTTLVoyageEarnings").val(voyage_earning.toFixed(2));
	
	if($("#txtTDays").val() == 0){var ttl_days = 1;}else{var ttl_days = $("#txtTDays").val();}
	var daily_earnings = parseFloat(voyage_earning) / parseFloat(ttl_days);
	var costbeforebamarage_erning = parseFloat(costbeforebamarage) / parseFloat(ttl_days);
	$("#txtDailyEarnings").val(costbeforebamarage_erning.toFixed(2));
	$("#txtNettDailyEarnings").val(daily_earnings.toFixed(2));
	
	
	if($("#txtDailyVesselOperatingExpenses").val() == ""){var daily_vessel_exp = 0;}else{var daily_vessel_exp = $("#txtDailyVesselOperatingExpenses").val();}
	
	var hireage_amt = parseFloat(parseFloat(daily_vessel_exp)*parseFloat($("#txtTDays").val())).toFixed(2);
	if(hireage_amt=="" || isNaN(hireage_amt)){hireage_amt =0;}
	$("#txtHireargeAmt").val(parseFloat(hireage_amt).toFixed(2));
	if($("#txtHireargePercent").val() == ""){var txtHireargePercent = 0;}else{var txtHireargePercent = $("#txtHireargePercent").val();}
	var txtHireargePercentAmt = parseFloat(parseFloat(parseFloat(hireage_amt)*parseFloat(txtHireargePercent))/100);
	
	if(txtHireargePercentAmt=="" || isNaN(txtHireargePercentAmt)){txtHireargePercentAmt =0;}
	$("#txtHireargePercentAmt").val(parseFloat(txtHireargePercentAmt).toFixed(2));
	
	var txtNettHireargeAmt = parseFloat(parseFloat(hireage_amt) -  parseFloat(txtHireargePercentAmt));
	$("#txtNettHireargeAmt").val(parseFloat(txtNettHireargeAmt).toFixed(2));	
	
	var nett_daily_profit = parseFloat(parseFloat(parseFloat($("#txtGTTLVoyageEarnings").val()) - parseFloat(txtNettHireargeAmt))/parseFloat($("#txtTDays").val()));
	if(nett_daily_profit=="" || isNaN(nett_daily_profit)){nett_daily_profit =0;}
	$("#txtNettDailyProfit").val(parseFloat(nett_daily_profit).toFixed(2));
	$("#txtPL").val( parseFloat(parseFloat($("#txtNettDailyProfit").val()) *  parseFloat($("#txtTDays").val())).toFixed(2));
	
	
	$('[id^=txtBunkerCost_]').each(function(index) {
		var rowid = this.id;
		var lasrvar1 = rowid.split('_').slice(1).join('_');
		var costMT = this.value / parseFloat(per_mt);
		$("#txtBunkerCostMT_"+lasrvar1).val(costMT.toFixed(2));
	});
	$('[id^=txtLPOSCCost_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				var costMT = this.value / parseFloat(per_mt);
				$("#txtLPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
	$('[id^=txtDPOSCCost_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				var costMT = this.value / parseFloat(per_mt);
				$("#txtDPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
	$('[id^=txtTPOSCCost_]').each(function(index) {
				var rowid = this.id;
				var lasrvar1 = rowid.split('_').slice(1).join('_');
				var costMT = this.value / parseFloat(per_mt);
				$("#txtTPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
			});
}


function getOMCCalculate(var1)
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	if(parseFloat(cargo_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	
	var txtOMICount = $("#txtOMICount").val();
	for(var var1=1; var1<=txtOMICount; var1++)
	{
		if($("#txtOMCAmount_"+var1).val() == ""){var cc_abs = 0;}else{var cc_abs = $("#txtOMCAmount_"+var1).val();}
		if($("#txtOMCAddComm_"+var1).val() == ""){var cc_addcomm = 0;}else{var cc_addcomm = $("#txtOMCAddComm_"+var1).val();}
		var cc_value = parseFloat(cc_abs) - parseFloat((parseFloat(cc_abs) * parseFloat(cc_addcomm))/100);
		$("#txtOMCAbs_"+var1).val(cc_value.toFixed(2));
		if($("#txtOMCAbs_"+var1).val() == ""){var cc_abs = 0;}else{var cc_abs = $("#txtOMCAbs_"+var1).val();}
		var calc = parseFloat(cc_abs) / parseFloat(per_mt);
		$("#txtOMCCostMT_"+var1).val(calc.toFixed(2));
	}
	
	if($("#txtOMCAbs_"+var1).val() == ""){var omc_abs = 0;}else{var omc_abs = $("#txtOMCAbs_"+var1).val();}
	var calc = parseFloat(omc_abs) / parseFloat(per_mt);
	$("#txtOMCCostMT_"+var1).val(calc.toFixed(2));
	getFinalCalculation();	
}


function addBrokerageRow()
{
	var id = $("#txtBRokageCount").val();
	if($("#txtBrCommPercent_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="tbrRow_<?php echo $i;?>"><td><a href="#tb'+id+'" onClick="removeBrokerage('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td><td><input type="text" name="txtBrCommPercent_'+id+'" id="txtBrCommPercent_'+id+'" class="form-control" autocomplete="off" value="0" onKeyUp="getFinalCalculation();"  /></td><td><input type="text" name="txtBrComm_'+id+'" id="txtBrComm_'+id+'" class="form-control" readonly value="0"/></td><td><select  name="selBrCommVendor_'+id+'" class="select form-control" id="selBrCommVendor_'+id+'"></select></td></tr>').appendTo("#tbodyBrokerage");
		$("[id^=txtBrCommPercent_]").numeric();	
		$("#selBrCommVendor_"+id).html($("#selVendor").html());
		$("#txtBRokageCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeBrokerage(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#tbrRow_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}

function getTotalDWT()
{
	if($("#rdoDWT1").is(":checked")){var dwt = $("#txtDWTS").val();}
	if($("#rdoDWT2").is(":checked")){var dwt = $("#txtDWTT").val();}
	
	if($("#rdoCap1").is(":checked"))
	{
		if($("#txtGCap").val() == ""){var gcap = 0;}else{var gcap = $("#txtGCap").val();}
		if($("#txtSF").val() != "" || $("#txtSF").val() != 0)
		{
			if(isNaN(txtSF) || txtSF=="" || txtSF=="Infinity")
			{txtSF = 0;}
			if(isNaN(gcap) || gcap=="" || gcap=="Infinity")
			{gcap = 0;}
			if(isNaN(dwt) || dwt=="" || dwt=="Infinity")
			{dwt = 0;}
			var ttl = parseFloat(gcap) / parseFloat(txtSF);
			if(ttl > parseFloat(dwt))
			{
				$("#txtLoadable").val(dwt);
			}
			else
			{
				if(isNaN(ttl) || ttl=="" || ttl=="Infinity")
			    {ttl = 0;}
				$("#txtLoadable").val(ttl.toFixed(2));
			}
		}
	}
	
}

function showCapField()
{
	if($("#rdoCap1").is(":checked"))
	{
		$("#txtGCap").removeAttr('disabled');
		$("#txtBCap").attr('disabled','disabled');
		getTotalDWT();
		getTotalDWT1();
	}
	if($("#rdoCap2").is(":checked"))
	{
		$("#txtGCap").attr('disabled','disabled');
		$("#txtBCap").removeAttr('disabled');		
		getTotalDWT();
		getTotalDWT1();
	}
}

function getTotalDWT1()
{
	if($("#rdoDWT1").is(":checked")){var dwt = $("#txtDWTS").val();}
	if($("#rdoDWT2").is(":checked")){var dwt = $("#txtDWTT").val();}
	
	if($("#rdoCap2").is(":checked"))
	{
		if($("#txtBCap").val() == ""){var bcap = 0;}else{var bcap = $("#txtBCap").val();}
		
		if($("#txtSF").val() != "" || $("#txtSF").val() != 0)
		{
			var txtSF = $("#txtSF").val();
			if(isNaN(txtSF) || txtSF=="" || txtSF=="Infinity")
			{txtSF = 0;}
			if(isNaN(bcap) || bcap=="" || bcap=="Infinity")
			{bcap = 0;}
			if(isNaN(dwt) || dwt=="" || dwt=="Infinity")
			{dwt = 0;}
			var ttl = parseFloat(bcap) / parseFloat(txtSF);
			if(ttl > parseFloat(dwt))
			{
				$("#txtLoadable").val(dwt);
			}
			else
			{
				if(isNaN(ttl) || ttl=="" || ttl=="Infinity")
			    {ttl = 0;}
				$("#txtLoadable").val(ttl.toFixed(2));
			}
		}
	}
	getFinalCalculation();
}

function getCharterersCostCalculate(var1)
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	if(parseFloat(cargo_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	var txtCCCount = $("#txtCCCount").val();
	for(var var1=1; var1<=txtCCCount; var1++)
	{
		if($("#txtCCAmount_"+var1).val() == ""){var cc_abs = 0;}else{var cc_abs = $("#txtCCAmount_"+var1).val();}
		if($("#txtCCAddComm_"+var1).val() == ""){var cc_addcomm = 0;}else{var cc_addcomm = $("#txtCCAddComm_"+var1).val();}
		var cc_value = parseFloat(cc_abs) - parseFloat((parseFloat(cc_abs) * parseFloat(cc_addcomm))/100);
		$("#txtCCAbs_"+var1).val(cc_value.toFixed(2));
		if($("#txtCCAbs_"+var1).val() == ""){var cc_abs = 0;}else{var cc_abs = $("#txtCCAbs_"+var1).val();}
		var calc = parseFloat(cc_abs) / parseFloat(per_mt);
		$("#txtCCostMT_"+var1).val(calc.toFixed(2));
	}
	getFinalCalculation();	
}

function getValidate(val4)
{
	$("#update_status").val(val4);
}



function addOWCRow()
{
	var id = $("#txtORCCount").val();
	if($("#txtORCAmt_"+id).val() != "" && $("#txtHidORCID_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="OWCRow_'+id+'"><td><a href="#tb1" onClick="removeOWC('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td></td><td><input type="text" name="txtHidORCID_'+id+'" id="txtHidORCID_'+id+'" class="form-control" placeholder="Owner Related Costs" autocomplete="off" value="" /></td><td><input type="text" name="txtORCAmt_'+id+'" id="txtORCAmt_'+id+'" onKeyUp="getORCCalculate('+id+');" class="form-control" autocomplete="off" value="" placeholder="0.00"  /></td><td><select  name="selORCVendor_'+id+'" class="select form-control" id="selORCVendor_'+id+'"></select><input type="hidden" name="txtHidORCAmt_'+id+'" id="txtHidORCAmt_'+id+'" autocomplete="off" value="0.00" class="form-control" readonly /></td></tr>').appendTo("#OWCBody");
		$("[id^=txtORCAmt_]").numeric();	
		$("#selORCVendor_"+id).html($("#selVendor").html());
		$("#txtORCCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeOWC(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#OWCRow_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}


function addCCRow()
{
	var id = $("#txtCCCount").val();
	if($("#txtHidCCID_"+id).val() != "" && $("#txtCCAbs_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="CCRow_'+id+'"><td><a href="#tb'+id+'" onClick="removeCC('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><input type="text" name="txtHidCCID_'+id+'" id="txtHidCCID_'+id+'" class="form-control" placeholder="Charterers Costs" autocomplete="off" value="" /></td><td><input type="text" name="txtCCAddComm_'+id+'" id="txtCCAddComm_'+id+'" onKeyUp="getCharterersCostCalculate();" class="form-control" autocomplete="off" value="" placeholder="0.00"  /></td><td><input type="text" name="txtCCAmount_'+id+'" id="txtCCAmount_'+id+'" onKeyUp="getCharterersCostCalculate();" class="form-control" autocomplete="off" value="" placeholder="0.00"  /></td><td><select name="selCCVendor_'+id+'" class="select form-control" id="selCCVendor_'+id+'" ></select></td><td><input type="text" name="txtCCAbs_'+id+'" id="txtCCAbs_'+id+'" class="form-control" autocomplete="off" value="" placeholder="0.00"  /><input type="hidden" name="txtCCostMT_'+id+'" id="txtCCostMT_'+id+'" class="form-control" readonly placeholder="0.00" /></td></tr>').appendTo("#CCBody");
		$("[id^=txtCCAbs_],[id^=txtCCAddComm_],[id^=txtCCAmount_]").numeric();	
		$("#txtCCCount").val(id); 
		$("#selCCVendor_"+id).html($("#selVendor").html());
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeCC(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#CCRow_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}

function addOMIRow()
{
	var id = $("#txtOMICount").val();
	if($("#txtHidOMCID_"+id).val() != "" && $("#txtOMCAbs_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="OMIRow_'+id+'"><td><a href="#OMI'+id+'" onClick="removeOMI('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><input type="text" name="txtHidOMCID_'+id+'" id="txtHidOMCID_'+id+'" class="form-control" placeholder="Other Misc. Income" autocomplete="off" value="" /></td><td><input type="text" name="txtOMCAddComm_'+id+'" id="txtOMCAddComm_'+id+'" onKeyUp="getOMCCalculate();" class="form-control" autocomplete="off" value="" placeholder="0.00" /></td><td><input type="text" name="txtOMCAmount_'+id+'" id="txtOMCAmount_'+id+'" onKeyUp="getOMCCalculate();" class="form-control" autocomplete="off" value="" placeholder="0.00" readonly/></td><td><select name="selOMCVendor_'+id+'" class="select form-control" id="selOMCVendor_'+id+'" ></select></td><td><input type="text" name="txtOMCAbs_'+id+'" id="txtOMCAbs_'+id+'" class="form-control" autocomplete="off" value="" placeholder="0.00"  /><input type="hidden" name="txtOMCCostMT_'+id+'" id="txtOMCCostMT_'+id+'" class="form-control" readonly placeholder="0.00" /></td></tr>').appendTo("#OMIBody");
         $("#selOMCVendor_"+id).html($("#selVendor").html());
		$("[id^=txtOMCAbs_],[id^=txtOMCAddComm_],[id^=txtOMCAmount_]").numeric();	
		$("#txtOMICount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeOMI(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#OMIRow_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}



function getZoneData()
{
	$("#selZone").val("");
	$.post("options.php?id=14",{selPort:""+$("#selOpenPort").val()+""}, function(html) {
	$("#selZone").val(html);
	});
}

function getShow()
{
	if($("#selCOASpot").val() == 1 || $("#selCOASpot").val() == 3 || $("#selCOASpot").val() == 4)
	{
		$("#tr_coa,#tr_coa1").hide();
		$("#selOwner,#selCOA,#txtNoLift,#selBroker,#txtCPDate").val("");
		$("#ttl_shipment").html("");
	}
	else if($("#selCOASpot").val() == 2)
	{
		$("#selOwner,#selCOA,#txtNoLift,#selBroker,#txtCPDate").val("");
		$("#ttl_shipment").html("");
		$("#tr_coa,#tr_coa1").show();
	}
}


function getTotalShipments()
{
	if($("#selCOA").val() != "")
	{
		$("#txtNoLift").val("");
		var coa_list = <?php echo $obj->getCOAMasterDataJson();?>;
		$.each(coa_list[$("#selCOA").val()], function(index, array) {
				$("#ttl_shipment").html("");
				$("#selBroker,#selOwner,#txtCPDate").val("");
				$("#ttl_shipment").html(array['NO_OF_SHIPMENT']);
				$("#selBroker").val(array['BROKER']);
				$("#selOwner").val(array['OWNER']);
				$("#txtCPDate").val(array['COA_DATE']);
			});
	}
	else
	{
		$("#txtNoLift").val("");
		$("#ttl_shipment").html("");
		$("#selBroker,#selOwner,#txtCPDate").val("");
	}
}


function addUI_Row()
{
	var id = $("#txtTID").val();
	
	if(id == 0)
	{
		if($("#txtDisponentOwner").val() != "")
		{
			id  = (id - 1 )+ 2;
			num = parseFloat(id +1);
			//alert(id);
			$('<div class="col-sm-4 invoice-col"> Disponent Owner '+num+' <address><select  name="txtDisponentOwner_'+id+'" class="form-control" id="txtDisponentOwner_'+id+'"><?php $obj->getVendorListNewForCOA(11); ?></select></address></div>').appendTo("#sort");
			$("#txtTID").val(id);
		}
		else
		{
			jAlert('Please fill the above entries.', 'Alert');
		}
	}
	else
	{
		if($("#txtDisponentOwner_"+id).val() != "")
		{
			id  = (id - 1 )+ 2;
			num = parseFloat(id +1);
			$('<div class="col-sm-4 invoice-col"> Disponent Owner '+num+' <address><select  name="txtDisponentOwner_'+id+'" class="form-control" id="txtDisponentOwner_'+id+'"><?php $obj->getVendorListNewForCOA(11); ?></select></address></div>').appendTo("#sort");
			$("#txtTID").val(id);
		}
		else
		{
			jAlert('Please fill the above entries.', 'Alert');
		}
	}	
}


function getMaterialCode()
{
	if($("#selMType").val() != "")
	{
		$("#txtMCode").val($("#selMType").val().split("@@@")[1]);
	}
	else
	{
		$("#txtMCode").val("");
	}
}

function Del_Upload(var2)
{
	jConfirm('Are you sure you want to delete this upload permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file_'+var2).remove();
	}
	else{return false;}
	});
}


function addPortRotationLocationDetails()
{
	var id = $("#p_locationID").val();
	if($("#txtLocationFrom_"+id).val() != "" && $("#txtLocationTo_"+id).val() != "" && $("#selPLocationType_"+id).val() != "" && $("#txtLocationDistance_"+id).val() != "" && $("#selSLocationSpeed_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="prl_Row_<?php echo $i;?>"><td><a href="#pr'+id+'" onClick="removePortLocation('+id+');" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td><td><input type="text" name="txtFromLocation_'+id+'" id="txtFromLocation_'+id+'" class="form-control" value="" autocomplete="off" placeholder="Location From"/></td><td><input type="text" class="form-control" name="txtToLocation_'+id+'" id="txtToLocation_'+id+'" value="" autocomplete="off" placeholder="Location To"/></td><td><select  name="selPLocationType_'+id+'" class="select form-control" id="selPLocationType_'+id+'" onChange="getVoyageTime();"><?php $obj->getPassageType(); ?></select></td><td><select  name="selSLocationSpeed_'+id+'" class="select form-control" id="selSLocationSpeed_'+id+'" onChange="getVoyageTime();"><?php $obj->getSelectSpeedList(); ?></select></td><td><input type="text" name="txtLocationDistance_'+id+'" id="txtLocationDistance_'+id+'" class="form-control" value="" placeholder="distance" onKeyUp="getVoyageTime();"/></td></tr>').appendTo("#tblPortLotation");
		
		$("#p_locationID").val(id);
		$("#txtLocationDistance_"+id).numeric();
		getVoyageTime();
	}
	else
	{
		jAlert('Please fill all the records for Location', 'Alert');
	}
}


function removePortLocation(val)
{
  jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
	if(r){	
		   $("#prl_Row_"+val).remove();
			getVoyageTime();
		}
	});
       
}


function getVoyageTime()
{
	var id = $("#p_rotationID").val();
	$("#txtBDays,#txtLDays").val(0);$("#txtLDist").val(0);
	$("#txtTtPIDays").val(0);$("#txtTtPWDays").val(0);
	$("#txtTDist").val(0);$("#txtTDays").val(0);
	$("#txtBDist").val(0);$("#txtTDUMT").val(0);$("#txtTFUMT").val(0);
	var ladendays = ballastdays = idealdays = workingdays = consumptiondo = consumptionfo =  0;
	for(var i=1;i<=id;i++)
	{
	    if($("#txtMargin_"+i).val() == ""){var margin = 0;}else{var margin = $("#txtMargin_"+i).val();}
		if($("#txtDistance_"+i).val() == ""){var distance = 0;}else{var distance = $("#txtDistance_"+i).val();}
		if($("#txtWeather_"+i).val() == ""){var speed_adj = 0;}else{var speed_adj = $("#txtWeather_"+i).val();}
		if($("#selPType_"+i).val() == 1)
		{
			if($("#selSSpeed_"+i).val() == 1)
			{
				var ballast = $("#txtBFullSpeed").val();
				var fo_ballast = $("#txtBFOFullSpeed").val();
				var do_ballast = $("#txtBDOFullSpeed").val();
			}
			if($("#selSSpeed_"+i).val() == 2)
			{
				var ballast = $("#txtBEcoSpeed1").val();
				var fo_ballast = $("#txtBFOEcoSpeed1").val();
				var do_ballast = $("#txtBDOEcoSpeed1").val();
			}
			if($("#selSSpeed_"+i).val() == 3)
			{
				var ballast = $("#txtBEcoSpeed2").val();
				var fo_ballast = $("#txtBFOEcoSpeed2").val();
				var do_ballast = $("#txtBDOEcoSpeed2").val();
			}
			var spd = ($("#txtDistance_"+i).val() /  (parseFloat(ballast) + parseFloat(speed_adj)));
			var calc = spd/24;
			var ttl_days = parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100);
			ballastdays = ballastdays + parseFloat(ttl_days.toFixed(2));
		
			var consp_fo = parseFloat((parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(fo_ballast));
			var consp_do = parseFloat((parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(do_ballast));	
			if(isNaN(consp_do) || consp_do=="" || consp_do=="Infinity")
	        {consp_do = 0;	}
			if(isNaN(consp_fo) || consp_fo=="" || consp_fo=="Infinity")
	        {consp_fo = 0;	}
			consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
			consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
			
			var ttl_dis = parseFloat(distance) + parseFloat($("#txtBDist").val());
			$("#txtBDist").val(ttl_dis.toFixed(2));
			$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
		}
		if($("#selPType_"+i).val() == 2)
		{
			if($("#selSSpeed_"+i).val() == 1)
			{
				var ladan = $("#txtLFullSpeed").val();
				var fo_ladan = $("#txtLFOFullSpeed").val();
				var do_ladan = $("#txtLDOFullSpeed").val();
			}
			if($("#selSSpeed_"+i).val() == 2)
			{
				var ladan = $("#txtLEcoSpeed1").val();
				var fo_ladan = $("#txtLFOEcoSpeed1").val();
				var do_ladan = $("#txtLDOEcoSpeed1").val();
			}
			if($("#selSSpeed_"+i).val() == 3)
			{
				var ladan = $("#txtLEcoSpeed2").val();
				var fo_ladan = $("#txtLFOEcoSpeed2").val();
				var do_ladan = $("#txtLDOEcoSpeed2").val();
			}
			
			var spd = ($("#txtDistance_"+i).val() /  (parseFloat(ladan) + parseFloat(speed_adj)));
			var calc = spd/24;
			var ttl_days = parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100);
			ladendays = parseFloat(ladendays) + parseFloat(ttl_days.toFixed(2));
			
			var consp_fo = parseFloat((parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(fo_ladan));
			var consp_do = parseFloat((parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(do_ladan));
			if(isNaN(consp_do) || consp_do=="" || consp_do=="Infinity")
	        {consp_do = 0;	}
			if(isNaN(consp_fo) || consp_fo=="" || consp_fo=="Infinity")
	        {consp_fo = 0;	}	
			consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
			consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
			
			var ttl_dis = parseFloat(distance) + parseFloat($("#txtLDist").val());
			$("#txtLDist").val(ttl_dis.toFixed(2));
			$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
		}
		
		if(isNaN(idle_days) || idle_days=="" || idle_days=="Infinity")
		{idle_days = 0;	}
		if(isNaN(w_days) || w_days=="" || w_days=="Infinity")
		{w_days = 0;	}
		if($("#txtIDays_"+i).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtIDays_"+i).val();}
		if($("#txtWDays_"+i).val() == ""){var w_days = 0;}else{var w_days = $("#txtWDays_"+i).val();}	
		idealdays = parseFloat(idealdays) + parseFloat(idle_days);
		var ttl_days1 = parseFloat(w_days);
		var ttl_days = parseFloat(ttl_days1);
		workingdays = parseFloat(workingdays) + parseFloat(ttl_days);

		var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
		var wrkingdays_fo = parseFloat(w_days) * parseFloat($("#txtPWFOFullSpeed").val());
		var consp_fo =  parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
		
		var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
		var wrkingdays_do = parseFloat(w_days) * parseFloat($("#txtPWDOFullSpeed").val());			
		var consp_do = parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));	
		if(isNaN(consp_do) || consp_do=="" || consp_do=="Infinity")
		{consp_do = 0;	}
		if(isNaN(consp_fo) || consp_fo=="" || consp_fo=="Infinity")
		{consp_fo = 0;	}
		consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
		consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
		
		//Dis Port
		if(isNaN(idle_days) || idle_days=="" || idle_days=="Infinity")
		{idle_days = 0;	}
		if(isNaN(w_days) || w_days=="" || w_days=="Infinity")
		{w_days = 0;	}
		if($("#txtDIDays_"+i).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtDIDays_"+i).val();}
		if($("#txtDWDays_"+i).val() == ""){var w_days = 0;}else{var w_days = $("#txtDWDays_"+i).val();}
		idealdays = parseFloat(idealdays) + parseFloat(idle_days);
		var ttl_days1 = parseFloat(w_days);
		var ttl_days = parseFloat(ttl_days1);
		workingdays = parseFloat(workingdays) + parseFloat(ttl_days);
		
		var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
		var wrkingdays_fo = parseFloat(w_days) * parseFloat($("#txtPWFOFullSpeed").val());
		var consp_fo =  parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
		
		var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
		var wrkingdays_do = parseFloat(w_days) * parseFloat($("#txtPWDOFullSpeed").val());	
		var consp_do =  parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));
		if(isNaN(consp_do) || consp_do=="" || consp_do=="Infinity")
		{consp_do = 0;	}
		if(isNaN(consp_fo) || consp_fo=="" || consp_fo=="Infinity")
		{consp_fo = 0;	}
		consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
		consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
		
		if($("#txtTLIDays_"+i).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtTLIDays_"+i).val();}
		idealdays = parseFloat(idealdays) + parseFloat(idle_days);

		
		var margin_fo = parseFloat(idle_days) * parseFloat($("#txtPIFOFullSpeed").val());
		var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
		if(isNaN(margin_do) || margin_do=="" || margin_do=="Infinity")
		{margin_do = 0;	}
		if(isNaN(margin_fo) || margin_fo=="" || margin_fo=="Infinity")
		{margin_fo = 0;	}
		consumptiondo = parseFloat(consumptiondo) + parseFloat(margin_do.toFixed(2));
		consumptionfo = parseFloat(consumptionfo) + parseFloat(margin_fo.toFixed(2));
	}
	var idd = $("#p_locationID").val();
	for(var i=1;i<=idd;i++)
	{
		if($("#txtLocationDistance_"+i).val() == ""){var distance1 = 0;}else{var distance1 = $("#txtLocationDistance_"+i).val();}
		if($("#selPLocationType_"+i).val() == 1)
		{
			if($("#selSLocationSpeed_"+i).val() == 1)
			{
				var ballast = $("#txtBFullSpeed").val();
				var fo_ballast = $("#txtBFOFullSpeed").val();
				var do_ballast = $("#txtBDOFullSpeed").val();
			}
			if($("#selSLocationSpeed_"+i).val() == 2)
			{
				var ballast = $("#txtBEcoSpeed1").val();
				var fo_ballast = $("#txtBFOEcoSpeed1").val();
				var do_ballast = $("#txtBDOEcoSpeed1").val();
			}
			if($("#selSLocationSpeed_"+i).val() == 3)
			{
				var ballast = $("#txtBEcoSpeed2").val();
				var fo_ballast = $("#txtBFOEcoSpeed2").val();
				var do_ballast = $("#txtBDOEcoSpeed2").val();
			}
			
			var spd = parseFloat(parseFloat(distance1) / parseFloat(ballast));
			var calc = spd/24;
			var ttl_days = parseFloat(calc.toFixed(2));
			ballastdays = ballastdays + parseFloat(ttl_days.toFixed(2));
			var consp_fo = parseFloat(parseFloat(calc) *  parseFloat(fo_ballast));
			var consp_do = parseFloat(parseFloat(calc) *  parseFloat(do_ballast));	
			if(isNaN(consp_do) || consp_do=="" || consp_do=="Infinity")
	        {consp_do = 0;	}
			if(isNaN(consp_fo) || consp_fo=="" || consp_fo=="Infinity")
	        {consp_fo = 0;	}
			consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
			consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
			var ttl_dis = parseFloat(distance1) + parseFloat($("#txtBDist").val());
			$("#txtBDist").val(ttl_dis.toFixed(2));
			$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
		}
		if($("#selPLocationType_"+i).val() == 2)
		{
			if($("#selSLocationSpeed_"+i).val() == 1)
			{
				var ladan = $("#txtLFullSpeed").val();
				var fo_ladan = $("#txtLFOFullSpeed").val();
				var do_ladan = $("#txtLDOFullSpeed").val();
			}
			if($("#selSLocationSpeed_"+i).val() == 2)
			{
				var ladan = $("#txtLEcoSpeed1").val();
				var fo_ladan = $("#txtLFOEcoSpeed1").val();
				var do_ladan = $("#txtLDOEcoSpeed1").val();
			}
			if($("#selSLocationSpeed_"+i).val() == 3)
			{
				var ladan = $("#txtLEcoSpeed2").val();
				var fo_ladan = $("#txtLFOEcoSpeed2").val();
				var do_ladan = $("#txtLDOEcoSpeed2").val();
			}
			var spd = parseFloat(parseFloat(distance1) / parseFloat(ladan));
			var calc = spd/24;
			var ttl_days = parseFloat(calc.toFixed(2));
			ladendays = parseFloat(ladendays) + parseFloat(ttl_days.toFixed(2));
			var consp_fo = parseFloat(parseFloat(calc) *  parseFloat(fo_ladan));
			var consp_do = parseFloat(parseFloat(calc) *  parseFloat(do_ladan));
			if(isNaN(consp_do) || consp_do=="" || consp_do=="Infinity")
	        {consp_do = 0;	}
			if(isNaN(consp_fo) || consp_fo=="" || consp_fo=="Infinity")
	        {consp_fo = 0;	}	
			consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
			consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
			var ttl_dis = parseFloat(distance1) + parseFloat($("#txtLDist").val());
			$("#txtLDist").val(ttl_dis.toFixed(2));
			$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
		}
		
	    /*var margin_do = parseFloat(idle_days) * parseFloat($("#txtPIDOFullSpeed").val());
		var wrkingdays_do = parseFloat(w_days) * parseFloat($("#txtPWDOFullSpeed").val());			
		var consp_do = parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));	
		if(isNaN(consp_do) || consp_do=="" || consp_do=="Infinity")
		{consp_do = 0;	}
		if(isNaN(consp_fo) || consp_fo=="" || consp_fo=="Infinity")
		{consp_fo = 0;	}
		consumptiondo = parseFloat(consumptiondo) + parseFloat(consp_do.toFixed(2));
		consumptionfo = parseFloat(consumptionfo) + parseFloat(consp_fo.toFixed(2));
		
		consumptiondo = parseFloat(consumptiondo) + parseFloat(margin_do.toFixed(2));
		consumptionfo = parseFloat(consumptionfo) + parseFloat(margin_fo.toFixed(2));*/
	}
	
	if(isNaN(consumptionfo) || consumptionfo=="")
	{consumptionfo = 0;	}
	$("#txtTFUMT").val(consumptionfo.toFixed(2));
	if(isNaN(consumptiondo) || consumptiondo=="")
	{consumptiondo = 0;	}
	$("#txtTDUMT").val(consumptiondo.toFixed(2));
	$("#txtLDays").val(ladendays.toFixed(2));
	$("#txtBDays").val(ballastdays.toFixed(2));
	
	$("#txtTtPIDays").val($("[id^=txtTLIDays_],[id^=txtIDays_],[id^=txtDIDays_]").sum().toFixed(2));
	$("#txtTtPWDays").val($("[id^=txtWDays_],[id^=txtDWDays_]").sum().toFixed(2));
	$("#txtTtPIDays").val(parseFloat(idealdays.toFixed(2)));$("#txtTtPWDays").val(parseFloat(workingdays.toFixed(2)));
	$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
	$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
	getBunkerCalculation();
}


function showHideQtyVendorDiv(value)
{
	if($("#rdoQtyType1").is(":checked"))
	{
		$("#divMarket1,#divMarket2,#divMarket3,#divMarket4,#divMarket5,#divMarket6,#divMarket7,#divMarket8,#divMarket9,#divMarket10").show();
		$("#divQty1,#divQty2,#divQty3,#divQty4,#tblQtyFreight1,#tblQtyFreight2").hide();
	}
	if($("#rdoQtyType2").is(":checked"))
	{
		$("#divMarket1,#divMarket2,#divMarket3,#divMarket4,#divMarket5,#divMarket6,#divMarket7,#divMarket8,#divMarket9,#divMarket10").hide();
		$("#divQty1,#divQty2,#divQty3,#divQty4,#tblQtyFreight1,#tblQtyFreight2").show();
		if(value==2)
		{
		  $("#tblQtyFreight,#tblQtyFreight1,#tblQtyFreight2").empty();
		  $("#txtQTYID").val(0);
		}
	}
	getPortCalculation();
}


function getMarLocalFreightCal()
{
	if($("#txtMarLocalAggriedFreight").val() == ""){var localfreightcost = 0;}else{var localfreightcost = $("#txtMarLocalAggriedFreight").val();}
	if($("#txtMarExchangeRate").val() == ""){var txtMarExchangeRate = 0;}else{var txtMarExchangeRate = $("#txtMarExchangeRate").val();}
	var calmul = parseFloat(localfreightcost)/parseFloat(txtMarExchangeRate);
	if(isNaN(calmul) || calmul=="" || calmul=="Infinity"){calmul = 0;}
	$("#txtMTCPDRate").val(calmul.toFixed(2));
	getFinalCalculation();
}


function getQtyLocalFreightCal()
{
	var id = $("#txtQTYID").val();
	for(var i=1;i<=id;i++)
	{
		if($("#txtQtyLocalAggriedFreight_"+i).val() == ""){var localfreightcost = 0;}else{var localfreightcost = $("#txtQtyLocalAggriedFreight_"+i).val();}
		if($("#txtDisExchangeRate_"+i).val() == ""){var txtDisExchangeRate = 0;}else{var txtDisExchangeRate = $("#txtDisExchangeRate_"+i).val();}
		var calmul = parseFloat(localfreightcost)/parseFloat(txtDisExchangeRate);
		if(isNaN(calmul) || calmul=="" || calmul=="Infinity"){calmul = 0;}
		$("#txtQtyAggriedFreight_"+i).val(calmul.toFixed(2));
		if($("#txtFreightQty_"+i).val() == ""){var txtFreightQty = 0;}else{var txtFreightQty = $("#txtFreightQty_"+i).val();}
		var calmul2 = parseFloat(calmul)*parseFloat(txtFreightQty);
		if(isNaN(calmul2) || calmul2=="" || calmul=="Infinity"){calmul2 =0;}
		$("#txtQtyFreight_"+i).val(calmul2.toFixed(2));
		$("#txtFreightQty1_"+i).val(calmul2.toFixed(2));
		
		if($("#txtQtyBrokeragePer_"+i).val() == ""){var brokrage = 0;}else{var brokrage = $("#txtQtyBrokeragePer_"+i).val();}
		var brokerage_usd = (parseFloat(calmul2) * parseFloat(brokrage))/100;
		$("#txtQtyBrokerageAmt_"+i).val(brokerage_usd.toFixed(2));
		var diff = $("#txtFreightQty1_"+i).val() - $("#txtQtyBrokerageAmt_"+i).val();
		if(diff == "" || isNaN(diff) || diff=="Infinity"){var diff = 0;}
		$("#txtQtyNetFreight_"+i).val(diff.toFixed(2));
		$("#txtQtyNetFreight1_"+i).val(diff.toFixed(2));
		var qtyMT = parseFloat(diff) / parseFloat(calmul2);
		if(qtyMT == "" || isNaN(qtyMT) || qtyMT=="Infinity"){var qtyMT = 0;}
		$("#txtQtyNetFreightMT1_"+i).val(qtyMT.toFixed(2));
	}
	$("#txtTotalFreightQty").val($("[id^=txtFreightQty_]").sum().toFixed(2));
	$("#txtTotalQtyFreight").val($("[id^=txtQtyFreight_]").sum().toFixed(2));
	$("#txtTotalNetBrokerage").val($("[id^=txtQtyBrokerageAmt_]").sum().toFixed(2));
	$("#txtTotalNetFreight").val($("[id^=txtQtyNetFreight_]").sum().toFixed(2));
	getFinalCalculation();
}


function addQtyVendorDetails()
{
	var id = $("#txtQTYID").val();
	if($("#selQtyVendorList").val() != "" && $("#txtQtyAggriedFreight").val() != "" && $("#txtFreightQty").val() != "" )
	{
		$("#tbrQtyVRow_empty,#tbrQtyVRow_empty1,#tbrQtyVRow_empty2").remove();
		id = (id - 1) + 2;
		$('<tr id="tbrQtyVRow_'+id+'"><td><a href="#tb'+id+'" onclick="deleteQtyVendorDetails('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><select  name="selQtyVendorList_'+id+'" class="select form-control" id="selQtyVendorList_'+id+'" onChange="showVendorName('+id+')"></select></td><td><input type="text" name="txtQtyLocalAggriedFreight_'+id+'" id="txtQtyLocalAggriedFreight_'+id+'" class="form-control"  autocomplete="off" value="" placeholder="Agreed Gross Freight (Local Currency)" onKeyUp="getQtyLocalFreightCal();"/></td><td><select  name="selCurrencyDisList_'+id+'" class="select form-control" id="selCurrencyDisList_'+id+'"><?php $obj->getCurrencyList(); ?></select></td><td><input type="text" name="txtDisExchangeRate_'+id+'" id="txtDisExchangeRate_'+id+'" class="form-control"  autocomplete="off" value="" placeholder="Exchange Rate" onKeyUp="getQtyLocalFreightCal();"/></td><td><input type="text" name="txtQtyAggriedFreight_'+id+'" id="txtQtyAggriedFreight_'+id+'" class="form-control"  autocomplete="off" value="" placeholder="Agreed Gross Freight (USD/MT)" onKeyUp="getQtyLocalFreightCal();"/></td><td><input type="text" name="txtFreightQty_'+id+'" id="txtFreightQty_'+id+'" class="form-control"  value="" placeholder="Quantity" onKeyUp="getQtyLocalFreightCal();"/></td><td><input type="text" name="txtQtyFreight_'+id+'" id="txtQtyFreight_'+id+'" class="form-control" value="0" readonly/></td></tr>').appendTo("#tblQtyFreight");
		$("#selQtyVendorList_"+id).html($("#selVendor").html());

		$('<tr id="tbrQtyVRow1_'+id+'"><td><span id="tbrqtyvrowvendor1_'+id+'">qqqqqq</span></td><td><input type="text" name="txtFreightQty1_'+id+'" id="txtFreightQty1_'+id+'" value="'+$("#txtQtyFreight").val()+'" autocomplete="off" class="form-control" readonly value="0.00" /></td><td><input type="text" name="txtQtyBrokeragePer_'+id+'" id="txtQtyBrokeragePer_'+id+'" value="0" onKeyUp="getQtyLocalFreightCal();" class="form-control" value="0.00" /></td><td><input type="text" name="txtQtyBrokerageAmt_'+id+'" id="txtQtyBrokerageAmt_'+id+'" value="0" class="form-control" readonly value="0.00" /></td><td><input type="text" name="txtQtyNetFreight_'+id+'" id="txtQtyNetFreight_'+id+'" value="0" class="form-control" readonly value="'+$("#txtQtyFreight").val()+'" /></td></tr>').appendTo("#tblQtyFreight1");
		
		
		$('<tr id="tbrQtyVRow2_'+id+'"><td>Final Nett Freight</td><td><span id="tbrqtyvrowvendor2_'+id+'">sdsdsd</span></td></td><td><input type="text"  name="txtQtyNetFreight1_'+id+'" id="txtQtyNetFreight1_'+id+'" class="form-control" readonly value="0.00" /></td><td><input type="text"  name="txtQtyNetFreightMT1_'+id+'" id="txtQtyNetFreightMT1_'+id+'" class="form-control" readonly value="0.00" /></td></tr>').appendTo("#tblQtyFreight2");
		$("#txtQTYID").val(id);
		$("[id^=txtQtyAggriedFreight_], [id^=txtFreightQty_], [id^=txtQtyBrokeragePer_], [id^=txtQtyLocalAggriedFreight_], [id^=txtDisExchangeRate_], [id^=txtMarLocalAggriedFreight_], [id^=txtMarExchangeRate_]").numeric();
	}
	else
	{
		jAlert('Please fill All details', 'Alert');
		return false;
	}
	getFinalCalculation();
}

function showVendorName(val)
{
	if($("#txtQtyFreight_"+val).val()=="" || isNaN($("#txtQtyFreight_"+val).val())){var freight = 0;}else{var freight = $("#txtQtyFreight_"+val).val();}
	$("#txtFreightQty1_"+val).val(freight);
	if($("#selQtyVendorList_"+val).val() == "")
	{
		$("#tbrQtyVRowVendor1_"+val).html("");
		$("#tbrQtyVRowVendor2_"+val).html("");
	}
	else
	{
		var vendorname = $("#selQtyVendorList_"+val+" option:selected").text();
		$("#tbrqtyvrowvendor1_"+val).html(vendorname);
		$("#tbrqtyvrowvendor2_"+val).html(vendorname);
	}
}

function deleteQtyVendorDetails(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#tbrQtyVRow_"+var1).remove();
					$("#tbrQtyVRow1_"+var1).remove();
					$("#tbrQtyVRow2_"+var1).remove();
					getQtyVendorDetailsSum();	
				 }
			else{return false;}
			});
}


function getDDCOwnerCalculation(){
	
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	
	$('[id^=txtDDCLPCost_]').each(function(index) {
		var rowid       = this.id;
		var ddswLP1     = rowid.split('_')[1];
		if($("#txtLPLayTimeIs_"+ddswLP1).val()==0)
		{
		$("#txtDDCLPCost_"+ddswLP1).val($("#txtEstDDCLPCost_"+ddswLP1).val());
		}
		if($("#txtDDCLPComm_"+ddswLP1).val() == ""){var addLPcomm = 0;}else{var addLPcomm = $("#txtDDCLPComm_"+ddswLP1).val();}
		if($("#txtDDCLPCost_"+ddswLP1).val() == ""){var addLPcost = 0;}else{var addLPcost = $("#txtDDCLPCost_"+ddswLP1).val();}
		var remLpAmt  = parseFloat(addLPcost) - parseFloat(parseFloat(parseFloat(addLPcost)*parseFloat(addLPcomm))/100);
		$('#txtDDCLPNetCostMT_'+ddswLP1).val(remLpAmt.toFixed(2));
		var ddswLPCCost = parseFloat(remLpAmt)/parseFloat(cargo_qty);
		$('#txtDDCLPCostMT_'+ddswLP1).val(ddswLPCCost.toFixed(2));
	});
	
	$('[id^=txtDDCDPCost_]').each(function(index) {
		var rowid       = this.id;
		var ddswDP1     = rowid.split('_')[1];
		if($("#txtDPLayTimeIs_"+ddswDP1).val()==0)
		{
		$("#txtDDCDPCost_"+ddswDP1).val($("#txtEstDDCDPCost_"+ddswDP1).val());
		}
		if($("#txtDDCDPComm_"+ddswDP1).val() == ""){var addDPcomm = 0;}else{var addDPcomm = $("#txtDDCDPComm_"+ddswDP1).val();}
		if($("#txtDDCDPCost_"+ddswDP1).val() == ""){var addDPcost = 0;}else{var addDPcost = $("#txtDDCDPCost_"+ddswDP1).val();}
		var remLpAmt  = parseFloat(addDPcost) - parseFloat(parseFloat(parseFloat(addDPcost)*parseFloat(addDPcomm))/100);
		$('#txtDDCDPNetCostMT_'+ddswDP1).val(remLpAmt.toFixed(2));
		var ddswDPCCost = parseFloat(remLpAmt)/parseFloat(cargo_qty);
		$('#txtDDCDPCostMT_'+ddswDP1).val(ddswDPCCost.toFixed(2));
	});
	var LPDPttl = parseFloat($('[id^=txtDDCLPNetCostMT_]').sum().toFixed(2)) + parseFloat($('[id^=txtDDCDPNetCostMT_]').sum().toFixed(2));
	return LPDPttl;
}

function ShowDDCDPDP()
{
	var id = $("#p_rotationID").val();
	for(var i=1;i<=id;i++)
	{
		if($("#ChkShowDDCLP_"+i).is(":checked"))
		{
			$("#DDCLProw_"+i).hide();
		}
		else
		{
			$("#DDCLProw_"+i).show();
		}
		
		if($("#ChkShowDDCDP_"+i).is(":checked"))
		{
			$("#DDCDProw_"+i).hide();
		}
		else
		{
			$("#DDCDProw_"+i).show();
		}
	}
}

function getShowHide()
{
	if($("#txtbid").val() == 0)
	{
		$('#BID').attr('title', 'Open Panel');
		$("#bunker_adj").hide();
		$("#txtbid").val(1);
	}
	else
	{
		$('#BID').attr('title', 'Close Panel');
		$("#bunker_adj").show();
		$("#txtbid").val(0);
	}
}


</script>
    </body>
</html>