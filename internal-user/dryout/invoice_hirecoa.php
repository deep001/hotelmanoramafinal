<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$comid = $_REQUEST['id'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "vessel_in_post_ops.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "vessel_in_history.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "coa_in_ops_at_glance.php"; $page_bar="In Ops at a glance - COA";$nav=20;$subtitle = 'COA';}else if($page == 5){$page_link = "coa_in_post_ops.php"; $page_bar="In Post Ops at a glance - COA";$nav=20;$subtitle = 'COA';}else{$page_link = "coa_in_history.php"; $page_bar="Vessels in History - COA";$nav=20;$subtitle = 'COA';}

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertInvoiceHireDetails();
	$msg = explode(",",$msg);
	header('Location:./hire_invoice_listcoa.php?msg='.$msg[0].'&id='.$msg[1].'&page='.$page);
}
$fcaid = $id = $obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($id);
$arr       = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$fcaid);

$lp_name = array(); 
$dp_name = array();
$discharge_port = "";

for($j=0;$j<count($arr);$j++)
{   
    $arr_explode = explode('@@',$arr[$j]);
	if($arr_explode[0] == "LP"){$lp_name[]= $obj->getPortNameBasedOnID($arr_explode[1]);}
	else{$dp_name[]= $obj->getPortNameBasedOnID($arr_explode[1]);}
	$k = $j+1;
	if($k = count($arr) && $arr_explode[0] == "DP"){$dp_name[] = $obj->getPortNameBasedOnID($arr_explode[1]);}
}


$lp_name  = implode(', ',$lp_name);
$dp_name  = implode(', ',$dp_name);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}

$bunkergrade = array( 1 =>1 , 2 =>2, 3 =>3 , 4 =>4);
$bunkergrade1= array( 1 =>'HSFO' , 2 =>'LSFO', 3 =>'HSDO' , 4 =>'LSDO');
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;
$sqlcoa = "select CARGO from coa_master where COAID='".$obj->getFun175()."'";
$resultcoa = mysql_query($sqlcoa) or die($sqlcoa);
$rowscoa   = mysql_fetch_assoc($resultcoa);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
$("#frm1").validate({
	rules: {
	selIType: {required: true},
	txtInvoiceNo: {required: true},
	txtDate: {required: true},
	txtDelLtTimeGMT:{required: true},
	txtReDelLtTimeGMT:{required: true},
	selDelPort:{required: true},
	selReDelPort:{required: true},
	txtTCCost:{required: true},
	txtReleaseHireDays:{required: true},
	selNOB:{required: true}
	},
messages: {
	selIType: {required: "*"},
	txtInvoiceNo: {required: true},
	txtDate: {required: true},
	txtDelLtTimeGMT:{required: "*"},
	txtReDelLtTimeGMT:{required: "*"},
	selDelPort:{required: "*"},
	selReDelPort:{required: "*"},
	txtTCCost:{required: "*"},
	txtReleaseHireDays:{required: "*"},
	selNOB:{required: "*"}
	},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

$("#txtTO,#txtP_PR,#txtTCCost,#txtReleaseHireDays,#txtBallastBonus,[id^=txtAddDesAmt_],#txtDelCVE,#txtDelCVEPer,[id^=txtSubDesAmt_],#txtCommission,#txtCommissionAmt,[id^=txtDelConspQtyMT_],[id^=txtDelPrice_],[id^=txtDelConspPer_],[id^=txtReDelConspQtyMT_],[id^=txtReDelPrice_],[id^=txtReDelConspPer_],[id^=txtOnReDelConspQtyMT_],[id^=txtOnReDelPrice_],[id^=txtOnReDelConspPer_],[id^=txtOffPer_1],#txtBrokerage").numeric();

$('#txtP_Remarks, #txtRemarks, #txtBankingDetails,[id^=txtSubDes_],[id^=txtAddDes_]').autosize({append: "\n"});

$('#txtDate, #txtP_Date').datepicker({
	format: 'dd-mm-yyyy',
	autoclose:true
});

$('#txtDelLtTimeGMT,#txtReDelLtTimeGMT,[id^=txtFromOff_],[id^=txtToOff_]').datetimepicker({
	format: 'dd-mm-yyyy hh:ii',
    todayBtn: true,
    minuteStep: 1,
	autoclose:true
	}).on('changeDate', function(){  getFullClaculation();	});
getDelBunkerGradeAmt();
getReDelBunkerGradeAmt();
getOnReDelBunkerGradeAmt();
getAdjustmentDiv();
});





function getTimeDiff(date2,date1)
{	
	if(date1 != '' &&  date2 != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat(date2);
		start_actual_time    =  getDateWithSpecificFormat(date1);
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000;
		return days.toFixed(5);	
	}
}


function getString(var1)
{
  var var2 = var1.split(' ');
  var var3 = var2[0].split('-');  
  return var3[2]+'/'+var3[1]+'/'+var3[0]+' '+var2[1];
}
 
function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' '+dateSplit1[1];
	return currentDate;
}

function AddNewAddRow()
{
	var id = $("#txtAddID").val();
	if($("#txtAddDes_"+id).val() != "" && $("#txtAddDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="add_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtAddDes_'+id+'" id="txtAddDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtAddDesAmt_'+id+'" id="txtAddDesAmt_'+id+'" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td></tr>').appendTo("#tblAdd");
		$("#txtAddID").val(id);
		$("[id^=txtAddDesAmt_]").numeric();	
	}
}


function removeAddRow(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#add_Row_"+var1).remove();
					getFullClaculation();
				 }
			else{return false;}
			});
}



function SubNewSubRow()
{
	var id = $("#txtSubID").val();
	if($("#txtSubDes_"+id).val() != "" && $("#txtSubDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="Sub_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtSubDes_'+id+'" id="txtSubDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtSubDesAmt_'+id+'" id="txtSubDesAmt_'+id+'" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td></tr>').appendTo("#tblSub");
		$("#txtSubID").val(id);
		$("[id^=txtSubDesAmt_]").numeric();	
	}
}


function removeSubRow(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#Sub_Row_"+var1).remove();
				getFullClaculation();
			 }
		else{return false;}
		});
}


function AddNewOffHire()
{
	var id = $("#txtOFFID").val();
	if($("#txtOffHireReason_"+id).val() != "" && $("#txtFromOff_"+id).val() != "" && $("#txtToOff_"+id).val() != "" && $("#txtOffPer_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="off_Row_'+id+'"><td align="center"><a href="#pr'+id+'" onclick="removeOffRow('+id+');"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtOffHireReason_'+id+'" id="txtOffHireReason_'+id+'" rows="2" placeholder="Off Hire Reason..."></textarea></td><td><input type="text" name="txtFromOff_'+id+'" id="txtFromOff_'+id+'" class="form-control" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""></td><td><input type="text" name="txtToOff_'+id+'" id="txtToOff_'+id+'" class="form-control" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""></td><td><input type="text" name="txtOffPer_'+id+'" id="txtOffPer_'+id+'" class="form-control"  placeholder="%" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td></tr>').appendTo("#tblOFF");
		$("#txtOFFID").val(id);
		$("[id^=txtOffPer_]").numeric();	
		$('#txtFromOff_'+id+',#txtToOff_'+id).datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
	    autoclose: true,
        todayBtn: true,
        minuteStep: 1
		}).on('changeDate', function(){  getFullClaculation(); });
	}
}


function removeOffRow(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#off_Row_"+var1).remove();
					getFullClaculation();
				 }
			else{return false;}
			});
}


function getFullClaculation()
{
    var delDays1 = parseFloat(getTimeDiff($("#txtReDelLtTimeGMT").val(),$("#txtDelLtTimeGMT").val()));
	if(isNaN(delDays1)){delDays1 = 0.00;}
	$("#txtTripDuration").val(parseFloat(delDays1).toFixed(5));
	var tccost = parseFloat($("#txtTCCost").val());
	if(isNaN(tccost)){tccost = 0.00;}
	var delDays = parseFloat($("#txtReleaseHireDays").val());
    var numoff = $("#txtOFFID").val();
	var offhiredays = 0;
	for(var i =1;i <=numoff;i++)
	{
		if(typeof($("#txtToOff_"+i).val())!='undefined')
		{
			var timediff = parseFloat(getTimeDiff($("#txtToOff_"+i).val(),$("#txtFromOff_"+i).val()));
			if(parseInt($("#txtOffPer_"+i).val())>0)
			{var percent = $("#txtOffPer_"+i).val();}
			else
			{var percent = 100;}
			offhiredays =   parseFloat(parseFloat(offhiredays) + parseFloat(parseFloat(parseFloat(timediff)*parseFloat(percent))/100));
			$("#txtOffPerAmt_"+i).val(parseFloat(parseFloat(parseFloat(timediff)*parseFloat(percent))/100).toFixed(5));
		}
	}
	if(isNaN(offhiredays)){offhiredays = 0.00;}
	$("#txtNettOffHire").val(offhiredays.toFixed(5));
	var nettperiod = parseFloat(delDays)- parseFloat(offhiredays);
	if(isNaN(nettperiod)){nettperiod = 0.00;}
	$("#txtNettRelease").val(parseFloat(nettperiod).toFixed(5));
	var nettHire = parseFloat(tccost) * parseFloat($("#txtNettRelease").val());
	if(isNaN(nettHire)){nettHire = 0.00;}
	$("#txtNettHire").val(parseFloat(nettHire).toFixed(2));
	var netthire =parseFloat($("#txtNettHire").val());
	if(isNaN(netthire)){netthire = 0.00;}
	var ballastbonus =parseFloat($("#txtBallastBonus").val());
	if(isNaN(ballastbonus)){ballastbonus = 0.00;}
	var nettballahire = parseFloat(ballastbonus) + parseFloat(netthire);
	var comission = $("#txtCommission").val();
	if(isNaN(comission)){comission = 0.00;}
	var commamt =   parseFloat(parseFloat(parseFloat(nettballahire)*parseFloat(comission))/100);
	if(isNaN(commamt)){commamt = 0.00;}
	$("#txtCommissionAmt").val(parseFloat(commamt).toFixed(2));
	
	var Brokerage = $("#txtBrokerage").val();
	if(isNaN(Brokerage)){Brokerage = 0.00;}
	var BrokerageAmt =   parseFloat(parseFloat(parseFloat(nettballahire)*parseFloat(Brokerage))/100);
	if(isNaN(BrokerageAmt)){BrokerageAmt = 0.00;}
	$("#txtBrokerageAmt").val(parseFloat(BrokerageAmt).toFixed(2));
	
	var cveamt = parseFloat(parseFloat($("#txtDelCVE").val())/30)*parseFloat($("#txtNettRelease").val());
	if(isNaN(cveamt)){cveamt = 0.00;}
	$("#txtDelCVEAmt").val(cveamt.toFixed(2));
	if($("#txtDelCVEPer").val()=="" || $("#txtDelCVEPer").val()==0){$("#txtDelCVEPerAmt").val($("#txtDelCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtDelCVEAmt").val())*parseFloat($("#txtDelCVEPer").val()))/100);
	if(isNaN(perAmt)){perAmt = 0.00;}$("#txtDelCVEPerAmt").val(perAmt.toFixed(2));}
	
	 var delAmt = parseFloat($("#txtDelConspTotalAmt").val());
	 if(isNaN(delAmt)){delAmt = 0.00;}
	 
	 var cveamt = parseFloat($("#txtDelCVEPerAmt").val());
	 if(isNaN(cveamt)){cveamt = 0.00;}
	 
	 var redelAmt = parseFloat($("#txtReDelConspTotalAmt").val());
	 if(isNaN(redelAmt)){redelAmt = 0.00;}
	 
	  var onRedelAmt = parseFloat($("#txtOnReDelConspTotalAmt").val());
	 if(isNaN(onRedelAmt)){onRedelAmt = 0.00;}
	
	$("#txtAddDesTotalAmt").val(parseFloat($("[id^=txtAddDesAmt_]").sum()).toFixed(2));
	
	$("#txtSubDesTotalAmt").val(parseFloat($("[id^=txtSubDesAmt_]").sum()).toFixed(2));
	
	var lastAmt = parseFloat(nettballahire) + parseFloat(delAmt) + parseFloat(cveamt) + parseFloat($("#txtAddDesTotalAmt").val()) + parseFloat(redelAmt)  - parseFloat(commamt) - parseFloat(BrokerageAmt) - parseFloat(onRedelAmt) - parseFloat($("#txtSubDesTotalAmt").val()) - parseFloat($("[id^=txtPrevInvoiceAmt_]").sum());
	if(isNaN(lastAmt)){lastAmt = 0.00;}//txtPrevInvoiceAmt_
	$("#txtFinalAmt").val(parseFloat(lastAmt).toFixed(2))
}

function getOnReDelBunkerGradeAmt()
{
	var total = $("#txtOnReDelbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtOnReDelConspQtyMT_"+i).val())*parseFloat($("#txtOnReDelPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtOnReDelConspAmt_"+i).val(amt.toFixed(2))
		if($("#txtOnReDelConspPer_"+i).val()=="" || $("#txtOnReDelConspPer_"+i).val()==0)
		{
		   $("#txtOnReDelConspPerAmt_"+i).val($("#txtOnReDelConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtOnReDelConspAmt_"+i).val())*parseFloat($("#txtOnReDelConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtOnReDelConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	$("#txtOnReDelConspTotalAmt").val(parseFloat($("[id^=txtOnReDelConspPerAmt_]").sum()).toFixed(2));
	getFullClaculation();
}


function getReDelBunkerGradeAmt()
{
	var total = $("#txtReDelbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtReDelConspQtyMT_"+i).val())*parseFloat($("#txtReDelPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtReDelConspAmt_"+i).val(amt.toFixed(2))
		if($("#txtReDelConspPer_"+i).val()=="" || $("#txtReDelConspPer_"+i).val()==0)
		{
		   $("#txtReDelConspPerAmt_"+i).val($("#txtReDelConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtReDelConspAmt_"+i).val())*parseFloat($("#txtReDelConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtReDelConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	$("#txtReDelConspTotalAmt").val(parseFloat($("[id^=txtReDelConspPerAmt_]").sum()).toFixed(2));
	getFullClaculation();
}

function getDelBunkerGradeAmt()
{
	var total = $("#txtDelbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtDelConspQtyMT_"+i).val())*parseFloat($("#txtDelPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtDelConspAmt_"+i).val(amt.toFixed(2))
		if($("#txtDelConspPer_"+i).val()=="" || $("#txtDelConspPer_"+i).val()==0)
		{
		   $("#txtDelConspPerAmt_"+i).val($("#txtDelConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtDelConspAmt_"+i).val())*parseFloat($("#txtDelConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtDelConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	$("#txtDelConspTotalAmt").val(parseFloat($("[id^=txtDelConspPerAmt_]").sum()).toFixed(2));
	getFullClaculation();
}

function getAdjustmentDiv()
{
	if($("#selIType option:selected").text()=="Adjustment")
	window.location= "addadjustHirecoa.php?id=<?php echo $comid;?>&page=<?php echo $page;?>";
}

function getValidate(var1)
{
	$("#txtStatus").val(var1);
	//document.frm1.submit();
}

</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?>&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
                <?php
				$sql = "SELECT * from invoice_hire_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and STATUS in (0,1) LIMIT 1";
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				if($rows['INV_TYPE']==2)
				{
					$sql4 = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
					$res4 = mysql_query($sql4);
					$rec4 = mysql_num_rows($res4);
					$bunkergrade = array();
					$bunkergrade1 = array();
					$i=1;
					while($rows4 = mysql_fetch_assoc($res4))
					{
						$bunkergrade[$i] = $rows4['BUNKERGRADEID'];
						$bunkergrade1[$i] = $rows4['NAME'];
						$i++;
					}
				}
				?>	
				<div align="right"><?php if($rec>0){?><a href="allPdf.php?id=46&comid=<?php echo $comid; ?>&invoiceid=<?php echo $rows['INVOICEID']; ?>" title="Pdf"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a><?php }?><a href="hire_invoice_listcoa.php?id=<?php echo $comid;?>&page=<?php echo  $page;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
							
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12" style="text-align:center;">
								<h2 class="page-header">
								Hire Statement
                                <input type="hidden" name="txtFcaid" id="txtFcaid" value="<?php echo $obj->getFun1();?>"/>
                                <input type="hidden" name="txtMapId" id="txtMapId" value="<?php echo $comid;?>"/>
                                <input type="hidden" name="txtInID" id="txtInID" value="<?php echo $rows['INVOICEID'];?>"/>
                                <input type="hidden" name="txtVendorID" id="txtVendorID" value="<?php echo $vendorid;?>"/>
								</h2>                            
							</div><!-- /.col -->
						</div>
					
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Statement Type
								<address>
                                
									<select  name="selIType" class="select form-control" id="selIType" onChange="getAdjustmentDiv();" >
										<?php 
										$obj->getInvoiceType();
										?>
									</select>
									<script>$('#selIType').val('<?php echo $rows['INVOICE_TYPE'];?>');</script>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
							   Statement No.
								<address>
									<input type="text" name="txtInvoiceNo" id="txtInvoiceNo" class="form-control" autocomplete="off" value="<?php echo $rows['INVOICE_NO'];?>"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								CP Date
								<address>
								   <label><?php
								   $cpdate = $obj->getCompareEstimateData($comid,"CP_DATE");
								   if($cpdate=="" || $cpdate=="0000-00-00" || $cpdate=="1970-01-01"){$cpdate = ""; }
								   else{$cpdate = date("d-m-Y",strtotime($obj->getCompareEstimateData($comid,"CP_DATE"))); }
								    echo $cpdate; ?></label>
								   <input type="hidden" name="txtCP_Date" id="txtCP_Date" class="input-text" size="10" value="<?php echo $cpdate; ?>" autocomplete="off" readonly/>
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
						  <div class="col-sm-4 invoice-col">
								 Statement Date
								<address>
								   <?php if($rec == 0){?>
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo date("d-m-Y",time());?>"/>
									<?php }else{?>
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo date("d-m-Y",strtotime($rows['INVOICE_DATE']));?>"/>
								<?php }?>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Nom ID
								<address>
								 <?php echo $obj->getCompareTableData($comid,"MESSAGE");?>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Vessel
								<address>
								   <label><?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></label>
								</address>
							</div><!-- /.col -->
						</div>
                        <div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
								Vessel IMO No.
								<address>
								   <label><?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"IMO_NO");?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-3 invoice-col">
								Supplier
								<address>
								        <label><?php echo $obj->getVendorListNewBasedOnID($vendorid);?></label>
										<input type="hidden" name="txtVendor" id="txtVendor" value="<?php echo $vendorid;?>" />
										<input type="hidden" name="txtNameid" id="txtNameid" value="<?php echo $id[0];?>" />
										<input type="hidden" name="txtGradeid" id="txtGradeid" value="<?php echo $id[2];?>" />
										<input type="hidden" name="txtFCAID" id="txtFCAID" value="<?php echo $obj->getFun1();?>" />
								</address>
							</div><!-- /.col -->                    
							<div class="col-sm-3 invoice-col">
								Loading Port
								<address>
								   <label><?php echo $lp_name;?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-3 invoice-col">
								Discharging Port
								<address>
								   <label><?php echo $dp_name;?></label>
								</address>
							</div><!-- /.col -->
						</div>
						<div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Remarks
								<address>
									<textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" rows="3" placeholder="Remarks ..." ><?php echo $rows['REMARKS'];?></textarea>
								</address>
							</div><!-- /.col -->						
						</div>
						
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								Supplier's Account : Statement
								</h2>                            
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Del At.
								<address>
									<select  name="selDelPort" class="select form-control" id="selDelPort" >
										<?php $obj->getPortList(); ?>
									</select>
									<script>$('#selDelPort').val('<?php echo $rows['DEL_PORTID'];?>');</script>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Del Date Time GMT
								<address>
                                <?php if($rec > 0)
								      {
										  if($rows['DEL_DATE'] !='' || $rows['DEL_DATE']!='0000-00-00 00:00:00' || date('d-m-Y',strtotime($rows['DEL_DATE']))!='1970-01-01')
										  {$delDate ="";}
										  else{$delDate =  date('d-m-Y H:i',strtotime($rows['DEL_DATE']));}
									  }else{$delDate = '';}?>
								  <input type="text" name="txtDelLtTimeGMT" id="txtDelLtTimeGMT" class="form-control" value="<?php echo $delDate;?>"/>
								</address>
							</div><!-- /.col -->
						</div>
						
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   ReDel At.
								<address>
									<select  name="selReDelPort" class="select form-control" id="selReDelPort" >
										<?php $obj->getPortList(); ?>
									</select>
									<script>$('#selReDelPort').val('<?php echo $rows['REDEL_PORTID'];?>');</script>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								ReDel Date Time GMT
								<address>
                                 <?php 
								 if($rec > 0)
								  {
									  if($rows['REDEL_DATE'] !='' || $rows['REDEL_DATE']!='0000-00-00 00:00:00' || date('d-m-Y',strtotime($rows['REDEL_DATE']))!='1970-01-01')
									  {$delDate ="";}
									  else{$delDate =  date('d-m-Y H:i',strtotime($rows['REDEL_DATE']));}
								  }else{$delDate = '';}?>
								  <input type="text" name="txtReDelLtTimeGMT" id="txtReDelLtTimeGMT" class="form-control" value="<?php echo $delDate;?>"/>
								</address>
							</div><!-- /.col -->
						</div>
						
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Total Trip Duration
								<address>
                                
									<input type="text" name="txtTripDuration" id="txtTripDuration" class="form-control" readonly autocomplete="off" value="<?php echo $rows['TRIP_DURATION'];?>"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
							   Tc Cost/Day (USD)
								<address>
									<input type="text" name="txtTCCost" id="txtTCCost" class="form-control" value="<?php echo $rows['TC_COST'];?>" onKeyUp="getFullClaculation();" autocomplete="off"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Release Hire Days
								<address>
								  <input type="text" name="txtReleaseHireDays" id="txtReleaseHireDays" class="form-control" onKeyUp="getFullClaculation();" value="<?php echo $rows['RELEASE_HIRE_DAYS'];?>" autocomplete="off"/>
								</address>
							</div><!-- /.col -->
						</div>
					 <div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Off Hire Reason</th>
                                    <th>Off Hire From (Days)</th>
                                    <th>Off Hire To (Days)</th>
                                    <?php $sql2 = "select * from invoice_hire_slave3 where INVOICEID='".$rows['INVOICEID']."'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									if($num2 >0)
									{
										$num21 = $num2;
									}
									else
									{
									$sql3 = "select * from freight_cost_estimete_slave10 where FCAID='".$obj->getFun1()."'";
									$res3 = mysql_query($sql3);
									$num3 = mysql_num_rows($res3);	
									if($num3 >0){$num21 = $num3;}else{$num21 = 1;}
									}
									?>
									<th>Off Hire %<input type="hidden" name="txtOFFID" id="txtOFFID" value="<?php echo $num21;?>"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblOFF">
                            <?php if($num2 >0)
							   {$i = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;
								?>
                                <tr id="off_Row_<?php echo $i;?>">
                                   <td align="center"><a href="#pr1" onClick="removeOffRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtOffHireReason_<?php echo $i;?>" id="txtOffHireReason_<?php echo $i;?>" rows="2" placeholder="Off Hire Reason..."><?php echo $rows2['OFFHIRE_REASON'];?></textarea></td>
                                   <td><input type="text" name="txtFromOff_<?php echo $i;?>" id="txtFromOff_<?php echo $i;?>" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($rows2['FROM_OFF_DATE']));?>"/></td>
                                   <td><input type="text" name="txtToOff_<?php echo $i;?>" id="txtToOff_<?php echo $i;?>" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($rows2['TO_OFF_DATE']));?>"/></td>
								   <td><input type="text" name="txtOffPer_<?php echo $i;?>" id="txtOffPer_<?php echo $i;?>" class="form-control"  placeholder="%" autocomplete="off" value="<?php echo $rows2['OFFHIRE_PERCENT'];?>" onKeyUp="getFullClaculation();"/><input type="hidden" name="txtOffPerAmt_<?php echo $i;?>" id="txtOffPerAmt_<?php echo $i;?>" value="" /></td>
                                </tr>
                                <?php }}else{
								if($num3 >0)
								   {$i = 0;
									while($rows3 = mysql_fetch_assoc($res3))
									{$i++;
									?>
                                 <tr id="off_Row_<?php echo $i;?>">
                                   <td align="center"><a href="#pr1" onClick="removeOffRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtOffHireReason_<?php echo $i;?>" id="txtOffHireReason_<?php echo $i;?>" rows="2" placeholder="Off Hire Reason..."><?php echo $rows3['OFFHIRE_REASON'];?></textarea></td>
                                   <td><input type="text" name="txtFromOff_<?php echo $i;?>" id="txtFromOff_<?php echo $i;?>" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($rows3['FROM_OFF_DATE']));?>"/></td>
                                   <td><input type="text" name="txtToOff_<?php echo $i;?>" id="txtToOff_<?php echo $i;?>" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($rows3['TO_OFF_DATE']));?>"/></td>
								   <td><input type="text" name="txtOffPer_<?php echo $i;?>" id="txtOffPer_<?php echo $i;?>" class="form-control"  placeholder="%" autocomplete="off" value="100" onKeyUp="getFullClaculation();"/><input type="hidden" name="txtOffPerAmt_<?php echo $i;?>" id="txtOffPerAmt_<?php echo $i;?>" value="<?php echo $rows3['OFF_HIRE_DAYS'];?>" /></td>
                                </tr>
                                  <?php }}else{?>  
                                <tr id="off_Row_1">
                                   <td align="center"><a href="#pr1" onClick="removeOffRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtOffHireReason_1" id="txtOffHireReason_1" rows="2" placeholder="Off Hire Reason..."></textarea></td>
                                   <td><input type="text" name="txtFromOff_1" id="txtFromOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/></td>
                                   <td><input type="text" name="txtToOff_1" id="txtToOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/></td>
								   <td><input type="text" name="txtOffPer_1" id="txtOffPer_1" class="form-control"  placeholder="%" autocomplete="off" value="" onKeyUp="getFullClaculation();"/><input type="hidden" name="txtOffPerAmt_1" id="txtOffPerAmt_1" value="" /></td>
                                </tr>
                                <?php }}?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="left" colspan="5"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewOffHire();">Add</button></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
						
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							  Nett Off Hire
								<address>
                                 <input type="text" name="txtNettOffHire" id="txtNettOffHire" class="form-control" autocomplete="off" value="<?php echo $rows['NETT_OFFHIRE'];?>" readonly/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Nett Release
								<address>
								  <input type="text" name="txtNettRelease" id="txtNettRelease" class="form-control" value="<?php echo $rows['NETT_RELEASE'];?>" readonly  autocomplete="off"/>
								</address>
							</div><!-- /.col -->
						</div>
						
						
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								Amount Due To Supplier
								</h2>                            
							</div><!-- /.col -->
						</div>
						
						<div class="box">
                         <div class="box-body no-padding">
							<table class="table table-striped">
								<tbody>
									<tr>
										<td width="20%">Nett Hire</td>
										<td width="15%"></td>
										<td width="20%"></td>
										<td width="20%"></td>
                                       
										<td width="25%"><input type="text" name="txtNettHire" id="txtNettHire" class="form-control" autocomplete="off" value="<?php echo $rows['NETT_HIRE'];?>" readonly/></td>
									</tr>
							 <?php 
									$sql3 = "select * from fca_tci_other_misc_cost where FCAID='".$obj->getFun1()."' and OTHER_MCOSTID='15'";
									$res3 = mysql_query($sql3);
									$rows3 = mysql_fetch_assoc($res3);
									
									
									?>
									<tr>
										<td width="20%">Ballast Bonus</td>
										<td width="15%"></td>
										<td width="20%"></td>
										<td width="20%"></td>
                                        <?php if($rec>0){?>
										<td width="25%"><input type="text" name="txtBallastBonus" id="txtBallastBonus" class="form-control" autocomplete="off" value="<?php echo $rows['BALLAST_BONUS'];?>"  onKeyUp="getFullClaculation();"/></td>
										<?php }else{?>
                                        <td width="25%"><input type="text" name="txtBallastBonus" id="txtBallastBonus" class="form-control" autocomplete="off" value="<?php echo $rows3['AMOUNT'];?>"  onKeyUp="getFullClaculation();"/></td>
                                        <?php }?>
									</tr>
								</tbody>
						      </table>
						 </div>
                       </div>
						
						
						
					<?php
					 $numgrade = count($bunkergrade);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Cost Of Bunker On Delivery</h3>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php  
						$invoice_ty = 1;
						      $sql_check = "select * from invoice_hire_slave2 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='DEL'";
						      $res_check2 = mysql_query($sql_check);
							  $rows_check2 = mysql_fetch_assoc($res_check2);
							  if($rows_check2>0)
							  {
						      for($i=1;$i<=$numgrade;$i++)
					             {
								$sql_inv2 = "select * from invoice_hire_slave2 where INVOICEID='".$rows['INVOICEID']."' and BUNKERID='".$i."' and IDENTIFY='DEL'"; 
								$res_inv2 = mysql_query($sql_inv2);
								$rows_inv2 = mysql_fetch_assoc($res_inv2);	 
									 ?>
                                  <tr>
                                    <td><?php echo $bunkergrade1[$i];?><input type="hidden" id="txtDelBunkerID_<?php echo $i;?>" name="txtDelBunkerID_<?php echo $i;?>" value="<?php echo $i;?>" /></td>
								    <td><input type="text" name="txtDelConspQtyMT_<?php echo $i;?>" id="txtDelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows_inv2['BUNKER_QTY'];?>" onKeyUp="getDelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtDelPrice_<?php echo $i;?>" id="txtDelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows_inv2['BUNKER_PRICE'];?>" onKeyUp="getDelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtDelConspAmt_<?php echo $i;?>" id="txtDelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows_inv2['BUNKER_AMT'];?>"/></td>
									<td><input type="text" name="txtDelConspPer_<?php echo $i;?>" id="txtDelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows_inv2['BUNKER_PER'];?>" onKeyUp="getDelBunkerGradeAmt();"/><input type="Hidden" name="txtDelConspPerAmt_<?php echo $i;?>" id="txtDelConspPerAmt_<?php echo $i;?>" value="<?php echo $rows_inv2['BUNKER_PER_AMT'];?>"/></td>
                                </tr>
                                <?php }}else{
                                $sql4 = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
								$invoice_ty = 2;
								$res4 = mysql_query($sql4);
								$rec4 = mysql_num_rows($res4);
								$numgrade = $rec4;$i = 1;
                                while($rows4 = mysql_fetch_assoc($res4))
								{?>
                                <tr>
                                    <td><?php echo $rows4['NAME'];?><input type="hidden" id="txtDelBunkerID_<?php echo $i;?>" name="txtDelBunkerID_<?php echo $i;?>" value="<?php echo $i;?>" /></td>
								    <td><input type="text" name="txtDelConspQtyMT_<?php echo $i;?>" id="txtDelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$rows4['BUNKERGRADEID'],"DEL_MT");?>" onKeyUp="getDelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtDelPrice_<?php echo $i;?>" id="txtDelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$rows4['BUNKERGRADEID'],"DEL_PRICE");?>" onKeyUp="getDelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtDelConspAmt_<?php echo $i;?>" id="txtDelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$rows4['BUNKERGRADEID'],"DEL_COST");?>"/></td>
									<td><input type="text" name="txtDelConspPer_<?php echo $i;?>" id="txtDelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="100" onKeyUp="getDelBunkerGradeAmt();"/><input type="Hidden" name="txtDelConspPerAmt_<?php echo $i;?>" id="txtDelConspPerAmt_<?php echo $i;?>" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$rows4['BUNKERGRADEID'],"DEL_COST");?>"/></td>
                                </tr>
                                <?php $i++;}}?>
								<tr>
                                    <td colspan="4">Total</td>
									<td><input type="text" name="txtDelConspTotalAmt" id="txtDelConspTotalAmt" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows['DEL_BUNKER_COST'];?>" readonly/><input type="hidden" name="txtDelbid" id="txtDelbid" value="<?php echo $numgrade;?>" /><input type="hidden" name="txtINVType" id="txtINVType" value="<?php echo $invoice_ty;?>" /></td>
                                </tr>
                            </tbody>
                            <tfoot>
							<?php 
								$sql_brok = "select * from fca_tci_owner_related_cost where FCAID='".$obj->getFun1()."' and OWNER_RCOSTID='3'";
								$res_brok = mysql_query($sql_brok);
								$row_brok = mysql_fetch_assoc($res_brok);
								?>
                               <tr>
                                    <td>CVE</td>
                                    <?php if($rec>0){?>
										<td><input type="text" name="txtDelCVE" id="txtDelCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows['CVE'];?>" onKeyUp="getFullClaculation();"/></td>
                                        <td></td>
                                        <td><input type="text" name="txtDelCVEAmt" id="txtDelCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows['CVE_AMT'];?>"/></td>
                                        <td><input type="text" name="txtDelCVEPer" id="txtDelCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows['CVE_PER'];?>" onKeyUp="getFullClaculation();"/><input type="Hidden" name="txtDelCVEPerAmt" id="txtDelCVEPerAmt" value="<?php echo $rows['CVE_PER_AMT'];?>"/></td>
										<?php }else{?>
                                       <td><input type="text" name="txtDelCVE" id="txtDelCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $row_brok['AMOUNT'];?>" onKeyUp="getFullClaculation();"/></td>
                                        <td></td>
                                        <td><input type="text" name="txtDelCVEAmt" id="txtDelCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
                                        <td><input type="text" name="txtDelCVEPer" id="txtDelCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getFullClaculation();"/><input type="Hidden" name="txtDelCVEPerAmt" id="txtDelCVEPerAmt" value="0.00"/></td>
                                        <?php }?>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
				
				
				
				
				<div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Description</th>
                                    <?php $sql2 = "select * from invoice_hire_slave1 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='ADD'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									?>
                          			<th>Amount<input type="hidden" name="txtAddID" id="txtAddID" value="<?php echo $num21;?>"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblAdd">
                            <?php if($num2 >0)
							   {$i = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;
								?>
                                <tr id="add_Row_<?php echo $i;?>">
                                   <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeAddRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtAddDes_<?php echo $i;?>" id="txtAddDes_<?php echo $i;?>" rows="2" placeholder="Description..."><?php echo $rows2['DESCRIPTION'];?></textarea></td>
                                   <td><input type="text" name="txtAddDesAmt_<?php echo $i;?>" id="txtAddDesAmt_<?php echo $i;?>" class="form-control"  placeholder="Amount" autocomplete="off" value="<?php echo $rows2['AMOUNT'];?>" onKeyUp="getFullClaculation();"/></td>
                                </tr>
                                <?php }}else{?>
                                <tr id="add_Row_1">
                                   <td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtAddDes_1" id="txtAddDes_1" rows="2" placeholder="Description..."></textarea></td>
                                   <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="left"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewAddRow();">Add</button></td>
									<td>Total</td>
									<td><input type="text" name="txtAddDesTotalAmt" id="txtAddDesTotalAmt" class="form-control" readonly  placeholder="0.00" autocomplete="off" value=""/></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
						
					
				   <div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
							     Less
								</h2>                            
							</div><!-- /.col -->
				    </div>
				
				   <div class="box">
                      <div class="box-body no-padding">
                      <h3>Commission</h3>
                        <table class="table table-striped">
                           <tbody>
							   <tr>
                                    <td><strong>Commission (%)</strong></td>
                                    <?php if($rec>0){?>
										<td><input type="text" name="txtCommission" id="txtCommission" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows['COMISSION_PER'];?>" onKeyUp="getFullClaculation();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtCommissionAmt" id="txtCommissionAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows['COMISSION_PER_AMT'];?>"/></td>
										<?php }else{?>
                                       <td><input type="text" name="txtCommission" id="txtCommission" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM_PERCENT");?>" onKeyUp="getFullClaculation();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtCommissionAmt" id="txtCommissionAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM");?>"/></td>
                                        <?php }?>
								    
                                </tr>
                            </tbody>
                          </table>
                    </div>
					
					<div class="box-body no-padding">
                      <h3>Brokerage</h3>
                        <table class="table table-striped">
                           <tbody>
							
                               <tr>
                                    <td><strong>Brokerage (%)</strong></td>
                                    <?php if($rec>0){?>
										<td><input type="text" name="txtBrokerage" id="txtBrokerage" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows['BROKERAGE_PER'];?>" onKeyUp="getFullClaculation();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtBrokerageAmt" id="txtBrokerageAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows['BROKERAGE_PER_AMT'];?>"/></td>
										<?php }else{?>
                                       <td><input type="text" name="txtBrokerage" id="txtBrokerage" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AGC_PERCENT");?>" onKeyUp="getFullClaculation();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtBrokerageAmt" id="txtBrokerageAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AGC_USD");?>"/></td>
                                        <?php }?>
								    
                                </tr>
                            </tbody>
                          </table>
                    </div>
                  </div>
				  
				  
				  
				  <!--<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
							     Cost Of Bunker On Redelivery
								</h2>                            
							</div> /.col 
				    </div>-->
						<?php
					 $numgrade = count($bunkergrade);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Cost Of Bunker On ReDelivery</h3>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php 
						      $sql_check = "select * from invoice_hire_slave2 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='ONREDEL'";
						      $res_check2 = mysql_query($sql_check);
							  $rows_check2 = mysql_fetch_assoc($res_check2);
							  if($rows_check2>0)
							  {
						       for($i=1;$i<=$numgrade;$i++)
					             {
								$sql_inv2 = "select * from invoice_hire_slave2 where INVOICEID='".$rows['INVOICEID']."' and BUNKERID='".$i."' and IDENTIFY='ONREDEL'"; 
								$res_inv2 = mysql_query($sql_inv2);
								$rows_inv2 = mysql_fetch_assoc($res_inv2);?>
                                  <tr>
                                    <td><?php echo $bunkergrade1[$i];?><input type="hidden" id="txtOnReDelBunkerID_<?php echo $i;?>" name="txtOnReDelBunkerID_<?php echo $i;?>" value="<?php echo $i;?>" /></td>
								    <td><input type="text" name="txtOnReDelConspQtyMT_<?php echo $i;?>" id="txtOnReDelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows_inv2['BUNKER_QTY'];?>" onKeyUp="getOnReDelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOnReDelPrice_<?php echo $i;?>" id="txtOnReDelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows_inv2['BUNKER_PRICE'];?>" onKeyUp="getOnReDelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOnReDelConspAmt_<?php echo $i;?>" id="txtOnReDelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows_inv2['BUNKER_AMT'];?>"/></td>
									<td><input type="text" name="txtOnReDelConspPer_<?php echo $i;?>" id="txtOnReDelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows_inv2['BUNKER_PER'];?>" onKeyUp="getOnReDelBunkerGradeAmt();"/><input type="Hidden" name="txtOnReDelConspPerAmt_<?php echo $i;?>" id="txtOnReDelConspPerAmt_<?php echo $i;?>" value="<?php echo $rows_inv2['BUNKER_PER_AMT'];?>"/></td>
                                </tr>
                                <?php }}else{
								$sql4 = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
								$res4 = mysql_query($sql4);
								$rec4 = mysql_num_rows($res4);
								$numgrade = $rec4;$i = 1;
                                while($rows4 = mysql_fetch_assoc($res4))
								{?>
                                <tr>
                                    <td><?php echo $rows4['NAME'];?><input type="hidden" id="txtOnReDelBunkerID_<?php echo $i;?>" name="txtOnReDelBunkerID_<?php echo $i;?>" value="<?php echo $i;?>" /></td>
								    <td><input type="text" name="txtOnReDelConspQtyMT_<?php echo $i;?>" id="txtOnReDelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$rows4['BUNKERGRADEID'],"RE_DEL_MT");?>" onKeyUp="getOnReDelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOnReDelPrice_<?php echo $i;?>" id="txtOnReDelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$rows4['BUNKERGRADEID'],"RE_DEL_PRICE");?>" onKeyUp="getOnReDelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOnReDelConspAmt_<?php echo $i;?>" id="txtOnReDelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$rows4['BUNKERGRADEID'],"RE_DEL_COST");?>"/></td>
									<td><input type="text" name="txtOnReDelConspPer_<?php echo $i;?>" id="txtOnReDelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="100" onKeyUp="getOnReDelBunkerGradeAmt();"/><input type="Hidden" name="txtOnReDelConspPerAmt_<?php echo $i;?>" id="txtOnReDelConspPerAmt_<?php echo $i;?>" value="<?php echo $obj->getBunkerValuesEstimate($obj->getFun1(),$rows4['BUNKERGRADEID'],"RE_DEL_COST");?>"/></td>
                                </tr>
                                <?php $i++;}}?>
								<tr>
                                    <td colspan="4">Total</td>
									<td><input type="text" name="txtOnReDelConspTotalAmt" id="txtOnReDelConspTotalAmt" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows['ONREDEL_BUNKER_COST'];?>" readonly/><input type="hidden" name="txtOnReDelbid" id="txtOnReDelbid" value="<?php echo $numgrade;?>" /></td>
                                </tr>
                            </tbody>
                          </table>
                    </div>
                  </div>
				  
				  		
				<div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Description</th>
                                     <?php $sql2 = "select * from invoice_hire_slave1 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='SUB'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									?>
                          			<th>Amount<input type="hidden" name="txtSubID" id="txtSubID" value="<?php echo $num21;?>"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblSub">
                            <?php if($num2 >0)
							   {$i = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;
								?>
                                <tr id="Sub_Row_<?php echo $i;?>">
                                   <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeSubRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtSubDes_<?php echo $i;?>" id="txtSubDes_<?php echo $i;?>" rows="2" placeholder="Description..."><?php echo $rows2['DESCRIPTION'];?></textarea></td>
                                   <td><input type="text" name="txtSubDesAmt_<?php echo $i;?>" id="txtSubDesAmt_<?php echo $i;?>" class="form-control"  placeholder="Amount" autocomplete="off" value="<?php echo $rows2['AMOUNT'];?>" onKeyUp="getFullClaculation();"/></td>
                                </tr>
                                <?php }}else{?>
                                <tr id="Sub_Row_1">
                                   <td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtSubDes_1" id="txtSubDes_1" rows="2" placeholder="Description..."></textarea></td>
                                   <td><input type="text" name="txtSubDesAmt_1" id="txtSubDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="left"><button type="button" class="btn btn-primary btn-flat" onClick="SubNewSubRow();">Add</button></td>
									<td>Total</td>
									<td><input type="text" name="txtSubDesTotalAmt" id="txtSubDesTotalAmt" class="form-control" readonly  placeholder="0.00" autocomplete="off" value=""/></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
						
					<div class="box">
							<div class="box-header">
							</div><!-- /.box-header -->
							
						</div>
                    <?php 
							$sql4 = "select * from invoice_hire_master where COMID='".$comid."' and STATUS=2";
	                        $res4 = mysql_query($sql4);
							$num4 = mysql_num_rows($res4);
							if($num4>0)
							{?>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                   <tbody>
							   <?php  $i=0;
								while($rows4 = mysql_fetch_assoc($res4))
								{$i++;
								?>
                                    <tr>
                                        <td width="30%"><?php echo $rows4['INVOICE_NO'];?></td>
                                        <td width="30%"></td>
                                        <td width="30%"><input type="text" autocomplete="off" name="txtPrevInvoiceAmt_<?php echo $i;?>" id="txtPrevInvoiceAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" readonly value="<?php echo $rows4['BALANCE_TO_OWNER'];?>"/></td>
                                    </tr>
                         <?php }  ?>    
                               </tbody>
                             </table>
                         </div>
                     <?php }?>
                    	
				  
				  <div class="box-body no-padding">
                        <table class="table table-striped">
                           <tbody>
							
                               <tr>
                                    <td> <h3>Balance To Supplier</h3></td>
                                    <td></td>
                                    <td><input type="text" name="txtFinalAmt" id="txtFinalAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value=""/></td>
                                </tr>
                            </tbody>
                          </table>
                    </div>
				       
                       <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
							  Banking Details
								<address>
                                <?php if($rec==0)
								{
									
									?>
                                 <textarea class="form-control areasize" name="txtBankingDetails" id="txtBankingDetails" rows="2" placeholder="Banking Details..."><?php echo $obj->getVendorListNewData($vendorid,"BANKING_DETAILS");?></textarea>
                                 <?php }else{?>
                                 <textarea class="form-control areasize" name="txtBankingDetails" id="txtBankingDetails" rows="2" placeholder="Banking Details..."><?php echo $rows['BANKINGDATAILS'];?></textarea>
                                 <?php }?>
								</address>
							</div><!-- /.col -->
						</div>
                       
						
						
                       <div class="box-footer" align="right">
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<button class="btn btn-primary btn-flat" type="submit" onClick="return getValidate(1);">Submit to Edit</button>
							<button class="btn btn-primary btn-flat" type="submit" onClick="return getValidate(2);">Submit to Close</button>
							<input type="hidden" name="action" id="action" value="submit" />
						    <input type="hidden" name="txtDEL_ID" id="txtDEL_ID" value="0"/>
				        </div>
												
					</form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>