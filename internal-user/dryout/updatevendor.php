<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->UpdateVendorMasterRecords();
	header('Location:./vendor_list.php?msg='.$msg);
 }
$obj->viewVendorMasterRecords($id);
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Vendor</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="allPdf.php?id=69&vendorid=<?php echo $id;?>" title="Pdf"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="vendor_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             UPDATE VENDOR
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                    
                    <div class="col-sm-12 invoice-col">
                           Vendor Type
                            <address>
                               <select name="selVType" id="selVType" class="form-control" disabled>
                               <?php 
							   $obj->getVendorTypeList();
							   ?>
                               </select>
                               
                            </address>
                        </div><!-- /.col -->
						
						<div class="col-sm-12 invoice-col">
                           Vendor Name
                            <address>
                               <input type="text" name="txtVName" id="txtVName" class="form-control" placeholder="Vendor Name" autocomplete="off" value="<?php echo $obj->getFun2();?>">
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Vendor Code
                            <address>
                               <input type="text" name="txtVCode" id="txtVCode" class="form-control" placeholder="Vendor Code" autocomplete="off" value="<?php echo $obj->getFun4();?>" readonly >
                               
                            </address>
                        </div><!-- /.col -->
                        
                       <div class="col-sm-12 invoice-col">
                           Vendor Short Name
                            <address>
                               <input type="text" name="txtVSName" id="txtVSName" class="form-control" placeholder="Vendor Short Name" autocomplete="off" value="<?php echo $obj->getFun3();?>" >
                            </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-12 invoice-col">
                           VAT/Tax Number
                            <address>
                               <input type="text" name="txtVatNumber" id="txtVatNumber" class="form-control" placeholder="VAT/Tax Number" autocomplete="off" value="<?php echo $obj->getFun22();?>" >
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-12 invoice-col">
                          Address 1
                            <address>
                               <textarea class="form-control areasize" name="txtAddress1" id="txtAddress1" rows="3" placeholder="Address" ><?php echo $obj->getFun11();?></textarea>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-12 invoice-col">
                          Person Incharge
                            <address>
                               <textarea class="form-control areasize" name="txtAddress2" id="txtAddress2" rows="3" placeholder="Person Incharge" ><?php echo $obj->getFun12();?></textarea>
                            </address>
                        </div><!-- /.col -->
                        
                        
                        <div class="col-sm-12 invoice-col">
                           City
                            <address>
                               <input type="text" name="txtCity" id="txtCity" class="form-control" placeholder="City" autocomplete="off" value="<?php echo $obj->getFun16();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Country
                            <address>
                               <input type="text" name="txtCountry" id="txtCountry" class="form-control" placeholder="Country" autocomplete="off" value="<?php echo $obj->getFun6();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Pin Code
                            <address>
                               <input type="text" name="txtPinCode" id="txtPinCode" class="form-control" placeholder="Pin Code" autocomplete="off" value="<?php echo $obj->getFun18();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                         <div class="col-sm-12 invoice-col">
                           Phone No.
                            <address>
                               <input type="text" name="txtPhoneNo" id="txtPhoneNo" class="form-control" placeholder="Phone No." autocomplete="off" value="<?php echo $obj->getFun19();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Fax No.
                            <address>
                               <input type="text" name="txtFax" id="txtFax" class="form-control" placeholder="Fax No." autocomplete="off" value="<?php echo $obj->getFun20();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Email Address
                            <address>
                               <input type="text" name="txtEmail" id="txtEmail" class="form-control" placeholder="Email Address" autocomplete="off" value="<?php echo $obj->getFun21();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                          Banking Details
                            <address>
                               <textarea class="form-control areasize" name="txtBankingDetails" id="txtBankingDetails" rows="3" placeholder="Banking Details..." ><?php echo $obj->getFun8();?></textarea>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-12 invoice-col">
                         Invoice/Payment Footer Address
                            <address>
                               <textarea class="form-control areasize" name="txtFooterDetails" id="txtFooterDetails" rows="3" placeholder="Footer Address" ><?php echo $obj->getFun23();?></textarea>
                            </address>
                        </div><!-- /.col -->
						
					</div>
                    <div class="box-footer" align="right">
                        <button type="submit" class="btn btn-primary btn-flat" >Submit</button>
                        <input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
                    </div>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#selVType").val(<?php echo $obj->getFun1();?>);

$("#frm1").validate({
	rules: {
		selVType:{required: true}
		},
	messages: {	
		selVType:"*"
		},
	submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
});

</script>
    </body>
</html>