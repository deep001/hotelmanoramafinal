<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	if($_REQUEST['txtStatus'] == "")
	{
		if($_REQUEST['txtComid2'] == "")
		{
			$msg = $obj->insertActualCostSheetNametc();
		}
		else
		{
			$msg = $obj->deleteInOpsEntrytc($_REQUEST['txtComid2']);
		}
		header('Location:./in_ops_tc.php?msg=3');
	}
	else if($_REQUEST['txtStatus'] == "1")
	{
		$msg = $obj->convertToPostOpstc($_REQUEST['txtComid2']);
	}
	else if($_REQUEST['txtStatus'] == "2")
	{
		
	}
	
	header('Location:./in_ops_tc.php?msg=3');
 }
 


if (isset($_REQUEST['selYear']) && $_REQUEST['selYear']!="")
{
	$selYear = $_REQUEST['selYear'];
}
else
{
	$selYear = date('Y',time());
}
$pagename = basename($_SERVER['PHP_SELF']);

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rights = $obj->getUserRights($uid,$moduleid,4); 

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(21); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops TC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops TC&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance TC</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> In Ops at a glance added/updated successfully.
				</div>
				<?php }?>
                <?php if($msg == 4){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> new sheet added successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> In Ops at a glance sheet successfully create.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }?>
				<?php if($msg == 6){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Nomination sent to "Post Ops".
				</div>
				<?php }}?>
				<div class="box box-primary">
					<h3 style=" text-align:center;">In Ops at a glance - TC</h3>
							
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                            <div class="col-xs-10">
								                          
							</div><!-- /.col -->
						    <div class="col-xs-2">
								  <select name="selYear" id="selYear" class="form-control" onChange="getSubmit();">
                                   <?php echo $obj->getYearListTC($selYear);?>
                                  </select>                      
							</div><!-- /.col -->
						<div style="height:10px;">
							<input type="hidden" name="txtComid2" id="txtComid2" value="" />
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<input type="hidden" name="txtFileName" id="txtFileName" value="" />
							<input type="hidden" name="action" id="action" value="submit"/>
						</div>
                
						<div class="box-body table-responsive" style="overflow:auto;">
							<table class="table table-bordered table-striped" id='in_ops_at_glance'>
								<thead>
									<tr valign="top">
										<th align="left">Final TC Est/Docs</th>
										<th align="left">Nom ID/TC No.</th>
										<th align="left">Business Type</th>
                                        <th align="left">Vessel</th>
                                        <th align="left">CP Date</th>
										<th align="left">Port Del/Port Re-Del</th>
                                        <th align="left">Checklist</th>
										<th align="left">TC days/ Fixture Note</th>
										<th align="left">TC Financials</th>
										<th align="left">Agency Letters</th>
                                        <th align="left">Payment/Invoices</th>
										<th align="left">De-activate/Compare</th>
                                        <th align="left">Re-Del Date</th>
                                        <th align="left">Change TC Status</th>	
									</tr>
								</thead>
									<?php
									$sql = "select chartering_estimate_tc_compare.COMID as COMID, chartering_estimate_tc_master.VESSEL_IMO_ID as VESSEL_IMO_ID, chartering_estimate_tc_master.TCOUTID as TCOUTID, chartering_estimate_tc_master.TC_NO as TC_NO, chartering_estimate_tc_compare.MESSAGE as MESSAGE, chartering_estimate_tc_master.FIXTURE_TYPE as FIXTURE_TYPE, chartering_estimate_tc_master.RE_DEL_DATE as RE_DEL_DATE,(select SUM(UTILISATION_DAY_EST) as UTILISATION_DAY_EST from chartering_tc_estimate_slave1 where chartering_tc_estimate_slave1.TCOUTID= (select TCOUTID from chartering_estimate_tc_master where chartering_estimate_tc_master.COMID=chartering_estimate_tc_compare.COMID order by TCOUTID desc limit 0,1)) as UTILISATION_DAY_EST, chartering_estimate_tc_master.TC_NO as VOYAGE_NAME from chartering_estimate_tc_compare inner join chartering_estimate_tc_master on chartering_estimate_tc_master.TCOUTID= chartering_estimate_tc_compare.TCOUTID WHERE chartering_estimate_tc_compare.MODULEID='".$_SESSION['moduleid']."' AND chartering_estimate_tc_compare.MCOMPANYID='".$_SESSION['company']."' and chartering_estimate_tc_compare.FINAL_ID!='' and chartering_estimate_tc_master.FIXED=1 and chartering_estimate_tc_compare.STATUS=1 and year(chartering_estimate_tc_master.UPDATE_ON_DATE)='".$selYear."' order by date(chartering_estimate_tc_master.FINAL_DATETIME) desc"; 
                                    $res = mysql_query($sql) or die($sql);
									$rec = mysql_num_rows($res);
									$i=1;
									$submit = 0;
										if($rec == 0)
										{
											echo '<tbody>';
									
											echo '</tbody>';
										}else{
									?>
								<tbody>
									<?php
										while($rows = mysql_fetch_assoc($res))
										{
											$ESTIMATE_TYPE = $obj->getCharteringEstimateTCData($rows['COMID'],"ESTIMATE_TYPE");											
											$sql2 = "select TCOUTID, CP_DATE1 from chartering_estimate_tc_master where COMID='".$rows['COMID']."' order by TCOUTID asc";
											$res2 = mysql_query($sql2);
											$row2 = mysql_fetch_assoc($res2);
										?>						
										<tr id="in_row_<?php echo $i;?>" style="font-size:11px;">
											<td>
												<a href="viewtcestimate.php?id=<?php echo $row2['TCOUTID'];?>&rttype=1" style="color:green;" title="Click me" >TC</a><br/><br/>
												<a href="documents_tc.php?comid=<?php echo $rows['COMID'];?>&page=1" style="color:green;" title="Click me" >Docs</a>
											</td>
                                           <td><?php echo $rows['MESSAGE']."<br/><br/><span style='color:red;'>".$rows['TC_NO'];?></span></td>
											<td><?php echo $obj->getEstimateList($ESTIMATE_TYPE);?></td>
											<td><?php echo $obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME");?>/<br/> <?php echo $obj->getCharteringEstimateTCData($rows['COMID'],"VESSEL_TYPE");?></td>
                                            <td><?php if($row2['CP_DATE1']=="" || $row2['CP_DATE1']=="0000-00-00"){$CP_DATE1 = "";}else{$CP_DATE1 = date('d-m-Y',strtotime($row2['CP_DATE1']));} echo $CP_DATE1;?></td>
                                            <td><?php echo $obj->getCharteringEstimateTCData($rows['COMID'],"DEL_RANGE_PORT");?>/<?php echo $obj->getCharteringEstimateTCData($rows['COMID'],"RE_DEL_RANGE"); ?></td>
                                            <td><a href="tc_check_list.php?comid=<?php echo $rows['COMID'];?>&page=1" style="color:red;" title="Click me" >Check List</a></td>
                                            <td><?php echo $rows['UTILISATION_DAY_EST']?> <br/><br/><a href="viewtcfixturenote.php?comid=<?php echo $rows['COMID'];?>&page=1" style="color:green;" title="Click me" >Fixture Note</a></td>
                                            <td style="text-align:center;">
											<?php
													$sql1 = "select * from cost_sheettc_name_master where COMID='".$rows['COMID']."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";

													$res1 = mysql_query($sql1);
													$rec1 = mysql_num_rows($res1);
													if($rec1 > 0)
													{
														while($rows1 = mysql_fetch_assoc($res1))
														{
															if($rows1['PROCESS'] != "EST")
															{
																$href = "./cost_sheet_tc.php?comid=".$rows['COMID']."&cost_sheet_id=".$rows1['COST_SHEETID']."&page=1";
																echo "<a href='".$href."'>".str_replace(' ','&nbsp;',$rows1['SHEET_NAME'])."</a><br/>";
																$curr_cst = $rows1['COST_SHEETID'];
															}
															else
															{
																$curr_cst = $obj->getCostSheetFixtureID($rows['COMID'],"SHEET_NO");
															}
														}
													}	
											      
												   $fcaid = $obj->getLatestCostSheetIDTC($rows['COMID']);	
												   $lastsheet = $obj->getLatestSheetIDTC($rows['COMID']);	
												   if($lastsheet==0)
												   {?>
                                                   <button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-modal1" type="button" title="Add New CS" onClick="openWin1(<?php echo $rows['COMID'];?>);">A</button>
                                                   <?php }else{
											       if($obj->getEstimateDatatc($fcaid,$lastsheet)==1)
													{
													?>
													<br/><button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-modal1" type="button" title="Add New CS" onClick="openWin1(<?php echo $rows['COMID'];?>);">A</button>
													<?php }else{?>
                                                    <br/><button class="btn btn-primary btn-flat" type="button" title="Add New CS" onClick="msg();">A</button>
                                                    <?php }}?>
											</td>
                                            <td><a href="agency_letter_inops_tc.php?comid=<?php echo $rows['COMID'];?>&tab=1&page=1">Generate Agency Letter</a></td>
                                            <td>
											<i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp;<a href="payment_grid_tc.php?comid=<?php echo $rows['COMID'];?>&tab=0&page=1" style="color:#005A86;"><strong>View</strong></i></a><br/>
											</td>
											<td><a href="#A" title="Deactivate entry" onClick="getValidate2(<?php echo $rows['COMID'];?>,<?php echo $i;?>);">
													<i class="fa fa-times" style="color:red;"></i>
												</a><br/>
											<button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-modal2" type="button" title="Compare Sheets" onClick="getCompareSheetData(<?php echo $rows['COMID'];?>);">Compare</button>
											</td>
											<td><?php if($rows['RE_DEL_DATE']=="" || $rows['RE_DEL_DATE']=="0000-00-00"){$RE_DEL_DATE = "";}else{$RE_DEL_DATE = date('d-m-Y',strtotime($rows['RE_DEL_DATE']));} echo $RE_DEL_DATE;?></td>
                                            <td>
											<button class="btn btn-primary btn-flat" id="inner-login-button"  type="button" title="Push to Post Ops" onClick="getValidate3(<?php echo $rows['COMID'];?>);">Post Ops</button>
											</td>
										</tr>	                                        
									<?php $i = $i + 1; } }?>
								</tbody>
							</table>
						</div>
					</form>
										
				</div>
				</div>
                <div class="modal fade" id="compose-modal1" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body">
									<div class="box box-primary">
										<div class="box-body no-padding">
											<form name="frm2" id="frm2" method="post" enctype="multipart/form-data" action="<?php echo $pagename;?>">
												<div class="row invoice-info">
													<div class="col-sm-12 invoice-col">
														<strong style="text-align:center;text-decoration:underline;display:block;">INSTRUCTIONS FOR CREATING 1ST TC SHEET</strong>
														<p style="padding:2%;">In the Ops side, the user should click A and enter any desired TC Sheet Name. This is then also possible after every "Submit to Close"</p>
													</div>
												</div>
												
												<div class="row invoice-info" style="padding:2%">
													<div class="col-sm-12 invoice-col">
														TC Sheet Name
														<address>
															<input type="text" name="txtFile" id="txtFile" class="form-control" autocomplete="off" value="<?php echo $obj->getNomDetailsData($comid,"FREIGHT_RATE");?>" placeholder="TC Sheet Name" />
															<input type="hidden" name="txtComid1" id="txtComid1" />
														</address>
													</div><!-- /.col -->
												</div>
												
												<?php if($rights == 1){ ?>
												<div class="row invoice-info">
													<div class="col-sm-12 invoice-col" style="text-align:center;">
														 <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
														 <input type="hidden" name="action" id="action" value="submit" />
													</div><!-- /.col -->
												</div>
												<?php } ?>
											</form>
										</div><!-- /.box-body -->
									</div>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
                    
                    
                <div class="modal fade" id="compose-modal2" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" style="width:98%">
					    <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Compare Sheets</h4>
                            </div>
                            <form name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                            <div class="modal-body" id="div_3" style="overflow:auto;">
                            </div>
                            
                            </form>
					    </div>
				    </div><!-- /.modal-dialog -->
				</div>		
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>

<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.validate.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#frm1").validate({
		rules: {
			
		},
		messages: {
			
		},
		submitHandler: function(form)  {
			jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
			$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
			$("#popup_content").css({"background":"none","text-align":"center"});
			$("#popup_ok,#popup_title").hide();  
			form.submit();
		}
	});
	$("#frm2").validate({
	rules: {
		
	},
	messages: {
		
	},
	submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
	//sortDateFormat();
	$("#in_ops_at_glance").dataTable({
    "iDisplayLength": 100
    });
	
//for uploading........
 $('#photoimg').live('change', function(){ 
	$("#cpDiv_"+$("#txtVar").val()).append("<span id='labelDiv_"+$("#txtVar").val()+"_"+$("#txtID_"+$("#txtVar").val()).val()+"'>abc</span>");
	$("#labelDiv_"+$("#txtVar").val()+"_"+$("#txtID_"+$("#txtVar").val()).val()).html('<img src="../../images/loader.gif" alt="Uploading...." width="20" height="10"  />');
	$("#imageform").ajaxForm({
				target: '#labelDiv_'+$("#txtVar").val()+'_'+$("#txtID_"+$("#txtVar").val()).val()
	}).submit();
	var id = $("#txtID_"+$("#txtVar").val()).val();
	id = (id - 1) + 2;
	$("#txtID_"+$("#txtVar").val()).val(id);
	$.modal.close();
});
//....................
}); 
function getData()
{
	$("#in_ops_at_glance tbody").empty();
	$("#in_ops_at_glance tbody").html('<tr><td height="200" colspan="17" align="center" ><img src="../../img/loading.gif" /></td></tr>');
	$.post("options.php?id=42",{selVName:""+$("#selVName").val()+""}, function(html) {
	$("#in_ops_at_glance tbody").empty();
	$("#in_ops_at_glance tbody").append(html);
	});
}
function openWin1(comid)
{
	$('#basic-modal-content').modal();
	$("#txtComid1").val(comid);
	$("#simplemodal-container").css({"height":"200px"});
}

function getValidate()
{
	if($("#txtFile").val() != "")
	{
		document.frm2.submit();
	}
	else
	{
		jAlert('Please fill the file name', 'Alert');
		return false;
	}
}

function msg()
{
	jAlert('Please make sure the last TC Sheet is Submit to Close', 'Alert');
}

function openWin2(var1,mappinid)
{
	$('#basic-modal-content1').modal();
	$("#txtVar").val(var1);
	$("#txtComid").val(comid);
	$("#simplemodal-container").css({"width":"300px","height":"50px"});
}	

function openWin(comid,vesselid,id)
{
	$('#basic-modal').modal();
	$("#txtComid").val(comid);
	$("#txtVesselid").val(vesselid);
	$("#txtuid").val(id);
	$("#simplemodal-container").css({"width":"300px","height":"50px"});
}

function getValid()
{
	if($('#txtfile').val() == '')
	{
		jAlert('Please choose the file', 'Alert');
		return false;
	}
	else
	{
		var name = $('#txtfile').val();
		name = name.split('.');
		if(name[1] == 'txt')
		{
			return true;
		}
		else
		{
			jAlert('Please choose only .txt file', 'Alert');
			$('#txtfile').val('');
			return false;
		}
	}
}

function getPdfForSea(comid,vesselid)
{
	location.href='allPdf.php?id=12&comid='+comid+'&vesselid='+vesselid;
}

function getValidate3(var1)
{
	jConfirm('Are you sure to send this Nom ID to post ops ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtComid2").val(var1);
			$("#txtStatus").val(1);
			 
			document.frm1.submit();
		}
		else{return false;}
		});	
}

function getValidate4(var1,file_name)
{
	jConfirm('Are you sure to remove this document permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtComid2").val(var1);
			$("#txtStatus").val(2);
			$("#txtFileName").val(file_name);
			document.frm1.submit();
		}
		else{return false;}
		});	
}

function getSubmit()
{ 
    $("#action").val("");
	document.frm1.submit();
}


function getCompareSheetData(comid)
{
	$("#div_3").empty();
	$("#div_3").html('<div style="height:200px;text-align:center;"><img src="../../img/loading.gif" /></div>');
	$.post("options.php?id=55",{comid:""+comid+""}, function(html) {
		$("#div_3").empty();
		$("#div_3").html(html);
	});
}


function getValidate2(var1,rowid)
{
	$("#in_row_"+rowid+" td").css({"background-color":"red"});
	jConfirm('Are you sure to de-activate this Nom ID ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtComid2").val(var1); 
			document.frm1.submit();
		}
		else{$("#in_row_"+rowid+" td").css({"background-color":""});return false;}
		});	
}

</script>
		
</body>
</html>