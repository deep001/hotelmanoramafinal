<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];
if(@$_REQUEST['action'] == 'submit')
{
 	$msg = $obj->updateVesselMainMasterTankers();
	header('Location:./fleet.php?msg='.$msg);
}

$rdoIceClass = $rdoIsStatement = $rdoIsSDWT = $rdoPitch = $rdoIsReported = $rdoIsPolution = $rdoIsGrounding = $rdoIsCasualty = $rdoIsAccident = $rdoOCIMF = $rdoChainStopper = $rdoOCIMFSize = $rdoGasSystem = $rdoBulkhead = $rdoMARPOLAnnex = $rdoCargoTankRestrictions = $rdoCargoControlRoom = $rdoTankInnageCCR = $rdoShipOperateUnderClosed = $rdoOverFillAlarm = $rdoVaporReturnSystem = $rdoOCIMFEdition = $rdoFittedStermManifold = $rdoTankCoiled = $rdoTankCoating1 = $rdoTankCoating2 = $rdoTankCoating3 = $rdoPublications = $rdoOwnerWarrant = $rdoUnderstandEnglish = $rdoICSHelicopter = $rdoSpillResponse = $rdoAgreementUS= $rdoOCIMFICS = 1;
if($obj->getVesselParticularData('RDO_ICE_CLASS','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoIceClass = 1;
}
if($obj->getVesselParticularData('RDO_ICE_CLASS','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoIceClass = 2;
}

if($obj->getVesselParticularData('RDOIS_STATEMENT','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoIsStatement = 1;
}
if($obj->getVesselParticularData('RDOIS_STATEMENT','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoIsStatement = 2;
}

if($obj->getVesselParticularData('RDODEADWEIGHT','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoIsSDWT = 1;
}
if($obj->getVesselParticularData('RDODEADWEIGHT','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoIsSDWT = 2;
}

if($obj->getVesselParticularData('PITCHTYPE','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoPitch = 1;
}
if($obj->getVesselParticularData('PITCHTYPE','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoPitch = 2;
}

if($obj->getVesselParticularData('ISREPORTED','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoIsReported = 1;
}
if($obj->getVesselParticularData('ISREPORTED','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoIsReported = 2;
}


if($obj->getVesselParticularData('RDOPOLUTION','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoIsPolution = 1;
}
if($obj->getVesselParticularData('RDOPOLUTION','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoIsPolution = 2;
}

if($obj->getVesselParticularData('RDOGROUNDING','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoIsGrounding = 1;
}
if($obj->getVesselParticularData('RDOGROUNDING','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoIsGrounding = 2;
}

if($obj->getVesselParticularData('RDOCASUALTY','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoIsCasualty = 1;
}
if($obj->getVesselParticularData('RDOCASUALTY','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoIsCasualty = 2;
}


if($obj->getVesselParticularData('RDOACCIDENT','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoIsAccident = 1;
}
if($obj->getVesselParticularData('RDOACCIDENT','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoIsAccident = 2;
}

if($obj->getVesselParticularData('RDOOCIMF','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoOCIMF = 1;
}
if($obj->getVesselParticularData('RDOOCIMF','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoOCIMF = 2;
}

if($obj->getVesselParticularData('RDOCHAINSTOPPER','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoChainStopper = 1;
}
if($obj->getVesselParticularData('RDOCHAINSTOPPER','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoChainStopper = 2;
}

if($obj->getVesselParticularData('RDOOCIMFSIZE','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoOCIMFSize = 1;
}
if($obj->getVesselParticularData('RDOOCIMFSIZE','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoOCIMFSize = 2;
}

if($obj->getVesselParticularData('RDOGASSYSTEM','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoGasSystem = 1;
}
if($obj->getVesselParticularData('RDOGASSYSTEM','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoGasSystem = 2;
}


if($obj->getVesselParticularData('RDOBULKHEAD','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoBulkhead = 1;
}
if($obj->getVesselParticularData('RDOBULKHEAD','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoBulkhead = 2;
}


if($obj->getVesselParticularData('RDOMARPOLANNEX','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoMARPOLAnnex = 1;
}
if($obj->getVesselParticularData('RDOMARPOLANNEX','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoMARPOLAnnex = 2;
}

if($obj->getVesselParticularData('RDOCARGOTANKRESTRICTIONS','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoCargoTankRestrictions = 1;
}
if($obj->getVesselParticularData('RDOCARGOTANKRESTRICTIONS','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoCargoTankRestrictions = 2;
}


if($obj->getVesselParticularData('RDOCARGOCONTROLROOM','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoCargoControlRoom = 1;
}
if($obj->getVesselParticularData('RDOCARGOCONTROLROOM','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoCargoControlRoom = 2;
}


if($obj->getVesselParticularData('RDOTANKINNAGECCR','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoTankInnageCCR = 1;
}
if($obj->getVesselParticularData('RDOTANKINNAGECCR','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoTankInnageCCR = 2;
}


if($obj->getVesselParticularData('RDOSHIPOPERATEUNDERCLOSED','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoShipOperateUnderClosed = 1;
}
if($obj->getVesselParticularData('RDOSHIPOPERATEUNDERCLOSED','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoShipOperateUnderClosed = 2;
}


if($obj->getVesselParticularData('RDOOVERFILLALARM','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoOverFillAlarm = 1;
}
if($obj->getVesselParticularData('RDOOVERFILLALARM','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoOverFillAlarm = 2;
}


if($obj->getVesselParticularData('RDOVAPORRETURNSYSTEM','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoVaporReturnSystem = 1;
}
if($obj->getVesselParticularData('RDOVAPORRETURNSYSTEM','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoVaporReturnSystem = 2;
}


if($obj->getVesselParticularData('RDOOCIMFEDITION','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoOCIMFEdition = 1;
}
if($obj->getVesselParticularData('RDOOCIMFEDITION','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoOCIMFEdition = 2;
}


if($obj->getVesselParticularData('RDOFITTEDSTERMMANIFOLD','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoFittedStermManifold = 1;
}
if($obj->getVesselParticularData('RDOFITTEDSTERMMANIFOLD','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoFittedStermManifold = 2;
}


if($obj->getVesselParticularData('RDOTANKCOILED','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoTankCoiled = 1;
}
if($obj->getVesselParticularData('RDOTANKCOILED','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoTankCoiled = 2;
}


if($obj->getVesselParticularData('RDOTANKCOATING1','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoTankCoating1 = 1;
}
if($obj->getVesselParticularData('RDOTANKCOATING1','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoTankCoating1 = 2;
}


if($obj->getVesselParticularData('RDOTANKCOATING2','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoTankCoating2 = 1;
}
if($obj->getVesselParticularData('RDOTANKCOATING2','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoTankCoating2 = 2;
}


if($obj->getVesselParticularData('RDOTANKCOATING3','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoTankCoating3 = 1;
}
if($obj->getVesselParticularData('RDOTANKCOATING3','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoTankCoating3 = 2;
}


if($obj->getVesselParticularData('RDOPUBLICATIONS','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoPublications = 1;
}
if($obj->getVesselParticularData('RDOPUBLICATIONS','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoPublications = 2;
}


if($obj->getVesselParticularData('RDOOWNERWARRANT','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoOwnerWarrant = 1;
}
if($obj->getVesselParticularData('RDOOWNERWARRANT','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoOwnerWarrant = 2;
}


if($obj->getVesselParticularData('RDOUNDERSTAND_ENGLISH','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoUnderstandEnglish = 1;
}
if($obj->getVesselParticularData('RDOUNDERSTAND_ENGLISH','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoUnderstandEnglish = 2;
}


if($obj->getVesselParticularData('RDOICSHELICOPTER','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoICSHelicopter = 1;
}
if($obj->getVesselParticularData('RDOICSHELICOPTER','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoICSHelicopter = 2;
}


if($obj->getVesselParticularData('RDOSPILLRESPONSE','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoSpillResponse = 1;
}
if($obj->getVesselParticularData('RDOSPILLRESPONSE','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoSpillResponse = 2;
}


if($obj->getVesselParticularData('RDOAGREEMENTUS','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoAgreementUS = 1;
}
if($obj->getVesselParticularData('RDOAGREEMENTUS','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoAgreementUS = 2;
}


if($obj->getVesselParticularData('RDOOCIMFICS','vessel_master_tankers',$_REQUEST['id'])==1)
{
	$rdoOCIMFICS = 1;
}
if($obj->getVesselParticularData('RDOOCIMFICS','vessel_master_tankers',$_REQUEST['id'])==2)
{
	$rdoOCIMFICS = 2;
}
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-anchor"></i>&nbsp;Fleet &nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Fleet</li>
						<li class="active"><?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME")); ?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="fleet.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
					<div class="row">
						<div class="col-xs-12">
							<h2 style=" text-align:center;">
								Vessel Particulars ( <?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME")); ?> )                              
							</h2>                            
						</div><!-- /.col -->
					</div>
					<div class="row" style="height:20px">&nbsp;</div>	
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								Vessel Description                           
							</h2>                            
						</div><!-- /.col -->
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Vessel's name
							<address>
								<input name="txtVName" type="text" class="form-control" id="txtVName" autocomplete="off" value="<?php echo strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME"));?>" readonly placeholder="Vessel’s Name"/>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							IMO number
							<address>
								<input name="txtIMONumber" readonly type="text" class="form-control" id="txtIMONumber" autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"IMO_NO");?>" placeholder="Vessel’s Previous Name(S)"/>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							Vessel's Previous Name(S)
							<address>
								<input name="txtVPName" type="text" class="form-control" id="txtVPName" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PREVIOUS_NAME','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Vessel’s Previous Name(S)"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Date (S) Of Change
							<address>
								<input name="txtDOC" type="text" class="form-control datepiker" id="txtDOC" autocomplete="off" value="<?php if($obj->checkVesselDate('DATEOFCHANGE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATEOFCHANGE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATEOFCHANGE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" />
							</address>
						</div><!-- /.col -->
                     </div>
                     <div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							 Date delivered
							<address>
								<input name="txtDateDelivered" type="text" class="form-control datepiker" id="txtDateDelivered"  autocomplete="off" value="<?php if($obj->checkVesselDate('DATEOFDELIVERY','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATEOFDELIVERY','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATEOFDELIVERY','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy"/>
								
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Builder (where built)
							<address>
								<input name="txtBuilder" type="text" class="form-control" id="txtBuilder" autocomplete="off" value="<?php echo $obj->getVesselParticularData('BUILDER','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Builder (where built)"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Flag
							<address>
								<select  name="selFlag" class="select form-control" id="selFlag" disabled="disabled">
									<?php $obj->getCountryNameList(); ?>
								</select>
								<script>$('#selFlag').val(<?php echo $obj->getVesselParticularData('FLAG','vessel_imo_master',$_REQUEST['id']);?>);</script>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							Port Of Registry
							<address>
								<select  name="selRegistryPort" class="select form-control" id="selRegistryPort">
									<?php $obj->getPortList(); ?>
								</select>
                                <script>$('#selRegistryPort').val(<?php echo $obj->getVesselParticularData('REGISTRY_PORT','vessel_master_tankers',$_REQUEST['id']);?>);</script>
							</address>
						</div><!-- /.col -->
                     </div>
                     <div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Call sign
							<address>
								<input name="txtCallSign" type="text" class="form-control" id="txtCallSign" autocomplete="off" value="<?php echo $obj->getVesselParticularData('CALL_SIGN','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Call sign"/>
							</address>
						</div><!-- /.col -->
                     </div>
                     <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
							Vessel's satcom phone number
							<address>
								<input name="txtPhoneNo" type="text" class="form-control" id="txtPhoneNo" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PHONE_NO','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Vessel's satcom phone number"/>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							Vessel's fax number
							<address>
								<input name="txtFaxNo" type="text" class="form-control" id="txtFaxNo" autocomplete="off" value="<?php echo $obj->getVesselParticularData('FAXNO','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Vessel's fax number"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Vessel's telex number
							<address>
								<input name="txtTelexNo" type="text" class="form-control" id="txtTelexNo" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TELEX_NO','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Vessel's telex number"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Vessel's email address
							<address>
								<input name="txtEmailAddress" type="text" class="form-control" id="txtEmailAddress" autocomplete="off" value="<?php echo $obj->getVesselParticularData('EMAIL_ADDRESS','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Vessel's email address"/>
							</address>
						</div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Type of vessel
							<address>
								<input name="txtTypeOfVessel" type="text" class="form-control" id="txtTypeOfVessel"  autocomplete="off" value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_TYPE"));?>" readonly placeholder="Type of vessel"/>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							Type of hull
							<address>
								<input name="txtHullType" type="text" class="form-control" id="txtHullType" autocomplete="off" value="<?php echo $obj->getVesselParticularData('HULL_TYPE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Type of hull"/>
							</address>
						</div><!-- /.col -->
						
					</div>
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              Classification
                            </h2>                            
                        </div><!-- /.col -->
                     </div> 
					<div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Classification Society
							<address>
								<select  name="selCLASS_SOC" class="form-control" disabled id="selCLASS_SOC" >
								<?php 
                                $obj->getClaSocList();
                                ?>
                                </select>
                                <script>$('#selCLASS_SOC').val(<?php echo $obj->getVesselIMOData($_REQUEST['id'],"CLA_SOC_ID");?>);</script>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Class notation
							<address>
								<textarea name="txtClassNotation" id="txtClassNotation" class="form-control areasize" placeholder="Class notation"><?php echo $obj->getVesselParticularData('CLASS_NOTATION','vessel_master_tankers',$_REQUEST['id']);?></textarea>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							If Classification society changed, name of previous society: 
							<address>
								<select  name="selPrevCLASS_SOC" class="form-control" id="selPrevCLASS_SOC" >
								<?php 
                                $obj->getClaSocList();
                                ?>
                                </select>
                                <script>$('#selPrevCLASS_SOC').val(<?php echo $obj->getVesselParticularData('PREV_CLA_SOC_ID','vessel_master_tankers',$_REQUEST['id']);?>);</script>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							If Classification society changed, date of change
							<address>
                                <input name="txtClassSOCChnageDate" type="text" class="form-control datepiker" id="txtClassSOCChnageDate"  autocomplete="off" value="<?php if($obj->checkVesselDate('DATEOFCLASSCHANGE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DATEOFCLASSCHANGE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DATEOFCLASSCHANGE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy"/>
							</address>
						</div><!-- /.col -->
                    </div> 
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
							IMO type, if applicable
							<address>
                                <input name="txtIMOType" type="text" class="form-control" id="txtIMOType"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('IMO_TYPE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="IMO type, if applicable"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Does the vessel have ice class ?
							<address>
                                <input type="radio" id="rdoIceClass1" name="rdoIceClass" value="1" <?php if($rdoIceClass == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoIceClass2" name="rdoIceClass" value="2" <?php if($rdoIceClass == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Ice Class
							<address>
                                <input name="txtIceClass" type="text" class="form-control" id="txtIceClass" <?php if($rdoIceClass == 2){echo "readonly";}else{}?> autocomplete="off" value="<?php echo $obj->getVesselParticularData('ICE_CLASS','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Ice Class"/>
							</address>
						</div><!-- /.col -->
					</div>
				
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
							Date of last dry-dock
							<address>
                                <input name="txtDryDockDate" type="text" class="form-control datepiker" id="txtDryDockDate"  autocomplete="off" value="<?php if($obj->checkVesselDate('DRY_DOCK_DATE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DRY_DOCK_DATE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DRY_DOCK_DATE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy"/>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							Place of last dry-dock 
							<address>
								<select  name="selDryDockPort" class="select form-control" id="selDryDockPort">
									<?php $obj->getPortList(); ?>
								</select>
                                <script>$('#selDryDockPort').val(<?php echo $obj->getVesselParticularData('DRY_DOCK_PORT','vessel_master_tankers',$_REQUEST['id']);?>);</script>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Date next dry dock due
							<address>
                                <input name="txtNextDryDockDate" type="text" class="form-control datepiker" id="txtNextDryDockDate"  autocomplete="off" value="<?php if($obj->checkVesselDate('NEXT_DRY_DOCK_DATE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('NEXT_DRY_DOCK_DATE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('NEXT_DRY_DOCK_DATE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy"/>
							</address>
						</div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
							Date of last special survey
							<address>
                                <input name="txtSurveyDate" type="text" class="form-control datepiker" id="txtSurveyDate"  autocomplete="off" value="<?php if($obj->checkVesselDate('SURVEY_DATE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('SURVEY_DATE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('SURVEY_DATE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy"/>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							Due date of next special survey
							<address>
								<input name="txtDueSurveyDate" type="text" class="form-control datepiker" id="txtDueSurveyDate"  autocomplete="off" value="<?php if($obj->checkVesselDate('DUE_SURVEY_DATE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('DUE_SURVEY_DATE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('DUE_SURVEY_DATE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Date of last annual survey
							<address>
								<input name="txtAnnualSurveyDate" type="text" class="form-control datepiker" id="txtAnnualSurveyDate"  autocomplete="off" value="<?php if($obj->checkVesselDate('ANNUAL_SURVEY_DATE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('ANNUAL_SURVEY_DATE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('ANNUAL_SURVEY_DATE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy"/>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							If ship has Condition Assessment Program (CAP), what is the latest overall rating
							<address>
								<input name="txtOverallRating" type="text" class="form-control" id="txtOverallRating"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('OVERALL_RATING','vessel_master_tankers',$_REQUEST['id']); ?>" placeholder="latest overall rating"/>
							</address>
						</div><!-- /.col -->
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Does the vessel have a statement of compliance issued under the provisions of the Condition Assessment Scheme (CAS) ?
If yes, what is the expiry date?
							<address>
                                <input type="radio" id="rdoIsStatement1" name="rdoIsStatement" value="1" <?php if($rdoIsStatement == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoIsStatement2" name="rdoIsStatement" value="2" <?php if($rdoIsStatement == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Expiry date of statement of compliance issued under the provisions of the Condition Assessment Scheme
							<address>
                                <input name="txtExpiryStatDate" type="text" class="form-control datepiker" id="txtExpiryStatDate"  autocomplete="off" value="<?php if($obj->checkVesselDate('EXPIRY_STAT_DATE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('EXPIRY_STAT_DATE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('EXPIRY_STAT_DATE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy" <?php if($rdoIsStatement == 2){echo "disabled";}?>/>
							</address>
						</div><!-- /.col -->
					</div>
				
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              Dimensions
                            </h2>                            
                        </div><!-- /.col -->
                     </div> 
					
					<div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Length Over All (LOA)(M)
							<address>
								<input type="text" class="form-control" name="txtLOA" id="txtLOA"  autocomplete="off" value="<?php echo $obj->getVesselIMOData($_REQUEST['id'],"LOA"); ?>" placeholder="Length Over All (LOA)(M)" readonly />
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Length Between Perpendiculars (LBP)(M)
							<address>
								<input type="text" class="form-control numeric" name="txtLBPLength" id="txtLBPLength"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('LBP_LENGTH','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Length Between Perpendiculars (LBP)(M)"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Extreme breadth (Beam)(M)
							<address>
								<input type="text" class="form-control numeric" name="txtBeam" id="txtBeam"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('BEAM','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Extreme breadth (Beam)(M)"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Moulded depth (M)
							<address>
								<input type="text" class="form-control numeric" name="txtMDepth" id="txtMDepth"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('MODULER_DEPTH','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Moulded depth (M)"/>
							</address>
						</div><!-- /.col -->
					</div>
				
					<div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Keel to Masthead (KTM)(M)
							<address>
								<input type="text" class="form-control numeric" name="txtKeelKTM" id="txtKeelKTM"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('KEEL_KTM','vessel_master_tankers',$_REQUEST['id']); ?>" placeholder="Keel to Masthead (KTM) (M)" />
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							 KTM in collapsed condition (if applicable)(M)
							<address>
								<input type="text" class="form-control numeric" name="txtKtmCollapsed" id="txtKtmCollapsed"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('KTM_COLLAPSED','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="KTM in collapsed condition (if applicable)(M)"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Bow to Center Manifold (BCM)(M)
							<address>
								<input type="text" class="form-control numeric" name="txtBCenterManifold" id="txtBCenterManifold"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('BCENTER_MANIFOLD','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Bow to Center Manifold (BCM)(M)"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							 Stern to Center Manifold (SCM)(M)
							<address>
								<input type="text" class="form-control numeric" name="txtSCenterManifold" id="txtSCenterManifold"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('SCENTER_MANIFOLD','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Stern to Center Manifold (SCM)(M)"/>
							</address>
						</div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Distance bridge front to center of manifold (M)
							<address>
								<input type="text" class="form-control numeric" name="txtFCenterManifold" id="txtFCenterManifold"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('FCENTER_MANIFOLD','vessel_master_tankers',$_REQUEST['id']); ?>" placeholder="Distance bridge front to center of manifold (M)" />
							</address>
						</div>
					</div>
                    
                    
                    <div class="box">
						
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody>
									<tr>
										<th width="25%">Parallel body distances</th>
										<th width="25%">Lightship(M)</th>
										<th width="25%">Normal Ballast(M)</th>
                                        <th width="25%">Summer Dwt(M)</th>
										
									</tr>
									<tr>
										<td>Forward to mid-point manifold</td>
										<td><input name="txtLightship_1" type="text" class="form-control numeric" id="txtLightship_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('LIGHTSHIP_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
										<td><input name="txtBallast_1" type="text" class="form-control numeric" id="txtBallast_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('BALLAST_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
                                        <td><input name="txtSummer_1" type="text" class="form-control numeric" id="txtSummer_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SDRAFT_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
									</tr>
									<tr>
										<td>Aft to mid-point manifold</td>
										<td><input name="txtLightship_2" type="text" class="form-control numeric" id="txtLightship_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('LIGHTSHIP_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
										<td><input name="txtBallast_2" type="text" class="form-control numeric" id="txtBallast_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('BALLAST_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtSummer_2" type="text" class="form-control numeric" id="txtSummer_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SDRAFT_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
									</tr>
									
									<tr>
										<td>Parallel body length</td>
										<td><input name="txtTropicalid_3" type="text" class="form-control numeric" id="txtTropicalid_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('LIGHTSHIP_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
										<td><input name="txtBallast_3" type="text" class="form-control numeric" id="txtBallast_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('BALLAST_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
                                        <td><input name="txtSummer_3" type="text" class="form-control numeric" id="txtSummer_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SDRAFT_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
									</tr>
								</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
				
					<div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							FWA at summer draft(M)
							<address>
								<input type="text" class="form-control numeric" name="txtFWASummer" id="txtFWASummer"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('FWA_SUMMER','vessel_master_tankers',$_REQUEST['id']); ?>" placeholder="FWA at summer draft"/>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							TPC immersion at summer draft(M)
							<address>
								<input type="text" class="form-control numeric" name="txtTPCSummer" id="txtTPCSummer"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('TPC_SUMMER','vessel_master_tankers',$_REQUEST['id']); ?>" placeholder="TPC immersion at summer draft"/>
							</address>
						</div><!-- /.col -->
					</div>
					
					
					<div class="box">
						<div class="box-header">
							<h4 class="box-title">What is the max height of mast above waterline (air draft)</h4>
						</div><!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody><tr>
									<th width="25%">&nbsp;</th>
									<th width="25%"> Full Mast(M)</th>
									<th width="25%"> Collapsed Mast(M)</th>
								</tr>
								<tr>
									<td>Lightship</td>
									<td><input name="txtFULL_MAST_1" type="text" class="form-control numeric" id="txtFULL_MAST_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('FULL_MAST_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtCollapsed_mast_1" type="text" class="form-control numeric" id="txtCollapsed_mast_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('COLLAPSED_MAST_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
								</tr>
								<tr>
									<td>Normal ballast</td>
									<td><input name="txtFULL_MAST_2" type="text" class="form-control numeric" id="txtFULL_MAST_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('FULL_MAST_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtCollapsed_mast_2" type="text" class="form-control numeric" id="txtCollapsed_mast_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('COLLAPSED_MAST_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
								</tr>
								<tr>
									<td>At loaded summer deadweight</td>
									<td><input name="txtFULL_MAST_3" type="text" class="form-control numeric" id="txtFULL_MAST_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('FULL_MAST_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtCollapsed_mast_3" type="text" class="form-control numeric" id="txtCollapsed_mast_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('COLLAPSED_MAST_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
								</tr>
							</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
				    
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              Tonnages
                            </h2>                            
                        </div><!-- /.col -->
                     </div> 
					
					<div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Net Tonnage
							<address>
								<input type="text" class="form-control numeric" name="txtNET_TONNAGE" id="txtNET_TONNAGE"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('NET_TONNAGE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Net Tonnage" />
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Gross Tonnage
							<address>
								<input type="text" class="form-control numeric" name="txtGROSS_TONNAGE" id="txtGROSS_TONNAGE"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('GROSS_TONNAGE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Gross Tonnage"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Reduced Gross Tonnage (if applicable)
							<address>
								<input type="text" class="form-control numeric" name="txtREGROSS_TONNAGE" id="txtREGROSS_TONNAGE"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('REGROSS_TONNAGE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Reduced Gross Tonnage (if applicable)"/>
							</address>
						</div><!-- /.col -->
                        
					</div>
                    
                    <div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Suez Canal Tonnage - Gross (SCGT)
							<address>
								<input type="text" class="form-control numeric" name="txtCANAL_TONNAGE" id="txtCANAL_TONNAGE"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CANAL_TONNAGE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Suez Canal Tonnage - Gross (SCGT)" />
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Net (SCNT)
							<address>
								<input type="text" class="form-control numeric" name="txtNet_SCNT" id="txtNet_SCNT"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('NET_SCNT','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Net (SCNT)"/>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							Panama Canal Net Tonnage (PCNT)
							<address>
								<input type="text" class="form-control numeric" name="txtPanamaTonnage" id="txtPanamaTonnage"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('PCN_TONNAGE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Panama Canal Net Tonnage (PCNT)"/>
							</address>
						</div><!-- /.col -->
					</div>
                    
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              Loadline Information
                            </h2>                            
                        </div><!-- /.col -->
                     </div> 
                     
                    <div class="box">
						<div class="box-header">
							<h4 class="box-title">Loadline</h4>
						</div><!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-striped">
								<tbody><tr>
									<th width="20%">&nbsp;</th>
									<th width="20%"> Freeboard(M)</th>
									<th width="20%"> Draft(M)</th>
                                    <th width="20%"> Deadweight(MT)</th>
                                    <th width="20%"> Displacement(MT)</th>
								</tr>
								<tr>
									<td>Summer</td>
									<td><input name="txtFreeboard_1" type="text" class="form-control numeric" id="txtFreeboard_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('FREEBOARD_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtDraft_1" type="text" class="form-control numeric" id="txtDraft_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DRAFT_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
                                    <td><input name="txtDeadWeight_1" type="text" class="form-control numeric" id="txtDeadWeight_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DEADWEIGHT_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtDisplacement_1" type="text" class="form-control numeric" id="txtDisplacement_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DISPLACEMENT_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
								</tr>
								<tr>
									<td>Winter</td>
									<td><input name="txtFreeboard_2" type="text" class="form-control numeric" id="txtFreeboard_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('FREEBOARD_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtDraft_2" type="text" class="form-control numeric" id="txtDraft_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DRAFT_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
                                    <td><input name="txtDeadWeight_2" type="text" class="form-control numeric" id="txtDeadWeight_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DEADWEIGHT_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtDisplacement_2" type="text" class="form-control numeric" id="txtDisplacement_2" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DISPLACEMENT_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
								</tr>
								<tr>
									<td>Tropical</td>
									<td><input name="txtFreeboard_3" type="text" class="form-control numeric" id="txtFreeboard_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('FREEBOARD_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtDraft_3" type="text" class="form-control numeric" id="txtDraft_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DRAFT_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
                                    <td><input name="txtDeadWeight_3" type="text" class="form-control numeric" id="txtDeadWeight_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DEADWEIGHT_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtDisplacement_3" type="text" class="form-control numeric" id="txtDisplacement_3" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DISPLACEMENT_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
								</tr>
                                <tr>
									<td>Lightship</td>
									<td><input name="txtFreeboard_4" type="text" class="form-control numeric" id="txtFreeboard_4" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('FREEBOARD_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtDraft_4" type="text" class="form-control numeric" id="txtDraft_4" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DRAFT_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
                                    <td><input name="txtDeadWeight_4" type="text" class="form-control numeric" id="txtDeadWeight_4" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DEADWEIGHT_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtDisplacement_4" type="text" class="form-control numeric" id="txtDisplacement_4" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DISPLACEMENT_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
								</tr>
                                <tr>
									<td>Normal Ballast Condition</td>
									<td><input name="txtFreeboard_5" type="text" class="form-control numeric" id="txtFreeboard_5" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('FREEBOARD_5','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtDraft_5" type="text" class="form-control numeric" id="txtDraft_5" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DRAFT_5','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
                                    <td><input name="txtDeadWeight_5" type="text" class="form-control numeric" id="txtDeadWeight_5" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DEADWEIGHT_5','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
									<td><input name="txtDisplacement_5" type="text" class="form-control numeric" id="txtDisplacement_5" size="5" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DISPLACEMENT_5','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
								</tr>
							</tbody>
							</table>
						</div><!-- /.box-body -->
					</div>
                    
                    
                    <div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							Does vessel have multiple SDWT?
							<address>
                                <input type="radio" id="rdoIsSDWT1" name="rdoIsSDWT" value="1" <?php if($rdoIsSDWT == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoIsSDWT2" name="rdoIsSDWT" value="2" <?php if($rdoIsSDWT == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							If yes, what is the maximum assigned deadweight(MT)?
							<address>
                                <input name="txtASSDEADWEIGHT" type="text" class="form-control numeric" id="txtASSDEADWEIGHT"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('ASSDEADWEIGHT','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" <?php if($rdoIsSDWT == 2){echo "readonly";}?>/>
							</address>
						</div><!-- /.col -->
					</div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             Ownership and Operation
                            </h2>                            
                        </div><!-- /.col -->
                     </div> 
					
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							Registered owner - Full style
							<address>
								<textarea name="txtRegisteredOwner" id="txtRegisteredOwner" class="form-control areasize" ><?php echo $obj->getVesselParticularData('REGISTEREDOWNER','vessel_master_tankers',$_REQUEST['id']);?></textarea>
							</address>
						</div><!-- /.col -->
						
						<div class="col-sm-6 invoice-col">
							Technical operator - Full style
							<address>
								<textarea name="txtTechnicalOperator" id="txtTechnicalOperator" class="form-control areasize" ><?php echo $obj->getVesselParticularData('TECHNICALOPERATOR','vessel_master_tankers',$_REQUEST['id']);?></textarea>
							</address>
						</div><!-- /.col -->
                     </div> 
                     <div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							Commercial operator - Full style
							<address>
								<textarea name="txtCommercialOperator" id="txtCommercialOperator" class="form-control areasize" ><?php echo $obj->getVesselParticularData('COMMERCIALOPERATOR','vessel_master_tankers',$_REQUEST['id']);?></textarea>
							</address>
						</div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
							Disponent owner - Full style
							<address>
								<textarea name="txtDisponentOwner" id="txtDisponentOwner" class="form-control areasize" ><?php echo $obj->getVesselParticularData('DISPONENTOWNER','vessel_master_tankers',$_REQUEST['id']);?></textarea>
							</address>
						</div><!-- /.col -->
					</div>
				
				
				<!----------------------------------------------------------------------------------------------------------->
				<!------------------------------------------End Main Form---------------------------------------------------->
				<!----------------------------------------------------------------------------------------------------------->
				
				<div class="row" style="height:20px"></div>
				<div class="row invoice-info">
					<div class="col-md-12">
						<!-- Custom Tabs -->
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#tab_1">CERTIFICATION</a></li>
								<li class=""><a data-toggle="tab" href="#tab_2">CREW MANAGEMENT</a></li>
								<li class=""><a data-toggle="tab" href="#tab_3">HELICOPTERS</a></li>
								<li class=""><a data-toggle="tab" href="#tab_4">FOR USA CALLS</a></li>
								<li class=""><a data-toggle="tab" href="#tab_5">CARGO AND BALLAST HANDLING</a></li>
								<li class=""><a data-toggle="tab" href="#tab_6">INERT GAS AND CRUDE OIL WASHING</a></li>
								<li class=""><a data-toggle="tab" href="#tab_7">MOORING</a></li>
								<li class=""><a data-toggle="tab" href="#tab_8">MISCELLANEOUS</a></li>
							</ul>
                            <div class="tab-content">
								<div id="tab_1" class="tab-pane active">
									<div class="box-body no-padding">
										<table class="table table-striped">
											<tbody id="tb2">
												<tr>
													<th width="20%">Certificate Name</th>
													<th width="20%">Date Of Issue</th>
													<th width="20%">Date Of Last Annual Endorsement</th>
													<th width="20%">Date Of Expiry</th>
													<th width="15%">Upload</th>
												</tr>
												
												<?php
												$sql = "select * from vessel_master_tankers_slave where VESSEL_IMO_ID=".$_REQUEST['id'];
												$res = mysql_query($sql);
												$rec = mysql_num_rows($res);
												
												if($rec == 0)
												{
												?>
												<tr>
													<td>
														<select  name="selCert_1" class="select form-control" id="selCert_1">
															<?php $obj->getCertificateList(); ?>
														</select>
													</td>
													<td>
														<input name="txtIDate_1" type="text" class="form-control datepiker" id="txtIDate_1" autocomplete="off" placeholder="dd-mm-yyyy"/>
													</td>
													<td>
														<input name="txtLAIDate_1" type="text" class="form-control datepiker" id="txtLAIDate_1" autocomplete="off" placeholder="dd-mm-yyyy"/>
													</td>
													<td>
														<input name="txtEDate_1" type="text" class="form-control datepiker" id="txtEDate_1" autocomplete="off" placeholder="dd-mm-yyyy"/>
													</td>
													<td>
														<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip">
                                                            <i class="fa fa-paperclip"></i> Attachment
                                                            <input type="file" class="form-control" multiple name="attach_file_1[]" id="attach_file_1" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                                        </div>										
													</td>
												</tr>
												<?php }else{ ?>
												<?php 
													$i =0;
													while($rows = mysql_fetch_assoc($res))
													{$i++;
												?>
													<tr>
														<td>
															<select  name="selCert_<?php echo $i;?>" class="select form-control" id="selCert_<?php echo $i;?>" >
																<?php $obj->getCertificateUpdateList($rows['CERTIFICATE_ID']); ?>
															</select>
															<script>$("#selCert_<?php echo $i;?>").val(<?php echo $rows['CERTIFICATE_ID'];?>);</script>
														</td>
														<td>
															<input name="txtIDate_<?php echo $i;?>" type="text" class="form-control datepiker" id="txtIDate_<?php echo $i;?>" autocomplete="off" value="<?php if($rows['DATE_ISSUE'] == "0000-00-00" || $rows['DATE_ISSUE'] == "1970-01-01"){ echo ""; }else{ echo date("d-m-Y",strtotime($rows['DATE_ISSUE'])); } ?>" placeholder="dd-mm-yyyy" />
														</td>
														<td>
															<input name="txtLAIDate_<?php echo $i;?>" type="text" class="form-control datepiker" id="txtLAIDate_<?php echo $i;?>" autocomplete="off" value="<?php if($rows['DATE_LAST'] == "0000-00-00" || $rows['DATE_LAST'] == "1970-01-01"){ echo ""; }else{ echo date("d-m-Y",strtotime($rows['DATE_LAST'])); } ?>" placeholder="dd-mm-yyyy" />
															
														</td>
														<td>
															<input name="txtEDate_<?php echo $i;?>" type="text" class="form-control datepiker" id="txtEDate_<?php echo $i;?>" autocomplete="off" value="<?php if($rows['DATE_EXPIRY'] == "0000-00-00" || $rows['DATE_EXPIRY'] == "1970-01-01"){ echo ""; }else{ echo date("d-m-Y",strtotime($rows['DATE_EXPIRY'])); } ?>" placeholder="dd-mm-yyyy" />
														</td>
														<td>
                                                        <div class="btn btn-success btn-file btn-flat" data-toggle="tooltip">
                                                            <i class="fa fa-paperclip"></i> Attachment
                                                            <input type="file" class="form-control" multiple name="attach_file_<?php echo $i;?>[]" id="attach_file_<?php echo $i;?>" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                                        </div>
															<input type="hidden" name="txtCRMNAME_<?php echo $i;?>" id="txtCRMNAME_<?php echo $i;?>" value=""/><input type="hidden" name="txtCRMFILE_<?php echo $i;?>" id="txtCRMFILE_<?php echo $i;?>" value=""/>
													<?php if($rows['UPLOAD'] != '')
														{ 
															$file = explode(",",$rows['UPLOAD']); 
															$name = explode(",",$rows['UPLOAD_NANE']); ?>										
														<table>
															<tr>
																<td colspan="2">
																   Previous Attachments
																</td>
															</tr>
															<?php
															
															for($j=0;$j<sizeof($file);$j++)
															{
															?>
															<tr id="row_file_<?php echo $i;?>_<?php echo $j+1;?>">
																<td width="40%" align="left" class="input-text"  valign="top"><a href="../../attachment/<?php echo $file[$j]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="<?php echo $name[$j];?>"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $name[$j];?></a>
																<input type="hidden" name="file1_<?php echo $i;?>_<?php echo $j+1;?>" id="file1_<?php echo $i;?>_<?php echo $j+1;?>" value="<?php echo $file[$j]; ?>" />
																<input type="hidden" name="name1_<?php echo $i;?>_<?php echo $j+1;?>" id="name1_<?php echo $i;?>_<?php echo $j+1;?>" value="<?php echo $name[$j]; ?>" />
																</td>
																<td ><a href="#1" onClick="Del_Upload(<?php echo $i;?>,<?php echo $j+1;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
															</tr>
															<?php }?>
														 </table>
												   <?php }?>
															
														</td>
													</tr>
												<?php } ?>
												<?php } ?>
												<?php 
													if($rec == 0){$value = 1;}
													else{$value = $rec;}
												?>
											</tbody>
												<tr>
													<td ><input type="hidden" class="input" name="txtCID" id="txtCID" value="<?php echo $value; ?>"/>
														<button type="button" onClick="addCertificateRow();" name="button" id="button" class="form-control btn btn-success">Add</button></td>
													<td colspan="2">
														
													</td>
													<td colspan="2"></td>
												</tr>
										</table>
									</div>
									
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Documentation
											</h2>                            
										</div><!-- /.col -->
									</div>
									
									<div class="row invoice-info">
										<div class="col-sm-6 invoice-col">
											Does vessel have all updated publications as listed in the Vessel Inspection Questionnaire, Chapter 2- Question 2.24, as applicable
											<address>
												<input type="radio" id="rdoPublications1" name="rdoPublications" value="1" <?php if($rdoPublications == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoPublications2" name="rdoPublications" value="2" <?php if($rdoPublications == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-6 invoice-col">
											Owner warrant that vessel is member of ITOPF and will remain so for the entire duration of this voyage/contract
											<address>
												<input type="radio" id="rdoOwnerWarrant1" name="rdoOwnerWarrant" value="1" <?php if($rdoOwnerWarrant == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoOwnerWarrant2" name="rdoOwnerWarrant" value="2" <?php if($rdoOwnerWarrant == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
									</div>
									
									
								</div>
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab One Form------------------------------------------------->
								<!----------------------------------------------------------------------------------------------------------->
                                <div id="tab_2" class="tab-pane">
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Nationality of Master
											<address>
												<input name="txtMasterNationality" type="text" class="form-control" id="txtMasterNationality" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MASTER_NATIONALITY','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Nationality of Master" />
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Nationality of Officers
											<address>
												<input name="txtOfficerNationality" type="text" class="form-control" id="txtOfficerNationality" autocomplete="off" value="<?php echo $obj->getVesselParticularData('OFFICER_NATIONALITY','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Nationality of Officers" />
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Nationality of Crew
											<address>
												<input name="txtCrewNationality" type="text" class="form-control" id="txtCrewNationality" autocomplete="off" value="<?php echo $obj->getVesselParticularData('CREW_NATIONALITY','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="Nationality of Crew" />
											</address>
										</div><!-- /.col -->
										<div class="col-sm-4 invoice-col">
											If Officers/Crew employed by a Manning Agency - Full style
											<address>
												<textarea name="txtManningAgency" id="txtManningAgency" class="form-control areasize" placeholder="If Officers/Crew employed by a Manning Agency - Full style"><?php echo $obj->getVesselParticularData('MANNING_AGENCY','vessel_master_tankers',$_REQUEST['id']);?></textarea>
											</address>
										</div><!-- /.col -->
									</div>	
									
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											What is the common working language onboard
											<address>
												<input name="txtOnboardWorkingLanguage" type="text" class="form-control" id="txtOnboardWorkingLanguage" autocomplete="off" value="<?php echo $obj->getVesselParticularData('WORKING_LANGUAGE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="What is the common working language onboard" />
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Do officers speak and understand English
											<address>
												<input type="radio" id="rdoUnderstandEnglish1" name="rdoUnderstandEnglish" value="1" <?php if($rdoUnderstandEnglish == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoUnderstandEnglish2" name="rdoUnderstandEnglish" value="2" <?php if($rdoUnderstandEnglish == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											In case of Flag Of Convenience, is the ITF Special Agreement on board
											<address>
												<input name="txtITFSpecialAgreement" type="text" class="form-control" id="txtITFSpecialAgreement" autocomplete="off" value="<?php echo $obj->getVesselParticularData('ITF_SPECIAL_AGREEMENT','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="In case of Flag Of Convenience, is the ITF Special Agreement on board" />
											</address>
										</div><!-- /.col -->
										<div class="col-sm-4 invoice-col">
											
										</div><!-- /.col -->
									</div>	
									
								</div><!-- /.tab_2-pane -->
								
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Two Form------------------------------------------------->
								<!----------------------------------------------------------------------------------------------------------->
								
								<div id="tab_3" class="tab-pane">
									<div class="row invoice-info">
										<div class="col-sm-4 invoice-col">
											Can the ship comply with the ICS Helicopter Guidelines
											<address>
												<input type="radio" id="rdoICSHelicopter1" name="rdoICSHelicopter" value="1" <?php if($rdoICSHelicopter == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoICSHelicopter2" name="rdoICSHelicopter" value="2" <?php if($rdoICSHelicopter == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-4 invoice-col">
											If Yes, state whether winching or landing area provided
											<address>
												<input name="txtICSHelicopter" type="text" class="form-control" id="txtICSHelicopter"  autocomplete="off" <?php if($rdoICSHelicopter == 2){echo "readonly";}?> value="<?php echo $obj->getVesselParticularData('ICSHELICOPTER_AREA','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="If Yes, state whether winching or landing area provided" />
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-4 invoice-col">
											
										</div><!-- /.col -->
										
										<div class="col-sm-4 invoice-col">
											
										</div><!-- /.col -->
									</div>		
								</div><!-- /.tab_3-pane -->
								
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Three Form----------------------------------------------->
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_4" class="tab-pane">
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Has the vessel Operator submitted a Vessel Spill Response Plan to the US Coast Guard which has been approved by official USCG letter
											<address>
												<input type="radio" id="rdoSpillResponse1" name="rdoSpillResponse" value="1" <?php if($rdoSpillResponse == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoSpillResponse2" name="rdoSpillResponse" value="2" <?php if($rdoSpillResponse == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Qualified individual (QI) - Full style
											<address>
												<textarea name="txtQualifiedIndividual" id="txtQualifiedIndividual" class="form-control areasize" placeholder="Qualified individual (QI) - Full style"><?php echo $obj->getVesselParticularData('QUALIFIED_INDIVIDUAL','vessel_master_tankers',$_REQUEST['id']);?></textarea>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Oil Spill Response Organization (OSRO) -Full style
											<address>
												<textarea name="txtSpillResponse" id="txtSpillResponse" class="form-control areasize" placeholder="Oil Spill Response Organization (OSRO) -Full style"><?php echo $obj->getVesselParticularData('SPILL_RESPONSE','vessel_master_tankers',$_REQUEST['id']);?></textarea>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Has technical operator signed the SCIA / C-TPAT agreement with US customs concerning drug smuggling
											<address>
												<input type="radio" id="rdoAgreementUS1" name="rdoAgreementUS" value="1" <?php if($rdoAgreementUS == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoAgreementUS2" name="rdoAgreementUS" value="2" <?php if($rdoAgreementUS == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
									</div>		
										
								</div><!-- /.tab_4-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Four Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_5" class="tab-pane">
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Double Hull Vessels             
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Is vessel fitted with centerline bulkhead in all cargo tanks
											<address>
												<input type="radio" id="rdoBulkhead1" name="rdoBulkhead" value="1" <?php if($rdoBulkhead == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoBulkhead2" name="rdoBulkhead" value="2" <?php if($rdoBulkhead == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-3 invoice-col">
											If Yes, is bulkhead solid or perforated
											<address>
												<input name="txtBulkhead" type="text" class="form-control" id="txtBulkhead" <?php if($rdoBulkhead == 2){echo "readonly";}?> autocomplete="off" value="<?php echo $obj->getVesselParticularData('BULKHEAD','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Cargo Tank Capacities             
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Capacity (98%) of each natural segregation with double valve (specify tanks)
											<address>
												<textarea name="txtCapacity98" id="txtCapacity98" class="form-control areasize" placeholder="Capacity (98%) of each natural segregation with double valve (specify tanks)"><?php echo $obj->getVesselParticularData('CAPACITY_98','vessel_master_tankers',$_REQUEST['id']);?></textarea>
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-3 invoice-col">
											Total cubic capacity (98%, excluding slop tanks)(M3)
											<address>
												<input name="txtCubicCapacity98" type="text" class="form-control numeric" id="txtCubicCapacity98" autocomplete="off" value="<?php echo $obj->getVesselParticularData('CUBIC_CAPACITY','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Slop tank(s) capacity (98%)(M3)
											<address>
												<input name="txtSlopCapacity98" type="text" class="form-control numeric" id="txtSlopCapacity98" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SLOP_CAPACITY98','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Residual/Retention oil tank(s) capacity (98%)(M3), if applicable
											<address>
												<input name="txtTankCapacity98" type="text" class="form-control numeric" id="txtTankCapacity98" autocomplete="off" value="<?php echo $obj->getVesselParticularData('OIL_TANKCAPACITY98','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Does vessel have Segregated Ballast Tanks (SBT) or Clean Ballast Tanks (CBT):
											<address>
												<input type="text" name="txtSegregated" id="txtSegregated" class="form-control" value="<?php echo $obj->getVesselParticularData('SEGREGATED_BALLAST','vessel_master_tankers',$_REQUEST['id']);?>" autocomplete="off"/>
											</address>
										</div><!-- /.col -->
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												SBT Vessels         
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											What is total capacity of SBT?(M3)
											<address>
												<input name="txtSBTCapacity" type="text" class="form-control numeric" id="txtSBTCapacity" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SBT_CAPACITY','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-3 invoice-col">
											What percentage of SDWT can vessel maintain with SBT only(%)
											<address>
												<input name="txtSDWTPercent" type="text" class="form-control numeric" id="txtSDWTPercent" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SDWT_PERCENT','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Does vessel meet the requirements of MARPOL Annex I Reg 18.2: (previously Reg 13.2)
											<address>
												<input type="radio" id="rdoMARPOLAnnex1" name="rdoMARPOLAnnex" value="1" <?php if($rdoMARPOLAnnex == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoMARPOLAnnex2" name="rdoMARPOLAnnex" value="2" <?php if($rdoMARPOLAnnex == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											
										</div><!-- /.col -->
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Cargo Handling     
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											How many grades/products can vessel load/discharge with double valve segregation
											<address>
												<input name="txtNoOfGradeProduct" type="text" class="form-control numeric" id="txtNoOfGradeProduct" autocomplete="off" value="<?php echo $obj->getVesselParticularData('NO_OF_CRUDE_PRODUCT','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-3 invoice-col">
											Maximum loading rate for homogenous cargo per manifold connection(M3/HR)
											<address>
												<input name="txtCargoRatePerManifold" type="text" class="form-control numeric" id="txtCargoRatePerManifold" autocomplete="off" value="<?php echo $obj->getVesselParticularData('CARGORATE_PERMANIFOLD','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
                                        <div class="col-sm-3 invoice-col">
											Maximum loading rate for homogenous cargo loaded simultaneously through all manifolds(M3/HR)
											<address>
												<input name="txtCargoRateAllManifold" type="text" class="form-control numeric" id="txtCargoRateAllManifold" autocomplete="off" value="<?php echo $obj->getVesselParticularData('CARGORATE_ALLMANIFOLD','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Are there any cargo tank filling restrictions. If yes, please specify
											<address>
												<input type="radio" id="rdoCargoTankRestrictions1" name="rdoCargoTankRestrictions" value="1" <?php if($rdoCargoTankRestrictions == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoCargoTankRestrictions2" name="rdoCargoTankRestrictions" value="2" <?php if($rdoCargoTankRestrictions == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
										    If yes, please specify
											<address>
												<input name="txtCargoTankRestrictions" type="text" class="form-control numeric" id="txtCargoTankRestrictions" <?php if($rdoCargoTankRestrictions == 2){echo "readonly";}?> autocomplete="off" value="<?php echo $obj->getVesselParticularData('CARGOTANKRESTRICTIONS','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Pumping Systems   
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="box-body no-padding">
										<table class="table table-striped">
											<tbody>
											<tr>
												<th width="15%">Pumps</th>
												<th width="15%">No.</th>
												<th width="15%">Type</th>
												<th width="15%">Capacity(M3/HR)</th>
											</tr>
											<tr>
												<td>Cargo</td>
												<td><input name="txtPumpsNo_1" type="text" class="form-control numeric" id="txtPumpsNo_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSNO_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
												<td><input name="txtPumpsType_1" type="text" class="form-control" id="txtPumpsType_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSTYPE_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												<td><input name="txtPumpsCapacity_1" type="text" class="form-control numeric" id="txtPumpsCapacity_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSCAPACITY_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
											</tr>
											<tr>
												<td>Stripping</td>
												<td><input name="txtPumpsNo_2" type="text" class="form-control numeric" id="txtPumpsNo_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSNO_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
												<td><input name="txtPumpsType_2" type="text" class="form-control" id="txtPumpsType_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSTYPE_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												<td><input name="txtPumpsCapacity_2" type="text" class="form-control numeric" id="txtPumpsCapacity_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSCAPACITY_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
											</tr>
											<tr>
												<td>Eductors</td>
												<td><input name="txtPumpsNo_3" type="text" class="form-control numeric" id="txtPumpsNo_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSNO_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
												<td><input name="txtPumpsType_3" type="text" class="form-control" id="txtPumpsType_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSTYPE_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												<td><input name="txtPumpsCapacity_3" type="text" class="form-control numeric" id="txtPumpsCapacity_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSCAPACITY_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
											</tr>
											<tr>
												<td>Ballast</td>
												<td><input name="txtPumpsNo_4" type="text" class="form-control numeric" id="txtPumpsNo_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSNO_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
												<td><input name="txtPumpsType_4" type="text" class="form-control" id="txtPumpsType_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSTYPE_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												<td><input name="txtPumpsCapacity_4" type="text" class="form-control numeric" id="txtPumpsCapacity_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('PUMPSCAPACITY_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
											</tr>
										</tbody>
										</table>
									</div>
									    
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											How many cargo pumps can be run simultaneously at full capacity
											<address>
												<input name="txtNoOfCargoPumps" type="text" class="form-control numeric" id="txtNoOfCargoPumps" autocomplete="off" value="<?php echo $obj->getVesselParticularData('NO_OF_CARGO_PUMPS','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Cargo Control Room
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Is ship fitted with a Cargo Control Room (CCR)
											<address>
												<input type="radio" id="rdoCargoControlRoom1" name="rdoCargoControlRoom" value="1" <?php if($rdoCargoControlRoom == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoCargoControlRoom2" name="rdoCargoControlRoom" value="2" <?php if($rdoCargoControlRoom == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-3 invoice-col">
											Can tank innage / ullage be read from the CCR
											<address>
												<input type="radio" id="rdoTankInnageCCR1" name="rdoTankInnageCCR" value="1" <?php if($rdoTankInnageCCR == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoTankInnageCCR2" name="rdoTankInnageCCR" value="2" <?php if($rdoTankInnageCCR == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											
										</div><!-- /.col -->
									</div>
									
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Gauging and Sampling    
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Can ship operate under closed conditions in accordance with ISGOTT
											<address>
												<input type="radio" id="rdoShipOperateUnderClosed1" name="rdoShipOperateUnderClosed" value="1" <?php if($rdoShipOperateUnderClosed == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoShipOperateUnderClosed2" name="rdoShipOperateUnderClosed" value="2" <?php if($rdoShipOperateUnderClosed == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-3 invoice-col">
											What type of fixed closed tank gauging system is fitted
											<address>
												<input name="txtTypeOfGaugingSystem" type="text" class="form-control" id="txtTypeOfGaugingSystem" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TYPE_OF_GAUGING_SYSTEM','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Are overfill (high-high) alarms fitted?
											<address>
												<input type="radio" id="rdoOverFillAlarm1" name="rdoOverFillAlarm" value="1" <?php if($rdoOverFillAlarm == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoOverFillAlarm2" name="rdoOverFillAlarm" value="2" <?php if($rdoOverFillAlarm == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
										    If Yes, indicate whether to all tanks or partial
											<address>
												<input name="txtOverFillAlarm" type="text" class="form-control" id="txtOverFillAlarm" <?php if($rdoOverFillAlarm == 2){echo "readonly";}?> autocomplete="off" value="<?php echo $obj->getVesselParticularData('OVERFILLALARM','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Vapor Emission Control   
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Is a vapor return system (VRS) fitted
											<address>
												<input type="radio" id="rdoVaporReturnSystem1" name="rdoVaporReturnSystem" value="1" <?php if($rdoVaporReturnSystem == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoVaporReturnSystem2" name="rdoVaporReturnSystem" value="2" <?php if($rdoVaporReturnSystem == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-3 invoice-col">
											Number of VRS manifolds (per side)
											<address>
												<input name="txtNoOfVRS" type="text" class="form-control" id="txtNoOfVRS" autocomplete="off" value="<?php echo $obj->getVesselParticularData('NO_OF_VRS','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-3 invoice-col">
										    Size of VRS manifolds (per side)(MM)
											<address>
												<input name="txtSizeOfVRS" type="text" class="form-control" id="txtSizeOfVRS" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SIZE_OF_VRS','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Venting  
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											State what type of venting system is fitted
											<address>
												<input name="txtTypeOfVentingSystem" type="text" class="form-control" id="txtTypeOfVentingSystem" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TYPE_OF_VENTING_SYSTEM','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Cargo Manifolds 
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
									    <div class="col-sm-3 invoice-col">
											Does vessel comply with the latest edition of the OCIMF 'Recommendations for Oil Tanker Manifolds and Associated Equipment
											<address>
												<input type="radio" id="rdoOCIMFEdition1" name="rdoOCIMFEdition" value="1" <?php if($rdoOCIMFEdition == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoOCIMFEdition2" name="rdoOCIMFEdition" value="2" <?php if($rdoOCIMFEdition == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											What is the number of cargo connections per side
											<address>
												<input name="txtCargoConnectionPerSide" type="text" class="form-control numeric" id="txtCargoConnectionPerSide" autocomplete="off" value="<?php echo $obj->getVesselParticularData('CARGO_CONNECTION_PER_SIDE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											What is the size of cargo connections
											<address>
												<input name="txtSizeOfCargoConnection" type="text" class="form-control numeric" id="txtSizeOfCargoConnection" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SIZE_OF_CARGO_CONNECTION','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											What is the material of the manifold
											<address>
												<input name="txtMaterialOfManifolds" type="text" class="form-control" id="txtMaterialOfManifolds" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MATERIAL_OF_MANIFOLDS','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Manifold Arrangement
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
									    <div class="col-sm-3 invoice-col">
											Distance between cargo manifold centers(MM)
											<address>
												<input name="txtManifoldsDistance" type="text" class="form-control numeric" id="txtManifoldsDistance" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MANIFOLDS_DISTANCE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Distance ships rail to manifold(MM)
											<address>
												<input name="txtShipRailToManifold" type="text" class="form-control numeric" id="txtShipRailToManifold" autocomplete="off" value="<?php echo $obj->getVesselParticularData('SHIPS_RAIL_MANIFOLD','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Distance manifold to ships side(MM)
											<address>
												<input name="txtShipSideDistance" type="text" class="form-control numeric" id="txtShipSideDistance" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MANIFOLD_SHIPSIDE_DISTANCE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Top of rail to center of manifold(MM)
											<address>
												<input name="txtTopRailCenterManifold" type="text" class="form-control" id="txtTopRailCenterManifold" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TOPRAIL_CENTER_MANIFOLD','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
									<div class="row invoice-info">
                                        <div class="col-sm-3 invoice-col">
											Distance main deck to center of manifold(MM)
											<address>
												<input name="txtDistanceMainDeck" type="text" class="form-control" id="txtDistanceMainDeck" autocomplete="off" value="<?php echo $obj->getVesselParticularData('DISTANCE_MAIN_DECK','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									    <div class="col-sm-3 invoice-col">
											Manifold height above the waterline in normal ballast condition(M)
											<address>
												<input name="txtHeightAtBallast" type="text" class="form-control numeric" id="txtHeightAtBallast" autocomplete="off" value="<?php echo $obj->getVesselParticularData('HEIGHT_AT_BALLAST','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Manifold height above the waterline in normal at SDWT condition(M)
											<address>
												<input name="txtHeightAtSDWT" type="text" class="form-control numeric" id="txtHeightAtSDWT" autocomplete="off" value="<?php echo $obj->getVesselParticularData('HEIGHT_AT_SDWT','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Number / size reducers
											<address>
												<textarea name="txtNumSizeReducers" id="txtNumSizeReducers" class="form-control areasize" ><?php echo $obj->getVesselParticularData('NUM_SIZE_REDUCERS','vessel_master_tankers',$_REQUEST['id']);?></textarea>
											</address>
										</div><!-- /.col -->
									</div>
									
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Stern Manifold
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
									    <div class="col-sm-3 invoice-col">
											Is vessel fitted with a stern manifold
											<address>
												<input type="radio" id="rdoFittedStermManifold1" name="rdoFittedStermManifold" value="1" <?php if($rdoFittedStermManifold == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoFittedStermManifold2" name="rdoFittedStermManifold" value="2" <?php if($rdoFittedStermManifold == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											If stern manifold fitted, state size(MM)
											<address>
												<input name="txtFittedStermManifold" type="text" class="form-control numeric" id="txtFittedStermManifold" <?php if($rdoFittedStermManifold == 2){echo "readonly";}?> autocomplete="off" value="<?php echo $obj->getVesselParticularData('FITTEDSTERMMANIFOLD','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											
										</div><!-- /.col -->
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Cargo Heating
											</h2>                        
										</div><!-- /.col -->
									</div>
									<div class="row invoice-info">
									    <div class="col-sm-3 invoice-col">
											Type of cargo heating system?
											<address>
												<input name="txtTypeOfCargoHeating" type="text" class="form-control" id="txtTypeOfCargoHeating" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TYPEODCARGOHEATING','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											If fitted, are all tanks coiled?
											<address>
												<input type="radio" id="rdoTankCoiled1" name="rdoTankCoiled" value="1" <?php if($rdoTankCoiled == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoTankCoiled2" name="rdoTankCoiled" value="2" <?php if($rdoTankCoiled == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											If stern manifold fitted, state size(MM)
											<address>
												<input name="txtTankCoiled" type="text" class="form-control" id="txtTankCoiled" <?php if($rdoTankCoiled == 2){echo "readonly";}?> autocomplete="off" value="<?php echo $obj->getVesselParticularData('TANKCOILED','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Maximum temperature cargo can be loaded/maintained (F)
											<address>
												<input name="txtMaximumTempreature" type="text" class="form-control" id="txtMaximumTempreature" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MAXIMUMTEMPRATURE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<h2 class="page-header">
												Tank Coating             
											</h2>                        
										</div><!-- /.col -->
									</div>
									
									
									<div class="box-body no-padding">
										<table class="table table-striped">
											<tbody>
											<tr>
												<th width="15%">Are cargo, ballast and slop tanks coated?</th>
												<th width="15%">Coated</th>
												<th width="15%">Type</th>
												<th width="15%">To What Extent</th>
											</tr>
											<tr>
												<td>Cargo tanks</td>
												<td><input type="radio" id="rdoTankCoating11" name="rdoTankCoating1" value="1" <?php if($rdoTankCoating1 == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoTankCoating12" name="rdoTankCoating1" value="2" <?php if($rdoTankCoating1 == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No</td>
												<td><input name="txtCoatingType_1" type="text" class="form-control" id="txtCoatingType_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('COATINGTYPE_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												<td><input name="txtCoatingExtent_1" type="text" class="form-control" id="txtCoatingExtent_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('COATINGEXTENT_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
											</tr>
											<tr>
												<td>Ballast tanks</td>
												<td><input type="radio" id="rdoTankCoating21" name="rdoTankCoating2" value="1" <?php if($rdoTankCoating2 == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoTankCoating22" name="rdoTankCoating2" value="2" <?php if($rdoTankCoating2 == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No</td>
												<td><input name="txtCoatingType_2" type="text" class="form-control" id="txtCoatingType_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('COATINGTYPE_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												<td><input name="txtCoatingExtent_2" type="text" class="form-control" id="txtCoatingExtent_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('COATINGEXTENT_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
											</tr>
											<tr>
												<td>Slop tanks</td>
												<td><input type="radio" id="rdoTankCoating31" name="rdoTankCoating3" value="1" <?php if($rdoTankCoating3 == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoTankCoating32" name="rdoTankCoating3" value="2" <?php if($rdoTankCoating3 == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No</td>
												<td><input name="txtCoatingType_3" type="text" class="form-control" id="txtCoatingType_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('COATINGTYPE_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												<td><input name="txtCoatingExtent_3" type="text" class="form-control" id="txtCoatingExtent_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('COATINGEXTENT_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
											</tr>
										</tbody>
										</table>
									</div>
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											If fitted, what type of anodes are used
											<address>
												<input name="txtCoatingTypeUsed" type="text" class="form-control" id="txtCoatingTypeUsed" autocomplete="off" value="<?php echo $obj->getVesselParticularData('COATINGTYPEUSED','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
									
								</div><!-- /.tab_5-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Five Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_6" class="tab-pane">
								    
									<div class="row invoice-info">
										<div class="col-sm-3 invoice-col">
											Is an Inert Gas System (IGS) fitted
											<address>
												<input type="radio" id="rdoGasSystem1" name="rdoGasSystem" value="1" <?php if($rdoGasSystem == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoGasSystem2" name="rdoGasSystem" value="2" <?php if($rdoGasSystem == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
											</address>
										</div><!-- /.col -->
										
										<div class="col-sm-3 invoice-col">
											Is IGS supplied by flue gas, inert gas (IG) generator and/or nitrogen
											<address>
												<input name="txtIGSSupplied" type="text" class="form-control" id="txtIGSSupplied"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('IGS_SUPPLIED','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
										<div class="col-sm-3 invoice-col">
											Is a Crude Oil Washing (COW) installation fitted
											<address>
												<input name="txtCrudeOilWashing" type="text" class="form-control" id="txtCrudeOilWashing"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CRUDEOILWASHING','vessel_master_tankers',$_REQUEST['id']);?>" placeholder=""/>
											</address>
										</div><!-- /.col -->
									</div>
								</div><!-- /.tab_6-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Six Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
								
								
								<div id="tab_7" class="tab-pane">
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Mooring wires (on drums)             
												</h2>                        
											</div><!-- /.col -->
										</div>
										
										
										<div class="box-body no-padding">
											<table class="table table-striped">
												<tbody>
												<tr>
													<th width="15%">&nbsp;</th>
													<th width="15%">No.</th>
													<th width="15%">Diameter(MM)</th>
													<th width="15%">Material</th>
													<th width="15%">Length(M)</th>
													<th width="15%">Breaking Strength(MT)</th>
												</tr>
												<tr>
													<td>Forecastle</td>
													<td><input name="txtMooringNo_1" type="text" class="form-control numeric" id="txtMooringNo_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter_1" type="text" class="form-control numeric" id="txtMooringDiameter_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial_1" type="text" class="form-control" id="txtMooringMaterial_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength_1" type="text" class="form-control numeric" id="txtMooringLength_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength_1" type="text" class="form-control numeric" id="txtMooringBreakStrength_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck fwd</td>
													<td><input name="txtMooringNo_2" type="text" class="form-control numeric" id="txtMooringNo_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter_2" type="text" class="form-control numeric" id="txtMooringDiameter_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial_2" type="text" class="form-control" id="txtMooringMaterial_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength_2" type="text" class="form-control numeric" id="txtMooringLength_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength_2" type="text" class="form-control numeric" id="txtMooringBreakStrength_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck aft</td>
													<td><input name="txtMooringNo_3" type="text" class="form-control numeric" id="txtMooringNo_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter_3" type="text" class="form-control numeric" id="txtMooringDiameter_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial_3" type="text" class="form-control" id="txtMooringMaterial_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength_3" type="text" class="form-control numeric" id="txtMooringLength_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength_3" type="text" class="form-control numeric" id="txtMooringBreakStrength_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Poop deck</td>
													<td><input name="txtMooringNo_4" type="text" class="form-control numeric" id="txtMooringNo_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter_4" type="text" class="form-control numeric" id="txtMooringDiameter_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial_4" type="text" class="form-control" id="txtMooringMaterial_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength_4" type="text" class="form-control numeric" id="txtMooringLength_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength_4" type="text" class="form-control numeric" id="txtMooringBreakStrength_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
											</tbody>
											</table>
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Wire tails            
												</h2>                        
											</div><!-- /.col -->
										</div>
										
										
										<div class="box-body no-padding">
											<table class="table table-striped">
												<tbody>
												<tr>
													<th width="15%">&nbsp;</th>
													<th width="15%">No.</th>
													<th width="15%">Diameter(MM)</th>
													<th width="15%">Material</th>
													<th width="15%">Length(M)</th>
													<th width="15%">Breaking Strength(MT)</th>
												</tr>
												<tr>
													<td>Forecastle</td>
													<td><input name="txtMooringNo1_1" type="text" class="form-control numeric" id="txtMooringNo1_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO1_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter1_1" type="text" class="form-control numeric" id="txtMooringDiameter1_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER1_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial1_1" type="text" class="form-control" id="txtMooringMaterial1_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL1_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength1_1" type="text" class="form-control numeric" id="txtMooringLength1_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH1_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength1_1" type="text" class="form-control numeric" id="txtMooringBreakStrength1_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH1_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck fwd</td>
													<td><input name="txtMooringNo1_2" type="text" class="form-control numeric" id="txtMooringNo1_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO1_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter1_2" type="text" class="form-control numeric" id="txtMooringDiameter1_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER1_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial1_2" type="text" class="form-control" id="txtMooringMaterial1_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL1_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength1_2" type="text" class="form-control numeric" id="txtMooringLength1_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH1_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength1_2" type="text" class="form-control numeric" id="txtMooringBreakStrength1_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH1_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck aft</td>
													<td><input name="txtMooringNo1_3" type="text" class="form-control numeric" id="txtMooringNo1_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO1_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter1_3" type="text" class="form-control numeric" id="txtMooringDiameter1_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER1_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial1_3" type="text" class="form-control" id="txtMooringMaterial1_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL1_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength1_3" type="text" class="form-control numeric" id="txtMooringLength1_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH1_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength1_3" type="text" class="form-control numeric" id="txtMooringBreakStrength1_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH1_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Poop deck</td>
													<td><input name="txtMooringNo1_4" type="text" class="form-control numeric" id="txtMooringNo1_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO1_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter1_4" type="text" class="form-control numeric" id="txtMooringDiameter1_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER1_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial1_4" type="text" class="form-control" id="txtMooringMaterial1_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL1_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength1_4" type="text" class="form-control numeric" id="txtMooringLength1_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH1_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength1_4" type="text" class="form-control numeric" id="txtMooringBreakStrength1_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH1_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
											</tbody>
											</table>
										</div>
										
										
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Mooring ropes (on drums)       
												</h2>                        
											</div><!-- /.col -->
										</div>
										
										
										<div class="box-body no-padding">
											<table class="table table-striped">
												<tbody>
												<tr>
													<th width="15%">&nbsp;</th>
													<th width="15%">No.</th>
													<th width="15%">Diameter(MM)</th>
													<th width="15%">Material</th>
													<th width="15%">Length(M)</th>
													<th width="15%">Breaking Strength(MT)</th>
												</tr>
												<tr>
													<td>Forecastle</td>
													<td><input name="txtMooringNo2_1" type="text" class="form-control numeric" id="txtMooringNo2_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO2_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter2_1" type="text" class="form-control numeric" id="txtMooringDiameter2_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER2_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial2_1" type="text" class="form-control" id="txtMooringMaterial2_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL2_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength2_1" type="text" class="form-control numeric" id="txtMooringLength2_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH2_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength2_1" type="text" class="form-control numeric" id="txtMooringBreakStrength2_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH2_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck fwd</td>
													<td><input name="txtMooringNo2_2" type="text" class="form-control numeric" id="txtMooringNo2_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO2_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter2_2" type="text" class="form-control numeric" id="txtMooringDiameter2_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER2_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial2_2" type="text" class="form-control" id="txtMooringMaterial2_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL2_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength2_2" type="text" class="form-control numeric" id="txtMooringLength2_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH2_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength2_2" type="text" class="form-control numeric" id="txtMooringBreakStrength2_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH2_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck aft</td>
													<td><input name="txtMooringNo2_3" type="text" class="form-control numeric" id="txtMooringNo2_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO2_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter2_3" type="text" class="form-control numeric" id="txtMooringDiameter2_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER2_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial2_3" type="text" class="form-control" id="txtMooringMaterial2_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL2_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength2_3" type="text" class="form-control numeric" id="txtMooringLength2_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH2_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength2_3" type="text" class="form-control numeric" id="txtMooringBreakStrength2_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH2_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Poop deck</td>
													<td><input name="txtMooringNo2_4" type="text" class="form-control numeric" id="txtMooringNo2_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO2_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter2_4" type="text" class="form-control numeric" id="txtMooringDiameter2_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER2_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial2_4" type="text" class="form-control" id="txtMooringMaterial2_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL2_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength2_4" type="text" class="form-control numeric" id="txtMooringLength2_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH2_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength2_4" type="text" class="form-control numeric" id="txtMooringBreakStrength2_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH2_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
											</tbody>
											</table>
										</div>
										
										
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Other mooring lines    
												</h2>                        
											</div><!-- /.col -->
										</div>
										
										
										<div class="box-body no-padding">
											<table class="table table-striped">
												<tbody>
												<tr>
													<th width="15%">&nbsp;</th>
													<th width="15%">No.</th>
													<th width="15%">Diameter(MM)</th>
													<th width="15%">Material</th>
													<th width="15%">Length(M)</th>
													<th width="15%">Breaking Strength(MT)</th>
												</tr>
												<tr>
													<td>Forecastle</td>
													<td><input name="txtMooringNo3_1" type="text" class="form-control numeric" id="txtMooringNo3_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO3_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter3_1" type="text" class="form-control numeric" id="txtMooringDiameter3_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER3_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial3_1" type="text" class="form-control" id="txtMooringMaterial3_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL3_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength3_1" type="text" class="form-control numeric" id="txtMooringLength3_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH3_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength3_1" type="text" class="form-control numeric" id="txtMooringBreakStrength3_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH3_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck fwd</td>
													<td><input name="txtMooringNo3_2" type="text" class="form-control numeric" id="txtMooringNo3_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO3_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter3_2" type="text" class="form-control numeric" id="txtMooringDiameter3_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER3_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial3_2" type="text" class="form-control" id="txtMooringMaterial3_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL3_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength3_2" type="text" class="form-control numeric" id="txtMooringLength3_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH3_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength3_2" type="text" class="form-control numeric" id="txtMooringBreakStrength3_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH3_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck aft</td>
													<td><input name="txtMooringNo3_3" type="text" class="form-control numeric" id="txtMooringNo3_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO3_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter3_3" type="text" class="form-control numeric" id="txtMooringDiameter3_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER3_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial3_3" type="text" class="form-control" id="txtMooringMaterial3_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL3_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength3_3" type="text" class="form-control numeric" id="txtMooringLength3_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH3_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength3_3" type="text" class="form-control numeric" id="txtMooringBreakStrength3_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH3_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Poop deck</td>
													<td><input name="txtMooringNo3_4" type="text" class="form-control numeric" id="txtMooringNo3_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORNO3_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDiameter3_4" type="text" class="form-control numeric" id="txtMooringDiameter3_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDIAMETER3_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringMaterial3_4" type="text" class="form-control" id="txtMooringMaterial3_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORMATERIAL3_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringLength3_4" type="text" class="form-control numeric" id="txtMooringLength3_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORLENGTH3_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBreakStrength3_4" type="text" class="form-control numeric" id="txtMooringBreakStrength3_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBREAKSTREANGTH3_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
											</tbody>
											</table>
										</div>
										
										
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Mooring winches  
												</h2>                        
											</div><!-- /.col -->
										</div>
										
										
										<div class="box-body no-padding">
											<table class="table table-striped">
												<tbody>
												<tr>
													<th width="15%">&nbsp;</th>
													<th width="15%">No.</th>
													<th width="15%">Drums</th>
													<th width="15%">Brake Capacity(MT)</th>
												</tr>
												<tr>
													<td>Forecastle</td>
													<td><input name="txtMooringWinNo_1" type="text" class="form-control numeric" id="txtMooringWinNo_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORWINNO_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDrums_1" type="text" class="form-control" id="txtMooringDrums_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDRUMS_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBrakeCap_1" type="text" class="form-control numeric" id="txtMooringBrakeCap_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBRAKECAP_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck fwd</td>
													<td><input name="txtMooringWinNo_2" type="text" class="form-control numeric" id="txtMooringWinNo_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORWINNO_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDrums_2" type="text" class="form-control" id="txtMooringDrums_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDRUMS_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBrakeCap_2" type="text" class="form-control numeric" id="txtMooringBrakeCap_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBRAKECAP_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck aft</td>
													<td><input name="txtMooringWinNo_3" type="text" class="form-control numeric" id="txtMooringWinNo_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORWINNO_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDrums_3" type="text" class="form-control" id="txtMooringDrums_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDRUMS_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBrakeCap_3" type="text" class="form-control numeric" id="txtMooringBrakeCap_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBRAKECAP_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Poop deck</td>
													<td><input name="txtMooringWinNo_4" type="text" class="form-control numeric" id="txtMooringWinNo_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORWINNO_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringDrums_4" type="text" class="form-control" id="txtMooringDrums_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORDRUMS_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
													<td><input name="txtMooringBrakeCap_4" type="text" class="form-control numeric" id="txtMooringBrakeCap_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBRAKECAP_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
											</tbody>
											</table>
										</div>
										
										
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Mooring bitts
												</h2>                        
											</div><!-- /.col -->
										</div>
										
										
										<div class="box-body no-padding">
											<table class="table table-striped">
												<tbody>
												<tr>
													<th width="15%">&nbsp;</th>
													<th width="15%">No.</th>
													<th width="15%">SWL(MT)</th>
												</tr>
												<tr>
													<td>Forecastle</td>
													<td><input name="txtMooringBittNo_1" type="text" class="form-control numeric" id="txtMooringBittNo_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBITTNO_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringSWL_1" type="text" class="form-control numeric" id="txtMooringSWL_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORSWL_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck fwd</td>
													<td><input name="txtMooringBittNo_2" type="text" class="form-control numeric" id="txtMooringBittNo_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBITTNO_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringSWL_2" type="text" class="form-control numeric" id="txtMooringSWL_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORSWL_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck aft</td>
													<td><input name="txtMooringBittNo_3" type="text" class="form-control numeric" id="txtMooringBittNo_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBITTNO_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringSWL_3" type="text" class="form-control numeric" id="txtMooringSWL_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORSWL_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Poop deck</td>
													<td><input name="txtMooringBittNo_4" type="text" class="form-control numeric" id="txtMooringBittNo_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORBITTNO_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringSWL_4" type="text" class="form-control numeric" id="txtMooringSWL_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORSWL_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
											</tbody>
											</table>
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Closed chocks and/or fairleads of enclosed type
												</h2>                        
											</div><!-- /.col -->
										</div>
										
										
										<div class="box-body no-padding">
											<table class="table table-striped">
												<tbody>
												<tr>
													<th width="15%">&nbsp;</th>
													<th width="15%">No.</th>
													<th width="15%">SWL(MT)</th>
												</tr>
												<tr>
													<td>Forecastle</td>
													<td><input name="txtMooringFairNo_1" type="text" class="form-control numeric" id="txtMooringFairNo_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORFAIRNO_1','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringFairSWL_1" type="text" class="form-control numeric" id="txtMooringFairSWL_1" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORFAIRSWL_1','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck fwd</td>
													<td><input name="txtMooringFairNo_2" type="text" class="form-control numeric" id="txtMooringFairNo_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORFAIRNO_2','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringFairSWL_2" type="text" class="form-control numeric" id="txtMooringFairSWL_2" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORFAIRSWL_2','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Main deck aft</td>
													<td><input name="txtMooringFairNo_3" type="text" class="form-control numeric" id="txtMooringFairNo_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORFAIRNO_3','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringFairSWL_3" type="text" class="form-control numeric" id="txtMooringFairSWL_3" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORFAIRSWL_3','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
												<tr>
													<td>Poop deck</td>
													<td><input name="txtMooringFairNo_4" type="text" class="form-control numeric" id="txtMooringFairNo_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORFAIRNO_4','vessel_master_tankers',$_REQUEST['id']);?>"/></td>
													<td><input name="txtMooringFairSWL_4" type="text" class="form-control numeric" id="txtMooringFairSWL_4" autocomplete="off" value="<?php echo $obj->getVesselParticularData('MOORFAIRSWL_4','vessel_master_tankers',$_REQUEST['id']);?>" /></td>
												</tr>
											</tbody>
											</table>
										</div>
										
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Emergency Towing System
												</h2>                        
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-3 invoice-col">
												Type of Emergency Towing system forward
												<address>
													<input type="text" name="txtETSFType" class="form-control" id="txtETSFType"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('ETSF_TYPE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-3 invoice-col">
												 SWL of Emergency Towing system forward(MT)
												<address>
													<input type="text" name="txtETSFSWL" id="txtETSFSWL" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('ETSF_SWL','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												Type of Emergency Towing system aft
												<address>
													<input type="text" name="txtETSFAFT" id="txtETSFAFT" class="form-control"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('ETSA_TYPE_AFT','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												SWL of Emergency Towing system aft(MT)
												<address>
													<input type="text" name="txtETSFSWLAFT" id="txtETSFSWLAFT" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('ETSA_SWL_AFT','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
										 </div>
										 
										 
										 <div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Anchors
												</h2>                        
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-3 invoice-col">
												Number of shackles on port cable
												<address>
													<input type="text" name="txtShacklesCableNo" class="form-control numeric" id="txtShacklesCableNo"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('SHACKLESCABLENO','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-3 invoice-col">
												  Number of shackles on starboard cable
												<address>
													<input type="text" name="txtShacklesSCableNo" id="txtShacklesSCableNo" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('SHACKLESSCABLENO','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
										 </div>
										 
										 <div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Escort Tug
												</h2>                        
											</div><!-- /.col -->
										</div>
										
										<div class="row invoice-info">
											<div class="col-sm-3 invoice-col">
												What is SWL of closed chock and/or fairleads of enclosed type on stern(MT)
												<address>
													<input type="text" name="txtClosedChockSWL" class="form-control numeric" id="txtClosedChockSWL"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CLOSED_CHOCK_SWL','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-3 invoice-col">
												  What is size of closed chock and/or fairleads of enclosed type on stern(MT)
												<address>
													<input type="text" name="txtClosedChockSize" id="txtClosedChockSize" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CLOSED_CHOCK_SIZE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												 What is SWL of bollard on poopdeck suitable for escort tug(MT)
												<address>
													<input type="text" name="txtBOllardSWL" id="txtBOllardSWL" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('BOLLARDSWL','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
										 </div>
										 
										 <div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Bow/Stern Thruster
												</h2>                        
											</div><!-- /.col -->
										</div>
										 
										 <div class="row invoice-info">
											<div class="col-sm-3 invoice-col">
												What is brake horse power of bow thruster (if fitted)(BHP)
												<address>
													<input type="text" name="txtHPOfBowThruster" class="form-control numeric" id="txtHPOfBowThruster"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('HPBOWTHRUSTER','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-3 invoice-col">
												  What is brake horse power of bow thruster (if fitted)(KW)
												<address>
													<input type="text" name="txtKWOfBowThruster" id="txtKWOfBowThruster" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('KWBOWTHRUSTER','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												 What is brake horse power of stern thruster (if fitted)(BHP)
												<address>
													<input type="text" name="txtHPOfSternThruster" id="txtHPOfSternThruster" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('HPSTERNTHRUSTER','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												 What is brake horse power of stern thruster (if fitted)(KW)
												<address>
													<input type="text" name="txtKWOfSternThruster" id="txtKWOfSternThruster" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('KWSTERNTHRUSTER','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
										 </div>
										 
										 <div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Single Point Mooring (SPM) Equipment
												</h2>                        
											</div><!-- /.col -->
										</div>
										 
										 <div class="row invoice-info">
											<div class="col-sm-3 invoice-col">
												Does vessel comply with the latest edition of OCIMF 'Recommendations for Equipment Employed in the Mooring of Vessels at Single Point Moorings (SPM)
												<address>
													<input type="radio" id="rdoOCIMF1" name="rdoOCIMF" value="1" <?php if($rdoOCIMF == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoOCIMF2" name="rdoOCIMF" value="2" <?php if($rdoOCIMF == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-3 invoice-col">
												  Is vessel fitted with chain stopper(s)
												<address>
													<input type="radio" id="rdoChainStopper1" name="rdoChainStopper" value="1" <?php if($rdoChainStopper == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoChainStopper2" name="rdoChainStopper" value="2" <?php if($rdoChainStopper == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												How many chain stopper(s) are fitted
												<address>
													<input type="text" name="txtChainStopper" id="txtChainStopper" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CHAINSTOPPER','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												 State type of chain stopper(s) fitted
												<address>
													<input type="text" name="txtChainStopperType" id="txtChainStopperType" class="form-control"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CHAINSTOPPERTYPE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
										 </div>
										 
										 
										 <div class="row invoice-info">
											<div class="col-sm-3 invoice-col">
												 Safe Working Load (SWL) of chain stopper(s)(MT)
												<address>
													<input type="text" name="txtLoadChainStopper" id="txtLoadChainStopper" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('LOADCHAINSTOPPER','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-3 invoice-col">
												 What is the maximum size chain diameter the bow stopper(s) can handle(MM)
												<address>
													<input type="text" name="txtChainStopperSize" id="txtChainStopperSize" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('SIZECHAINSTOPPER','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												Distance between the bow fairlead and chain stopper/bracket(MM)
												<address>
													<input type="text" name="txtChainStopperDistance" id="txtChainStopperDistance" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('DISTANCECHAINSTOPPER','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												 
											</div><!-- /.col -->
										 </div>
										 
										 <div class="row invoice-info">
											<div class="col-sm-3 invoice-col">
												 Is bow chock and/or fairlead of enclosed type of OCIMF recommended size (600mm x 450mm)?
												<address>
													<input type="radio" id="rdoOCIMFSize1" name="rdoOCIMFSize" value="1" <?php if($rdoOCIMFSize == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoOCIMFSize2" name="rdoOCIMFSize" value="2" <?php if($rdoOCIMFSize == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-3 invoice-col">
												If not, give details of size
												<address>
													<input type="text" name="txtOCIMFSize" id="txtOCIMFSize" class="form-control" <?php if($rdoOCIMFSize == 1){echo "readonly";}?>  autocomplete="off" value="<?php echo $obj->getVesselParticularData('OCIMF_SIZE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												 
											</div><!-- /.col -->
										 </div>
										 
										
										<div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Lifting Equipment
												</h2>                        
											</div><!-- /.col -->
										</div>
										 
										 <div class="row invoice-info">
											<div class="col-sm-3 invoice-col">
												Derrick / Crane description (Number)
												<address>
													<input type="text" name="txtCraneNumber" id="txtCraneNumber" class="form-control"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CCRANE_NUMBER','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-3 invoice-col">
												  Derrick / Crane description (SWL)
												<address>
													<input type="text" name="txtCraneSWL" id="txtCraneSWL" class="form-control"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CCRANE_SWL','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												Derrick / Crane description (location)
												<address>
													<input type="text" name="txtCraneLocation" id="txtCraneLocation" class="form-control"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CCRANE_LOCATION','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												What is maximum outreach of cranes / derricks outboard of the ship's side(M)
												<address>
													<input type="text" name="txtCraneOutreach" id="txtCraneOutreach" class="form-control"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('CCRANE_OUTREACH','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
												</address>
											</div><!-- /.col -->
										 </div>
										 
										 
										 <div class="row">
											<div class="col-xs-12">
												<h2 class="page-header">
													Ship To Ship Transfer (STS)
												</h2>                        
											</div><!-- /.col -->
										</div>
										 
										 <div class="row invoice-info">
											<div class="col-sm-3 invoice-col">
												Does vessel comply with recommendations contained in OCIMF/ICS Ship To Ship Transfer Guide (Petroleum or Liquified Gas, as applicable)
												<address>
													<input type="radio" id="rdoOCIMFICS1" name="rdoOCIMFICS" value="1" <?php if($rdoOCIMFICS == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoOCIMFICS2" name="rdoOCIMFICS" value="2" <?php if($rdoOCIMFICS == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
												</address>
											</div><!-- /.col -->
											
											<div class="col-sm-3 invoice-col">
												  
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												
											</div><!-- /.col -->
											<div class="col-sm-3 invoice-col">
												
											</div><!-- /.col -->
										 </div>
										
										
								</div><!-- /.tab_7-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Seven Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
									
								<div id="tab_8" class="tab-pane">
									<div class="row">
                                        <div class="col-xs-12">
                                            <h2 class="page-header">
                                             Engine Room
                                            </h2>                            
                                        </div><!-- /.col -->
                                     </div> 
                                    
                                    <div class="row invoice-info">
                                        <div class="col-sm-3 invoice-col">
                                            What type of fuel is used for main propulsion?
                                            <address>
                                                <input type="text" name="txtPropultionFuel" class="form-control" id="txtPropultionFuel"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('PROPULTION_FUEL','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
                                            </address>
                                        </div><!-- /.col -->
                                        
                                        <div class="col-sm-3 invoice-col">
                                            What type of fuel is used in the generating plant?
                                            <address>
                                                <input type="text" name="txtPlantFuel" id="txtPlantFuel" class="form-control"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('PLANT_FUEL','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
                                            </address>
                                        </div><!-- /.col -->
                                        <div class="col-sm-3 invoice-col">
                                            Capacity of bunker tanks - IFO(M3)
                                            <address>
                                                <input type="text" name="txtIFO_Capacity" id="txtIFO_Capacity" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('IFO_CAPACITY','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
                                            </address>
                                        </div><!-- /.col -->
                                        <div class="col-sm-3 invoice-col">
                                            Capacity of bunker tanks -  MDO/MGO(M3)
                                            <address>
                                                <input type="text" name="txtDO_Capacity" id="txtDO_Capacity" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('DO_CAPACITY','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
                                            </address>
                                        </div><!-- /.col -->
                                     </div> 
                                    
                                    <div class="row invoice-info">
                                        <div class="col-sm-3 invoice-col">
                                            Is vessel fitted with fixed or controllable pitch propeller(s)?
                                            <address>
                                                <input type="radio" id="rdoPitch1" name="rdoPitch" value="1" <?php if($rdoPitch == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Fixed Pitch &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoPitch2" name="rdoPitch" value="2" <?php if($rdoPitch == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Controllable Pitch
                                            </address>
                                        </div><!-- /.col -->
                                    </div> 
                                     
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h2 class="page-header">
                                             Insurance
                                            </h2>                            
                                        </div><!-- /.col -->
                                    </div> 
                                    
                                    <div class="row invoice-info">
                                        <div class="col-sm-6 invoice-col">
                                            P & I Club - Full Style
                                            <address>
                                                <textarea name="txtP_I_CLUB" id="txtP_I_CLUB" class="form-control areasize" ><?php echo $obj->getVesselParticularData('P_I_CLUB','vessel_master_tankers',$_REQUEST['id']);?></textarea>
                                            </address>
                                        </div><!-- /.col -->
                                        
                                        <div class="col-sm-3 invoice-col">
                                            P & I Club coverage - pollution liability coverage(USD)
                                            <address>
                                                <input type="text" name="txtP_I_Coverage" id="txtP_I_Coverage" class="form-control numeric"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('P_I_COVERAGE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
                                            </address>
                                        </div><!-- /.col -->
                                    </div> 
                                    
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h2 class="page-header">
                                             Port State Control
                                            </h2>                            
                                        </div><!-- /.col -->
                                    </div> 
                                    
                                    <div class="row invoice-info">
                                        <div class="col-sm-3 invoice-col">
                                            Date of last Port State Control inspection
                                            <address>
                                                <input type="text" name="txtLastPortStateDate" id="txtLastPortStateDate" class="form-control datepiker"  autocomplete="off" value="<?php if($obj->checkVesselDate('LAST_PORT_STATE_DATE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('LAST_PORT_STATE_DATE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('LAST_PORT_STATE_DATE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="" />
                                            </address>
                                        </div><!-- /.col -->
                                        
                                        <div class="col-sm-3 invoice-col">
                                            Place of last Port State Control inspection
                                            <address>
                                                <input type="text" name="txtLastPortStatePlace" id="txtLastPortStatePlace" class="form-control"  autocomplete="off" value="<?php echo $obj->getVesselParticularData('LAST_PORT_STATE_PLACE','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
                                            </address>
                                        </div><!-- /.col -->
                                        
                                        <div class="col-sm-3 invoice-col">
                                            Any outstanding deficiencies as reported by any Port State Control
                                            <address>
                                                <input type="radio" id="rdoIsReported1" name="rdoIsReported" value="1" <?php if($rdoIsReported == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoIsReported2" name="rdoIsReported" value="2" <?php if($rdoIsReported == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
                                            </address>
                                        </div><!-- /.col -->
                                        
                                        <div class="col-sm-3 invoice-col">
                                            If yes, provide details
                                            <address>
                                                <input type="text" name="txtReportedDetails" id="txtReportedDetails" class="form-control" <?php if($rdoIsReported == 2){echo "readonly";}?>  autocomplete="off" value="<?php echo $obj->getVesselParticularData('REPORTED_DETAILS','vessel_master_tankers',$_REQUEST['id']);?>" placeholder="" />
                                            </address>
                                        </div><!-- /.col -->
                                    </div> 
                                    
                                    
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h2 class="page-header">
                                             Recent Operational History
                                            </h2>                            
                                        </div><!-- /.col -->
                                    </div> 
                                    
                                    <div class="row invoice-info">
                                        <div class="col-sm-3 invoice-col">
                                            Has vessel been involved in a pollution during the past 12 months?
                                            <address>
                                                <input type="radio" id="rdoIsPolution1" name="rdoIsPolution" value="1" <?php if($rdoIsPolution == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoIsPolution2" name="rdoIsPolution" value="2" <?php if($rdoIsPolution == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
                                            </address>
                                        </div><!-- /.col -->
                                        
                                        <div class="col-sm-3 invoice-col">
                                            If yes, provide details
                                            <address>
                                                <textarea name="txtPolutionDetails" id="txtPolutionDetails" class="form-control areasize" <?php if($rdoIsPolution == 2){echo "readonly";}?>  autocomplete="off" ><?php echo $obj->getVesselParticularData('POLUTION_DETAILS','vessel_master_tankers',$_REQUEST['id']);?></textarea>
                                            </address>
                                        </div><!-- /.col -->
                                        <div class="col-sm-3 invoice-col">
                                            Has vessel been involved in a grounding during the past 12 months?
                                            <address>
                                                <input type="radio" id="rdoIsGrounding1" name="rdoIsGrounding" value="1" <?php if($rdoIsGrounding == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoIsGrounding2" name="rdoIsGrounding" value="2" <?php if($rdoIsGrounding == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
                                            </address>
                                        </div><!-- /.col -->
                                        
                                        <div class="col-sm-3 invoice-col">
                                            If yes, provide details
                                            <address>
                                                <textarea name="txtGroundingDetails" id="txtGroundingDetails" class="form-control areasize" <?php if($rdoIsGrounding == 2){echo "readonly";}?>  autocomplete="off" ><?php echo $obj->getVesselParticularData('GROUNDING_DETAILS','vessel_master_tankers',$_REQUEST['id']);?></textarea>
                                            </address>
                                        </div><!-- /.col -->
                                    </div> 
                                    
                                    <div class="row invoice-info">
                                        <div class="col-sm-3 invoice-col">
                                            Has vessel been involved in a serious casualty during the past 12 months?
                                            <address>
                                                <input type="radio" id="rdoIsCasualty1" name="rdoIsCasualty" value="1" <?php if($rdoIsCasualty == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoIsCasualty2" name="rdoIsCasualty" value="2" <?php if($rdoIsCasualty == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
                                            </address>
                                        </div><!-- /.col -->
                                        
                                        <div class="col-sm-3 invoice-col">
                                            If yes, provide details
                                            <address>
                                                <textarea name="txtCasualtDetails" id="txtCasualtyDetails" class="form-control areasize" <?php if($rdoIsCasualty == 2){echo "readonly";}?>  autocomplete="off" ><?php echo $obj->getVesselParticularData('CASUALTY_DETAILS','vessel_master_tankers',$_REQUEST['id']);?></textarea>
                                            </address>
                                        </div><!-- /.col -->
                                        <div class="col-sm-3 invoice-col">
                                            Has vessel been involved in a collision incident during the past 12 months?
                                            <address>
                                                <input type="radio" id="rdoIsAccident1" name="rdoIsAccident" value="1" <?php if($rdoIsAccident == 1){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="rdoIsAccident2" name="rdoIsAccident" value="2" <?php if($rdoIsAccident == 2){echo "checked";}?>/>&nbsp;&nbsp;&nbsp;No
                                            </address>
                                        </div><!-- /.col -->
                                        
                                        <div class="col-sm-3 invoice-col">
                                            If yes, provide details
                                            <address>
                                                <textarea name="txtAccidentDetails" id="txtAccidentDetails" class="form-control areasize" <?php if($rdoIsAccident == 2){echo "readonly";}?>  autocomplete="off" ><?php echo $obj->getVesselParticularData('ACCIDENT_DETAILS','vessel_master_tankers',$_REQUEST['id']);?></textarea>
                                            </address>
                                        </div><!-- /.col -->
                                    </div>
                                    
                                     
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h2 class="page-header">
                                             Vetting
                                            </h2>                            
                                        </div><!-- /.col -->
                                    </div> 
                                    
                                    <div class="row invoice-info">
                                        <div class="col-sm-3 invoice-col">
                                            Date of last SIRE Inspection
                                            <address>
                                                <input name="txtLastSireDate" type="text" class="form-control datepiker" id="txtLastSireDate"  autocomplete="off" value="<?php if($obj->checkVesselDate('LAST_SIRE_DATE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('LAST_SIRE_DATE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('LAST_SIRE_DATE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy"/>
                                            </address>
                                        </div><!-- /.col -->
                                        <div class="col-sm-3 invoice-col">
                                            Place of last SIRE Inspection
                                            <address>
                                                <select  name="selSirePort" class="select form-control" id="selSirePort">
                                                    <?php $obj->getPortList(); ?>
                                                </select>
                                                <script>$('#selSirePort').val(<?php echo $obj->getVesselParticularData('SIRE_PORT','vessel_master_tankers',$_REQUEST['id']);?>);</script>
                                            </address>
                                        </div><!-- /.col -->
                                        <div class="col-sm-3 invoice-col">
                                            Date of last CDI Inspection
                                            <address>
                                                <input name="txtLastCDIDate" type="text" class="form-control datepiker" id="txtLastCDIDate"  autocomplete="off" value="<?php if($obj->checkVesselDate('LAST_CDI_DATE','vessel_master_tankers',$_REQUEST['id']) == "0000-00-00" || $obj->checkVesselDate('LAST_CDI_DATE','vessel_master_tankers',$_REQUEST['id']) == ""){ echo ""; }else{ echo date("d-m-Y",strtotime($obj->checkVesselDate('LAST_CDI_DATE','vessel_master_tankers',$_REQUEST['id']))); } ?>" placeholder="dd-mm-yyyy"/>
                                            </address>
                                        </div><!-- /.col -->
                                        <div class="col-sm-3 invoice-col">
                                            Place of last CDI Inspection
                                            <address>
                                                <select  name="selCDIPort" class="select form-control" id="selCDIPort">
                                                    <?php $obj->getPortList(); ?>
                                                </select>
                                                <script>$('#selCDIPort').val(<?php echo $obj->getVesselParticularData('CDI_PORT','vessel_master_tankers',$_REQUEST['id']);?>);</script>
                                            </address>
                                        </div><!-- /.col -->
                                    </div>
									<div class="col-sm-4 invoice-col"><input type="hidden" name="txtCRM" id="txtCRM" value="" /></div>
								</div><!-- /.tab_8-pane -->
								
								<!----------------------------------------------------------------------------------------------------------->
								<!------------------------------------------End Tab Eight Form------------------------------------------------>
								<!----------------------------------------------------------------------------------------------------------->
								
							</div><!-- /.tab-content -->
						</div><!-- nav-tabs-custom -->
					</div>
				</div>
                
                    <div class="box-footer" align="right">
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate1();">Submit</button>
						<input type="hidden" name="action" value="submit">
					</div>
                  </form>
				<!--  content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script src='../../js/jquery.numeric.js'></script>
<script type="text/javascript">

$(document).ready(function(){
	$(".numeric").numeric();

$(".areasize").autosize({append: "\n"});
$(".numeric").numeric();
$('.datepiker').datepicker({
    format: 'dd-mm-yyyy',
	autoclose: true
});

$("#frm1").validate({
	rules: {
		
	},
	messages: {
		
	},
	submitHandler: function(form)  {
		for(var i =1;i<=$("#txtCID").val();i++)
		{
			var var6 = $("[id^=file1_"+i+"_]").map(function () {return this.value;}).get().join(",");
			$('#txtCRMFILE_'+i).val(var6);
			var var7 = $("[id^=name1_"+i+"_]").map(function () {return this.value;}).get().join(",");
			$('#txtCRMNAME_'+i).val(var7);
		}
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

/*$('#selFlag').val(<?php echo $obj->getVesselIMOData($_REQUEST['id'],"FLAG"); ?>);
$('#selFlag_1').val(<?php echo $obj->getVesselIMOData($_REQUEST['id'],"FLAG"); ?>);
$('#selCountry').val(<?php echo $obj->getVesselParticularData('COUNTRY_ID','vessel_master_1',$_REQUEST['id']); ?>);
$('#selPort').val(<?php echo $obj->getVesselParticularData('PORT_ID','vessel_master_1',$_REQUEST['id']); ?>);
$('#selIVFFTO_1').val(<?php echo $obj->getVesselParticularData('PANAMA_CANAL','vessel_master_1',$_REQUEST['id']); ?>);
$('#selIVFFTO_2').val(<?php echo $obj->getVesselParticularData('SUEZ_CANAL','vessel_master_1',$_REQUEST['id']); ?>);
$('#selIVFFTO_3').val(<?php echo $obj->getVesselParticularData('SEAWAY','vessel_master_1',$_REQUEST['id']); ?>);
$('#selPanama').val(<?php echo $obj->getVesselParticularData('PANAMA_RADIUS','vessel_master_1',$_REQUEST['id']); ?>);
$('#selSHAFT_GEN').val(<?php echo $obj->getVesselParticularData('SHAFT_GEN','vessel_master_1',$_REQUEST['id']); ?>);

$('#selCLASS_SOC').val(<?php echo $obj->getVesselIMOData($_REQUEST['id'],"CLA_SOC_ID"); ?>);
$('#selCLASS_1').val(<?php echo $obj->getVesselParticularData('CLASS_1','vessel_master_2',$_REQUEST['id']); ?>);
$('#selSTR').val(<?php echo $obj->getVesselParticularData('STR','vessel_master_2',$_REQUEST['id']); ?>);
$('#selSTR_1').val(<?php echo $obj->getVesselParticularData('STR_1','vessel_master_2',$_REQUEST['id']); ?>);
$('#selPOLD').val(<?php echo $obj->getVesselParticularData('PORT_DRYDOCK','vessel_master_2',$_REQUEST['id']); ?>);
$('#selISM').val(<?php echo $obj->getVesselParticularData('ISM','vessel_master_2',$_REQUEST['id']); ?>);
$('#selADVISE_PORT').val(<?php echo $obj->getVesselParticularData('PORT_ADVISE','vessel_master_2',$_REQUEST['id']); ?>);
$('#selDETEN').val(<?php echo $obj->getVesselParticularData('DETEN','vessel_master_2',$_REQUEST['id']); ?>); 

$('#selHOLDS_CLEAR').val(<?php echo $obj->getVesselParticularData('HOLDS_CLEAR','vessel_master_4',$_REQUEST['id']); ?>);
$('#selTANKTOP').val(<?php echo $obj->getVesselParticularData('TANKTOP','vessel_master_4',$_REQUEST['id']); ?>);
$('#selCO2').val(<?php echo $obj->getVesselParticularData('CO2','vessel_master_4',$_REQUEST['id']); ?>);
$('#selDET_SYS').val(<?php echo $obj->getVesselParticularData('DET_SYS','vessel_master_4',$_REQUEST['id']); ?>);
$('#selLADDER').val(<?php echo $obj->getVesselParticularData('LADDER','vessel_master_4',$_REQUEST['id']); ?>);
$('#selCALC').val(<?php echo $obj->getVesselParticularData('CALC','vessel_master_4',$_REQUEST['id']); ?>);
$('#selHOLD_SIDE').val(<?php echo $obj->getVesselParticularData('HOLD_SIDE','vessel_master_4',$_REQUEST['id']); ?>);
$('#selF_BULKHEAD').val(<?php echo $obj->getVesselParticularData('F_BULKHEAD','vessel_master_4',$_REQUEST['id']); ?>);
$('#selAFT_BULKHEAD').val(<?php echo $obj->getVesselParticularData('AFT_BULKHEAD','vessel_master_4',$_REQUEST['id']); ?>);
$('#selSHAPED').val(<?php echo $obj->getVesselParticularData('SHAPED','vessel_master_4',$_REQUEST['id']); ?>);
$('#selH_LIFT').val(<?php echo $obj->getVesselParticularData('H_LIFT','vessel_master_4',$_REQUEST['id']); ?>);
$('#selE_HYDRA').val(<?php echo $obj->getVesselParticularData('E_HYDRA','vessel_master_4',$_REQUEST['id']); ?>);
$('#selN_WORK').val(<?php echo $obj->getVesselParticularData('N_WORK','vessel_master_4',$_REQUEST['id']); ?>); 

$('#selCARGO_1').val(<?php echo $obj->getVesselParticularData('CARGO_1','vessel_master_6',$_REQUEST['id']); ?>); 

$('#selFEU').val(<?php echo $obj->getVesselParticularData('FEU','vessel_master_7',$_REQUEST['id']); ?>);
$('#selCOVERS').val(<?php echo $obj->getVesselParticularData('COVERS','vessel_master_7',$_REQUEST['id']); ?>);
$('#selTWEENS').val(<?php echo $obj->getVesselParticularData('TWEENS','vessel_master_7',$_REQUEST['id']); ?>);
$('#selFLUSH').val(<?php echo $obj->getVesselParticularData('FLUSH','vessel_master_7',$_REQUEST['id']); ?>);
$('#selB_FITTED').val(<?php echo $obj->getVesselParticularData('B_FITTED','vessel_master_7',$_REQUEST['id']); ?>);
$('#selCO2_FITT').val(<?php echo $obj->getVesselParticularData('CO2_FITT','vessel_master_7',$_REQUEST['id']); ?>);

$('#selCLASS_SOC_1').val(<?php echo $obj->getVesselIMOData($_REQUEST['id'],"CLA_SOC_ID"); ?>);*/
    $('#rdoIceClass1').on('ifChecked', function () { 
	    $("#txtIceClass").removeAttr('readonly');
	});
	 
	$('#rdoIceClass1').on('ifUnchecked', function () { 
	    $("#txtIceClass").attr('readonly',true);
	});
	
	$('#rdoIceClass2').on('ifChecked', function () { 
	    $("#txtIceClass").attr('readonly',true);
	});
	
	$('#rdoIceClass2').on('ifUnchecked', function () { 
		$("#txtIceClass").removeAttr('readonly');
	});
	
	
	$('#rdoIsStatement1').on('ifChecked', function () { 
	    $("#txtExpiryStatDate").removeAttr('disabled');
	});
	 
	$('#rdoIsStatement1').on('ifUnchecked', function () { 
	    $("#txtExpiryStatDate").attr('disabled',true);
	});
	
	$('#rdoIsStatement2').on('ifChecked', function () { 
	    $("#txtExpiryStatDate").attr('disabled',true);
	});
	
	$('#rdoIsStatement2').on('ifUnchecked', function () { 
		$("#txtExpiryStatDate").removeAttr('disabled');
	});
	
	
	$('#rdoIsSDWT1').on('ifChecked', function () { 
	    $("#txtASSDEADWEIGHT").removeAttr('readonly');
	});
	 
	$('#rdoIsSDWT1').on('ifUnchecked', function () { 
	    $("#txtASSDEADWEIGHT").attr('readonly',true);
	});
	
	$('#rdoIsSDWT2').on('ifChecked', function () { 
	    $("#txtASSDEADWEIGHT").attr('readonly',true);
	});
	
	$('#rdoIsSDWT2').on('ifUnchecked', function () { 
		$("#txtASSDEADWEIGHT").removeAttr('readonly');
	});
	
	
	$('#rdoIsReported1').on('ifChecked', function () { 
	    $("#txtReportedDetails").removeAttr('readonly');
	});
	 
	$('#rdoIsReported1').on('ifUnchecked', function () { 
	    $("#txtReportedDetails").attr('readonly',true);
	});
	
	$('#rdoIsReported2').on('ifChecked', function () { 
	    $("#txtReportedDetails").attr('readonly',true);
	});
	
	$('#rdoIsReported2').on('ifUnchecked', function () { 
		$("#txtReportedDetails").removeAttr('readonly');
	});
	
	
	$('#rdoIsPolution1').on('ifChecked', function () { 
	    $("#txtPolutionDetails").removeAttr('readonly');
	});
	 
	$('#rdoIsPolution1').on('ifUnchecked', function () { 
	    $("#txtPolutionDetails").attr('readonly',true);
	});
	
	$('#rdoIsPolution2').on('ifChecked', function () { 
	    $("#txtPolutionDetails").attr('readonly',true);
	});
	
	$('#rdoIsPolution2').on('ifUnchecked', function () { 
		$("#txtPolutionDetails").removeAttr('readonly');
	});
	
	
	$('#rdoIsGrounding1').on('ifChecked', function () { 
	    $("#txtGroundingDetails").removeAttr('readonly');
	});
	 
	$('#rdoIsGrounding1').on('ifUnchecked', function () { 
	    $("#txtGroundingDetails").attr('readonly',true);
	});
	
	$('#rdoIsGrounding2').on('ifChecked', function () { 
	    $("#txtGroundingDetails").attr('readonly',true);
	});
	
	$('#rdoIsGrounding2').on('ifUnchecked', function () { 
		$("#txtGroundingDetails").removeAttr('readonly');
	});
	
	
	$('#rdoIsCasualty1').on('ifChecked', function () { 
	    $("#txtCasualtyDetails").removeAttr('readonly');
	});
	 
	$('#rdoIsCasualty1').on('ifUnchecked', function () { 
	    $("#txtCasualtyDetails").attr('readonly',true);
	});
	
	$('#rdoIsCasualtyn2').on('ifChecked', function () { 
	    $("#txtCasualtyDetails").attr('readonly',true);
	});
	
	$('#rdoIsCasualtyn2').on('ifUnchecked', function () { 
		$("#txtCasualtyDetails").removeAttr('readonly');
	});
	
	
	$('#rdoIsAccident1').on('ifChecked', function () { 
	    $("#txtAccidentDetails").removeAttr('readonly');
	});
	 
	$('#rdoIsAccident1').on('ifUnchecked', function () { 
	    $("#txtAccidentDetails").attr('readonly',true);
	});
	
	$('#rdoIsAccident2').on('ifChecked', function () { 
	    $("#txtAccidentDetails").attr('readonly',true);
	});
	
	$('#rdoIsAccident2').on('ifUnchecked', function () { 
		$("#txtAccidentDetails").removeAttr('readonly');
	});
	
	
	$('#rdoOCIMFSize1').on('ifChecked', function () { 
	    $("#txtOCIMFSize").attr('readonly',true);
	});
	 
	$('#rdoOCIMFSize1').on('ifUnchecked', function () { 
	    $("#txtOCIMFSize").removeAttr('readonly');
	});
	
	$('#rdoOCIMFSize2').on('ifChecked', function () { 
	    $("#txtOCIMFSize").removeAttr('readonly');
	});
	
	$('#rdoOCIMFSize2').on('ifUnchecked', function () { 
		$("#txtOCIMFSize").attr('readonly',true);
	});
	
	
	$('#rdoBulkhead1').on('ifChecked', function () { 
	    $("#txtBulkhead").removeAttr('readonly');
	});
	 
	$('#rdoBulkhead1').on('ifUnchecked', function () { 
	    $("#txtBulkhead").attr('readonly',true);
	});
	
	$('#rdoBulkhead2').on('ifChecked', function () { 
	    $("#txtBulkhead").attr('readonly',true);
	});
	
	$('#rdoBulkhead2').on('ifUnchecked', function () { 
		$("#txtBulkhead").removeAttr('readonly');
	});
	
	
	$('#rdoCargoTankRestrictions1').on('ifChecked', function () { 
	    $("#txtCargoTankRestrictions").removeAttr('readonly');
	});
	 
	$('#rdoCargoTankRestrictions1').on('ifUnchecked', function () { 
	    $("#txtCargoTankRestrictions").attr('readonly',true);
	});
	
	$('#rdoCargoTankRestrictions2').on('ifChecked', function () { 
	    $("#txtCargoTankRestrictions").attr('readonly',true);
	});
	
	$('#rdoCargoTankRestrictions2').on('ifUnchecked', function () { 
		$("#txtCargoTankRestrictions").removeAttr('readonly');
	});
	
	
	$('#rdoOverFillAlarm1').on('ifChecked', function () { 
	    $("#txtOverFillAlarm").removeAttr('readonly');
	});
	 
	$('#rdoOverFillAlarm1').on('ifUnchecked', function () { 
	    $("#txtOverFillAlarm").attr('readonly',true);
	});
	
	$('#rdoOverFillAlarm2').on('ifChecked', function () { 
	    $("#txtOverFillAlarm").attr('readonly',true);
	});
	
	$('#rdoOverFillAlarm2').on('ifUnchecked', function () { 
		$("#txtOverFillAlarm").removeAttr('readonly');
	});
	
	
	$('#rdoFittedStermManifold1').on('ifChecked', function () { 
	    $("#txtFittedStermManifold").removeAttr('readonly');
	});
	 
	$('#rdoFittedStermManifold1').on('ifUnchecked', function () { 
	    $("#txtFittedStermManifold").attr('readonly',true);
	});
	
	$('#rdoFittedStermManifold2').on('ifChecked', function () { 
	    $("#txtFittedStermManifold").attr('readonly',true);
	});
	
	$('#rdoFittedStermManifold2').on('ifUnchecked', function () { 
		$("#txtFittedStermManifold").removeAttr('readonly');
	});
	
	$('#rdoTankCoiled1').on('ifChecked', function () { 
	    $("#txtTankCoiled").removeAttr('readonly');
	});
	 
	$('#rdoTankCoiled1').on('ifUnchecked', function () { 
	    $("#txtTankCoiled").attr('readonly',true);
	});
	
	$('#rdoTankCoiled2').on('ifChecked', function () { 
	    $("#txtTankCoiled").attr('readonly',true);
	});
	
	$('#rdoTankCoiled2').on('ifUnchecked', function () { 
		$("#txtTankCoiled").removeAttr('readonly');
	});
	
	
	$('#rdoICSHelicopter1').on('ifChecked', function () { 
	    $("#txtICSHelicopter").removeAttr('readonly');
	});
	 
	$('#rdoICSHelicopter1').on('ifUnchecked', function () { 
	    $("#txtICSHelicopter").attr('readonly',true);
	});
	
	$('#rdoICSHelicopter2').on('ifChecked', function () { 
	    $("#txtICSHelicopter").attr('readonly',true);
	});
	
	$('#rdoICSHelicopter2').on('ifUnchecked', function () { 
		$("#txtICSHelicopter").removeAttr('readonly');
	});
});

function addCertificateRow()
{
	var id = $("#txtCID").val();
	if($("#selCert_"+id).val() == 0)
	{
		jAlert('Please Select Certificate Name', 'Alert Dialog');
		$("#selCert_"+id).focus();
	}
	else
	{
		id  = (id - 1 )+ 2;
		$('<tr height="10"><td><select name="selCert_'+id+'" type="text" class="select form-control" id="selCert_'+id+'" autocomplete="off" value="" onchange="getCertificate('+id+');"><?php $obj->getCertificateList();?></select></td><td><input name="txtIDate_'+id+'" type="text" class="form-control datepiker" id="txtIDate_'+id+'" autocomplete="off" value="" placeholder="dd-mm-yyyy" /></td><td><input name="txtLAIDate_'+id+'" type="text" class="form-control datepiker" id="txtLAIDate_'+id+'" autocomplete="off" value="" placeholder="dd-mm-yyyy" /></td><td><input name="txtEDate_'+id+'" type="text" class="form-control datepiker" id="txtEDate_'+id+'" autocomplete="off" value="" placeholder="dd-mm-yyyy" /></td><td><div class="btn btn-success btn-file btn-flat" data-toggle="tooltip"><i class="fa fa-paperclip"></i>&nbsp;Attachment<input type="file" class="form-control" multiple name="attach_file_1[]" id="attach_file_1" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/></div></td></tr>').appendTo("#tb2");
		$("#txtCID").val(id);
		$('.datepiker').datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true
		});
	}
}


function getValidate1()
{
    for(var i =1;i<=$("#txtCID").val();i++)
	{
		var var6 = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
		$('#txtCRMFILE_'+i).val(var6);
		var var7 = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
		$('#txtCRMNAME_'+i).val(var7);
	}
	return true;
}

function Del_Upload(var1,val2)
{
	jConfirm('Are you sure you want to remove this attachment permanently ?', 'Confirmation', function(r) {
	if(r){
			$("#row_file_"+var1+"_"+val2).remove();	
		 }
	else{return false;}
	});
}


function getPdf(var1)
{
	location.href='allPdf.php?id=64&vesselid='+var1;
}

</script>
	</body>
</html>