<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");

require_once("../../includes/functions_internal_user_dryout.inc.php");

$obj = new data();

$display = new display();
$display->logout_iu();

/* get customer detail code start */
$custid = base64_decode($_GET['customerid']);
$customerdetail = $obj->getCustomerCompleteDetail($custid);
$fetchcustomerrecord = mysql_fetch_array($customerdetail);

/* end code */
/*if (@$_REQUEST['action'] == 'submit')
{
 	
	$submitcustomerdetail = $obj->insertCustomerDetails();
	 if($submitcustomerdetail->status == true)
			{
		        $successmessage = $submitcustomerdetail->msg;
	        }
	   else if ($submitcustomerdetail->status == false) {
                $responseerror =  $submitcustomerdetail->msg;
                  
            } 
}*/
$pagename = basename($_SERVER['PHP_SELF']);

$rigts    = explode(',',$obj->getUserRights($_SESSION['uid'],$_SESSION['moduleid'],18));
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
	<?php $display->js(); ?>

<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(18); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Customer Detail&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Customers&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Detail</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="customerlist.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
					   <?php
						echo (isset($successmessage))? '<div class="alert alert-primary" style="color:red;">'.$successmessage.'</div>':'';
						echo (isset($responseerror))? '<div class="alert alert-primary" style="color:red;">'.$responseerror.'</div>':'';
						?>
				<form role="form" name="frmnew" id="frmnew" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">	
					
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             Customer Detail<select name="selVendor" id="selVendor" style="display:none;"><?php //$obj->getVendorListNewUpdate();?></select>
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
							<b><u>Customer's Name</u></b>
                            <address>
                        <?php echo $fetchcustomerrecord['name'];?>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							<b><u>Father's Name</u></b>
                            <address>
                        <?php echo $fetchcustomerrecord['fathername'];?>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							<b><u>Mother's Name</u></b>
                            <address>
                          <?php echo $fetchcustomerrecord['motherame'];?>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							<b><u>Date of Birth</u></b>
                            <address>
                             <?php echo date("d-m-Y",strtotime($fetchcustomerrecord['DOB']));?>
                            </address>
                        </div><!-- /.col -->
					</div>
					
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
							<b><u>Email ID</u></b>
                            <address>
                              <?php echo $fetchcustomerrecord['emailid'];?>
                            </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
							<b><u>Phone Number</u></b>
                            <address>
                             
                             <?php echo $fetchcustomerrecord['contactno'];?>
                            </address>
                        </div>
						  <div class="col-sm-3 invoice-col">
							  <b><u>Permenent Address</u></b>
                            <address>
                             <?php echo $fetchcustomerrecord['PermenentAddress'];?>
                            </address>
                        </div>
						  <div class="col-sm-3 invoice-col">
							  <b><u>Nationality</u></b>
                            <address>
                             <?php echo $fetchcustomerrecord['Nationality'];?>
                            </address>
                        </div>
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
							<b><u>Date of Visit</u></b>
                            <address>
                        <?php echo date("d-m-Y",strtotime($fetchcustomerrecord['Dateofvisit']));?>
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-3 invoice-col">
							<b><u>Check In Time</u></b>
                            <address>
                            <?php echo date("g:i a",$fetchcustomerrecord['checkintime']);?>
                             </address>
                        </div><!-- /.col -->
						  <div class="col-sm-3 invoice-col">
							  <b><u>Departure Date</u></b>
                            <address>
                           <?php echo date("d-m-Y",strtotime($fetchcustomerrecord['DepartureDate']));?>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							<b><u>Check Out Time</u></b>
                            <address>
                              <?php echo date("g:i a",$fetchcustomerrecord['checkouttime']);?>
                             </address>
                        </div><!-- /.col -->
                       <!-- /.col -->
                        
					</div>
					  <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
							<b><u>Coming From</u></b>
                            <address>
                           <?php echo $fetchcustomerrecord['ComingFrom'];?>
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-3 invoice-col">
							<b><u>Going to</u></b>
                            <address>
                             <?php echo $fetchcustomerrecord['GoingTo'];?>
                             </address>
                        </div><!-- /.col -->
						  <div class="col-sm-3 invoice-col">
							  <b><u>Purpose of Visit</u></b>
                            <address>
							<?php echo $fetchcustomerrecord['PurposeofVisit'];?>	
                            </address>
                        </div>
						  <div class="col-sm-3 invoice-col">
							  <b><u>Number of Persons</u></b>
                            <address>
                            Male - <?php echo $fetchcustomerrecord['Male'];?>
							Female - <?php echo $fetchcustomerrecord['Female'];?>
							Children - <?php echo $fetchcustomerrecord['Children'];?>
                            </address>
                        </div><!-- /.col -->
                       
                       <!-- /.col -->
                        
					</div>
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
							<b><u>Passport No</u></b>
                            <address>
								<?php echo $fetchcustomerrecord['PassportNo'];?>	
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-3 invoice-col">
							<b><u>Date of Issue</u></b>
                            <address>
                             <?php echo date("d-m-Y",strtotime($fetchcustomerrecord['Passportissuedate']));?>
                             </address>
                        </div><!-- /.col -->
						  <div class="col-sm-3 invoice-col">
							  <b><u>Validity</u></b> 
                            <address>
                     <?php echo date("d-m-Y",strtotime($fetchcustomerrecord['Passportexpirydate']));?>
                            </address>
                        </div><!-- /.col -->
                       
                       <!-- /.col -->
                        
					</div>
					<div class="row invoice-info">
                        <div class="col-sm-2 invoice-col">
							<b><u>Room No</u></b>
                            <address>
                           <?php echo $fetchcustomerrecord['Roomno'];?>
							
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-2 invoice-col">
							<b><u>Room Rent</u></b>
                            <address>
                            <?php echo $fetchcustomerrecord['Roomrent'];?>
                             </address>
                        </div><!-- /.col -->
						  <div class="col-sm-3 invoice-col">
							  <b><u>Amount</u></b>
                            <address>
                        <?php echo $fetchcustomerrecord['Amount'];?>
                            </address>
                        </div>
						  <div class="col-sm-3 invoice-col">
							  <b><u>Paid by Customer</u></b>
                            <address>
                        <?php echo $fetchcustomerrecord['PaidbyCustomer'];?>
                            </address>
                        </div>
						 <div class="col-sm-2 invoice-col">
							 <b><u>Balance</u></b>
                            <address>
						<?php echo $fetchcustomerrecord['Balance'];?>
                            </address>
                        </div>
                       
                       <!-- /.col -->
                        
					</div>
                    <div class="row invoice-info">
						 <div class="col-sm-3 invoice-col">
							 <b><u> Aadhar Card(Primary Customer)</u></b>
                            <address>
                               <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['aadharcard'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['aadharcard'];?>" style="width:200px;height:200px;"></a>
                            </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
							<b><u>Voter Id</u></b>
                            <address>
                                <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['voterid'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['voterid'];?>" style="width:200px;height:200px;"></a>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							<b><u>Pen Card</u></b>
                            <address>
								<a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['pencard'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['pencard'];?>" style="width:200px;height:200px;"></a>
                            </address>
                        </div><!-- /.col -->
						 <div class="col-sm-3 invoice-col">
							 <b><u>Profile Photo</u></b>
                            <address>
                                <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['profilephoto'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['profilephoto'];?>" style="width:200px;height:200px;"></a>
                            </address>
                        </div>
						<!--<div class = "newone">
                        <div class="col-sm-3 invoice-col">
                     &nbsp;
                          <address>
                               <input name="rdoWstype" id="rdoWstype1" type="radio" style="cursor:pointer;" value="1"     checked  />&nbsp;&nbsp;WS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:10px; font-style:italic;"></span><input name="rdoWstype" id="rdoWstype2" type="radio" style="cursor:pointer;" value="2" />&nbsp;&nbsp;Lumpsum
                           <address>
                        </div>
                        <div class="col-sm-3 invoice-col">
                          &nbsp;
                            <address>
                                <input type="text"  name="txtWsLumpsum"  class="form-control numeric" id="txtWsLumpsum" placeholder="USD" autocomplete="off" value=""/>
                            </address>
                         </div>
					     </div>-->
                        
					</div>
				   <div class="row invoice-info">
						 <div class="col-sm-3 invoice-col">
							 <b><u>ID Card(Member 1)</u></b>
                            <address>
                                <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmemberfirst'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmemberfirst'];?>" style="width:200px;height:200px;"></a>
                            </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
							<b><u>ID Card(Member 2)</u></b>
                            <address>
                                <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmembersecond'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmembersecond'];?>" style="width:200px;height:200px;"></a>
							</address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							<b><u>ID Card(Member 3)</u></b>
                            <address>
                                <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmemberthird'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmemberthird'];?>" style="width:200px;height:200px;"></a>
							</address>
                        </div><!-- /.col -->
						 <div class="col-sm-3 invoice-col">
							 <b><u>ID Card(Member 4)</u></b>
                            <address>
                                <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmemberfourth'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmemberfourth'];?>" style="width:200px;height:200px;"></a>
							 </address>
                        </div>
						
                        
					</div>
					<div class="row invoice-info">
						 <div class="col-sm-3 invoice-col">
							 <b><u>ID Card(Member 5)</u></b>
                            <address>
                                <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmemberfifth'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmemberfifth'];?>" style="width:200px;height:200px;"></a>
							 </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
							<b><u>ID Card(Member 6)</u></b>
                            <address>
                                <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmembersixth'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmembersixth'];?>" style="width:200px;height:200px;"></a>
							</address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
							<b><u>ID Card(Member 7)</u></b>
                            <address>
                                <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmemberseven'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmemberseven'];?>" style="width:200px;height:200px;"></a>
							</address>
                        </div><!-- /.col -->
						 <div class="col-sm-3 invoice-col">
							 <b><u>ID Card(Member 8)</u></b>
                            <address>
                                <a href="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmembereight'];?>" target="_blank"><img src="../../attachment/aadhar/<?php echo $fetchcustomerrecord['idmembereight'];?>" style="width:200px;height:200px;"></a>
							 </address>
                        </div>
						
                        
					</div>
                    
                    
                    
					<!--<div class="box-footer" align="right">
                    <?php if(in_array(2, $rigts)){?>
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Save as Draft</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-success btn-flat" onClick="return getValidate(1);">Send to Checker</button> 
					<?php }?>
                    <input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>-->
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>




	
		<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/sort.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />
<script src="../../js/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" href="../../css/chosen.css" rel="stylesheet" />

  <script src="../../js/jquery-3.4.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
//$("#selShipper,#selCharterer,#selOwner,#selReceiver").html($("#selVendor").html());
//$(".numeric").numeric();
//$(".areasize").autosize({append: "\n"});
/*$('#txtFDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#txtTDate').datepicker('setStartDate', new Date(getString($(this).val())));
 });*/
 
/*$('#dob,#dov').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){   });*/

 $("#dob").datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
});
/*$('#dob,#dov').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#dob').datepicker('setStartDate', new Date(getString($(this).val())));
 });*/
function getString(var1)
{
  var var2 = var1.split('-');  
  return var2[2]+'/'+var2[1]+'/'+var2[0];
}


$(".areasize").autosize({append: "\n"});
$("[id^=ui-datepicker-div]").hide();
$("#selVOPID").val(" ");


$("#frmnew").validate({
	rules: {
		
		
		/*     txtFreight: {required:function(){
			   if($("#selBType").val() == '1' || $("#selBType").val() == '3')
			   {
			       return true;
			   }
			   else
			   {
			       return false;
			   }
			  }},
			txtWsLumpsum: {required:function(){
			   if($("#selBType").val() == '2')
			   {
			       return true;
			   }
			   else
			   {
			       return false;
			   }
			  }}, */
		},
	messages: {
	
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
getVesselTypeList();
getCargoTypList();
//getShowhide();
	
	
	
});

	





function getVesselTypeList()
{
    var vesseltypelist = <?php echo $obj->getVesselTypeListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selVesselType');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selVesselType");
	$.each(vesseltypelist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selVesselType");
	});
}


function getCargoTypList()
{
    var cargolist = <?php echo $obj->getCargoListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selCargo');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selCargo");
	$.each(cargolist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selCargo");
	});
}


function getValidate(val)
{
	$("#upstatus").val(val);
	return true;
}
	
function getShowhide()
{
		    var selBType = $("#selBType").val();
		    $("#selBType").find("option:selected").each(function(){
            var optionValue = $("#selBType").attr("value");
            if(optionValue == '2'){
                $(".newone").show();
                $(".basef").hide();
            } else{
                $(".newone").hide();
                $(".basef").show();
            }
        });
}
	//function fillupp
</script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
$(document).ready(function() {
		 $('#rdoWstype1').click(function() {
		 if ($("input[name='rdoWstype']:checked").val() == '1')
            {
			$( "#txtWsLumpsum" ).attr('placeholder','WS')
			//$("#rdoWstype2").removeAttr("2");
			}
         });
		 $('#rdoWstype2').click(function() {
		 if ($("input[name='rdoWstype']:checked").val() == '2')
            {
			$( "#txtWsLumpsum" ).attr('placeholder','USD')
			//$("#rdoWstype1").removeAttr("1");
			}
         });
});
	
	function getBalance()
	{
		var paidamount = $("#paidbycustomer").val();
		var amount = $("#amount").val();
		if(isNaN(paidamount)){paidamount =0; }
		if(isNaN(amount)){amount =0;}
		var remainbalance = parseFloat(amount) - parseFloat(paidamount);
		if(isNaN(remainbalance)){remainbalance =0;}
		$("#balance").val(remainbalance.toFixed(2));
		
	}
</script>
    </body>
</html>