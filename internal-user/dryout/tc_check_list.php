<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id 	= $_REQUEST['id'];
$comid 	= $_REQUEST['comid'];
$page  	= $_REQUEST['page'];

if($page == 1){$page_link = "in_ops_tc.php";}else if($page == 2){$page_link = "vessel_in_post_tc.php";}else {$page_link = "vessel_in_history_tc.php";}

if (@$_REQUEST['action'] == 'submit')
{ 
 	$msg = $obj->insertchecklistDetailstc($id);
	header('Location:./'.$page_link.'?msg='.$msg);
}

$id = $obj->getLatestCostSheetIDTC($comid);
$obj->viewTCEstimationTempleteRecordsNew($id);
$result = mysql_query("select MESSAGE from chartering_estimate_tc_compare where COMID='".$comid."'") or die;
$rows1 = mysql_fetch_assoc($result);

$pagename 		= basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;
$uid	  		= $_SESSION['uid'];
$moduleid 		= $_SESSION['moduleid'];

if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);
$sql1 = "select DEL_DATE_EST, DEL_HFO_MT_EST, DEL_MGO_MT_EST from chartering_tc_estimate_slave1 where TCOUTID=".$obj->getFun1()." order by TC_SLAVE1ID ASC";
$result1 = mysql_query($sql1);
$rowsdel = mysql_fetch_assoc($result1);
$sql2 = "select REDEL_DATE_EST, REDEL_HFO_MT_EST, REDEL_MGO_MT_EST from chartering_tc_estimate_slave1 where TCOUTID=".$obj->getFun1()." order by TC_SLAVE1ID DESC";
$result2 = mysql_query($sql2);
$rowsredel = mysql_fetch_assoc($result2);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>


<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
	}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(21); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops TC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops TC&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Check List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right">
                    <a href="allPdf.php?id=61&comid=<?php echo $comid; ?>" title="CHeck List Pdf">
                        <button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                    </a>
                    <a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a>
                </div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
				
				<div class="row">
					<div class="col-xs-12">
					  <h3 style=" text-align:center;">TC CHECKLIST</h3>		                       
					</div><!-- /.col -->
				</div>
				<div style="height:10px;"></div>	
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
						 FIXTURE DETAILS
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				<?php
				$sql 		= "select * from check_list_tc where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";
				$res 		= mysql_query($sql);
				$num  		= mysql_num_rows($res);
				$rows 		= mysql_fetch_assoc($res);
				$chartPni 	= $rows['CHARTERER_PNI'];
				?>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					   TC No. 
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFun7();?></strong>
						</address>
					</div>
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
						VESSEL NAME
						<address>
							<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselIMOData($obj->getFun3(),'VESSEL_NAME');?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
						CP DATE
						<address>
							<strong>&nbsp;&nbsp;&nbsp;<?php echo date("d-M-Y",strtotime($obj->getFun9()));?>	</strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  CHARTERER
						<address>
							<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVendorListNewBasedOnID($obj->getFun11(),"NAME");?></strong>
						</address>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
						 VSL DETAILS
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					  BUILT
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFun16();?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  DEADWEIGHT
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFun30();?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  DRAFT
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFun31();?></strong>
						</address>
					</div>
				</div>
				<div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
					  GRT/NRT
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselParticularData('GRT_NRT','vessel_imo_master',$obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"))."/".$obj->getVesselParticularData('NRT','vessel_imo_master',$obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"));?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					<?php if($obj->getVesselIMOData($obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"),"BUSINESSTYPEID")==3){$tpc = $obj->getVesselParticularData('SUMMER_3','vessel_master_1',$obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"));}else{$tpc = $obj->getVesselParticularData('TPC_SUMMER','vessel_master_tankers',$obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"));}?>
					  TPC
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $tpc;?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  VESSEL PNI
						<address>
						<strong>&nbsp;&nbsp;&nbsp;
						<?php echo $obj->getVendorOnlyNameBasedOnID($obj->getVesselIMOData($obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"),'P_I'));?></strong>
						</address>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
							VESSEL CHECKS
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					  REG
						<address>
						<?php if($rows['REG']=="1"){$style_1 = 'checked';} else {$style_1 = '';}?>
							<input type="checkbox" name="regcheckbox" <?php echo $style_1; ?> id="regcheckbox" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  CLASS
						<address>
						<?php if($rows['CLASS']=="1"){$style_2 = 'checked';} else {$style_2 = '';}?>
							<input type="checkbox" name="classcheckbox" <?php echo $style_2; ?> id="classcheckbox" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  PNI
						<address>
						<?php if($rows['PNI']=="1"){$style_3 = 'checked';} else {$style_3 = '';}?>
							<input type="checkbox" name="pnicheckbox" <?php echo $style_3; ?> id="pnicheckbox" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  ISM
						<address>
						<?php if($rows['ISM']=="1"){$style_4 = 'checked';} else {$style_4 = '';}?>
							<input type="checkbox" name="ismcheckbox" <?php echo $style_4; ?> id="ismcheckbox" value="1" />
						</address>
					</div>
					
					<div class="col-sm-4 invoice-col">
					  DOC
						<address>
						<?php if($rows['DOC']=="1"){$style_5 = 'checked';} else {$style_5 = '';}?>
							<input type="checkbox" name="doccheckbox" <?php echo $style_5; ?> id="doccheckbox" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  ITC
						<address>
						<?php if($rows['ITC']=="1"){$style_6 = 'checked';} else {$style_6 = '';}?>
							<input type="checkbox" name="itccheckbox" <?php echo $style_6; ?> id="itccheckbox" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  ISPS
						<address>
						<?php if($rows['ISPS']=="1"){$style_7 = 'checked';} else {$style_7 = '';}?>
							<input type="checkbox" name="ispscheckbox" <?php echo $style_7; ?> id="ispscheckbox" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  LL
						<address>
						<?php if($rows['LL']=="1"){$style_8 = 'checked';} else {$style_8 = '';}?>
							<input type="checkbox" name="llcheckbox" <?php echo $style_8; ?> id="llcheckbox" value="1" />
						</address>
					</div>
					
					<div class="col-sm-4 invoice-col">
					  BQ
						<address>
						<?php if($rows['BQ']=="1"){$style_9 = 'checked';} else {$style_9 = '';}?>
							<input type="checkbox" name="bqcheckbox" <?php echo $style_9; ?> id="bqcheckbox" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  H&M
						<address>
						<?php if($rows['H_M']=="1"){$style_10 = 'checked';} else {$style_10 = '';}?>
							<input type="checkbox" name="hmcheckbox" <?php echo $style_10; ?> id="hmcheckbox" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  SEA-WEB
						<address>
						<?php if($rows['SEA_WEB']=="1"){$style_11 = 'checked';} else {$style_11 = '';}?>
							<input type="checkbox" name="seawebcheckbox" <?php echo $style_11; ?> id="seawebcheckbox" value="1" />
						</address>
					</div>
				
					<div class="col-sm-4 invoice-col">
					  CARGO DECL. SIGN MASTER
						<address>
						<?php if($rows['CARGODECL_MASTER']=="1"){$style_13 = 'checked';} else {$style_13 = '';}?>
						<input type="checkbox" name="cdecsigmcheckbox" <?php echo $style_13; ?> id="cdecsigmcheckbox" value="1" />
						</address>
					</div>
				</div>
				             
			<div class="row invoice-info">
				<div class="col-sm-4 invoice-col">
				  Required Docs sent to Insurance Desk
					<address>
					<?php if($rows['REQDOCSSENTTOINS']=="1"){$style_18 = 'checked';} else {$style_18 = '';}?>
						<input type="checkbox" name="reqdoccheckbox" <?php echo $style_18; ?> id="reqdoccheckbox" value="1" />
					</address>
				</div>
				<div class="col-sm-4 invoice-col">
				  CHARTERERS PNI
					<address>
					<select name="selVName" id="selVName" class="form-control" >
					   <?php 
							$obj->updatecheckListChartererPniByVendorListNewForCOA(17,$chartPni);
						?>
					</select>
					</address>
				</div>
				<div class="col-sm-4 invoice-col">
					LAST PORT AGENT
					<address>
						<input type="text" name="txtLastPortAgnt" id="txtLastPortAgnt" class="form-control" autocomplete="off" value="<?php echo $rows['LASTPORTAGENT'];?>" placeholder="LAST PORT AGENT" />
					</address>
				</div>
			</div>
			
			<div class="row">
			<div class="col-xs-12">
				<h2 class="page-header">
					DELIVERY
				</h2>                            
			</div><!-- /.col -->
			</div>
			
			<table width="100%">
			<tbody>
			<div class="row invoice-info">
			<?php if($obj->getFun50()=="" || $obj->getFun50()=="0000-00-00 00:00:00"  || $obj->getFun50()=="1970-01-01 00:00:00"){$txtLaycanFromDate = "";}
			elseif($rows['LAYCAN_FROM']!=''){$txtLaycanFromDate = date('d-m-Y h:i',strtotime($rows['LAYCAN_FROM']));}
			else{$txtLaycanFromDate = date('d-m-Y h:i',strtotime($obj->getFun50()));}?>
				<div class="col-sm-4 invoice-col">
				  LAYCAN FROM
					<address>
						<input type="text" name="txtLaycanFrom" id="txtLaycanFrom" class="form-control" autocomplete="off" value="<?php echo $txtLaycanFromDate; ?>" placeholder="LAYCAN FROM" />
					</address>
				</div>
			<?php if($obj->getFun51()=="" || $obj->getFun51()=="0000-00-00 00:00:00"  || $obj->getFun51()=="1970-01-01 00:00:00"){$txtLaycanToDate = "";}
			elseif($rows['LAYCAN_TO']!=''){$txtLaycanToDate = date('d-m-Y h:i',strtotime($rows['LAYCAN_TO']));}
			else{$txtLaycanToDate = date('d-m-Y h:i',strtotime($obj->getFun51()));}?>
				<div class="col-sm-4 invoice-col">
				  LAYCAN TO
					<address>
						<input type="text" name="txtLaycanTo" id="txtLaycanTo" class="form-control" autocomplete="off" value="<?php echo $txtLaycanToDate;?>" placeholder="LAYCAN TO" />
					</address>
				</div>
				<div class="col-sm-2 invoice-col"></div>
			</div>
			
			
			<div class="row invoice-info">
				<div class="col-sm-4 invoice-col">
					<address>
				  DRAFT RESTRICTIONS AS PER CP
				  </address>
				</div>
				<div class="col-sm-4 invoice-col">
					<address>
						<input type="text" name="txtDrftResAsPerCP" id="txtDrftResAsPerCP" class="form-control" autocomplete="off" value="<?php echo $rows['DRFTRESASPERCP'];?>" placeholder="DRAFT RESTRICTIONS AS PER CP" />
					</address>
				</div>
				<div class="col-sm-2 invoice-col"></div>
			</div>
			
			<div class="row invoice-info">
				<div class="col-sm-4 invoice-col">
					<address>
				  LOAD RATE - CP
				  </address>
				</div>
				<div class="col-sm-4 invoice-col">
					<address>
						<input type="text" name="txtLoadRateCP" id="txtLoadRateCP" class="form-control" autocomplete="off" value="<?php echo $rows['LOAD_RATE_CP'];?>" placeholder="LOAD RATE - CP" />
					</address>
				</div>
				<div class="col-sm-2 invoice-col"></div>
			</div>
			
			<div class="row invoice-info">
				<div class="col-sm-4 invoice-col">
					<address>
				  DISCHARGE RATE - CP
				  </address>
				</div>
				<div class="col-sm-4 invoice-col">
					<address>
						<input type="text" name="txtDischargeRateCP" id="txtDischargeRateCP" class="form-control" autocomplete="off" value="<?php echo $rows['DIS_RATE_CP'];?>" placeholder="DISCHARGE RATE - CP" />
					</address>
				</div>
				<div class="col-sm-2 invoice-col"></div>
			</div>
			
			<div class="row">
			<div class="col-xs-12">
				<h2 class="page-header">
					ETA NOTICES
				</h2>                            
			</div><!-- /.col -->
			</div>
			</tbody>	
			<tbody id="etaNoticesBody">
			<?php $sql1 = "SELECT * FROM check_list_tc_s1 WHERE STATUS=1 AND CHKLISTTC_ID='".$rows['CHKLISTTC_ID']."'";
				  $result1 = mysql_query($sql1);
				  $num1 = mysql_num_rows($result1);$k = 0;
				  if($num1==0){$k++;?>
				<div class="row invoice-info" id="etaNoticeBodyRow_1">
					<div class="col-sm-1 invoice-col"></div>
					<div class="col-sm-1 invoice-col">
						<address>
							<a href="#tb1" onClick="removeEtaNoticeBody(1);" ><i class="fa fa-times" style="color:red;"></i></a>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
						<address>
							<input type="text" name="txtEtaNoticesText_1" id="txtEtaNoticesText_1" class="form-control" autocomplete="off" value="" placeholder="ENTER ETA NOTICES" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
						<address>
							<input type="text" name="txtEtaNoticesData_1" id="txtEtaNoticesData_1" class="form-control" autocomplete="off" value="" placeholder="DD-MM-YYYY HH:MM" />
						</address>
					</div>
				</div>
			<?php }else{
		   while($rows1 = mysql_fetch_assoc($result1))
		   {$k++;?>
				<div class="row invoice-info" id="etaNoticeBodyRow_<?php echo $k;?>">
					<div class="col-sm-1 invoice-col"></div>
					<div class="col-sm-1 invoice-col">
						<address>
							<a href="#tb1" onClick="removeEtaNoticeBody(<?php echo $k;?>);" ><i class="fa fa-times" style="color:red;"></i></a>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
						<address>
							<input type="text" name="txtEtaNoticesText_<?php echo $k;?>" id="txtEtaNoticesText_<?php echo $k;?>" class="form-control" autocomplete="off" value="<?php echo $rows1['ETA_NOTICES_TEXT']; ?>" placeholder="ENTER ETA NOTICES" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
						<address>
							<input type="text" name="txtEtaNoticesData_<?php echo $k;?>" id="txtEtaNoticesData_<?php echo $k;?>" class="form-control" autocomplete="off" value="<?php echo date("d-m-Y H:i", strtotime($rows1['ETA_NOTICES_DATA'])); ?>" placeholder="DD-MM-YYYY HH:MM" />
						</address>
					</div>
				</div>
				<?php }}?>
				</tbody>
				</table>
				<tbody>	
				<div class="row invoice-info">
				<div class="col-sm-1 invoice-col"></div>
					<div class="col-sm-2 invoice-col">
						<address>
							<button type="button" class="btn btn-primary btn-flat" onClick="addOtherEtaNotices();">Add</button>
							<input type="hidden" name="hddnOtherID" id="hddnOtherID" value="<?php echo $k;?>"/>
						</address>
					</div>
					<div class="col-sm-4 invoice-col"></div>
					<div class="col-sm-4 invoice-col"></div>
				</div>
					</tbody>
					
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								ARRIVAL
							</h2>                            
						</div><!-- /.col -->
					</div>
					<table width="100%">
					<tbody>
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtActualArrival" id="txtActualArrival" class="form-control" autocomplete="off" value="<?php if($num==0){echo "ACTUAL ARRIVAL";} else { echo $rows['DEL_ARRI_TEXT'];}?>" placeholder="ACTUAL ARRIVAL" />
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
						<?php if($rows['DEL_ARRI_DATA']=="" || $rows['DEL_ARRI_DATA']=="0000-00-00 00:00:00"  || date('d-m-Y',strtotime($rows['DEL_ARRI_DATA']))=="01-01-1970"){$txtActualArrivalDate = "";}else{$txtActualArrivalDate = date('d-m-Y H:i',strtotime($rows['DEL_ARRI_DATA']));}?>
							<address>
								<input type="text" name="txtActualArrivalDate" id="txtActualArrivalDate" class="form-control" autocomplete="off" value="<?php echo $txtActualArrivalDate; ?>" placeholder="DD-MM-YYYY HH:MM" />
							</address>
						</div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtNorTendered" id="txtNorTendered" class="form-control" autocomplete="off" value="<?php if($num==0){echo "NOR TENDERED";} else { echo $rows['DEL_NORTEN_TEXT'];}?>" placeholder="NOR TENDERED" />
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
						<?php if($rows['DEL_NORTEN_DATA']=="" || $rows['DEL_NORTEN_DATA']=="0000-00-00 00:00:00"  || date('d-m-Y',strtotime($rows['DEL_NORTEN_DATA']))=="01-01-1970"){$txtNorTenderedDate = "";}else{$txtNorTenderedDate = date('d-m-Y H:i',strtotime($rows['DEL_NORTEN_DATA']));}?>
							<address>
								<input type="text" name="txtNorTenderedDate" id="txtNorTenderedDate" class="form-control" autocomplete="off" value="<?php echo $txtNorTenderedDate;?>" placeholder="DD-MM-YYYY HH:MM" />
							</address>
						</div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtDelPlacePort" id="txtDelPlacePort" class="form-control" autocomplete="off" value="<?php if($num==0){echo "DELIVERY PLACE/PORT";} else { echo $rows['DEL_PORT_TEXT'];}?>" placeholder="DELIVERY PLACE/PORT" />
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtDelPlacePortData" id="txtDelPlacePortData" class="form-control" autocomplete="off" value="<?php echo $obj->getFun44();?>" placeholder="" readonly />
							</address>
						</div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtDelFoDo" id="txtDelFoDo" class="form-control" autocomplete="off" value="<?php if($num==0){echo "DELIVERY FO/DO (MT)";} else { echo $rows['DEL_FO_DO_TEXT'];}?>" placeholder="DELIVERY FO/DO (MT)" />
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtDelFoDoData" id="txtDelFoDoData" class="form-control" autocomplete="off" value="<?php echo $rows['DEL_FO_DO_DATA'];?>" placeholder="DELIVERY FO/DO (MT)" />
							</address>
						</div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtDelDateTime" id="txtDelDateTime" class="form-control" autocomplete="off" value="<?php if($num==0){echo "DELIVERY DATE/TIME";} else { echo $rows['DEL_DATETIM_TXT'];}?>" placeholder="DELIVERY DATE/TIME" />
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
						<?php if($rowsdel['DEL_DATE_EST']=="" || $rowsdel['DEL_DATE_EST']=="0000-00-00 00:00:00"  || $rowsdel['DEL_DATE_EST']=="1970-01-01 00:00:00"){$txtDeliveryDate = "";}else{$txtDeliveryDate = date('d-m-Y H:i',strtotime($rowsdel['DEL_DATE_EST']));}?>
							<address>
								<input type="text" name="txtDelDateTimeDate" id="txtDelDateTimeDate" class="form-control" value="<?php echo $txtDeliveryDate; ?>" placeholder="" readonly />
							</address>
						</div>
					</div>
					
					<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
							RE-DELIVERY
						</h2>                            
					</div><!-- /.col -->
					</div>
					<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
							ETA NOTICES
						</h2>                            
					</div><!-- /.col -->
					</div>
					</tbody>
					<tbody id="etaNoticesBody2">
					<?php $sql2 = "SELECT * FROM check_list_tc_s1 WHERE STATUS=2 AND CHKLISTTC_ID='".$rows['CHKLISTTC_ID']."'";
					  $result2 = mysql_query($sql2);
					  $num2 = mysql_num_rows($result2);$k = 0;
					  if($num2==0){$k++;?>
						<div class="row invoice-info" id="etaNoticeBodyRow2_1">
							<div class="col-sm-1 invoice-col"></div>
							<div class="col-sm-1 invoice-col">
								<address>
									<a href="#tb2" onClick="removeEtaNoticeBody2(1);" ><i class="fa fa-times" style="color:red;"></i></a>
								</address>
							</div>
							<div class="col-sm-4 invoice-col">
								<address>
									<input type="text" name="txtEtaNoticesText2_1" id="txtEtaNoticesText2_1" class="form-control" autocomplete="off" value="" placeholder="ENTER ETA NOTICES" />
								</address>
							</div>
							<div class="col-sm-4 invoice-col">
								<address>
									<input type="text" name="txtEtaNoticesData2_1" id="txtEtaNoticesData2_1" class="form-control" autocomplete="off" value="" placeholder="DD-MM-YYYY HH:MM" />
								</address>
							</div>
						</div>
					<?php }else{
				   while($rows2 = mysql_fetch_assoc($result2))
				   {$k++;?>
					<div class="row invoice-info" id="etaNoticeBodyRow2_<?php echo $k;?>">
						<div class="col-sm-1 invoice-col"></div>
						<div class="col-sm-1 invoice-col">
							<address>
								<a href="#tb1" onClick="removeEtaNoticeBody2(<?php echo $k;?>);" ><i class="fa fa-times" style="color:red;"></i></a>
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtEtaNoticesText2_<?php echo $k;?>" id="txtEtaNoticesText2_<?php echo $k;?>" class="form-control" autocomplete="off" value="<?php echo $rows2['ETA_NOTICES_TEXT']; ?>" placeholder="ENTER ETA NOTICES" />
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtEtaNoticesData2_<?php echo $k;?>" id="txtEtaNoticesData2_<?php echo $k;?>" class="form-control" autocomplete="off" value="<?php echo date("d-m-Y H:i", strtotime($rows2['ETA_NOTICES_DATA'])); ?>" placeholder="DD-MM-YYYY HH:MM" />
							</address>
						</div>
					</div>
					<?php }}?>
					</tbody>
					</table>
					<tbody>	
					<div class="row invoice-info">
					<div class="col-sm-1 invoice-col"></div>
						<div class="col-sm-2 invoice-col">
							<address>
								<button type="button" class="btn btn-primary btn-flat" onClick="addOtherEtaNotices2();">Add</button>
								<input type="hidden" name="hddnOtherID2" id="hddnOtherID2" value="<?php echo $k;?>"/>
							</address>
						</div>
						<div class="col-sm-4 invoice-col"></div>
						<div class="col-sm-4 invoice-col"></div>
					</div>
					</tbody>
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								ARRIVAL
							</h2>                            
						</div><!-- /.col -->
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtActualArrival2" id="txtActualArrival2" class="form-control" autocomplete="off" value="<?php if($num==0){echo "ACTUAL ARRIVAL";} else { echo $rows['RDEL_ARRI_TEXT'];}?>" placeholder="ACTUAL ARRIVAL" />
							</address>
						</div>
						<?php if($rows['RDEL_ARRI_DATA']=="" || $rows['RDEL_ARRI_DATA']=="0000-00-00 00:00:00"  || date('d-m-Y',strtotime($rows['RDEL_ARRI_DATA']))=="01-01-1970"){$txtActualArrivalDate2 = "";}else{$txtActualArrivalDate2 = date('d-m-Y H:i',strtotime($rows['RDEL_ARRI_DATA']));}?>
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtActualArrivalDate2" id="txtActualArrivalDate2" class="form-control" autocomplete="off" value="<?php echo $txtActualArrivalDate2;?>" placeholder="DD-MM-YYYY HH:MM" />
							</address>
						</div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtNorTendered2" id="txtNorTendered2" class="form-control" autocomplete="off" value="<?php if($num==0){echo "NOR TENDERED";} else { echo $rows['RDEL_NORTEN_TEXT'];}?>" placeholder="NOR TENDERED" />
							</address>
						</div>
						<?php if($rows['RDEL_NORTEN_DATE']=="" || $rows['RDEL_NORTEN_DATE']=="0000-00-00 00:00:00"  || date('d-m-Y',strtotime($rows['RDEL_NORTEN_DATE']))=="01-01-1970"){$txtNorTenderedDate2 = "";}else{$txtNorTenderedDate2 = date('d-m-Y H:i',strtotime($rows['RDEL_NORTEN_DATE']));}?>
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtNorTenderedDate2" id="txtNorTenderedDate2" class="form-control" autocomplete="off" value="<?php echo $txtNorTenderedDate2;?>" placeholder="DD-MM-YYYY HH:MM" />
							</address>
						</div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtReDelPlacePort" id="txtReDelPlacePort" class="form-control" autocomplete="off" value="<?php if($num==0){echo "RE-DELIVERY PLACE/PORT";} else { echo $rows['RDEL_PORT_TEXT'];}?>" placeholder="RE-DELIVERY PLACE/PORT" />
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtReDelPlacePortData" id="txtReDelPlacePortData" class="form-control" autocomplete="off" value="<?php echo $obj->getFun53();?>" placeholder="" readonly />
							</address>
						</div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtReDeliveryFoDo" id="txtReDeliveryFoDo" class="form-control" autocomplete="off" value="<?php if($num==0){echo "RE-DELIVERY FO/DO (MT)";} else { echo $rows['RDEL_FO_DO_TEXT'];}?>" placeholder="RE-DELIVERY FO/DO (MT)" />
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtReDelFoDoData" id="txtReDelFoDoData" class="form-control" value="<?php echo $rows['RDEL_FO_DO_DATA'];?>" placeholder="RE-DELIVERY FO/DO (MT)"/>
							</address>
						</div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<address>
								<input type="text" name="txtReDelDateTime" id="txtReDelDateTime" class="form-control" autocomplete="off" value="<?php if($num==0){echo "RE-DELIVERY DATE/TIME";} else { echo $rows['RDEL_DATETIM_TXT'];}?>" placeholder="RE-DELIVERY DATE/TIME" />
							</address>
						</div>
						<div class="col-sm-4 invoice-col">
                        <?php if($rowsredel['REDEL_DATE_EST']=="" || $rowsredel['REDEL_DATE_EST']=="0000-00-00 00:00:00"  || $rowsredel['REDEL_DATE_EST']=="1970-01-01 00:00:00"){$txtReDeliveryDate = "";}else{$txtReDeliveryDate = date('d-m-Y H:i',strtotime($rowsredel['REDEL_DATE_EST']));}?>
                        
						
							<address>
								<input type="text" name="txtReDelDateTimeDate" id="txtReDelDateTimeDate" class="form-control" autocomplete="off" value="<?php echo $txtReDeliveryDate; ?>" placeholder="" readonly />
							</address>
						</div>
					</div>
				
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								REMARKS
							</h2>                            
						</div><!-- /.col -->
					</div>
				
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							<address>
								<textarea name="txttcchklistRemarks" id="txttcchklistRemarks" class="form-control" placeholder="REMARKS"><?php echo $rows['REMARKS'];?></textarea>
							</address>
						</div>
					</div>
				
				
				<?php if($rights == 1){ ?>
				<div class="box-footer" align="right">
					<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
					<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
				</div>
				<?php } ?>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
		
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 

		$("#dem_rate").numeric();
		$("[id^=txtlpid_0_]").numeric();
 
		$("#").datepicker({ 
			format: 'dd-mm-yyyy',
			autoclose: true	 
			});

		$("#date, [id^=txtlpid_3_],[id^=txtEtaNoticesData_],[id^=txtEtaNoticesData2_],#txtActualArrivalDate,#txtNorTenderedDate,#txtActualArrivalDate2,#txtNorTenderedDate2,#txtLaycanFrom,#txtLaycanTo").datetimepicker({
			format: 'dd-mm-yyyy hh:ii',
			autoclose: true,
			todayBtn: true,
			minuteStep: 1
			});
		
		$("#frm1").validate({
		rules: {
			txtLastPortAgnt:"required",
			selVName:"required",
			txtLaycanFrom:"required",
			txtLaycanTo:"required",
			txttcchklistRemarks:"required"
			},
		messages: {
			txtLastPortAgnt:"*",
			},
			submitHandler: function(form)  {
				jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
				if(r){
					jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
					$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
					$("#popup_content").css({"background":"none","text-align":"center"});
					$("#popup_ok,#popup_title").hide();  
					frm1.submit();
					}
					else{return false;}
				});
			}
		});
		
	

});

function getPdf(var1)
{
	location.href='allPdf.php?id=26&comid='+var1;
}


function getValidate(val)
{
	
}

function addOtherEtaNotices()
{
    var idd = $("#hddnOtherID").val();
	if($("#txtEtaNoticesText_"+idd).val()!='' && $("#txtEtaNoticesData_"+idd).val()!='')
	{
	    idd = parseInt(idd) + 1;
	    $('<div class="row invoice-info" id="etaNoticeBodyRow_'+idd+'"><div class="col-sm-1 invoice-col"></div><div class="col-sm-1 invoice-col"><address><a href="#tb1" onClick="removeEtaNoticeBody('+idd+');" ><i class="fa fa-times" style="color:red;"></i></a></address></div><div class="col-sm-4 invoice-col"><address><input type="text" name="txtEtaNoticesText_'+idd+'" id="txtEtaNoticesText_'+idd+'" class="form-control" autocomplete="off" value="" placeholder="ENTER ETA NOTICES"/></address></div><div class="col-sm-4 invoice-col"><address><input type="text" name="txtEtaNoticesData_'+idd+'" id="txtEtaNoticesData_'+idd+'" class="form-control" autocomplete="off" value="" placeholder="DD-MM-YYYY HH:MM"/></address></div></div></tr>').appendTo("#etaNoticesBody");
		$("#hddnOtherID").val(idd);
		$("[id^=txtEtaNoticesData_]").datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
	    autoclose: true,
        todayBtn: true,
        minuteStep: 1
		});
	}
	else
	{
		jAlert('Please fill previous fields', 'Alert');
		return false;
	}
}

function removeEtaNoticeBody(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#etaNoticeBodyRow_"+var1).remove();		
				 }
			else{return false;}
			});
}

function addOtherEtaNotices2()
{
    var idd = $("#hddnOtherID2").val();
	if($("#txtEtaNoticesText2_"+idd).val()!='' && $("#txtEtaNoticesData2_"+idd).val()!='')
	{
	    idd = parseInt(idd) + 1;
	    $('<div class="row invoice-info" id="etaNoticeBodyRow2_'+idd+'"><div class="col-sm-1 invoice-col"></div><div class="col-sm-1 invoice-col"><address><a href="#tb2" onClick="removeEtaNoticeBody2('+idd+');" ><i class="fa fa-times" style="color:red;"></i></a></address></div><div class="col-sm-4 invoice-col"><address><input type="text" name="txtEtaNoticesText2_'+idd+'" id="txtEtaNoticesText2_'+idd+'" class="form-control" autocomplete="off" value="" placeholder="ENTER ETA NOTICES"/></address></div><div class="col-sm-4 invoice-col"><address><input type="text" name="txtEtaNoticesData2_'+idd+'" id="txtEtaNoticesData2_'+idd+'" class="form-control" autocomplete="off" value="" placeholder="DD-MM-YYYY HH:MM"/></address></div></div></tr>').appendTo("#etaNoticesBody2");
		$("#hddnOtherID2").val(idd);
		$("[id^=txtEtaNoticesData2_]").datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
	    autoclose: true,
        todayBtn: true,
        minuteStep: 1
		});
	}
	else
	{
		jAlert('Please fill previous fields', 'Alert');
		return false;
	}
}

function removeEtaNoticeBody2(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#etaNoticeBodyRow2_"+var1).remove();		
				 }
			else{return false;}
			});
}

	
</script>

</body>
</html>