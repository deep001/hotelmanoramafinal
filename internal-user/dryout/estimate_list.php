<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$ids = $_REQUEST['ids'];
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->deleteFCETempleteRecordsNew();
	header('Location:./estimate_list.php?msg='.$msg);
 }
if (@$_REQUEST['action1'] == 'submit')
 {
 	$msg = $obj->submitFinalFCARecordCompare();
	header('Location:./decisionchart_list.php?msg=1');
 }
unset($_SESSION["final"]);
 
if(isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$_SESSION['selBType'] = $_REQUEST['selBType'];
	$estimatetype = $selBType = $_SESSION['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$estimatetype = $selBType = $_SESSION['selBType'];
}
else
{
	unset($_SESSION['selBType']);
	$selBType = '';
	$estimatetype = 1;
}

$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.benchclass{
	background-color:#f39c12;
	color:#ffffff;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(9); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-leaf"></i>&nbsp;VC Estimates&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">VC Estimates : Estimate</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> VC Estimates added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating VC Estimates.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> VC Estimates delete successfully.
				</div>
				<?php }
				if($msg == 3){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Final VC Estimates added successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
				<h3 style=" text-align:center;">VC Estimates : Estimate List</h3>
                <form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
				    <div class="row" style="padding-right:10px; padding-left:10px; margin-bottom:5px;">
                         <div class="col-xs-6">&nbsp;
                         </div>
                         <div class="col-xs-3">
                             &nbsp;&nbsp;&nbsp;&nbsp;<input name="estimatetype" class="checkbox" id="estimatetype1" type="radio" value="1"  <?php if($estimatetype == 1) echo "checked"; ?> />&nbsp;&nbsp;&nbsp;Gas&nbsp;&nbsp;&nbsp;&nbsp;<input name="estimatetype" class="checkbox" id="estimatetype2" type="radio" value="2"  <?php if($estimatetype == 2) echo "checked"; ?> />&nbsp;&nbsp;&nbsp;Tanker&nbsp;&nbsp;&nbsp;&nbsp;<input name="estimatetype" class="checkbox" id="estimatetype3" type="radio" value="3"  <?php if($estimatetype == 3) echo "checked"; ?> />&nbsp;&nbsp;&nbsp;Dry Cargo
                         </div>
                         <div class="col-xs-3">
                            <div style="float:right;"><a href="addprevestimate.php?estimatetype=<?=$estimatetype;?>" title="Add" id="addprev"><button type="button" class="btn btn-info btn-flat">Add From Previous</button></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="addestimate.php?estimatetype=<?=$estimatetype;?>" title="Add" id="addnew"><button type="button" class="btn btn-info btn-flat">Add New</button></a></div>
                         </div>
                     </div>
				     <div class="row" style="padding-right:10px; padding-left:10px;">
                         <div class="col-xs-6">&nbsp;
                         </div>
                         <div class="col-xs-4">
                            <a href="#" onClick="getListToCompare();" style="float:right;"><button type="button" data-toggle="modal" data-target="#compose-modal" class="btn btn-warning" onClick="getListToCompare();" >Create Decision Chart</button></a>
                         </div>
                         <div class="col-xs-2" >
                             <select name="selBType" id="selBType" class="form-control" onChange="getSubmit();">
                             <option value="">---Select Business Type---</option>
                             <?php echo $obj->getBusinessTypeList1($selBType);?>
                             </select>                  
                         </div><!-- /.col -->
                     </div>
                    
					<div >
						<input name="txttblid" id="txttblid" type="hidden" value="" />
						<input name="txttblstatus" id="txttblstatus" type="hidden" value="" />
						<input name="delId" id="delId" type="hidden" value="" />
						<input type="hidden" name="action" id="action" value="submit" />
					</div>				
					<?php $obj->displayFCETempleteListNew($selBType);?>
                    <div style="clear:both;"></div>
				</form>
				</div>
                
              <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" style="width:95%">
					 <div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Decision Chart</h4>
						</div>
						<form name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
						<div class="modal-body" id="div_3" style="overflow:auto;">
						</div>
                        <input type="hidden" name="action1" value="submit" />
                        </form>
					 </div>
				</div>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<script src='../../js/jquery.autosize.js'></script>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
<?php if(isset($_REQUEST['ids']) && $_REQUEST['ids']!=""){?>
getListToCompare1("<?php echo $_REQUEST['ids'];?>");
<?php }?>
$('#estimatetype1').on('ifChecked', function () { 
   $("#addprev").attr("href", "addprevestimate.php?estimatetype="+$(this).val());
   $("#addnew").attr("href", "addestimate.php?estimatetype="+$(this).val());
});
 
$('#estimatetype1').on('ifUnchecked', function () { 
   $("#addprev").attr("href", "");
   $("#addnew").attr("href", "");
});
$('#estimatetype2').on('ifChecked', function () { 
   $("#addprev").attr("href", "addprevestimate.php?estimatetype="+$(this).val());
   $("#addnew").attr("href", "addestimate.php?estimatetype="+$(this).val());
});
 
$('#estimatetype2').on('ifUnchecked', function () { 
   $("#addprev").attr("href", "");
   $("#addnew").attr("href", "");
});
$('#estimatetype3').on('ifChecked', function () { 
   $("#addprev").attr("href", "addprevestimate.php?estimatetype="+$(this).val());
   $("#addnew").attr("href", "addestimate.php?estimatetype="+$(this).val());
});
 
$('#estimatetype3').on('ifUnchecked', function () { 
   $("#addprev").attr("href", "");
   $("#addnew").attr("href", "");
});
$('#chkAll').on('ifChecked', function () { 
    $('[id^=chkFCA_]').attr("checked",true);
	$('[id^=chkFCA_]').iCheck({
		checkboxClass: 'icheckbox_minimal',
		radioClass: 'iradio_minimal'
		});
});
 
$('#chkAll').on('ifUnchecked', function () { 
   $('[id^=chkFCA_]').attr("checked",false);
	$('[id^=chkFCA_]').iCheck({
		checkboxClass: 'icheckbox_minimal',
		radioClass: 'iradio_minimal'
		});
});

});
$(function() {
	$("#fce_list").dataTable();
    
});
function getDelete(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently?', 'Confirmation', function(r) {
	if(r){ 
		$("#delId").val(var1);
		document.frm1.submit();
	}
	else{return false;}
	});
}

function getData()
{
	$("#tbody").html("");
	$("#tbody").html('<tr><td height="100" colspan="8" align="center" ><img src="../../img/loading.gif" /></td></tr>');
	$.post("options.php?id=31",{selVName:""+$("#selVName").val()+"",selSheetName:""+$("#selSheetName").val()+""}, function(html) {
		$("#tbody").html("");
		$("#tbody").html(html);
		$("#fce_list").dataTable();
		$('[id^=txtDesc_]').autosize({append: "\n"});
	});
}

function getCheckAllCHeckbox()
{
	if($("#chkAll").attr('checked'))
	{
		$('[id^=chkFCA_]').attr("checked",true);
		$('[id^=chkFCA_]').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
			});
	}
	else
	{
		$('[id^=chkFCA_]').attr("checked",false);
		$('[id^=chkFCA_]').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
			});
	}
}

function getListToCompare()
{
	var anyBoxesChecked = false;
	var chkArr = Array();
	var j = 0;
    $('[id^=chkFCA_]').each(function() {
        if ($(this).is(":checked")) {
			chkArr[j] = $(this).val();
            anyBoxesChecked = true;
			j = j + 1;
        }
    });
 
    if (anyBoxesChecked == false) {
       jAlert('Please select at least one checkbox', 'alert');
      return false;
    } 
	else
	{  
	    $("#buttonPop1").click();
	    $("#div_3").html('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />');
		$.post("options.php?id=36",{chkArr:""+chkArr+""}, function(html) {
		$("#div_3").html("");
		$("#div_3").html(html);
		$("input[type='radio']").iCheck({
				checkboxClass: 'icheckbox_minimal',
				radioClass: 'iradio_minimal'
			    });
		$('[id^=txtDesc_]').autosize({append: "\n"});
	});
	}
}


function getListToCompare1(ids)
{
	
	$("#buttonPop1").click();
	$("#div_3").html('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />');
	$.post("options.php?id=36",{chkArr:""+ids+""}, function(html) {
	$("#div_3").html("");
	$("#div_3").html(html);
	$("input[type='radio']").iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
			});
	    $('[id^=txtDesc_]').autosize({append: "\n"});
	});
	
}



function SubmitToCompare()
{
	var anyBoxesChecked = false;
	var chkArr = Array();
	var j = 0;
    $('[id^=rdoFCA_]').each(function() {
        if ($(this).is(":checked")) {
			chkArr[j] = $(this).val();
			var rowid = this.id;
			var lasrvar1 = rowid.split("_")[1];
			if($("#txtDesc_"+lasrvar1).val() != "")	
			{
				anyBoxesChecked = true;
				j = j + 1;
			}
			
        }
    });
 
    if (anyBoxesChecked == false) {
       jAlert('Please select one Fixture and fill remarks', 'alert');
      return false;
    } 
	else
	{  
	   document.frm2.submit();
	}
}


function getSubmit()
{ 
    $("#action").val("ss");
	document.frm1.submit();
}

</script>
		
</body>
</html>