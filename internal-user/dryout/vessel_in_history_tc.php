<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
if (isset($_REQUEST['selYear']) && $_REQUEST['selYear']!="")
{
	$selYear = $_REQUEST['selYear'];
}
else
{
	$selYear = date('Y',time());
}

if (isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$selBType = $_SESSION['selBType'] = $_REQUEST['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	unset($_SESSION['selBType']);
	$selBType = '';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(21); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops TC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops TC&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Vessels in History - TC</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Vessels in History added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Vessels in History sheet successfully create.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
					<h3 style=" text-align:center;">Vessels in History - TC</h3>
				
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                 
                        <div class="col-xs-8">
								             &nbsp; <input type="hidden" name="txtMappingid2" id="txtMappingid2" value="" />
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<input type="hidden" name="txtFileName" id="txtFileName" value="" />
							<input type="hidden" name="action" id="action" value="submit" />                
							</div><!-- /.col -->
                            <div class="col-xs-2">
                                 <select name="selBType" id="selBType" class="form-control" onChange="getSubmit();">
                                 <option value="">---Select Business Type---</option>
								   <?php echo $obj->getBusinessTypeList1($selBType);?>
                                  </select>                  
							</div><!-- /.col -->
						    <div class="col-xs-2">
								  <select name="selYear" id="selYear" class="form-control" onChange="getSubmit();">
                                   <?php echo $obj->getYearListTC($selYear);?>
                                  </select>                      
							</div><!-- /.col -->
						<div class="box-body table-responsive" style="overflow:auto;">
							<table class="table table-bordered table-striped" id='in_ops_at_glance'>
								<thead>
									<tr valign="top">
										<th align="left">Final TC Est/Docs</th>
										<th align="left">Nom ID/TC No.</th>
										<th align="left">Business Type</th>
                                        <th align="left">Vessel</th>
                                        <th align="left">CP Date</th>
										<th align="left">Port Del/Port Re-Del</th>
                                        <th align="left">Checklist</th>
										<th align="left">TC days/ Fixture Note</th>
										<th align="left">TC Financials</th>
										<th align="left">Agency Letters</th>
                                        <th align="left">Payment/Invoices</th>
										<th align="left">De-activate/Compare</th>
                                        <th align="left">Re-Del Date</th>
                                       	
									</tr>
								</thead>
									<?php
									$sql = "select chartering_estimate_tc_compare.COMID as COMID, chartering_estimate_tc_master.VESSEL_IMO_ID as VESSEL_IMO_ID, chartering_estimate_tc_master.TC_NO as TC_NO, chartering_estimate_tc_compare.MESSAGE as MESSAGE, chartering_estimate_tc_master.FIXTURE_TYPE as FIXTURE_TYPE, chartering_estimate_tc_master.RE_DEL_DATE as RE_DEL_DATE,(select SUM(TC_DAYS_EST) as SUM from chartering_tc_estimate_slave1 where chartering_tc_estimate_slave1.TCOUTID= chartering_estimate_tc_master.TCOUTID) as TC_DAYS_EST, chartering_estimate_tc_master.TC_NO as VOYAGE_NAME from chartering_estimate_tc_compare inner join chartering_estimate_tc_master on chartering_estimate_tc_master.TCOUTID= chartering_estimate_tc_compare.TCOUTID WHERE chartering_estimate_tc_compare.MODULEID='".$_SESSION['moduleid']."' AND chartering_estimate_tc_compare.MCOMPANYID='".$_SESSION['company']."' and chartering_estimate_tc_compare.FINAL_ID!='' and chartering_estimate_tc_master.FIXED=1 and chartering_estimate_tc_compare.STATUS=4 and year(chartering_estimate_tc_master.UPDATE_ON_DATE)='".$selYear."' and chartering_estimate_tc_master.FIXED=1 and chartering_estimate_tc_compare.STATUS in (3,4) and year(chartering_estimate_tc_master.UPDATE_ON_DATE)='".$selYear."' and chartering_estimate_tc_master.ESTIMATE_TYPE='".$_SESSION['selBType']."' order by date(chartering_estimate_tc_master.FINAL_DATETIME) desc"; 
                                    $res = mysql_query($sql) or die($sql);
									$rec = mysql_num_rows($res);
									$i=1;
									$submit = 0;
										if($rec == 0)
										{
											echo '<tbody>';
									
											echo '</tbody>';
										}else{
									?>
								<tbody>
									<?php
										while($rows = mysql_fetch_assoc($res))
										{
											$ESTIMATE_TYPE = $obj->getCharteringEstimateTCData($rows['COMID'],"ESTIMATE_TYPE");											
											$sql2 = "select TCOUTID, CP_DATE1 from chartering_estimate_tc_master where COMID='".$rows['COMID']."' order by TCOUTID asc";
											$res2 = mysql_query($sql2);
											$row2 = mysql_fetch_assoc($res2);
										?>						
										<tr id="in_row_<?php echo $i;?>" style="font-size:11px;">
											<td>
												<a href="viewtcestimate.php?id=<?php echo $row2['TCOUTID'];?>&rttype=3" style="color:green;" title="Click me" >TC</a><br/><br/>
												<a href="documents_tc.php?comid=<?php echo $rows['COMID'];?>&page=3" style="color:green;" title="Click me" >Docs</a>
											</td>
                                           <td><?php echo $rows['MESSAGE']."<br/><br/><span style='color:red;'>".$rows['TC_NO'];?></span></td>
											<td><?php echo $obj->getEstimateList($ESTIMATE_TYPE);?></td>
											<td><?php echo $obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME");?>/<br/> <?php echo $obj->getCharteringEstimateTCData($rows['COMID'],"VESSEL_TYPE");?></td>
                                            <td><?php if($row2['CP_DATE1']=="" || $row2['CP_DATE1']=="0000-00-00"){$CP_DATE1 = "";}else{$CP_DATE1 = date('d-m-Y',strtotime($row2['CP_DATE1']));} echo $CP_DATE1;?></td>
                                            <td><?php echo $obj->getCharteringEstimateTCData($rows['COMID'],"DEL_RANGE_PORT");?>/<?php echo $obj->getCharteringEstimateTCData($rows['COMID'],"RE_DEL_RANGE"); ?></td>
                                            <td><a href="tc_check_list.php?comid=<?php echo $rows['COMID'];?>&page=3" style="color:red;" title="Click me" >Check List</a></td>
                                            <td><?php echo $rows['TC_DAYS_EST']?></td>
                                            <td style="text-align:center;">
											<?php
													$sql1 = "select * from cost_sheettc_name_master where COMID='".$rows['COMID']."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";

													$res1 = mysql_query($sql1);
													$rec1 = mysql_num_rows($res1);
													if($rec1 > 0)
													{
														while($rows1 = mysql_fetch_assoc($res1))
														{
															if($rows1['PROCESS'] != "EST")
															{
																$href = "./cost_sheet_tc.php?comid=".$rows['COMID']."&cost_sheet_id=".$rows1['COST_SHEETID']."&page=3";
																echo "<a href='".$href."'>".str_replace(' ','&nbsp;',$rows1['SHEET_NAME'])."</a><br/>";
																$curr_cst = $rows1['COST_SHEETID'];
															}
															else
															{
																$curr_cst = $obj->getCostSheetFixtureID($rows['COMID'],"SHEET_NO");
															}
														}
													}	
											      
												   $fcaid = $obj->getLatestCostSheetIDTC($rows['COMID']);	
												   $lastsheet = $obj->getLatestSheetIDTC($rows['COMID']);	
												   if($lastsheet==0)
												   {?>
                                                   <button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-modal1" type="button" title="Add New CS" onClick="openWin1(<?php echo $rows['COMID'];?>);">A</button>
                                                   <?php }else{
											       if($obj->getEstimateDatatc($fcaid,$lastsheet)==1)
													{
													?>
													<br/><button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-modal1" type="button" title="Add New CS" onClick="openWin1(<?php echo $rows['COMID'];?>);">A</button>
													<?php }else{?>
                                                    
                                                    <?php }}?>
											</td>
                                            <td><a href="agency_letter_inops_tc.php?comid=<?php echo $rows['COMID'];?>&tab=1&page=3">Generate Agency Letter</a></td>
                                            <td>
											<i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp;<a href="payment_grid_tc.php?comid=<?php echo $rows['COMID'];?>&tab=0&page=3" style="color:#005A86;"><strong>View</strong></i></a><br/>
											</td>
											<td>
											<button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-modal2" type="button" title="Compare Sheets" onClick="getCompareSheetData(<?php echo $rows['COMID'];?>);">Compare</button>
											</td>
											<td><?php if($rows['RE_DEL_DATE']=="" || $rows['RE_DEL_DATE']=="0000-00-00"){$RE_DEL_DATE = "";}else{$RE_DEL_DATE = date('d-m-Y',strtotime($rows['RE_DEL_DATE']));} echo $RE_DEL_DATE;?></td>
                                            
										</tr>	                                        
									<?php $i = $i + 1; } }?>
								</tbody>
							</table>
						</div>
					</form>	
				</div>
                
                
                <div class="modal fade" id="compose-modal2" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" style="width:98%">
					    <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Compare Sheets</h4>
                            </div>
                            <form name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                            <div class="modal-body" id="div_3" style="overflow:auto;">
                            </div>
                            
                            </form>
					    </div>
				    </div><!-- /.modal-dialog -->
				</div>		
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>

<script type="text/javascript">
$(document).ready(function(){ 

$("#in_ops_at_glance").dataTable();

function getPdfForSea(mappingid,vesselid)
{
	location.href='allPdf.php?id=12&mappingid='+mappingid+'&vesselid='+vesselid;
}
});


function getCompareSheetData(comid)
{
	$("#div_3").empty();
	$("#div_3").html('<div style="height:200px;text-align:center;"><img src="../../img/loading.gif" /></div>');
	$.post("options.php?id=55",{comid:""+comid+""}, function(html) {
		$("#div_3").empty();
		$("#div_3").html(html);
	});
}

function getSubmit()
{ 
    $("#action").val("");
	document.frm1.submit();
}
</script>
		
</body>
</html>