<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
$invoiceid  = $_REQUEST['invoiceid'];

$sheetid = $obj->getLatestCostSheetIDTC($comid);
$obj->viewTCEstimationTempleteRecordsNew($sheetid);
$arr  = $obj->getTCLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($sheetid);
$result = mysql_query("select MESSAGE from chartering_estimate_tc_compare where COMID='".$comid."'") or die;
$rows1 = mysql_fetch_assoc($result);
$vendorid = $obj->getFun103();
$lp_name = array(); 
$dp_name = array();
for($j=0;$j<count($arr);$j++)
{   
    $arr_explode = explode('-',$arr[$j]);
	if($arr_explode[0] == "LP"){$lp_name[]= $obj->getPortNameBasedOnID($arr_explode[1]);}
	elseif($arr_explode[0] == "DP"){$dp_name[]= $obj->getPortNameBasedOnID($arr_explode[1]);}
}

$lp_name  = implode(', ',$lp_name);
$dp_name  = implode(', ',$dp_name);
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
getShowBunkerDiv();
});


function getShowBunkerDiv()
{
	if($("#selIType").val()=='Final')
	{
		$("#bunkerdiv1,#bunkerdiv2,#offhirediv").show();
	}
	else
	{
		$("#bunkerdiv1,#bunkerdiv2,#offhirediv").hide();
	}
	getFullClaculation();
}


</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;In Ops at a glance&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>
                
                <section class="content invoice">
				<!--   content put here..................-->
                <?php
				$sql = "SELECT * from invoice_tchire_master where INVOICEID='".$invoiceid."'";
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				
				?>	
				<div align="right"><?php if($rec>0){?><a href="allPdf.php?id=59&comid=<?php echo $comid; ?>&invoiceid=<?php echo $rows['INVOICEID']; ?>" title="Pdf"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a><?php }?><a href="tchire_invoice_list.php?comid=<?php echo $comid;?>&page=<?php echo  $page;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
							
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12" style="text-align:center;">
								<h2 class="page-header">
								Hire Invoice
                                <input type="hidden" name="txtFcaid" id="txtFcaid" value="<?php echo $obj->getFun1();?>"/>
                                <input type="hidden" name="txtMapId" id="txtMapId" value="<?php echo $comid;?>"/>
                                <input type="hidden" name="txtInID" id="txtInID" value="<?php echo $rows['INVOICEID'];?>"/>
                                <input type="hidden" name="txtVendorID" id="txtVendorID" value="<?php echo $vendorid;?>"/>
								</h2>                            
							</div><!-- /.col -->
						</div>
					
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Invoice Type
								<address>
                                
									<select  name="selIType" class="select form-control" disabled id="selIType">
										<?php 
										$obj->getInvoiceType();
										?>
									</select>
									<script>$('#selIType').val('<?php echo $rows['INVOICE_TYPE'];?>');</script>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
							   Invoice No.
								<address>
									<input type="text" name="txtInvoiceNo" id="txtInvoiceNo" disabled class="form-control" autocomplete="off" value="<?php echo $rows['INVOICE_NO'];?>"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								CP Date
								<address>
								   <label><?php
								      $cpdate = date("d-m-Y",strtotime($obj->getFun16())); 
									  echo $cpdate;?></label>
								   <input type="hidden" name="txtCP_Date" id="txtCP_Date" class="input-text" size="10" value="<?php echo $cpdate; ?>" autocomplete="off" readonly/>
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
						  <div class="col-sm-4 invoice-col">
								 Invoice Date
								<address>
								   <?php if($rec == 0){?>
									<input type="text" name="txtDate" id="txtDate" disabled class="form-control" value="<?php echo date("d-m-Y",time());?>"/>
									<?php }else{?>
									<input type="text" name="txtDate" id="txtDate" disabled class="form-control" value="<?php echo date("d-m-Y",strtotime($rows['INVOICE_DATE']));?>"/>
								<?php }?>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Nom ID
								<address>
								 <?php echo $rows1["MESSAGE"];?>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Vessel
								<address>
								   <label><?php echo $obj->getVesselIMOData($obj->getFun2(),"VESSEL_NAME");?></label>
								</address>
							</div><!-- /.col -->
						</div>
                        <div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
								Vessel IMO No.
								<address>
								   <label><?php echo $obj->getVesselIMOData($obj->getFun2(),"IMO_NO");?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-3 invoice-col">
								Charterer
								<address>
								        <label><?php echo $obj->getVendorListNewBasedOnID($vendorid);?></label>
										<input type="hidden" name="txtVendor" id="txtVendor" value="<?php echo $vendorid;?>" />
										<input type="hidden" name="txtNameid" id="txtNameid" value="<?php echo $id[0];?>" />
										<input type="hidden" name="txtGradeid" id="txtGradeid" value="<?php echo $id[2];?>" />
                                        <input type="hidden" name="txtBunkerExpense" id="txtBunkerExpense" value="<?php echo $obj->getFun79();?>" />
										<input type="hidden" name="txtFCAID" id="txtFCAID" value="<?php echo $obj->getFun1();?>" />
								</address>
							</div><!-- /.col -->                    
							<div class="col-sm-3 invoice-col">
								Loading Port
								<address>
								   <label><?php echo $lp_name;?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-3 invoice-col">
								Discharging Port
								<address>
								   <label><?php echo $dp_name;?></label>
								</address>
							</div><!-- /.col -->
						</div>
						<div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
								Remarks
								<address>
									<textarea class="form-control areasize" disabled name="txtRemarks" id="txtRemarks" rows="3" placeholder="Remarks ..." ><?php echo $rows['REMARKS'];?></textarea>
								</address>
							</div><!-- /.col -->						
						</div>
						
						<div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
								Owner's Account : Statement
								</h3>                            
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Del At.
								<address>
									&nbsp;&nbsp;<?php echo $obj->getPortNameBasedOnID($obj->getFun68());?>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Del Date Time GMT
								<address>
                                <?php if($obj->getFun69()=='0000-00-00 00:00:00' || $obj->getFun69()==''){$deldate = '';}else{$deldate = date('d-m-Y H:i',strtotime($obj->getFun69()));}?>
								  &nbsp;&nbsp;<?php echo $deldate;?>
								</address>
							</div><!-- /.col -->
						</div>
						
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   ReDel At.
								<address>
									&nbsp;&nbsp;<?php echo $obj->getPortNameBasedOnID($obj->getFun70());?>)
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								ReDel Date Time GMT
								<address>
                                 <?php if($obj->getFun71()=='0000-00-00 00:00:00' || $obj->getFun71()==''){$redeldate = '';}else{$redeldate = date('d-m-Y H:i',strtotime($obj->getFun71()));}?>
								  &nbsp;&nbsp;<?php echo $redeldate;?>
								</address>
							</div><!-- /.col -->
						</div>
						
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Total Trip Duration
								<address>
                                
									&nbsp;&nbsp;<?php echo $obj->getFun100();?><input type="hidden" name="txtTripDuration" id="txtTripDuration" class="form-control" readonly autocomplete="off" value="<?php echo ($obj->getFun100() - $invsum);?>"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
							   Tc Cost/Day (USD)
								<address>
									&nbsp;&nbsp;<?php echo $obj->getFun11();?><input type="hidden" name="txtTCCost" id="txtTCCost" class="form-control" readonly value="<?php echo $obj->getFun11();?>" onKeyUp="getFullClaculation();" autocomplete="off"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Nett Release Hire Days
								<address>
								  <input type="text" name="txtReleaseHireDays" disabled id="txtReleaseHireDays" class="form-control" onKeyUp="getFullClaculation();" value="<?php echo $rows['RELEASE_HIRE_DAYS'];?>" autocomplete="off"/>
								</address>
							</div><!-- /.col -->
						</div>
					 <div class="box" id="offhirediv">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Off Hire Reason</th>
                                    <th>Off Hire From (Days)</th>
                                    <th>Off Hire To (Days)</th>
                                    <?php $sql2 = "select * from invoice_tchire_slave3 where INVOICEID='".$rows['INVOICEID']."'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									?>
				                    <th>Off Hire Days</th>
                                </tr>
                            </thead>
                            <tbody id="tblOFF">
                            <?php if($num2 >0)
							   {$i = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;
								?>
                                <tr id="off_Row_<?php echo $i;?>">
                                   <td><?php echo $rows2['OFFHIRE_REASON'];?></td>
                                   <td><?php echo date('d-m-Y H:i',strtotime($rows2['FROM_OFF_DATE']));?></td>
                                   <td><?php echo date('d-m-Y H:i',strtotime($rows2['TO_OFF_DATE']));?></td>
                                   <td><?php echo $rows2['OFFHIRE_DAYS'];?></td>
                                </tr>
                                <?php }}else{
								if($num3 >0)
								   {
                                }}?>
                            </tbody>
                        </table>
                    </div>
                  </div>
						
						<div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
								Amount Due To Owner
								</h3>                            
							</div><!-- /.col -->
						</div>
						
						<div class="box">
                         <div class="box-body no-padding">
							<table class="table table-striped">
								<tbody>
									<tr>
										<td width="20%">Nett Hire</td>
										<td width="15%"></td>
										<td width="20%"></td>
										<td width="20%"></td>
                                       
										<td width="25%"><span id="NettHireSpan"><?php echo $rows['NETT_HIRE'];?></span><input type="hidden" name="txtNettHire" id="txtNettHire" class="form-control" autocomplete="off" value="<?php echo $rows['NETT_HIRE'];?>" readonly/></td>
									</tr>
								</tbody>
						      </table>
						 </div>
                       </div>
						
						
                    <div class="box" id="bunkerdiv1" style="display:none;">
                      <div class="box-body no-padding">
                      <h3>Cost Of Bunker On Delivery</h3>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									
                                </tr>
                            </thead>
                            <tbody>
                        <?php  
						
						     $sql2 = "select * from invoice_tchire_slave2 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='DEL'";
							 $res2 = mysql_query($sql2) or die($sql2);
							 $num2 = mysql_num_rows($res2);
									
							  if($num2>0)
							  {
						      while($rows2 = mysql_fetch_assoc($res2))
								{
								?>
                                  <tr>
                                    <td><?php echo $obj->getBunkerGradeBasedOnID($rows2['BUNKERID']);?></td>
								    <td><?php echo $rows2['BUNKER_QTY'];?></td>
                                    <td><?php echo $rows2['BUNKER_PRICE'];?></td>
                                    <td><?php echo $rows2['BUNKER_AMT'];?></td>
                                </tr>
                            
                                <?php }}?>
                            </tbody>
                         </table>
                         
                    </div>
                  </div>
				
				
				
				
				<div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                  
                                    <th>Description</th>
                                    <?php $sql2 = "select * from invoice_tchire_slave1 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='ADD'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									?>
                          			<th>Amount<input type="hidden" name="txtAddID" id="txtAddID" value="<?php echo $num21;?>"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblAdd">
                            <?php if($num2 >0)
							   {$i = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;
								?>
                                <tr id="add_Row_<?php echo $i;?>">
                                   
                                   <td><?php echo $rows2['DESCRIPTION'];?></td>
                                   <td><?php echo $rows2['AMOUNT'];?></td>
                                </tr>
                                <?php }}else{?>
                                
                                <?php }?>
                            </tbody>
                           <tfoot>
                              <?php if($rows['IS_CVE']==1){?>
                              <tr>
                                    <td align="left"><input name="chkShowCVE" class="checkbox" id="chkShowCVE" type="checkbox" value="1" checked onClick="getFullClaculation();"/></td>
									<td>CVE</td>
									<td><input type="text" name="txtCVEAmt" id="txtCVEAmt" class="form-control" readonly  placeholder="0.00" autocomplete="off" value="<?php echo $rows['CVEAMT'];?>"/></td>
                                </tr>
                                <?php }?>
                                <?php if($rows['IS_ILOHC']==1){?>
                                <tr>
                                    <td align="left"><input name="chkShowILOHC" class="checkbox" id="chkShowILOHC" type="checkbox" value="1" checked onClick="getFullClaculation();"/></td>
									<td>ILOHC</td>
									<td><input type="text" name="txtILOHCAmt" id="txtILOHCAmt" class="form-control" readonly  placeholder="0.00" autocomplete="off" value="<?php echo $rows['ILOHCAMT'];?>"/></td>
                                </tr>
                                <?php }?>
                                <?php if($rows['IS_BALLASTBONUS']==1){?>
                                <tr>
                                    <td align="left"><input name="chkShowBalastBonus" class="checkbox" id="chkShowBalastBonus" type="checkbox" value="1" checked onClick="getFullClaculation();"/></td>
									<td>Ballast Bonus</td>
									<td><input type="text" name="txtBalastBonusAmt" id="txtBalastBonusAmt" class="form-control" readonly  placeholder="0.00" autocomplete="off" value="<?php echo $rows['BALLASTBONUSAMT'];?>"/></td>
                                </tr>
                                <?php }?>
                           </tfoot>
                        </table>
                    </div>
                  </div>
						
					
				   <div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
							     Less
								</h3>                            
							</div><!-- /.col -->
				    </div>
				
				   <div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                           <tbody>
							   <tr>
                                    <td><strong>Commission (%)</strong></td>
                                    <td><?php echo $obj->getFun72();?><input type="hidden" name="txtCommission" id="txtCommission" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun72();?>" onKeyUp="getFullClaculation();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtCommissionAmt" id="txtCommissionAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows['COMISSION_PER_AMT'];?>"/></td>
                                </tr>
                            
							   <tr>
                                    <td><strong>Brokerage (%)</strong></td>
								    <td><?php echo $obj->getFun74();?><input type="hidden" name="txtBrokerage" id="txtBrokerage" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun74();?>" onKeyUp="getFullClaculation();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtBrokerageAmt" id="txtBrokerageAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows['BROKERAGE_PER_AMT'];?>"/></td>
                                </tr>
                            </tbody>
                          </table>
                    </div>
                  </div>
				  

                    <div class="box"  id="bunkerdiv2" style="display:none;">
                      <div class="box-body no-padding">
                      <h4>Cost Of Bunker On ReDelivery</h4>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									
                                </tr>
                            </thead>
                            <tbody>
                        <?php  
						
						     $sql2 = "select * from invoice_tchire_slave2 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='REDEL'";
							 $res2 = mysql_query($sql2) or die($sql2);
							 $num2 = mysql_num_rows($res2);
									
							  if($num2>0)
							  {
						      while($rows2 = mysql_fetch_assoc($res2))
								{
								?>
                                  <tr>
                                    <td><?php echo $obj->getBunkerGradeBasedOnID($rows2['BUNKERID']);?></td>
								    <td><?php echo $rows2['BUNKER_QTY'];?></td>
                                    <td><?php echo $rows2['BUNKER_PRICE'];?></td>
                                    <td><?php echo $rows2['BUNKER_AMT'];?></td>
                                </tr>
                            
                                <?php }}?>
                            </tbody>
                         </table>
                    </div>
                  </div>
				  
				  		
				<div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                   
                                    <th>Description</th>
                                     <?php $sql2 = "select * from invoice_tchire_slave1 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='SUB'";
									$res2 = mysql_query($sql2);
									$num2 = mysql_num_rows($res2);
									if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									?>
                          			<th>Amount<input type="hidden" name="txtSubID" id="txtSubID" value="<?php echo $num21;?>"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblSub">
                            <?php if($num2 >0)
							   {$i = 0;
								while($rows2 = mysql_fetch_assoc($res2))
								{$i++;
								?>
                                <tr id="Sub_Row_<?php echo $i;?>">
                                   <td><?php echo $rows2['DESCRIPTION'];?></td>
                                   <td><?php echo $rows2['AMOUNT'];?></td>
                                </tr>
                                <?php }}else{?>
                                
                                <?php }?>
                            </tbody>
                            
                        </table>
                    </div>
                  </div>
						
					<div class="box">
							<div class="box-header">
							</div><!-- /.box-header -->
							
						</div>
                    <?php 
							$sql4 = "select * from invoice_tchire_master where COMID='".$comid."' and STATUS=2";
	                        $res4 = mysql_query($sql4);
							$num4 = mysql_num_rows($res4);
							if($num4>0)
							{?>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                   <tbody>
							   <?php  $i=0;
								while($rows4 = mysql_fetch_assoc($res4))
								{$i++;
								?>
                                    <tr>
                                        <td width="30%"><?php echo $rows4['INVOICE_NO'];?></td>
                                        <td width="30%"></td>
                                        <td width="30%"><input type="text" autocomplete="off" name="txtPrevInvoiceAmt_<?php echo $i;?>" id="txtPrevInvoiceAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" readonly value="<?php echo $rows4['BALANCE_TO_OWNER'];?>"/></td>
                                    </tr>
                         <?php }  ?>    
                               </tbody>
                             </table>
                         </div>
                     <?php }?>
                    	
				  
				  <div class="box-body no-padding">
                        <table class="table table-striped">
                           <tbody>
                               <tr>
                                    <td><h4>Balance To Owner</h4></td>
                                    <td></td>
                                    <td><input type="text" name="txtFinalAmt" id="txtFinalAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows['BALANCE_TO_OWNER'];?>"/></td>
                                </tr>
                            </tbody>
                          </table>
                    </div>
				       
                       <div class="row invoice-info">
							<div class="col-sm-12 invoice-col">
							  Banking Details
								<address>
                                <?php if($rec==0)
								{
									
									?>
                                 <textarea class="form-control areasize" name="txtBankingDetails" disabled id="txtBankingDetails" rows="5" placeholder="Banking Details..."><?php echo $obj->getVendorListNewData($vendorid,"BANKING_DETAILS");?></textarea>
                                 <?php }else{?>
                                 <textarea class="form-control areasize" name="txtBankingDetails" disabled id="txtBankingDetails" rows="5" placeholder="Banking Details..."><?php echo $rows['BANKINGDATAILS'];?></textarea>
                                 <?php }?>
								</address>
							</div><!-- /.col -->
						</div>				
					</form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>