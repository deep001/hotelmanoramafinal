<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");

require_once("../../includes/functions_internal_user_dryout.inc.php");

$obj = new data();

$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')   
{
 	
	$submitcustomerdetail = $obj->insertCustomerDetails();
	 if($submitcustomerdetail->status == true)
			{
		        $successmessage = $submitcustomerdetail->msg;
	        }
	   else if ($submitcustomerdetail->status == false) {
                $responseerror =  $submitcustomerdetail->msg;
                  
            } 
}
$pagename = basename($_SERVER['PHP_SELF']);

$rigts    = explode(',',$obj->getUserRights($_SESSION['uid'],$_SESSION['moduleid'],18));
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
	<?php $display->js(); ?>

<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(18); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ragistration Form&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Customers&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Ragistration</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="customerlist.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
					   <?php
						echo (isset($successmessage))? '<div class="alert alert-primary" style="color:red;">'.$successmessage.'</div>':'';
						echo (isset($responseerror))? '<div class="alert alert-primary" style="color:red;">'.$responseerror.'</div>':'';
						?>
				<form role="form" name="frmnew" id="frmnew" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">	
					
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             Add Customer<select name="selVendor" id="selVendor" style="display:none;"><?php //$obj->getVendorListNewUpdate();?></select>
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                           Customer's Name
                            <address>
                              <input type="text" name="customername" id="customername"  class="form-control required"  placeholder="" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                          Father's Name
                            <address>
                             <!--   <select  name="selShipper" class="form-control required" id="selShipper" ></select>-->
                             <input type="text" name="fathername" id="fathername"  class="form-control required"  placeholder="" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                          Mother's Name
                            <address>
                              <!--  <select  name="selCharterer" class="form-control required" id="selCharterer" ></select>-->
                              <input type="text" name="mothername" id="mothername"  class="form-control required"  placeholder="" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                            Date of Birth
                            <address>
                               <!--<select  name="selOwner" class="form-control required" id="selOwner" ></select>-->
							<!--	<input type="text" name="dob" id="dob" style="width:90px;color:#175c84;" class="input-text" value="<?php //echo date("d-m-Y",time());?>" placeholder"dd-mm-yy" />-->
                               <!--<input type="text"  name="dob"  class="form-control required" id="dob" value="" placeholder"" />-->
								<input type="text"  name="dob"  class="form-control required" id="dob" placeholder="dd-mm-yyyy"  value=""/>
                            </address>
                        </div><!-- /.col -->
					</div>
					
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                           Email ID
                            <address>
                              <input type="text" name="emailid" id="emailid"  class="form-control required"  placeholder="" autocomplete="off" value=""/>
                            </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
                          Phone Number
                            <address>
                             
                             <input type="text" name="contactno" id="contactno"  class="form-control required"  placeholder="" autocomplete="off" value=""/>
                            </address>
                        </div>
						  <div class="col-sm-3 invoice-col">
                          Permenent Address
                            <address>
                             <textarea rows="4" cols="10" name="peraddress" id="peraddress" class="form-control required"></textarea>
                            
                            </address>
                        </div>
						  <div class="col-sm-3 invoice-col">
                          Nationality
                            <address>
                             <select  name="nationality" class="form-control required" id="nationality" >
                               <option value="">----Select from list----</option>
								<?php 
                                $obj->getNation();
                                ?>
                                </select>
                            </address>
                        </div>
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                         Date of Visit
                            <address>
                           <input type="text"  name="dov"  class="form-control required" id="dov" value="" placeholder="dd-mm-yyyy" />
								<!--<input type="text" name="dov" id="dov" style="width:90px;color:#175c84;" class="input-text" value="<?php //echo date("d-m-Y",time());?>" placeholder"dd-mm-yy" />-->
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-3 invoice-col">
                           Check In Time
                            <address>
                               <select  name="checkintime" class="form-control required" id="checkintime" >
                               <option value="">----Select from list----</option>
								<?php 
                                $obj->get15MinTimeSlots();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
						  <div class="col-sm-3 invoice-col">
                         Departure Date
                            <address>
                           <input type="text"  name="departuredate"  class="form-control required" id="departuredate" value="" placeholder="dd-mm-yyyy" />
								<!--<input type="text" name="dov" id="dov" style="width:90px;color:#175c84;" class="input-text" value="<?php //echo date("d-m-Y",time());?>" placeholder"dd-mm-yy" />-->
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                           Check Out Time
                            <address>
                              <select  name="checkouttime" class="form-control required" id="checkouttime" >
                               <option value="">----Select from list----</option>
								<?php 
                                $obj->get15MinTimeSlots();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
                       <!-- /.col -->
                        
					</div>
					  <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                         Coming From
                            <address>
                           <input type="text"  name="comingfrom"  class="form-control required" id="comingfrom" value="" placeholder="" />
								<!--<input type="text" name="dov" id="dov" style="width:90px;color:#175c84;" class="input-text" value="<?php //echo date("d-m-Y",time());?>" placeholder"dd-mm-yy" />-->
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-3 invoice-col">
                           Going to
                            <address>
                             <input type="text"  name="goingto"  class="form-control required" id="goingto" value="" placeholder="" />
                             </address>
                        </div><!-- /.col -->
						  <div class="col-sm-3 invoice-col">
                         Purpose of Visit
                            <address>
                       <textarea rows="4" cols="10" name="perpose" id="perpose" class="form-control required"></textarea>
                            </address>
                        </div>
						  <div class="col-sm-3 invoice-col">
                         Number of Persons
                            <address>
                        <select  name="male" class="form-control required" id="male" >
                               <option value="">----Male----</option>
								<?php 
                               $obj->getNumber();
                                ?>
                                </select>
								 <select  name="female" class="form-control required" id="female" >
                               <option value="">----Female----</option>
								<?php 
                               $obj->getNumber();
                                ?>
                                </select>
								 <select  name="children" class="form-control required" id="children" >
                               <option value="">----Children----</option>
								<?php 
                               $obj->getNumber();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                       
                       <!-- /.col -->
                        
					</div>
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                         Passport No
                            <address>
                           <input type="text"  name="passportno"  class="form-control required" id="passportno" value="" placeholder="" />
								<!--<input type="text" name="dov" id="dov" style="width:90px;color:#175c84;" class="input-text" value="<?php //echo date("d-m-Y",time());?>" placeholder"dd-mm-yy" />-->
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-3 invoice-col">
                           Date of Issue
                            <address>
                             <input type="text"  name="dateofissue"  class="form-control required" id="dateofissue" placeholder="dd-mm-yyyy"  value=""/>
                             </address>
                        </div><!-- /.col -->
						  <div class="col-sm-3 invoice-col">
                         Validity 
                            <address>
                     <input type="text"  name="validity"  class="form-control required" id="validity" placeholder="dd-mm-yyyy"  value=""/>
                            </address>
                        </div><!-- /.col -->
                       
                       <!-- /.col -->
                        
					</div>
					<div class="row invoice-info">
                        <div class="col-sm-2 invoice-col">
                         Room No
                            <address>
                           <input type="text"  name="roomno"  class="form-control required" id="roomno" value="" placeholder="" />
							
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-2 invoice-col">
                           Room Rent
                            <address>
                             <input type="text"  name="roomrent"  class="form-control required" id="roomrent" placeholder=""  value=""/>
                             </address>
                        </div><!-- /.col -->
						  <div class="col-sm-3 invoice-col">
                        Amount
                            <address>
                     <input type="text"  name="amount"  class="form-control required" id="amount" placeholder=""  value=""/>
                            </address>
                        </div>
						  <div class="col-sm-3 invoice-col">
                        Paid by Customer
                            <address>
                     <input type="text"  name="paidbycustomer"  class="form-control required" id="paidbycustomer" placeholder=""  value="" onKeyUp="getBalance();" />
                            </address>
                        </div>
						 <div class="col-sm-2 invoice-col">
                        Balance
                            <address>
                     <input type="text"  name="balance"  class="form-control required" id="balance" placeholder=""  value=""/>
                            </address>
                        </div>
                       
                       <!-- /.col -->
                        
					</div>
                    <div class="row invoice-info">
						 <div class="col-sm-3 invoice-col">
                            Aadhar Card(Primary Customer)
                            <address>
                                <input type="file"  name="aadharcard"  class="form-control required" id="aadharcard"  autocomplete="off" value=""/>
                            </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
                         Voter Id
                            <address>
                                <input type="file"  name="voterid"  class="form-control required" id="voterid"  autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                       Pen Card
                            <address>
                                <input type="file"  name="pencard"  class="form-control required" id="pencard"  autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
						 <div class="col-sm-3 invoice-col">
                       Profile Photo
                            <address>
                                <input type="file"  name="profilephoto"  class="form-control required" id="profilephoto"  autocomplete="off" value=""/>
                            </address>
                        </div>
						<!--<div class = "newone">
                        <div class="col-sm-3 invoice-col">
                     &nbsp;
                          <address>
                               <input name="rdoWstype" id="rdoWstype1" type="radio" style="cursor:pointer;" value="1"     checked  />&nbsp;&nbsp;WS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:10px; font-style:italic;"></span><input name="rdoWstype" id="rdoWstype2" type="radio" style="cursor:pointer;" value="2" />&nbsp;&nbsp;Lumpsum
                           <address>
                        </div>
                        <div class="col-sm-3 invoice-col">
                          &nbsp;
                            <address>
                                <input type="text"  name="txtWsLumpsum"  class="form-control numeric" id="txtWsLumpsum" placeholder="USD" autocomplete="off" value=""/>
                            </address>
                         </div>
					     </div>-->
                        
					</div>
				   <div class="row invoice-info">
						 <div class="col-sm-3 invoice-col">
                            ID Card(Member 1)
                            <address>
                                <input type="file"  name="IDMemberfirst"  class="form-control required" id="IDMemberfirst"  autocomplete="off" value=""/>
                            </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
                         ID Card(Member 2)
                            <address>
                                <input type="file"  name="IDMembersecond"  class="form-control required" id="IDMembersecond"  autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                       ID Card(Member 3)
                            <address>
                                <input type="file"  name="IDMemberthird"  class="form-control required" id="IDMemberthird"  autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
						 <div class="col-sm-3 invoice-col">
                       ID Card(Member 4)
                            <address>
                                <input type="file"  name="IDMemberfourth"  class="form-control required" id="IDMemberfourth"  autocomplete="off" value=""/>
                            </address>
                        </div>
						
                        
					</div>
					<div class="row invoice-info">
						 <div class="col-sm-3 invoice-col">
                            ID Card(Member 5)
                            <address>
                                <input type="file"  name="IDMemberfive"  class="form-control required" id="IDMemberfive"  autocomplete="off" value=""/>
                            </address>
                        </div>
                        <div class="col-sm-3 invoice-col">
                         ID Card(Member 6)
                            <address>
                                <input type="file"  name="IDMembersix"  class="form-control required" id="IDMembersix"  autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                       ID Card(Member 7)
                            <address>
                                <input type="file"  name="IDMemberseven"  class="form-control required" id="IDMemberseven"  autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
						 <div class="col-sm-3 invoice-col">
                       ID Card(Member 8)
                            <address>
                                <input type="file"  name="IDMembereight"  class="form-control required" id="IDMembereight"  autocomplete="off" value=""/>
                            </address>
                        </div>
						
                        
					</div>
                    
                    
                    
					<div class="box-footer" align="right">
                    <?php if(in_array(2, $rigts)){?>
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Save as Draft</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-success btn-flat" onClick="return getValidate(1);">Send to Checker</button> 
					<?php }?>
                    <input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>




	
		<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/sort.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type='text/javascript' src='../../js/jquery.simplemodal.js'></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />
<script src="../../js/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" href="../../css/chosen.css" rel="stylesheet" />

  <script src="../../js/jquery-3.4.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
//$("#selShipper,#selCharterer,#selOwner,#selReceiver").html($("#selVendor").html());
//$(".numeric").numeric();
//$(".areasize").autosize({append: "\n"});
/*$('#txtFDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#txtTDate').datepicker('setStartDate', new Date(getString($(this).val())));
 });*/
 
/*$('#dob,#dov').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){   });*/

 $("#dob").datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
});
/*$('#dob,#dov').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#dob').datepicker('setStartDate', new Date(getString($(this).val())));
 });*/
function getString(var1)
{
  var var2 = var1.split('-');  
  return var2[2]+'/'+var2[1]+'/'+var2[0];
}


$(".areasize").autosize({append: "\n"});
$("[id^=ui-datepicker-div]").hide();
$("#selVOPID").val(" ");


$("#frmnew").validate({
	rules: {
		
		
		/*     txtFreight: {required:function(){
			   if($("#selBType").val() == '1' || $("#selBType").val() == '3')
			   {
			       return true;
			   }
			   else
			   {
			       return false;
			   }
			  }},
			txtWsLumpsum: {required:function(){
			   if($("#selBType").val() == '2')
			   {
			       return true;
			   }
			   else
			   {
			       return false;
			   }
			  }}, */
		},
	messages: {
	
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
getVesselTypeList();
getCargoTypList();
//getShowhide();
	
	
	
});

	





function getVesselTypeList()
{
    var vesseltypelist = <?php echo $obj->getVesselTypeListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selVesselType');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selVesselType");
	$.each(vesseltypelist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selVesselType");
	});
}


function getCargoTypList()
{
    var cargolist = <?php echo $obj->getCargoListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selCargo');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selCargo");
	$.each(cargolist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selCargo");
	});
}


function getValidate(val)
{
	$("#upstatus").val(val);
	return true;
}
	
function getShowhide()
{
		    var selBType = $("#selBType").val();
		    $("#selBType").find("option:selected").each(function(){
            var optionValue = $("#selBType").attr("value");
            if(optionValue == '2'){
                $(".newone").show();
                $(".basef").hide();
            } else{
                $(".newone").hide();
                $(".basef").show();
            }
        });
}
	//function fillupp
</script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
$(document).ready(function() {
		 $('#rdoWstype1').click(function() {
		 if ($("input[name='rdoWstype']:checked").val() == '1')
            {
			$( "#txtWsLumpsum" ).attr('placeholder','WS')
			//$("#rdoWstype2").removeAttr("2");
			}
         });
		 $('#rdoWstype2').click(function() {
		 if ($("input[name='rdoWstype']:checked").val() == '2')
            {
			$( "#txtWsLumpsum" ).attr('placeholder','USD')
			//$("#rdoWstype1").removeAttr("1");
			}
         });
});
	
	function getBalance()
	{
		var paidamount = $("#paidbycustomer").val();
		var amount = $("#amount").val();
		if(isNaN(paidamount)){paidamount =0; }
		if(isNaN(amount)){amount =0;}
		var remainbalance = parseFloat(amount) - parseFloat(paidamount);
		if(isNaN(remainbalance)){remainbalance =0;}
		$("#balance").val(remainbalance.toFixed(2));
		
	}
</script>
    </body>
</html>