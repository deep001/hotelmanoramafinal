<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$mappingid 		= $_REQUEST['mappingid'];
$cost_sheet_id 	= $_REQUEST['cost_sheet_id'];
$page  			= $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}

/*if (@$_REQUEST['action'] == 'submit')
 { 
 	$msg = $obj->updateTCICostSheetDetails();
	header('Location:./'.$page_link.'?msg='.$msg);
 }*/
//$l_cost_sheet_id = $obj->getLatestCostSheetID($mappingid);
$obj->viewFreightEstimationRecords($mappingid,$cost_sheet_id);

$actual_sp 	= $obj->getActualSeaPassageData($obj->getFun1());
$actual_lp 	= $obj->getActualLoadPortData($obj->getFun1());
$actual_dp 	= $obj->getActualDeschargePortData($obj->getFun1());
$actual_tp 	= $obj->getActualTransitPortData($obj->getFun1());
$pagename  	= basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&cost_sheet_id=".$cost_sheet_id."&page=".$page;
$rdoMMarket = $obj->getFun8();
$rdoDWT     = $obj->getFun7();
$rdoQty     = $obj->getFun28();
$rdoCap     = $obj->getFun29();
$rdoQtyType = $obj->getFun31();
if($rdoQtyType ==""){$rdoQtyType = 1;}
$b_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"B_FULL_SPEED");
if($b_full_speed == "" ){$bfs = 0;}else{$bfs = $b_full_speed;}
$b_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"B_ECO_SPEED1");
if($b_ech_speed1 == "" ){$bes1 = 0;}else{$bes1 = $b_ech_speed1;}
$b_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"B_ECO_SPEED2");
if($b_ech_speed2 == "" ){$bes2 = 0;}else{$bes2 = $b_ech_speed2;}
$fo_b_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_FO_FULL_SPEED");
if($fo_b_full_speed == ""){$fo_bfs = 0;}else{$fo_bfs = $fo_b_full_speed;}
$fo_b_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_FO_ECO_SPEED1");
if($fo_b_ech_speed1 == ""){$fo_bes1 = 0;}else{$fo_bes1 = $fo_b_ech_speed1;}
$fo_b_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_FO_ECO_SPEED2");
if($fo_b_ech_speed2 == ""){$fo_bes2 = 0;}else{$fo_bes2 = $fo_b_ech_speed2;}
$do_b_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_DO_FULL_SPEED");
if($do_b_full_speed == ""){$do_bfs = 0;}else{$do_bfs = $do_b_full_speed;}
$do_b_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_DO_ECO_SPEED1");
if($do_b_ech_speed1 == ""){$do_bes1 = 0;}else{$do_bes1 = $do_b_ech_speed1;}
$do_b_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_DO_ECO_SPEED2");
if($do_b_ech_speed2 == ""){$do_bes2 = 0;}else{$do_bes2 = $do_b_ech_speed2;}


$l_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"L_FULL_SPEED");
if($l_full_speed == "" ){$lfs = 0;}else{$lfs = $l_full_speed;}
$l_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"L_ECO_SPEED1");
if($l_ech_speed1 == "" ){$les1 = 0;}else{$les1 = $l_ech_speed1;}
$l_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"L_ECO_SPEED2");
if($l_ech_speed2 == "" ){$les2 = 0;}else{$les2 = $l_ech_speed2;}
$fo_l_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_FO_FULL_SPEED");
if($fo_l_full_speed == ""){$fo_lfs = 0;}else{$fo_lfs = $fo_l_full_speed;}
$fo_l_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_FO_ECO_SPEED1");
if($fo_l_ech_speed1 == ""){$fo_les1 = 0;}else{$fo_les1 = $fo_l_ech_speed1;}
$fo_l_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_FO_ECO_SPEED2");
if($fo_l_ech_speed2 == ""){$fo_les2 = 0;}else{$fo_les2 = $fo_l_ech_speed2;}
$do_l_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_DO_FULL_SPEED");
if($do_l_full_speed == ""){$do_lfs = 0;}else{$do_lfs = $do_l_full_speed;}
$do_l_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_DO_ECO_SPEED1");
if($do_l_ech_speed1 == ""){$do_les1 = 0;}else{$do_les1 = $do_l_ech_speed1;}
$do_l_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_DO_ECO_SPEED2");
if($do_l_ech_speed2 == ""){$do_les2 = 0;}else{$do_les2 = $do_l_ech_speed2;}

$fo_inport_idle = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PI_FO_FULL_SPEED");
if($fo_inport_idle == ""){$foidle = 0;}else{$foidle = $fo_inport_idle;}
$fo_inport_wrking = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PW_FO_FULL_SPEED");
if($fo_inport_wrking == ""){$fowrking = 0;}else{$fowrking = $fo_inport_wrking;}

$do_inport_idle = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PI_DO_FULL_SPEED");
if($do_inport_idle == ""){$doidle = 0;}else{$doidle = $do_inport_idle;}
$do_inport_wrking = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PW_DO_FULL_SPEED");
if($do_inport_wrking == ""){$dowrking = 0;}else{$dowrking = $do_inport_wrking;}

$submitid1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUBMITID");
if($submitid1 == ""){$submitid = 0;}else{$submitid = $submitid1;}

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>

<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}

</style>
</head>
    <body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onClick="getFinalCalculation();">Calculate</a>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
					<!--   content put here..................-->
					<div align="right">
                        <a href="view_cost_sheet_list.php?mappingid=<?php echo $mappingid;?>&page=<?php echo $page;?>"><button class="btn btn-info btn-flat">Back</button></a>
                    </div>
					<div style="height:10px;">&nbsp;</div>
						
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Voyage Financials 
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
				
                    <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
						<div class="col-sm-4 invoice-col" style="display:none;" >
							<address>
							   <select  name="selVendor" class="select form-control" id="selVendor" >
									<?php $obj->getVendorListNewUpdate("");	?>
								</select> 
							</address>
						</div><!-- /.col -->
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Fixture Type
								<address>
								<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFreightFixtureOutBasedOnID(2);?></strong>
								</address>
							</div><!-- /.col -->
						</div>
                            
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								 Main Particulars
								</h2>                            
							</div><!-- /.col -->
						</div>
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Nom ID
								<address>
									<input type="text" name="txtNomID" id="txtNomID" class="form-control" readonly value="<?php echo $obj->getMappingData($mappingid,"NOM_NAME");?>" placeholder="Nom ID"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								 Vessel Name
								<address>
								   <input type="text" name="txtVName" id="txtVName" class="form-control" readonly value="<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?>" placeholder="Vessel Name" />
								</address>
							</div><!-- /.col -->
							 <div class="col-sm-4 invoice-col">
								 Vessel Type
								<address>
								   <input type="text" name="txtVType" id="txtVType" class="form-control" readonly value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_TYPE"));?>" placeholder="Vessel Type"/>
								</address>
							</div><!-- /.col -->
						</div>
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Date
								<address>
									<input type="text" name="txtDate" id="txtDate" class="form-control" readonly placeholder"dd-mm-yy"  value="<?php if($obj->getFun3() == ""){ echo "dd-mm-yy"; }else{ echo date("d-m-Y",strtotime($obj->getFun3())); }?>"/>
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Voyage No.
								<address>
									<input type="text" name="txtVNo" id="txtVNo" class="form-control" readonly autocomplete="off" placeholder="Voyage No." value="<?php echo $obj->getFun6()?>"/>
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								 Voyage Financials Name
								<address>
									<input type="text" name="txtENo" style="color:red;" id="txtENo" class="form-control" readonly placeholder=" Voyage Financials Name" value="<?php echo $obj->getCostSheetNameBasedOnID($cost_sheet_id);?>"/>
								</address>
							</div><!-- /.col -->
							
						 </div>
                            
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoDWT" class="checkbox" id="rdoDWT1" type="radio" value="1" disabled <?php if($rdoDWT == 1) echo "checked"; ?> onClick="showDWTField();"  />
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
								<input name="rdoDWT" class="checkbox" id="rdoDWT2" type="radio" readonly value="2" disabled <?php if($rdoDWT == 2) echo "checked"; ?> onClick="showDWTField();" />
								</address>
							</div><!-- /.col -->
						   
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>
								<address>
									<input type="text" name="txtDWTS" id="txtDWTS" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUMMER_1");?>" placeholder="DWT (Summer)" >
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>
								<address>
									<input type="text" name="txtDWTT" id="txtDWTT" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParticularData('TROPICAL_1','vessel_master_1',$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"));?>" placeholder="DWT (Tropical)" disabled="disabled" />
								</address>
							</div><!-- /.col -->
							
							
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoCap" id="rdoCap1" type="radio" style="cursor:pointer;"  disabled value="1"  <?php if($rdoCap == 1) echo "checked"; ?> onClick="showCapField();"  />
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoCap" id="rdoCap2" type="radio" style="cursor:pointer;" disabled value="2"  <?php if($rdoCap == 2) echo "checked"; ?> onClick="showCapField();" />
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
								<address>
									<input type="text" name="txtGCap" id="txtGCap" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"GRAIN");?>" />
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Bale Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
								<address>
									<input type="text" name="txtBCap" id="txtBCap" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BALE");?>" disabled="disabled" />
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-8 invoice-col">
								&nbsp;
								<address>
									&nbsp;
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								SF <span style="font-size:10px; font-style:italic;">(ft3/lt)</span>
								<address>
									<input type="text" name="txtSF" id="txtSF"  autocomplete="off" readonly class="form-control" value="<?php echo $obj->getFun32();?>"  onkeyup="getTotalDWT(),getTotalDWT1()" />
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-8 invoice-col">
								&nbsp;
								<address>
									&nbsp;
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Loadable <span style="font-size:10px; font-style:italic;">(MT)</span>
								<address>
									<input type="text" name="txtLoadable" id="txtLoadable" autocomplete="off" class="form-control" readonly value="<?php echo $obj->getFun31();?>"/>
								</address>
							</div><!-- /.col -->
						</div>
                             
                        
                        <div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								<address>
									<input name="rdoQtyType" id="rdoQtyType1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoQtyType == 1) echo "checked"; ?> onClick="showHideQtyVendorDiv(2);"  disabled /> <strong>Single</strong>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								<address>
									<input name="rdoQtyType" id="rdoQtyType2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoQtyType == 2) echo "checked"; ?> onClick="showHideQtyVendorDiv(2);" disabled /> <strong>Distributed</strong>
								</address>
							</div><!-- /.col -->
						</div>
                        
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								 Market
								</h2>                            
							</div><!-- /.col -->
						</div>
                        
                        <div class="row invoice-info" id="divQty1">
                                <div class="col-sm-3 invoice-col">
                                  Customer
                                    <address>
										<select  name="selQtyVendorList" class="select form-control" id="selQtyVendorList" disabled ></select>
										<script>
                                            $("#selQtyVendorList").html($("#selVendor").html());
                                            $("#selQtyVendorList").val('');
                                        </script>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                   Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(Local Currency/MT)</span>
                                    <address>
										<input type="text" name="txtQtyLocalAggriedFreight" id="txtQtyLocalAggriedFreight" class="form-control"  autocomplete="off" value="" placeholder="Agreed Gross Freight (Local Currency)" onKeyUp="getQtyLocalFreightCal();" readonly/>
                                    </address>
                                </div><!-- /.col -->
                                
                                <div class="col-sm-3 invoice-col">
                                  Currency
                                    <address>
										<select  name="selCurrencyDisList" class="select form-control" id="selCurrencyDisList" disabled>
                                        	<?php $obj->getCurrencyList(); ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                
                                <div class="col-sm-3 invoice-col">
                                   Exchange Rate
                                    <address>
										<input type="text" name="txtDisExchangeRate" id="txtDisExchangeRate" class="form-control"  autocomplete="off" value="" placeholder="Exchange Rate" onKeyUp="getQtyLocalFreightCal();" readonly/>
                                    </address>
                                </div><!-- /.col -->
                                <div style="clear:both;"></div>
                                <div class="col-sm-3 invoice-col">
                                   Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
										<input type="text" name="txtQtyAggriedFreight" id="txtQtyAggriedFreight" class="form-control"  autocomplete="off" value="" placeholder="Agreed Gross Freight (USD/MT)" onKeyUp="getQtyFreightVendorCal();" readonly/>
                                    </address>
                                </div><!-- /.col -->
                                
								<div class="col-sm-3 invoice-col">
                                    Quantity <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                         <input type="text" name="txtFreightQty" id="txtFreightQty" class="form-control"  value="" placeholder="Quantity" onKeyUp="getQtyFreightVendorCal();" readonly/>
                                    </address>
                                </div><!-- /.col -->
								<div class="col-sm-3 invoice-col">
                                    Gross Freight <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address> 
                                    <span id="spanQtyFreight">0</span>
                                        <input type="hidden" name="txtQtyFreight" id="txtQtyFreight" class="form-control" value="0" readonly />
                                    </address>
                                </div><!-- /.col -->
								<div class="col-sm-3 invoice-col">
                                    &nbsp;
                                    <address>
										<!--<button type="button" class="btn btn-primary btn-flat" onClick="addQtyVendorDetails()" >Add</button>-->
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            
							<div class="box" id="divQty2">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th width="25%">Customer</th>
												<th>Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span></th>
												<th>Quantity <span style="font-size:10px; font-style:italic;">(MT)</span></th>
                                                <?php 
												$sql12 = "select * from fca_tci_multiple_vendor_qty_details where FCAID='".$obj->getFun1()."'";
												$res12 = mysql_query($sql12);
												$rec12 = mysql_num_rows($res12);
											    ?>
                                                
												<th>Gross Freight <span style="font-size:10px; font-style:italic;">(USD)</span><input type="hidden" name="txtQTYID" id="txtQTYID" value="<?php echo $rec12;?>" /></th>
												</th>
											</tr>
										</thead>
										<tbody id="tblQtyFreight">	
                                        <?php if($rec12==0)
										{?>
											<tr id="tbrQtyVRow_empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
                                            <?php }
											else
											{$i = 0;
											while($rows2 = mysql_fetch_assoc($res12))
											{$i = $i + 1;
											?>
                                            <tr id="tbrQtyVRow_<?php echo $i;?>">
                                                <td><a href="#tb1" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($rows2['QTY_VENDORID']);?><input type="hidden" name="selQtyVendorList_<?php echo $i;?>" id="selQtyVendorList_<?php echo $i;?>" autocomplete="off" value="<?php echo $rows2['QTY_VENDORID'];?>"/></td>
                                                <td><?php echo $rows2['AGREED_GROSS_FREIGHT'];?><input type="hidden" name="txtQtyAggriedFreight_<?php echo $i;?>" id="txtQtyAggriedFreight_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['AGREED_GROSS_FREIGHT'];?>"/></td>
                                                <td><?php echo $rows2['QUANTITY'];?><input type="hidden" name="txtFreightQty_<?php echo $i;?>" id="txtFreightQty_<?php echo $i;?>" value="<?php echo $rows2['QUANTITY'];?>"/></td>
                                                <td><?php echo $rows2['GROSS_FREIGHT'];?><input type="hidden" name="txtQtyFreight_<?php echo $i;?>" id="txtQtyFreight_<?php echo $i;?>" value="<?php echo $rows2['GROSS_FREIGHT'];?>"/><input type="hidden" name="txtCurrencyDis_<?php echo $i;?>" id="txtCurrencyDis_<?php echo $i;?>" value="<?php echo $rows2['CURRENCYID'];?>"/><input type="hidden" name="txtQtyLocalAggried_<?php echo $i;?>" id="txtQtyLocalAggried_<?php echo $i;?>" value="<?php echo $rows2['AGREED_GROSS_FREIGHT_LOCAL'];?>"/><input type="hidden" name="txtDisExchangeRate_<?php echo $i;?>" id="txtDisExchangeRate_<?php echo $i;?>" value="<?php echo $rows2['EXCHANGE_RATE'];?>"/></td>
                                            </tr>
                                            
                                            <?php }}?>
										</tbody>
                                        <tfoot>
                                          <tr><td>Total</td><td></td><td></td><td><input type="text" name="txtTotalFreightQty" id="txtTotalFreightQty" class="form-control" readonly value=""/></td><td><input type="text" name="txtTotalQtyFreight" id="txtTotalQtyFreight" class="form-control" readonly value=""/></td></tr>
                                        </tfoot>
									</table>
								</div>
							</div>
						 
						<div class="row invoice-info" id="divMarket1">
							<div class="col-sm-4 invoice-col">
								<address>
								  <input name="rdoMMarket" class="checkbox" id="rdoMMarket1" type="radio" value="1"  <?php if($rdoMMarket == 1) echo "checked"; ?>  onclick="showMMarketField();"  disabled />
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								<address>
									<input name="rdoMMarket" class="checkbox" id="rdoMMarket2" type="radio" value="2" <?php if($rdoMMarket == 2) echo "checked"; ?> onClick="showMMarketField();" disabled />
								</address>
							</div><!-- /.col -->
						</div>
                        
                        <div class="row invoice-info" id="divMarket2">
                            <div class="col-sm-4 invoice-col">
                               Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(Local Currency/MT)</span>
                                <address>
                                    <input type="text" name="txtMarLocalAggriedFreight" id="txtMarLocalAggriedFreight" class="form-control"  autocomplete="off" value="<?php echo $obj->getFun33();?>" placeholder="Agreed Gross Freight (Local Currency)" onKeyUp="getMarLocalFreightCal();" readonly/>
                                </address>
                            </div><!-- /.col -->
                            
                            <div class="col-sm-4 invoice-col">
                              Currency
                                <address>
                                    <select  name="selCurrencyMarList" class="select form-control" id="selCurrencyMarList" disabled>
                                    <?php $obj->getCurrencyList(); ?>
                                    </select>
                                </address>
                            </div><!-- /.col -->
                            
                            <script>$("#selCurrencyMarList").val(<?php echo $obj->getFun34();?>);</script>
                            
                            <div class="col-sm-4 invoice-col">
                               Exchange Rate
                                <address>
                                    <input type="text" name="txtMarExchangeRate" id="txtMarExchangeRate" class="form-control"  autocomplete="off" value="<?php echo $obj->getFun35();?>" placeholder="Exchange Rate" onKeyUp="getMarLocalFreightCal();" readonly/>
                                </address>
                            </div><!-- /.col -->
                            <div style="clear:both;"></div>
                            <div class="col-sm-4 invoice-col">
								Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
								<address>
									<input type="text" name="txtMTCPDRate" id="txtMTCPDRate" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" placeholder="Agreed Gross Freight (USD/MT)" value="<?php echo $obj->getFun9();?>" readonly />
								</address>
                            </div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Lumpsum <span style="font-size:10px; font-style:italic;">(USD)</span>
								<address>
									<input type="text" name="txtMLumpsum" id="txtMLumpsum" class="form-control" autocomplete="off" disabled="disabled" onKeyUp="getFinalCalculation();" placeholder="Lumpsum (USD)" value="<?php echo $obj->getFun10();?>" readonly/>
								</address>
						   </div><!-- /.col -->
                               <div class="col-sm-4 invoice-col">
									Addnl Cargo Rate <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
										<input type="text"  name="txtAddnlCRate" id="txtAddnlCRate" class="form-control" autocomplete="off" onKeyUp="getFinalCalculation();" placeholder="Addnl Cargo Rate (USD/MT)" value="<?php echo $obj->getFun26();?>" readonly/>
                                    </address>
                               </div><!-- /.col -->
                         </div>
                        
                        <div class="row" id="divMarket3">
							<div class="col-xs-12">
								<h2 class="page-header">
								 Cargo
								</h2>                            
							</div><!-- /.col -->
						</div>
                        <div class="row invoice-info" id="divMarket4">
                                <div class="col-sm-4 invoice-col">
                                    Quantity <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
										<input type="text" name="txtCQMT" id="txtCQMT" class="form-control" autocomplete="off" value="<?php echo $obj->getFun15();?>" onKeyUp="getFinalCalculation();" placeholder="Quantity (MT)" readonly/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                   Cargo Type
                                    <address>
										<select  name="selCType" class="select form-control" id="selCType" disabled>
											<?php 
												$_REQUEST['selCType'] = $obj->getFun16();
												$obj->getCargoTypeList();
											?>
										</select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Cargo Name
                                    <address>
                                        <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCargoContarctForMapping($obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"CARGO_POSITION"));?></strong>
                                    </address>
                                </div><!-- /.col -->
                                
                            </div>
                            
                            <div class="row invoice-info" id="divMarket5">
                                <div class="col-sm-4 invoice-col">
                                  <span style="font-size:13px; font-style:italic; color:#dc631e">( Please put dead freight quantity / addnl quantity separately )</span>  
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <address>
										<input name="rdoQty" class="checkbox" id="rdoQty1" type="radio" value="1"  <?php if($rdoQty == 1) echo "checked"; ?>  onclick="showQtyField();" disabled />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <address>
										<input name="rdoQty" class="checkbox" id="rdoQty2" type="radio" value="2" <?php if($rdoQty == 2) echo "checked"; ?> onClick="showQtyField();" disabled />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                            
                            
                            <div class="row invoice-info" id="divMarket6">
                                <div class="col-sm-4 invoice-col">
                                  &nbsp;
                                    <address>
										<input type="hidden" name="txtAQMT" id="txtAQMT" class="form-control" autocomplete="off" value="<?php echo $obj->getFun26();?>"  onkeyup="getFinalCalculation();" readonly/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    DF Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
										<input type="text" name="txtDFQMT" id="txtDFQMT" class="form-control" autocomplete="off" value="<?php echo $obj->getFun27();?>"  onkeyup="getFinalCalculation();" placeholder="DF Qty (MT)" readonly/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Addnl Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                        <input type="text" name="txtAddnlQMT" id="txtAddnlQMT" class="form-control" autocomplete="off" value="<?php echo $obj->getFun18();?>" onKeyUp="getFinalCalculation();" placeholder="Addnl Qty (MT)" disabled="disabled" readonly/>
                                    </address>
                                </div><!-- /.col -->
								<div class="col-sm-4 invoice-col" style="display:none;" >
                                    <address>
                                       <select  name="selVendor" class="select form-control" id="selVendor" disabled>
											<?php $obj->getVendorListNewUpdate("");	?>
										</select> 
                                    </address>
                                </div><!-- /.col -->
                            </div>
							
							
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
									Passage Locations
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							 <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Location From
                                    <address>
										<input type="text" name="txtLocationFrom" id="txtLocationFrom" class="form-control" autocomplete="off"  value="" placeholder="Location To" readonly/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Location To
                                    <address>
										<input type="text" name="txtLocationTo" id="txtLocationTo" class="form-control"  autocomplete="off" value="" placeholder="Location To" readonly/>
                                    </address>
                                </div><!-- /.col -->
                                
								<div class="col-sm-4 invoice-col">
                                    Distance
                                    <address>
										<input type="text" name="txtLocationDistance" id="txtLocationDistance" class="form-control"  value="" placeholder="distance" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Passage Type
                                    <address>
										<select  name="selPLocationType" class="select form-control" id="selPLocationType" disabled >
											<?php $obj->getPassageType(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Select Speed
                                    <address>
										<select  name="selSLocationSpeed" class="select form-control" id="selSLocationSpeed" disabled>
											<?php $obj->getSelectSpeedList(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
										<!--<button type="button" class="btn btn-primary btn-flat" onClick="addPortRotationLocationDetails()" style="margin-left:100px;">Add</button>-->
										<input type="hidden" name="txtVoyageLTime" id="txtVoyageLTime" class="form-control" autocomplete="off" value="<?php echo $obj->getFun19();?>" />
										<input type="hidden" name="txtTTLVoyageLDays" id="txtTTLVoyageLDays" class="form-control" autocomplete="off" value="<?php echo $obj->getFun20();?>" />
										<input type="hidden" name="txtTTLLFoConsp" id="txtTTLLFoConsp" class="form-control" readonly value="<?php echo $obj->getFun21();?>"  />
										<input type="hidden" name="txtTTLLDoConsp" id="txtTTLLDoConsp" class="form-control" readonly value="<?php echo $obj->getFun22();?>"  />
                                    </address>
                                </div><!-- /.col -->
								
                            </div>
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Location From</th>
												<th>Location To</th>
												<th>Passage Type</th>
												<?php 
												$sql = "select * from fca_tci_sea_passage_location where FCAID='".$obj->getFun1()."'";
												$res = mysql_query($sql);
												$rec = mysql_num_rows($res);
										        ?>
												<th>Distance<input type="hidden" name="p_locationID" id="p_locationID" value="<?php echo $rec; ?>" /></th>
												</th>
											</tr>
										</thead>
										<tbody id="tblPortLocation">
										
											
										<?php if($rec == 0){?>	
											<tr id="PRLrow_Empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
										<?php }else{
										$i=1;
										while($rows = mysql_fetch_assoc($res)){
										//SEA_LOCATIONID, LOCATION_FROM, LOCATION_TO, DISTANCE, PASSAGE_TYPE, SPEED_TYPE, CONSP_FO, CONSP_DO, BALLASTDAYS, FCAID
										?>
										<tr id="prl_Row_<?php echo $i;?>">
										<td><a href="#pr<?php echo $i;?>" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td>
										<td><?php echo $rows['LOCATION_FROM'];?><input type="hidden" name="txtFromLocation_<?php echo $i;?>" id="txtFromLocation_<?php echo $i;?>" value="<?php echo $rows['LOCATION_FROM'];?>"/></td>
										<td><?php echo $rows['LOCATION_TO'];?><input type="hidden" name="txtToLocation_<?php echo $i;?>" id="txtToLocation_<?php echo $i;?>" value="<?php echo $rows['LOCATION_TO'];?>"/></td>
										<td><?php echo $obj->getPassageTypeNameBasedOnID($rows['PASSAGE_TYPE'])." (".$obj->getPassageSpeedBasedOnID($rows['SPEED_TYPE'])." )";?><input type="hidden" name="txtPLocationType_<?php echo $i;?>" id="txtPLocationType_<?php echo $i;?>" value="<?php echo $rows['PASSAGE_TYPE'];?>"/><input type="hidden" name="txtSLocationSpeed_<?php echo $i;?>" id="txtSLocationSpeed_<?php echo $i;?>" value="<?php echo $rows['SPEED_TYPE'];?>"/></td>
										<td><?php echo $rows['DISTANCE'];?><input type="hidden" name="txtLocationDistance_<?php echo $i;?>" id="txtLocationDistance_<?php echo $i;?>" value="<?php echo $rows['DISTANCE'];?>"/><input type="hidden" name="txtLocationConspFO_<?php echo $i;?>" id="txtLocationConspFO_<?php echo $i;?>" value="<?php echo $rows['CONSP_FO'];?>"/><input type="hidden" name="txtLocationConspDO_<?php echo $i;?>" id="txtLocationConspDO_<?php echo $i;?>" value="<?php echo $rows['CONSP_DO'];?>"/><input type="hidden" name="txtBallastDays_<?php echo $i;?>" id="txtBallastDays_<?php echo $i;?>" value="<?php echo $rows['BALLASTDAYS'];?>"/>
										</td>
										</tr>
										<?php }}?>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
									Sea Passage
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="row invoice-info" id="estRow">
                                <div class="col-sm-4 invoice-col">
                                  From Port
                                    <address>
										<select  name="selFPort" class="select form-control" id="selFPort" onChange="getDistance();" disabled>
											<?php $obj->getPortList(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    To Port
                                    <address>
										<select  name="selTPort" class="select form-control" id="selTPort" onChange="getDistance();" disabled>
											<?php //$obj->getPortList(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Distance Type
                                    <address>
										<select  name="selDType" class="select form-control" id="selDType" onChange="getDistance();" disabled>
											<?php $obj->getPortDistanceType(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Speed Adj.
                                    <address>
										<input type="text" name="txtWeather" id="txtWeather" class="form-control" autocomplete="off" value="" placeholder="Speed Adj." readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Margin Distance <span style="font-size:10px; font-style:italic;">(%)</span>
                                    <address>
										<input type="text" name="txtMargin" id="txtMargin" class="form-control" autocomplete="off" value="" placeholder="Margin Distance (%)" readonly />
										<input type="hidden" name="txtVoyageTime" id="txtVoyageTime" class="form-control" autocomplete="off" value="<?php echo $obj->getFun19();?>" readonly />
										<input type="hidden" name="txtTTLVoyageDays" id="txtTTLVoyageDays" class="form-control" autocomplete="off" value="<?php echo $obj->getFun20();?>" readonly />
										<input type="hidden" name="txtTTLFoConsp" id="txtTTLFoConsp" class="form-control" readonly value="<?php echo $obj->getFun21();?>" readonly />
										<input type="hidden" name="txtTTLDoConsp" id="txtTTLDoConsp" class="form-control" readonly value="<?php echo $obj->getFun22();?>" readonly />										
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Distance
                                    <address>
										<input type="text" name="txtDistance" id="txtDistance" class="form-control"  value="" placeholder="distance" readonly/>
										<span id="loader1" style="display:none;" ><img src="../../img/ajax-loader.gif" /></span>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Passage Type
                                    <address>
										<select  name="selPType" class="select form-control" id="selPType" disabled>
											<?php $obj->getPassageType(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    Select Speed
                                    <address>
										<select  name="selSSpeed" class="select form-control" id="selSSpeed" disabled >
											<?php $obj->getSelectSpeedList(); ?>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
										<!--<button type="button" class="btn btn-primary btn-flat" onClick="addPortRotationDetails()" style="margin-left:100px;">Add</button>-->
										<input type="hidden" name="txtEstDays" id="txtEstDays" value="" />
                                    </address>
                                </div><!-- /.col -->
                            </div>
							
							<div class="row invoice-info" style="background:#F1F1F1;display:none;margin:0%;" id="ActRow">
                                <div class="col-sm-4 invoice-col">
									From Date
                                    <address>
										<input type="text" class="form-control" name="txtFromDate" id="txtFromDate" placeholder="dd-mm-yyyy HH:MM" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									To Date
                                    <address>
										<input type="text" class="form-control" name="txtToDate" id="txtToDate" placeholder="dd-mm-yyyy HH:MM" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Days
                                    <address>
										<input type="text" class="form-control" name="txtDDays" id="txtDDays" readonly placeholder="0.00" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Margin<span style="font-size:10px; font-style:italic;">(Hours)</span>
                                    <address>
										<input type="text" class="form-control" name="txtMarginHrs" id="txtMarginHrs" autocomplete="off" onKeyUp="getMarginDays();" placeholder="0.00" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Margin Distance <span style="font-size:10px; font-style:italic;">(%)</span>
                                    <address>
										<input type="text" class="form-control" name="txtMarginDays" id="txtMarginDays" readonly placeholder="0.00"readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Actual<span style="font-size:10px; font-style:italic;">(days)</span>
                                    <address>
										<input type="text" class="form-control" name="txtActualDays" id="txtActualDays" readonly placeholder="0.00"readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										<button class="btn btn-primary btn-flat" type="button" onClick="removeActualSeaPassageShow();" >CANCEL</button>&nbsp;&nbsp;
										<button class="btn btn-primary btn-flat" type="button" onClick="addActualSeaPassageShow();">ADD</button>
										<input type="hidden" name="txtPUID" id="txtPUID" class="form-control" value="" />
                                    </address>
                                </div><!-- /.col -->
								
							</div>
							
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<?php 
											$sql = "select * from fca_tci_sea_passage where FCAID='".$obj->getFun1()."'";
											$res = mysql_query($sql);
											$rec = mysql_num_rows($res);
											$i = 1;
											?>
											<tr>
												<th>#</th>
												<th></th>
												<th>From Port</th>
												<th>To Port</th>
												<th>Passage Type</th>
												<th>Distance</th>
												<th>Speed Adj.</th>
												<th>Margin Distance <span style="font-size:10px; font-style:italic;">(%)</span>
													<input type="hidden" name="p_rotationID" id="p_rotationID" class="input" size="5" value="<?php echo $rec;?>" /></th>
												<th>Estimated <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<th>Actual <span style="font-size:10px; font-style:italic;">(days)</span></th>
											</tr>
										</thead>
										<tbody id="tblPortRotation">
											<?php if($rec == 0){?>
											<tr id="PRrow_Empty">
												<td valign="top" align="center" colspan="10" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
											<?php }else{
												$i=1;
												while($rows = mysql_fetch_assoc($res))
												{
												?>
												<tr id="pr_Row_<?php echo $i;?>">
												<td>
												<?php if($i == $rec){?>
												<a href="#pr<?php echo $i;?>" id="spcancel_<?php echo $i;?>"><i class="fa fa-times" style="color:red;"></i></a>
												<?php }else{?>
												<a href="#pr<?php echo $i;?>" style="display:none;" ><i class="fa fa-times" style="color:red;"></i></a>
												<?php }?>
												</td>
												<td>
												<a href="#Edit<?php echo $i;?>" ><i class="fa fa-edit" style="color:#1b77a6;"></i></a>
												</td>
												<td><?php echo $obj->getPortNameBasedOnID($rows['FROM_PORTID']);?><input type="hidden" name="txtFPort_<?php echo $i;?>" id="txtFPort_<?php echo $i;?>" value="<?php echo $rows['FROM_PORTID'];?>"/></td>
												<td><?php echo $obj->getPortNameBasedOnID($rows['TO_PORTID']);?><input type="hidden" name="txtTPort_<?php echo $i;?>" id="txtTPort_<?php echo $i;?>" value="<?php echo $rows['TO_PORTID'];?>"/></td>
												<td><?php echo $obj->getPassageTypeNameBasedOnID($rows['PASSAGETYPEID'])." (".$obj->getPassageSpeedBasedOnID($rows['SPEEDID'])." )";?><input type="hidden" name="txtPType_<?php echo $i;?>" id="txtPType_<?php echo $i;?>" value="<?php echo $rows['PASSAGETYPEID'];?>"/><input type="hidden" name="txtSSpeed_<?php echo $i;?>" id="txtSSpeed_<?php echo $i;?>" value="<?php echo $rows['SPEEDID'];?>"/></td>
												<td><?php echo $rows['DISTANCE']." (".$obj->getDistanceTypeBasedOnID($rows['DISTANCE_TYPEID'])." )";?><input type="hidden" name="txtDistance_<?php echo $i;?>" id="txtDistance_<?php echo $i;?>" value="<?php echo $rows['DISTANCE'];?>"/><input type="hidden" name="txtDType_<?php echo $i;?>" id="txtDType_<?php echo $i;?>" value="<?php echo $rows['DISTANCE_TYPEID'];?>"/></td>
												<td><?php echo $rows['WEATHER'];?><input type="hidden" name="txtWeather_<?php echo $i;?>" id="txtWeather_<?php echo $i;?>" value="<?php echo $rows['WEATHER'];?>"/></td>
												<td><?php echo $rows['MARGIN'];?><input type="hidden" name="txtMargin_<?php echo $i;?>" id="txtMargin_<?php echo $i;?>" value="<?php echo $rows['MARGIN'];?>"/></td>
												<td><?php echo $obj->getEstimatedDays($mappingid,$rows['MARGIN'],$rows['DISTANCE'],$rows['SPEEDID'],$rows['PASSAGETYPEID'],$rows['WEATHER']);?><input type="hidden" name="txtEDays_<?php echo $i;?>" id="txtEDays_<?php echo $i;?>" value="<?php echo $obj->getEstimatedDays($mappingid,$rows['MARGIN'],$rows['DISTANCE'],$rows['SPEEDID'],$rows['PASSAGETYPEID'],$rows['WEATHER']);?>"/></td>
												<td>
												<span id="adaysLabel_<?php echo $i;?>"><?php if($actual_sp[$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_sp[$i]['ACTUAL_DAYS'];$ttl_actual += $actual_sp[$i]['ACTUAL_DAYS'];}?></span>
												<input type="hidden" name="txtFDate_<?php echo $i;?>" id="txtFDate_<?php echo $i;?>" value="<?php echo $actual_sp[$i]['FROM_DATE']?>"/>
												<input type="hidden" name="txtTDate_<?php echo $i;?>" id="txtTDate_<?php echo $i;?>" value="<?php echo $actual_sp[$i]['TO_DATE']?>"/>
												<input type="hidden" name="txtFTDays_<?php echo $i;?>" id="txtFTDays_<?php echo $i;?>" value="<?php if($actual_sp[$i]['DAYS'] == ""){echo "0";}else{echo $actual_sp[$i]['DAYS'];}?>"/>
												<input type="hidden" name="txtMarginHrs_<?php echo $i;?>" id="txtMarginHrs_<?php echo $i;?>" value="<?php if($actual_sp[$i]['MARGIN_HRS'] == ""){echo "0";}else{echo $actual_sp[$i]['MARGIN_HRS'];}?>"/>
												<input type="hidden" name="txtMarginDays_<?php echo $i;?>" id="txtMarginDays_<?php echo $i;?>" value="<?php if($actual_sp[$i]['MARGIN_DAYS'] == ""){echo "0";}else{echo $actual_sp[$i]['MARGIN_DAYS'];}?>"/>
												<input type="hidden" name="txtADays_<?php echo $i;?>" id="txtADays_<?php echo $i;?>" value="<?php if($actual_sp[$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_sp[$i]['ACTUAL_DAYS'];}?>"/>
												</td>
												</tr>
												<?php $i++;} }?>
											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
									<input type="hidden" name="txtRow_id" id="txtRow_id" autocomplete="off" value="0" />
									Load Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							<div class="row invoice-info" id="LPEstRow">
                                <div class="col-sm-4 invoice-col">
                                  Load Port
                                    <address>
										<select  name="selLoadPort" class="select form-control" id="selLoadPort" disabled>
											<option value="">--Select from list--</option>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Cargo
                                    <address>
										<select  name="selLPCName" class="select form-control" id="selLPCName" onChange="getLOadPortQty();" disabled>
											<?php 
												$obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"SHIPPING_STAGE"));
											?>
										</select>
                                        <script>$("#selLPCName").val('<?php echo $obj->getMappingData($mappingid,"CARGO_IDS");?>');</script>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
										<input type="text" name="txtPCosts" id="txtPCosts" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Qty MT
                                    <address>
										<input type="text" name="txtQMT" id="txtQMT" class="form-control" onKeyUp="getLoadPortCalculation();" autocomplete="off" value="" placeholder="Qty MT" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                                    <address>
										<input type="text" name="txtRate" id="txtRate" class="form-control" onKeyUp="getLoadPortCalculation();" autocomplete="off" value="" placeholder="Rate (MT/Day)" readonly/>
                                    </address>
                                </div><!-- /.col -->
                                
								<div class="col-sm-4 invoice-col">
									LP/Terms
                                    <address>
										<select  name="selLPTerms" class="select form-control" id="selLPTerms" onChange="getLPRemoveDaysAttr(),getLoadPortCalculation();" disabled>
											<?php 
												$obj->getLPTermsList(0);
											?>
										</select>
                                    </address>
                                </div><!-- /.col -->
                                
								<div class="col-sm-4 invoice-col">
									Work Days
                                    <address>
										<input type="text" name="txtWDays" id="txtWDays" class="form-control" readonly value="" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Idle Days
                                    <address>
										<input type="text" name="txtIDays" id="txtIDays" class="form-control" autocomplete="off" value="" placeholder="Idle Days" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
										<!--<button type="button" class="btn btn-primary btn-flat" onClick="addLoadPortDetails()" style="margin-left:100px;">Add</button>-->
                                    </address>
                                </div><!-- /.col -->
							</div>
							
							<div class="row invoice-info" style="background:#F1F1F1;margin:0%;display:none;" id="LPActRow">
                                <div class="col-sm-4 invoice-col">
                                  Idle From Date
                                    <address>
										<input type="text" class="form-control" name="txtIdleFromDate" id="txtIdleFromDate" placeholder="dd-mm-yyyy HH:MM" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Idle To Date
                                    <address>
										<input type="text" class="form-control" name="txtIdleToDate" id="txtIdleToDate" placeholder="dd-mm-yyyy HH:MM" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Idle Days
                                    <address>
										<input type="text" class="form-control" name="txtIdleDays" id="txtIdleDays" placeholder="0.00" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								
								<div class="col-sm-4 invoice-col">
                                  Working From Date
                                    <address>
										<input type="text" class="form-control" name="txtWrkingFromDate" id="txtWrkingFromDate" placeholder="dd-mm-yyyy HH:MM" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Working To Date
                                    <address>
										<input type="text" class="form-control" name="txtwrkingToDate" id="txtwrkingToDate" placeholder="dd-mm-yyyy HH:MM" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Working Days
                                    <address>
										<input type="text" class="form-control" name="txtwrkingDays" id="txtwrkingDays" placeholder="0.00" readonly readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Stoppage Reason
                                    <address>
										<input type="text" class="form-control" name="txtLPOHReason" id="txtLPOHReason" placeholder="" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Stoppage <span style="font-size:10px; font-style:italic;">(Hours)</span>
                                    <address>
										<input type="text" class="form-control" name="txtLPOHHrs" id="txtLPOHHrs" onKeyUp="getLPOHDays();" placeholder="0.00" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Stoppage <span style="font-size:10px; font-style:italic;">(days)</span>
                                    <address>
										<input type="text" class="form-control" name="txtLPOHDays" id="txtLPOHDays" placeholder="0.00" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  &nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Actual <span style="font-size:10px; font-style:italic;">(days)</span>
                                    <address>
										<input type="text" class="form-control" name="txtLPActualDays" id="txtLPActualDays" placeholder="0.00" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->								
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										<button class="btn btn-primary btn-flat" type="button" onClick="removeActualLPShow();">CANCEL</button>&nbsp;&nbsp;
										<button class="btn btn-primary btn-flat" type="button" onClick="addActualLPShow();">ADD</button>
										<input type="hidden" name="txtLPID" id="txtLPID" class="form-control" value=""/>
                                    </address>
                                </div><!-- /.col -->
							</div>
							
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<?php
												$sql1 = "select * from fca_tci_load_port where FCAID='".$obj->getFun1()."'";
												$res1 = mysql_query($sql1,$connect);
												$rec1 = mysql_num_rows($res1);
											?>
											<tr>
												<th>#</th>
												<th></th>
												<th>Load Port</th>
												<th>Cargo Name</th>
												<th>Qty MT</th>
												<th>Rate<span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
												<th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
												<th>Idle Days</th>
												<th>Work Days</th>
												<!--<th>BL Date</th>-->
												<th>Estimated <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<th>Actual <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<input type="hidden" name="load_portID" id="load_portID" class="input" size="5" value="<?php echo $rec1;?>"/>
											</tr>
										</thead>
										<tbody id="tblLoadPort">
											<?php if($rec1 == 0){ ?>
											<tr id="LProw_Empty">
												<td valign="top" align="center" colspan="11" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
											<?php 
											}else{
												$i=1;
												while($rows1 = mysql_fetch_assoc($res1)){?>
												<tr id="lp_Row_<?php echo $i;?>">
													<td><a href="#lp<?php echo $i;?>"><i class="fa fa-times" style="color:red;"></i></a></td>
													<td>
													<a href="#Edit<?php echo $i;?>"><i class="fa fa-edit" style="color:#1b77a6;"></i></a>
													</td>
													<td><?php echo $obj->getPortNameBasedOnID($rows1['LOADPORTID']);?><input type="hidden" name="txtLoadPort_<?php echo $i;?>" id="txtLoadPort_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows1['LOADPORTID'];?>"/></td>
													<td><?php echo $obj->getCargoContarctForMapping($rows1['PURCHASE_ALLOCATIONID'],$obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"SHIPPING_STAGE"));?><input type="hidden" name="txtLPCID_<?php echo $i;?>" id="txtLPCID_<?php echo $i;?>" value="<?php echo $rows1['PURCHASE_ALLOCATIONID'];?>"/></td>
													<td><?php echo $rows1['QTY_MT'];?><input type="hidden" name="txtLpQMT_<?php echo $i;?>" id="txtLpQMT_<?php echo $i;?>" value="<?php echo $rows1['QTY_MT'];?>"/></td>
													<td align="left" class="input-text" ><?php echo $rows1['RATE'];?><input type="hidden" name="txtLPRate_<?php echo $i;?>" id="txtLPRate_<?php echo $i;?>" value="<?php echo $rows1['RATE'];?>"/></td>
													
													<?php if($obj->getFDA($mappingid,'LP',$rows1['LOADPORTID']) > 0){ ?>
													<td style="background-color:#f19560;" >
													<span style="color:#fff; font-weight:bolder;"><?php echo $obj->getFDA($mappingid,'LP',$rows1['LOADPORTID']);?></span>
														<input type="hidden" name="txtPCosts_<?php echo $i;?>" id="txtPCosts_<?php echo $i;?>" value="<?php echo $obj->getFDA($mappingid,'LP',$rows1['LOADPORTID']);?>"/>
														</td>
													<?php }else{ ?>
													<td>
													<?php echo $rows1['PORT_COST'];?>
														<input type="hidden" name="txtPCosts_<?php echo $i;?>" id="txtPCosts_<?php echo $i;?>" value="<?php echo $rows1['PORT_COST'];?>"/>
														</td>
													<?php }?>
													
													<td><?php echo $rows1['IDLE_DAYS'];?><input type="hidden" name="txtLPIDays_<?php echo $i;?>" id="txtLPIDays_<?php echo $i;?>" value="<?php echo $rows1['IDLE_DAYS'];?>"/></td>
													<td><?php echo $rows1['WORK_DAYS'];?><input type="hidden" name="txtLPWDays_<?php echo $i;?>" id="txtLPWDays_<?php echo $i;?>" value="<?php echo $rows1['WORK_DAYS'];?>"/><input type="hidden" name="txtLPCType_<?php echo $i;?>" id="txtLPCType_<?php echo $i;?>" value="<?php echo $rows1['CRANES'];?>"/></td>
													
													<!--<td><?php //echo $obj->getBLDateBasedOnMappingidAndPort($mappingid,'LP',$rows1['LOADPORTID']);?></td>-->
													<td><?php echo $rows1['IDLE_DAYS'] + $rows1['WORK_DAYS'];?><input type="hidden" name="txtLPEDays_<?php echo $i;?>" id="txtLPEDays_<?php echo $i;?>" value="<?php echo $rows1['IDLE_DAYS'] + $rows1['WORK_DAYS'];?>"/></td>
													<td><span id="lpadaysLabel_<?php echo $i;?>"><?php if($actual_lp[$rows1['LOADPORTID']][$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_lp[$rows1['LOADPORTID']][$i]['ACTUAL_DAYS'];}?></span>
													<input type="hidden" name="txtLPIFromDate_<?php echo $i;?>" id="txtLPIFromDate_<?php echo $i;?>" value="<?php echo $actual_lp[$rows1['LOADPORTID']][$i]['IDLE_FROM_DATE']?>"/>
													<input type="hidden" name="txtLPIToDate_<?php echo $i;?>" id="txtLPIToDate_<?php echo $i;?>" value="<?php echo $actual_lp[$rows1['LOADPORTID']][$i]['IDLE_TO_DATE']?>"/>
													<input type="hidden" name="txtLPTIDays_<?php echo $i;?>" id="txtLPTIDays_<?php echo $i;?>" value="<?php if($actual_lp[$rows1['LOADPORTID']][$i]['IDLE_DAYS'] == ""){echo "0";}else{echo $actual_lp[$rows1['LOADPORTID']][$i]['IDLE_DAYS'];}?>"/>
													<input type="hidden" name="txtLPWFromDate_<?php echo $i;?>" id="txtLPWFromDate_<?php echo $i;?>" value="<?php echo $actual_lp[$rows1['LOADPORTID']][$i]['WRKING_FROM_DATE']?>"/>
													<input type="hidden" name="txtLPWToDate_<?php echo $i;?>" id="txtLPWToDate_<?php echo $i;?>" value="<?php echo $actual_lp[$rows1['LOADPORTID']][$i]['WRKING_TO_DATE']?>"/>
													<input type="hidden" name="txtLPTWDays_<?php echo $i;?>" id="txtLPTWDays_<?php echo $i;?>" value="<?php if($actual_lp[$rows1['LOADPORTID']][$i]['WRKING_DAYS'] == ""){echo "0";}else{echo $actual_lp[$rows1['LOADPORTID']][$i]['WRKING_DAYS'];}?>"/>
													<input type="hidden" name="txtLPOHReason_<?php echo $i;?>" id="txtLPOHReason_<?php echo $i;?>" value="<?php echo $actual_lp[$rows1['LOADPORTID']][$i]['REASON_OFF_HIRE']?>"/>
													<input type="hidden" name="txtLPOHHrs_<?php echo $i;?>" id="txtLPOHHrs_<?php echo $i;?>" value="<?php if($actual_lp[$rows1['LOADPORTID']][$i]['OFF_HIRE_HRS'] == ""){echo "0";}else{echo $actual_lp[$rows1['LOADPORTID']][$i]['OFF_HIRE_HRS'];}?>"/>
													<input type="hidden" name="txtLPOHDays_<?php echo $i;?>" id="txtLPOHDays_<?php echo $i;?>" value="<?php if($actual_lp[$rows1['LOADPORTID']][$i]['OFF_HIRE_DAYS'] == ""){echo "0";}else{echo $actual_lp[$rows1['LOADPORTID']][$i]['OFF_HIRE_DAYS'];}?>"/>
													<input type="hidden" name="txtLPADays_<?php echo $i;?>" id="txtLPADays_<?php echo $i;?>" value="<?php if($actual_lp[$rows1['LOADPORTID']][$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_lp[$rows1['LOADPORTID']][$i]['ACTUAL_DAYS'];}?>"/>
													<input type="hidden" name="txtLPCrType_<?php echo $i;?>" id="txtLPCrType_<?php echo $i;?>" value="<?php echo $actual_lp[$rows1['LOADPORTID']][$i]['PUMPS'];?>"/><input type="hidden" name="txtLPTermsVal_<?php echo $i;?>" id="txtLPTermsVal_<?php echo $i;?>" class="form-control" value="<?php echo $rows1['LPTERMS'];?>"/>
													</td>
												</tr>
											<?php $i++;} }?>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Discharge Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="row invoice-info" id="DPEstRow">
                                <div class="col-sm-4 invoice-col">
                                  Discharge Port
                                    <address>
										<select  name="selDisPort" class="select form-control" id="selDisPort" disabled>
											<option value="">--Select from list--</option>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Cargo
                                    <address>
										<select  name="selDPCName" class="select form-control" id="selDPCName" onChange="getDisPortQty();" disabled>
											<?php 
												$obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"SHIPPING_STAGE"));
											?>
										</select>
                                        <script>$("#selDPCName").val('<?php echo $obj->getMappingData($mappingid,"CARGO_IDS");?>');</script>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
										<input type="text" name="txtDPCosts" id="txtDPCosts" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Qty MT
                                    <address>
										<input type="text"  name="txtDQMT" id="txtDQMT" class="form-control" onKeyUp="getDisPortCalculation();" autocomplete="off" value="" placeholder="Qty MT" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                                    <address>
										<input type="text" name="txtDRate" id="txtDRate" class="form-control" onKeyUp="getDisPortCalculation();" autocomplete="off" value="" placeholder="Rate (MT/Day)" readonly />
                                    </address>
                                </div><!-- /.col -->
								
                                 <div class="col-sm-4 invoice-col">
									DP/Terms
                                    <address>
										<select  name="selDPTerms" class="select form-control" id="selDPTerms" onChange="getDPRemoveDaysAttr(),getDisPortCalculation();" disabled>
											<?php 
												$obj->getLPTermsList(0);
											?>
										</select>
                                    </address>
                                </div><!-- /.col -->
                                
								<div class="col-sm-4 invoice-col">
									Work Days
                                    <address>
										<input type="text"name="txtDWDays" id="txtDWDays" class="form-control" readonly value="" />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Idle Days
                                    <address>
										<input type="text" name="txtDIDays" id="txtDIDays" class="form-control" autocomplete="off" value="" placeholder="Idle Days" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
										<!--<button type="button" class="btn btn-primary btn-flat" onClick="addDisPortDetails()" style="margin-left:100px;">Add</button>-->
                                    </address>
                                </div><!-- /.col -->
							</div>
							
							
							<div class="row invoice-info" id="DPActRow" style="background:#F1F1F1;display:none;margin:0%;">
                                <div class="col-sm-4 invoice-col">
									Idle From Date
                                    <address>
										<input type="text" class="form-control" name="txtDPIdleFromDate" id="txtDPIdleFromDate" placeholder="dd-mm-yyyy HH:MM" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Idle To Date
                                    <address>
										<input type="text" class="form-control" name="txtDPIdleToDate" id="txtDPIdleToDate" placeholder="dd-mm-yyyy HH:MM" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Idle Days
                                    <address>
										<input type="text" class="form-control" name="txtDPIdleDays" id="txtDPIdleDays" readonly placeholder="0.00" readonly />	
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Working From Date
                                    <address>
										<input type="text" class="form-control" name="txtDPWrkingFromDate" id="txtDPWrkingFromDate" placeholder="dd-mm-yyyy HH:MM" readonly />	
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Working To Date
                                    <address>
										<input type="text" class="form-control" name="txtDPwrkingToDate" id="txtDPwrkingToDate" placeholder="dd-mm-yyyy HH:MM" readonly />	
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Working Days
                                    <address>
										<input type="text" class="form-control" name="txtDPwrkingDays" id="txtDPwrkingDays" readonly placeholder="0.00" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Stoppage Reason
                                    <address>
										<input type="text" class="form-control" name="txtDPOHReason" id="txtDPOHReason"  autocomplete="off" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Stoppage <span style="font-size:10px; font-style:italic;">(Hours)</span>
                                    <address>
										<input type="text" class="form-control" name="txtDPOHHrs" id="txtDPOHHrs" autocomplete="off" onKeyUp="getDPOHDays();" placeholder="0.00" readonly/>	
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Stoppage <span style="font-size:10px; font-style:italic;">(days)</span>
                                    <address>
										<input type="text" class="form-control" name="txtDPOHDays" id="txtDPOHDays" readonly placeholder="0.00" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Actual <span style="font-size:10px; font-style:italic;">(days)</span>
                                    <address>
										<input type="text" class="form-control" name="txtDPActualDays" id="txtDPActualDays" size="10" readonly  value="0.00" readonly/>
                                    </address>
                                </div><!-- /.col -->

								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->								
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										<button class="btn btn-primary btn-flat" type="button" onClick="removeActualDPShow();">CANCEL</button>&nbsp;&nbsp;
										<button class="btn btn-primary btn-flat" type="button" onClick="addActualDPShow();">ADD</button>
										<input type="hidden" name="txtDPID" id="txtDPID" value="" />
                                    </address>
                                </div><!-- /.col -->
							</div>
							
							
							
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<?php 
												$sql2 = "select * from fca_tci_disch_port where FCAID='".$obj->getFun1()."'";
												$res2 = mysql_query($sql2);
												$rec2 = mysql_num_rows($res2);
											?>
											<tr>
												<th>#</th>
												<th></th>
												<th>Discharge Port</th>
												<th>Cargo Name</th>
												<th>Qty MT</th>
												<th>Rate<span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
												<th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
												<th>Idle Days</th>
												<th>Work Days
												  <input type="hidden" name="dis_portID" id="dis_portID" class="input" size="5" value="<?php echo $rec2;?>" /></th>
												<!--<th>BL Date</th>-->
												<th>Estimated <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<th>Actual <span style="font-size:10px; font-style:italic;">(days)</span></th>
												
											</tr>
										</thead>
										<tbody id="tblDisPort">
											<?php if($rec2 == 0){?>
											<tr id="DProw_Empty">
												<td valign="top" align="center" colspan="11" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
											<?php 
												}else{
												$i=1;
												while($rows2 = mysql_fetch_assoc($res2))
												{
											?>
											<tr id="dp_Row_<?php echo $i;?>">
												<td><a href="#dp<?php echo $i;?>" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td><a href="#Edit<?php echo $i;?>" ><i class="fa fa-edit" style="color:#1b77a6;"></i></a></td>
												<td><?php echo $obj->getPortNameBasedOnID($rows2['DIS_PORT_ID']);?><input type="hidden" name="txtDisPort_<?php echo $i;?>" id="txtDisPort_<?php echo $i;?>" value="<?php echo $rows2['DIS_PORT_ID'];?>"/></td>
												<td><?php echo $obj->getCargoContarctForMapping($rows2['PURCHASE_ALLOCID'],$obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"SHIPPING_STAGE"));?><input type="hidden" name="txtDPCID_<?php echo $i;?>" id="txtDPCID_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows2['PURCHASE_ALLOCID'];?>"/></td>
												<td><?php echo $rows2['QTY_MT'];?><input type="hidden" name="txtDpQMT_<?php echo $i;?>" id="txtDpQMT_<?php echo $i;?>" value="<?php echo $rows2['QTY_MT'];?>"/></td>
												<td><?php echo $rows2['RATE'];?><input type="hidden" name="txtDPRate_<?php echo $i;?>" id="txtDPRate_<?php echo $i;?>"  value="<?php echo $rows2['RATE'];?>"/></td>
		
												<?php if($obj->getFDA($mappingid,'DP',$rows2['DIS_PORT_ID']) > 0){ ?>
												<td style="background-color:#f19560;" >
													<span style="color:#fff; font-weight:bolder;"><?php echo $obj->getFDA($mappingid,'DP',$rows2['DIS_PORT_ID']);?></span>
													<input type="hidden" name="txtDPCosts_<?php echo $i;?>" id="txtDPCosts_<?php echo $i;?>" value="<?php echo $obj->getFDA($mappingid,'DP',$rows2['DIS_PORT_ID']);?>"/>
												</td>
												<?php }else{ ?>
												<td>
												<?php echo $rows2['PORT_COST'];?>
													<input type="hidden" name="txtDPCosts_<?php echo $i;?>" id="txtDPCosts_<?php echo $i;?>" value="<?php echo $rows2['PORT_COST'];?>"/>
													</td>
												<?php }?>
		
												<td><?php echo $rows2['IDLE_DAYS'];?><input type="hidden" name="txtDPIDays_<?php echo $i;?>" id="txtDPIDays_<?php echo $i;?>" value="<?php echo $rows2['IDLE_DAYS'];?>"/></td>
												
												<td><?php echo $rows2['WORK_DAYS'];?><input type="hidden" name="txtDPWDays_<?php echo $i;?>" id="txtDPWDays_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows2['WORK_DAYS'];?>"/><input type="hidden" name="txtDPCType_<?php echo $i;?>" id="txtDPCType_<?php echo $i;?>" class="input" size="10" value="<?php echo abs($rows2['PUMPS']);?>"/></td>
		
												<!--<td><?php //echo $obj->getBLDateBasedOnMappingidAndPort($mappingid,'DP',$rows2['DIS_PORT_ID']);?></td>-->
												
												<td><?php echo $rows2['IDLE_DAYS'] + $rows2['WORK_DAYS'];?><input type="hidden" name="txtDPEDays_<?php echo $i;?>" id="txtDPEDays_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows2['IDLE_DAYS'] + $rows2['WORK_DAYS'];?>"/></td>
												
												<td><span id="dpadaysLabel_<?php echo $i;?>"><?php if($actual_dp[$rows2['DIS_PORT_ID']][$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['ACTUAL_DAYS'];}?></span>
													<input type="hidden" name="txtDPIFromDate_<?php echo $i;?>" id="txtDPIFromDate_<?php echo $i;?>" value="<?php echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['IDLE_FROM_DATE']?>"/>
													<input type="hidden" name="txtDPIToDate_<?php echo $i;?>" id="txtDPIToDate_<?php echo $i;?>" value="<?php echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['IDLE_TO_DATE']?>"/>
													<input type="hidden" name="txtDPTIDays_<?php echo $i;?>" id="txtDPTIDays_<?php echo $i;?>" value="<?php if($actual_dp[$rows2['DIS_PORT_ID']][$i]['IDLE_DAYS'] == ""){echo "0";}else{echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['IDLE_DAYS'];}?>"/>
													<input type="hidden" name="txtDPWFromDate_<?php echo $i;?>" id="txtDPWFromDate_<?php echo $i;?>" value="<?php echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['WRKING_FROM_DATE']?>"/>
													<input type="hidden" name="txtDPWToDate_<?php echo $i;?>" id="txtDPWToDate_<?php echo $i;?>" value="<?php echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['WRKING_TO_DATE']?>"/>
													<input type="hidden" name="txtDPTWDays_<?php echo $i;?>" id="txtDPTWDays_<?php echo $i;?>" value="<?php if($actual_dp[$rows2['DIS_PORT_ID']][$i]['WRKING_DAYS'] == ""){echo "0";}else{echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['WRKING_DAYS'];}?>"/>
													<input type="hidden" name="txtDPOHReason_<?php echo $i;?>" id="txtDPOHReason_<?php echo $i;?>" value="<?php echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['REASON_OFF_HIRE']?>"/>
													<input type="hidden" name="txtDPOHHrs_<?php echo $i;?>" id="txtDPOHHrs_<?php echo $i;?>" value="<?php if($actual_dp[$rows2['DIS_PORT_ID']][$i]['OFF_HIRE_HRS'] == ""){echo "0";}else{echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['OFF_HIRE_HRS'];}?>"/>
													<input type="hidden" name="txtDPOHDays_<?php echo $i;?>" id="txtDPOHDays_<?php echo $i;?>" value="<?php if($actual_dp[$rows2['DIS_PORT_ID']][$i]['OFF_HIRE_DAYS'] == ""){echo "0";}else{echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['OFF_HIRE_DAYS'];}?>"/>
													<input type="hidden" name="txtDPADays_<?php echo $i;?>" id="txtDPADays_<?php echo $i;?>" value="<?php if($actual_dp[$rows2['DIS_PORT_ID']][$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['ACTUAL_DAYS'];}?>"/>
													<input type="hidden" name="txtDPCrType_<?php echo $i;?>" id="txtDPCrType_<?php echo $i;?>" value="<?php echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['PUMPS'];?>"/><input type="hidden" name="txtDPTermsVal_<?php echo $i;?>" id="txtDPTermsVal_<?php echo $i;?>" class="form-control" value="<?php echo $rows1['DPTERMS'];?>"/>
												</td>	
											</tr>
										<?php $i++;} }?>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Transit Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="row invoice-info" id="TPEstRow">
                                <div class="col-sm-4 invoice-col">
                                  Transit Port
                                    <address>
										<select  name="selTLoadPort" id="selTLoadPort" class="select form-control" disabled >
											<option value="">--Select from list--</option>
										</select>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
										<input type="text" name="txtTLPCosts" id="txtTLPCosts" class="form-control" autocomplete="off" value="" placeholder="Port Costs (USD)" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Idle Days
                                    <address>
										<input type="text" name="txtTLIDays" id="txtTLIDays" class="form-control" autocomplete="off" value="" placeholder="Idle Days" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
										<!--<button type="button" class="btn btn-primary btn-flat" onClick="addTransitPortDetails()" style="margin-left:100px;">Add</button>-->
                                    </address>
                                </div><!-- /.col -->
								
							</div>
							
							<div class="row invoice-info" style="display:none; background:#F1F1F1;margin:0%;" id="TPActRow">
                                <div class="col-sm-4 invoice-col">
									Idle From Date
                                    <address>
										<input type="text" class="form-control" name="txtTPIdleFromDate" id="txtTPIdleFromDate" placeholder="dd-mm-yyyy HH:MM" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Idle To Date
                                    <address>
										<input type="text" class="form-control" name="txtTPIdleToDate" id="txtTPIdleToDate" placeholder="dd-mm-yyyy HH:MM" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Idle Days
                                    <address>
										<input type="text" class="form-control" name="txtTPIdleDays" id="txtTPIdleDays" readonly placeholder="0.00"readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Stoppage Reason
                                    <address>
										<input type="text" class="form-control" name="txtTPOHReason" id="txtTPOHReason"  autocomplete="off" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Stoppage <span style="font-size:10px; font-style:italic;">(Hours)</span>
                                    <address>
										<input type="text" class="form-control" name="txtTPOHHrs" id="txtTPOHHrs" autocomplete="off" onKeyUp="getTPOHDays();" placeholder="0.00" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Stoppage <span style="font-size:10px; font-style:italic;">(days)</span>
                                    <address>
										<input type="text" class="form-control" name="txtTPOHDays" id="txtTPOHDays" readonly placeholder="0.00" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  &nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  &nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									Actual <span style="font-size:10px; font-style:italic;">(days)</span>
                                    <address>
										<input type="text" class="form-control" name="txtTPActualDays" id="txtTPActualDays" readonly  value="0.00"/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  &nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  &nbsp;
                                    <address>
										&nbsp;
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
									&nbsp;
                                    <address>
										<button class="btn btn-primary btn-flat" type="button" onClick="removeActualTPShow();">CANCEL</button>&nbsp;&nbsp;
										<button class="btn btn-primary btn-flat" type="button" onClick="addActualTPShow();">ADD</button>
										<input type="hidden" name="txtTPID" id="txtTPID" value="" />
                                    </address>
                                </div><!-- /.col -->
								
							</div>
							
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<?php 
												$sql3 = "select * from fca_tci_transit_port where FCAID='".$obj->getFun1()."'";
												$res3 = mysql_query($sql3);
												$rec3 = mysql_num_rows($res3);
											?>
											<tr>
												<th>#</th>
												<th></th>
												<th>Transit Port</th>
												<th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
												<th>Idle Days<input type="hidden" name="transit_portID" id="transit_portID" value="<?php echo $rec3;?>" /></th>
												<th>Estimated <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<th>Actual <span style="font-size:10px; font-style:italic;">(days)</span></th>
												
												
											</tr>
										</thead>
										
										<tbody id="tblTransitPort">
											<?php if($rec3 == 0){?>
											<tr id="TProw_Empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
											<?php }else{
												$i=1;
												while($rows3 = mysql_fetch_assoc($res3))
												{
											?>
											<tr id="tp_Row_<?php echo $i;?>">
												<td><a href="#dp<?php echo $i;?>" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>
												<a href="#Edit<?php echo $i;?>" ><i class="fa fa-edit" style="color:#1b77a6;"></i></a>
												</td>	
												<td><?php echo $obj->getPortNameBasedOnID($rows3['LOAD_PORTID']);?><input type="hidden" name="txtTLoadPort_<?php echo $i;?>" id="txtTLoadPort_<?php echo $i;?>" value="<?php echo $rows3['LOAD_PORTID'];?>"/></td>
												<td><?php echo $rows3['PORT_COST'];?><input type="hidden" name="txtTPCosts_<?php echo $i;?>" id="txtTPCosts_<?php echo $i;?>" value="<?php echo $rows3['PORT_COST'];?>"/></td>
												<td><?php echo $rows3['IDLE_DAYS'];?><input type="hidden" name="txtTPIDays_<?php echo $i;?>" id="txtTPIDays_<?php echo $i;?>" value="<?php echo $rows3['IDLE_DAYS'];?>"/></td>
												<td><?php echo $rows3['IDLE_DAYS'];?><input type="hidden" name="txtTPEDays_<?php echo $i;?>" id="txtTPEDays_<?php echo $i;?>" value="<?php echo $rows3['IDLE_DAYS'];?>"/></td>
												<td><span id="tpadaysLabel_<?php echo $i;?>"><?php if($actual_tp[$rows3['LOAD_PORTID']][$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_tp[$rows3['LOAD_PORTID']][$i]['ACTUAL_DAYS'];}?></span>
												<input type="hidden" name="txtTPIFromDate_<?php echo $i;?>" id="txtTPIFromDate_<?php echo $i;?>" value="<?php echo $actual_tp[$rows3['LOAD_PORTID']][$i]['IDLE_FROM_DATE']?>"/>
												<input type="hidden" name="txtTPIToDate_<?php echo $i;?>" id="txtTPIToDate_<?php echo $i;?>" value="<?php echo $actual_tp[$rows3['LOAD_PORTID']][$i]['IDLE_TO_DATE']?>"/>
												<input type="hidden" name="txtTPTIDays_<?php echo $i;?>" id="txtTPTIDays_<?php echo $i;?>" value="<?php if($actual_tp[$rows3['LOAD_PORTID']][$i]['IDLE_DAYS'] == ""){echo "0";}else{echo $actual_tp[$rows3['LOAD_PORTID']][$i]['IDLE_DAYS'];}?>"/>
												<input type="hidden" name="txtTPOHReason_<?php echo $i;?>" id="txtTPOHReason_<?php echo $i;?>" value="<?php echo $actual_tp[$rows3['LOAD_PORTID']][$i]['REASON_OFF_HIRE']?>"/>
												<input type="hidden" name="txtTPOHHrs_<?php echo $i;?>" id="txtTPOHHrs_<?php echo $i;?>" value="<?php if($actual_tp[$rows3['LOAD_PORTID']][$i]['OFF_HIRE_HRS'] == ""){echo "0";}else{echo $actual_tp[$rows3['LOAD_PORTID']][$i]['OFF_HIRE_HRS'];}?>"/>
												<input type="hidden" name="txtTPOHDays_<?php echo $i;?>" id="txtTPOHDays_<?php echo $i;?>" value="<?php if($actual_tp[$rows3['LOAD_PORTID']][$i]['OFF_HIRE_DAYS'] == ""){echo "0";}else{echo $actual_tp[$rows3['LOAD_PORTID']][$i]['OFF_HIRE_DAYS'];}?>"/>
												<input type="hidden" name="txtTPADays_<?php echo $i;?>" id="txtTPADays_<?php echo $i;?>" value="<?php if($actual_tp[$rows3['LOAD_PORTID']][$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_tp[$rows3['LOAD_PORTID']][$i]['ACTUAL_DAYS'];}?>"/>
												</td>	
											</tr>
											<?php $i++;} }?>
										</tbody>
									</table>
								</div>
							</div>
							
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Totals
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Laden Dist
                                    <address>
										<input type="text" name="txtLDist" id="txtLDist" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"LADEN_DIST");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ballast Dist
                                    <address>
										<input type="text" name="txtBDist" id="txtBDist" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BALLAST_DIST");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Dist
                                    <address>
										<input type="text" name="txtTDist" id="txtTDist" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TOTAL_DIST");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Laden Days
                                    <address>
										<input type="text" name="txtLDays" id="txtLDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"LADEN_DAYS");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ballast Days
                                    <address>
										<input type="text" name="txtBDays" id="txtBDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BALLAST_DAYS");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Sea Days
                                    <address>
										<input type="text" name="txtTSDays" id="txtTSDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SEA_DAYS");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl Port Idle Days
                                    <address>
										<input type="text" name="txtTtPIDays" id="txtTtPIDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_IDLE_DAYS");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl Port Work Days
                                    <address>
										<input type="text" name="txtTtPWDays" id="txtTtPWDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_WORK_DAYS");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Days
                                    <address>
										<input type="text" name="txtTDays" id="txtTDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DAYS");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
							</div>
							<div class="row invoice-info">	
								<div class="col-sm-4 invoice-col">
                                  Ttl FO Consp MT
                                    <address>
										<input type="text" name="txtTFUMT" id="txtTFUMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_FO_CONSP_MT");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl DO Consp MT
                                    <address>
										<input type="text" name="txtTDUMT" id="txtTDUMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DO_CONSP_MT");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
							</div>
                            <div class="row invoice-info">	
								<div class="col-sm-4 invoice-col">
                                  Ttl FO Consp MT (Manual)
                                    <address>
										<input type="text" name="txtTFUMTManual" id="txtTFUMTManual" class="form-control" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_FO_CONSP_MT_MANUAL");?>"  onKeyUp="getBunkerCalculation();" readonly/>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl DO Consp MT (Manual)
                                    <address>
										<input type="text" name="txtTDUMTManual" id="txtTDUMTManual" class="form-control" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DO_CONSP_MT_MANUAL");?>"  onKeyUp="getBunkerCalculation();" readonly/>
                                    </address>
                                </div><!-- /.col -->
							</div>
							<div class="row invoice-info">	
								<div class="col-sm-4 invoice-col">
                                  Ttl FO Consp Off Hire
                                    <address>
										<input type="text" name="txtTFOCOffHire" id="txtTFOCOffHire" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_FO_CONSP_OFFHIRE");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl DO Consp Off Hire
                                    <address>
										<input type="text" name="txtTDOCOffHire" id="txtTDOCOffHire" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DO_CONSP_OFFHIRE");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
								
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Freight Adjustment</h3>
								</div>
								<div class="box-body no-padding"> 
                                   <table class="table table-striped" id="divQty3">
                                    <thead>
                                        <tr>
                                            <th width="24%">Customer</th>
                                            <th width="19%">Gross Freight <span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                            <th width="19%">Brokerage(%)</th>
                                            <th width="19%">Net Brokerage</th>
                                            <th width="19%">Net Freight <span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                        </tr>
                                    </thead>
                                    <tbody id="tblQtyFreight1">	
                                    <?php 
									$sql12 = "select * from fca_tci_multiple_vendor_qty_details where FCAID='".$obj->getFun1()."'";
									$res12 = mysql_query($sql12);
									$rec12 = mysql_num_rows($res12);
									?>
                                      <?php if($rec12==0)
										{?>
											<tr id="tbrQtyVRow_empty1">
												<td valign="top" align="center" colspan="5" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
                                            <?php }
											else
											{$i = 0;
											while($rows2 = mysql_fetch_assoc($res12))
											{$i = $i + 1;
											?>
                                     
                                            <tr id="tbrQtyVRow1_<?php echo $i;?>">
                                                <td><?php echo $obj->getVendorListNewBasedOnID($rows2['QTY_VENDORID']);?></td>
                                                <td><input type="text" name="txtFreightQty1_<?php echo $i;?>" id="txtFreightQty1_<?php echo $i;?>" autocomplete="off" class="form-control" readonly value="<?php echo $rows2['GROSS_FREIGHT'];?>" /></td>
                                                <td><input type="text" name="txtQtyBrokeragePer_<?php echo $i;?>" id="txtQtyBrokeragePer_<?php echo $i;?>" onKeyUp="getFinalCalculation();" class="form-control" value="<?php echo $rows2['BROKERAGE'];?>" /></td>
                                                <td><input type="text" name="txtQtyBrokerageAmt_<?php echo $i;?>" id="txtQtyBrokerageAmt_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows2['NET_BROKERAGE'];?>" /></td>
                                                <td><input type="text" name="txtQtyNetFreight_<?php echo $i;?>" id="txtQtyNetFreight_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows2['NET_FREIGHT'];?>" /></td>
                                            </tr>
                                            
                                            <?php }}?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total Net</td>
                                            <td></td>
                                            <td></td>
                                            <td><input type="text"  name="txtTotalNetBrokerage" id="txtTotalNetBrokerage" autocomplete="off" class="form-control" readonly value="0.00" /></td>
                                            <td><input type="text"  name="txtTotalNetFreight" id="txtTotalNetFreight" autocomplete="off" class="form-control" readonly value="0.00" /></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                
                                
									<table class="table table-striped" id="divMarket7">
										<thead>
											<tr>
												<th width="20%">&nbsp;&nbsp;</th>
												<th width="20%">&nbsp;&nbsp;</th>
												<th width="20%">Percent</th>
												<th width="20%">USD</th>
												<th width="20%">Per MT</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Gross Freight</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdGF" id="txtFrAdjUsdGF" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"GROSS_FREIGHT_USD");?>" /></td>
												<td><input type="text"  name="txtFrAdjUsdGFMT" id="txtFrAdjUsdGFMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"GROSS_FREIGHT_PERMT");?>" /></td>
											</tr>
											
											<tr>
												<td>Dead Freight</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdDF" id="txtFrAdjUsdDF" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"DEAD_FREIGHT_USD");?>" /></td>
												<td><input type="text"  name="txtFrAdjUsdDFMT" id="txtFrAdjUsdDFMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"DEAD_FREIGHT_PERMT");?>" /></td>
											</tr>
											
											<tr>
												<td>Addnl Freight</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdAF" id="txtFrAdjUsdAF" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"ADDNL_FREIGHT_USD");?>" /></td>
												<td><input type="text"  name="txtFrAdjUsdAFMT" id="txtFrAdjUsdAFMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"ADDNL_FREIGHT_PERMT");?>" /></td>
											</tr>
											
											<tr>
												<td>Total Freight</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdTF" id="txtFrAdjUsdTF" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"TTL_FREIGHT_USD");?>" /></td>
												<td><input type="text"  name="txtFrAdjUsdTFMT" id="txtFrAdjUsdTFMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"TTL_FREIGHT_PERMT");?>" /></td>
											</tr>
											
											<!--<tr>
												<td>Address Commission</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjPerAC" id="txtFrAdjPerAC" class="form-control" autocomplete="off"  onkeyup="getFinalCalculation();"  value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AC_PERCENT");?>" /></td>
												<td><input type="text"  name="txtFrAdjUsdAC" id="txtFrAdjUsdAC" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AC_USD");?>" /></td>
												<td><input type="text"  name="txtFrAdjUsdACMT" id="txtFrAdjUsdACMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AC_PERMT");?>" /></td>
											</tr>-->
											
											<tr>
												<td>Brokerage</td>
												<td></td>
												<td><input type="text"  name="txtFrAdjPerAgC" id="txtFrAdjPerAgC"  onkeyup="getFinalCalculation(),getValue()" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AGC_PERCENT");?>" readonly ></td>
												<td><input type="text"  name="txtFrAdjUsdAgC" id="txtFrAdjUsdAgC" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AGC_USD");?>" /></td>
												<td></td>
											</tr>
											
											<tr>
												<td>Net Freight Payable</td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtFrAdjUsdFP" id="txtFrAdjUsdFP" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"NETT_FRT_PAYABLE");?>"  /></td>
												<td></td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
                            
                            <div class="box" id="divQty4">
								<div class="box-header">
									<h3 class="box-title">Freight Details</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="25%;"></th>
												<th width="25%;"></th>
												<th width="25%;"></th>
												<th width="25%;"></th>
											</tr>
										</thead>
                                        
										<tbody id="tblQtyFreight2">
                                        <?php 
										$sql12 = "select * from fca_tci_multiple_vendor_qty_details where FCAID='".$obj->getFun1()."'";
										$res12 = mysql_query($sql12);
										$rec12 = mysql_num_rows($res12);
										?>
										<?php if($rec12==0)
										{?>
											<tr id="tbrQtyVRow_empty2">
												<td valign="top" align="center" colspan="4" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
                                            <?php }
											else
											{$i = 0;
											while($rows2 = mysql_fetch_assoc($res12))
											{$i = $i + 1;
											?>
                                        
                                            <tr id="tbrQtyVRow2_<?php echo $i;?>">
                                                <td>Final Nett Freight</td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($rows2['QTY_VENDORID']);?></td>
                                                <td><input type="text" name="txtQtyNetFreight1_<?php echo $i;?>" id="txtQtyNetFreight1_<?php echo $i;?>" autocomplete="off" class="form-control" readonly value="<?php echo $rows2['NET_FREIGHT'];?>" /></td>
                                                <td><input type="text" name="txtQtyNetFreightMT1_<?php echo $i;?>" id="txtQtyNetFreightMT1_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['NET_FREIGHT_PERMT'];?>" readonly /></td>
                                            </tr>
                                            <?php }}?>								
										</tbody>
									</table>
								</div>
							</div>
                            
							
							<div class="box" id="divMarket8">
								<div class="box-header">
									<h3 class="box-title">Freight Details</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="25%;"></th>
												<th width="25%;"></th>
												<th width="25%;"></th>
												<th width="25%;"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Final Nett Freight</td>
												<td><select  name="selFGFVList" class="select form-control" id="selFGFVList" disabled ></select></td>
												<script>
													$("#selFGFVList").html($("#selVendor").html());
													$("#selFGFVList").val('<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"FGF_VENDORID");?>');
												</script>
												<td><input type="text"  name="txtFGFFD" id="txtFGFFD" class="form-control" onKeyUp="getFinalCalculation();" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"FINAL_NETT_FREIGHT_USD");?>" /></td>
												<td><input type="text"  name="txtFGFFDMT" id="txtFGFFDMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"FNF_MT");?>" /></td>
											</tr>											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Bunker Adjustment</h3>
									<a href="javascript:void(0)" onClick="getShowHide();" id="BID" name="BID"><img src="../../img/close.png" title="Show Hide" /></a>
									<input type="hidden" id="txtbid" value="0" />
								</div>
								<div class="box-body no-padding table-responsive" style="overflow:auto;" id="bunker_adj">
									<table class="table table-striped">
										<thead>
											<tr class="GridviewScrollHeader">
												<th colspan="1">Select Bunker grade</th>
												<?php
													
													$sql = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
													$res = mysql_query($sql);
													$rec = mysql_num_rows($res);
													
													if($rec == 0)
													{
												?>
												<tr>
													<td valign="top" align="center" colspan="6" style="color:red;">First fill the Bunker Grade Master Data.</td>
												</tr>
												<?php	
												}else{
												?>
												<?php $m=0; while($rows = mysql_fetch_assoc($res)){
													$ttl_bg[]  = $rows['NAME'];
													$ttl_bg1[] = $rows['BUNKERGRADEID'];
												?>
													 <th colspan="3" style="text-align:center;"><?php echo $rows['NAME'];?>&nbsp;&nbsp;</th>
												<?php $m++; ?>
											<?php } ?>
											</tr>
										</thead>
										<tbody>
											<tr class="GridviewScrollHeader">
												<td>&nbsp;</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){?>
												<td style="text-align:center;">MT</td>
												<td style="text-align:center;">Price</td>
												<td style="text-align:center;">Cost<input type="hidden" name="txtBunkerRec" id="txtBunkerRec" class="form-control" readonly value="<?php echo $rec;?>"/>			 
												<input type="hidden" name="txtBunkerGradeName_<?php echo $i+1;?>" id="txtBunkerGradeName_<?php echo $i+1;?>" class="form-control" readonly value="<?php echo $ttl_bg[$i];?>"/>	
												<input type="hidden" name="txtBunkerGradeID_<?php echo $i+1;?>" id="txtBunkerGradeID_<?php echo $i+1;?>" class="form-control" readonly value="<?php echo $ttl_bg1[$i];?>"/>
												 <input type="hidden" name="txtBHID_<?php echo $ttl_bg1[$i];?>" id="txtBHID_<?php echo $ttl_bg1[$i];?>" value="<?php echo $ttl_bg[$i];?>" class="form-control" readonly/>
												 <input type="hidden" name="txtBHID1[]" id="txtBHID1_<?php echo $ttl_bg1[$i];?>" value="<?php echo $ttl_bg1[$i];?>" class="form-control" readonly/>
												 <input type="hidden" name="txtTTLBAA_<?php echo $ttl_bg1[$i];?>" id="txtTTLBAA_<?php echo $ttl_bg1[$i];?>" class="form-control" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"OWNER_ADJ_COST");?>" readonly />
												 <input type="hidden" name="txtTTLEst_<?php echo $ttl_bg1[$i];?>" id="txtTTLEst_<?php echo $ttl_bg1[$i];?>" class="form-control" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_COST");?>" readonly /></td>
												<?php }?>
											</tr>
		
											<tr class="GridviewScrollItem">
												<td class="fixed-column">Estimated</td>
												<?php $j=$k=0; for($i=0;$i<count($ttl_bg);$i++){			
												?>
												<td>
												<input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_1" id="txt<?php echo $ttl_bg[$i];?>_1_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_MT");?>" class="form-control" autocomplete="off" style="width:150px;" readonly/>
												</td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_2" id="txt<?php echo $ttl_bg[$i];?>_1_2" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;" readonly /></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_3" id="txt<?php echo $ttl_bg[$i];?>_1_3" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_COST");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;" readonly /></td>
												<?php }?>
											</tr>
		
											<tr class="GridviewScrollItem">
												<td class="fixed-column">Supply</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_1" id="txt<?php echo $ttl_bg[$i];?>_8_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPPLY_MT");?>" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_2" id="txt<?php echo $ttl_bg[$i];?>_8_2" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPPLY_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_3" id="txt<?php echo $ttl_bg[$i];?>_8_3" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPPLY_COST");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
												<?php }?>
											</tr>
											<tr class="GridviewScrollItem">
												<td class="fixed-column">Supplier Adjustment</td>
													<?php for($i=0;$i<count($ttl_bg);$i++){
													
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_1" id="txt<?php echo $ttl_bg[$i];?>_9_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPP_ADJ_MT");?>" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_2" id="txt<?php echo $ttl_bg[$i];?>_9_2" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPP_ADJ_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_3" id="txt<?php echo $ttl_bg[$i];?>_9_3" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPP_ADJ_COST");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
												<?php }?>
											</tr>
											<tr class="GridviewScrollItem">
												<td class="fixed-column">Nett Supply </td>
													<?php for($i=0;$i<count($ttl_bg);$i++){
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_1" id="txt<?php echo $ttl_bg[$i];?>_10_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"NETT_MT");?>" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_2" id="txt<?php echo $ttl_bg[$i];?>_10_2" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"NETT_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_3" id="txt<?php echo $ttl_bg[$i];?>_10_3" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"NETT_COST");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
												<?php }?>
											</tr>
											<tr class="GridviewScrollItem">
												<td class="fixed-column">Actual Consumption</td>
													<?php for($i=0;$i<count($ttl_bg);$i++){
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_1" id="txt<?php echo $ttl_bg[$i];?>_11_1" onKeyUp="getBunkerCalculation();" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"ACTUAL_MT");?>" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_2" id="txt<?php echo $ttl_bg[$i];?>_11_2" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"ACTUAL_PRICE");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>

													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_3" id="txt<?php echo $ttl_bg[$i];?>_11_3" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"ACTUAL_COST");?>" onKeyUp="getBunkerCalculation();" class="form-control" autocomplete="off" style="width:150px;" readonly/></td>
												    <?php }?>
											</tr>
										<?php }?>									
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Bunkers Nett Supply</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="20%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="20%">Value/MT</th>
											</tr>
										</thead>
										<tbody id="tbbunkers">
											<?php 
											for($i=0;$i<count($ttl_bg);$i++){
											$query = "select * from fca_tci_bunker_nett where BUNKERGRADEID='".$ttl_bg1[$i]."' and FCAID='".$obj->getFun1()."'";
											
											$qres = mysql_query($query) or die($query);
											$qrec = mysql_num_rows($qres);
											$qrows = mysql_fetch_assoc($qres);
											?>
											<tr id="rowB_<?php echo $ttl_bg[$i];?>">
												<td><?php echo $ttl_bg[$i];?> Nett</td>
												<td></td>
												<td><input type="text"  name="txtBunkerCost_<?php echo $ttl_bg[$i];?>" id="txtBunkerCost_<?php echo $ttl_bg[$i];?>" class="form-control" readonly value="<?php echo $qrows['COST'];?>"/></td>
												<td><select  name="selBFOVList_<?php echo $ttl_bg[$i];?>" class="select form-control" id="selBFOVList_<?php echo $ttl_bg[$i];?>" disabled></select>
												<script>
												$("#selBFOVList_<?php echo $ttl_bg[$i];?>").html($("#selVendor").html());
												$("#selBFOVList_<?php echo $ttl_bg[$i];?>").val('<?php echo $qrows['VENDORID'];?>')
												</script>
												</td>
												<td><input type="text" name="txtBunkerCostMT_<?php echo $ttl_bg[$i];?>" id="txtBunkerCostMT_<?php echo $ttl_bg[$i];?>" class="form-control" readonly value="<?php echo $qrows['COST_MT'];?>"/></td>
											</tr>
										<?php }?>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Related Costs (Others)</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
											</tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                                <td></td>
                                                <td>Address Commission</td>
                                                <td><input type="text"  name="txtFrAdjPerAC" id="txtFrAdjPerAC" class="form-control" autocomplete="off"  onkeyup="getFinalCalculation();"  value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AC_PERCENT");?>" readonly /></td>
                                                <td><input type="text"  name="txtFrAdjUsdAC" id="txtFrAdjUsdAC" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AC_USD");?>" /></td>
                                                <td><input type="text"  name="txtFrAdjUsdACMT" id="txtFrAdjUsdACMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AC_PERMT");?>" /></td>
                                            </tr>
                                        </tbody>
										
										<tbody id="tbodyBrokerage">
										<?php 
										$sql_brok = "select * from fca_tci_brokage_commission where FCAID='".$obj->getFun1()."'";
										
										$res_brok = mysql_query($sql_brok);
										$num_brok = mysql_num_rows($res_brok);
										if($num_brok==0)
										{$num_brok =1;
										?>
                                             <tr id="tbrRow_1">
                                                <td><a href="#tb1'" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();" readonly /></td>
												<td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="form-control" readonly value="0.00" /></td>
												<td><select  name="selBroVList_1" class="select form-control" id="selBroVList_1" disabled></select>
													<script>$("#selBroVList_1").html($("#selVendor").html());$("#selBroVList_1").val('');</script>
												</td>
											</tr>
									<?php }
									     else
									      {$i=0;$num_brok = $num_brok;
										  while($rows_brok = mysql_fetch_assoc($res_brok))
										  {$i = $i + 1;?>
										  
										      <tr id="tbrRow_<?php echo $i;?>">
                                                <td><a href="#tb1'"><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_<?php echo $i;?>" id="txtBrCommPercent_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows_brok['BROKAGE_PERCENT'];?>" onKeyUp="getFinalCalculation();" readonly /></td>
												<td><input type="text" name="txtBrComm_<?php echo $i;?>" id="txtBrComm_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows_brok['BROKAGE_AMT'];?>" /></td>
												<td><select  name="selBroVList_<?php echo $i;?>" class="select form-control" id="selBroVList_<?php echo $i;?>" disabled></select>
													<script>$("#selBroVList_<?php echo $i;?>").html($("#selVendor").html());$("#selBroVList_<?php echo $i;?>").val('<?php echo $rows_brok['VENDORID'];?>');</script>
												</td>
											</tr>
										  <?php }} ?>
											
                                            </tbody>
										<tbody>
											<tr>
                                                <td><!--<button type="button" class="btn btn-primary btn-flat" onClick="addBrokerageRow()">Add</button>--><input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="<?php echo $num_brok;?>"/></td>
												<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM_PERCENT");?>" readonly/></td>
												<td><input type="text" name="txtBrComm" id="txtBrComm" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM");?>" /></td>
												<td>
												</td>
											</tr>
											</tbody>
											<tbody>
                                            <?php 
											$sql17 = "select * from owner_related_cost_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
											$res17 = mysql_query($sql17);
											$rec17 = mysql_num_rows($res17);
											$i=1;
											while($rows17 = mysql_fetch_assoc($res17))
											{
											?>
											<?php 
												$sql4 = "select * from fca_tci_owner_related_cost where FCAID='".$obj->getFun1()."' and OWNER_RCOSTID='".$rows17['OWNER_RCOSTID']."'";
												$res4 = mysql_query($sql4);
												$rows4 = mysql_fetch_assoc($res4);
																					
											?>
											<tr>
												<td>
												<?php echo $rows17['NAME'];?><input type="hidden" name="txtHidORCID_<?php echo $i;?>" id="txtHidORCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows17['OWNER_RCOSTID'];?>" />
												</td>
												<td></td>
												 <?php if($rows17['NAME'] == "CVE"){$read="readonly";$days = $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DAYS"); if($rows4['REMARKS']>0){$cveamt = $rows4['REMARKS'];}else{$cveamt = $rows4['AMOUNT'];} $cveamt1 =($cveamt/30)*$days;?>
												<td><input type="hidden" name="txtORCAmtCVEIdentify" id="txtORCAmtCVEIdentify"  value="<?php echo $i;?>" /><input type="text" name="txtORCAmtCVE_<?php echo $i;?>" id="txtORCAmtCVE_<?php echo $i;?>" onKeyUp="getCVEAmount();" class="form-control" autocomplete="off" value="<?php echo abs($cveamt);?>" placeholder="0.00" readonly /></td>
                                                <?php }else{$read="";$cveamt1=$rows4['AMOUNT']; ?>
                                                <td></td>
                                                <?php }?>
												<td>
                                                <?php if($rows17['RDO_STATUS'] == 1){?>
												<input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>" onKeyUp="getORCCalculate(<?php echo $rows17['RDO_STATUS'];?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="<?php echo number_format(abs($cveamt1),2,'.','');?>" placeholder="0.00" <?php echo $read;?> readonly />
												<?php }else if($rows17['RDO_STATUS'] == 2){?>
												<input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>"  onkeyup="getORCCalculate(<?php echo $rows17['RDO_STATUS'];?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="<?php echo number_format(abs($cveamt1),2,'.','');?>" <?php echo $read;?> placeholder="0.00" style="color:red;" readonly />
												<?php }?>
												</td>
												<td>
													<select  name="selORCVList_<?php echo $i;?>" class="select form-control" id="selORCVList_<?php echo $i;?>" disabled>
														<?php //$obj->getVendorListNewUpdate("");	?>
													</select>
												<input type="hidden" name="txtHidORCAmt_<?php echo $i;?>" id="txtHidORCAmt_<?php echo $i;?>" autocomplete="off" value="<?php echo $rows4['AMOUNT'];?>" readonly /></td>
												<script>$("#selORCVList_<?php echo $i;?>").html($("#selVendor").html());$("#selORCVList_<?php echo $i;?>").val('<?php echo $rows4['VENDORID'];?>')</script>
											</tr>								
											<?php $i++;}?>
											<tr>
												<td>Total Shipowner Expenses<input type="hidden" name="txtORC_id" id="txtORC_id" value="<?php echo $rec17;?>" /></td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtTTLORCAmt" id="txtTTLORCAmt" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SHIPOWNER_EXP");?>" /></td>
												<td></td>
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td style="font-weight:bold;">Value/MT</td>
												<td><input type="text" name="txtTTLORCCostMT" id="txtTTLORCCostMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SHIPOWNER_EXP_MT");?>" /></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Charterers' Costs</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%">Add Comm (%)</th>
												<th width="20%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="20%">Nett Value</th>
											</tr>
										</thead>
										<tbody>
                                        
                                        <?php 
											$sql21 = "select * from charterers_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
											$res21 = mysql_query($sql21);
											$rec21 = mysql_num_rows($res21);
											$i=1;
											while($rows21 = mysql_fetch_assoc($res21))
											{
												$sql5 = "select * from fca_tci_charterer_cost where FCAID='".$obj->getFun1()."' and CHARTERER_COSTID='".$rows21['CHARTERER_COSTID']."'";
												$res5 = mysql_query($sql5);
												$ccvalue = "";
												$rows5 = mysql_fetch_assoc($res5);
											?>
											<tr>
												<td>
												<?php echo $rows21['NAME'];?><input type="hidden" name="txtHidCCID_<?php echo $i;?>" id="txtHidCCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows21['CHARTERER_COSTID'];?>" />
												</td>
												<td><input type="text"  name="txtCCAddComm_<?php echo $i;?>" id="txtCCAddComm_<?php echo $i;?>" autocomplete="off" value="<?php echo $rows5['ADDCOMM'];?>" onKeyUp="getCharterersCostCalculate(<?php echo $i;?>);" class="form-control" placeholder="0.00" readonly/></td>
													<td><input type="text"  name="txtCCAbs_<?php echo $i;?>" id="txtCCAbs_<?php echo $i;?>" autocomplete="off" value="<?php echo $rows5['ABSOLUTE'];?>" onKeyUp="getCharterersCostCalculate(<?php echo $i;?>);" class="form-control" placeholder="0.00" readonly /></td>
													<td>
														<select  name="selCCVList_<?php echo $i;?>" class="select form-control" id="selCCVList_<?php echo $i;?>" disabled>
															<?php //$obj->getVendorListNewUpdate("");	?>
														</select>
													</td>
													<script>$("#selCCVList_<?php echo $i;?>").html($("#selVendor").html());$("#selCCVList_<?php echo $i;?>").val('<?php echo $rows5['VENDORID'];?>')</script>
													<td>
														<?php $ccvalue = $rows5['ABSOLUTE'] - (($rows5['ABSOLUTE'] * $rows5['ADDCOMM'])/100);?>
														<input type="text" name="txtCCValue_<?php echo $i;?>" id="txtCCValue_<?php echo $i;?>" value="<?php echo number_format($ccvalue,2,'.','');?>" class="form-control" placeholder="0.00" readonly />
														<input type="hidden" name="txtCCostMT_<?php echo $i;?>" id="txtCCostMT_<?php echo $i;?>" readonly value="<?php echo $rows5['COST_MT'];?>"/>
													</td>
												
											</tr>
											<?php $i++;}?>
                                                  <tr>
													<td colspan="5"><input type="hidden" name="txtCC_id" id="txtCC_id" value="<?php echo $rec21;?>" /></td>
												</tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Port Costs</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="20%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="20%">Value/MT</th>
											</tr>
										</thead>
										<tbody id="tbPortCosts">
											<?php 
											$mysql = "select * from fca_tci_portcosts where FCAID='".$obj->getFun1()."' order by FCA_PORTCOSTID asc";
											$myres = mysql_query($mysql);
											$myrec = mysql_num_rows($myres);
											if($myrec > 0)
											{
												$i=1;
												$j=1;
												$k=1;
												while($myrows = mysql_fetch_assoc($myres))
												{
													if($myrows['PORT'] == "Load")
													{?>
												<tr id="oscLProw_<?php echo $i;?>">
													<td>Load Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
													<td></td>
													<td>
													<?php if($obj->getFDA($mappingid,'LP',$myrows['LOADPORTID']) > 0){?>
													<input type="text"  name="txtLPOSCCost_<?php echo $i?>" id="txtLPOSCCost_<?php echo $i?>" onKeyUp="getPortCostSum(<?php echo $i;?>,'LP');" class="form-control" readonly value="<?php echo $obj->getFDA($mappingid,'LP',$myrows['LOADPORTID']);?>" />
													 <?php }else{?>
													<input type="text"  name="txtLPOSCCost_<?php echo $i?>" id="txtLPOSCCost_<?php echo $i?>" onKeyUp="getPortCostSum(<?php echo $i;?>,'LP');" class="form-control" readonly value="<?php echo $myrows['COST'];?>" />
													<?php }?>
													</td>
													<td>
														<select  name="selLPOSCCost_<?php echo $i;?>" class="select form-control" id="selLPOSCCost_<?php echo $i;?>" disabled>
															<?php //$obj->getVendorListNewUpdate("");	?>
														</select>
													</td>
													<script>$("#selLPOSCCost_<?php echo $i;?>").html($("#selVendor").html());$("#selLPOSCCost_<?php echo $i;?>").val('<?php echo $myrows['VENDORID'];?>')</script>
													<td><input type="text" name="txtLPOSCCostMT_<?php echo $i;?>" id="txtLPOSCCostMT_<?php echo $i;?>" readonly value="<?php echo $myrows['COST_MT'];?>" class="form-control" /></td>
												</tr>
												<!--<tr id="oscLProw1_<?php //echo $i?>" colspan="5">
													<td></td>
												</tr>-->
												<?php $i++;
												}else if($myrows['PORT'] == "Discharge"){?>
												<tr id="oscDProw_<?php echo $j;?>">
													<td>Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
													<td></td>
													<td>
													<?php if($obj->getFDA($mappingid,'DP',$myrows['LOADPORTID']) > 0){?>
													<input type="text"  name="txtDPOSCCost_<?php echo $j;?>" id="txtDPOSCCost_<?php echo $j;?>" readonly onKeyUp="getPortCostSum(<?php echo $j;?>,'DP');" value="<?php echo $obj->getFDA($mappingid,'DP',$myrows['LOADPORTID']);?>" class="form-control" />
													<?php }else{?>
													<input type="text"  name="txtDPOSCCost_<?php echo $j;?>" id="txtDPOSCCost_<?php echo $j;?>" readonly onKeyUp="getPortCostSum(<?php echo $j;?>,'DP');" value="<?php echo $myrows['COST'];?>" class="form-control" />
													<?php }?>
													</td>
													<td>				
														<select  name="selDPOSCCost_<?php echo $j;?>" class="select form-control" id="selDPOSCCost_<?php echo $j;?>" disabled>
															<?php //$obj->getVendorListNewUpdate("");	?>
														</select>
													</td>
													<script>$("#selDPOSCCost_<?php echo $j;?>").html($("#selVendor").html());$("#selDPOSCCost_<?php echo $j;?>").val('<?php echo $myrows['VENDORID'];?>')</script>
													<td><input type="text" name="txtDPOSCCostMT_<?php echo $j;?>" id="txtDPOSCCostMT_<?php echo $j;?>" readonly value="<?php echo $myrows['COST_MT'];?>" class="form-control"  /></td>
												</tr>
												<!--<tr id="oscDProw1_<?php //echo $j;?>" colspan="5">
													<td></td>
												</tr>-->
												<?php $j++;}else if($myrows['PORT'] == "Transit"){?>
												<tr id="oscTProw_<?php echo $k;?>">
													<td>Transit Port   <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
													<td></td>
													<td><input type="text"  name="txtTPOSCCost_<?php echo $k;?>" id="txtTPOSCCost_<?php echo $k;?>" readonly onKeyUp="getPortCostSum(<?php echo $k;?>,'TP');" value="<?php echo $myrows['COST'];?>" class="form-control" readonly /></td>
													<td>
														<select  name="selTPOSCCost_<?php echo $k;?>" class="select form-control" id="selTPOSCCost_<?php echo $k;?>" disabled>
															<?php //$obj->getVendorListNewUpdate("");	?>
														</select>
													</td>
													<script>$("#selTPOSCCost_<?php echo $k;?>").html($("#selVendor").html());$("#selTPOSCCost_<?php echo $k;?>").val('<?php echo $myrows['VENDORID'];?>')</script>
													<td><input type="text" name="txtTPOSCCostMT_<?php echo $k;?>" id="txtTPOSCCostMT_<?php echo $k;?>" readonly value="<?php echo $myrows['COST_MT'];?>" class="form-control" /></td>
												</tr>
												<?php $k++;}?>
											<?php }
											
											}?>
											</tbody>
											<tfoot>
												<tr>
													<td><b>Total Port Costs</b></td>
													<td></td>
													<td><input type="text"  name="txtTTLPortCosts" id="txtTTLPortCosts" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_COSTS");?>" readonly /></td>
													<td></td>
													<td><input type="text" name="txtTTLPortCostsMT" id="txtTTLPortCostsMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_COSTS_MT");?>" readonly /></td>
												</tr>
										</tfoot>
									</table>
								</div>
							</div>			
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Demurrage Dispatch Customer</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="15%">Add Comm (%)</th>
												<th width="15%">Estimated Cost</th>
                                                <th width="15%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="15%">Nett Value</th>
											</tr>
										</thead>
										<tbody id="tbDDSW" >
										<?php 
											$mysql2 = "select * from fca_tci_dd_shipowner where FCAID='".$obj->getFun1()."'";
											$myres2 = mysql_query($mysql2);
											$myrec2 = mysql_num_rows($myres2);
											if($myrec2 > 0)
											{$i=1;
												while($myrows2 = mysql_fetch_assoc($myres2)){
													if($myrows2['PORT_TYPE']== "Load")
													{
													$laytime_val = $obj->getLayTimeValuesBasedOnMappingID($mappingid,$myrows2['LOADPORTID'],"LP");
													$laytime_val = explode('@#@',$laytime_val);
													if($laytime_val[1]== 0){$laytime = $laytime_val[2];}else{$laytime = $laytime_val[1];}
													if($laytime_val[0]== 0){$readonly = "";}else{$readonly = "readonly";}
											?>
											<tr id="ddswLProw_<?php echo $i;?>"> 
												<td>Load Port <?php echo $obj->getPortNameBasedOnID($myrows2['LOADPORTID']);?></td>
												<td><input type="text"  name="txtDDSOLPComm_<?php echo $i;?>" id="txtDDSOLPComm_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $myrows2['ADDCOMM'];?>" onKeyUp="getFinalCalculation();" placeholder="0.00" readonly /></td>
												
												<td><input type="text"  name="txtEstDDSOLPCost_<?php echo $i;?>" id="txtEstDDSOLPCost_<?php echo $i;?>" class="form-control" value="<?php echo $myrows2['COST'];?>" onKeyUp="getFinalCalculation();" <?php echo $readonly;?> readonly /><input type="hidden" name="txtLPLayTimeIs_<?php echo $i;?>" id="txtLPLayTimeIs_<?php echo $i;?>" value="<?php echo $laytime_val[0];?>" /></td>
												
												<td><input type="text"  name="txtDDSOLPCost_<?php echo $i;?>" id="txtDDSOLPCost_<?php echo $i;?>" class="form-control" value="<?php echo $laytime;?>" onKeyUp="getFinalCalculation();" readonly/></td>
                                                <td><select name="selFGFVSOListLPortC_<?php echo $i;?>" class="select form-control" id="selFGFVSOListLPortC_<?php echo $i;?>" disabled></select></td>
												<?php $omcvalue = $myrows2['COST'] - (($myrows2['COST'] * $myrows2['ADDCOMM'])/100);?>
												<td><input type="text" name="txtDDSOLPNetCostMT_<?php echo $i;?>" id="txtDDSOLPNetCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $omcvalue;?>" readonly /><input type="hidden" name="txtDDSOLPCostMT_<?php echo $i;?>" id="txtDDSOLPCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $myrows2['COST_MT'];?>" /></td>
                                                <script>$("#selFGFVSOListLPortC_<?php echo $i;?>").html($("#selVendor").html());$("#selFGFVSOListLPortC_<?php echo $i;?>").val('<?php echo $myrows2['VENDORID'];?>');</script>
											</tr>	
											<?php $i++;}} ?>
											<?php }?>
                                            
                                            <?php 
											$mysql2 = "select * from fca_tci_dd_shipowner where FCAID='".$obj->getFun1()."'";
											$myres2 = mysql_query($mysql2);
											$myrec2 = mysql_num_rows($myres2);
											if($myrec2 > 0)
											{$i=1;
												while($myrows2 = mysql_fetch_assoc($myres2))
												{
													if($myrows2['PORT_TYPE']== "Discharge")
													{
													 $laytime_val = $obj->getLayTimeValuesBasedOnMappingID($mappingid,$myrows2['LOADPORTID'],"DP");
													 $laytime_val = explode('@#@',$laytime_val);
													 if($laytime_val[1]== 0){$laytime = $laytime_val[2];}else{$laytime = $laytime_val[1];}
													 if($laytime_val[0]== 0){$readonly = "";}else{$readonly = "readonly";}
													?>
													<tr id="ddswDProw_<?php echo $i;?>">
													<td>Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows2['LOADPORTID']);?></td>
													<td><input type="text"  name="txtDDSODPComm_<?php echo $i;?>" id="txtDDSODPComm_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $myrows2['ADDCOMM'];?>" onKeyUp="getFinalCalculation();" placeholder="0.00" readonly /></td>
                                                    
													<td><input type="text"  name="txtEstDDSODPCost_<?php echo $i;?>" id="txtEstDDSODPCost_<?php echo $i;?>" class="form-control" value="<?php echo $myrows2['COST'];?>" onKeyUp="getFinalCalculation();" <?php echo $readonly;?> readonly/><input type="hidden" name="txtDPLayTimeIs_<?php echo $i;?>" id="txtDPLayTimeIs_<?php echo $i;?>" value="<?php echo $laytime_val[0];?>" readonly /></td>
													
													<td><input type="text"  name="txtDDSODPCost_<?php echo $i;?>" id="txtDDSODPCost_<?php echo $i;?>" class="form-control" value="<?php echo $laytime;?>" onKeyUp="getFinalCalculation();" readonly/></td>
													<td><select name="selFGFVSOListDPortC_<?php echo $i;?>" class="select form-control" id="selFGFVSOListDPortC_<?php echo $i;?>" disabled ></select></td>
													<?php $omcvalue = $myrows2['COST'] - (($myrows2['COST'] * $myrows2['ADDCOMM'])/100);?>
													<td><input type="text" name="txtDDSODPNetCostMT_<?php echo $i;?>" id="txtDDSODPNetCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $omcvalue;?>" /><input type="hidden" name="txtDDSODPCostMT_<?php echo $i;?>" id="txtDDSODPCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $myrows2['COST_MT'];?>" readonly /></td>
                                                    <script>$("#selFGFVSOListDPortC_<?php echo $i;?>").html($("#selVendor").html());$("#selFGFVSOListDPortC_<?php echo $i;?>").val('<?php echo $myrows2['VENDORID'];?>');</script>
													</tr>	
											<?php $i++;}}}?>
											
										</tbody>
									</table>
								</div>
							</div>				
							
							<!--<div class="box">
								<div class="box-header">
									<h3 class="box-title">Demurrage Dispatch Ship Owner</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%">Add Comm (%)</th>
												<th width="20%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="20%">Nett Value</th>
												
											</tr>
										</thead>
										<tbody>
										<tbody>
										<?php
											$cost_sheet_id 	= $obj->getLatestCostSheetID($mappingid);
											$arr  			= $obj->getLoadPortAndDischargePortArrBasedOnMappingidAndProcessWithoutTBN($mappingid,$cost_sheet_id);
											$port_arr   	= $obj->getLoadPortAndDischargePortArrBasedOnIDMappingidAndProcessWithoutTBN($mappingid,$cost_sheet_id);
										?>
										<?php 
										$j = 1;
										for($i=0;$i<count($arr);$i++){?>
										<tr>
											<td><?php echo $arr[$i]; ?><input type="hidden" name="txtLPDP_<?php echo $j;?>" id="name="txtLPDP_<?php echo $j;?>"" value="<?php echo $arr[$i]; ?>"></td>
											<td><input type="text"  name="txtddswAddComm_<?php echo $j;?>" id="txtddswAddComm_<?php echo $j;?>" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"DEMU_ADD_COMM");?>" onKeyUp="getFinalCalculation();" placeholder="0.00" /></td>
											<td><input type="text"  name="txtddswCost_<?php echo $j;?>" id="txtddswCost_<?php echo $j;?>" class="form-control" value="<?php echo $obj->getLaytimeRecordsBestOnMappingIdAndLoadPort($mappingid,$arr[$i],$port_arr[$i],"TTL_DEMURRAGE");?>" onKeyUp="getFinalCalculation();" placeholder="0.00" readonly/></td>
											<td>
											<select  name="selddswCost_<?php echo $j;?>" class="select form-control" id="selddswCost_<?php echo $j;?>">
											</select></td>
											<script>$("#selddswCost_<?php echo $j;?>").html($("#selVendor").html());$("#selddswCost_<?php echo $j;?>").val('<?php echo $obj->getLaytimeRecordsBestOnMappingIdAndLoadPort($mappingid,$arr[$i],$port_arr[$i],"VENDOR");?>')</script>
											<td><input type="text" name="txtddswValue_<?php echo $j;?>" id="txtddswValue_<?php echo $j;?>" class="form-control" value="<?php echo $obj->getLaytimeRecordsBestOnMappingIdAndLoadPort($mappingid,$arr[$i],$port_arr[$i],"TTL_DEMURRAGE");?>" placeholder="0.00" readonly/></td>
										</tr>										
										<?php $j++; } ?>
										<tr><td colspan="5"><input type="hidden" name="txtDemurrageDispatchID" id="txtDemurrageDispatchID" value="<?php echo count($arr); ?>"></td></tr>
										</tbody>
									</table>
								</div>
							</div>-->
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Other Misc. Income</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%">Add Comm (%)</th>
												<th width="20%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="20%">Nett Value</th>
												
											</tr>
										</thead>
										<tbody>
                                          <?php 
											$sql31 = "select * from other_misc_cost_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
											$res31 = mysql_query($sql31);
											$rec31 = mysql_num_rows($res31);
											$i=1;
											while($rows31 = mysql_fetch_assoc($res31))
											{
											 
												$sql6 = "select * from fca_tci_other_misc_cost where FCAID='".$obj->getFun1()."' and OTHER_MCOSTID='".$rows31['OTHER_MCOSTID']."'";
												$res6 = mysql_query($sql6);
												$rows6 = mysql_fetch_assoc($res6);
											?>
											<tr>
												<td><?php echo $rows31['NAME'];?><input type="hidden" name="txtHidOMCID_<?php echo $i;?>" id="txtHidOMCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows31['OTHER_MCOSTID'];?>" /></td>
												<td><input type="text"  name="txtOMCAddComm_<?php echo $i;?>" id="txtOMCAddComm_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows6['ADDCOMM'];?>" onKeyUp="getOMCCalculate(<?php echo $i;?>);" placeholder="0.00" readonly /></td>
												<td><input type="text"  name="txtOMCAbs_<?php echo $i;?>" id="txtOMCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows6['AMOUNT'];?>" onKeyUp="getOMCCalculate(<?php echo $i;?>);" readonly /></td>
												<td>
													<select  name="selOMCVList_<?php echo $i;?>" class="select form-control" id="selOMCVList_<?php echo $i;?>" disabled>
														<?php //$obj->getVendorListNewUpdate("");	?>
													</select>
												</td>
												<script>$("#selOMCVList_<?php echo $i;?>").html($("#selVendor").html());$("#selOMCVList_<?php echo $i;?>").val('<?php echo $rows6['VENDORID'];?>')</script>
												<td>
												<?php $omcvalue = $rows6['AMOUNT'] - (($rows6['AMOUNT'] * $rows6['ADDCOMM'])/100);?>
												<input type="text" name="txtOMCValue_<?php echo $i;?>" id="txtOMCValue_<?php echo $i;?>" readonly value="<?php echo number_format($omcvalue,2,'.','');?>" class="form-control" autocomplete="off"/>
												<input type="hidden"  name="txtOMCCostMT_<?php echo $i;?>" id="txtOMCCostMT_<?php echo $i;?>" readonly value="<?php echo $rows6['COST_MT'];?>" /></td>
											</tr>											
											<?php $i++; ?>
										<?php }?>
										<tr><td colspan="5" align="left" class="text" valign="top" ><input type="hidden" name="txtAOMC_id" id="txtAOMC_id" value="<?php echo $rec31;?>" /></td></tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Results</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="30%"></th>
												<th width="23%"></th>
												<th width="23%"></th>
												<th width="24%"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Revenue (Final Nett Freight)</td>
												<td></td>
												<td><input type="text"  name="txtRevenue" id="txtRevenue" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"REVENUE");?>" placeholder="0.00" /></td>
												<td></td>
											</tr>
											<tr>
												<td>Total Owners Expenses</td>
												<td></td>
												<td><input type="text"  name="txtTTLOwnersExpenses" id="txtTTLOwnersExpenses" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_OWNER_EXP");?>" placeholder="0.00" /></td>
												<td></td>
											</tr>	
											<tr>
												<td>Total Charterers' Expenses</td>
												<td></td>
												<td><input type="text"  name="txtTTLCharterersExpenses" id="txtTTLCharterersExpenses" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_CHARTERER_EXP");?>" placeholder="0.00" /></td>
												<td></td>
											</tr>
											<tr>
												<td>Voyage Earnings</td>
												<td></td>
												<td><input type="text"  name="txtVoyageEarnings" id="txtVoyageEarnings" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"VOYAGE_EARNING");?>" placeholder="0.00" /></td>
												<td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;" >Daily Earnings / TCE</td>
												<td></td>
												<td><input type="text"  name="txtDailyEarnings" id="txtDailyEarnings" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"DAILY_EARNING");?>" placeholder="0.00" /></td>
												<td></td>
											</tr>
											<tr>
												<td>Daily Time Charter (USD/Day)</td>
												<td></td>
												<td><input type="text"  name="txtDailyVesselOperatingExpenses" id="txtDailyVesselOperatingExpenses" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"DAILY_VO_EXP");?>" onKeyUp="getFinalCalculation();" placeholder="0.00" readonly /></td>
												<td><select  name="selFGFVListDailyVesselOperatingExpenses" class="select form-control" id="selFGFVListDailyVesselOperatingExpenses" disabled ></select>
												<script>
												$("#selFGFVListDailyVesselOperatingExpenses").html($("#selVendor").html());
												$("#selFGFVListDailyVesselOperatingExpenses").val("<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"VESSEL_OPR_VENDORID");?>");
												</script></td>
											</tr>
                                            <tr>
												<td>Hireage </td>
												<td></td>
												<td><input type="text"  name="txtHireargeAmt" id="txtHireargeAmt" class="form-control" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"HIREAGE_AMT");?>" placeholder="0.00" readonly /></td>
												<td></td>
											</tr>
                                            <tr>
												<td>Add Comm (%)</td>
												<td><input type="text"  name="txtHireargePercent" id="txtHireargePercent" class="form-control" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"HIREAGE_PERCENT");?>" placeholder="%" onKeyUp="getFinalCalculation();" readonly/></td>
												<td><input type="text"  name="txtHireargePercentAmt" id="txtHireargePercentAmt" class="form-control" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"HIREAGE_PER_AMT");?>" placeholder="0.00" readonly /></td>
												<td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;" >G Total Voyage Earnings&nbsp;&nbsp;&nbsp;<span style="font-size:10px; font-style:italic;">(Voyage Earnings + Demurrage)</span></td>
												<td></td>
												<td><input type="text"  name="txtGTTLVoyageEarnings" id="txtGTTLVoyageEarnings" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"GRAND_TTL_VOYAGE_EARNING");?>" placeholder="0.00" /></td>
												<td></td>
											</tr>
                                            <tr>
												<td>Nett Hireage </td>
												<td></td>
												<td><input type="text"  name="txtNettHireargeAmt" id="txtNettHireargeAmt" class="form-control" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"HIREAGE_NETT_AMT");?>" placeholder="0.00" readonly /></td>
												<td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">Nett Daily Earnings</td>
												<td></td>
												<td><input type="text"  name="txtNettDailyEarnings" id="txtNettDailyEarnings" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"NETT_DAILY_EARNING");?>" placeholder="0.00" /></td>
												<td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">Nett Daily Profit</td>
												<td></td>
												<td><input type="text"  name="txtNettDailyProfit" id="txtNettDailyProfit" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"NETT_DAILY_PROFIT");?>" placeholder="0.00" /></td>
												<td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">P/L</td>
												<td></td>
												<td><input type="text"  name="txtPL" id="txtPL" class="form-control" value="<?php echo $obj->getFun30();?>" readonly placeholder="0.00" /></td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">&nbsp;
									
									</h2>                            
								</div><!-- /.col -->
							</div>
							            
                        <?php if($rights == 1){ ?>	            
                        <div class="box-footer" align="right">
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<!--<button class="btn btn-primary btn-flat" type="button" onClick="return getValidate('1');">Submit to Estimate</button>
							<button class="btn btn-primary btn-flat" type="button" onClick="return getValidate('2');">Submit to Close</button>-->
							<input type="hidden" name="action" value="submit" />
				        </div>
						<?php } ?>
                  </form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>


<script type="text/javascript">
$(document).ready(function(){

$('[data-toggle="tooltip"]').tooltip();

$("#TCI_18,#TCI_19").show();
if($("#rdoQtyType1").is(":checked"))
{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
if($("#rdoQtyType2").is(":checked"))
{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
if(parseFloat(cargo_qty))
{
	var per_mt = parseFloat(cargo_qty);
}

var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat(per_mt);
$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));

$("#txtMTCPDRate,#txtMLumpsum ,#txtWSFR, #txtWSFD ,#txtCQMT ,#txtMinQMT ,#txtAddnlQMT ,#txtWeather, #txtMargin ,#txtDistance ,#txtPCosts ,#txtQMT ,#txtRate ,#txtWDays1 ,#txtIDays ,#txtDPCosts ,#txtDQMT ,#txtDRate ,#txtDWDays1 ,#txtDIDays ,#txtTLPCosts ,#txtTLIDays ,#txtTFOCOffHire ,#txtTDOCOffHire ,#txtFrAdjPerAC ,#txtDiversionCost ,#txtDemCost ,#txtExtraDues ,#txtBrCommPercent ,[id^=txtORCAmt_],[id^=txtCCAbs_] ,#txtDailyVesselOperatingExpenses,[id^=txtCCAddComm_] ,[id^=txtMCWS_], [id^=txtMCDistanceLeg_], [id^=txtMCTotalDistance_],[id^=txtOMCAddComm_],[id^=txtOMCAbs_], [id^=txtOvrWS_],[id^=txtOvrDistanceLeg_],[id^=txtOvrTotalDistance_],[id^=txtAdditionAmt_],[id^=txtDeductionAmt_],#txtFrAdjUsdGF,#txtFrAdjPerACTF,#txtFrAdjPerACGF,#txtddswAddComm,[id^=txtDDSODPComm_],[id^=txtEstDDSODPCost_],[id^=txtDDSOLPComm_],[id^=txtEstDDSOLPCost_],#txtQtyAggriedFreight,#txtFreightQty,[id^=txtQtyBrokeragePer_],#txtQtyLocalAggriedFreight,#txtDisExchangeRate,#txtMarLocalAggriedFreight,#txtMarExchangeRate,#txtHireargePercent,#txtHireargeAmt").numeric();


$("#txtDate").datepicker({
	dateFormat: 'dd-M-yy'
});

showDWTField();
showMMarketField1();


var selFPortHtml = $("#selFPort").html();
$("#selTPort").html(selFPortHtml);


$('[id^=txtFPort_]').each(function(index) {
	var rowid = this.id;
	var lasrvar1 = rowid.split('_')[1];
	var text = $("#selFPort [value='"+$("#txtFPort_"+lasrvar1).val()+"']").text();
	//for load port.....................
	$('<option value="'+$("#txtFPort_"+lasrvar1).val()+'">'+text+'</option>').appendTo("#selLoadPort");
	//.........ends...................
	//for transit port.....................
	$('<option value="'+$("#txtFPort_"+lasrvar1).val()+'">'+text+'</option>').appendTo("#selTLoadPort");
	//.........ends...................
});

$('[id^=txtTPort_]').each(function(index) {
	var rowid = this.id;
	var lasrvar1 = rowid.split('_')[1];
	var text = $("#selTPort [value='"+$("#txtTPort_"+lasrvar1).val()+"']").text();
	//for discharge port.....................
	$('<option value="'+$("#txtTPort_"+lasrvar1).val()+'">'+text+'</option>').appendTo("#selDisPort");
	//.........ends...................
});


function getString(var1)
{
  var var2 = var1.split(' ');
  var var3 = var2[0].split('-');  
  return var3[2]+'/'+var3[1]+'/'+var3[0]+' '+var2[1];
}



$('#txtFromDate').datetimepicker({
	format: 'dd-mm-yyyy hh:ii',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
	}).on('changeDate', function(){
		$('#txtToDate').datetimepicker('setStartDate', new Date(getString($(this).val())));
		getTimeDiff();
 });
 
$('#txtToDate').datetimepicker({
  format: 'dd-mm-yyyy hh:ii',
  autoclose: true,
  todayBtn: true,
  minuteStep: 1
 }).on('changeDate', function(){  getTimeDiff(); });
 
 
$('#txtIdleFromDate').datetimepicker({
	format: 'dd-mm-yyyy hh:ii',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
	}).on('changeDate', function(){
		$('#txtIdleToDate').datetimepicker('setStartDate', new Date(getString($(this).val())));
		getIdleTimeDiff();
 });
 
$('#txtIdleToDate').datetimepicker({
  format: 'dd-mm-yyyy hh:ii',
  autoclose: true,
  todayBtn: true,
  minuteStep: 1
 }).on('changeDate', function(){  getIdleTimeDiff(); });


$('#txtWrkingFromDate').datetimepicker({
	format: 'dd-mm-yyyy hh:ii',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
	}).on('changeDate', function(){
		$('#txtwrkingToDate').datetimepicker('setStartDate', new Date(getString($(this).val())));
		getWrkingTimeDiff();
 });
 
$('#txtwrkingToDate').datetimepicker({
  format: 'dd-mm-yyyy hh:ii',
  autoclose: true,
  todayBtn: true,
  minuteStep: 1
 }).on('changeDate', function(){  getWrkingTimeDiff(); });
 

$('#txtDPIdleFromDate').datetimepicker({
	format: 'dd-mm-yyyy hh:ii',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
	}).on('changeDate', function(){
		$('#txtDPIdleToDate').datetimepicker('setStartDate', new Date(getString($(this).val())));
		getDPIdleTimeDiff();
 });
 
$('#txtDPIdleToDate').datetimepicker({
  format: 'dd-mm-yyyy hh:ii',
  autoclose: true,
  todayBtn: true,
  minuteStep: 1
 }).on('changeDate', function(){  getDPIdleTimeDiff(); });
 
 
$('#txtDPWrkingFromDate').datetimepicker({
	format: 'dd-mm-yyyy hh:ii',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
	}).on('changeDate', function(){
		$('#txtDPwrkingToDate').datetimepicker('setStartDate', new Date(getString($(this).val())));
		getDPWrkingTimeDiff();
 });
 
$('#txtDPwrkingToDate').datetimepicker({
  format: 'dd-mm-yyyy hh:ii',
  autoclose: true,
  todayBtn: true,
  minuteStep: 1
 }).on('changeDate', function(){  getDPWrkingTimeDiff(); });
 
 
$('#txtTPIdleFromDate').datetimepicker({
	format: 'dd-mm-yyyy hh:ii',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
	}).on('changeDate', function(){
		$('#txtTPIdleToDate').datetimepicker('setStartDate', new Date(getString($(this).val())));
		getTPIdleTimeDiff();
 });
 
$('#txtTPIdleToDate').datetimepicker({
  format: 'dd-mm-yyyy hh:ii',
  autoclose: true,
  todayBtn: true,
  minuteStep: 1
 }).on('changeDate', function(){  getTPIdleTimeDiff(); });
 
<?php for($i=0;$i<count($ttl_bg);$i++){			
?>
$("[id^=txt<?php echo $ttl_bg[$i];?>_]").numeric();
<?php }?>
showHideQtyVendorDiv(1);
getBunkerCalculation();
getTotalDWT();
getTotalDWT1();
});


function addPortRotationLocationDetails()
{
	if($("#txtLocationFrom").val() != "" && $("#txtLocationTo").val() != "" && $("#selPLocationType").val() != "" && $("#txtLocationDistance").val() != "" && $("#selSLocationSpeed").val() != "")
	{
			if($("#txtLocationDistance").val() == ""){var distance = 0;}else{var distance = $("#txtLocationDistance").val();}
			if($("#selPLocationType").val() == 1)
			{
				if($("#selSLocationSpeed").val() == 1)
				{
					var ballast    = <?php echo $bfs;?>;
					var fo_ballast = <?php echo $fo_bfs;?>;
					var do_ballast = <?php echo $do_bfs;?>;
				}
				if($("#selSLocationSpeed").val() == 2)
				{
					var ballast    = <?php echo $bes1;?>;
					var fo_ballast = <?php echo $fo_bes1;?>;
					var do_ballast = <?php echo $do_bes1;?>;
				}
				if($("#selSLocationSpeed").val() == 3)
				{
					var ballast    = <?php echo $bes2;?>;
					var fo_ballast = <?php echo $fo_bes2;?>;
					var do_ballast = <?php echo $do_bes2;?>;
				}
				
				var calc = (parseFloat(distance)/ parseFloat(parseFloat(ballast))/24) ;
				
				var voyage_time = parseFloat($("#txtVoyageLTime").val()) + parseFloat(calc);
				$("#txtVoyageLTime,#txtTTLVoyageLDays").val(voyage_time.toFixed(2));
				var ttl_days = parseFloat(calc) + parseFloat($("#txtBDays").val()) ;
				$("#txtBDays").val(ttl_days.toFixed(2));
				$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
				$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
				
				var consp_fo1 = parseFloat(parseFloat(calc) *  parseFloat(fo_ballast));
				var consp_do1 = parseFloat(parseFloat(calc) *  parseFloat(do_ballast));
				var consp_fo =  parseFloat($("#txtTFUMT").val()) + parseFloat(parseFloat(calc) *  parseFloat(fo_ballast));
				$("#txtTFUMT").val(consp_fo.toFixed(2));	
				var consp_do =  parseFloat($("#txtTDUMT").val()) + parseFloat(parseFloat(calc) *  parseFloat(do_ballast));
				$("#txtTDUMT").val(consp_do.toFixed(2));	
				
				var ttl_dis = parseFloat(distance) + parseFloat($("#txtBDist").val());
				$("#txtBDist").val(ttl_dis.toFixed(2));
				$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
			}
			if($("#selPLocationType").val() == 2)
			{
				if($("#selSLocationSpeed").val() == 1)
				{
					var ladan    = <?php echo $lfs;?>;
					var fo_ladan = <?php echo $fo_lfs;?>;
					var do_ladan = <?php echo $do_lfs;?>;
				}
				if($("#selSLocationSpeed").val() == 2)
				{
					var ladan    = <?php echo $les1;?>;
					var fo_ladan = <?php echo $fo_les1;?>;
					var do_ladan = <?php echo $do_les1;?>;
				}
				if($("#selSLocationSpeed").val() == 3)
				{
					var ladan    = <?php echo $les2;?>;
					var fo_ladan = <?php echo $fo_les2;?>;
					var do_ladan = <?php echo $do_les2;?>;
				}
				var calc = (parseFloat(distance) / parseFloat(parseFloat(ladan))/24) ;
				var voyage_time = parseFloat($("#txtVoyageLTime").val()) + parseFloat(calc);
				$("#txtVoyageLTime,#txtTTLVoyageLDays").val(voyage_time.toFixed(2));
				var ttl_days = parseFloat(calc) + parseFloat($("#txtLDays").val());
				$("#txtLDays").val(ttl_days.toFixed(2));
				$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
				$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
				
				var consp_fo1 = parseFloat(parseFloat(calc) *  parseFloat(fo_ladan));
				var consp_do1 = parseFloat(parseFloat(calc) *  parseFloat(do_ladan));
				var consp_fo =  parseFloat($("#txtTFUMT").val()) + parseFloat(parseFloat(calc) *  parseFloat(fo_ladan));
				$("#txtTFUMT").val(consp_fo.toFixed(2));	
				var consp_do =  parseFloat($("#txtTDUMT").val()) + parseFloat(parseFloat(calc) *  parseFloat(do_ladan));
				$("#txtTDUMT").val(consp_do.toFixed(2));	
				
				var ttl_dis = parseFloat(distance) + parseFloat($("#txtLDist").val());
				$("#txtLDist").val(ttl_dis.toFixed(2));
				$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
			}
			getBunkerCalculation();
			
			var id = $("#p_locationID").val();
			id = (id - 1) + 2;
			$("#PRLrow_Empty").remove();
			var p_type = document.getElementById("selPLocationType").selectedIndex;
			var p_type_text = document.getElementById("selPLocationType").options[p_type].text;
		
			var speed_type = document.getElementById("selSLocationSpeed").selectedIndex;
			var speed_type_text = document.getElementById("selSLocationSpeed").options[speed_type].text;
			
			$('<tr id="prl_Row_'+id+'"><td><a href="#pr'+id+'" id="splcancel_'+id+'" onclick="removePortLocation('+id+');" style="diplay:block;" ><i class="fa fa-times" style="color:red;"></i></a></td><td>'+$("#txtLocationFrom").val()+'<input type="hidden" name="txtFromLocation_'+id+'" id="txtFromLocation_'+id+'" value="'+$("#txtLocationFrom").val()+'"/></td><td>'+$("#txtLocationTo").val()+'<input type="hidden" name="txtToLocation_'+id+'" id="txtToLocation_'+id+'" value="'+$("#txtLocationTo").val()+'"/></td><td>'+p_type_text+'('+speed_type_text+')<input type="hidden" name="txtPLocationType_'+id+'" id="txtPLocationType_'+id+'" value="'+$("#selPLocationType").val()+'"/><input type="hidden" name="txtSLocationSpeed_'+id+'" id="txtSLocationSpeed_'+id+'" value="'+$("#selSLocationSpeed").val()+'"/></td><td>'+$("#txtLocationDistance").val()+'<input type="hidden" name="txtLocationDistance_'+id+'" id="txtLocationDistance_'+id+'" value="'+parseFloat($("#txtLocationDistance").val())+'"/><input type="hidden" name="txtLocationConspFO_'+id+'" id="txtLocationConspFO_'+id+'" value="'+parseFloat(consp_fo1.toFixed(2))+'"/><input type="hidden" name="txtLocationConspDO_'+id+'" id="txtLocationConspDO_'+id+'" value="'+parseFloat(consp_do1.toFixed(2))+'"/><input type="hidden" name="txtBallastDays_'+id+'" id="txtBallastDays_'+id+'" value="'+parseFloat(calc.toFixed(2))+'"/></td></tr>').appendTo("#tblPortLocation");
			
			$("#p_locationID").val(id);
			$("#txtLocationFrom,#txtLocationTo,#selPLocationType,#txtLocationDistance,#selSLocationSpeed").val("");
	}
	else
	{
		jAlert('Please fill all the records for Location', 'Alert');
	}
}


function removePortLocation(val)
{
  jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
	if(r){	
		var consp_fo = $("#txtLocationConspFO_"+val).val();
		var consp_do = $("#txtLocationConspDO_"+val).val();
		var distance = $("#txtLocationDistance_"+val).val();
		var bdays = $("#txtBallastDays_"+val).val();
	   
		if($("#txtPLocationType_"+val).val() == 1)
			{
				var ttl_days = parseFloat($("#txtBDays").val()) - parseFloat(bdays) ;
				$("#txtBDays").val(ttl_days.toFixed(2));
				$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
				$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
				
				var consp_fo =  parseFloat($("#txtTFUMT").val()) - parseFloat(consp_fo);
				$("#txtTFUMT").val(consp_fo.toFixed(2));	
				var consp_do =  parseFloat($("#txtTDUMT").val()) - parseFloat(consp_do);
				$("#txtTDUMT").val(consp_do.toFixed(2));	
				
				var ttl_dis = parseFloat($("#txtBDist").val()) - parseFloat(distance);
				$("#txtBDist").val(ttl_dis.toFixed(2));
				$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
			}
			if($("#txtPLocationType_"+val).val() == 2)
			{
				var ttl_days = parseFloat($("#txtLDays").val()) - parseFloat(bdays);
				$("#txtLDays").val(ttl_days.toFixed(2));
				$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
				$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
				
				var consp_fo =  parseFloat($("#txtTFUMT").val()) - parseFloat(consp_fo);
				$("#txtTFUMT").val(consp_fo.toFixed(2));	
				var consp_do =  parseFloat($("#txtTDUMT").val()) - parseFloat(consp_do);
				$("#txtTDUMT").val(consp_do.toFixed(2));	
				
				var ttl_dis = parseFloat($("#txtLDist").val()) - parseFloat(distance);
				$("#txtLDist").val(ttl_dis.toFixed(2));
				$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
			} 
			$("#prl_Row_"+val).remove();
			getBunkerCalculation();
		}
	});
       
}


function showCapField()
{
	if($("#rdoCap1").is(":checked"))
	{
		$("#txtGCap").removeAttr('disabled');
		$("#txtBCap").attr('disabled','disabled');
		getTotalDWT();
		getTotalDWT1();
	}
	if($("#rdoCap2").is(":checked"))
	{
		$("#txtGCap").attr('disabled','disabled');
		$("#txtBCap").removeAttr('disabled');		
		getTotalDWT();
		getTotalDWT1();
	}
}

function getTotalDWT()
{
	if($("#rdoDWT1").is(":checked")){var dwt = $("#txtDWTS").val();}
	if($("#rdoDWT2").is(":checked")){var dwt = $("#txtDWTT").val();}
	
	if($("#rdoCap1").is(":checked"))
	{
		if($("#txtGCap").val() == ""){var gcap = 0;}else{var gcap = $("#txtGCap").val();}
		if(parseFloat($("#txtSF").val())>0)
		{
			var ttl = parseFloat(parseFloat(gcap)*35.3146) / parseFloat($("#txtSF").val());
			$("#txtLoadable").val(ttl.toFixed(2));
		}else{
			$("#txtLoadable").val(0);
		}
	}
	getFinalCalculation();
}

function getTotalDWT1()
{
	if($("#rdoDWT1").is(":checked")){var dwt = $("#txtDWTS").val();}
	if($("#rdoDWT2").is(":checked")){var dwt = $("#txtDWTT").val();}
	
	if($("#rdoCap2").is(":checked"))
	{
		if($("#txtBCap").val() == ""){var bcap = 0;}else{var bcap = $("#txtBCap").val();}
		
		if(parseFloat($("#txtSF").val())>0)
		{	
			var ttl = parseFloat(parseFloat(bcap)*35.3146) / parseFloat($("#txtSF").val());
			$("#txtLoadable").val(ttl.toFixed(2));
		}else{
			$("#txtLoadable").val(0);
		}
	}
	getFinalCalculation();
}

function getTimeDiff()
{	
	if($('#txtFromDate').val() != '' &&  $('#txtToDate').val() != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat($('#txtToDate').val());
		start_actual_time    =  getDateWithSpecificFormat($('#txtFromDate').val());
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000;
		$('#txtDDays').val(days.toFixed(2));
		
		var actual = parseFloat($("#txtDDays,#txtMarginDays").sum()) - parseFloat($("#txtOHDDays,#txtOHIDays").sum());
		$("#txtActualDays").val(actual.toFixed(2));
	}
}

function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' '+dateSplit1[1];
	return currentDate;
}

function getIdleTimeDiff()
{
	if($('#txtIdleFromDate').val() != '' &&  $('#txtIdleToDate').val() != '')
	{
		var end_actual_time  	 =  getDateWithSpecificFormat($('#txtIdleToDate').val());
		var start_actual_time    =  getDateWithSpecificFormat($('#txtIdleFromDate').val());
	
		start_actual_time = new Date(start_actual_time);
		end_actual_time   = new Date(end_actual_time);
	
		var diff = end_actual_time - start_actual_time;
		var days = (diff) / 86400000;
		$('#txtIdleDays').val(days.toFixed(2));
		
		var actual = parseFloat($("#txtIdleDays,#txtwrkingDays").sum()) - parseFloat($("#txtLPOHDays").sum());
		$("#txtLPActualDays").val(actual.toFixed(2));
	}
}

function getTPIdleTimeDiff()
{
	if($('#txtTPIdleFromDate').val() != '' &&  $('#txtTPIdleToDate').val() != '')
	{
		var end_actual_time  	 =  getDateWithSpecificFormat($('#txtTPIdleToDate').val());
		var start_actual_time    =  getDateWithSpecificFormat($('#txtTPIdleFromDate').val());
	
		start_actual_time = new Date(start_actual_time);
		end_actual_time = new Date(end_actual_time);
	
		var diff = end_actual_time - start_actual_time;
		var days = (diff) / 86400000;
		$('#txtTPIdleDays').val(days.toFixed(2));
		
		var actual = parseFloat($("#txtTPIdleDays").sum()) - parseFloat($("#txtTPOHDays").sum());
		$("#txtTPActualDays").val(actual.toFixed(2));
	}
}

function getDPWrkingTimeDiff()
{
	if($('#txtDPWrkingFromDate').val() != '' &&  $('#txtDPwrkingToDate').val() != '')
	{
		var end_actual_time  	 =  getDateWithSpecificFormat($('#txtDPwrkingToDate').val());
		var start_actual_time    =  getDateWithSpecificFormat($('#txtDPWrkingFromDate').val());
	
		start_actual_time = new Date(start_actual_time);
		end_actual_time   = new Date(end_actual_time);
	
		var diff = end_actual_time - start_actual_time;
		var days = (diff) / 86400000;
		$('#txtDPwrkingDays').val(days.toFixed(2));
		
		var actual = parseFloat($("#txtDPIdleDays,#txtDPwrkingDays").sum()) - parseFloat($("#txtDPOHDays").sum());
		$("#txtDPActualDays").val(actual.toFixed(2));
	}
}

function getDPIdleTimeDiff()
{
	if($('#txtDPIdleFromDate').val() != '' &&  $('#txtDPIdleToDate').val() != '')
	{
		var end_actual_time  	 =  getDateWithSpecificFormat($('#txtDPIdleToDate').val());
		var start_actual_time    =  getDateWithSpecificFormat($('#txtDPIdleFromDate').val());
	
		start_actual_time = new Date(start_actual_time);
		end_actual_time = new Date(end_actual_time);
	
		var diff = end_actual_time - start_actual_time;
		var days = (diff) / 86400000;
		$('#txtDPIdleDays').val(days.toFixed(2));
		
		var actual = parseFloat($("#txtDPIdleDays,#txtDPwrkingDays").sum()) - parseFloat($("#txtDPOHDays").sum());
		$("#txtDPActualDays").val(actual.toFixed(2));
	}
}

function getWrkingTimeDiff()
{
	if($('#txtWrkingFromDate').val() != '' &&  $('#txtwrkingToDate').val() != '')
	{
		var end_actual_time  	 =  getDateWithSpecificFormat($('#txtwrkingToDate').val());
		var start_actual_time    =  getDateWithSpecificFormat($('#txtWrkingFromDate').val());
	
		start_actual_time = new Date(start_actual_time);
		end_actual_time = new Date(end_actual_time);
	
		var diff = end_actual_time - start_actual_time;
		var days = (diff) / 86400000;
		$('#txtwrkingDays').val(days.toFixed(2));
		
		var actual = parseFloat($("#txtIdleDays,#txtwrkingDays").sum()) - parseFloat($("#txtLPOHDays").sum());
		$("#txtLPActualDays").val(actual.toFixed(2));
	}

}


function getActualSeaPassageShow(var1)
{
	if($("#txtPUID").val() == "")
	{
		$("[id^=estRow]").hide();
		$("[id^=ActRow]").show();
		$("#txtFromDate").val($("#txtFDate_"+var1).val());
		$("#txtToDate").val($("#txtTDate_"+var1).val());
		$("#txtDDays").val($("#txtFTDays_"+var1).val());
		$("#txtMarginHrs").val($("#txtMarginHrs_"+var1).val());
		$("#txtMarginDays").val($("#txtMarginDays_"+var1).val());
		$("#txtActualDays").val($("#txtADays_"+var1).val());
		$("#txtPUID").val(var1);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}	
}


function removeActualSeaPassageShow()
{
	$("[id^=estRow]").show();
	$("[id^=ActRow]").hide();
	$("#txtPUID").val("");
}


function addActualSeaPassageShow()
{
	var var1 = $("#txtPUID").val();
	getTotal(var1);
	
	$("#txtFDate_"+var1).val($("#txtFromDate").val());
	$("#txtTDate_"+var1).val($("#txtToDate").val());
	$("#txtFTDays_"+var1).val($("#txtDDays").val());
	$("#txtMarginHrs_"+var1).val($("#txtMarginHrs").val());
	$("#txtMarginDays_"+var1).val($("#txtMarginDays").val());
	$("#txtADays_"+var1).val($("#txtActualDays").val());
	$("#adaysLabel_"+var1).html($("#txtActualDays").val());
	
	$("[id^=estRow]").show();
	$("[id^=ActRow]").hide();
	$("#txtPUID").val("");
	$("#txtFromDate,#txtToDate").removeClass("hasDatepicker");
}


function showDWTField()
{
	if($("#rdoDWT1").is(":checked"))
	{
		$("#txtDWTS").removeAttr('disabled');
		$("#txtDWTT").attr('disabled','disabled');
	}
	if($("#rdoDWT2").is(":checked"))
	{
		$("#txtDWTS").attr('disabled','disabled');
		$("#txtDWTT").removeAttr('disabled');
	}
}


function showMMarketField1()
{
	if($("#rdoMMarket1").is(":checked"))
	{
		$("#txtMTCPDRate,#txtMarLocalAggriedFreight,#selCurrencyMarList,#txtMarExchangeRate").removeAttr('disabled');
		$("#txtMLumpsum").attr('disabled','disabled');
	}
	if($("#rdoMMarket2").is(":checked"))
	{
		$("#txtMTCPDRate,#txtMarLocalAggriedFreight,#selCurrencyMarList,#txtMarExchangeRate").attr('disabled','disabled');
		$("#txtMLumpsum").removeAttr('disabled');
	}
}



function showQtyField()
{
	if($("#rdoQty1").is(":checked"))
	{
		$("#txtDFQMT").removeAttr('disabled');
		$("#txtAddnlQMT").attr('disabled','disabled');
		$("#txtDFQMT,#txtAddnlQMT").val("0.00");
		$("#txtDFQMT").focus();
	}
	if($("#rdoQty2").is(":checked"))
	{
		$("#txtDFQMT").attr('disabled','disabled');
		$("#txtAddnlQMT").removeAttr('disabled');
		$("#txtDFQMT,#txtAddnlQMT").val("0.00");
		$("#txtAddnlQMT").focus();
	}
}


function showMMarketField()
{
	if($("#rdoMMarket1").is(":checked"))
	{
		$("#txtMTCPDRate").removeAttr('disabled');
		$("#txtMLumpsum").attr('disabled','disabled');
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtMTCPDRate").focus();
		getFinalCalculation();
	}
	if($("#rdoMMarket2").is(":checked"))
	{
		$("#txtMTCPDRate").attr('disabled','disabled');
		$("#txtMLumpsum").removeAttr('disabled');
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtMLumpsum").focus();
		getFinalCalculation();
	}
}



function getToPort()
{
	if($('#selWSPCFP').val() != "" && $('#selWSPCTP').val() != "")
	{
		$('#txtWSFR,#txtWSFD').val("");
		$.post("options.php?id=11",{selFPort:""+$("#selWSPCFP").val()+"",selTPort:""+$("#selWSPCTP").val()+""}, function(html) {
		var var1 = html.split("#");
		$('#txtWSFR').val(var1[0]);
		$('#txtWSFD').val(var1[3]);
		getFinalCalculation();
		});
	}
	else
	{
		$('#txtWSFR,#txtWSFD').val("0.00");
	}
}



function getCalculateAddnl()
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
	var diff  = parseFloat(cargo_qty) - parseFloat(min_qty); 
	if(diff.toFixed(2) < 0){$("#txtAddnlQMT").val("0.00");}
	else{$("#txtAddnlQMT").val(diff.toFixed(2));}
}



function getDistance()
{
	$("#txtDistance").val("");
	$("#loader1").show();
	if($('#selFPort').val() != "" && $('#selTPort').val() != "" && $('#selDType').val() != "")
	{
		$("#txtDistance").val("");
		$.post("options.php?id=10",{selFPort:""+$("#selFPort").val()+"",selTPort:""+$("#selTPort").val()+"",selDType:""+$("#selDType").val()+""}, function(data) 
		{
				$('#txtDistance').val(data);
				$("#loader1").hide();
		});
	}
	else
	{
		$('#txtDistance').val("");
		$("#loader1").hide();
	}
}


//..............for See Passage details.....................

function addPortRotationDetails()
{
	if($("#selFPort").val() != "" && $("#selTPort").val() != "" && $("#selPType").val() != "" && $("#txtDistance").val() != "" && $("#selSSpeed").val() != "")
	{
		var id = $("#p_rotationID").val();
		var lasrvar1 = "";
		$('[id^=txtFPort_]').each(function(index) {
			var rowid = this.id;
			lasrvar1 = rowid.split('_')[1];
		});
		if($("#selFPort").val() == $("#txtTPort_"+lasrvar1).val() || id == 0 || lasrvar1 == "")
		{
			getVoyageTime();
			id = (id - 1) + 2;
			$("#PRrow_Empty").remove();
			var frm_port = document.getElementById("selFPort").selectedIndex;
			var frm_port_text = document.getElementById("selFPort").options[frm_port].text;
			var to_port = document.getElementById("selTPort").selectedIndex;
			var to_port_text = document.getElementById("selTPort").options[to_port].text;
			var p_type = document.getElementById("selPType").selectedIndex;
			var p_type_text = document.getElementById("selPType").options[p_type].text;
			
			var d_type = document.getElementById("selDType").selectedIndex;
			var d_type_text = document.getElementById("selDType").options[d_type].text;
			
			var speed_type = document.getElementById("selSSpeed").selectedIndex;
			var speed_type_text = document.getElementById("selSSpeed").options[speed_type].text;
			
			var FROM_DATE = TO_DATE = DAYS = MARGIN_HRS = MARGIN_DAYS = ACTUAL_DAYS = "";
			    var actual_sp = <?php echo $obj->getActualSeaPassagePortDataJson($obj->getFun1());?>;
				if(actual_sp!=null)
				{
				  $.each(actual_sp, function(index, array) {
				       if(array['FROM_PORTID'] == $("#selFPort").val() && array['TO_PORTID'] == $("#selTPort").val())
					    {
							FROM_DATE        = array['FROM_DATE'];
							TO_DATE 		 = array['TO_DATE'];
							DAYS             = array['DAYS'];
							MARGIN_HRS       = array['MARGIN_HRS'];
							MARGIN_DAYS 	 = array['MARGIN_DAYS'];
							ACTUAL_DAYS      = array['ACTUAL_DAYS'];
						}
				  });	
			   }
			if(MARGIN_HRS == "" && MARGIN_DAYS == "" && ACTUAL_DAYS == "" && DAYS == "")	
			{FROM_DATE = TO_DATE = ""; DAYS = MARGIN_HRS = MARGIN_DAYS = ACTUAL_DAYS = 0;}
			$('<tr id="pr_Row_'+id+'"><td><a href="#pr'+id+'" id="spcancel_'+id+'" onclick="removePortRotation('+id+','+$("#selFPort").val()+','+$("#selTPort").val()+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><a href="#Edit'+id+'" onclick="getActualSeaPassageShow('+id+');" ><i class="fa fa-edit" style="color:#1b77a6;"></i></a></td><td>'+frm_port_text+'<input type="hidden" name="txtFPort_'+id+'" id="txtFPort_'+id+'" value="'+$("#selFPort").val()+'"/></td><td>'+to_port_text+'<input type="hidden" name="txtTPort_'+id+'" id="txtTPort_'+id+'" value="'+$("#selTPort").val()+'"/></td><td>'+p_type_text+'('+speed_type_text+')<input type="hidden" name="txtPType_'+id+'" id="txtPType_'+id+'" value="'+$("#selPType").val()+'"/><input type="hidden" name="txtSSpeed_'+id+'" id="txtSSpeed_'+id+'" class="input" size="10" value="'+$("#selSSpeed").val()+'"/></td><td>'+parseFloat($("#txtDistance").val())+'('+d_type_text+')<input type="hidden" name="txtDistance_'+id+'" id="txtDistance_'+id+'" value="'+parseFloat($("#txtDistance").val())+'"/><input type="hidden" name="txtDType_'+id+'" id="txtDType_'+id+'" value="'+$("#selDType").val()+'"/></td><td>'+$("#txtWeather").val()+'<input type="hidden" name="txtWeather_'+id+'" id="txtWeather_'+id+'" value="'+$("#txtWeather").val()+'"/></td><td>'+$("#txtMargin").val()+'<input type="hidden" name="txtMargin_'+id+'" id="txtMargin_'+id+'" value="'+$("#txtMargin").val()+'"/></td><td>'+$("#txtEstDays").val()+'<input type="hidden" name="txtEDays_'+id+'" id="txtEDays_'+id+'" value="'+$("#txtEstDays").val()+'"/></td><td><span id="adaysLabel_'+id+'">'+ACTUAL_DAYS+'</span><input type="hidden" name="txtFDate_'+id+'" id="txtFDate_'+id+'" value="'+FROM_DATE+'"/><input type="hidden" name="txtTDate_'+id+'" id="txtTDate_'+id+'" value="'+TO_DATE+'"/><input type="hidden" name="txtFTDays_'+id+'" id="txtFTDays_'+id+'" class="input" size="10" value="'+DAYS+'"/><input type="hidden" name="txtMarginHrs_'+id+'" id="txtMarginHrs_'+id+'" value="'+MARGIN_HRS+'"/><input type="hidden" name="txtMarginDays_'+id+'" id="txtMarginDays_'+id+'" value="'+MARGIN_DAYS+'"/><input type="hidden" name="txtADays_'+id+'" id="txtADays_'+id+'" value="'+ACTUAL_DAYS+'"/></td></tr>').appendTo("#tblPortRotation");
			
			$("#txtEstDays").val("");
			//for load port.....................
			$('<option value="'+$("#selFPort").val()+'">'+frm_port_text+'</option>').appendTo("#selLoadPort");
			//.........ends...................
			//for discharge port.....................
			$('<option value="'+$("#selTPort").val()+'">'+to_port_text+'</option>').appendTo("#selDisPort");
			//.........ends...................
			//for transit port.....................
			$('<option value="'+$("#selFPort").val()+'">'+frm_port_text+'</option>').appendTo("#selTLoadPort");
			//.........ends...................
			$("#p_rotationID").val(id);
			$("#selFPort").val($("#selTPort").val());
			$("#selTPort,#selPType,#txtDistance,#txtWeather,#txtMargin,#selSSpeed").val("");
			$("#selDType").val("1");
			
			var lasrvar = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar = rowid.split('_')[1];
			});
			$("[id^=spcancel_]").hide();
			$("#spcancel_"+lasrvar).show();
		}
		else
		{
			jAlert('Please select in sequence of ports', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Sea Passage', 'Alert');
	}
	getBunkerCalculation();
	getFinalCalculation();
}



function getTotal(var1)
{
	if(parseFloat($("#txtActualDays").val()) == 0)
	{
		if($("#txtDistance_"+var1).val() == ""){var distance = 0;}else{var distance = $("#txtDistance_"+var1).val();}
		if($("#txtPType_"+var1).val() == 1)
		{
			if($("#txtSSpeed_"+var1).val() == 1)
			{
				var ballast = <?php echo $bfs;?>;
				var fo_ballast = <?php echo $fo_bfs;?>;
				var do_ballast = <?php echo $do_bfs;?>;
			}
			if($("#txtSSpeed_"+var1).val() == 2)
			{
				var ballast = <?php echo $bes1;?>;
				var fo_ballast = <?php echo $fo_bes1;?>;
				var do_ballast = <?php echo $do_bes1;?>;
			}
			if($("#txtSSpeed_"+var1).val() == 3)
			{
				var ballast = <?php echo $bes2;?>;
				var fo_ballast = <?php echo $fo_bes2;?>;
				var do_ballast = <?php echo $do_bes2;?>;
			}
			
			var calc = (parseFloat(distance) /  parseFloat(ballast))/24;
			
			if($("#txtADays_"+var1).val() == ""){var actual_days = 0;}else{var actual_days = $("#txtADays_"+var1).val();}
			if(parseFloat(actual_days) > 0)
			{
				if($("#txtMargin_"+var1).val() == ""){var o_margin = 0;}else{var o_margin = $("#txtMargin_"+var1).val();}
				
				var old_ttl = parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(o_margin))/100);
				
				var old_bdays = (parseFloat($("#txtBDays").val()) - parseFloat(actual_days)) + parseFloat(old_ttl.toFixed(2));
				$("#txtBDays").val(old_bdays.toFixed(2));
				
				$("#txtBDays").val($("#txtBDays,#txtActualDays").sum().toFixed(2));
				
				$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
				$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
				
				var consp_fo =  (parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(actual_days) *  parseFloat(fo_ballast))) + parseFloat(parseFloat(old_ttl.toFixed(2)) *  parseFloat(fo_ballast));
				$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));					
				
				var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(actual_days) *  parseFloat(do_ballast))) + parseFloat(parseFloat(old_ttl.toFixed(2)) *  parseFloat(do_ballast));
				$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
				
			}
		}
		if($("#txtPType_"+var1).val() == 2)
		{
			if($("#txtSSpeed_"+var1).val() == 1)
			{
				var ladan = <?php echo $lfs;?>;
				var fo_ladan = <?php echo $fo_lfs;?>;
				var do_ladan = <?php echo $do_lfs;?>;
			}
			if($("#txtSSpeed_"+var1).val() == 2)
			{
				var ladan = <?php echo $les1;?>;
				var fo_ladan = <?php echo $fo_les1;?>;
				var do_ladan = <?php echo $do_les1;?>;
			}
			if($("#txtSSpeed_"+var1).val() == 3)
			{
				var ladan = <?php echo $les2;?>;
				var fo_ladan = <?php echo $fo_les2;?>;
				var do_ladan = <?php echo $do_les2;?>;
			}
			var calc = (parseFloat(distance) /  parseFloat(ladan))/24;
			if($("#txtADays_"+var1).val() == ""){var actual_days = 0;}else{var actual_days = $("#txtADays_"+var1).val();}
			if(parseFloat(actual_days) > 0)
			{
				if($("#txtMargin_"+var1).val() == ""){var o_margin = 0;}else{var o_margin = $("#txtMargin_"+var1).val();}
				
				var old_ttl = parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(o_margin))/100);
				
				var old_ldays = (parseFloat($("#txtLDays").val()) - parseFloat(actual_days)) + parseFloat(old_ttl.toFixed(2));
				$("#txtLDays").val(old_ldays.toFixed(2));
				
				$("#txtLDays").val($("#txtLDays,#txtActualDays").sum().toFixed(2));
				
				$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
				$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
								
				var consp_fo =  (parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(actual_days) *  parseFloat(fo_ladan))) + parseFloat(parseFloat(old_ttl.toFixed(2)) *  parseFloat(fo_ladan)); ;
				$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));					
				
				var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(actual_days) *  parseFloat(do_ladan))) + parseFloat(parseFloat(old_ttl.toFixed(2)) *  parseFloat(do_ladan));
				$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
				
			}
		}
		//getTTLFreight();
	}
	
	if(parseFloat($("#txtActualDays").val()) > 0)
	{
		if($("#txtDistance_"+var1).val() == ""){var distance = 0;}else{var distance = $("#txtDistance_"+var1).val();}
		if($("#txtPType_"+var1).val() == 1)
		{
			if($("#txtSSpeed_"+var1).val() == 1)
			{
				var ballast = <?php echo $bfs;?>;
				var fo_ballast = <?php echo $fo_bfs;?>;
				var do_ballast = <?php echo $do_bfs;?>;
			}
			if($("#txtSSpeed_"+var1).val() == 2)
			{
				var ballast = <?php echo $bes1;?>;
				var fo_ballast = <?php echo $fo_bes1;?>;
				var do_ballast = <?php echo $do_bes1;?>;
			}
			if($("#txtSSpeed_"+var1).val() == 3)
			{
				var ballast = <?php echo $bes2;?>;
				var fo_ballast = <?php echo $fo_bes2;?>;
				var do_ballast = <?php echo $do_bes2;?>;
			}
			
			var calc = (parseFloat(distance) /  parseFloat(ballast))/24;
			
			if($("#txtADays_"+var1).val() == ""){var actual_days = 0;}else{var actual_days = $("#txtADays_"+var1).val();}
			if(parseFloat(actual_days) == 0)
			{
				if($("#txtMargin_"+var1).val() == ""){var o_margin = 0;}else{var o_margin = $("#txtMargin_"+var1).val();}
				
				var old_ttl = parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(o_margin))/100);
				
				var old_bdays = parseFloat($("#txtBDays").val()) - (old_ttl.toFixed(2));
				$("#txtBDays").val(old_bdays.toFixed(2));
				
				$("#txtBDays").val($("#txtBDays,#txtActualDays").sum().toFixed(2));
				
				$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
				$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
				
				var old_consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(old_ttl.toFixed(2)) *  parseFloat(fo_ballast));
				$("#txtTTLFoConsp").val(old_consp_fo.toFixed(2));
				
				var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(parseFloat($("#txtActualDays").val()) *  parseFloat(fo_ballast));
				$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));					
				
				var old_consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(old_ttl.toFixed(2)) *  parseFloat(do_ballast));
				$("#txtTTLDoConsp").val(old_consp_do.toFixed(2));
				
				var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(parseFloat($("#txtActualDays").val()) *  parseFloat(do_ballast));
				$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
				
			}
			if(parseFloat(actual_days) > 0)
			{
				var old_bdays = parseFloat($("#txtBDays").val()) - parseFloat(actual_days);
				$("#txtBDays").val(old_bdays.toFixed(2));
				
				$("#txtBDays").val($("#txtBDays,#txtActualDays").sum().toFixed(2));
				
				$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
				$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
				
				var old_consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(actual_days) *  parseFloat(fo_ballast));
				$("#txtTTLFoConsp").val(old_consp_fo.toFixed(2));
				
				var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(parseFloat($("#txtActualDays").val()) *  parseFloat(fo_ballast));
				$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));					
				
				var old_consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(actual_days) *  parseFloat(do_ballast));
				$("#txtTTLDoConsp").val(old_consp_do.toFixed(2));
				
				var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(parseFloat($("#txtActualDays").val()) *  parseFloat(do_ballast));
				$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
				
			}
		}
		if($("#txtPType_"+var1).val() == 2)
		{
			if($("#txtSSpeed_"+var1).val() == 1)
			{
				var ladan = <?php echo $lfs;?>;
				var fo_ladan = <?php echo $fo_lfs;?>;
				var do_ladan = <?php echo $do_lfs;?>;
			}
			if($("#txtSSpeed_"+var1).val() == 2)
			{
				var ladan = <?php echo $les1;?>;
				var fo_ladan = <?php echo $fo_les1;?>;
				var do_ladan = <?php echo $do_les1;?>;
			}
			if($("#txtSSpeed_"+var1).val() == 3)
			{
				var ladan = <?php echo $les2;?>;
				var fo_ladan = <?php echo $fo_les2;?>;
				var do_ladan = <?php echo $do_les2;?>;
			}
			var calc = (parseFloat(distance) /  parseFloat(ladan))/24;
			if($("#txtADays_"+var1).val() == ""){var actual_days = 0;}else{var actual_days = $("#txtADays_"+var1).val();}
			if(parseFloat(actual_days) == 0)
			{
				if($("#txtMargin_"+var1).val() == ""){var o_margin = 0;}else{var o_margin = $("#txtMargin_"+var1).val();}
				
				var old_ttl = parseFloat(calc) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(o_margin))/100);
				
				var old_ldays = parseFloat($("#txtLDays").val()) - (old_ttl.toFixed(2));
				$("#txtLDays").val(old_ldays.toFixed(2));
				
				$("#txtLDays").val($("#txtLDays,#txtActualDays").sum().toFixed(2));
				
				$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
				$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
				
				var old_consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(old_ttl.toFixed(2)) *  parseFloat(fo_ladan));
				$("#txtTTLFoConsp").val(old_consp_fo.toFixed(2));
				
				var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(parseFloat($("#txtActualDays").val()) *  parseFloat(fo_ladan));
				$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));					
				
				var old_consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(old_ttl.toFixed(2)) *  parseFloat(do_ladan));
				$("#txtTTLDoConsp").val(old_consp_do.toFixed(2));
				
				var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(parseFloat($("#txtActualDays").val()) *  parseFloat(do_ladan));
				$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
				
			}
			if(parseFloat(actual_days) > 0)
			{
				var old_ldays = parseFloat($("#txtLDays").val()) - parseFloat(actual_days);
				$("#txtLDays").val(old_ldays.toFixed(2));
				
				$("#txtLDays").val($("#txtLDays,#txtActualDays").sum().toFixed(2));
				
				$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
				$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
				
				var old_consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(actual_days) *  parseFloat(fo_ladan));
				$("#txtTTLFoConsp").val(old_consp_fo.toFixed(2));
				
				var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(parseFloat($("#txtActualDays").val()) *  parseFloat(fo_ladan));
				$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));					
				
				var old_consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(actual_days) *  parseFloat(do_ladan));
				$("#txtTTLDoConsp").val(old_consp_do.toFixed(2));
				
				var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(parseFloat($("#txtActualDays").val()) *  parseFloat(do_ladan));
				$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
				
			}
		}
	}
	getBunkerCalculation();
}


function getVoyageTime()
{
	if($("#txtMargin").val() == ""){var margin = 0;}else{var margin = $("#txtMargin").val();}
	if($("#txtDistance").val() == ""){var distance = 0;}else{var distance = $("#txtDistance").val();}
	if($("#txtWeather").val() == ""){var speed_adj = 0;}else{var speed_adj = $("#txtWeather").val();}
	if($("#selPType").val() == 1)
	{
		if($("#selSSpeed").val() == 1)
		{
			var ballast = <?php echo $bfs;?>;
			var fo_ballast = <?php echo $fo_bfs;?>;
			var do_ballast = <?php echo $do_bfs;?>;
		}
		if($("#selSSpeed").val() == 2)
		{
			var ballast = <?php echo $bes1;?>;
			var fo_ballast = <?php echo $fo_bes1;?>;
			var do_ballast = <?php echo $do_bes1;?>;
		}
		if($("#selSSpeed").val() == 3)
		{
			var ballast = <?php echo $bes2;?>;
			var fo_ballast = <?php echo $fo_bes2;?>;
			var do_ballast = <?php echo $do_bes2;?>;
		}
		
		var calc = ($("#txtDistance").val() /  (parseFloat(ballast) + parseFloat(speed_adj)))/24;
		
		var ttl_est = parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100);
		$("#txtEstDays").val(ttl_est.toFixed(2));
		
		var voyage_time = parseFloat($("#txtVoyageTime").val()) + parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100);
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		var ttl_days = parseFloat(calc.toFixed(2)) + parseFloat($("#txtBDays").val()) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100);
		$("#txtBDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat((parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(fo_ballast));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat((parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(do_ballast));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat(distance) + parseFloat($("#txtBDist").val());
		$("#txtBDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
	if($("#selPType").val() == 2)
	{
		if($("#selSSpeed").val() == 1)
		{
			var ladan = <?php echo $lfs;?>;
			var fo_ladan = <?php echo $fo_lfs;?>;
			var do_ladan = <?php echo $do_lfs;?>;
		}
		if($("#selSSpeed").val() == 2)
		{
			var ladan = <?php echo $les1;?>;
			var fo_ladan = <?php echo $fo_les1;?>;
			var do_ladan = <?php echo $do_les1;?>;
		}
		if($("#selSSpeed").val() == 3)
		{
			var ladan = <?php echo $les2;?>;
			var fo_ladan = <?php echo $fo_les2;?>;
			var do_ladan = <?php echo $do_les2;?>;
		}
		
		var calc = ($("#txtDistance").val() / (parseFloat(ladan) + parseFloat(speed_adj)) )/24;
		
		var ttl_est = parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100);
		$("#txtEstDays").val(ttl_est.toFixed(2));
		
		var voyage_time = parseFloat($("#txtVoyageTime").val()) + parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100);
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		var ttl_days = parseFloat(calc.toFixed(2)) + parseFloat($("#txtLDays").val()) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100);
		$("#txtLDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat((parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(fo_ladan));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat((parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(do_ladan));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat(distance) + parseFloat($("#txtLDist").val());
		$("#txtLDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
getBunkerCalculation();
}



function getOHDeviationDays()
{
	if($("#txtOHDHrs").val() == ""){var hrs = 0;}else{var hrs = $("#txtOHDHrs").val();}
	var calc = hrs / 24;
	$("#txtOHDDays").val(calc.toFixed(2));
	
	var actual = parseFloat($("#txtDDays,#txtMarginDays").sum()) - parseFloat($("#txtOHDDays,#txtOHIDays").sum());
	$("#txtActualDays").val(actual.toFixed(2));
}




function getOHIdleDays()
{
	if($("#txtOHIHrs").val() == ""){var hrs = 0;}else{var hrs = $("#txtOHIHrs").val();}
	var calc = hrs / 24;
	$("#txtOHIDays").val(calc.toFixed(2));
	
	var actual = parseFloat($("#txtDDays,#txtMarginDays").sum()) - parseFloat($("#txtOHDDays,#txtOHIDays").sum());
	$("#txtActualDays").val(actual.toFixed(2));
}



function getMarginDays()
{
	if($("#txtMarginHrs").val() == ""){var hrs = 0;}else{var hrs = $("#txtMarginHrs").val();}
	var calc = hrs / 24;
	$("#txtMarginDays").val(calc.toFixed(2));
	
	var actual = parseFloat($("#txtDDays,#txtMarginDays").sum()) - parseFloat($("#txtOHDDays,#txtOHIDays").sum());
	$("#txtActualDays").val(actual.toFixed(2));
}


function getActualLPShow(var1)
{
	if($("#txtLPID").val() == "")
	{
		$("[id^=LPEstRow]").hide();
		$("[id^=LPActRow]").show();
		$("#txtIdleFromDate").val($("#txtLPIFromDate_"+var1).val());
		$("#txtIdleToDate").val($("#txtLPIToDate_"+var1).val());
		$("#txtIdleDays").val($("#txtLPTIDays_"+var1).val());
		$("#txtWrkingFromDate").val($("#txtLPWFromDate_"+var1).val());
		$("#txtwrkingToDate").val($("#txtLPWToDate_"+var1).val());
		$("#txtwrkingDays").val($("#txtLPTWDays_"+var1).val());
		$("#txtLPOHReason").val($("#txtLPOHReason_"+var1).val());
		$("#txtLPOHHrs").val($("#txtLPOHHrs_"+var1).val());
		$("#txtLPOHDays").val($("#txtLPOHDays_"+var1).val());
		$("#txtLPActualDays").val($("#txtLPADays_"+var1).val());
		$("#selPpType").val($("#txtLPCrType_"+var1).val());
		$("#txtLPID").val(var1);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}	
}


function addActualLPShow()
{
	/*if($("#selPpType").val() != "")
	{*/
		var var1 = $("#txtLPID").val();		
		var actual = parseFloat($("#txtIdleDays,#txtwrkingDays").sum()) - parseFloat($("#txtLPOHDays").sum());
		$("#txtLPActualDays").val(actual.toFixed(2));		
		getTotalLoadPort(var1);
		
		$("#txtLPIFromDate_"+var1).val($("#txtIdleFromDate").val());
		$("#txtLPIToDate_"+var1).val($("#txtIdleToDate").val());
		$("#txtLPTIDays_"+var1).val($("#txtIdleDays").val());
		$("#txtLPWFromDate_"+var1).val($("#txtWrkingFromDate").val());
		$("#txtLPWToDate_"+var1).val($("#txtwrkingToDate").val());
		$("#txtLPTWDays_"+var1).val($("#txtwrkingDays").val());
		$("#txtLPOHReason_"+var1).val($("#txtLPOHReason").val());
		$("#txtLPOHHrs_"+var1).val($("#txtLPOHHrs").val());
		$("#txtLPOHDays_"+var1).val($("#txtLPOHDays").val());
		$("#txtLPADays_"+var1).val($("#txtLPActualDays").val());
		//$("#txtLPCrType_"+var1).val($("#selPpType").val());
		$("#lpadaysLabel_"+var1).html($("#txtLPActualDays").val());
		
		$("[id^=LPEstRow]").show();
		$("[id^=LPActRow]").hide();
		$("#txtLPID").val("");
		$("#txtIdleFromDate,#txtIdleToDate,#txtWrkingFromDate,#txtwrkingToDate").removeClass("hasDatepicker");
	/*}
	else
	{
		jAlert('Please select Pump', 'Alert');
		return false;
	}*/
}

function removeActualLPShow()
{
	$("[id^=LPEstRow]").show();
	$("[id^=LPActRow]").hide();
	$("#txtLPID").val("");
}


//..............for Load Port details...............................................
function addLoadPortDetails()
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	
	if($("#selLoadPort").val() != "" && $("#txtQMT").val() != "" && $("#txtRate").val() != "" && $("#selLPCName").val() != "" && $("#selLPTerms").val() != "")
	{
		if(cargo_qty >= parseFloat($("[id^=txtLpQMT_],#txtQMT").sum()))
		{
			var id = $("#load_portID").val();
			id = (id - 1) + 2;
			$("#LProw_Empty").remove();
			var load_port 		= document.getElementById("selLoadPort").selectedIndex;
			var load_port_text  = document.getElementById("selLoadPort").options[load_port].text;
			
			var cargo 			= document.getElementById("selLPCName").selectedIndex;
			var cargo_text 		= document.getElementById("selLPCName").options[cargo].text;
			
			var row_id = $("#txtRow_id").val(); 
			if(row_id == 0)
			{		
			    var IDLE_FROM_DATE = IDLE_TO_DATE = IDLE_DAYS = WRKING_FROM_DATE = WRKING_TO_DATE = WRKING_DAYS = REASON_OFF_HIRE = OFF_HIRE_HRS = OFF_HIRE_DAYS =  ACTUAL_DAYS = PUMPS = LPTERM = "";
			    var actual_lp = <?php echo $obj->getActualLoadPortDataJson($obj->getFun1());?>;
				if(actual_lp!=null)
				{
				  $.each(actual_lp, function(index, array) {
				      if(array['LOADPORTID'] == $("#selLoadPort").val())
					     {
							IDLE_FROM_DATE   = array['IDLE_FROM_DATE'];
							IDLE_TO_DATE 	 = array['IDLE_TO_DATE'];
							IDLE_DAYS 		 = array['IDLE_DAYS'];
							WRKING_FROM_DATE = array['WRKING_FROM_DATE'];
							WRKING_TO_DATE   = array['WRKING_TO_DATE'];
							WRKING_DAYS 	 = array['WRKING_DAYS'];
							REASON_OFF_HIRE  = array['REASON_OFF_HIRE'];
							OFF_HIRE_HRS 	 = array['OFF_HIRE_HRS'];
							OFF_HIRE_DAYS 	 = array['OFF_HIRE_DAYS'];
							ACTUAL_DAYS 	 = array['ACTUAL_DAYS'];
							PUMPS 	 		 = array['PUMPS'];
							LPTERM 	 		 = array['LPTERM'];
						 }
					  });
				 }
					if(ACTUAL_DAYS=="" && WRKING_DAYS=="" && IDLE_DAYS=="")	
					{IDLE_FROM_DATE = IDLE_TO_DATE = WRKING_FROM_DATE = WRKING_TO_DATE = REASON_OFF_HIRE = PUMPS = LPTERM = ""; IDLE_DAYS = WRKING_DAYS = OFF_HIRE_HRS = OFF_HIRE_DAYS = ACTUAL_DAYS = 0;}	
				$('<tr id="lp_Row_'+id+'"><td><a href="#lp'+id+'" onclick="removeLoadPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><a href="#Edit'+id+'" onclick="getActualLPShow('+id+');" ><i class="fa fa-edit" style="color:#1b77a6;"></i></a></td><td>'+load_port_text+'<input type="hidden" name="txtLoadPort_'+id+'" id="txtLoadPort_'+id+'" value="'+$("#selLoadPort").val()+'"/></td><td>'+cargo_text+'<input type="hidden" name="txtLPCID_'+id+'" id="txtLPCID_'+id+'" value="'+$("#selLPCName").val()+'"/></td><td>'+$("#txtQMT").val()+'<input type="hidden" name="txtLpQMT_'+id+'" id="txtLpQMT_'+id+'" value="'+$("#txtQMT").val()+'"/></td><td>'+$("#txtRate").val()+'<input type="hidden" name="txtLPRate_'+id+'" id="txtLPRate_'+id+'" value="'+$("#txtRate").val()+'"/></td><td>'+$("#txtPCosts").val()+'<input type="hidden" name="txtPCosts_'+id+'" id="txtPCosts_'+id+'" value="'+$("#txtPCosts").val()+'"/></td><td>'+$("#txtIDays").val()+'<input type="hidden" name="txtLPIDays_'+id+'" id="txtLPIDays_'+id+'" value="'+$("#txtIDays").val()+'"/></td><td>'+$("#txtWDays").val()+'<input type="hidden" name="txtLPWDays_'+id+'" id="txtLPWDays_'+id+'" value="'+$("#txtWDays").val()+'"/><input type="hidden" name="txtLPCType_'+id+'" id="txtLPCType_'+id+'" value="0"/></td><td>'+(parseFloat($("#txtIDays").val())+parseFloat($("#txtWDays").val()))+'<input type="hidden" name="txtLPEDays_'+id+'" id="txtLPEDays_'+id+'" value="'+(parseFloat($("#txtIDays").val())+parseFloat($("#txtWDays").val()))+'"/></td><td><span id="lpadaysLabel_'+id+'">'+ACTUAL_DAYS+'</span><input type="hidden" name="txtLPIFromDate_'+id+'" id="txtLPIFromDate_'+id+'" value="'+IDLE_FROM_DATE+'"/><input type="hidden" name="txtLPIToDate_'+id+'" id="txtLPIToDate_'+id+'" value="'+IDLE_TO_DATE+'"/><input type="hidden" name="txtLPTIDays_'+id+'" id="txtLPTIDays_'+id+'" value="'+IDLE_DAYS+'"/><input type="hidden" name="txtLPWFromDate_'+id+'" id="txtLPWFromDate_'+id+'" value="'+WRKING_FROM_DATE+'"/><input type="hidden" name="txtLPWToDate_'+id+'" id="txtLPWToDate_'+id+'" value="'+WRKING_TO_DATE+'"/><input type="hidden" name="txtLPTWDays_'+id+'" id="txtLPTWDays_'+id+'" value="'+WRKING_DAYS+'"/><input type="hidden" name="txtLPOHReason_'+id+'" id="txtLPOHReason_'+id+'" value="'+REASON_OFF_HIRE+'"/><input type="hidden" name="txtLPOHHrs_'+id+'" id="txtLPOHHrs_'+id+'" value="'+OFF_HIRE_HRS+'"/><input type="hidden" name="txtLPOHDays_'+id+'" id="txtLPOHDays_'+id+'" value="'+OFF_HIRE_DAYS+'"/><input type="hidden" name="txtLPADays_'+id+'" id="txtLPADays_'+id+'" value="'+ACTUAL_DAYS+'"/><input type="hidden" name="txtLPCrType_'+id+'" id="txtLPCrType_'+id+'" value="'+PUMPS+'"/><input type="hidden" name="txtLPTermsVal_'+id+'" id="txtLPTermsVal_'+id+'" class="form-control" value="'+LPTERM+'"/></td></tr>').appendTo("#tblLoadPort");
			}
			else
			{
				var IDLE_FROM_DATE = IDLE_TO_DATE = IDLE_DAYS = WRKING_FROM_DATE = WRKING_TO_DATE = WRKING_DAYS = REASON_OFF_HIRE = OFF_HIRE_HRS = OFF_HIRE_DAYS =  ACTUAL_DAYS = PUMPS = LPTERM = "";
			    var actual_lp = <?php echo $obj->getActualLoadPortDataJson($obj->getFun1());?>;
				if(actual_lp!=null)
				{
				 $.each(actual_lp, function(index, array) {
				      if(array['LOADPORTID'] == $("#selLoadPort").val())
					     {
							IDLE_FROM_DATE   = array['IDLE_FROM_DATE'];
							IDLE_TO_DATE 	 = array['IDLE_TO_DATE'];
							IDLE_DAYS 		 = array['IDLE_DAYS'];
							WRKING_FROM_DATE = array['WRKING_FROM_DATE'];
							WRKING_TO_DATE   = array['WRKING_TO_DATE'];
							WRKING_DAYS 	 = array['WRKING_DAYS'];
							REASON_OFF_HIRE  = array['REASON_OFF_HIRE'];
							OFF_HIRE_HRS 	 = array['OFF_HIRE_HRS'];
							OFF_HIRE_DAYS 	 = array['OFF_HIRE_DAYS'];
							ACTUAL_DAYS 	 = array['ACTUAL_DAYS'];
							PUMPS 	 		 = array['PUMPS'];
							LPTERM 	 		 = array['LPTERM'];
						 }
					  });
					}
					if(ACTUAL_DAYS=="" && WRKING_DAYS=="" && IDLE_DAYS=="")	
					{IDLE_FROM_DATE = IDLE_TO_DATE = WRKING_FROM_DATE = WRKING_TO_DATE = REASON_OFF_HIRE = PUMPS = LPTERM = ""; IDLE_DAYS = WRKING_DAYS = OFF_HIRE_HRS = OFF_HIRE_DAYS = ACTUAL_DAYS = 0;}	
				$('<tr id="lp_Row_'+id+'"><td><a href="#lp'+id+'" onclick="removeLoadPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td><a href="#Edit'+id+'" onclick="getActualLPShow('+id+');"><i class="fa fa-edit" style="color:#1b77a6;"></i></a></td><td>'+load_port_text+'<input type="hidden" name="txtLoadPort_'+id+'" id="txtLoadPort_'+id+'" value="'+$("#selLoadPort").val()+'"/></td><td>'+cargo_text+'<input type="hidden" name="txtLPCID_'+id+'" id="txtLPCID_'+id+'" value="'+$("#selLPCName").val()+'"/></td><td>'+$("#txtQMT").val()+'<input type="hidden" name="txtLpQMT_'+id+'" id="txtLpQMT_'+id+'" value="'+$("#txtQMT").val()+'"/></td><td>'+$("#txtRate").val()+'<input type="hidden" name="txtLPRate_'+id+'" id="txtLPRate_'+id+'" value="'+$("#txtRate").val()+'"/></td><td>'+$("#txtPCosts").val()+'<input type="hidden" name="txtPCosts_'+id+'" id="txtPCosts_'+id+'" value="'+$("#txtPCosts").val()+'"/></td><td>'+$("#txtIDays").val()+'<input type="hidden" name="txtLPIDays_'+id+'" id="txtLPIDays_'+id+'" value="'+$("#txtIDays").val()+'"/></td><td>'+$("#txtWDays").val()+'<input type="hidden" name="txtLPWDays_'+id+'" id="txtLPWDays_'+id+'" value="'+$("#txtWDays").val()+'"/><input type="hidden" name="txtLPCType_'+id+'" id="txtLPCType_'+id+'" value="0"/></td><td>'+(parseFloat($("#txtIDays").val())+parseFloat($("#txtWDays").val()))+'<input type="hidden" name="txtLPEDays_'+id+'" id="txtLPEDays_'+id+'" value="'+(parseFloat($("#txtIDays").val())+parseFloat($("#txtWDays").val()))+'"/></td><td><span id="lpadaysLabel_'+id+'">'+ACTUAL_DAYS+'</span><input type="hidden" name="txtLPIFromDate_'+id+'" id="txtLPIFromDate_'+id+'" value="'+IDLE_FROM_DATE+'"/><input type="hidden" name="txtLPIToDate_'+id+'" id="txtLPIToDate_'+id+'" value="'+IDLE_TO_DATE+'"/><input type="hidden" name="txtLPTIDays_'+id+'" id="txtLPTIDays_'+id+'" value="'+IDLE_DAYS+'"/><input type="hidden" name="txtLPWFromDate_'+id+'" id="txtLPWFromDate_'+id+'" value="'+WRKING_FROM_DATE+'"/><input type="hidden" name="txtLPWToDate_'+id+'" id="txtLPWToDate_'+id+'" value="'+WRKING_TO_DATE+'"/><input type="hidden" name="txtLPTWDays_'+id+'" id="txtLPTWDays_'+id+'" value="'+WRKING_DAYS+'"/><input type="hidden" name="txtLPOHReason_'+id+'" id="txtLPOHReason_'+id+'" value="'+REASON_OFF_HIRE+'"/><input type="hidden" name="txtLPOHHrs_'+id+'" id="txtLPOHHrs_'+id+'" value="'+OFF_HIRE_HRS+'"/><input type="hidden" name="txtLPOHDays_'+id+'" id="txtLPOHDays_'+id+'" value="'+OFF_HIRE_DAYS+'"/><input type="hidden" name="txtLPADays_'+id+'" id="txtLPADays_'+id+'" value="'+ACTUAL_DAYS+'"/><input type="hidden" name="txtLPCrType_'+id+'" id="txtLPCrType_'+id+'" value="'+PUMPS+'"/><input type="hidden" name="txtLPTermsVal_'+id+'" id="txtLPTermsVal_'+id+'" class="form-control" value="'+LPTERM+'"/></td></tr>').appendTo("#tblLoadPort");
			}
			
			getTTLVoyageDays();
			if($("#txtPCosts").val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtPCosts").val();}
			
			if($("#rdoQtyType1").is(":checked"))
			{if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}}
			if($("#rdoQtyType2").is(":checked"))
			{if($("#txtTotalFreightQty").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtTotalFreightQty").val();}}
			
			var lpcostMT = parseFloat(lp_cost) / parseFloat(c_qty);
			var lp = "'LP'";	
				
			$('<tr id="oscLProw_'+id+'"><td>Load Port   '+load_port_text+'</td><td></td><td><input type="text" class="form-control" name="txtLPOSCCost_'+id+'" id="txtLPOSCCost_'+id+'" onkeyup="getPortCostSum('+id+','+lp+');" readonly="true" value="'+$("#txtPCosts").val()+'" /></td><td><select  name="selLPOSCCost_'+id+'" class="form-control select" id="selLPOSCCost_'+id+'" ><?php $obj->getVendorListNew();?></select></td><td><input type="text" name="txtLPOSCCostMT_'+id+'" id="txtLPOSCCostMT_'+id+'" class="form-control" readonly="true" value="'+lpcostMT.toFixed(2)+'" /></td></tr><tr id="oscLProw1_'+id+'" ><td></td></tr>').appendTo("#tbPortCosts");
		
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat(c_qty);
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
			$('<tr id="ddswLProw_'+id+'"><td>Load Port   '+load_port_text+'</td><td><input type="text" name="txtDDSOLPComm_'+id+'" id="txtDDSOLPComm_'+id+'" class="form-control" autocomplete="off" value="0.00" onkeyup="getFinalCalculation();" placeholder="0.00"></td><td><input type="text"  name="txtEstDDSOLPCost_'+id+'" id="txtEstDDSOLPCost_'+id+'" class="form-control" value="0.00" onKeyUp="getFinalCalculation();" /></td><td><input type="text"  name="txtDDSODPCost_'+id+'" id="txtDDSODPCost_'+id+'" class="form-control" readonly="true" value="0.00"/></td><td><select  name="selFGFVSOListLPortC_'+id+'" class="form-control select" id="selFGFVSOListLPortC_'+id+'"><?php $obj->getVendorListNew();?></select></td><td><input type="text" name="txtDDSOLPNetCostMT_'+id+'" id="txtDDSOLPNetCostMT_'+id+'" class="form-control" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDSW");
			
			$("#txtTtPIDays").val($("#txtTtPIDays,#txtIDays").sum().toFixed(2));
			if($("#txtLPADays_"+id).val()> 0)
			{
			$("#txtTtPWDays").val($("#txtTtPWDays,#txtLPADays_"+id).sum().toFixed(2));
			}
			else
			{
			$("#txtTtPWDays").val($("#txtTtPWDays,#txtWDays").sum().toFixed(2));
			}
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			$("[id^=txtDDSLPOSCCost_],[id^=txtDDNRLPOSCCost_],[id^=txtEstDDSOLPCost_]").numeric();
			$("#load_portID").val(id);
			$("#txtQMT,#txtRate,#selLoadPort,#txtPCosts,#txtIDays,#txtWDays,#txtWDays1,#txtLPDraftM,#selLPCName,#selCrType,#selLPTerms").val("");
			getLPRemoveDaysAttr();
		}
		else
		{
			jAlert('Loaded quantity is greater than Cargo Qty.', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Load Port', 'Alert');
	}
	getBunkerCalculation();
	getFinalCalculation();
}


function getTTLVoyageDays()
{
	if($("#txtIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtIDays").val();}
	if($("#txtWDays").val() == ""){var w_days = 0;}else{var w_days = $("#txtWDays").val();}
	
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
	var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
	
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	getBunkerCalculation();
}


function getLPOHDays()
{
	if($("#txtLPOHHrs").val() == ""){var hrs = 0;}else{var hrs = $("#txtLPOHHrs").val();}
	var calc = hrs / 24;
	$("#txtLPOHDays").val(calc.toFixed(2));
	
	var actual = parseFloat($("#txtIdleDays,#txtwrkingDays").sum()) - parseFloat($("#txtLPOHDays").sum());
	$("#txtLPActualDays").val(actual.toFixed(2));
}


function getLOadPortQty()
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtTotalFreightQty").val();}}
	
	if($("#selLPCName").val() != "")
	{
		if(c_qty != "")
		{
			$("#txtQMT").val(parseFloat(c_qty) - parseFloat($("[id^=txtLpQMT_]").sum()));
			getLoadPortCalculation();
		}
		else
		{
			$("#txtQMT").val(0);
		}
	}
	else
	{
		$("#txtQMT,#txtRate,#txtWDays,#txtWDays1").val("");
	}
}


function getLoadPortCalculation()
{
	if($("#txtQMT").val() != "" && $("#txtRate").val() != "") 
	{
		var value = 0;
		if($("#selLPTerms").val() == 1)
		{
			value = ($("#txtQMT").val() / $("#txtRate").val());
		}
		else if($("#selLPTerms").val() == 2)
		{
			value = parseFloat($("#txtQMT").val() / $("#txtRate").val()) * 1.262 ;
		}
		else if($("#selLPTerms").val() == 3)
		{
			value = parseFloat($("#txtQMT").val() / $("#txtRate").val()) * 1.405 ;
		}
		else
		{
			value = parseFloat($("#txtQMT").val() / $("#txtRate").val());
		}
		
		if($("#selLPTerms").val() != 4)
		{
		    $("#txtWDays").val(value.toFixed(2));
		}
	}
	else
	{
		$("#txtWDays").val('0.00');
	}
}


function getTotalDischargePort(var1)
{
	if($("#txtDPActualDays").val() == 0)
	{
		if($("#txtDPADays_"+var1).val() > 0)
		{
			var ttl_pi_days1 = parseFloat($("#txtTtPIDays").sum().toFixed(2)) - parseFloat($("#txtDPTIDays_"+var1).val());
			var ttl_pi_days = parseFloat(ttl_pi_days1) + parseFloat($("#txtDPIDays_"+var1).val());
			$("#txtTtPIDays").val(ttl_pi_days.toFixed(2));
			
			var ttl_pw_days1 = parseFloat($("#txtTtPWDays").sum().toFixed(2)) - parseFloat($("#txtDPTWDays_"+var1).val());
			var ttl_pw_days = parseFloat(ttl_pw_days1) + parseFloat($("#txtDPWDays_"+var1).val());
			$("#txtTtPWDays").val(ttl_pw_days.toFixed(2));
			
			var lp_ttl_days = parseFloat($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			$("#txtTDays").val(lp_ttl_days.toFixed(2));
			
			var margin_fo = parseFloat($("#txtDPIDays_"+var1).val()) * <?php echo $foidle;?>;
			var margin_fo1 = parseFloat($("#txtDPTIDays_"+var1).val()) * <?php echo $foidle;?>;
			
			if($("#txtDPCType_"+var1).val() == 0)
			{
				var wrkingdays_fo = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtDPCType_"+var1).val() == 1)
			{
				var wrkingdays_fo = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtDPCType_"+var1).val() == 2)
			{
				var wrkingdays_fo = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $foidle;?>;
			}			
			
			
			/*if($("#txtDPCrType_"+var1).val() == 1)
			{
				var wrkingdays_fo1 = parseFloat($("#txtDPTWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtDPCrType_"+var1).val() == 2)
			{*/
				var wrkingdays_fo1 = parseFloat($("#txtDPTWDays_"+var1).val()) * <?php echo $foidle;?>;
			/*}*/
			
			var consp_fo =  (parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo) + parseFloat(wrkingdays_fo)) - (parseFloat(margin_fo1) + parseFloat(wrkingdays_fo1));
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat($("#txtDPIDays_"+var1).val()) * <?php echo $doidle;?>;
			var margin_do1 = parseFloat($("#txtDPTIDays_"+var1).val()) * <?php echo $doidle;?>;
			
			if($("#txtDPCType_"+var1).val() == 0)
			{
				var wrkingdays_do = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtDPCType_"+var1).val() == 1)
			{
				var wrkingdays_do = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtDPCType_"+var1).val() == 2)
			{
				var wrkingdays_do = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $doidle;?>;
			}	
			
			
			/*if($("#txtDPCrType_"+var1).val() == 1)
			{
				var wrkingdays_do1 = parseFloat($("#txtDPTWDays_"+var1).val()) * <?php echo $dowrking;?>;	
			}
			else if($("#txtDPCrType_"+var1).val() == 2)
			{*/
				var wrkingdays_do1 = parseFloat($("#txtDPTWDays_"+var1).val()) * <?php echo $doidle;?>;	
			/*}*/		
					
			var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do) + parseFloat(wrkingdays_do))  - (parseFloat(margin_do1) + parseFloat(wrkingdays_do1));
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
		}
	}
	if($("#txtDPActualDays").val() > 0)
	{
		if($("#txtDPADays_"+var1).val() == 0)
		{
			var ttl_pi_days1 = parseFloat($("#txtTtPIDays").sum().toFixed(2)) - parseFloat($("#txtDPIDays_"+var1).val());
			var ttl_pi_days = parseFloat(ttl_pi_days1) + parseFloat($("#txtDPIdleDays").val());
			$("#txtTtPIDays").val(ttl_pi_days.toFixed(2));
			
			var ttl_pw_days1 = parseFloat($("#txtTtPWDays").sum().toFixed(2)) - parseFloat($("#txtDPWDays_"+var1).val());
			var ttl_pw_days = parseFloat(ttl_pw_days1) + parseFloat($("#txtDPwrkingDays").val());
			$("#txtTtPWDays").val(ttl_pw_days.toFixed(2));
			
			var lp_ttl_days = parseFloat($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));// - parseFloat($("#txtDPOHDays").val());
			$("#txtTDays").val(lp_ttl_days.toFixed(2));//alert($("#txtTSDays").val());alert($("#txtTtPIDays").val());alert($("#txtTtPWDays").val());
			var margin_fo = parseFloat($("#txtDPIDays_"+var1).val()) * <?php echo $foidle;?>;
			var margin_fo1 = parseFloat($("#txtDPIdleDays").val()) * <?php echo $foidle;?>;
			
			if($("#txtDPCType_"+var1).val() == 0)
			{
				var wrkingdays_fo = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtDPCType_"+var1).val() == 1)
			{
				var wrkingdays_fo = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtDPCType_"+var1).val() == 2)
			{
				var wrkingdays_fo = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $foidle;?>;
			}
			
			/*if($("#selDPPpType").val() == 1)
			{
				var wrkingdays_fo1 = parseFloat($("#txtDPwrkingDays").val()) * <?php echo $fowrking;?>;
			}
			else if($("#selDPPpType").val() == 2)
			{*/
				var wrkingdays_fo1 = parseFloat($("#txtDPwrkingDays").val()) * <?php echo $foidle;?>;
			/*}*/	
			
			var consp_fo =  (parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo1) + parseFloat(wrkingdays_fo1)) - (parseFloat(margin_fo) + parseFloat(wrkingdays_fo));
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat($("#txtDPIDays_"+var1).val()) * <?php echo $doidle;?>;
			var margin_do1 = parseFloat($("#txtDPIdleDays").val()) * <?php echo $doidle;?>;
			
			if($("#txtDPCType_"+var1).val() == 0)
			{
				var wrkingdays_do = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtDPCType_"+var1).val() == 1)
			{
				var wrkingdays_do = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtDPCType_"+var1).val() == 2)
			{
				var wrkingdays_do = parseFloat($("#txtDPWDays_"+var1).val()) * <?php echo $doidle;?>;
			}
			
			/*if($("#selDPPpType").val() == 1)
			{
				var wrkingdays_do1 = parseFloat($("#txtDPwrkingDays").val()) * <?php echo $dowrking;?>;	
			}
			else if($("#selDPPpType").val() == 2)
			{*/
				var wrkingdays_do1 = parseFloat($("#txtDPwrkingDays").val()) * <?php echo $doidle;?>;	
			/*}*/		
					
			var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do1) + parseFloat(wrkingdays_do1))  - (parseFloat(margin_do) + parseFloat(wrkingdays_do));
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
		}
		if($("#txtDPADays_"+var1).val() > 0)
		{
			var ttl_pi_days1 = parseFloat($("#txtTtPIDays").sum().toFixed(2)) - parseFloat($("#txtDPTIDays_"+var1).val());
			var ttl_pi_days = parseFloat(ttl_pi_days1) + parseFloat($("#txtDPIdleDays").val());
			$("#txtTtPIDays").val(ttl_pi_days.toFixed(2));
			
			var ttl_pw_days1 = parseFloat($("#txtTtPWDays").sum().toFixed(2)) - parseFloat($("#txtDPTWDays_"+var1).val());
			var ttl_pw_days = parseFloat(ttl_pw_days1) + parseFloat($("#txtDPwrkingDays").val());
			$("#txtTtPWDays").val(ttl_pw_days.toFixed(2));
			
			var lp_ttl_days = (parseFloat($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2)) + parseFloat($("#txtDPOHDays").val())) - parseFloat($("#txtDPOHDays_"+var1).val());
			$("#txtTDays").val(lp_ttl_days.toFixed(2));
			
			var margin_fo = parseFloat($("#txtDPTIDays_"+var1).val()) * <?php echo $foidle;?>;
			var margin_fo1 = parseFloat($("#txtDPIdleDays").val()) * <?php echo $foidle;?>;
			
			/*if($("#txtDPCrType_"+var1).val() == 1)
			{
				var wrkingdays_fo = parseFloat($("#txtDPTWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtDPCrType_"+var1).val() == 2)
			{*/
				var wrkingdays_fo = parseFloat($("#txtDPTWDays_"+var1).val()) * <?php echo $foidle;?>;
			/*}*/
			
			/*if($("#selDPPpType").val() == 1)
			{
				var wrkingdays_fo1 = parseFloat($("#txtDPwrkingDays").val()) * <?php echo $fowrking;?>;
			}
			else if($("#selDPPpType").val() == 2)
			{*/
				var wrkingdays_fo1 = parseFloat($("#txtDPwrkingDays").val()) * <?php echo $foidle;?>;
			/*}*/
			
			var consp_fo =  (parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo1) + parseFloat(wrkingdays_fo1)) - (parseFloat(margin_fo) + parseFloat(wrkingdays_fo));
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat($("#txtDPTIDays_"+var1).val()) * <?php echo $doidle;?>;
			var margin_do1 = parseFloat($("#txtDPIdleDays").val()) * <?php echo $doidle;?>;
			
			/*if($("#txtDPCrType_"+var1).val() == 1)
			{
				var wrkingdays_do = parseFloat($("#txtDPTWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtDPCrType_"+var1).val() == 2)
			{*/
				var wrkingdays_do = parseFloat($("#txtDPTWDays_"+var1).val()) * <?php echo $doidle;?>;
			/*}*/
			
			/*if($("#selDPPpType").val() == 1)
			{
				var wrkingdays_do1 = parseFloat($("#txtDPwrkingDays").val()) * <?php echo $dowrking;?>;		
			}
			else if($("#selDPPpType").val() == 2)
			{*/
				var wrkingdays_do1 = parseFloat($("#txtDPwrkingDays").val()) * <?php echo $doidle;?>;		
			/*}*/		
					
			var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do1) + parseFloat(wrkingdays_do1))  - (parseFloat(margin_do) + parseFloat(wrkingdays_do));
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
		}
	}
	getBunkerCalculation();
}

function getLoadDisPortvalue()
{


}


function getFinalCalculation()
{
	getQtyVendorDetailsSum();
	
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	
	$("#txtQMT").val(parseFloat(cargo_qty) - parseFloat($("[id^=txtLpQMT_]").sum()));
	$("#txtDQMT").val(parseFloat(cargo_qty) - parseFloat($("[id^=txtDpQMT_]").sum()));
	
	if(parseFloat(cargo_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	
	$('[id^=txtCCAbs_]').each(function(index) {
		var rowid = this.id;
		var lasrvar1 = rowid.split('_').slice(1).join('_');
		
		if(this.value == ""){var cc_abs = 0;}else{var cc_abs = this.value;}
		if($("#txtCCAddComm_"+lasrvar1).val() == ""){var cc_addcomm = 0;}else{var cc_addcomm = $("#txtCCAddComm_"+lasrvar1).val();}
		var cc_value = "";
		cc_value = parseFloat(cc_abs) - parseFloat((parseFloat(cc_abs) * parseFloat(cc_addcomm))/100);
		$("#txtCCValue_"+lasrvar1).val(cc_value.toFixed(2));
		
		var costMT = this.value / parseFloat(per_mt);				
		$("#txtCCostMT_"+lasrvar1).val(costMT.toFixed(2));
	});
	
	/*$('[id^=txtOMCAbs_]').each(function(index) {
		var rowid = this.id;
		var lasrvar1 = rowid.split('_').slice(1).join('_');
		
		if(this.value == ""){var omc_abs = 0;}else{var omc_abs = this.value;}
		if($("#txtOMCAddComm_"+lasrvar1).val() == ""){var omc_addcomm = 0;}else{var omc_addcomm = $("#txtOMCAddComm_"+lasrvar1).val();}
		var omc_value = "";
		omc_value = parseFloat(omc_abs) - parseFloat((parseFloat(omc_abs) * parseFloat(omc_addcomm))/100);
		$("#txtOMCValue_"+lasrvar1).val(omc_value.toFixed(2));
		
		var costMT = this.value / parseFloat(per_mt);				
		$("#txtOMCCostMT_"+lasrvar1).val(costMT.toFixed(2));
	});*/
			
	if($("#txtddswCost").val() == ""){var ddsw_cost = 0;}else{var ddsw_cost = $("#txtddswCost").val();}
	if($("#txtddswAddComm").val() == ""){var ddsw_addcomm = 0;}else{var ddsw_addcomm = $("#txtddswAddComm").val();}
	var ddsw_value = parseFloat(ddsw_cost) - parseFloat((parseFloat(ddsw_cost) * parseFloat(ddsw_addcomm))/100);
	$("#txtddswValue").val(ddsw_value.toFixed(2));


	if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
	if($("#txtWSFR").val() == ""){var ws_flat_rate = 0;}else{var ws_flat_rate = $("#txtWSFR").val();}
	if($("#txtWSFD").val() == ""){var fixed_diff = 0;}else{var fixed_diff = $("#txtWSFD").val();}
	if($("#txtMTCPDRate").val() == ""){var ws_rate = 0;}else{var ws_rate = $("#txtMTCPDRate").val();}
	if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}

	$("[id^=txtMinCargo_]").val(min_qty);$("[id^=txtOverage_]").val(addnl_qty);
	$("[id^=txtMCWSFlatRate_],[id^=txtOvrWSFlatRate_]").val(ws_flat_rate);
	
	$("[id^=txtTotalCargoQty_]").val(cargo_qty);
	
	<!----------------- Quantity  ------------------->
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var per_amt = 0;}else{var per_amt = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var per_amt = 0;}else{var per_amt = $("#txtTotalFreightQty").val();}}
	
	//var per_amt = $("#txtCQMT").val();
	if($("#rdoMMarket1").is(":checked"))
	{
		<!----------------- Agreed Gross Freight  ------------------->
		if($("#txtMTCPDRate").val() == ""){var agr_gross_fr = 0;}else{var agr_gross_fr = $("#txtMTCPDRate").val();}
		//if($("#txtAQMT").val() == ""){var actual_qty = 0;}else{var actual_qty = $("#txtAQMT").val();}
		
		<!----------------- (Freight Adjustment) Gross Freight  ------------------->
		var gross_fr = parseFloat(agr_gross_fr) * parseFloat(per_amt);
		$("#txtFrAdjUsdGF").val(gross_fr.toFixed(2));
		
		<!----------------- (Freight Adjustment) Gross Freight(MT)  ------------------->
		var gross_fr_mt = parseFloat(gross_fr) / parseFloat(per_amt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		
		<!-----------------  DF Qty (MT)  ------------------->
		if($("#txtDFQMT").val() == "" || $("#txtDFQMT").val() == 0)
		{
			var df_qty = 0;
			$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
			
		}
		else
		{
			<!-----------------  Dead Freight(USD)  ------------------->
			var df_qty = $("#txtDFQMT").val();
			var dead_fr = parseFloat(agr_gross_fr) * parseFloat(df_qty);
			$("#txtFrAdjUsdDF").val(dead_fr.toFixed(2));
			
			<!-----------------  Dead Freight(MT)  ------------------->
			var dead_fr_mt = parseFloat(dead_fr) / parseFloat(df_qty);
			$("#txtFrAdjUsdDFMT").val(dead_fr_mt.toFixed(2));
		}
		
		<!-----------------  Addnl Cargo Rate (USD/MT)  ------------------->
		
		if($("#txtAddnlCRate").val() == ""){var addnl_rate = 0;}else{var addnl_rate = $("#txtAddnlCRate").val();}
		//if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
		
		
		if($("#txtAddnlQMT").val() == "" || $("#txtAddnlQMT").val() == 0 )
		{
			var addnl_qty = 0;
			$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		}
		else
		{
			<!-----------------  Addnl Qty (MT)  ------------------->
			var addnl_qty = $("#txtAddnlQMT").val();
			
			<!-----------------(Freight Adjustment)  Addnl Freight(USD)  ------------------->
			var addnl_fr = parseFloat(addnl_rate) * parseFloat(addnl_qty);
			$("#txtFrAdjUsdAF").val(addnl_fr.toFixed(2));
			
			<!-----------------(Freight Adjustment)  Addnl Freight(MT)  ------------------->
			var addnl_fr_mt = parseFloat(addnl_fr) / parseFloat(addnl_qty);
			$("#txtFrAdjUsdAFMT").val(addnl_fr_mt.toFixed(2));
		}
		
		<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum().toFixed(2));
		
		<!-----------------(Freight Adjustment)  Total Freight(MT)  ------------------->
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / (parseFloat(per_amt) + parseFloat(df_qty) + parseFloat(addnl_qty));
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
	}
	
	if($("#rdoMMarket2").is(":checked"))
	{
		<!-----------------(Freight Adjustment)  Gross Freight(USD)  ------------------->
		$("#txtFrAdjUsdGF").val($("#txtMLumpsum").val());
		
		<!-----------------(Freight Adjustment)  Gross Freight(MT)  ------------------->
		var gross_fr_mt = parseFloat($("#txtFrAdjUsdGF").val()) / parseFloat(per_amt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		
		$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
		$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		
		<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum());
		
		<!-----------------(Freight Adjustment)  Gross Freight(MT)  ------------------->
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / parseFloat(per_amt);
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
	}
	
	<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtFrAdjUsdTF").val() == ""){var ttl_fr = 0;}else{var ttl_fr = $("#txtFrAdjUsdTF").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{var ttl_fr = parseFloat($("[id^=txtFreightQty1_]").sum()); if(ttl_fr == ""){var ttl_fr = 0;} }
	
	<!-----------------(Freight Adjustment) Address Commission(Percent)  ------------------->
	if($("#txtFrAdjPerAC").val() == ""){var ac_per = 0;}else{var ac_per = $("#txtFrAdjPerAC").val();}
	
	<!-----------------(Freight Adjustment) Address Commission(USD)  ------------------->
	var address_commission = (parseFloat(ttl_fr) * parseFloat(ac_per)) / 100;
	$("#txtFrAdjUsdAC").val(address_commission.toFixed(2));
	
	<!-----------------(Freight Adjustment) Address Commission(MT)  ------------------->
	var address_commission_mt = parseFloat($("#txtFrAdjUsdAC").val()) / parseFloat(per_amt);
	$("#txtFrAdjUsdACMT").val(address_commission_mt.toFixed(2));
	
	<!-----------------(Freight Adjustment) Brokerage(Persent)  ------------------->
	if($("#txtFrAdjPerAgC").val() == ""){var ag_per = 0;}else{var ag_per = $("#txtFrAdjPerAgC").val();}
	var agent_commission = (parseFloat(ttl_fr) * parseFloat(ag_per)) / 100;
	
	<!-----------------(Freight Adjustment) Brokerage(USD)  ------------------->
	$("#txtFrAdjUsdAgC").val(agent_commission.toFixed(2));
	
	<!-----------------(Freight Adjustment) Net Freight Payable(USD)  ------------------->
	var net_fr = parseFloat(ttl_fr) - parseFloat($("#txtFrAdjUsdAgC").val());
	$("#txtFrAdjUsdFP,#txtFGFFD").val(net_fr.toFixed(2));
	
	<!-----------------(Freight Adjustment) Final Net Freight Payable(MT)  ------------------->
	var net_fr_mt = parseFloat(net_fr) / parseFloat(per_amt);
	$("#txtFGFFDMT").val(net_fr_mt.toFixed(2));
	
	<!----------------- Revenue (Final Nett Freight) ------------------->
	if($("#txtFGFFD").val() == ""){var final_nett_frt = 0;}else{var final_nett_frt = $("#txtFGFFD").val();}
	var rev = parseFloat($("[id^=txtOMCValue_]").sum().toFixed(2)) + parseFloat(final_nett_frt);
	$("#txtRevenue").val(rev.toFixed(2));
	
	<!--##########################################-->
	
	/*if($("#txtFrAdjPerACTF").val() == ""){var less_address_comm_tf = 0;}else{var less_address_comm_tf = $("#txtFrAdjPerACTF").val();}
	if($("#txtFrAdjUsdTF").val() == ""){var ttl_freight = 0;}else{var ttl_freight = $("#txtFrAdjUsdTF").val();}
	
	var add_comm_usd_tf = (parseFloat(ttl_freight) * parseFloat(less_address_comm_tf))/100;
	$("#txtFrAdjUsdACTF").val(add_comm_usd_tf.toFixed(2));
	
	
	if($("#txtFrAdjPerACGF").val() == ""){var less_address_comm_gf = 0;}else{var less_address_comm_gf = $("#txtFrAdjPerACGF").val();}
	if($("#txtFrAdjUsdGF").val() == ""){var gross_freight = 0;}else{var gross_freight = $("#txtFrAdjUsdGF").val();}
	
	var add_comm_usd_gf = (parseFloat(gross_freight) * parseFloat(less_address_comm_gf))/100;
	$("#txtFrAdjUsdACGF").val(add_comm_usd_gf.toFixed(2));		
	
	
	if($("#txtFrAdjUsdACTF").val() == ""){var add_comm_usd_tf1 = 0;}else{var add_comm_usd_tf1 = $("#txtFrAdjUsdACTF").val();}
	if($("#txtFrAdjUsdACGF").val() == ""){var add_comm_usd_gf1 = 0;}else{var add_comm_usd_gf1 = $("#txtFrAdjUsdACGF").val();}
	
	
	var balance_freight = parseFloat(gross_freight) - (parseFloat(add_comm_usd_tf1) + parseFloat(add_comm_usd_gf1));
	$("#txtBalanceFreight").val(balance_freight.toFixed(2));
	
	var final_nett_frt = parseFloat(balance_freight.toFixed(2)) + parseFloat($("[id^=txtAdditionAmt_]").sum().toFixed(2)) - parseFloat($("[id^=txtDeductionAmt_]").sum().toFixed(2));
	$("#txtFGFFD").val(final_nett_frt.toFixed(2));
	
	var rev = parseFloat($("[id^=txtOMCValue_]").sum().toFixed(2)) + parseFloat(final_nett_frt.toFixed(2));
	$("#txtRevenue").val(rev.toFixed(2));
	
	if($("#txtFGFFD").val() == ""){var final_nett_freight = 0;}else{var final_nett_freight = $("#txtFGFFD").val();}
	var final_nett_freight_mt = parseFloat(final_nett_freight) / parseFloat(per_mt);
	$("#txtFGFFDMT").val(final_nett_freight_mt.toFixed(2));*/
	
	<!----------------- Brokerage ------------------->
	for(var l = 1;l<=$("#txtBRokageCount").val();l++)
	{
	if($("#txtBrCommPercent_"+l).val() == ""){var brokerage_comm = 0;}else{var brokerage_comm = $("#txtBrCommPercent_"+l).val();}
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtFrAdjUsdTF").val() == ""){var t_frt = 0;}else{var t_frt = $("#txtFrAdjUsdTF").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{var t_frt = parseFloat($("[id^=txtFreightQty1_]").sum()); if(t_frt == ""){var t_frt = 0;} }
	
	var brokerage_comm_usd = (parseFloat(t_frt) * parseFloat(brokerage_comm))/100;
	$("#txtBrComm_"+l).val(brokerage_comm_usd.toFixed(2));
	}
	$("#txtBrCommPercent").val($("[id^=txtBrCommPercent_]").sum().toFixed(2));
	$("#txtBrComm").val($("[id^=txtBrComm_]").sum().toFixed(2));
	
	
	//if($("#txtBrCommPercent").val() == ""){var brokerage_comm = 0;}else{var brokerage_comm = $("#txtBrCommPercent").val();}
	//if($("#txtFrAdjUsdTF").val() == ""){var t_frt = 0;}else{var t_frt = $("#txtFrAdjUsdTF").val();}
	
	//var brokerage_comm_usd = (parseFloat(t_frt) * parseFloat(brokerage_comm))/100;
	//$("#txtBrComm").val(brokerage_comm_usd.toFixed(2));
	
	$("#txtTTLORCAmt").val($("[id^=txtHidORCAmt_],#txtBrComm,#txtFrAdjUsdAC").sum());
	var calc = parseFloat($("#txtTTLORCAmt").val()) / parseFloat(per_mt);
	$("#txtTTLORCCostMT").val(calc.toFixed(2));
	
	$("#txtTTLOwnersExpenses").val($("[id^=txtBunkerCost_],#txtTTLPortCosts,#txtTTLORCAmt").sum().toFixed(2));
	$("#txtTTLCharterersExpenses").val($("[id^=txtCCValue_]").sum().toFixed(2));
	
	if($("#txtRevenue").val() == ""){var revenue = 0;}else{var revenue = $("#txtRevenue").val();}
	if($("#txtTTLOwnersExpenses").val() == ""){var owners_expenses = 0;}else{var owners_expenses = $("#txtTTLOwnersExpenses").val();}
	var costbeforebamarage = parseFloat(revenue) - parseFloat(owners_expenses);
	var voyage_earning = parseFloat(revenue) - parseFloat(owners_expenses) + parseFloat(getDDSOOwnerCalculation());
	$("#txtVoyageEarnings").val(costbeforebamarage.toFixed(2));
	$("#txtGTTLVoyageEarnings").val(voyage_earning.toFixed(2));
	//$("#txtVoyageEarnings").val(voyage_earning.toFixed(2));
	
	/*var g_voyage_earning = parseFloat(voyage_earning.toFixed(2)) + parseFloat($("#txtddswValue").val());
	$("#txtGTTLVoyageEarnings").val(g_voyage_earning.toFixed(2));*/
	
	//-----------------------------------INDRESH-----------------------------------------------
	
	$('[id^=txtddswAddComm_]').each(function(index) {
		var rowid = this.id;
		var lasrvar1 = rowid.split('_').slice(1);
		if($("#txtddswAddComm_"+lasrvar1).val() == ""){var addCom_per = 0;}else{ var addCom_per = $("#txtddswAddComm_"+lasrvar1).val();}
		if($("#txtddswCost_"+lasrvar1).val() == ""){var addCost = 0;}else{ var addCost = $("#txtddswCost_"+lasrvar1).val();}
		var netVal = parseFloat(addCost) - parseFloat(parseFloat(addCost) * parseFloat(parseFloat(addCom_per)/100));
		
		$("#txtddswValue_"+lasrvar1).val(netVal.toFixed(2));		

	});
	$("[id^=txtddswValue_]").sum().toFixed(2)
	$("#txtGTTLVoyageEarnings").val($("[id^=txtddswValue_],#txtVoyageEarnings").sum().toFixed(2));
	
	$("#txtNettDailyEarnings").val($("[id^=txtddswValue_],#txtNettDailyEarnings").sum().toFixed(2));
	$("#txtNettDailyProfit").val($("[id^=txtddswValue_],#txtNettDailyProfit").sum().toFixed(2));
	
	//------------------------------------------------------------------------------------------------
	
	
	/*if($("#txtTDays").val() == 0){var ttl_days = 1;}else{var ttl_days = $("#txtTDays").val();}
	var daily_earnings = parseFloat(g_voyage_earning) / parseFloat(ttl_days);
	$("#txtNettDailyEarnings").val(daily_earnings.toFixed(2));*/
	if($("#txtRevenue").val() == ""){var revenue = 0;}else{var revenue = $("#txtRevenue").val();}
	if($("#txtTTLOwnersExpenses").val() == ""){var owners_expenses = 0;}else{var owners_expenses = $("#txtTTLOwnersExpenses").val();}
	
	var costbeforebamarage = parseFloat(revenue) - parseFloat(owners_expenses);
	var voyage_earning = parseFloat(revenue) - parseFloat(owners_expenses) + parseFloat(getDDSOOwnerCalculation());
	
	$("#txtVoyageEarnings").val(costbeforebamarage.toFixed(2));
	$("#txtGTTLVoyageEarnings").val(voyage_earning.toFixed(2));
	
	if($("#txtTDays").val() == 0){var ttl_days = 1;}else{var ttl_days = $("#txtTDays").val();}
	var daily_earnings = parseFloat(voyage_earning) / parseFloat(ttl_days);
	var costbeforebamarage_erning = parseFloat(costbeforebamarage) / parseFloat(ttl_days);
	$("#txtDailyEarnings").val(costbeforebamarage_erning.toFixed(2));
	$("#txtNettDailyEarnings").val(daily_earnings.toFixed(2));
	
	
	if($("#txtDailyVesselOperatingExpenses").val() == ""){var daily_vessel_exp = 0;}else{var daily_vessel_exp = $("#txtDailyVesselOperatingExpenses").val();}
	
	var hireage_amt = parseFloat(parseFloat(daily_vessel_exp)*parseFloat($("#txtTDays").val())).toFixed(2);
	if(hireage_amt=="" || isNaN(hireage_amt)){hireage_amt =0;}
	$("#txtHireargeAmt").val(parseFloat(hireage_amt).toFixed(2));
	if($("#txtHireargePercent").val() == ""){var txtHireargePercent = 0;}else{var txtHireargePercent = $("#txtHireargePercent").val();}
	var txtHireargePercentAmt = parseFloat(parseFloat(parseFloat(hireage_amt)*parseFloat(txtHireargePercent))/100);
	$("#txtHireargePercentAmt").val(parseFloat(txtHireargePercentAmt).toFixed(2));
	
	var txtNettHireargeAmt = parseFloat(parseFloat(hireage_amt) -  parseFloat(txtHireargePercentAmt));
	$("#txtNettHireargeAmt").val(parseFloat(txtNettHireargeAmt).toFixed(2));	
	
	var nett_daily_profit = parseFloat(parseFloat(parseFloat($("#txtGTTLVoyageEarnings").val()) - parseFloat(txtNettHireargeAmt))/parseFloat($("#txtTDays").val()));
	$("#txtNettDailyProfit").val(parseFloat(nett_daily_profit).toFixed(2));
	
	$("#txtPL").val( parseFloat(parseFloat($("#txtNettDailyProfit").val()) *  parseFloat($("#txtTDays").val())).toFixed(2));	
	
	$('[id^=txtBunkerCost_]').each(function(index) {
		var rowid = this.id;
		var lasrvar1 = rowid.split('_').slice(1).join('_');
		var costMT = this.value / parseFloat(per_mt);
		$("#txtBunkerCostMT_"+lasrvar1).val(costMT.toFixed(2));
	});
	$('[id^=txtLPOSCCost_]').each(function(index) {
		var rowid = this.id;
		var lasrvar1 = rowid.split('_').slice(1).join('_');
		var costMT = this.value / parseFloat(per_mt);
		$("#txtLPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
	});
	$('[id^=txtDPOSCCost_]').each(function(index) {
		var rowid = this.id;
		var lasrvar1 = rowid.split('_').slice(1).join('_');
		var costMT = this.value / parseFloat(per_mt);
		$("#txtDPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
	});
	$('[id^=txtTPOSCCost_]').each(function(index) {
		var rowid = this.id;
		var lasrvar1 = rowid.split('_').slice(1).join('_');
		var costMT = this.value / parseFloat(per_mt);
		$("#txtTPOSCCostMT_"+lasrvar1).val(costMT.toFixed(2));
	});
}


function getDDSOOwnerCalculation(){
	
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	
	$('[id^=txtDDSOLPCost_]').each(function(index) {
		var rowid       = this.id;
		var ddswLP1     = rowid.split('_')[1];
		if($("#txtLPLayTimeIs_"+ddswLP1).val()==0)
		{
		$("#txtDDSOLPCost_"+ddswLP1).val($("#txtEstDDSOLPCost_"+ddswLP1).val());
		}
		if($("#txtDDSOLPComm_"+ddswLP1).val() == ""){var addLPcomm = 0;}else{var addLPcomm = $("#txtDDSOLPComm_"+ddswLP1).val();}
		if($("#txtDDSOLPCost_"+ddswLP1).val() == ""){var addLPcost = 0;}else{var addLPcost = $("#txtDDSOLPCost_"+ddswLP1).val();}
		var remLpAmt  = parseFloat(addLPcost) - parseFloat(parseFloat(parseFloat(addLPcost)*parseFloat(addLPcomm))/100);
		$('#txtDDSOLPNetCostMT_'+ddswLP1).val(remLpAmt.toFixed(2));
		var ddswLPCCost = parseFloat(remLpAmt)/parseFloat(cargo_qty);
		$('#txtDDSOLPCostMT_'+ddswLP1).val(ddswLPCCost.toFixed(2));
	});
	$('[id^=txtDDSODPCost_]').each(function(index) {
		var rowid       = this.id;
		var ddswDP1     = rowid.split('_')[1];//alert($("#txtDPLayTimeIs_"+ddswDP1).val());
		if($("#txtDPLayTimeIs_"+ddswDP1).val()==0)
		{
		$("#txtDDSODPCost_"+ddswDP1).val($("#txtEstDDSODPCost_"+ddswDP1).val());
		}
		if($("#txtDDSODPComm_"+ddswDP1).val() == ""){var addDPcomm = 0;}else{var addDPcomm = $("#txtDDSODPComm_"+ddswDP1).val();}
		if($("#txtDDSODPCost_"+ddswDP1).val() == ""){var addDPcost = 0;}else{var addDPcost = $("#txtDDSODPCost_"+ddswDP1).val();}
		var remDpAmt  = parseFloat(addDPcost) - parseFloat(parseFloat(parseFloat(addDPcost)*parseFloat(addDPcomm))/100);
		$('#txtDDSODPNetCostMT_'+ddswDP1).val(remDpAmt.toFixed(2));
		var ddswDPCCost = parseFloat(remDpAmt)/parseFloat(cargo_qty);
		$('#txtDDSODPCostMT_'+ddswDP1).val(ddswDPCCost.toFixed(2));
	});
	var LPDPttl = parseFloat($('[id^=txtDDSOLPNetCostMT_]').sum().toFixed(2)) + parseFloat($('[id^=txtDDSODPNetCostMT_]').sum().toFixed(2));
	return LPDPttl;
}

/*****************************************************/
function getDDSOwnerCalculation(){
	
	$('[id^=txtddswLPCost_]').each(function(index) {
		var rowid       = this.id;
		var ddswLP1     = rowid.split('_')[1];
		var ddswLPCCost = $('#txtddswLPCost_'+ddswLP1).val();
		$('#txtddswLPCostMT_'+ddswLP1).val(ddswLPCCost);
	});
	$('[id^=txtddswDPCost_]').each(function(index) {
		var rowid       = this.id;
		var ddswLP1     = rowid.split('_')[1];
		var ddswLPCCost = $('#txtddswDPCost_'+ddswLP1).val();
		$('#txtddswDPCostMT_'+ddswLP1).val(ddswLPCCost);
	});
	var LPDPttl = parseFloat($('[id^=txtddswDPCostMT_]').sum().toFixed(2)) + parseFloat($('[id^=txtddswLPCostMT_]').sum().toFixed(2));
	return LPDPttl;
}


function getTotalLoadPort(var1)
{
	if($("#txtLPActualDays").val() == 0)
	{
		if($("#txtLPADays_"+var1).val() > 0)
		{
			var ttl_pi_days1 = parseFloat($("#txtTtPIDays").sum().toFixed(2)) - parseFloat($("#txtLPTIDays_"+var1).val());
			var ttl_pi_days  = parseFloat(ttl_pi_days1) + parseFloat($("#txtLPIDays_"+var1).val());
			$("#txtTtPIDays").val(ttl_pi_days.toFixed(2));
			
			var ttl_pw_days1 = parseFloat($("#txtTtPWDays").sum().toFixed(2)) - parseFloat($("#txtLPTWDays_"+var1).val());
			var ttl_pw_days = parseFloat(ttl_pw_days1) + parseFloat($("#txtLPWDays_"+var1).val());
			$("#txtTtPWDays").val(ttl_pw_days.toFixed(2));
			
			var lp_ttl_days = parseFloat($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			$("#txtTDays").val(lp_ttl_days.toFixed(2));
			
			var margin_fo = parseFloat($("#txtLPIDays_"+var1).val()) * <?php echo $foidle;?>;
			var margin_fo1 = parseFloat($("#txtLPTIDays_"+var1).val()) * <?php echo $foidle;?>;
			
			if($("#txtLPCType_"+var1).val() == 0)
			{
				var wrkingdays_fo = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 1)
			{
				var wrkingdays_fo = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 2)
			{
				var wrkingdays_fo = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $foidle;?>;
			}
			
			
			/*if($("#txtLPCrType_"+var1).val() == 1)
			{
				var wrkingdays_fo1 = parseFloat($("#txtLPTWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtLPCrType_"+var1).val() == 2)
			{*/
				var wrkingdays_fo1 = parseFloat($("#txtLPTWDays_"+var1).val()) * <?php echo $foidle;?>;
		/*	}*/
			
			var consp_fo = (parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo) + parseFloat(wrkingdays_fo)) - (parseFloat(margin_fo1) + parseFloat(wrkingdays_fo1));
			
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat($("#txtLPIDays_"+var1).val()) * <?php echo $doidle;?>;
			var margin_do1 = parseFloat($("#txtLPTIDays_"+var1).val()) * <?php echo $doidle;?>;
			
			if($("#txtLPCType_"+var1).val() == 0)
			{
				var wrkingdays_do = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 1)
			{
				var wrkingdays_do = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 2)
			{
				var wrkingdays_do = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $doidle;?>;
			}
				
			/*if($("#txtLPCrType_"+var1).val() == 1)
			{
				var wrkingdays_do1 = parseFloat($("#txtLPTWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtLPCrType_"+var1).val() == 2)
			{*/
				var wrkingdays_do1 = parseFloat($("#txtLPTWDays_"+var1).val()) * <?php echo $doidle;?>;
			/*}*/				
					
			var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do) + parseFloat(wrkingdays_do))  - (parseFloat(margin_do1) + parseFloat(wrkingdays_do1));
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
		}
	}
	if($("#txtLPActualDays").val() > 0)
	{
		if($("#txtLPADays_"+var1).val() == 0)
		{			
			var ttl_pi_days1 = parseFloat($("#txtTtPIDays").sum().toFixed(2)) - parseFloat($("#txtLPIDays_"+var1).val());
			var ttl_pi_days = parseFloat(ttl_pi_days1) + parseFloat($("#txtIdleDays").val());
			$("#txtTtPIDays").val(ttl_pi_days.toFixed(2));
			
			var ttl_pw_days1 = parseFloat($("#txtTtPWDays").sum().toFixed(2)) - parseFloat($("#txtLPWDays_"+var1).val());
			var ttl_pw_days = parseFloat(ttl_pw_days1) + parseFloat($("#txtwrkingDays").val());
			$("#txtTtPWDays").val(ttl_pw_days.toFixed(2));
			
			var lp_ttl_days = parseFloat($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));// - parseFloat($("#txtLPOHDays").val());
			$("#txtTDays").val(lp_ttl_days.toFixed(2));//alert($("#txtTSDays").val());alert($("#txtTtPIDays").val());alert($("#txtTtPWDays").val());
			var margin_fo = parseFloat($("#txtLPIDays_"+var1).val()) * <?php echo $foidle;?>;
			var margin_fo1 = parseFloat($("#txtIdleDays").val()) * <?php echo $foidle;?>;
			
			
			if($("#txtLPCType_"+var1).val() == 0)
			{
				var wrkingdays_fo = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 1)
			{
				var wrkingdays_fo = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 2)
			{
				var wrkingdays_fo = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $foidle;?>;
			}
			
			/*if($("#selPpType").val() == 1)
			{
				var wrkingdays_fo1 = parseFloat($("#txtwrkingDays").val()) * <?php echo $fowrking;?>;
			}
			else if($("#selPpType").val() == 2)
			{*/
				var wrkingdays_fo1 = parseFloat($("#txtwrkingDays").val()) * <?php echo $foidle;?>;//txtwrkingDays
			/*}	*/
		
			var consp_fo =  (parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo1) + parseFloat(wrkingdays_fo1)) - (parseFloat(margin_fo) + parseFloat(wrkingdays_fo));
			
			
			
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat($("#txtLPIDays_"+var1).val()) * <?php echo $doidle;?>;
			var margin_do1 = parseFloat($("#txtIdleDays").val()) * <?php echo $doidle;?>;
			
			if($("#txtLPCType_"+var1).val() == 0)
			{
				var wrkingdays_do = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 1)
			{
				var wrkingdays_do = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 2)
			{
				var wrkingdays_do = parseFloat($("#txtLPWDays_"+var1).val()) * <?php echo $doidle;?>;
			}
			
			/*if($("#selPpType").val() == 1)
			{
				var wrkingdays_do1 = parseFloat($("#txtwrkingDays").val()) * <?php echo $dowrking;?>;	
			}
			else if($("#selPpType").val() == 2)
			{*/
				var wrkingdays_do1 = parseFloat($("#txtwrkingDays").val()) * <?php echo $doidle;?>;	
			/*}	*/	
					
			var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do1) + parseFloat(wrkingdays_do1))  - (parseFloat(margin_do) + parseFloat(wrkingdays_do));
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
		}
		if($("#txtLPADays_"+var1).val() > 0)
		{
			var ttl_pi_days1 = parseFloat($("#txtTtPIDays").sum().toFixed(2)) - parseFloat($("#txtLPTIDays_"+var1).val());
			var ttl_pi_days = parseFloat(ttl_pi_days1) + parseFloat($("#txtIdleDays").val());
			$("#txtTtPIDays").val(ttl_pi_days.toFixed(2));
			
			var ttl_pw_days1 = parseFloat($("#txtTtPWDays").sum().toFixed(2)) - parseFloat($("#txtLPTWDays_"+var1).val());
			var ttl_pw_days = parseFloat(ttl_pw_days1) + parseFloat($("#txtwrkingDays").val());
			$("#txtTtPWDays").val(ttl_pw_days.toFixed(2));
			
			var lp_ttl_days = (parseFloat($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2)) + parseFloat($("#txtLPOHDays").val())) - parseFloat($("#txtLPOHDays_"+var1).val());
			$("#txtTDays").val(lp_ttl_days.toFixed(2));
			
			var margin_fo = parseFloat($("#txtLPTIDays_"+var1).val()) * <?php echo $foidle;?>;
			var margin_fo1 = parseFloat($("#txtIdleDays").val()) * <?php echo $foidle;?>;
			
			/*if($("#txtLPCrType_"+var1).val() == 1)
			{
				var wrkingdays_fo = parseFloat($("#txtLPTWDays_"+var1).val()) * <?php echo $fowrking;?>;
			}
			else if($("#txtLPCrType_"+var1).val() == 2)
			{*/
				var wrkingdays_fo = parseFloat($("#txtLPTWDays_"+var1).val()) * <?php echo $foidle;?>;
			/*}*/
			//...................................................
			/*if($("#selPpType").val() == 1)
			{
				var wrkingdays_fo1 = parseFloat($("#txtwrkingDays").val()) * <?php echo $fowrking;?>;
			}
			else if($("#selPpType").val() == 2)
			{*/
				var wrkingdays_fo1 = parseFloat($("#txtwrkingDays").val()) * <?php echo $foidle;?>;
			/*}*/
			
			var consp_fo =  (parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo1) + parseFloat(wrkingdays_fo1)) - (parseFloat(margin_fo) + parseFloat(wrkingdays_fo));
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat($("#txtLPTIDays_"+var1).val()) * <?php echo $doidle;?>;
			var margin_do1 = parseFloat($("#txtIdleDays").val()) * <?php echo $doidle;?>;
			
			/*if($("#txtLPCrType_"+var1).val() == 1)
			{
				var wrkingdays_do = parseFloat($("#txtLPTWDays_"+var1).val()) * <?php echo $dowrking;?>;
			}
			else if($("#txtLPCrType_"+var1).val() == 2)
			{*/
				var wrkingdays_do = parseFloat($("#txtLPTWDays_"+var1).val()) * <?php echo $doidle;?>;
			/*}*/
			//...................................................
			
			/*if($("#selPpType").val() == 1)
			{
				var wrkingdays_do1 = parseFloat($("#txtwrkingDays").val()) * <?php echo $dowrking;?>;	
			}
			else if($("#selPpType").val() == 2)
			{*/
				var wrkingdays_do1 = parseFloat($("#txtwrkingDays").val()) * <?php echo $doidle;?>;	
			/*}	*/			
					
			var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do1) + parseFloat(wrkingdays_do1))  - (parseFloat(margin_do) + parseFloat(wrkingdays_do));
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
		}
	}
		getFinalCalculation();
	//getTTLFreight();
}


function removePortRotation(var1,portid,disportid)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){	
			//for load port.....................
			$("#selLoadPort option[value='"+$("#txtFPort_"+var1).val()+"']").remove();
			$('[id^=txtLoadPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == portid)
				{
					if($("#txtPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtPCosts_"+rowid.split('_')[1]).val();}
					//if($("#txtLPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtLPIDays_"+rowid.split('_')[1]).val();}
					//if($("#txtLPWDays_"+rowid.split('_')[1]).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtLPWDays_"+rowid.split('_')[1]).val();}
					
					//var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					//var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
					//$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					//$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
					//$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDays(rowid.split('_')[1]);
					
					$("#lp_Row_"+rowid.split('_')[1]).remove();
					$("#oscLProw_"+rowid.split('_')[1]).remove();
					$("#oscLProw1_"+rowid.split('_')[1]).remove();
					$("#ddswLProw_"+rowid.split('_')[1]).remove();
					$("#ddswLProw1_"+rowid.split('_')[1]).remove();
					
					if($("#rdoQtyType1").is(":checked"))
					{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
					if($("#rdoQtyType2").is(":checked"))
					{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
					if(parseFloat(cargo_qty))
					{
						var per_mt = parseFloat(cargo_qty);
					}
					
					$("#txtDDNRLPCost").val($("[id^=txtDDSLPOSCCost_]").sum().toFixed(2));
					var ttlcostMT = parseFloat($("#txtDDNRLPCost").val()) / parseFloat(per_mt);
					$("#txtDDNRLPCostMT").val(ttlcostMT.toFixed(2));
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat(per_mt);
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					//getTTLFreight();
				}
			});
			//.........ends...................	
			//for discharge port.....................
			$("#selDisPort option[value='"+$("#txtTPort_"+var1).val()+"']").remove();
			$('[id^=txtDisPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == disportid)
				{
					if($("#txtDPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtDPCosts_"+rowid.split('_')[1]).val();}
					//if($("#txtDPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtDPIDays_"+rowid.split('_')[1]).val();}
					//if($("#txtDPWDays_"+rowid.split('_')[1]).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtDPWDays_"+rowid.split('_')[1]).val();}
					
					//var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					//var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
					//$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					//$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
					//$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDaysDP(rowid.split('_')[1]);
					
					$("#dp_Row_"+rowid.split('_')[1]).remove();
					$("#oscDProw_"+rowid.split('_')[1]).remove();
					$("#oscDProw1_"+rowid.split('_')[1]).remove();	
					$("#ddswDProw_"+rowid.split('_')[1]).remove();
					$("#ddswDProw1_"+rowid.split('_')[1]).remove();		
					
					if($("#rdoQtyType1").is(":checked"))
					{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
					if($("#rdoQtyType2").is(":checked"))
					{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
					
					if(parseFloat(cargo_qty))
					{
						var per_mt = parseFloat(cargo_qty);
					}
					$("#txtDDNRDPCost").val($("[id^=txtDDRDPOSCCost_]").sum().toFixed(2));
					var ttlcostMT = parseFloat($("#txtDDNRDPCost").val()) / parseFloat(per_mt);
					$("#txtDDNRDPCostMT").val(ttlcostMT.toFixed(2));
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat(per_mt);
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					//getTTLFreight();
				}
			});
			//.........ends...................		
			//for transit port.....................
			$("#selTLoadPort option[value='"+$("#txtFPort_"+var1).val()+"']").remove();
			$('[id^=txtTLoadPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == portid)
				{
					if($("#txtTPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtTPCosts_"+rowid.split('_')[1]).val();}
					//if($("#txtTPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtTPIDays_"+rowid.split('_')[1]).val();}
								
					//var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					//$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					//$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDaysTP(rowid.split('_')[1]);
					
					$("#tp_Row_"+rowid.split('_')[1]).remove();
					$("#oscTProw_"+rowid.split('_')[1]).remove();
					$("#oscTProw1_"+rowid.split('_')[1]).remove();
					
					if($("#rdoQtyType1").is(":checked"))
					{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
					if($("#rdoQtyType2").is(":checked"))
					{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}

					if(parseFloat(cargo_qty))
					{
						var per_mt = parseFloat(cargo_qty);
					}
					
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat(per_mt);
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					//getTTLFreight();
				}
			});
			//.........ends...................
			getRemoveVoyageTime(var1);	
			$("#pr_Row_"+var1).remove();
			var lasrvar = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar = rowid.split('_')[1];
			});
			$("#selTPort,#selPType,#txtDistance,#txtWeather,#txtMargin,#selSSpeed").val("");
			$("#selFPort").val($("#txtTPort_"+lasrvar).val());
			$("[id^=spcancel_]").hide();
			$("#spcancel_"+lasrvar).show();
			//getTTLFreight();
		}
		else{return false;}
		});	
		getFinalCalculation();
}



function getRemoveVoyageTime(var1)
{
if($("#txtADays_"+var1).val() == 0)
{
	if($("#txtMargin_"+var1).val() == ""){var margin = 0;}else{var margin = $("#txtMargin_"+var1).val();}
	if($("#txtDistance_"+var1).val() == ""){var distance = 0;}else{var distance = $("#txtDistance_"+var1).val();}
	if($("#txtWeather_"+var1).val() == ""){var speed_adj = 0;}else{var speed_adj = $("#txtWeather_"+var1).val();}
	if($("#txtPType_"+var1).val() == 1)
	{
		if($("#txtSSpeed_"+var1).val() == 1)
		{
			var ballast = <?php echo $bfs;?>;
			var fo_ballast = <?php echo $fo_bfs;?>;
			var do_ballast = <?php echo $do_bfs;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 2)
		{
			var ballast = <?php echo $bes1;?>;
			var fo_ballast = <?php echo $fo_bes1;?>;
			var do_ballast = <?php echo $do_bes1;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 3)
		{
			var ballast = <?php echo $bes2;?>;
			var fo_ballast = <?php echo $fo_bes2;?>;
			var do_ballast = <?php echo $do_bes2;?>;
		}
		
		var calc = ($("#txtDistance_"+var1).val() / (parseFloat(ballast) + parseFloat(speed_adj)))/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) - parseFloat(parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100));
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));	
		
		var ttl_days = parseFloat($("#txtBDays").val()) - parseFloat(parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100));
		$("#txtBDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var diff_fo = (parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(fo_ballast);
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(diff_fo.toFixed(2));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		
		var diff_do = (parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(do_ballast);
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(diff_do.toFixed(2));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat($("#txtBDist").val()) - parseFloat(distance);
		$("#txtBDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));	
	}
	if($("#txtPType_"+var1).val() == 2)
	{
		if($("#txtSSpeed_"+var1).val() == 1)
		{
			var ladan = <?php echo $lfs;?>;
			var fo_ladan = <?php echo $fo_lfs;?>;
			var do_ladan = <?php echo $do_lfs;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 2)
		{
			var ladan = <?php echo $les1;?>;
			var fo_ladan = <?php echo $fo_les1;?>;
			var do_ladan = <?php echo $do_les1;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 3)
		{
			var ladan = <?php echo $les2;?>;
			var fo_ladan = <?php echo $fo_les2;?>;
			var do_ladan = <?php echo $do_les2;?>;
		}
		
		var calc = ($("#txtDistance_"+var1).val() / (parseFloat(ladan) + parseFloat(speed_adj)))/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) - (parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100));
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		
		var ttl_days = parseFloat($("#txtLDays").val()) - (parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100));
		$("#txtLDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var diff_fo = (parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(fo_ladan);
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(diff_fo.toFixed(2));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		
		var diff_do = (parseFloat(calc.toFixed(2)) + parseFloat(parseFloat(parseFloat(calc)*parseFloat(margin))/100)) *  parseFloat(do_ladan);
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(diff_do.toFixed(2));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat($("#txtLDist").val()) - parseFloat(distance);
		$("#txtLDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
}
if($("#txtADays_"+var1).val() > 0)
{
	if($("#txtMargin_"+var1).val() == ""){var margin = 0;}else{var margin = $("#txtMargin_"+var1).val();}
	if($("#txtDistance_"+var1).val() == ""){var distance = 0;}else{var distance = $("#txtDistance_"+var1).val();}
	if($("#txtPType_"+var1).val() == 1)
	{
		if($("#txtSSpeed_"+var1).val() == 1)
		{
			var ballast = <?php echo $bfs;?>;
			var fo_ballast = <?php echo $fo_bfs;?>;
			var do_ballast = <?php echo $do_bfs;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 2)
		{
			var ballast = <?php echo $bes1;?>;
			var fo_ballast = <?php echo $fo_bes1;?>;
			var do_ballast = <?php echo $do_bes1;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 3)
		{
			var ballast = <?php echo $bes2;?>;
			var fo_ballast = <?php echo $fo_bes2;?>;
			var do_ballast = <?php echo $do_bes2;?>;
		}
		
		//var calc = ($("#txtDistance_"+var1).val() / parseFloat(ballast))/24;
		//var voyage_time = parseFloat($("#txtVoyageTime").val()) - (parseFloat(calc) + parseFloat(margin));
		//$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));	
		
		var ttl_days = parseFloat($("#txtBDays").val()) - parseFloat($("#txtADays_"+var1).val());
		$("#txtBDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var diff_fo = parseFloat($("#txtADays_"+var1).val()) *  parseFloat(fo_ballast);
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(diff_fo.toFixed(2));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		
		var diff_do = parseFloat($("#txtADays_"+var1).val()) *  parseFloat(do_ballast);
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(diff_do.toFixed(2));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat($("#txtBDist").val()) - parseFloat(distance);
		$("#txtBDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));	
	}
	if($("#txtPType_"+var1).val() == 2)
	{
		if($("#txtSSpeed_"+var1).val() == 1)
		{
			var ladan = <?php echo $lfs;?>;
			var fo_ladan = <?php echo $fo_lfs;?>;
			var do_ladan = <?php echo $do_lfs;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 2)
		{
			var ladan = <?php echo $les1;?>;
			var fo_ladan = <?php echo $fo_les1;?>;
			var do_ladan = <?php echo $do_les1;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 3)
		{
			var ladan = <?php echo $les2;?>;
			var fo_ladan = <?php echo $fo_les2;?>;
			var do_ladan = <?php echo $do_les2;?>;
		}
		
		//var calc = ($("#txtDistance_"+var1).val() / parseFloat(ladan))/24;
		//var voyage_time = parseFloat($("#txtVoyageTime").val()) - (parseFloat(calc) + parseFloat(margin));
		//$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		
		var ttl_days = parseFloat($("#txtLDays").val()) - parseFloat($("#txtADays_"+var1).val());
		$("#txtLDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var diff_fo = parseFloat($("#txtADays_"+var1).val()) *  parseFloat(fo_ladan);
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(diff_fo.toFixed(2));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		
		var diff_do = parseFloat($("#txtADays_"+var1).val()) *  parseFloat(do_ladan);
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(diff_do.toFixed(2));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat($("#txtLDist").val()) - parseFloat(distance);
		$("#txtLDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
}
getBunkerCalculation();
}


function removeLoadPort(var1)
{
	var mappingid = '<?php echo $mappingid;?>';
	$.post("options.php?id=38",{port_id:""+$("#txtLoadPort_"+var1).val()+"",port:"Load",mappingid:""+mappingid+""}, function(data) 
		{
				if(data == 0)
				{
				jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
					if(r){ 
						if($("#txtPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtPCosts_"+var1).val();}
						//if($("#txtLPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtLPIDays_"+var1).val();}
						//if($("#txtLPWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtLPWDays_"+var1).val();}
						
						//var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
						//var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
						//$("#txtTtPIDays").val(ttl_idays.toFixed(2));
						//$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
						//$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
						
						getRemoveTTLVoyageDays(var1);
						
						$("#lp_Row_"+var1).remove();
						$("#oscLProw_"+var1).remove();
						$("#oscLProw1_"+var1).remove();
						$("#ddswLProw_"+var1).remove();
						$("#ddswLProw1_"+var1).remove();
						
						if($("#rdoQtyType1").is(":checked"))
						{if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}}
						if($("#rdoQtyType2").is(":checked"))
						{if($("#txtTotalFreightQty").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtTotalFreightQty").val();}}
						$("#txtDDNRLPCost").val($("[id^=txtDDSLPOSCCost_]").sum().toFixed(2));
						var ttlcostMT = parseFloat($("#txtDDNRLPCost").val()) / parseFloat(c_qty);
						$("#txtDDNRLPCostMT").val(ttlcostMT.toFixed(2));
						
						var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat(c_qty);
						$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
						$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
						//getTTLFreight();
					}
					else{return false;}
					});	
				 }
				else
				{
					jAlert("Payment request has already been generated for this port. Cannot delete.<br>Please cancel Payment Request.", 'Alert');
				}		
		});
		getFinalCalculation();
}


function getRemoveTTLVoyageDays(var1)
{
	if($("#txtLPADays_"+var1).val() == 0)
	{
			if($("#txtLPIDays_"+var1).val() == ""){var idle_days = "0.00";}else{var idle_days = $("#txtLPIDays_"+var1).val();}
			if($("#txtLPWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtLPWDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idle_days);
			var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
			if($("#txtLPCType_"+var1).val() == 0)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 1)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 2)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $foidle;?>;
			}
			
			var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(margin_fo) + parseFloat(wrkingdays_fo));
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
			if($("#txtLPCType_"+var1).val() == 0)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;
			}
			else if($("#txtLPCType_"+var1).val() == 1)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
			}
			else if($("#txtLPCType_"+var1).val() == 2)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $doidle;?>;		
			}
			
			var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(margin_do) + parseFloat(wrkingdays_do));
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	}
	if($("#txtLPADays_"+var1).val() > 0)
	{
			if($("#txtLPTIDays_"+var1).val() == ""){var idle_days = "0.00";}else{var idle_days = $("#txtLPTIDays_"+var1).val();}
			if($("#txtLPTWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtLPTWDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idle_days);
			var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
			if($("#txtLPCrType_"+var1).val() == 0)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
			}
			else if($("#txtLPCrType_"+var1).val() == 1)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
			}
			else if($("#txtLPCrType_"+var1).val() == 2)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $foidle;?>;
			}
			
			var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2)));
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
			if($("#txtLPCrType_"+var1).val() == 0)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;
			}
			else if($("#txtLPCrType_"+var1).val() == 1)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;	
			}
			else if($("#txtLPCrType_"+var1).val() == 2)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $doidle;?>;	
			}
				
			var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2)));
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	}	
	getBunkerCalculation();
}

function getDisPortQty()
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtTotalFreightQty").val();}}
	
	if($("#selDPCName").val() != "")
	{
		if(c_qty != "")
		{
			$("#txtDQMT").val(parseFloat(c_qty) - parseFloat($("[id^=txtDpQMT_]").sum()));
			getDisPortCalculation();
		}
		else
		{
			$("#txtDQMT").val(0);
		}
	}
	else
	{
		$("#txtDQMT,#txtDRate,#txtDWDays,#txtDWDays1").val("");
	}
	getFinalCalculation();
}

function getDisPortCalculation()
{
	if($("#txtDQMT").val() != "" && $("#txtDRate").val() != "") 
	{
		//var value = ($("#txtDQMT").val() / $("#txtDRate").val());
		//$("#txtDWDays").val(value.toFixed(2));
		
		var value = 0;
		if($("#selDPTerms").val() == 1)
		{
			value = ($("#txtDQMT").val() / $("#txtDRate").val());
		}
		else if($("#selDPTerms").val() == 2)
		{
			value = parseFloat($("#txtDQMT").val() / $("#txtDRate").val()) * 1.262 ;
		}
		else if($("#selDPTerms").val() == 3)
		{
			value = parseFloat($("#txtDQMT").val() / $("#txtDRate").val()) * 1.405 ;
		}
		else
		{
			value = parseFloat($("#txtDQMT").val() / $("#txtDRate").val());
		}
		
		if($("#selDPTerms").val() != 4)
		{
		    $("#txtDWDays").val(value.toFixed(2));
		}
	}
	else
	{
		$("#txtDWDays,#txtDWDays1").val('0.00');
	}
	getFinalCalculation();
}



//..............for Dis Port details...............................................
function addDisPortDetails()
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtTotalFreightQty").val();}}
	
	if($("#selDisPort").val() != "" && $("#txtDQMT").val() != "" && $("#txtDRate").val() != "" && $("#selDPCName").val() != "" && $("#selDPTerms").val() != "")
	{
		if(parseFloat(c_qty) >= parseFloat($("[id^=txtDpQMT_],#txtDQMT").sum()))
		{
			var id = $("#dis_portID").val();
			id = (id - 1) + 2;
			$("#DProw_Empty").remove();
			var dis_port = document.getElementById("selDisPort").selectedIndex;
			var dis_port_text = document.getElementById("selDisPort").options[dis_port].text;
			
			var cargo = document.getElementById("selDPCName").selectedIndex;
			var cargo_text = document.getElementById("selDPCName").options[cargo].text;
						
			var row_id = $("#txtRow_id").val(); 
			if(row_id == 0)
			{	
			    var IDLE_FROM_DATE = IDLE_TO_DATE = IDLE_DAYS = WRKING_FROM_DATE = WRKING_TO_DATE = WRKING_DAYS = REASON_OFF_HIRE = OFF_HIRE_HRS = OFF_HIRE_DAYS =  ACTUAL_DAYS = PUMPS = DPTERM = "";
			    var actual_dp = <?php echo $obj->getActualDeschargePortDataJson($obj->getFun1());?>;
				if(actual_dp!=null)
				{
				$.each(actual_dp, function(index, array) {
				     if(array['DISPORTID'] == $("#selDisPort").val())
					     {
							IDLE_FROM_DATE   = array['IDLE_FROM_DATE'];
							IDLE_TO_DATE 	 = array['IDLE_TO_DATE'];
							IDLE_DAYS 		 = array['IDLE_DAYS'];
							WRKING_FROM_DATE = array['WRKING_FROM_DATE'];
							WRKING_TO_DATE   = array['WRKING_TO_DATE'];
							WRKING_DAYS 	 = array['WRKING_DAYS'];
							REASON_OFF_HIRE  = array['REASON_OFF_HIRE'];
							OFF_HIRE_HRS 	 = array['OFF_HIRE_HRS'];
							OFF_HIRE_DAYS 	 = array['OFF_HIRE_DAYS'];
							ACTUAL_DAYS 	 = array['ACTUAL_DAYS'];
							PUMPS 	 		 = array['PUMPS'];
							DPTERM 	 		 = array['DPTERM'];
						}
					});	
				 }
				if(ACTUAL_DAYS=="" && WRKING_DAYS=="" && IDLE_DAYS=="")	
					{IDLE_FROM_DATE = IDLE_TO_DATE = WRKING_FROM_DATE = WRKING_TO_DATE = REASON_OFF_HIRE = PUMPS = DPTERM = ""; IDLE_DAYS = WRKING_DAYS = WRKING_DAYS = OFF_HIRE_DAYS = ACTUAL_DAYS = 0;}	
				$('<tr id="dp_Row_'+id+'"><td align="center" class="input-text" ><a href="#dp'+id+'" onclick="removeDisPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="center" class="input-text" ><a href="#Edit'+id+'" onclick="getActualDPShow('+id+');" ><i class="fa fa-edit" style="color:#1b77a6;"></i></a></td><td align="left" class="input-text" >'+dis_port_text+'<input type="hidden" name="txtDisPort_'+id+'" id="txtDisPort_'+id+'" class="input" size="10" value="'+$("#selDisPort").val()+'"/></td><td align="left" class="input-text" >'+cargo_text+'<input type="hidden" name="txtDPCID_'+id+'" id="txtDPCID_'+id+'" class="input" size="10" value="'+$("#selDPCName").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDQMT").val()+'<input type="hidden" name="txtDpQMT_'+id+'" id="txtDpQMT_'+id+'" class="input" size="10" value="'+$("#txtDQMT").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDRate").val()+'<input type="hidden" name="txtDPRate_'+id+'" id="txtDPRate_'+id+'" class="input" size="10" value="'+$("#txtDRate").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDPCosts").val()+'<input type="hidden" name="txtDPCosts_'+id+'" id="txtDPCosts_'+id+'" class="input" size="10" value="'+$("#txtDPCosts").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDIDays").val()+'<input type="hidden" name="txtDPIDays_'+id+'" id="txtDPIDays_'+id+'" class="input" size="10" value="'+$("#txtDIDays").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDWDays").val()+'<input type="hidden" name="txtDPWDays_'+id+'" id="txtDPWDays_'+id+'" class="input" size="10" value="'+$("#txtDWDays").val()+'"/><input type="hidden" name="txtDPCType_'+id+'" id="txtDPCType_'+id+'" class="input" size="10" value="0"/></td><td align="left" class="input-text" >'+(parseFloat($("#txtDIDays").val()) + parseFloat($("#txtDWDays").val())).toFixed(2)+'<input type="hidden" name="txtDPEDays_'+id+'" id="txtDPEDays_'+id+'" class="input" size="10" value="'+(parseFloat($("#txtDIDays").val()) + parseFloat($("#txtDWDays").val())).toFixed(2)+'"/></td><td><span id="dpadaysLabel_'+id+'">'+ACTUAL_DAYS+'</span><input type="hidden" name="txtDPIFromDate_'+id+'" id="txtDPIFromDate_'+id+'" value="'+IDLE_FROM_DATE+'"/><input type="hidden" name="txtDPIToDate_'+id+'" id="txtDPIToDate_'+id+'" value="'+IDLE_TO_DATE+'"/><input type="hidden" name="txtDPTIDays_'+id+'" id="txtDPTIDays_'+id+'" value="'+IDLE_DAYS+'"/><input type="hidden" name="txtDPWFromDate_'+id+'" id="txtDPWFromDate_'+id+'" value="'+WRKING_FROM_DATE+'"/><input type="hidden" name="txtDPWToDate_'+id+'" id="txtDPWToDate_'+id+'" value="'+WRKING_TO_DATE+'"/><input type="hidden" name="txtDPTWDays_'+id+'" id="txtDPTWDays_'+id+'" value="'+WRKING_DAYS+'"/><input type="hidden" name="txtDPOHReason_'+id+'" id="txtDPOHReason_'+id+'" value="'+REASON_OFF_HIRE+'"/><input type="hidden" name="txtDPOHHrs_'+id+'" id="txtDPOHHrs_'+id+'" value="'+OFF_HIRE_HRS+'"/><input type="hidden" name="txtDPOHDays_'+id+'" id="txtDPOHDays_'+id+'" value="'+OFF_HIRE_DAYS+'"/><input type="hidden" name="txtDPADays_'+id+'" id="txtDPADays_'+id+'" value="'+ACTUAL_DAYS+'"/><input type="hidden" name="txtDPCrType_'+id+'" id="txtDPCrType_'+id+'" value="'+PUMPS+'"/><input type="hidden" name="txtDPTermsVal_'+id+'" id="txtDPTermsVal_'+id+'" class="form-control" value="'+DPTERM+'"/></td></tr>').appendTo("#tblDisPort");
			}
			else
			{
				var IDLE_FROM_DATE = IDLE_TO_DATE = IDLE_DAYS = WRKING_FROM_DATE = WRKING_TO_DATE = WRKING_DAYS = REASON_OFF_HIRE = OFF_HIRE_HRS = OFF_HIRE_DAYS =  ACTUAL_DAYS = PUMPS = DPTERM = "";
			    var actual_dp = <?php echo $obj->getActualDeschargePortDataJson($obj->getFun1());?>;
				if(actual_dp!=null)
				{
				$.each(actual_dp, function(index, array) {
				     if(array['DISPORTID'] == $("#selDisPort").val())
					     {
							IDLE_FROM_DATE   = array['IDLE_FROM_DATE'];
							IDLE_TO_DATE 	 = array['IDLE_TO_DATE'];
							IDLE_DAYS 		 = array['IDLE_DAYS'];
							WRKING_FROM_DATE = array['WRKING_FROM_DATE'];
							WRKING_TO_DATE   = array['WRKING_TO_DATE'];
							WRKING_DAYS 	 = array['WRKING_DAYS'];
							REASON_OFF_HIRE  = array['REASON_OFF_HIRE'];
							OFF_HIRE_HRS 	 = array['OFF_HIRE_HRS'];
							OFF_HIRE_DAYS 	 = array['OFF_HIRE_DAYS'];
							ACTUAL_DAYS 	 = array['ACTUAL_DAYS'];
							PUMPS 	 		 = array['PUMPS'];
							DPTERM 	 		 = array['DPTERM'];
						}
					});	
					}
					if(ACTUAL_DAYS=="" && WRKING_DAYS=="" && IDLE_DAYS=="")	
					{IDLE_FROM_DATE = IDLE_TO_DATE = WRKING_FROM_DATE = WRKING_TO_DATE = REASON_OFF_HIRE = PUMPS = DPTERM = ""; IDLE_DAYS = WRKING_DAYS = WRKING_DAYS = OFF_HIRE_DAYS = ACTUAL_DAYS = 0;}	
				$('<tr id="dp_Row_'+id+'"><td><a href="#dp'+id+'" onclick="removeDisPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="center"><a href="#Edit'+id+'" onclick="getActualDPShow('+id+');" ><i class="fa fa-edit" style="color:#1b77a6;"></i></a></td><td>'+dis_port_text+'<input type="hidden" name="txtDisPort_'+id+'" id="txtDisPort_'+id+'" value="'+$("#selDisPort").val()+'"/></td><td>'+cargo_text+'<input type="hidden" name="txtDPCID_'+id+'" id="txtDPCID_'+id+'" value="'+$("#selDPCName").val()+'"/></td><td>'+$("#txtDQMT").val()+'<input type="hidden" name="txtDpQMT_'+id+'" id="txtDpQMT_'+id+'" value="'+$("#txtDQMT").val()+'"/></td><td>'+$("#txtDRate").val()+'<input type="hidden" name="txtDPRate_'+id+'" id="txtDPRate_'+id+'" value="'+$("#txtDRate").val()+'"/></td><td>'+$("#txtDPCosts").val()+'<input type="hidden" name="txtDPCosts_'+id+'" id="txtDPCosts_'+id+'" value="'+$("#txtDPCosts").val()+'"/></td><td>'+$("#txtDIDays").val()+'<input type="hidden" name="txtDPIDays_'+id+'" id="txtDPIDays_'+id+'" value="'+$("#txtDIDays").val()+'"/></td><td>'+$("#txtDWDays").val()+'<input type="hidden" name="txtDPWDays_'+id+'" id="txtDPWDays_'+id+'" value="'+$("#txtDWDays").val()+'"/><input type="hidden" name="txtDPCType_'+id+'" id="txtDPCType_'+id+'" value="0"/></td><td>'+(parseFloat($("#txtDIDays").val()) + parseFloat($("#txtDWDays").val())).toFixed(2)+'<input type="hidden" name="txtDPEDays_'+id+'" id="txtDPEDays_'+id+'" value="'+(parseFloat($("#txtDIDays").val()) + parseFloat($("#txtDWDays").val())).toFixed(2)+'"/></td><td><span id="dpadaysLabel_'+id+'">'+ACTUAL_DAYS+'</span><input type="hidden" name="txtDPIFromDate_'+id+'" id="txtDPIFromDate_'+id+'" value="'+IDLE_FROM_DATE+'"/><input type="hidden" name="txtDPIToDate_'+id+'" id="txtDPIToDate_'+id+'" value="'+IDLE_TO_DATE+'"/><input type="hidden" name="txtDPTIDays_'+id+'" id="txtDPTIDays_'+id+'" value="'+IDLE_DAYS+'"/><input type="hidden" name="txtDPWFromDate_'+id+'" id="txtDPWFromDate_'+id+'" value="'+WRKING_FROM_DATE+'"/><input type="hidden" name="txtDPWToDate_'+id+'" id="txtDPWToDate_'+id+'" value="'+WRKING_TO_DATE+'"/><input type="hidden" name="txtDPTWDays_'+id+'" id="txtDPTWDays_'+id+'" value="'+WRKING_DAYS+'"/><input type="hidden" name="txtDPOHReason_'+id+'" id="txtDPOHReason_'+id+'" value="'+REASON_OFF_HIRE+'"/><input type="hidden" name="txtDPOHHrs_'+id+'" id="txtDPOHHrs_'+id+'" value="'+OFF_HIRE_HRS+'"/><input type="hidden" name="txtDPOHDays_'+id+'" id="txtDPOHDays_'+id+'" value="'+OFF_HIRE_DAYS+'"/><input type="hidden" name="txtDPADays_'+id+'" id="txtDPADays_'+id+'" value="'+ACTUAL_DAYS+'"/><input type="hidden" name="txtDPCrType_'+id+'" id="txtDPCrType_'+id+'" value="'+PUMPS+'"/><input type="hidden" name="txtDPTermsVal_'+id+'" id="txtDPTermsVal_'+id+'" class="form-control" value="'+DPTERM+'"/></td></tr>').appendTo("#tblDisPort");
				
			}
			getTTLVoyageDaysDP();
			if($("#txtDPCosts").val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPCosts").val();}
			
			if($("#rdoQtyType1").is(":checked"))
			{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
			if($("#rdoQtyType2").is(":checked"))
			{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
			
			var dpcostMT = parseFloat(dp_cost) / parseFloat(cargo_qty);
			var dp = "'DP'";
			$('<tr id="oscDProw_'+id+'"><td width="35%" align="left" class="input-text"  valign="top" >Discharge Port   '+dis_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"><input type="text"  name="txtDPOSCCost_'+id+'" id="txtDPOSCCost_'+id+'" onkeyup="getPortCostSum('+id+','+dp+');" class="form-control" size="10" readonly="true" value="'+$("#txtDPCosts").val()+'" /></td><td width="14%" align="left" class="input-text" valign="top"><select  name="selDPOSCCost_'+id+'" class="form-control" id="selDPOSCCost_'+id+'" ><?php $obj->getVendorListNew();?></select></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtDPOSCCostMT_'+id+'" id="txtDPOSCCostMT_'+id+'" class="form-control" size="10" readonly="true" value="'+dpcostMT.toFixed(2)+'"  /></td></tr><tr id="oscDProw1_'+id+'" height="5"><td colspan="10" align="left" class="text" valign="top" ></td></tr>').appendTo("#tbPortCosts");
		
		   $('<tr id="ddswDProw_'+id+'"><td>Discharge Port '+dis_port_text+'</td><td><input type="text" name="txtDDSODPComm_'+id+'" id="txtDDSODPComm_'+id+'" class="form-control" autocomplete="off" value="0.00" onkeyup="getFinalCalculation();" placeholder="0.00"></td><td><input type="text"  name="txtEstDDSODPCost_'+id+'" id="txtEstDDSODPCost_'+id+'" class="form-control" value="0.00" onKeyUp="getFinalCalculation();"/></td><td><input type="text"  name="txtDDSODPCost_'+id+'" id="txtDDSODPCost_'+id+'" class="form-control" value="0.00" onkeyup="getFinalCalculation();"/></td><td><select  name="selFGFVSOListDPortC_'+id+'" class="select form-control" id="selFGFVSOListDPortC_'+id+'" ><?php $obj->getVendorListNewUpdate("");	?></select></td><td><input type="text" name="txtDDSODPCostMT_'+id+'" id="txtDDSODPCostMT_'+id+'" class="form-control" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDSW");
		   
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat(cargo_qty);
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
			$("#txtTtPIDays").val($("#txtTtPIDays,#txtDIDays").sum().toFixed(2));
			if($("#txtDPADays_"+id).val()> 0)
			{
			$("#txtTtPWDays").val($("#txtTtPWDays,#txtDPADays_"+id).sum().toFixed(2));
			}
			else
			{
			$("#txtTtPWDays").val($("#txtTtPWDays,#txtDWDays").sum().toFixed(2));
			}
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			$("[id^=txtDDSODPComm_],[id^=txtEstDDSODPCost_]").numeric();
			$("#dis_portID").val(id);
			$("#selDisPort,#txtDQMT,#txtDRate,#txtDPDraftM,#txtDPCosts,#txtDIDays,#txtDWDays,#txtDWDays1,#selDPCName,#selDPCrType,#selDPTerms").val("");
			getDPRemoveDaysAttr();
		}
		else
		{
			jAlert('Discharged quantity is greater than Cargo Qty.', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Discharge Port', 'Alert');
	}
	getFinalCalculation();
}



function getActualDPShow(var1)
{
	if($("#txtDPID").val() == "")
	{
		$("[id^=DPEstRow]").hide();
		$("[id^=DPActRow]").show();
		$("#txtDPIdleFromDate").val($("#txtDPIFromDate_"+var1).val());
		$("#txtDPIdleToDate").val($("#txtDPIToDate_"+var1).val());
		$("#txtDPIdleDays").val($("#txtDPTIDays_"+var1).val());
		$("#txtDPWrkingFromDate").val($("#txtDPWFromDate_"+var1).val());
		$("#txtDPwrkingToDate").val($("#txtDPWToDate_"+var1).val());
		$("#txtDPwrkingDays").val($("#txtDPTWDays_"+var1).val());
		$("#txtDPOHReason").val($("#txtDPOHReason_"+var1).val());
		$("#txtDPOHHrs").val($("#txtDPOHHrs_"+var1).val());
		$("#txtDPOHDays").val($("#txtDPOHDays_"+var1).val());
		$("#txtDPActualDays").val($("#txtDPADays_"+var1).val());
		//$("#selDPPpType").val($("#txtDPCrType_"+var1).val());
		$("#txtDPID").val(var1);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}	
	getFinalCalculation();
}


function getTTLVoyageDaysDP()
{
	if($("#txtDIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtDIDays").val();}
	if($("#txtDWDays").val() == ""){var w_days = 0;}else{var w_days = $("#txtDWDays").val();}
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
		
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
	var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
		
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
	getBunkerCalculation();
}


function removeDisPort(var1)
{
	var mappingid = '<?php echo $mappingid;?>';
	$.post("options.php?id=38",{port_id:""+$("#txtDisPort_"+var1).val()+"",port:"Discharge",mappingid:""+mappingid+""}, function(data) 
		{
				if(data == 0)
				{
				jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
					if(r){ 
						if($("#txtDPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtDPCosts_"+var1).val();}
						//if($("#txtDPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtDPIDays_"+var1).val();}
						//if($("#txtDPWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtDPWDays_"+var1).val();}
						
						//var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
						//var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
						//$("#txtTtPIDays").val(ttl_idays.toFixed(2));
						//$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
						//$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
						
						getRemoveTTLVoyageDaysDP(var1);
						
						$("#dp_Row_"+var1).remove();
						$("#oscDProw_"+var1).remove();
						$("#oscDProw1_"+var1).remove();			
						$("#ddswDProw_"+var1).remove();
						$("#ddswDProw1_"+var1).remove();
						
						if($("#rdoQtyType1").is(":checked"))
						{if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}}
						if($("#rdoQtyType2").is(":checked"))
						{if($("#txtTotalFreightQty").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtTotalFreightQty").val();}}
						
						$("#txtDDNRDPCost").val($("[id^=txtDDRDPOSCCost_]").sum().toFixed(2));
						var ttlcostMT = parseFloat($("#txtDDNRDPCost").val()) / parseFloat(c_qty);
						$("#txtDDNRDPCostMT").val(ttlcostMT.toFixed(2));
						
						var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat(c_qty);
						$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
						$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
						//getTTLFreight();
					}
					else{return false;}
					});	
				}
				else
				{
					jAlert("Payment request has already been generated for this port. Cannot delete.<br>Please cancel Payment Request.", 'Alert');
				}		
		});
		getFinalCalculation();
}



function getRemoveTTLVoyageDaysDP(var1)
{
	if($("#txtDPADays_"+var1).val() == 0)
	{
			if($("#txtDPIDays_"+var1).val() == ""){var idle_days = "0.00";}else{var idle_days = $("#txtDPIDays_"+var1).val();}
			if($("#txtDPWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtDPWDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idle_days);
			var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
			if($("#txtDPCType_"+var1).val() == 0)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
			}
			else if($("#txtDPCType_"+var1).val() == 1)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
			}
			else if($("#txtDPCType_"+var1).val() == 2)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $foidle;?>;
			}
			
			var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(margin_fo) + parseFloat(wrkingdays_fo));
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
			if($("#txtDPCType_"+var1).val() == 0)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;	
			}
			else if($("#txtDPCType_"+var1).val() == 1)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;	
			}
			else if($("#txtDPCType_"+var1).val() == 2)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $doidle;?>;	
			}
				
			var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(margin_do) + parseFloat(wrkingdays_do));
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	}
	if($("#txtDPADays_"+var1).val() > 0)
	{
			if($("#txtDPTIDays_"+var1).val() == ""){var idle_days = "0.00";}else{var idle_days = $("#txtDPTIDays_"+var1).val();}
			if($("#txtDPTWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtDPTWDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idle_days);
			var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
			if($("#txtDPCrType_"+var1).val() == 0)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;	
			}
			else if($("#txtDPCrType_"+var1).val() == 1)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
			}
			else if($("#txtDPCrType_"+var1).val() == 2)
			{
				var wrkingdays_fo = parseFloat(w_days) * <?php echo $foidle;?>;
			}
			
			var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(margin_fo) + parseFloat(wrkingdays_fo));
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
			if($("#txtDPCrType_"+var1).val() == 0)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;	
			}
			else if($("#txtDPCrType_"+var1).val() == 1)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;	
			}
			else if($("#txtDPCrType_"+var1).val() == 2)
			{
				var wrkingdays_do = parseFloat(w_days) * <?php echo $doidle;?>;	
			}
				//alert(wrkingdays_do);	
			var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(margin_do) + parseFloat(wrkingdays_do));
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	}
	getBunkerCalculation();
}


function addTransitPortDetails()
{
	if($("#selTLoadPort").val() != "")
	{
		getTTLVoyageDaysTP();
		var id = $("#transit_portID").val();
		id = (id - 1) + 2;
		$("#TProw_Empty").remove();
		var load_port = document.getElementById("selTLoadPort").selectedIndex;
		var load_port_text = document.getElementById("selTLoadPort").options[load_port].text;
				
		var row_id = $("#txtRow_id").val(); 
		if(row_id == 0)
		{	
		        var IDLE_FROM_DATE = IDLE_TO_DATE = IDLE_DAYS = REASON_OFF_HIRE = OFF_HIRE_HRS = OFF_HIRE_DAYS = ACTUAL_DAYS="";
				
		        var actual_tp = <?php echo $obj->getActualTransitPortDataJson($obj->getFun1());?>;
				if(actual_tp!=null)
				{
				$.each(actual_tp[$("#selTLoadPort").val()], function(index, array) {
							IDLE_FROM_DATE   = array['IDLE_FROM_DATE'];
							IDLE_TO_DATE 	 = array['IDLE_TO_DATE'];
							IDLE_DAYS 		 = array['IDLE_DAYS'];
							REASON_OFF_HIRE  = array['REASON_OFF_HIRE'];
							OFF_HIRE_HRS 	 = array['OFF_HIRE_HRS'];
							OFF_HIRE_DAYS 	 = array['OFF_HIRE_DAYS'];
							ACTUAL_DAYS 	 = array['ACTUAL_DAYS'];
						});
					}
			if($("#txtTLIDays").val() == ""){var est_days = "0.0";}else{var est_days = $("#txtTLIDays").val();}		
			$('<tr id="tp_Row_'+id+'"><td><a href="#dp'+id+'" onclick="removeTransitPort('+id+');" ><i class="fa fa-times" style="color:red;"></a></td><td><a href="#Edit'+id+'" onclick="getActualTPShow('+id+');" ><i class="fa fa-edit" style="color:#1b77a6;"></i></i></a></td><td>'+load_port_text+'<input type="hidden" name="txtTLoadPort_'+id+'" id="txtTLoadPort_'+id+'" value="'+$("#selTLoadPort").val()+'"/></td><td>'+$("#txtTLPCosts").val()+'<input type="hidden" name="txtTPCosts_'+id+'" id="txtTPCosts_'+id+'" value="'+$("#txtTLPCosts").val()+'"/></td><td>'+est_days+'<input type="hidden" name="txtTPIDays_'+id+'" id="txtTPIDays_'+id+'" value="'+est_days+'"/></td><td>'+est_days+'<input type="hidden" name="txtTPEDays_'+id+'" id="txtTPEDays_'+id+'" value="'+est_days+'"/></td><td><span id="tpadaysLabel_'+id+'">'+ACTUAL_DAYS+'</span><input type="hidden" name="txtTPIFromDate_'+id+'" id="txtTPIFromDate_'+id+'" value="'+IDLE_FROM_DATE+'"/><input type="hidden" name="txtTPIToDate_'+id+'" id="txtTPIToDate_'+id+'" value="'+IDLE_TO_DATE+'"/><input type="hidden" name="txtTPTIDays_'+id+'" id="txtTPTIDays_'+id+'" value="'+IDLE_DAYS+'"/><input type="hidden" name="txtTPOHReason_'+id+'" id="txtTPOHReason_'+id+'" value="'+REASON_OFF_HIRE+'"/><input type="hidden" name="txtTPOHHrs_'+id+'" id="txtTPOHHrs_'+id+'" value="'+OFF_HIRE_HRS+'"/><input type="hidden" name="txtTPOHDays_'+id+'" id="txtTPOHDays_'+id+'" value="'+OFF_HIRE_DAYS+'"/><input type="hidden" name="txtTPADays_'+id+'" id="txtTPADays_'+id+'" value=""/></td></tr>').appendTo("#tblTransitPort");
			}
		else
			{
				var IDLE_FROM_DATE = IDLE_TO_DATE = IDLE_DAYS = REASON_OFF_HIRE = OFF_HIRE_HRS = OFF_HIRE_DAYS = ACTUAL_DAYS="";
		        var actual_tp = <?php echo $obj->getActualTransitPortDataJson($obj->getFun1());?>;
				if(actual_tp!=null)
				{
				$.each(actual_tp[$("#selTLoadPort").val()], function(index, array) {
							IDLE_FROM_DATE   = array['IDLE_FROM_DATE'];
							IDLE_TO_DATE 	 = array['IDLE_TO_DATE'];
							IDLE_DAYS 		 = array['IDLE_DAYS'];
							REASON_OFF_HIRE  = array['REASON_OFF_HIRE'];
							OFF_HIRE_HRS 	 = array['OFF_HIRE_HRS'];
							OFF_HIRE_DAYS 	 = array['OFF_HIRE_DAYS'];
							ACTUAL_DAYS 	 = array['ACTUAL_DAYS'];
						});
					}
				if($("#txtTLIDays").val() == ""){var est_days = "0.0";}else{var est_days = $("#txtTLIDays").val();}						
				$('<tr id="tp_Row_'+id+'"><td><a href="#dp'+id+'" onclick="removeTransitPort('+id+');" ><i class="fa fa-times" style="color:red;"></a></td><td><a href="#Edit'+id+'" onclick="getActualTPShow('+id+');" ><i class="fa fa-edit" style="color:#1b77a6;"></i></a></td><td>'+load_port_text+'<input type="hidden" name="txtTLoadPort_'+id+'" id="txtTLoadPort_'+id+'" value="'+$("#selTLoadPort").val()+'"/></td><td>'+$("#txtTLPCosts").val()+'<input type="hidden" name="txtTPCosts_'+id+'" id="txtTPCosts_'+id+'" value="'+$("#txtTLPCosts").val()+'"/></td><td>'+est_days+'<input type="hidden" name="txtTPIDays_'+id+'" id="txtTPIDays_'+id+'" value="'+est_days+'"/></td><td>'+est_days+'<input type="hidden" name="txtTPEDays_'+id+'" id="txtTPEDays_'+id+'" value="'+est_days+'"/></td><td><span id="tpadaysLabel_'+id+'">'+ACTUAL_DAYS+'</span><input type="hidden" name="txtTPIFromDate_'+id+'" id="txtTPIFromDate_'+id+'" value="'+IDLE_FROM_DATE+'"/><input type="hidden" name="txtTPIToDate_'+id+'" id="txtTPIToDate_'+id+'" value="'+IDLE_TO_DATE+'"/><input type="hidden" name="txtTPTIDays_'+id+'" id="txtTPTIDays_'+id+'" value="'+IDLE_DAYS+'"/><input type="hidden" name="txtTPOHReason_'+id+'" id="txtTPOHReason_'+id+'" value="'+REASON_OFF_HIRE+'"/><input type="hidden" name="txtTPOHHrs_'+id+'" id="txtTPOHHrs_'+id+'" value="'+OFF_HIRE_HRS+'"/><input type="hidden" name="txtTPOHDays_'+id+'" id="txtTPOHDays_'+id+'" value="'+OFF_HIRE_DAYS+'"/><input type="hidden" name="txtTPADays_'+id+'" id="txtTPADays_'+id+'" value=""/></td></tr>').appendTo("#tblTransitPort");
			}
		
		if($("#rdoQtyType1").is(":checked"))
		{if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}}
		if($("#rdoQtyType2").is(":checked"))
		{if($("#txtTotalFreightQty").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtTotalFreightQty").val();}}
		
		if($("#txtTLPCosts").val() == ""){var tp_cost = 0;}else{var tp_cost = $("#txtTLPCosts").val();}
		var tpcostMT = parseFloat(tp_cost) / parseFloat(c_qty);
		var tp = "'TP'";
		
		$('<tr id="oscTProw_'+id+'"><td>Transit Port   '+load_port_text+'</td><td></td><td><input type="text"  name="txtTPOSCCost_'+id+'" id="txtTPOSCCost_'+id+'" class="form-control" readonly="true" value="'+$("#txtTLPCosts").val()+'" /></td><td><select  name="selTPOSCCost_'+id+'" class="select form-control" id="selTPOSCCost_'+id+'"><?php $obj->getVendorListNew();?></select></td><td><input type="text" name="txtTPOSCCostMT_'+id+'" id="txtTPOSCCostMT_'+id+'" class="form-control" readonly="true" value="'+tpcostMT.toFixed(2)+'" /></td></tr><tr id="oscTProw1_'+id+'"><td colspan="10"></td></tr>').appendTo("#tbPortCosts");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat(c_qty);
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
		$("#txtTtPIDays").val($("#txtTtPIDays,#txtTLIDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		$("#transit_portID").val(id);
		$("#selTLoadPort,#txtTLPCosts,#txtTLIDays").val("");
		//getTTLFreight();
	}
	else
	{
		jAlert('Please fill all the records for Tansit Port', 'Alert');
	}
	getBunkerCalculation();
	getFinalCalculation();
}


function getActualTPShow(var1)
{
	if($("#txtTPID").val() == "")
	{
		$("[id^=TPEstRow]").hide();
		$("[id^=TPActRow]").show();
		$("#txtTPIdleFromDate").val($("#txtTPIFromDate_"+var1).val());
		$("#txtTPIdleToDate").val($("#txtTPIToDate_"+var1).val());
		$("#txtTPIdleDays").val($("#txtTPTIDays_"+var1).val());
		$("#txtTPOHReason").val($("#txtTPOHReason_"+var1).val());
		$("#txtTPOHHrs").val($("#txtTPOHHrs_"+var1).val());
		$("#txtTPOHDays").val($("#txtTPOHDays_"+var1).val());
		$("#txtTPActualDays").val($("#txtTPADays_"+var1).val());
		$("#txtTPID").val(var1);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}	
	getFinalCalculation();
}


function getTTLVoyageDaysTP()
{
	if($("#txtTLIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtTLIDays").val();}
	var ttl_days1 = parseFloat(idle_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo);
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;	
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do);
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	getBunkerCalculation();
}


function removeTransitPort(var1)
{
	var mappingid = '<?php echo $mappingid;?>';
	$.post("options.php?id=38",{port_id:""+$("#txtTLoadPort_"+var1).val()+"",port:"Transit",mappingid:""+mappingid+""}, function(data) 
		{
				if(data == 0)
				{
				jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
					if(r){
						if($("#txtTPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtTPCosts_"+var1).val();}
						//if($("#txtTPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtTPIDays_"+var1).val();}
									
						//var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
						//$("#txtTtPIDays").val(ttl_idays.toFixed(2));
						//$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
						
						getRemoveTTLVoyageDaysTP(var1);
						 
						$("#tp_Row_"+var1).remove();
						$("#oscTProw_"+var1).remove();
						$("#oscTProw1_"+var1).remove();
						
						if($("#rdoQtyType1").is(":checked"))
						{if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}}
						if($("#rdoQtyType2").is(":checked"))
						{if($("#txtTotalFreightQty").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtTotalFreightQty").val();}}
						
						var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat(c_qty);
						$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
						$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
						getFinalCalculation();
					}
					else{return false;}
					});	
				}
				else
				{
					jAlert("Payment request has already been generated for this port. Cannot delete.<br>Please cancel Payment Request.", 'Alert');
				}		
		});	
		
}


function getRemoveTTLVoyageDaysTP(var1)
{
	if($("#txtTPADays_"+var1).val() == 0)
	{
			if($("#txtTPIDays_"+var1).val() == ""){var idle_days = "0.00";}else{var idle_days = $("#txtTPIDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idle_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
			var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(margin_fo);
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;		
			var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(margin_do);
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	}
	if($("#txtTPADays_"+var1).val() > 0)
	{
			if($("#txtTPTIDays_"+var1).val() == ""){var idle_days = "0.00";}else{var idle_days = $("#txtTPTIDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idle_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
			var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(margin_fo);
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;		
			var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(margin_do);
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	}	
	getBunkerCalculation();
}


function getShowHide()
{
	if($("#txtbid").val() == 0)
	{
		$('#BID').attr('title', 'Open Panel');
		$("#bunker_adj").hide();
		$("#txtbid").val(1);
	}
	else
	{
		$('#BID').attr('title', 'Close Panel');
		$("#bunker_adj").show();
		$("#txtbid").val(0);
	}
}


function getBunkerCalculation()
{
	    var noOfBunker = $("#txtBunkerRec").val();
		for(var i=1;i<=noOfBunker;i++)
		{
		   var bunkergrade = $("#txtBunkerGradeName_"+i).val();
		   var str = bunkergrade.substr(0,3);
		   if(str == 'IFO')
			{
				if($("#txtTFUMTManual").val()>0)
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTFUMTManual").val());
				}
				else
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTFUMT").val());
				}
			}
			else if(str == 'MDO')
			{
				if($("#txtTDUMTManual").val()>0)
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMTManual").val());
				}
				else
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMT").val());
				}
			}
			else if(str == 'MGO')
			{
				if($("#txtTDUMTManual").val()>0)
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMTManual").val());
				}
				else
				{
					$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMT").val());
				}
			}
			else
			{
				$("#txt"+bunkergrade+"_1_1").val('0.00');
			}
			
			var var3 = $("#txt"+bunkergrade+"_1_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_1_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_1_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_1_3").val(calc.toFixed(2));
			
			var var3 = $("#txt"+bunkergrade+"_8_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_8_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_8_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_8_3").val(calc.toFixed(2));
			
			var var3 = $("#txt"+bunkergrade+"_9_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_9_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_9_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_9_3").val(calc.toFixed(2));
			
			var var3 = $("#txt"+bunkergrade+"_10_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_10_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_10_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_10_3").val(calc.toFixed(2));
			
			var var3 = $("#txt"+bunkergrade+"_11_1").val();
			if(var3 == ""){var first = 0;}else{var first = var3;}
			if($("#txt"+bunkergrade+"_11_2").val() == ""){var second = 0;}else{var second = $("#txt"+bunkergrade+"_11_2").val();}
			var calc = parseFloat(first) * parseFloat(second);
			$("#txt"+bunkergrade+"_11_3").val(calc.toFixed(2));
			
			if($("#txt"+bunkergrade+"_10_3").val() > 0)
			{
			  $("#txtBunkerCost_"+bunkergrade).val($("#txt"+bunkergrade+"_10_3").val());
			}
			else
			{
			  $("#txtBunkerCost_"+bunkergrade).val($("#txt"+bunkergrade+"_1_3").val());
			}
			
		}
	getFinalCalculation();
}


function getORCCalculate(status,var1)
{
	if(status == 1)
	{
		$("#txtHidORCAmt_"+var1).val($("#txtORCAmt_"+var1).val());
	}
	if(status == 2)
	{
		$("#txtHidORCAmt_"+var1).val("-"+$("#txtORCAmt_"+var1).val());
	}
	getFinalCalculation();
}


function getCharterersCostCalculate(var1)
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	if(parseFloat(cargo_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	
	if($("#txtCCAbs_"+var1).val() == ""){var cc_abs = 0;}else{var cc_abs = $("#txtCCAbs_"+var1).val();}
	if($("#txtCCAddComm_"+var1).val() == ""){var cc_addcomm = 0;}else{var cc_addcomm = $("#txtCCAddComm_"+var1).val();}
	var cc_value = parseFloat(cc_abs) - parseFloat((parseFloat(cc_abs) * parseFloat(cc_addcomm))/100);
	
	$("#txtCCValue_"+var1).val(cc_value.toFixed(2));
	var calc = parseFloat(cc_abs) / parseFloat(per_mt);
	$("#txtCCostMT_"+var1).val(calc.toFixed(2));
	getFinalCalculation();	
}


function getOMCCalculate(var1)
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	if(parseFloat(cargo_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	if($("#txtOMCAbs_"+var1).val() == ""){var omc_abs = 0;}else{var omc_abs = $("#txtOMCAbs_"+var1).val();}
	if($("#txtOMCAddComm_"+var1).val() == ""){var omc_addcomm = 0;}else{var omc_addcomm = $("#txtOMCAddComm_"+var1).val();}
	var omc_value = parseFloat(omc_abs) - parseFloat((parseFloat(omc_abs) * parseFloat(omc_addcomm))/100);
	//alert(omc_value);
	$("#txtOMCValue_"+var1).val(omc_value.toFixed(2));
	
	var calc = parseFloat(omc_value) / parseFloat(per_mt);
	$("#txtOMCCostMT_"+var1).val(calc.toFixed(2));
	getFinalCalculation();	
}


function getPortCostSum(var1,port)
{
	if($("#rdoQtyType1").is(":checked"))
	{if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}}
	if($("#rdoQtyType2").is(":checked"))
	{if($("#txtTotalFreightQty").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtTotalFreightQty").val();}}
	
	if(port == 'LP')
	{
		if($("#txtLPOSCCost_"+var1).val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtLPOSCCost_"+var1).val();}
		var lpcostMT = parseFloat(lp_cost) / parseFloat(cargo_qty);
		$("#txtLPOSCCostMT_"+var1).val(lpcostMT.toFixed(2));
	}
	else if(port == 'DP')
	{
		if($("#txtDPOSCCost_"+var1).val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPOSCCost_"+var1).val();}
		var dpcostMT = parseFloat(dp_cost) / parseFloat(cargo_qty);
		$("#txtDPOSCCostMT_"+var1).val(dpcostMT.toFixed(2));
	}
	else if(port == 'TP')
	{
		if($("#txtTPOSCCost_"+var1).val() == ""){var tp_cost = 0;}else{var tp_cost = $("#txtTPOSCCost_"+var1).val();}
		var tpcostMT = parseFloat(tp_cost) / parseFloat(cargo_qty);
		$("#txtTPOSCCostMT_"+var1).val(tpcostMT.toFixed(2));
	}
	getFinalCalculation();			
}


function getValidate(var1)
{
	if($("#rdoMMarket1").is(":checked"))
	{
		var arr = new Array(); var arr1 = new Array();
		var i=0;
		$('[id^=txtFreightSpecs_]').each(function(index) {
				var j = i+1;
				if($("#txtFreightSpecs_"+j).val() == "" || $("#txtMinCargo_"+j).val() == "" || $("#txtMCWSFlatRate_"+j).val() == "" || $("#txtMCWS_"+j).val() == "" || $("#txtMCAmount_"+j).val() == "" || parseFloat($("#txtMCAmount_"+j).val()) == "0.00")
				{
					arr[i] = j;		
				}			
				i++;
		});
		if(arr.length == 0)
		{
		jConfirm('Have you selected required vendor(s) Y/N ?', 'Confirmation', function(r) {
			if(r){
					getFinalCalculation();
					$("#txtStatus").val(var1);
					document.frm1.submit();
				 }
			else{return false;}
			});
		}
		else
		{
			jAlert('Please fill mandatory fields', 'Alert');
			return false;
		}
	}
	if($("#rdoMMarket2").is(":checked"))
	{
		jConfirm('Have you selected required vendor(s) Y/N ?', 'Confirmation', function(r) {
			if(r){
					getFinalCalculation();
					$("#txtStatus").val(var1);
					document.frm1.submit();
				 }
			else{return false;}
			});
	}
}


function getRowid(port)
{
	if(port.value != "")
	{
		var i = 1;
		$('[id^=txtLoadPort_]').each(function(index) {
						if(this.value == port.value)
						{
							$("#txtRow_id").val(i); 
						}
						i++;
			});
	}
	else
	{
		$("#txtRow_id").val(0); 
	}
}


function getRowidDP(port)
{
	if(port.value != "")
	{
		var i = 1;
		$('[id^=txtDisPort_]').each(function(index) {
						if(this.value == port.value)
						{
							$("#txtRow_id").val(i); 
						}
						i++;
			});
	}
	else
	{
		$("#txtRow_id").val(0); 
	}
}



function getRowidTP(port)
{
	if(port.value != "")
	{
		var i = 1;
		$('[id^=txtTLoadPort_]').each(function(index) {
						if(this.value == port.value)
						{
							$("#txtRow_id").val(i); 
						}
						i++;
			});
	}
	else
	{
		$("#txtRow_id").val(0); 
	}
}


function getTPOHDays()
{
	if($("#txtTPOHHrs").val() == ""){var hrs = 0;}else{var hrs = $("#txtTPOHHrs").val();}
	var calc = hrs / 24;
	$("#txtTPOHDays").val(calc.toFixed(2));
	
	var actual = parseFloat($("#txtTPIdleDays").sum()) - parseFloat($("#txtTPOHDays").sum());
	$("#txtTPActualDays").val(actual.toFixed(2));
}


function removeActualTPShow()
{
	$("[id^=TPEstRow]").show();
	$("[id^=TPActRow]").hide();
	$("#txtTPID").val("");
}


function addActualTPShow()
{
	var var1 = $("#txtTPID").val();
	getTotalTransitPort(var1);
	
	$("#txtTPIFromDate_"+var1).val($("#txtTPIdleFromDate").val());
	$("#txtTPIToDate_"+var1).val($("#txtTPIdleToDate").val());
	$("#txtTPTIDays_"+var1).val($("#txtTPIdleDays").val());
	$("#txtTPOHReason_"+var1).val($("#txtTPOHReason").val());
	$("#txtTPOHHrs_"+var1).val($("#txtTPOHHrs").val());
	$("#txtTPOHDays_"+var1).val($("#txtTPOHDays").val());
	$("#txtTPADays_"+var1).val($("#txtTPActualDays").val());
	$("#tpadaysLabel_"+var1).html($("#txtTPActualDays").val());
	
	$("[id^=TPEstRow]").show();
	$("[id^=TPActRow]").hide();
	$("#txtTPID").val("");
	$("#txtTPIdleFromDate,#txtTPIdleToDate").removeClass("hasDatepicker");
}


function getTotalTransitPort(var1)
{
	if($("#txtTPActualDays").val() == 0)
	{
		if($("#txtTPADays_"+var1).val() > 0)
		{
			var ttl_pi_days1 = parseFloat($("#txtTtPIDays").sum().toFixed(2)) - parseFloat($("#txtTPTIDays_"+var1).val());
			var ttl_pi_days = parseFloat(ttl_pi_days1) + parseFloat($("#txtTPIDays_"+var1).val());
			$("#txtTtPIDays").val(ttl_pi_days.toFixed(2));
						
			var lp_ttl_days = parseFloat($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			$("#txtTDays").val(lp_ttl_days.toFixed(2));
			
			var margin_fo = parseFloat($("#txtTPIDays_"+var1).val()) * <?php echo $foidle;?>;
			var margin_fo1 = parseFloat($("#txtTPTIDays_"+var1).val()) * <?php echo $foidle;?>;
			
			var consp_fo =  (parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo)) - parseFloat(margin_fo1);
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat($("#txtTPIDays_"+var1).val()) * <?php echo $doidle;?>;
			var margin_do1 = parseFloat($("#txtTPTIDays_"+var1).val()) * <?php echo $doidle;?>;		
					
			var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do))  - parseFloat(margin_do1);
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
		}
	}
	if($("#txtTPActualDays").val() > 0)
	{
		if($("#txtTPADays_"+var1).val() == 0)
		{
			var ttl_pi_days1 = parseFloat($("#txtTtPIDays").sum().toFixed(2)) - parseFloat($("#txtTPIDays_"+var1).val());
			var ttl_pi_days = parseFloat(ttl_pi_days1) + parseFloat($("#txtTPIdleDays").val());
			$("#txtTtPIDays").val(ttl_pi_days.toFixed(2));
			
			var lp_ttl_days = parseFloat($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));// - parseFloat($("#txtTPOHDays").val());
			$("#txtTDays").val(lp_ttl_days.toFixed(2));
			
			var margin_fo = parseFloat($("#txtTPIDays_"+var1).val()) * <?php echo $foidle;?>;
			var margin_fo1 = parseFloat($("#txtTPIdleDays").val()) * <?php echo $foidle;?>;
			
			var consp_fo =  (parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo1)) - parseFloat(margin_fo);
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat($("#txtTPIDays_"+var1).val()) * <?php echo $doidle;?>;
			var margin_do1 = parseFloat($("#txtTPIdleDays").val()) * <?php echo $doidle;?>;	
					
			var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do1))  - parseFloat(margin_do);
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
		}
		if($("#txtTPADays_"+var1).val() > 0)
		{
			var ttl_pi_days1 = parseFloat($("#txtTtPIDays").sum().toFixed(2)) - parseFloat($("#txtTPTIDays_"+var1).val());
			var ttl_pi_days = parseFloat(ttl_pi_days1) + parseFloat($("#txtTPIdleDays").val());
			$("#txtTtPIDays").val(ttl_pi_days.toFixed(2));
			
			var lp_ttl_days = (parseFloat($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2)) + parseFloat($("#txtTPOHDays").val())) - parseFloat($("#txtTPOHDays_"+var1).val());
			$("#txtTDays").val(lp_ttl_days.toFixed(2));
			
			var margin_fo = parseFloat($("#txtTPTIDays_"+var1).val()) * <?php echo $foidle;?>;
			var margin_fo1 = parseFloat($("#txtTPIdleDays").val()) * <?php echo $foidle;?>;
			
			var consp_fo =  (parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo1)) - parseFloat(margin_fo);
			$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
			
			var margin_do = parseFloat($("#txtTPTIDays_"+var1).val()) * <?php echo $doidle;?>;
			var margin_do1 = parseFloat($("#txtTPIdleDays").val()) * <?php echo $doidle;?>;	
					
			var consp_do =  (parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do1))  - parseFloat(margin_do);
			$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
		}
	}
	getBunkerCalculation();
}


function getRemoveFrtAdj(var1)
{
	var arr = new Array(); var arr1 = new Array();
	var i=0;
	$('[id^=txtFreightSpecs_]').each(function(index) {
				var j = i+1;
				arr[i] = j;					
				i++;
		});
	if(arr.length != 1)
	{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#frt_adj_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
	}
	else 
	{
		jAlert('Atleast one Freight Specs is required.', 'Alert');
		return false;
	}
}


function AddFrtAdjRow()
{
	var id = $("#txtFSID").val();
	if($("#txtFreightSpecs_"+id).val() != "" && $("#txtMinCargo_"+id).val() != "" && $("#txtMCWSFlatRate_"+id).val() != "" && $("#txtMCAmount_"+id).val() != "" && $("#txtMCAmount_"+id).val() != "0.00")
	{
		id = (id - 1) + 2;
			
		$('<tbody id="frt_adj_'+id+'"><tr height="35"><td align="left" class="input-text" valign="top" colspan="10" ></td></tr><tr height="25"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="15%" align="left" class="input-text"  valign="top" >Freight Specs</td><td width="1%" align="left" class="input-text" valign="top">:</td><td width="23%" align="left" class="input-text" valign="top"><input type="text"  name="txtFreightSpecs_'+id+'" id="txtFreightSpecs_'+id+'" class="input-text" size="18"  autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top">WS Flat Rate</td><td width="11%" align="left" class="input-text" valign="top">WS</td><td width="11%" align="left" class="input-text" valign="top">Distance Leg</td><td width="11%" align="left" class="input-text" valign="top">Total Distance</td><td width="11%" align="left" class="input-text" valign="top">Amount</td><td width="6%" align="left" class="input-text" valign="top"><a href="#del" style="cursor:pointer;" onclick="getRemoveFrtAdj('+id+');"  title="Remove Entry" ><img src="../../images/icon_delete.gif"  /></a></td></tr><tr  height="25"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="15%" align="left" class="input-text"  valign="top" >Min Cargo (MT)</td><td width="1%" align="left" class="input-text" valign="top">:</td><td width="23%" align="left" class="input-text" valign="top"><input type="text"  name="txtMinCargo_'+id+'" id="txtMinCargo_'+id+'" class="input-text" size="18" readonly="true" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtMCWSFlatRate_'+id+'" id="txtMCWSFlatRate_'+id+'" class="input-text" size="4" readonly="true" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtMCWS_'+id+'" id="txtMCWS_'+id+'" class="input-text" size="4" onkeyup="getFinalCalculation();" autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtMCDistanceLeg_'+id+'" id="txtMCDistanceLeg_'+id+'" class="input-text" size="4"  autocomplete="off" onkeyup="getFinalCalculation();" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtMCTotalDistance_'+id+'" id="txtMCTotalDistance_'+id+'" class="input-text" size="4" onkeyup="getFinalCalculation();"   autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtMCAmount_'+id+'" id="txtMCAmount_'+id+'" class="input-text" size="4" readonly="true" value=""/></td><td width="6%" align="left" class="input-text" valign="top"></td></tr><tr height="25"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="15%" align="left" class="input-text"  valign="top" >Overage (MT)</td><td width="1%" align="left" class="input-text" valign="top">:</td><td width="23%" align="left" class="input-text" valign="top"><input type="text"  name="txtOverage_'+id+'" id="txtOverage_'+id+'" class="input-text" size="18" readonly="true" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtOvrWSFlatRate_'+id+'" id="txtOvrWSFlatRate_'+id+'" class="input-text" size="4" readonly="true" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtOvrWS_'+id+'" id="txtOvrWS_'+id+'" class="input-text" size="4" onkeyup="getFinalCalculation();" autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtOvrDistanceLeg_'+id+'" id="txtOvrDistanceLeg_'+id+'" class="input-text" size="4" onkeyup="getFinalCalculation();"  autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtOvrTotalDistance_'+id+'" id="txtOvrTotalDistance_'+id+'" class="input-text" size="4" onkeyup="getFinalCalculation();"  autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtOvrAmount_'+id+'" id="txtOvrAmount_'+id+'" class="input-text" size="4" readonly="true" value=""/></td><td width="6%" align="left" class="input-text" valign="top"></td></tr><tr height="25"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="15%" align="left" class="input-text"  valign="top" >Total Cargo Qty (MT)</td><td width="1%" align="left" class="input-text" valign="top">:</td><td width="23%" align="left" class="input-text" valign="top"><input type="text"  name="txtTotalCargoQty_'+id+'" id="txtTotalCargoQty_'+id+'" class="input-text" size="18"  readonly="true" value=""/></td><td width="11%" align="left" class="input-text" valign="top"></td><td width="11%" align="left" class="input-text" valign="top"></td><td width="11%" align="left" class="input-text" valign="top"></td><td width="11%" align="left" class="input-text" valign="top"></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtTotalAmount_'+id+'" id="txtTotalAmount_'+id+'" class="input-text" size="4"  readonly="true" value=""/></td><td width="6%" align="left" class="input-text" valign="top"></td></tr></tbody>').appendTo("#tbl_frt_adj");
	
		if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
		if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
		if($("#txtWSFR").val() == ""){var ws_flat_rate = 0;}else{var ws_flat_rate = $("#txtWSFR").val();}
		
		$("#txtMinCargo_"+id).val(min_qty);$("#txtOverage_"+id).val(addnl_qty);
		$("#txtMCWSFlatRate_"+id).val(ws_flat_rate);$("#txtOvrWSFlatRate_"+id).val(ws_flat_rate);
		$("#txtTotalCargoQty_"+id).val($("#txtMinCargo_"+id+" , #txtOverage_"+id+"").sum());
		
		$("[id^=txtMCWS_],[id^=txtMCDistanceLeg_],[id^=txtMCTotalDistance_],[id^=txtOvrWS_],[id^=txtOvrDistanceLeg_],[id^=txtOvrTotalDistance_]").numeric();	
		$("#txtFSID").val(id);
	}
	else
	{
		jAlert('Please fill data for Freight Specs', 'Alert');
		return false;
	}
}


function addAddition()
{
	var id = $("#txtADDID").val();
	if($("#txtAddition_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="add_row_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="43%" align="left" class="input-text"  valign="top" ><input type="text"  name="txtAddition_'+id+'" id="txtAddition_'+id+'" class="input-text" size="30" autocomplete="off" value="" /></td><td width="17%" align="left" class="input-text" valign="top"></td><td width="18%" align="center" class="input-text" valign="top"><input type="text"  name="txtAdditionAmt_'+id+'" id="txtAdditionAmt_'+id+'" class="input-text" size="10" autocomplete="off" onkeyup="getFinalCalculation();" value="0.00" /></td><td width="22%" align="center" class="input-text" valign="top"></td></tr>').appendTo("#tbody_add");
	
		$("[id^=txtAdditionAmt_]").numeric();	
		$("#txtADDID").val(id);
	}
	else
	{
		jAlert('Please fill data for Additions', 'Alert');
		return false;
	}
}


function addDeductions()
{
	var id = $("#txtDEDID").val();
	if($("#txtDeduction_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="ded_row_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="43%" align="left" class="input-text"  valign="top" ><input type="text"  name="txtDeduction_'+id+'" id="txtDeduction_'+id+'" class="input-text" size="30" autocomplete="off" value="" /></td><td width="17%" align="left" class="input-text" valign="top"></td><td width="18%" align="center" class="input-text" valign="top"><input type="text"  name="txtDeductionAmt_'+id+'" id="txtDeductionAmt_'+id+'" class="input-text" size="10" autocomplete="off" onkeyup="getFinalCalculation();" value="0.00" /></td><td width="22%" align="center" class="input-text" valign="top"></td></tr>').appendTo("#tbody_ded");
	
		$("[id^=txtDeductionAmt_]").numeric();	
		$("#txtDEDID").val(id);
	}
	else
	{
		jAlert('Please fill data for Deductions', 'Alert');
		return false;
	}
}


function getDPOHDays()
{
	if($("#txtDPOHHrs").val() == ""){var hrs = 0;}else{var hrs = $("#txtDPOHHrs").val();}
	var calc = hrs / 24;
	$("#txtDPOHDays").val(calc.toFixed(2));
	
	var actual = parseFloat($("#txtDPIdleDays,#txtDPwrkingDays").sum()) - parseFloat($("#txtDPOHDays").sum());
	$("#txtDPActualDays").val(actual.toFixed(2));
}

function addActualDPShow()
{
	/*if($("#selDPPpType").val() != "")
	{*/
		var var1 = $("#txtDPID").val();
		var actual = parseFloat($("#txtDPIdleDays,#txtDPwrkingDays").sum()) - parseFloat($("#txtDPOHDays").sum());
		$("#txtDPActualDays").val(actual.toFixed(2));
		var var1 = $("#txtDPID").val();
		getTotalDischargePort(var1);
		
		$("#txtDPIFromDate_"+var1).val($("#txtDPIdleFromDate").val());
		$("#txtDPIToDate_"+var1).val($("#txtDPIdleToDate").val());
		$("#txtDPTIDays_"+var1).val($("#txtDPIdleDays").val());
		$("#txtDPWFromDate_"+var1).val($("#txtDPWrkingFromDate").val());
		$("#txtDPWToDate_"+var1).val($("#txtDPwrkingToDate").val());
		$("#txtDPTWDays_"+var1).val($("#txtDPwrkingDays").val());
		$("#txtDPOHReason_"+var1).val($("#txtDPOHReason").val());
		$("#txtDPOHHrs_"+var1).val($("#txtDPOHHrs").val());
		$("#txtDPOHDays_"+var1).val($("#txtDPOHDays").val());
		$("#txtDPADays_"+var1).val($("#txtDPActualDays").val());
		//$("#txtDPCrType_"+var1).val($("#selDPPpType").val());
		$("#dpadaysLabel_"+var1).html($("#txtDPActualDays").val());
		
		$("[id^=DPEstRow]").show();
		$("[id^=DPActRow]").hide();
		$("#txtDPID").val("");
		$("#txtDPIdleFromDate,#txtDPIdleToDate,#txtDPWrkingFromDate,#txtDPwrkingToDate").removeClass("hasDatepicker");
	/*}
	else
	{
		jAlert('Please select Pump', 'Alert');
		return false;
	}*/
}

function removeActualDPShow()
{
	$("[id^=DPEstRow]").show();
	$("[id^=DPActRow]").hide();
	$("#txtDPID").val("");
}



function addBrokerageRow()
{
	var id = $("#txtBRokageCount").val();
	if($("#txtBrCommPercent_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="tbrRow_'+id+'"><td><a href="#tb1" onclick="removeBrokerage('+id+');" ><i class="fa fa-times" style="color:red;"></a></td><td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td><td><input type="text" name="txtBrCommPercent_'+id+'" id="txtBrCommPercent_'+id+'" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td><td><input type="text" name="txtBrComm_'+id+'" id="txtBrComm_'+id+'" class="form-control" readonly value="0.00" /></td><td><select  name="selBroVList_'+id+'" class="select form-control" id="selBroVList_'+id+'"></select></td></tr>').appendTo("#tbodyBrokerage");
	    $("#selBroVList_"+id).html($("#selVendor").html());$("#selBroVList_"+id).val('');
		$("[id^=txtBrCommPercent_]").numeric();	
		$("#txtBRokageCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeBrokerage(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#tbrRow_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}


function showHideQtyVendorDiv(value)
{
	if($("#rdoQtyType1").is(":checked"))
	{
		$("#divMarket1,#divMarket2,#divMarket3,#divMarket4,#divMarket5,#divMarket6,#divMarket7,#divMarket8,#divMarket9,#divMarket10").show();
		$("#divQty1,#divQty2,#divQty3,#divQty4,#tblQtyFreight1,#tblQtyFreight2").hide();
	}
	if($("#rdoQtyType2").is(":checked"))
	{
		$("#divMarket1,#divMarket2,#divMarket3,#divMarket4,#divMarket5,#divMarket6,#divMarket7,#divMarket8,#divMarket9,#divMarket10").hide();
		$("#divQty1,#divQty2,#divQty3,#divQty4,#tblQtyFreight1,#tblQtyFreight2").show();
		if(value==2)
		{
		  $("#tblQtyFreight,#tblQtyFreight1,#tblQtyFreight2").empty();
		  $("#txtQTYID").val(0);
		}
	}
	getFinalCalculation();
}

function getQtyLocalFreightCal()
{
	if($("#txtQtyLocalAggriedFreight").val() == ""){var localfreightcost = 0;}else{var localfreightcost = $("#txtQtyLocalAggriedFreight").val();}
	if($("#txtDisExchangeRate").val() == ""){var txtDisExchangeRate = 0;}else{var txtDisExchangeRate = $("#txtDisExchangeRate").val();}
	var calmul = parseFloat(localfreightcost)/parseFloat(txtDisExchangeRate);
	$("#txtQtyAggriedFreight").val(calmul.toFixed(2));
	getQtyFreightVendorCal();
}

function getQtyFreightVendorCal()
{
	if($("#txtQtyAggriedFreight").val() == ""){var txtQtyAggriedFreight = 0;}else{var txtQtyAggriedFreight = $("#txtQtyAggriedFreight").val();}
	if($("#txtFreightQty").val() == ""){var txtFreightQty = 0;}else{var txtFreightQty = $("#txtFreightQty").val();}
	var calmul = parseFloat(txtQtyAggriedFreight)*parseFloat(txtFreightQty);
	$("#txtQtyFreight").val(calmul);
	$("#spanQtyFreight").text(calmul);
}

function addQtyVendorDetails()
{
	var id = $("#txtQTYID").val();
	if($("#selQtyVendorList").val() != "" && $("#txtQtyAggriedFreight").val() != "" && $("#txtFreightQty").val() != "" )
	{
		id = (id - 1) + 2;
		$("#tbrQtyVRow_empty").remove();
		$('<tr id="tbrQtyVRow_'+id+'"><td><a href="#tb1" onclick="deleteQtyVendorDetails('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td>'+$("#selQtyVendorList option:selected").text()+'<input type="hidden" name="selQtyVendorList_'+id+'" id="selQtyVendorList_'+id+'" autocomplete="off" value="'+$("#selQtyVendorList").val()+'"/></td><td>'+$("#txtQtyAggriedFreight").val()+'<input type="hidden" name="txtQtyAggriedFreight_'+id+'" id="txtQtyAggriedFreight_'+id+'" class="form-control" value="'+$("#txtQtyAggriedFreight").val()+'"/></td><td>'+$("#txtFreightQty").val()+'<input type="hidden" name="txtFreightQty_'+id+'" id="txtFreightQty_'+id+'" value="'+$("#txtFreightQty").val()+'"/></td><td>'+$("#txtQtyFreight").val()+'<input type="hidden" name="txtQtyFreight_'+id+'" id="txtQtyFreight_'+id+'" value="'+$("#txtQtyFreight").val()+'"/><input type="hidden" name="txtCurrencyDis_'+id+'" id="txtCurrencyDis_'+id+'" value="'+$("#selCurrencyDisList").val()+'"/><input type="hidden" name="txtQtyLocalAggried_'+id+'" id="txtQtyLocalAggried_'+id+'" value="'+$("#txtQtyLocalAggriedFreight").val()+'"/><input type="hidden" name="txtDisExchangeRate_'+id+'" id="txtDisExchangeRate_'+id+'" value="'+$("#txtDisExchangeRate").val()+'"/></td></tr>').appendTo("#tblQtyFreight");
		
		if($("#txtQtyFreight").val()=="" || isNaN($("#txtQtyFreight").val())){var freight = 0;}else{var freight = $("#txtQtyFreight").val();}
		if($("#txtFreightQty").val()=="" || isNaN($("#txtFreightQty").val())){var qty = 0;}else{var qty = $("#txtFreightQty").val();}
		var costmt = parseFloat(freight)/parseFloat(qty);
		$('<tr id="tbrQtyVRow1_'+id+'"><td>'+$("#selQtyVendorList option:selected").text()+'</td><td><input type="text" name="txtFreightQty1_'+id+'" id="txtFreightQty1_'+id+'" value="'+$("#txtQtyFreight").val()+'" autocomplete="off" class="form-control" readonly value="0.00" /></td><td><input type="text" name="txtQtyBrokeragePer_'+id+'" id="txtQtyBrokeragePer_'+id+'" value="0" onKeyUp="getFinalCalculation();" class="form-control" value="0.00" /></td><td><input type="text" name="txtQtyBrokerageAmt_'+id+'" id="txtQtyBrokerageAmt_'+id+'" value="0" class="form-control" readonly value="0.00" /></td><td><input type="text" name="txtQtyNetFreight_'+id+'" id="txtQtyNetFreight_'+id+'" value="0" class="form-control" readonly value="'+$("#txtQtyFreight").val()+'" /></td></tr>').appendTo("#tblQtyFreight1");
		
		
		$('<tr id="tbrQtyVRow2_'+id+'"><td>Final Nett Freight</td><td>'+$("#selQtyVendorList option:selected").text()+'</td></td><td><input type="text"  name="txtQtyNetFreight1_'+id+'" id="txtQtyNetFreight1_'+id+'" class="form-control" readonly value="0.00" /></td><td><input type="text"  name="txtQtyNetFreightMT1_'+id+'" id="txtQtyNetFreightMT1_'+id+'" class="form-control" readonly value="0.00" /></td></tr>').appendTo("#tblQtyFreight2");
		
		$("#txtQTYID").val('');
		$("#txtQtyBrokeragePer_"+id).numeric();
		$("#spanQtyFreight").text(''); 
		$("#selQtyVendorList,#txtQtyAggriedFreight,#txtFreightQty,#txtQtyFreight").val('');
		$("#txtQTYID").val(id);
	}
	else
	{
		jAlert('Please fill All details', 'Alert');
		return false;
	}
	getFinalCalculation();
}

function deleteQtyVendorDetails(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#tbrQtyVRow_"+var1).remove();
					$("#tbrQtyVRow1_"+var1).remove();
					$("#tbrQtyVRow2_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
}

function getQtyVendorDetailsSum()
{
	$("#txtTotalFreightQty").val($("[id^=txtFreightQty_]").sum().toFixed(2));
	$("#txtTotalQtyFreight").val($("[id^=txtQtyFreight_]").sum().toFixed(2));
	
	for(var i = 1;i<=$("#txtQTYID").val();i++)
	{
		if($("#txtFreightQty1_"+i).val() == ""){var FreightQty = 0;}else{var FreightQty = $("#txtFreightQty1_"+i).val();}
		if($("#txtQtyBrokeragePer_"+i).val() == ""){var brokrage = 0;}else{var brokrage = $("#txtQtyBrokeragePer_"+i).val();}
		var brokerage_usd = (parseFloat(FreightQty) * parseFloat(brokrage))/100;
		$("#txtQtyBrokerageAmt_"+i).val(brokerage_usd.toFixed(2));
		var diff = $("#txtFreightQty1_"+i).val() - $("#txtQtyBrokerageAmt_"+i).val();
		if(diff == "" || isNaN(diff)){var diff = 0;}
		$("#txtQtyNetFreight_"+i).val(diff.toFixed(2));
		$("#txtQtyNetFreight1_"+i).val(diff.toFixed(2));
		var qtyMT = parseFloat(diff) / parseFloat(FreightQty);
		$("#txtQtyNetFreightMT1_"+i).val(qtyMT.toFixed(2));
	}
	$("#txtTotalNetBrokerage").val($("[id^=txtQtyBrokerageAmt_]").sum().toFixed(2));
	$("#txtTotalNetFreight").val($("[id^=txtQtyNetFreight_]").sum().toFixed(2));
	
}


function getLPRemoveDaysAttr()
{
	if($("#selLPTerms").val() == 4)
	{
		
		$("#txtWDays").removeAttr('readonly');
		$("#txtWDays").val('');
	}
	else
	{
		$("#txtWDays").attr('readonly',true);
	}
}

function getDPRemoveDaysAttr()
{
	if($("#selDPTerms").val() == 4)
	{
		$("#txtDWDays").removeAttr('readonly');
		$("#txtDWDays").val('');
	}
	else
	{
		$("#txtDWDays").attr('readonly',true);
	}	
}

function getMarLocalFreightCal()
{
	if($("#txtMarLocalAggriedFreight").val() == ""){var localfreightcost = 0;}else{var localfreightcost = $("#txtMarLocalAggriedFreight").val();}
	if($("#txtMarExchangeRate").val() == ""){var txtMarExchangeRate = 0;}else{var txtMarExchangeRate = $("#txtMarExchangeRate").val();}
	var calmul = parseFloat(localfreightcost)/parseFloat(txtMarExchangeRate);
	$("#txtMTCPDRate").val(calmul.toFixed(2));
	getFinalCalculation();
}


function getCVEAmount()
{
	var val = $("#txtORCAmtCVEIdentify").val();
	if(val!="")
	{
		if($("#txtORCAmtCVE_"+val).val() == ""){var txtORCAmtCVE = 0;}else{var txtORCAmtCVE = $("#txtORCAmtCVE_"+val).val();}
		if($("#txtTDays").val() == ""){var txtTDays = 0;}else{var txtTDays = $("#txtTDays").val();}
		var calmul = parseFloat(parseFloat(parseFloat(txtORCAmtCVE)/30)*parseFloat(txtTDays));
		$("#txtORCAmt_"+val).val(calmul.toFixed(2));
		$("#txtHidORCAmt_"+val).val(calmul.toFixed(2));
	}
	getFinalCalculation();
}



</script>
    </body>
</html>