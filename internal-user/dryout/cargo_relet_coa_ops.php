<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	if($_REQUEST['txtStatus'] == "")
	{
		if($_REQUEST['txtComid2'] == "")
		{
			$msg = $obj->insertCargoReletActualCostSheetName();
		}
		else
		{
			$msg = $obj->deleteInOpsEntry($_REQUEST['txtComid2']);
		}
		header('Location:./coa_in_ops_at_glance.php?msg=3');
	}	
	header('Location:./cargo_relet_coa_ops.php?msg=3');
 }
 

if (isset($_REQUEST['selYear']) && $_REQUEST['selYear']!="")
{
	$selYear = $_REQUEST['selYear'];
}
else
{
	$selYear = date('Y',time());
}

if (isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$selBType = $_SESSION['selBType'] = $_REQUEST['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	unset($_SESSION['selBType']);
	$selBType = '';
}

$pagename = basename($_SERVER['PHP_SELF']);

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rights = $obj->getUserRights($uid,$moduleid,19); 

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(15); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops COA Cargo Relet&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops COA Cargo Relet&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Cargo Relet - COA</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Cargo Relet - COA added/updated successfully.
				</div>
				<?php }?>
                <?php if($msg == 4){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> new sheet added successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Cargo Relet - COA sheet successfully create.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }?>
				<?php if($msg == 6){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Nomination sent to "Post Ops".
				</div>
				<?php }}?>
				<div class="box box-primary">
					<h3 style=" text-align:center;">Cargo Relet - COA</h3>
							
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                            <div class="col-xs-6">
                            </div>
                            <div class="col-xs-3">
                                 <select name="selBType" id="selBType" class="form-control" onChange="getSubmit();">
                                 <option value="">---Select Business Type---</option>
								   <?php echo $obj->getBusinessTypeList1($selBType);?>
                                  </select>                  
							</div><!-- /.col -->
						    <div class="col-xs-3">
								  <select name="selYear" id="selYear" class="form-control" onChange="getSubmit();">
                                   <?php echo $obj->getCargoreletYearList($selYear);?>
                                  </select>                      
							</div><!-- /.col -->
						<div style="height:10px;">
							<input type="hidden" name="txtComid2" id="txtComid2" value="" />
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<input type="hidden" name="txtFileName" id="txtFileName" value="" />
							<input type="hidden" name="action" id="action" value="submit" />
						</div>
                
						<div class="box-body table-responsive" style="overflow:auto;">
							<table class="table table-bordered table-striped" id='in_ops_at_glance'>
								<thead>
									<tr valign="top">
										<th align="left">Relet Calc /Docs</th>
										<th align="left">Nom ID/COA ID</th>
                                        <th align="left">Business Type</th>
										<th align="left">Material Name</th>
                                        <th align="left">Vessel(if applicable)</th>
                                        <th align="left">CP Date</th>
										<th align="left">Relet Financials</th>
										<th align="left">LP/DP</th>
										<th align="left">SOF</th>
										<th align="left">Laytime</th>	
										<th align="center">Payment / Invoice</th>
										<th align="center">Shipment Dates(From/To)</th>
										<th align="center">Deactivate/ Alerts/Compare Sheets</th>
                                        <th align="center">Complete</th>
									</tr>
								</thead>
                                <tbody>
									<?php
									if($selBType!='')
									{
									$sql = "select cargo_relet_estimate_compare.COMID as COMID, cargo_relet_estimate_compare.FCAID as FCAID, cargo_relet_estimate_masster.TRANS_DATE, cargo_relet_estimate_masster.VESSEL_IMO_ID as VESSEL_IMO_ID, cargo_relet_estimate_masster.COAID as COAID, cargo_relet_estimate_masster.OPEN_CARGOID as OPEN_CARGOID, cargo_relet_estimate_compare.MESSAGE as MESSAGE from cargo_relet_estimate_compare inner join cargo_relet_estimate_masster on cargo_relet_estimate_masster.FCAID= cargo_relet_estimate_compare.FCAID WHERE cargo_relet_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND cargo_relet_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and cargo_relet_estimate_compare.FINAL_ID!='' and cargo_relet_estimate_masster.FIXED=1 and cargo_relet_estimate_compare.STATUS=1 and year(cargo_relet_estimate_masster.ADDONDATE)='".$selYear."' and cargo_relet_estimate_masster.BUSINESSTYPEID='".$_SESSION['selBType']."' and COAAID is not NULL order by date(cargo_relet_estimate_masster.ADDONDATE) desc"; 
									//echo $sql;
                                    $res = mysql_query($sql) or die($sql);
									$rec = mysql_num_rows($res);
									$i=1;
									$submit = 0;
										if($rec == 0)
										{
											echo '<tbody>';
									
											echo '</tbody>';
										}else{
									?>
								
									<?php
										while($rows = mysql_fetch_assoc($res))
										{
											$sql2 = "select * from cargo_relet_estimate_slave2 where FCAID='".$rows['FCAID']."' and IDENTIFY='IN'";
											$result2 = mysql_query($sql2);
											$arr_loadport = $arr_disport = array();
											while($rows2 = mysql_fetch_assoc($result2))
											{
												if($rows2['PORT_TYPE'] == 'LP')
												{
													$arr_loadport[] = $obj->getPortName($rows2['PORTID']);
												}
												else
												{
													$arr_disport[] = $obj->getPortName($rows2['PORTID']);
												}
											}
											$cp_date = $obj->getCoaData($rows["COAID"],'COA_DATE');
											if($cp_date!="0000-00-00" && $cp_date!=""){$cp_date = date('d-m-Y',strtotime($cp_date));}else{$cp_date = "";}
											
											$materialid = $obj->getCargoPlanningData($rows['OPEN_CARGOID'],"CARGO");
											
											$ESTIMATE_TYPE = $obj->getCompareCargoReletData($rows['COMID'],"BUSINESSTYPEID");
										?>
										<tr id="in_row_<?php echo $i;?>" style="font-size:11px;">
                                           <td>
												<a href="viewcoacargorelet.php?id=<?php echo $rows['FCAID'];?>&page=2" style="color:green;" title="Click me" >Relet Calc</a><br/><br/>
												<a href="documents.php?comid=<?php echo $rows['COMID'];?>&page=4" style="color:green;" title="Click me" >Docs</a>
											</td>
                                            
                                            <td>
												<?php echo $rows['MESSAGE'];?>/<br/><?php echo $obj->getCoaData($rows["COAID"],'COA_ID');?>
											</td>
                                            <td>
												<?php echo $obj->getEstimateList($ESTIMATE_TYPE);?>
											</td>
											<td>
												<?php echo $obj->getMaterialCodeDesBasedOnCode($materialid);?>
											</td>
                                            
											<td>
												<?php echo $obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME");?>/<br/> <?php echo $obj->getCompareCargoReletData($rows['COMID'],"VESSEL_TYPE");?>
											</td>
                                            <td>
												<?php echo $cp_date;?>
											</td>
                                            <td style="text-align:center;">
												<?php
													$sql1 = "select * from cost_sheetrelet_name_master where COMID='".$rows['COMID']."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";

													$res1 = mysql_query($sql1);
													$rec1 = mysql_num_rows($res1);
													$cost_sheet_id = '';
													if($rec1 > 0)
													{
														while($rows1 = mysql_fetch_assoc($res1))
														{
															$cost_sheet_id = $rows1['COST_SHEETID'];
															if($rows1['PROCESS'] != "EST")
															{
																$href = "./cost_sheet_relet.php?comid=".$rows['COMID']."&cost_sheet_id=".$rows1['COST_SHEETID'];
																
																echo "<a href='".$href."'>".str_replace(' ','&nbsp;',$rows1['SHEET_NAME'])."</a><br/>";
																$curr_cst = $rows1['COST_SHEETID'];
															}
															else
															{
																$curr_cst = $obj->getCostSheetCargoreletID($rows['COMID'],"SHEET_NO");
															}
														}
													}	
											        if(($obj->getCompareCargoReletData($rows['COMID'],"FINAL_STATUS")==1 && !empty($obj->getTCICostSheetIDRelet($rows['COMID'], $cost_sheet_id))) || $cost_sheet_id=='')
													{
													?>
														<br/><button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-modal1" type="button" title="Add New CS" onClick="openWin1(<?php echo $rows['COMID'];?>);">A</button>
													<?php }else{?>
                                                    <br/><button class="btn btn-primary btn-flat" type="button" title="Add New CS" onClick="msg();">A</button>
                                                    <?php }?>
											</td>
											<td>
												<?php echo implode(', ',$arr_loadport).'<br/>/'.implode(', ',$arr_disport);?>
											</td>
														
											<td>
												<a href="sofrelet.php?comid=<?php echo $rows['COMID'];?>&page=4">SOF</a>		
											</td>
											<td>
												<a href="laytime_calculationrelet.php?comid=<?php echo $rows['COMID'];?>&page=4" style="color:blue;" title="Click me" >Calculations</a>
											</td>
											<td>
												<a href="payment_gridrelet.php?comid=<?php echo $rows['COMID'];?>" >View</a><br><br>
												<br>
												<a href="piclubdeclarationrelet.php?comid=<?php echo $rows['COMID'];?>" >P & I Club Declaration</a>
												
											</td>
                                            <td>
                                                <?php echo date('d-m-Y',strtotime($rows['TRANS_DATE']));?>
                                            </td>
                                            <td style="text-align:center;">
												<a href="#A" title="Deactivate entry" onClick="getValidate2(<?php echo $rows['COMID'];?>,<?php echo $i;?>);">
													<i class="fa fa-times" style="color:red;"></i>
												</a><br/>
                                                <button class="btn btn-primary btn-flat" type="button" title="Compare Sheets" onClick="getPdf(<?php echo $rows['COMID'];?>);">Compare Sheets</button><br/><br/>
                                                
                                                <?php 
												$freightsql = "select * from freight_invoicerelet_master WHERE (MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$rows['COMID']."' and P_AMT is NULL) or (MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$rows['COMID']."' and P_AMT=0)";
												$freightres = mysql_query($freightsql);
												$freightrec = mysql_num_rows($freightres);
												if($freightrec >0)
												{?>
                                                <span style="color:red; font-weight:bold;">Payment not Received</span>
                                                <?php }?>
												
												
											</td>
											<td>
											   
												<button class="btn btn-primary btn-flat" id="inner-login-button"  type="button" title="Push to Post Ops" onClick="getValidate3(<?php echo $rows['COMID'];?>);">Post Ops</button>
											</td>
											
										
										</tr>	
                                        
									<?php $i = $i + 1; } }}else{?>
                                    
                                    <?php }?>
								</tbody>
							</table>
						</div>
					</form>
										
				</div>
                
                
                			
				</div>
                
                
                <div class="modal fade" id="compose-modal1" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body">
									<div class="box box-primary">
										<div class="box-body no-padding">
											<form name="frm2" id="frm2" method="post" enctype="multipart/form-data" action="<?php echo $pagename;?>">
												<div class="row invoice-info">
													
												</div>
												
												<div class="row invoice-info" style="padding:2%">
													<div class="col-sm-12 invoice-col">
														Please enter Cargo Relet Name and Submit
														<address>
															<input type="text" name="txtFile" id="txtFile" class="form-control" autocomplete="off" value="<?php echo $obj->getNomDetailsData($comid,"FREIGHT_RATE");?>" placeholder="Cargo Relet Name" />
															<input type="hidden" name="txtComid1" id="txtComid1" />
														</address>
													</div><!-- /.col -->
												</div>
												
												
												<div class="row invoice-info">
													<div class="col-sm-12 invoice-col" style="text-align:center;">
														 <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
														 <input type="hidden" name="action" id="action" value="submit" />
													</div><!-- /.col -->
												</div>
												
											</form>
										</div><!-- /.box-body -->
									</div>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
                    
                    
                		
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>

<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.validate.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#frm1").validate({
		rules: {
			
		},
		messages: {
			
		},
		submitHandler: function(form)  {
			jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
			$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "110%"});
			$("#popup_content").css({"background":"none","text-align":"center"});
			$("#popup_ok,#popup_title").hide();  
			form.submit();
		}
	});
	$("#frm2").validate({
	rules: {
		
	},
	messages: {
		
	},
	submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "110%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
	//sortDateFormat();
	$("#in_ops_at_glance").dataTable({
    "iDisplayLength": 100
    });
	
//for uploading........
 $('#photoimg').live('change', function(){ 
	$("#cpDiv_"+$("#txtVar").val()).append("<span id='labelDiv_"+$("#txtVar").val()+"_"+$("#txtID_"+$("#txtVar").val()).val()+"'>abc</span>");
	$("#labelDiv_"+$("#txtVar").val()+"_"+$("#txtID_"+$("#txtVar").val()).val()).html('<img src="../../images/loader.gif" alt="Uploading...." width="20" height="10"  />');
	$("#imageform").ajaxForm({
				target: '#labelDiv_'+$("#txtVar").val()+'_'+$("#txtID_"+$("#txtVar").val()).val()
	}).submit();
	var id = $("#txtID_"+$("#txtVar").val()).val();
	id = (id - 1) + 2;
	$("#txtID_"+$("#txtVar").val()).val(id);
	$.modal.close();
});
//....................
}); 
function getData()
{
	$("#in_ops_at_glance tbody").empty();
	$("#in_ops_at_glance tbody").html('<tr><td height="200" colspan="17" align="center" ><img src="../../img/loading.gif" /></td></tr>');
	$.post("options.php?id=42",{selVName:""+$("#selVName").val()+""}, function(html) {
	$("#in_ops_at_glance tbody").empty();
	$("#in_ops_at_glance tbody").append(html);
	});
}
function openWin1(comid)
{
	$('#basic-modal-content').modal();
	$("#txtComid1").val(comid);
	$("#simplemodal-container").css({"height":"200px"});
}

function getValidate()
{
	if($("#txtFile").val() != "")
	{
		document.frm2.submit();
	}
	else
	{
		jAlert('Please fill the file name', 'Alert');
		return false;
	}
}

function msg()
{
	jAlert('Please make sure the last Cargo Relet is Submit to Close', 'Alert');
}

function openWin2(var1,mappinid)
{
	$('#basic-modal-content1').modal();
	$("#txtVar").val(var1);
	$("#txtComid").val(comid);
	$("#simplemodal-container").css({"width":"300px","height":"50px"});
}	

function openWin(comid,vesselid,id)
{
	$('#basic-modal').modal();
	$("#txtComid").val(comid);
	$("#txtVesselid").val(vesselid);
	$("#txtuid").val(id);
	$("#simplemodal-container").css({"width":"300px","height":"50px"});
}

function getValid()
{
	if($('#txtfile').val() == '')
	{
		jAlert('Please choose the file', 'Alert');
		return false;
	}
	else
	{
		var name = $('#txtfile').val();
		name = name.split('.');
		if(name[1] == 'txt')
		{
			return true;
		}
		else
		{
			jAlert('Please choose only .txt file', 'Alert');
			$('#txtfile').val('');
			return false;
		}
	}
}

function getPdfForSea(comid,vesselid)
{
	location.href='allPdf.php?id=12&comid='+comid+'&vesselid='+vesselid;
}

function getValidate2(var1,rowid)
{
	$("#in_row_"+rowid+" td").css({"background-color":"red"});
	jConfirm('Are you sure to de-activate this Nom ID ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtComid2").val(var1); 
			document.frm1.submit();
		}
		else{$("#in_row_"+rowid+" td").css({"background-color":""});return false;}
		});	
}

function getValidate3(var1)
{
	jConfirm('Are you sure to send this Nom ID to post ops ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtComid2").val(var1);
			$("#txtStatus").val(1);
			 
			document.frm1.submit();
		}
		else{return false;}
		});	
}

function getValidate4(var1,file_name)
{
	jConfirm('Are you sure to remove this document permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtComid2").val(var1);
			$("#txtStatus").val(2);
			$("#txtFileName").val(file_name);
			document.frm1.submit();
		}
		else{return false;}
		});	
}

function getSubmit()
{ 
    $("#action").val("");
	 
	document.frm1.submit();
}


function getCompareSheetData(comid)
{
	$("#div_3").empty();
	$("#div_3").html('<div style="height:200px;text-align:center;"><img src="../../img/loading.gif" /></div>');
	$.post("options.php?id=49",{comid:""+comid+""}, function(html) {
		$("#div_3").empty();
		$("#div_3").html(html);
	});
}

function getPdf(comid)
{
	location.href='allPdf.php?id=71&comid='+comid;
}

</script>
		
</body>
</html>