<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if(@$_REQUEST['action'] == 'submit1')
{
	$msg = $obj->deleteFCETempleteRecordsNew();
	header('Location:./coa_list.php?msg='.$msg);
}

if(@$_REQUEST['action'] == 'submit2')
{
	$msg = $obj->deleteFCETempleteRecordsNew();
	header('Location:./coa_list.php?msg='.$msg);
}

if(@$_REQUEST['action'] == 'submit3')
{
	$msg = $obj->deleteFCETempleteRecordsNew();
	header('Location:./coa_list.php?msg='.$msg);
}

$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
if(isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$_SESSION['selBType'] = $_REQUEST['selBType'];
	$estimatetype = $selBType = $_SESSION['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$estimatetype = $selBType = $_SESSION['selBType'];
}
else
{
	unset($_SESSION['selBType']);
	$selBType = '';
	$estimatetype = 1;
}
$rigts    = explode(',',$obj->getUserRights($_SESSION['uid'],$_SESSION['moduleid'],14));
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(14); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-truck"></i>&nbsp;Contract of Affreightment&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Contract of Affreightment</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> COA added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating COA.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> entry deleted successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
				<h3 style=" text-align:center;">Contract of Affreightment List</h3>
				<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                    
				     <div class="row" style="padding-right:10px; padding-left:10px;">
                         <div class="col-xs-10">
						 <?php if(in_array(2, $rigts)){?>
                            <div style="float:right;"><a href="addcoa.php" title="Add New"><button type="button" class="btn btn-info btn-flat">Add New</button></a></div>
						 <?php }?>
                         </div>
                         <div class="col-xs-2" >
                             <select name="selBType" id="selBType" class="form-control" onChange="getDefaultData();">
                             <option value="">---Select Business Type---</option>
                             <?php echo $obj->getBusinessTypeList1($selBType);?>
                             </select>                  
                         </div><!-- /.col -->
                     </div>
					<div style="height:10px;">
						<input name="txttblid" id="txttblid" type="hidden" value="" />
						<input name="txttblstatus" id="txttblstatus" type="hidden" value="" />
						<input type="hidden" name="action" id="action" value="submit" />
                        <input type="hidden" name="delId" id="delId" value=""/>
					</div>				
					
                    <div class="box-body table-responsive" style="overflow:auto;">
                        <table id="coa_list" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th align="left" valign="middle" width="7%">COA Route</th>
                                    <th align="left" valign="middle" width="7%">COA ID</th>
                                    <th align="left" valign="middle" width="7%">COA No.</th>
                                    <th align="left" valign="middle" width="7%">COA Date</th>
                                    <th align="left" valign="middle" width="7%">Vessel Type</th>
                                    <th align="center" valign="middle" width="7%">Charterer</th>
                                    <th align="center" valign="middle" width="7%">Cargo</th>
                                    <th align="center" valign="middle" width="7%">Min Qty(MT)</th>
                                    <th align="center" valign="middle" width="7%">Duration</th>
                                    <th align="center" valign="middle" width="7%">Total Shipments</th>
                                    <th align="center" valign="middle" width="7%">Shipments Performed</th>
                                    <th align="left" valign="middle" width="7%">Balance Cargo(MT)</th>
                                    <th align="center" valign="middle" width="7%">Nominate</th>
                                    <th align="center" valign="middle" width="7%">Details</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
				</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
            
            <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" style="width:95%">
					 <div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;COA Estimates</h4>
						</div>
						
						<div class="modal-body" id="div_3" style="overflow:auto;">
						</div>
                        
					 </div>
				</div>
			</div>
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src='../../js/plugins/datatables/jquery.dataTables.min.js'></script>
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
 getDefaultData();
});

function getDefaultData()
{
		var table = $('#coa_list').dataTable();
		if(table != null)table.fnDestroy();
	  
		table = $("#coa_list").DataTable( {
			"processing": true,
			"serverSide": true,
			"stateSave": true,
			"order": [[1, 'desc']],
			"ajax": {
				"url": "options.php?id=69",
				"type": "POST",
				"dataSrc": function ( d ) {
					return d.records;
				},
				"data": function ( d ) {d.gridtype=2;d.businesstype=$("#selBType").val()},
			},
			"columns": [
				{ "data": "col1" },
				{ "data": "col2" },
				{ "data": "col3" },
				{ "data": "col4" },
				{ "data": "col5" },
				{ "data": "col6" },
				{ "data": "col7" },
				{ "data": "col8" },
				{ "data": "col9" },
				{ "data": "col10" },
				{ "data": "col11" },
				{ "data": "col12" },
				{ "data": "col13" },
				{ "data": "col14" },
				{ "data": "col15" },
				
			],
			"language": {
				  "zeroRecords": "SORRY CURRENTLY THERE ARE ZERO(0) RECORDS"
				},
			 "fnDrawCallback": function(oSettings, json) {
				  $('[data-toggle="tooltip"]').tooltip();	
				},	
			"columnDefs": [ 
			        {
						"targets": 9,
						"orderable": false
					},
			        {
						"targets": 10,
						"orderable": false
					},
					{
						"targets": 11,
						"orderable": false
					},
					{
						"targets": 12,
						"orderable": false
					},
					{
						"targets": 13,
						"orderable": false
					},
					{
						"targets": 14,
						"orderable": false
					}
				]
					
		});
}

function getDelete(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently?', 'Confirmation', function(r) {
	if(r){ 
		$("#delId").val(var1);
		$("#action").val("submit1");
		document.frm1.submit();
	}
	else{return false;}
	});
}

function getDelete1(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently?', 'Confirmation', function(r) {
	if(r){ 
		$("#delId").val(var1);
		$("#action").val("submit2");
		document.frm1.submit();
	}
	else{return false;}
	});
}

function getListOFEstimate(coaid)
{
	$("#div_3").html('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />');
	$.post("options.php?id=68",{coaid:""+coaid+""}, function(html) {
	$("#div_3").html("");
	$("#div_3").html(html);
	$("#fce_list,#relet_list").dataTable();
	});
}
</script>
		
</body>
</html>