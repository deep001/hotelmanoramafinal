<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_tc.php"; $page_bar="In Ops at a glance";}else if($page == 2){$page_link = "vessel_in_post_tc.php"; $page_bar="Vessels in Post Ops";}else {$page_link = "vessel_in_history_tc.php"; $page_bar="Vessels in History";}

if (@$_REQUEST['action'] == 'submit')
 {
 	if($_REQUEST['txtStatus'] == "1")
	{
		$msg = $obj->insertPdfDetails($_REQUEST['comid']);
	}
	else if($_REQUEST['txtStatus'] == "2")
	{
		$msg = $obj->deletePdfDetails($_REQUEST['comid'],$_REQUEST['txtFileName']);
	}
	else if($_REQUEST['txtStatus'] == "3")
	{
		$msg = $obj->insertRemarksDetails($_REQUEST['comid']);
	}
	
	header('Location:./documents_tc.php?comid='.$_REQUEST['comid'].'&page='.$page);
}

$id 		= $obj->getCharteringEstimateTCData($comid,"TCOUTID");
$obj->viewTCEstimationTempleteRecordsNew($id);	
$pagename 	= basename($_SERVER['PHP_SELF'])."?comid=".$comid."&page=".$page;
$uid	  	= $_SESSION['uid'];
$moduleid 	= $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(21); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops at a glance - TC&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops at a glance - TC&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;<?php echo $page_bar; ?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             DOCUMENTS<input type="hidden" name="txtStatus" id="txtStatus" value="1" /><input type="hidden" name="txtFileName" id="txtFileName" value="" /><input type="hidden" name="action" id="action" value="submit" />
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    
                    
                    <div class="row invoice-info" >
                        <div class="col-sm-4 invoice-col">
                        File Name
                            <address>
                              <input type="text" name="txtFile" id="txtFile" value="" class="form-control" placeholder="File Name" autocomplete="off" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                             &nbsp;
                            <address>
								<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Attachment">
                                    <i class="fa fa-paperclip"></i> Attachment
                                    <input type="file" class="form-control" multiple name="mul_file[]" id="mul_file" title="" data-widget="Add Attachment" data-toggle="tooltip" data-original-title="Add Attachment"/>
                                </div>
							</address>
						</div><!-- /.col -->
                        
						<?php if($rights == 1){ ?>
						<div class="col-sm-4 invoice-col">
                        &nbsp;
                            <address>
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
				           </address>
                        </div><!-- /.col -->
                     <?php } ?>
                       
					</div>
					
					<div class="row">
                      <div class="col-xs-12">
                         <div class="box box-primary">
						    <div class="box-body no-padding">
							  <table class="table table-striped">
							     <thead>
                                  <tr>
                                    <th width="40%" align="left" valign="middle">File Name</th>
                                    <th width="30%" align="center" valign="middle">Upload</th>
                                    <th width="30%" align="center" valign="middle">Details</th>
                                  </tr>	  
                                  
                                </thead>
                                 <tbody>
                                    <?php 
									$qsql = "select * from cp_pdf_details where COMID=".$comid." order by CPID";
									$qres = mysql_query($qsql);
									$qrec = mysql_num_rows($qres);
									if($qrec == 0)
									{
										
									}
									else 
									{$i=1;
										while($qrows = mysql_fetch_assoc($qres))
										{?>
                                        <tr>
                                            <td align="left" valign="middle"><?php echo $qrows['USER_FILE_NAME'];?></td>
                                            <td align="left" valign="middle">
                                            <?php if($qrows['FILE_NAME'] != " "){
												$attachments = explode(",",$qrows['FILE_NAME']);
												$attachments_name = explode(",",$qrows['FILE_NAME_1']);
												for($m=0;$m<count($attachments);$m++)
												{
												?>
                                                  <a href="../../attachment/<?php echo $attachments[$m];?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="Click to view file"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $attachments_name[$m];?></a><br/>
                                            
                                            <?php } }?>
                                            </td>
                                            <td align="center" valign="middle" ><a href="#1" onClick="getValidate2('<?php echo $qrows['FILE_NAME'];?>');"><i class="fa fa-times " style="color:red;"></i></a></td>
                                            </tr>
                                        <?php $i++;}
                                        }?>
                                    </tbody>
                               
                              </table>
                            </div>
                         </div>
                      </div>
                   </div>
               
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                         Open Vessel Details (<?php echo $obj->getVesselIMOData($obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?>) : Attachments
                        </h2>                            
                    </div><!-- /.col -->
                </div>
               
               <div class="row">
                      <div class="col-xs-12">
                         <div class="box box-primary">
						    <div class="box-body no-padding">
							  <table class="table table-striped">
							     <thead>
                                  <tr>
                                    <th width="40%" align="left" valign="middle">File Name</th>
		                            <th width="40%" align="center" valign="middle">Upload</th>
                                  </tr>	  
                                  
                                </thead>
                                <tbody>
                                 
                                    <?php 
									
									if($obj->getFun115() != "")
										{
										$attachments = explode(",",$obj->getFun115());
										$attachments_name = explode(",",$obj->getFun116());
										for($m=0;$m<count($attachments);$m++)
											{
												echo "<tr>";
												echo '<td align="left" valign="middle">'.$attachments_name[$m].'</td>';
												echo '<td align="center" valign="middle">';
												echo '<a target="_blank" href="../../attachment/'.$attachments[$m].'" target="_blank" ><i class="fa fa-external-link"></i>&nbsp;&nbsp;'.$attachments_name[$m].'</a>';
												echo '</td>';
												echo '</tr>';
											}
										}
										else
										{
										}
                                      ?>
                                    </tbody>
                               
                              </table>
                            </div>
                         </div>
                      </div>
                   </div>
                                    
                  <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                         Open Cargo Details (<?php echo $obj->getVesselIMOData($obj->getCharteringEstimateTCData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?>) : Attachments
                        </h2>                            
                    </div><!-- /.col -->
                </div>
               
               <div class="row">
                      <div class="col-xs-12">
                         <div class="box box-primary">
						    <div class="box-body no-padding">
							  <table class="table table-striped">
							     <thead>
                                  <tr>
                                    <th width="40%" align="left" valign="middle">File Name</th>
		                            <th width="40%" align="center" valign="middle">Upload</th>
                                  </tr>	  
                                  
                                </thead>
                                <tbody>
                                 
                                    <?php 
									
									if($obj->getFun169() != " ")
										{
										$attachments = explode(",",$obj->getFun169());
										$attachments_name = explode(",",$obj->getFun170());
										for($m=0;$m<count($attachments);$m++)
											{
												echo "<tr>";
												echo '<td align="left" valign="middle">'.$attachments_name[$m].'</td>';
												echo '<td align="center" valign="middle">';
												echo '<a target="_blank" href="../../attachment/'.$attachments[$m].'" target="_blank" ><i class="fa fa-external-link"></i>&nbsp;&nbsp;'.$attachments_name[$m].'</a>';
												echo '</td>';
												echo '</tr>';
											}
										}
										else
										{
										}
                                      ?>
                                    </tbody>
                               
                              </table>
                            </div>
                         </div>
                      </div>
                   </div>
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$(".areasize").autosize({append: "\n"});
$("#frm1").validate({
rules: {
	txtFile:"required"
	},
messages: {	
	txtFile:"*"
	}
});
});

function getValidate2(file_name)
{
	jConfirm('Are you sure to remove this document permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtStatus").val(2);
			$("#txtFileName").val(file_name);
			document.frm1.submit();
		}
		else{return false;}
		});	
}

function getValidate3()
{
	if($("#txtRemark").val() != "")
	{
			$("#txtStatus").val(3);
			document.frm1.submit();
	}
	else
	{
		jAlert('Please fill remarks.', 'Alert');
		return false;
	}
}
</script>
    </body>
</html>