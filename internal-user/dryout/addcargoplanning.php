<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
{
 	$msg = $obj->insertCargoOpenPositionDetails();
	header('Location:./cargo_planning.php?msg='.$msg);
}
$pagename = basename($_SERVER['PHP_SELF']);

$rigts    = explode(',',$obj->getUserRights($_SESSION['uid'],$_SESSION['moduleid'],18));
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(18); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Cargo Tonnage Book</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="cargo_planning.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             ADD CARGO TONNAGE<select name="selVendor" id="selVendor" style="display:none;"><?php $obj->getVendorListNewUpdate();?></select>
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                           CP ID
                            <address>
                              <input type="text" name="txtCID" id="txtCID" readonly class="form-control"  placeholder="Cargo ID" autocomplete="off" value="<?php echo $obj->getMessageNumberForCP();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                          Shipper
                            <address>
                                <select  name="selShipper" class="form-control required" id="selShipper" ></select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                          Charterer
                            <address>
                                <select  name="selCharterer" class="form-control required" id="selCharterer" ></select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                            Owner
                            <address>
                               <select  name="selOwner" class="form-control required" id="selOwner" ></select>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                         Receiver 
                            <address>
                                <select  name="selReceiver" class="form-control required" id="selReceiver" ></select>
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-3 invoice-col">
                           Business Type
                            <address>
                               <select  name="selBType" class="form-control required" id="selBType" onChange="getVesselTypeList(),getCargoTypList(),getShowhide();">
                               <option value="">----Select from list----</option>
								<?php 
                                $obj->getBusinessTypeList1();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                           Cargo 
                            <address>
                               <select  name="selCargo" class="form-control required" id="selCargo" >
                               <option value="">----Select from list----</option>
								<?php 
                                $obj->getMaterialTypeForMultiple();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                            Cargo Stem Size(MT)
                            <address>
                                <input type="text"  name="txtQty"  class="form-control numeric required" id="txtQty" placeholder="Cargo Stem Size(MT)" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        
					</div>
                    <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                         Tolerance/Terms
                            <address>
                                <input type="text"  name="txtTolerance"  class="form-control required" id="txtTolerance" placeholder="Tolerance/Terms" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col basef">
                          Base Freight (USD/MT)
                            <address>
                                <input type="text"  name="txtFreight"  class="form-control numeric" id="txtFreight" placeholder="Base Freight (USD/MT)" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
						<div class = "newone">
                        <div class="col-sm-3 invoice-col">
                     &nbsp;
                          <address>
                               <input name="rdoWstype" id="rdoWstype1" type="radio" style="cursor:pointer;" value="1"     checked  />&nbsp;&nbsp;WS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:10px; font-style:italic;"></span><input name="rdoWstype" id="rdoWstype2" type="radio" style="cursor:pointer;" value="2" />&nbsp;&nbsp;Lumpsum
                           <address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                          &nbsp;
                            <address>
                                <input type="text"  name="txtWsLumpsum"  class="form-control numeric" id="txtWsLumpsum" placeholder="USD" autocomplete="off" value=""/>
                            </address>
                         </div><!-- /.col -->
					     </div>
                        
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                          Basin 
                            <address>
                                <select  name="selBaseIn" class="form-control required" id="selBaseIn">
								<?php 
                                $obj->getBaseinList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                          Bunker Hedge 
                            <address>
                                <select  name="selBunkerHedge" class="form-control required" id="selBunkerHedge">
								<?php 
                                $obj->getLoader();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
                         COA/Spot
                            <address>
                                <select  name="selPlannintType" class="form-control required" id="selPlannintType">
								<?php 
                                $obj->getCOASpotList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                          COA/CP Date
                            <address>
                                <input type="text"  name="txtCOADate"  class="form-control required" id="txtCOADate" placeholder="dd-mm-yyyy" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">&nbsp;</div>
                    </div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Load Port
                            <address>
                               <select  name="selLPort" class="form-control required" id="selLPort">
								<?php 
                                $obj->getPortList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Discharge Port
                            <address>
                                <select  name="selDPort" class="form-control required" id="selDPort">
								<?php 
                                $obj->getPortList();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Vessel Type 
                            <address>
                                <select  name="selVesselType" class="form-control required" id="selVesselType">
								
                                </select>
                             </address>
                        </div><!-- /.col -->
					</div>
					<div class="row invoice-info" >
						<div class="col-sm-4 invoice-col">
							LayCan Start Date
							<address>
								<input type="text" name="txtFDate" id="txtFDate" class="form-control required"  placeholder="dd-mm-yyyy" autocomplete="off" value=""/>
							 </address>
						</div><!-- /.col -->
						
						 <div class="col-sm-4 invoice-col">
						   LayCan Finish Date
							<address>
								<input type="text" name="txtTDate" id="txtTDate" class="form-control required"  placeholder="dd-mm-yyyy" autocomplete="off" value=""/>
							 </address>
						</div><!-- /.col -->
							
					
                        <div class="col-sm-4 invoice-col">
                          Cargo Relet/Voyage
                            <address>
                               <select  name="selCType" class="form-control required" id="selCType">
								<?php 
                                $obj->getBusinessTypeForCargoPlanning('');
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                    </div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Status
                            <address>
                                <select  name="selStatus" class="form-control required" id="selStatus">
								<?php 
                                $obj->getStatusForCargoPlanning();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
						   Nom Clause
							<address>
								<input type="text" name="txtNomClause" id="txtNomClause required" class="form-control"  placeholder="" autocomplete="off" value=""/>
							 </address>
						</div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
							Remarks 
							<address>
							   <textarea class="form-control areasize required" name="txtRemarks" id="txtRemarks" rows="2" placeholder="Remarks" ></textarea>
							</address>
						</div><!-- /.col -->
                    </div>
					
					<div class="box-footer" align="right">
                    <?php if(in_array(2, $rigts)){?>
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Save as Draft</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-success btn-flat" onClick="return getValidate(1);">Send to Checker</button> 
					<?php }?>
                    <input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<!--<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>-->
<script type="text/javascript">
$(document).ready(function(){ 
$("#selShipper,#selCharterer,#selOwner,#selReceiver").html($("#selVendor").html());
$(".numeric").numeric();
$(".areasize").autosize({append: "\n"});
$('#txtFDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#txtTDate').datepicker('setStartDate', new Date(getString($(this).val())));
 });
 
$('#txtTDate,#txtCOADate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){   });

function getString(var1)
{
  var var2 = var1.split('-');  
  return var2[2]+'/'+var2[1]+'/'+var2[0];
}


$(".areasize").autosize({append: "\n"});
$("[id^=ui-datepicker-div]").hide();
$("#selVOPID").val(" ");


$("#frm1").validate({
	rules: {
		     txtFreight: {required:function(){
			   if($("#selBType").val() == '1' || $("#selBType").val() == '3')
			   {
			       return true;
			   }
			   else
			   {
			       return false;
			   }
			  }},
			txtWsLumpsum: {required:function(){
			   if($("#selBType").val() == '2')
			   {
			       return true;
			   }
			   else
			   {
			       return false;
			   }
			  }},
		},
	messages: {
		
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
getVesselTypeList();
getCargoTypList();
//getShowhide();
	
	
	
});


function getVesselTypeList()
{
    var vesseltypelist = <?php echo $obj->getVesselTypeListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selVesselType');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selVesselType");
	$.each(vesseltypelist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selVesselType");
	});
}


function getCargoTypList()
{
    var cargolist = <?php echo $obj->getCargoListJson();?>;	
	var system = $('#selBType').val();  
	var sel1 = $('#selCargo');	
	var options1 = sel1.attr('options');
	$('option', sel1).remove();	
	$("<option value=''>---Select from list---</option>").prependTo("#selCargo");
	$.each(cargolist[system], function(index, array) {
		$("<option value='"+array['id']+"'>"+array['name']+"</option>").appendTo("#selCargo");
	});
}


function getValidate(val)
{
	$("#upstatus").val(val);
	return true;
}
	
function getShowhide()
{
		    var selBType = $("#selBType").val();
		    $("#selBType").find("option:selected").each(function(){
            var optionValue = $("#selBType").attr("value");
            if(optionValue == '2'){
                $(".newone").show();
                $(".basef").hide();
            } else { 
                $(".newone").hide();
                $(".basef").show();
            }
        });
} 
	//function fillupp
</script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
$(document).ready(function() {
		 $('#rdoWstype1').click(function() {
		 if ($("input[name='rdoWstype']:checked").val() == '1')
            {
			$( "#txtWsLumpsum" ).attr('placeholder','WS')
			//$("#rdoWstype2").removeAttr("2");
			}
         });
		 $('#rdoWstype2').click(function() {
		 if ($("input[name='rdoWstype']:checked").val() == '2')
            {
			$( "#txtWsLumpsum" ).attr('placeholder','USD')
			//$("#rdoWstype1").removeAttr("1");
			}
         });
});
</script>
    </body>
</html>