<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "payment_grid.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "payment_grid.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "payment_grid.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "payment_gridcoa.php"; $page_bar="In Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else if($page == 5){$page_link = "payment_gridcoa.php"; $page_bar="In Post Ops at a glance - COA";$nav=16;$subtitle = 'COA';}else{$page_link = "payment_gridcoa.php"; $page_bar="Vessels in History - COA";$nav=16;$subtitle = 'COA';}

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertInvoiceHireDetails();
	$msg = explode(",",$msg);
	header('Location:./invoice_hire.php?msg='.$msg[0].'&comid='.$comid.'&page='.$page);
}
if (@$_REQUEST['action2'] == 'submit1')
{
	$msg = $obj->insertHirePaymentReceivedDetails();
	$msg = explode("_",$msg);
	header('Location:./invoice_hire.php?msg='.$msg[0].'&comid='.$comid.'&page='.$page);
}
if (@$_REQUEST['action11'] == 'submit2')
{
	$msg = $obj->deleteHireInvoiceRecords();
	$msg = explode(",",$msg);
	header('Location:./invoice_hire.php?msg='.$msg[0].'&comid='.$comid.'&page='.$page);
}

$id = $obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($id);
$arr  = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$id);
$vendorid = $obj->getFun140();
$lp_name = array(); 
$dp_name = array();

for($j=0;$j<count($arr);$j++)
{   $arr_explode = explode('@@',$arr[$j]);
    
    if($arr_explode[0] == "LP" && $arr_explode[8]==''){$lp_name[]= $obj->getPortNameBasedOnID($arr_explode[1]);}
	if($arr_explode[0] == "DP" && $arr_explode[8]==''){$dp_name[]= $obj->getPortNameBasedOnID($arr_explode[1]);}
}

$lp_name  = implode(', ',$lp_name);
$dp_name  = implode(', ',$dp_name);

if($obj->getFun175()!='')
{
	$sqlcoa = "select * from coa_master where COAID='".$obj->getFun175()."'";
	$resultcoa = mysql_query($sqlcoa) or die($sqlcoa);
	$rowscoa   = mysql_fetch_assoc($resultcoa);
	$currency = $rowscoa['CURRENCY'];
}else
{
	$currency = 'USD';
}

$bunkergrade = array( 1 =>1 , 2 =>2, 3 =>3 , 4 =>4);
$bunkergrade1= array( 1 =>'HSFO' , 2 =>'LSFO', 3 =>'HSDO' , 4 =>'LSDO');
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$_REQUEST['comid'].'&page='.$_REQUEST['page'];
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
	$(".autosize").autosize({append: "\n"});
	$("#frm1").validate({
		rules: {
		selFromOwner: {required: true},
		selIType: {required: true},
		txtInvNo: {required: true},
		txtInvDate: {required: true},
		txtDueDate:{required: true},
		txtExchangeRate:{required: true},
		selExchangeCurrency:{required: true},
		txtPaymentTerms:{required: true},
		selBankingDetails:{required: true},
		txtHireFrom:{required: true},
		txtHireTo:{required: true},
		txtAttenName:{required: true},
		txtExchangeDate:{required: true}
		},
	messages: {
		selIType: {required: "*"},
		txtInvNo: {required: true},
		txtInvDate: {required: true},
		txtDueDate:{required: "*"},
		txtExchangeRate:{required: "*"},
		selExchangeCurrency:{required: "*"},
		txtPaymentTerms:{required: "*"},
		selBankingDetails:{required: "*"},
		txtHireFrom:{required: "*"},
		txtHireTo:{required: "*"},
		txtAttenName:{required: "*"},
		txtExchangeDate:{required: "*"}
		},
	submitHandler: function(form)  {
		    jConfirm('Are you sure you want to save this data ?', 'Confirmation', function(r) {
			if(r){
				jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
				$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
				$("#popup_content").css({"background":"none","text-align":"center"});
				$("#popup_ok,#popup_title").hide();  
				form.submit();
			}
			else{return false;}
			});
		}
	});

	$(".numeric").numeric();
	$('.datepicker').datepicker({
		format: 'dd-mm-yyyy',
		autoclose:true
	});
	
	$('#txtHireFrom,#txtHireTo').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
		}).on('changeDate', function(){
		getFullClaculation();
	});
	
	$('#ChkShowOffHire').on('ifChecked', function () { 
	   $("#offHireTR,#offHireTR1,#offHireTR2").show();
	   getFullClaculation();
	});
	 
	$('#ChkShowOffHire').on('ifUnchecked', function () { 
	   $("#offHireTR,#offHireTR1,#offHireTR2").hide();
	   getFullClaculation();
	});
	
	$("#ChkDeliveryBunker").on('ifChecked', function () { 
	   $("[id^=DivBunkerDelivery_]").show();
	   getFullClaculation();
	});
	
	$("#ChkDeliveryBunker").on('ifUnchecked', function () { 
	   $("[id^=DivBunkerDelivery_]").hide();
	   getFullClaculation();
	});
	
	$("#ChkReDeliveryBunker").on('ifChecked', function () { 
	   $("[id^=DivBunkerReDelivery_]").show();
	   getFullClaculation();
	});
	
	$("#ChkReDeliveryBunker").on('ifUnchecked', function () { 
	   $("[id^=DivBunkerReDelivery_]").hide();
	   getFullClaculation();
	});
	
	getBankingDetailsData();
	getFullClaculation();
	
});


function getTimeDiff(date2,date1)
{	
	if(date1 != '' &&  date2 != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat(date2);
		start_actual_time    =  getDateWithSpecificFormat(date1);
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000;
		return days.toFixed(5);	
	}
}

function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' '+dateSplit1[1];
	return currentDate;
}

function AddNewAddRow()
{
	var id = $("#txtAddID").val();
	if($("#txtAddDes_"+id).val() != "" && $("#txtAddDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="add_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td colspan="2"><textarea class="form-control areasize" name="txtAddDes_'+id+'" id="txtAddDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtAddDesAmt_'+id+'" id="txtAddDesAmt_'+id+'" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td></tr>').appendTo("#tblAdd");
		$("#txtAddID").val(id);
		$(".numeric").numeric();	
	}
}


function removeAddRow(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#add_Row_"+var1).remove();
					getFullClaculation();
				 }
			else{return false;}
			});
}



function SubNewSubRow()
{
	var id = $("#txtSubID").val();
	if($("#txtSubDes_"+id).val() != "" && $("#txtSubDesAmt_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="Sub_Row_'+id+'"><td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td><td colspan="2"><textarea class="form-control areasize" name="txtSubDes_'+id+'" id="txtSubDes_'+id+'" rows="2" placeholder="Description..."></textarea></td><td><input type="text" name="txtSubDesAmt_'+id+'" id="txtSubDesAmt_'+id+'" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td></tr>').appendTo("#tblSub");
		$("#txtSubID").val(id);
		$(".numeric").numeric();
	}
}


function removeSubRow(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#Sub_Row_"+var1).remove();
				getFullClaculation();
			 }
		else{return false;}
		});
}



function AddNewDELRow()
{
	var id = $("#txtAddDELID").val();
	if($("#txtDelBunkerQty_"+id).val() != "" && $("#selDELBunker_"+id).val() != "" && $("#txtDelBunkerPrice_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="DivBunkerDelivery_'+id+'"><td><select name="selDELBunker_'+id+'" id="selDELBunker_'+id+'" class="select form-control" onChange="getFullClaculation();"><?php $obj->getBunkerList();?></select></td><td><input type="text" name="txtDelBunkerQty_'+id+'" id="txtDelBunkerQty_'+id+'" class="form-control numeric" onKeyUp="getFullClaculation();" placeholder="0.00" autocomplete="off" value=""/></td><td><input type="text" name="txtDelBunkerPrice_'+id+'" id="txtDelBunkerPrice_'+id+'" class="form-control numeric" onKeyUp="getFullClaculation();" placeholder="0.00" autocomplete="off" value=""/></td><td><input type="text" name="txtDelBunkerAmt_'+id+'" id="txtDelBunkerAmt_'+id+'" class="form-control"  placeholder="0.00" readonly autocomplete="off" value=""/></td></tr>').appendTo("#bunkerDeliverybody");
		$("#txtAddDELID").val(id);
		$(".numeric").numeric();	
	}
}

function AddNewREDELRow()
{
	var id = $("#txtAddREDELID").val();
	if($("#txtReDelBunkerQty_"+id).val() != "" && $("#selReDELBunker_"+id).val() != "" && $("#txtReDelBunkerPrice_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="DivBunkerReDelivery_'+id+'"><td><select name="selReDELBunker_'+id+'" id="selReDELBunker_'+id+'" class="select form-control" onChange="getFullClaculation();"><?php $obj->getBunkerList();?></select></td><td><input type="text" name="txtReDelBunkerQty_'+id+'" id="txtReDelBunkerQty_'+id+'" class="form-control numeric" onKeyUp="getFullClaculation();" placeholder="0.00" autocomplete="off" value=""/></td><td><input type="text" name="txtReDelBunkerPrice_'+id+'" id="txtReDelBunkerPrice_'+id+'" class="form-control numeric" placeholder="0.00" onKeyUp="getFullClaculation();" autocomplete="off" value=""/></td><td><input type="text" name="txtReDelBunkerAmt_'+id+'" id="txtReDelBunkerAmt_'+id+'" class="form-control"  placeholder="0.00" readonly autocomplete="off" value=""/></td></tr>').appendTo("#bunkerReDeliverybody");
		$("#txtAddREDELID").val(id);
		$(".numeric").numeric();	
	}
}

function getFullClaculation()
{
    var delDays1 = parseFloat(getTimeDiff($("#txtHireTo").val(),$("#txtHireFrom").val()));
	if(isNaN(delDays1)){delDays1 = 0.0000;}
	$("#txtHireDays").val(parseFloat(delDays1).toFixed(4));
	
	var txtHireperDay = parseFloat($("#txtHireperDay").val());
	if(isNaN(txtHireperDay)){txtHireperDay = 0.00;}
	
	var nettHire = parseFloat(txtHireperDay) * parseFloat(delDays1);
	if(isNaN(nettHire)){nettHire = 0.00;}
	$("#txtTotalHire").val(parseFloat(nettHire).toFixed(2));
	
	var txtNetHire = parseFloat($("#txtTotalTCHire").val());
	if(isNaN(txtNetHire)){txtNetHire = 0.00;}
	
	for(var i = 1;i<=$("#txtAddDELID").val();i++)
	{
		if($("#selDELBunker_"+i).val()!='')
		{
			if($("#txtDelBunkerQty_"+i).val() == ""){var txtDelBunkerQty = 0;}else{var txtDelBunkerQty = $("#txtDelBunkerQty_"+i).val();}
			if($("#txtDelBunkerPrice_"+i).val() == ""){var txtDelBunkerPrice = 0;}else{var txtDelBunkerPrice = $("#txtDelBunkerPrice_"+i).val();}
			var txtDelBunkerAmt = parseFloat(txtDelBunkerQty)*parseFloat(txtDelBunkerPrice);if(isNaN(txtDelBunkerAmt)){txtDelBunkerAmt = 0.00;}
			$("#txtDelBunkerAmt_"+i).val(parseFloat(txtDelBunkerAmt).toFixed(2));
		}
		else
		{
			$("#txtDelBunkerQty_"+i).val('');
			$("#txtDelBunkerPrice_"+i).val('');
			$("#txtDelBunkerAmt_"+i).val('')
		}
	}
	
	for(var i = 1;i<=$("#txtAddREDELID").val();i++)
	{
		if($("#selReDELBunker_"+i).val()!='')
		{
			if($("#txtReDelBunkerQty_"+i).val() == ""){var txtReDelBunkerQty = 0;}else{var txtReDelBunkerQty = $("#txtReDelBunkerQty_"+i).val();}
			if($("#txtReDelBunkerPrice_"+i).val() == ""){var txtReDelBunkerPrice = 0;}else{var txtReDelBunkerPrice = $("#txtReDelBunkerPrice_"+i).val();}
			var txtReDelBunkerAmt = parseFloat(txtReDelBunkerQty)*parseFloat(txtReDelBunkerPrice);if(isNaN(txtReDelBunkerAmt)){txtReDelBunkerAmt = 0.00;}
			$("#txtReDelBunkerAmt_"+i).val(parseFloat(txtReDelBunkerAmt).toFixed(2));
		}
		else
		{
			$("#txtReDelBunkerQty_"+i).val('');
			$("#txtReDelBunkerPrice_"+i).val('');
			$("#txtReDelBunkerAmt_"+i).val('')
		}
	}
	
	if($("#txtCVEOffHireMonth").val() == ""){var txtCVEOffHireMonth = 0;}else{var txtCVEOffHireMonth = $("#txtCVEOffHireMonth").val();}
	if($("#txtOffHireDays").val() == ""){var txtOffHireDays = 0;}else{var txtOffHireDays = $("#txtOffHireDays").val();}
	var txtOffHire = parseFloat(txtHireperDay)*parseFloat(txtOffHireDays);
	$("#txtOffHire").val(parseFloat(txtOffHire).toFixed(2));
	var txtCVEOFFHIRE_Amt = parseFloat(parseFloat(parseFloat(parseFloat(txtCVEOffHireMonth)*12)/365)*parseFloat(txtOffHireDays));if(isNaN(txtCVEOFFHIRE_Amt)){txtCVEOFFHIRE_Amt = 0.00;}
	$("#txtCVEOFFHIRE_Amt").val(parseFloat(txtCVEOFFHIRE_Amt).toFixed(2));
	
	if($("#txtCVEMonth").val() == ""){var txtCVEMonth = 0;}else{var txtCVEMonth = $("#txtCVEMonth").val();}
	var txtCVE_Amt = parseFloat(parseFloat(parseFloat(parseFloat(txtCVEMonth)*12)/365)*parseFloat(delDays1));if(isNaN(txtCVE_Amt)){txtCVE_Amt = 0.00;}
	$("#txtCVE_Amt").val(parseFloat(txtCVE_Amt).toFixed(2));
	
	if($("#txtAddrCommPer").val() == ""){var txtAddrCommPer = 0;}else{var txtAddrCommPer = $("#txtAddrCommPer").val();}
	var txtAddrCommAmt = parseFloat(parseFloat(parseFloat(nettHire)*parseFloat(txtAddrCommPer))/100);if(isNaN(txtAddrCommAmt)){txtAddrCommAmt = 0.00;}
	$("#txtAddrCommAmt").val(parseFloat(txtAddrCommAmt).toFixed(2));
	
	if($("#txtBroCommPer").val() == ""){var txtBroCommPer = 0;}else{var txtBroCommPer = $("#txtBroCommPer").val();}
	var txtBroCommAmt = parseFloat(parseFloat(parseFloat(nettHire)*parseFloat(txtBroCommPer))/100);if(isNaN(txtBroCommAmt)){txtBroCommAmt = 0.00;}
	$("#txtBroCommAmt").val(parseFloat(txtBroCommAmt).toFixed(2));		
	
	var txthddnAmt = parseFloat($("[id^=txthddnUsdAmt_]").sum()).toFixed(2);
	
	
	var otheradd = parseFloat($("[id^=txtAddDesAmt_]").sum()).toFixed(2);
	otheradd = parseFloat(otheradd) + parseFloat(txtCVE_Amt);
	var otherless = parseFloat($("[id^=txtSubDesAmt_]").sum()).toFixed(2);
	
	if($("#ChkShowOffHire").is(":checked"))
	{
	    var txtOffHire = parseFloat($("#txtOffHire").val());if(isNaN(txtOffHire)){txtOffHire = 0.00;}
		otherless = parseFloat(otherless) + parseFloat(txtOffHire.toFixed(2));
		otherless = parseFloat(otherless) + parseFloat($("#txtCVEOFFHIRE_Amt").val());
	}
	
	if($("#ChkDeliveryBunker").is(":checked"))
	{
		otheradd = parseFloat(otheradd) + parseFloat($("[id^=txtDelBunkerAmt_]").sum());
	}
	
	if($("#ChkReDeliveryBunker").is(":checked"))
	{
		otherless = parseFloat(otherless) + parseFloat($("[id^=txtReDelBunkerAmt_]").sum());
	}
	otherless = parseFloat(otherless) + parseFloat($("[id^=txtPrevInvAmt_]").sum());
	
	otherless = parseFloat(otherless) + parseFloat(txtAddrCommAmt);
	otherless = parseFloat(otherless) + parseFloat(txtBroCommAmt);
	
	var finalamt = parseFloat(nettHire) + parseFloat(otheradd) - parseFloat(otherless);
	$("#txtFinalAmt").val(parseFloat(finalamt).toFixed(2));
	
	var txtBalanceShipOwner = parseFloat(txtNetHire) - parseFloat(finalamt) - parseFloat(txthddnAmt);
	$("#txtBalanceShipOwner").val(parseFloat(txtBalanceShipOwner).toFixed(2));
}

function getValidate(var1)
{
	
		$("#txtStatus").val(var1);
		return true;
	
}

function getDelete(invoiceid,var1)
{ 
    jConfirm('Are you sure you want to delete this statement permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#action11").val('submit2');
			
			$("#txtDEL_ID").val(invoiceid);
			document.frm3.submit();
	    }
		else{return false;}
		});
}

function getBankingDetailsData()
{   
	var1 = $('#selBankingDetails').val();
	if($("#selBankingDetails").val()!="")
	{
		$.post("options.php?id=43",{selNOB:""+$("#selBankingDetails").val()+""}, function(html) {
			if(html!="")
			{
				$("#bankingrow").show();
				var res = html.split("@@@@");
				$('#span_1_0').text(res[0]);
				$('#span_1_1').text(res[1]);
				$('#span_1_2').text(res[2]);
				$('#span_1_3').text(res[3]);
				$('#span_1_4').text(res[4]);
				$('#span_1_5').text(res[5]);
				$('#span_1_6').text(res[6]);
				$('#span_1_7').text(res[7]);
				$('#span_1_8').text(res[8]);
				$('#span_1_9').text(res[9]);
				$('#span_1_10').text(res[10]);
			}
		});
	}
	else

	{
	    $("#bankingrow").hide();	
		$('#span_1_0').text("");
	    $('#span_1_1').text("");
		$('#span_1_2').text("");
		$('#span_1_3').text("");
		$('#span_1_4').text("");
		$('#span_1_5').text("");
		$('#span_1_6').text("");
		$('#span_1_7').text("");
		$('#span_1_8').text("");
		$('#span_1_9').text("");
		$('#span_1_10').text("");
	}
}

function getValid()
{
	if($('#txtP_PR').val() == '' || $('#txtP_Date').val() =='' || $('#txtP_Remarks').val()=='')
	{
		jAlert('Please fill the Payment Received & Date & Remarks', 'Alert');
		return false;
	}
	else
	{
		var file_temp_name = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
	    $('#txtCRMFILE1').val(file_temp_name);
	    var file_actual_name = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
	    $('#txtCRMNAME1').val(file_actual_name);
		document.frm2.submit();
		return true;
	}
}


function openWin(var1)
{
	$("#divPayment").empty();
	$("#divPayment").html('<div><img src="../../img/ajax-loader.gif" /><br><b>Loading...</b></div>'); 
	$.post("options.php?id=45",{invoiceid:""+var1+""}, function(html) {
		$("#divPayment").empty();
		$("#divPayment").append(html);
		if($("#txtP_PR").val() > 0)
		{
			$("#btnhide").hide();	
		}
		$("#txtP_PR").numeric();
        $('#txtP_Remarks').autosize({append: "\n"});
		$('#txtP_Date').datepicker({
			format: 'dd-mm-yyyy',
			autoclose:true
		});
	});
}

function Del_Upload1(var2)
{
	jConfirm('Are you sure you want to delete this upload permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file1_'+var2).remove();
	}
	});
}

</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?>&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
                        <?php 
							if(isset($_REQUEST['msg'])){
								$msg = $_REQUEST['msg'];
								if($msg == 0){?>
									<div class="alert alert-success alert-dismissable">
										<i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<b>Congratulations!</b> Hire Statement added/updated successfully.
									</div>
								<?php }?>
								<?php if($msg == 1){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Sorry!</b> Hire Statement can not be created.
								</div>
								<?php }?>
								<?php if($msg == 2){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Congratulations!</b> Hire Statement deleted successfully.
								</div>
								<?php }?>
                                <?php if($msg == 3){?>
								<div class="alert alert-success alert-dismissable">
									<i class="fa fa-ban"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<b>Congratulations!</b> Payment paid details saved successfully.
								</div>
								<?php }?>
						<?php }?>
				<!--   content put here..................-->
                <?php
				$sql = "SELECT * from invoice_hire_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and STATUS=0";
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				?>	
				<div align="right"><?php if($rec>0){?><a href="allPdf.php?id=46&comid=<?php echo $comid; ?>&invoiceid=<?php echo $rows['INVOICEID']; ?>" title="Pdf"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a><?php }?><a href="<?php echo $page_link;?>?comid=<?php echo $comid;?>&page=<?php echo $page;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
				
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12" style="text-align:center;">
								<h2 class="page-header">
								HIRE STATEMENT CREATION
                                <input type="hidden" name="txtInID" id="txtInID" value="<?php echo $rows['INVOICEID'];?>"/>
                                <input type="hidden" name="txtFCAID" id="txtFCAID" value="<?php echo $obj->getFun1();?>"/>
								</h2>                            
							</div><!-- /.col -->
						</div>
					
						<div class="row invoice-info">
							<div class="col-sm-3 invoice-col">
							   <strong>Creating Company :</strong>
								<address>
                                     <select  name="selFromOwner" class="select form-control" id="selFromOwner">
										<?php $obj->getVendorListNewForCOA(11);?>
									</select>
									<script>$('#selFromOwner').val('<?php echo $rows['SHIP_OWNER'];?>');</script>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-1 invoice-col">
                            </div>
							<div class="col-sm-3 invoice-col">
							   <strong>To :</strong>
								<address>
									<?php echo $obj->getVendorListNewBasedOnID($vendorid,"NAME");?><br/>
                                    <?php echo $obj->getVendorListNewData($vendorid,"STREET_1");?><br/>
                                    <?php if($obj->getVendorListNewData($vendorid,"STREET_2")!=''){echo $obj->getVendorListNewData($vendorid,"STREET_2").'<br/>';}?>
                                     <?php echo $obj->getVendorListNewData($vendorid,"CITY");?> <?php echo $obj->getVendorListNewData($vendorid,"COUNTRY");?> <?php echo $obj->getVendorListNewData($vendorid,"CITY_POSTAL_CODE");?>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-1 invoice-col">
                            </div>
							<div class="col-sm-3 invoice-col">
								<strong>Hire Details :</strong>
								<address>
								    Nom ID :- <?php echo $obj->getCompareTableData($comid,"MESSAGE");?><br/>
                                    Vessel :- <?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?><br/>
                                    CP Date :- <?php
								   $cpdate = $obj->getOpenCargoPositionData($obj->getCompareEstimateData($comid,"OPEN_CARGOID"),'COA_DATE');
								   if($cpdate=="" || $cpdate=="0000-00-00" || $cpdate=="1970-01-01"){$cpdate = ""; }
								   else{$cpdate = date("d-m-Y",strtotime($obj->getOpenCargoPositionData($obj->getCompareEstimateData($comid,"OPEN_CARGOID"),'COA_DATE'))); }
								    echo $cpdate; ?><br/>
                                    Loading Port :- <?php echo $lp_name;?><br/>
                                    Discharging Port :- <?php echo $dp_name;?>
								</address>
							</div><!-- /.col -->
						</div>
						<br/>
						<div class="row invoice-info">
						   <div class="col-sm-5 invoice-col">
                               <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th width="47%" style="width:50%;padding-top:15px;padding-bottom:15px;">Statement Type :</th>
                                            <td width="53%"><select  name="selIType" class="select form-control" id="selIType"><?php $obj->getInvoiceType();?></select></td>
                                        </tr>
                                        <script>$("#selIType").val('<?php echo $rows['INVOICE_TYPE'];?>');</script>
                                        
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Hire Statement Number :</th>
                                            <td><input type="text" name="txtInvNo" id="txtInvNo" class="form-control"  placeholder="Hire Number" autocomplete="off" value="<?php echo $rows['INVOICE_NO'];?>"/></td>
                                        </tr>
                                        <?php
										$invdate = date('d-m-Y',strtotime($rows['INVOICE_DATE']));
										if($invdate=='01-01-1970'){$invdate = '';}
										$duedate = date('d-m-Y',strtotime($rows['DUE_DATE']));
										if($duedate=='01-01-1970'){$duedate = '';}
										$exchangeDate = date('d-m-Y',strtotime($rows['EXCHANGE_DATE']));
										if($exchangeDate=='01-01-1970'){$exchangeDate = '';}
										?>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Hire Statement Date :</th>
                                            <td><input type="text" name="txtInvDate" id="txtInvDate" class="form-control datepicker"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $invdate;?>"/></td>
                                        </tr>
                                        
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Exchange Rate :</th>
                                            <td><input type="text" name="txtExchangeRate" id="txtExchangeRate" class="form-control numeric"  placeholder="Exchange Rate" autocomplete="off" value="<?php echo $rows['EXCHANGE_RATE'];?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Exchange Date :</th>
                                            <td><input type="text" name="txtExchangeDate" id="txtExchangeDate" class="form-control datepicker"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $exchangeDate;?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="width:50%;padding-top:15px;padding-bottom:15px;">Exchange To Currency :</th>
                                            <td><select  name="selExchangeCurrency" class="select form-control" id="selExchangeCurrency"><?php $obj->getCurrencyList();?></select></td>
                                        </tr>
                                        <script>$("#selExchangeCurrency").val('<?php echo $rows['CURRENCY'];?>');</script>
                                        
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Payment Terms :</th>
                                            <td><input type="text" name="txtPaymentTerms" id="txtPaymentTerms" class="form-control"  placeholder="Payment Terms" autocomplete="off" value="<?php echo $rows['PAYMENT_TERMS'];?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;">Description :</th>
                                            <td><textarea name="txtDesc" id="txtDesc" class="form-control autosize" rows="3" placeholder="Description"><?php echo $rows['DESCRIPTION'];?></textarea></td>
                                        </tr>
                                        
                                     </tbody>
                                     
                                   </table>
                               </div>
						   </div><!-- /.col -->
						   <div class="col-sm-7 invoice-col">
							   <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;" colspan="3">Total Voyage Days(Sea & Port):</th>
                                            <td width="30%"><input type="text" name="txtEstHireDays" id="txtEstHireDays" class="form-control" readonly style="background-color:#eee;"  placeholder="Hire Days" autocomplete="off" value="<?php echo $obj->getFun62();?>"/>
                                        </tr>
                                        
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;" colspan="3">Daily Time Charter(<?php echo $currency;?>/Day) :</th>
                                            <td><input type="text" name="txtHireperDay" id="txtHireperDay" class="form-control"  readonly style="background-color:#eee;" placeholder="Daily Hire(USD/Day)" autocomplete="off" value="<?php echo $obj->getFun91();?>"/></td>
                                        </tr>
                                        <?php
										$txtHireFrom = date('d-m-Y H:i',strtotime($rows['HIRE_FROM']));
										if($txtHireFrom=='01-01-1970 00:00' || $txtHireFrom=='01-01-1970 08:00' || $txtHireFrom=='01-01-1970 01:00'){$txtHireFrom = '';}
										$txtHireTo = date('d-m-Y H:i',strtotime($rows['HIRE_TO']));
										if($txtHireTo=='01-01-1970 00:00' || $txtHireTo=='01-01-1970 08:00' || $txtHireTo=='01-01-1970 01:00'){$txtHireTo = '';}
										?>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;" colspan="3">Hire From :</th>
                                            <td><input type="text" name="txtHireFrom" id="txtHireFrom" class="form-control" onKeyUp="getFullClaculation();"  placeholder="dd-mm-yyyy hh:mm" autocomplete="off" value="<?php echo $txtHireFrom;?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;" colspan="3">Hire To :</th>
                                            <td><input type="text" name="txtHireTo" id="txtHireTo" class="form-control" onKeyUp="getFullClaculation();" placeholder="dd-mm-yyyy hh:mm" autocomplete="off" value="<?php echo $txtHireTo;?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;" colspan="3">Hire Days for Invoice :</th>
                                            <td><input type="text" name="txtHireDays" id="txtHireDays" class="form-control" readonly style="background-color:#eee;"  placeholder="" autocomplete="off" value="<?php echo $rows['HIRE_DAYS'];?>"/></td>
                                        </tr>
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;" colspan="3">Total Hire for this Invoice :</th>
                                            <td><input type="text" name="txtTotalHire" id="txtTotalHire" class="form-control" readonly style="background-color:#eee;"  placeholder="Total Hire" autocomplete="off" value="<?php echo $rows['HIRE_INV_AMT'];?>"/></td>
                                        </tr>
                                        
                                        <tr>
                                            <th style="padding-top:15px;padding-bottom:15px;" colspan="4">Other Add</th>
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="2" style="font-size:13px;"><strong>&nbsp;&nbsp;&nbsp;<i>CVE(<?php echo $currency;?>)</strong></i></td>
                                            <td width="25%"><input type="text" name="txtCVEMonth" id="txtCVEMonth" class="form-control numeric"  placeholder="CVE" autocomplete="off" value="<?php echo $rows['CVE'];?>" onKeyUp="getFullClaculation();"/></td>
                                            <td><input type="text" name="txtCVE_Amt" id="txtCVE_Amt" class="form-control numeric" readonly  placeholder="CVE Amount" autocomplete="off" value="<?php echo $rows['CVE_AMT'];?>"/></td>
                                        </tr>
										
                                     </tbody>
                                     <?php $sql2 = "select * from invoice_hire_slave1 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='ADD'";
										   $res2 = mysql_query($sql2);
										   $num2 = mysql_num_rows($res2);
										   if($num2 >0){$num21 = $num2;}else{$num21 = 1;}
									       ?>
                                      <tbody id="tblAdd">
                                  <?php if($num2 >0)
									    {$i = 0;
										  while($rows2 = mysql_fetch_assoc($res2))
										  {$i++;
										  ?>
										    <tr id="add_Row_<?php echo $i;?>">
										  	    <td width="25%" align="center"><a href="#pr<?php echo $i;?>" onClick="removeAddRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
										     	<td colspan="2"><textarea class="form-control areasize" name="txtAddDes_<?php echo $i;?>" id="txtAddDes_<?php echo $i;?>" rows="2" placeholder="Description..."><?php echo $rows2['DESCRIPTION'];?></textarea></td>
										   	    <td><input type="text" name="txtAddDesAmt_<?php echo $i;?>" id="txtAddDesAmt_<?php echo $i;?>" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="<?php echo $rows2['AMOUNT'];?>" onKeyUp="getFullClaculation();"/></td>
										    </tr>
										<?php }}else{?>
										   <tr id="add_Row_1">
										       <td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
										       <td colspan="2"><textarea class="form-control areasize" name="txtAddDes_1" id="txtAddDes_1" rows="2" placeholder="Description..."></textarea></td>
										       <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td>
										   </tr>
								   <?php }?>
                                       </tbody>
                                       <tbody>
                                           <tr>
                                               <td align="left" colspan="4"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewAddRow();">Add</button><input type="hidden" name="txtAddID" id="txtAddID" value="<?php echo $num21;?>"/></td>
                                           </tr>
                                           <tr>
                                               <th style="padding-top:15px;padding-bottom:15px;" colspan="4">Other Less</th>
                                           </tr>
                                           <tr>
                                               <td colspan="2" style="font-size:13px;"><strong>&nbsp;&nbsp;&nbsp;<i>Address Commission(%)</i></strong></td>
                                               <td><input type="text" name="txtAddrCommPer" id="txtAddrCommPer" class="form-control numeric"  placeholder="Address Commission" autocomplete="off" value="<?php echo $obj->getFun129();?>" onKeyUp="getFullClaculation();"/></td>
                                               <td><input type="text" name="txtAddrCommAmt" id="txtAddrCommAmt" class="form-control numeric" readonly  placeholder="Amount" autocomplete="off" value="<?php echo $rows['ADD_COMM_AMT'];?>"/></td>
                                           </tr>
                                           <tr>
                                               <td colspan="2" style="font-size:13px;"><strong>&nbsp;&nbsp;&nbsp;<i>Broker Commission(%)</i></strong></td>
                                               <td><input type="text" name="txtBroCommPer" id="txtBroCommPer" class="form-control numeric"  placeholder="Broker Commission" autocomplete="off" value="<?php echo $rows['BRO_COMM_PER'];?>" onKeyUp="getFullClaculation();"/></td>
                                               <td><input type="text" name="txtBroCommAmt" id="txtBroCommAmt" class="form-control numeric" readonly  placeholder="Amount" autocomplete="off" value="<?php echo $rows['BRO_COMM_AMT'];?>"/></td>
                                           </tr>
                                      <?php $sql2 = "select * from invoice_hire_slave1 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='SUB'";
											$res2 = mysql_query($sql2);
											$num2 = mysql_num_rows($res2);
											if($num2 >0){$num21 = $num2;}else{$num21 = 1;}?>
                                       </tbody>
                                       <tbody id="tblSub">
                                     <?php if($num2 >0)
										   {$i = 0;
											while($rows2 = mysql_fetch_assoc($res2))
											{$i++;
											?>
											<tr id="Sub_Row_<?php echo $i;?>">
											   <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeSubRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
											   <td colspan="2"><textarea class="form-control areasize" name="txtSubDes_<?php echo $i;?>" id="txtSubDes_<?php echo $i;?>" rows="2" placeholder="Description..."><?php echo $rows2['DESCRIPTION'];?></textarea></td>
											   <td><input type="text" name="txtSubDesAmt_<?php echo $i;?>" id="txtSubDesAmt_<?php echo $i;?>" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="<?php echo $rows2['AMOUNT'];?>" onKeyUp="getFullClaculation();"/></td>
											</tr>
											<?php }}else{?>
											<tr id="Sub_Row_1">
											   <td align="center"><a href="#pr1" onClick="removeSubRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
											   <td colspan="2"><textarea class="form-control areasize" name="txtSubDes_1" id="txtSubDes_1" rows="2" placeholder="Description..."></textarea></td>
											   <td><input type="text" name="txtSubDesAmt_1" id="txtSubDesAmt_1" class="form-control numeric"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td>
											</tr>
									<?php }?>
                                       </tbody>
                                       <tbody>
                                           <tr>
                                               <td align="left" colspan="4"><button type="button" class="btn btn-primary btn-flat" onClick="SubNewSubRow();">Add</button><input type="hidden" name="txtSubID" id="txtSubID" value="<?php echo $num21;?>"/></td>
                                           </tr>
                                           <tr>
                                               <th align="left" colspan="2"><input name="ChkShowOffHire" class="checkbox" id="ChkShowOffHire" type="checkbox" value="1" <?php if($rows['CHK_OFFHIRE']==1){echo 'checked';}?>/>&nbsp;&nbsp;Off-hire(<?php echo $currency;?>)</th>
                                               <?php if($rows['CHK_OFFHIRE']==1){$offhiredisplay = '';}else{$offhiredisplay = 'none';}?>
                                               <td><span id="offHireTR2" style="display:<?php echo $offhiredisplay;?>"><input type="text" name="txtOffHireDays" id="txtOffHireDays" class="form-control numeric" placeholder="Off Hire Days" autocomplete="off" value="<?php echo $rows['OFFHIRE_DAYS'];?>" onKeyUp="getFullClaculation();"/></span></td>
                                               <td><span id="offHireTR" style="display:<?php echo $offhiredisplay;?>"><input type="text" name="txtOffHire" id="txtOffHire" class="form-control"  placeholder="0.00" readonly autocomplete="off" value="<?php echo $rows['OFFHIRE_AMT'];?>"/></span></td>
                                            </tr>  
                                            <tr id="offHireTR1" style="display:<?php echo $offhiredisplay;?>">
                                                <td colspan="2" style="font-size:13px;"><strong>&nbsp;&nbsp;&nbsp;<i>CVE on Off-Hire(<?php echo $currency;?>)</i></strong></td>
                                                <td><input type="text" name="txtCVEOffHireMonth" id="txtCVEOffHireMonth" class="form-control numeric" placeholder="CVE on Off-Hire/Month" autocomplete="off" value="<?php echo $rows['OFFHIRE_CVE'];?>" onKeyUp="getFullClaculation();"/></td>
                                                <td><input type="text" name="txtCVEOFFHIRE_Amt" id="txtCVEOFFHIRE_Amt" class="form-control numeric" readonly  placeholder="CVE on Off-Hire" autocomplete="off" value="<?php echo $rows['OFFHIRE_CVE_AMT'];?>"/></td>
                                            
                                            </tr>
                                            <tr>
                                               <th align="left" colspan="4"><input name="ChkDeliveryBunker" class="checkbox" id="ChkDeliveryBunker" type="checkbox" value="1" <?php if($rows['CHK_DELIVERY']==1){echo 'checked';}?> />&nbsp;&nbsp;Delivery Bunkers</th>
                                            </tr>
                                            
                                          <?php if($rows['CHK_DELIVERY']==1){$deliverydisplay = '';}else{$deliverydisplay = 'none';}?>
                                            <tr id="DivBunkerDelivery_0" style="display:<?php echo $deliverydisplay;?>;">
                                            <td>Bunker Grade</td>
                                            <td>Qty(MT)</td>
                                            <td>Price(<?php echo $currency;?>)</td>
                                            <td>Amount(<?php echo $currency;?>)</td>
                                            </tr>
                                            <tbody id="bunkerDeliverybody">
                                      <?php $sql_inv2 = "select * from invoice_hire_slave2 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='DEL'"; 
											$res_inv2 = mysql_query($sql_inv2);
											$num_inv2 = mysql_num_rows($res_inv2);	
											if($num_inv2 == 0)
											{$del = 1;?>
                                            <tr id="DivBunkerDelivery_1" style="display:<?php echo $deliverydisplay;?>;">
                                            <td><select name="selDELBunker_1" id="selDELBunker_1" class="select form-control" onChange="getFullClaculation();"><?php $obj->getBunkerList();?></select></td>
                                            <td><input type="text" name="txtDelBunkerQty_1" id="txtDelBunkerQty_1" class="form-control numeric" onKeyUp="getFullClaculation();" placeholder="0.00" autocomplete="off" value=""/></td>
                                            <td><input type="text" name="txtDelBunkerPrice_1" id="txtDelBunkerPrice_1" class="form-control numeric" onKeyUp="getFullClaculation();" placeholder="0.00" autocomplete="off" value=""/></td>
                                            <td><input type="text" name="txtDelBunkerAmt_1" id="txtDelBunkerAmt_1" class="form-control"  placeholder="0.00" readonly autocomplete="off" value=""/></td>
										    </tr>
											<?php }else{$del = 0;
											$res_inv2 = mysql_query($sql_inv2);
											while($rows_inv2 = mysql_fetch_assoc($res_inv2))
											{$del++;
											?>
                                            <tr id="DivBunkerDelivery_<?php echo $del;?>" style="display:<?php echo $deliverydisplay;?>;">
                                            <td><select name="selDELBunker_<?php echo $del;?>" id="selDELBunker_<?php echo $del;?>" class="select form-control" onChange="getFullClaculation();"><?php $obj->getBunkerList();?></select></td>
                                            <td><input type="text" name="txtDelBunkerQty_<?php echo $del;?>" id="txtDelBunkerQty_<?php echo $del;?>" onKeyUp="getFullClaculation();" class="form-control numeric"  placeholder="0.00" autocomplete="off" value="<?php echo $rows_inv2['BUNKER_QTY'];?>"/></td>
                                            <td><input type="text" name="txtDelBunkerPrice_<?php echo $del;?>" id="txtDelBunkerPrice_<?php echo $del;?>" onKeyUp="getFullClaculation();" class="form-control numeric"  placeholder="0.00" autocomplete="off" value="<?php echo $rows_inv2['BUNKER_PRICE'];?>"/></td>
                                            <td><input type="text" name="txtDelBunkerAmt_<?php echo $del;?>" id="txtDelBunkerAmt_<?php echo $del;?>" class="form-control"  placeholder="0.00" readonly autocomplete="off" value="<?php echo $rows_inv2['BUNKER_AMT'];?>"/></td>
										    </tr>
                                            <script>$("#selDELBunker_<?php echo $del;?>").val(<?php echo $rows_inv2['BUNKERID'];?>);</script>  
                                            <?php }}?> 
                                            </tbody>   
											<tr id="DivBunkerDelivery_add" style="display:<?php echo $deliverydisplay;?>;">
											   <td align="left" colspan="4"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewDELRow();">Add</button><input type="hidden" name="txtAddDELID" id="txtAddDELID" value="<?php echo $del;?>"/></td>
											</tr>
                                            <tr>
                                               <th align="left" colspan="4"><input name="ChkReDeliveryBunker" class="checkbox" id="ChkReDeliveryBunker" type="checkbox" value="1" <?php if($rows['CHK_REDELIVERY']==1){echo 'checked';}?> />&nbsp;&nbsp;Re-Delivery Bunkers</th>
                                            </tr>
                                            <?php if($rows['CHK_REDELIVERY']==1){$redeliverydisplay = '';}else{$redeliverydisplay = 'none';}?>
                                            <tr id="DivBunkerREDelivery_0" style="display:<?php echo $redeliverydisplay;?>;">
                                            <td>Bunker Grade</td>
                                            <td>Qty(MT)</td>
                                            <td>Price(<?php echo $currency;?>)</td>
                                            <td>Amount(<?php echo $currency;?>)</td>
                                            </tr>
                                            <tbody id="bunkerReDeliverybody">
                                      <?php $sql_inv2 = "select * from invoice_hire_slave2 where INVOICEID='".$rows['INVOICEID']."' and IDENTIFY='REDEL'"; 
											$res_inv2 = mysql_query($sql_inv2);
											$num_inv2 = mysql_num_rows($res_inv2);	
											if($num_inv2 == 0)
											{$redel = 1;?>
                                            <tr id="DivBunkerReDelivery_1" style="display:<?php echo $redeliverydisplay;?>;">
                                            <td><select name="selReDELBunker_1" id="selReDELBunker_1" class="select form-control" onChange="getFullClaculation();"><?php $obj->getBunkerList();?></select></td>
                                            <td><input type="text" name="txtReDelBunkerQty_1" id="txtReDelBunkerQty_1" class="form-control numeric" onKeyUp="getFullClaculation();" placeholder="0.00" autocomplete="off" value=""/></td>
                                            <td><input type="text" name="txtReDelBunkerPrice_1" id="txtReDelBunkerPrice_1" class="form-control numeric" onKeyUp="getFullClaculation();" placeholder="0.00" autocomplete="off" value=""/></td>
                                            <td><input type="text" name="txtReDelBunkerAmt_1" id="txtReDelBunkerAmt_1" class="form-control"  placeholder="0.00" readonly autocomplete="off" value=""/></td>
										    </tr>
											<?php }else{$redel = 0;
											$res_inv2 = mysql_query($sql_inv2);
											while($rows_inv2 = mysql_fetch_assoc($res_inv2))
											{$redel++;
											?>
                                            <tr id="DivBunkerReDelivery_<?php echo $redel;?>" style="display:<?php echo $redeliverydisplay;?>;">
                                            <td><select name="selReDELBunker_<?php echo $redel;?>" id="selReDELBunker_<?php echo $redel;?>" class="select form-control" onChange="getFullClaculation();"><?php $obj->getBunkerList();?></select></td>
                                            <td><input type="text" name="txtReDelBunkerQty_<?php echo $redel;?>" id="txtReDelBunkerQty_<?php echo $redel;?>" onKeyUp="getFullClaculation();" class="form-control numeric"  placeholder="0.00" autocomplete="off" value="<?php echo $rows_inv2['BUNKER_QTY'];?>"/></td>
                                            <td><input type="text" name="txtReDelBunkerPrice_<?php echo $redel;?>" id="txtReDelBunkerPrice_<?php echo $redel;?>" onKeyUp="getFullClaculation();" class="form-control numeric"  placeholder="0.00" autocomplete="off" value="<?php echo $rows_inv2['BUNKER_PRICE'];?>"/></td>
                                            <td><input type="text" name="txtReDelBunkerAmt_<?php echo $redel;?>" id="txtReDelBunkerAmt_<?php echo $redel;?>" class="form-control"  placeholder="0.00" readonly autocomplete="off" value="<?php echo $rows_inv2['BUNKER_AMT'];?>"/></td>
										    </tr>
                                            <script>$("#selReDELBunker_<?php echo $redel;?>").val(<?php echo $rows_inv2['BUNKERID'];?>);</script>  
                                            <?php }}?> 
                                            </tbody>   
											<tr id="DivBunkerReDelivery_add" style="display:<?php echo $redeliverydisplay;?>;">
											   <td align="left" colspan="4"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewREDELRow();">Add</button><input type="hidden" name="txtAddREDELID" id="txtAddREDELID" value="<?php echo $redel;?>"/></td>
											</tr>
                                            <?php 
											$sqlprev = "SELECT * from invoice_hire_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
											$resprev = mysql_query($sqlprev);
											$rec = mysql_num_rows($resprev);$m=0;
											while($rowsprev = mysql_fetch_assoc($resprev))
											{$m++;
											?>
                                            <tr>
                                                <td colspan="3"><strong><?php echo $m;?>. <?php echo $rowsprev['INVOICE_NO'];?></strong></td>
                                                <td><input type="text" name="txtPrevInvAmt_<?php echo $m;?>" id="txtPrevInvAmt_<?php echo $m;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rowsprev['P_AMT'];?>"/></td>
                                            </tr>
                                        <?php }?>
                                           <tr>
                                                <td colspan="3"><strong>Balance to Owners (<?php echo $currency;?>)</strong></td>
                                                
                                                <td><input type="text" name="txtFinalAmt" id="txtFinalAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows['FINAL_AMT'];?>"/></td>
                                            </tr>
                                            <tr style="display:none;">
                                            <th style="padding-top:15px;padding-bottom:15px;" colspan="3">Balance to Shipowner(<?php echo $currency;?>) :</th>
                                            <td><input type="text" name="txtBalanceShipOwner" id="txtBalanceShipOwner" class="form-control" readonly style="background-color:#eee;"  placeholder="Balance to Shipowner" autocomplete="off" value="<?php echo $rows['BALANCE_TO_OWNER'];?>"/></td>
                                        </tr>
                                        
                                       </tbody>
                                   </table>
                               </div>
						   </div><!-- /.col -->
						</div>
                      
                       <div class="box-footer" align="right">
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<button class="btn btn-primary btn-flat" type="submit" onClick="return getValidate(0);">Submit to Edit</button>
							<button class="btn btn-primary btn-flat" type="submit" onClick="return getValidate(1);">Submit to Close</button>
							<input type="hidden" name="action" id="action" value="submit" />
                           
						    
				        </div>
                         </form> 
					
						
						<form role="form" name="frm3" id="frm3" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                        <div class="box-body no-padding" style="overflow:auto;">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="3%"># <input type="hidden" name="action11" id="action11" value="" /><input type="hidden" name="txtDEL_ID" id="txtDEL_ID" value="0"/></th>
                                        <th width="10%">Hire Statement Date</th>
                                        
                                        <th width="10%">Hire Statement No.</th>
                                        <th width="10%">Hire Days</th>
                                        <th width="10%">Amount</th>
                                        <th width="10%">Generate PDF</th>
                                        <th width="10%">Payment Actioned</th>
                                        <th width="3%">Delete</th>
                                    </tr>
                                </thead>
                                <tbody id="tbl_txtID">
                                <?php 
                                $sql = "SELECT * from invoice_hire_master where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
                                $res = mysql_query($sql);
                                $rec = mysql_num_rows($res);
                                if($rec == 0){
                                ?>
                                <tr id="tpRow"><td valign="top" align="center" colspan="9" style="color:red;">Sorry , currently zero(0) records added.</td></tr>
                                <?php }else{
                                $m=1;
                                while($rows = mysql_fetch_assoc($res))
                                {
                                ?>
                                <tr id="tpRow_<?php echo $m;?>">
                                <td align="center"><?php echo $m.".";?></td>
                                <td align="left"><?php echo date("d-M-Y",strtotime($rows['INVOICE_DATE']));?></td>
                                
                                <td align="left"><?php echo $rows["INVOICE_NO"];?></td>
                                <td align="left"><?php echo $rows['HIRE_DAYS'];?></td>
                                <td align="left"><?php echo $rows['FINAL_AMT'];?><input type="hidden" name="txthddnAmt_<?php echo $m;?>" id="txthddnAmt_<?php echo $m;?>" value="<?php echo $rows['FINAL_AMT'];?>"/><input type="hidden" name="txthddnUsdAmt_<?php echo $m;?>" id="txthddnUsdAmt_<?php echo $m;?>" value="<?php echo number_format(($rows['FINAL_AMT']*$rows['EXCHANGE_RATE']),2,'.','');?>"/><input type="hidden" name="txthddnID_<?php echo $m;?>" id="txthddnID_<?php echo $m;?>" value="<?php echo $rows['INVOICEID'];?>"/><input type="hidden" id="tblid1" name="tblid1" value=""/></td>
                                <td align="left"><p>
                                <a href="allPdf.php?id=46&comid=<?php echo $comid; ?>&invoiceid=<?php echo $rows['INVOICEID']; ?>" title="Pdf"><button type="button" class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a></p></td>
                                <td align="left" valign="middle">
								<a href="#Payment" title="Payment Received" class="btn btn-default btn-sm" data-toggle="modal" data-target="#compose-modal" onClick="openWin(<?php echo $rows['INVOICEID'];?>);">Payment Actioned</a>
                                </td>
                                <td align="center">
                                <a href="#?" title="Delete Entry" onClick="getDelete(<?php echo $rows['INVOICEID'];?>,<?php echo $m;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                </tr>
                                <?php $m++;}}?>
                                </tbody>
                                
                            </table>
                         </div>
												
					</form>
                    <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								</div>
								<div class="modal-body">
									<form name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>
                                        <div id="divPayment">
                                            
                                        </div>
                                        <div class="box-footer" align="right">
                                            <button type="submit" id="btnhide" class="btn btn-primary btn-flat" onClick="return getValid();" >Submit</button>
                                            <input type="hidden" id="action2" name="action2" value="submit1" />
                                            <input type="hidden" name="txtCRMFILE1" id="txtCRMFILE1" value="" />
                                            <input type="hidden" name="txtCRMNAME1" id="txtCRMNAME1" value="" />
                                            <input type="hidden" id="tblid" name="tblid" value=""/>
                                        </div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>