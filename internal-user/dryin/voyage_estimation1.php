<?php
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$mappingid 		= $_REQUEST['mappingid'];
$cost_sheet_id 	= $_REQUEST['cost_sheet_id'];

if($obj->getFreightEstimationStatus1($mappingid) == 1 )
{
	header('Location : ./voyage_estimation.php?mappingid='.$mappingid.'&cost_sheet_id='.$cost_sheet_id);
}
if($obj->getFreightEstimationRecProcessWise($mappingid,$cost_sheet_id) > 0 )
{  
	if($obj->getFreightEstimationStatus($mappingid,$cost_sheet_id) == 1 )
	{
		header('Location : ./update_voyage_estimation.php?mappingid='.$mappingid.'&cost_sheet_id='.$cost_sheet_id);
	}
	else
	{
		header('Location : ./update_voyage_estimation1.php?mappingid='.$mappingid.'&cost_sheet_id='.$cost_sheet_id);
	}
}

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertVCIDetails();
	header('Location : ./nomination_at_glance.php?msg='.$msg);
}
if($obj->getLastCostSheetID($mappingid) > 0)
{
	$obj->viewFreightEstimationRecords($mappingid,$obj->getLastCostSheetID($mappingid));
	$rdoMMarket = $obj->getFun12();
	$rdoCap = $obj->getFun6();
	$rdoDWT = $obj->getFun21();
	$rdoQty = $obj->getFun29();
	$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&selFType=".$obj->getFun2().'&cost_sheet_id='.$cost_sheet_id;
}
else
{
	$rdoMarket = $rdoMMarket = $rdoCap = $rdoDWT = $rdoQty = 1;
	$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid.'&cost_sheet_id='.$cost_sheet_id;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
h2 {
    color: #1b77a6;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 22px;
    font-weight: normal;
    line-height: 1;
    margin-bottom: 5px;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Nominations at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="nomination_at_glance.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
						
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             Cost Sheet : Estimate
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
                    <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                        <div class="col-sm-4 invoice-col" style="display:none;" >
                            <address>
                               <select  name="selVendor" class="select form-control" id="selVendor" >
                                    <?php $obj->getVendorListNewUpdate("");	?>
                                </select> 
                            </address>
                        </div><!-- /.col -->
						<?php if($obj->getLastCostSheetID($mappingid) > 0){?> 
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	Fixture Type
                                    <address>
                                        <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFreightFixtureBasedOnID($obj->getFun2());?></strong>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Main Particulars
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                            	<div class="col-sm-4 invoice-col">
                                	Nom ID
                                    <address>
                                    <input type="text" name="txtNomID" id="txtNomID" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getMappingData($mappingid,"NOMINATION_ID");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	Vessel Name
                                    <address>
                                    <input type="text" name="txtVName" id="txtVName" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	Vessel Type
                                    <address>
                                    <input type="text" name="txtVType" id="txtVType" class="form-control" autocomplete="off" placeholder="Vessel Type" readonly value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_TYPE"));?>" />              </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	Date<span style="font-size:10px; font-style:italic;"> (for financial year)</span>
                                    <address>
                                    <input type="text" name="txtDate" id="txtDate" class="form-control" autocomplete="off" placeholder="Date (for financial year)" value="<?php echo date("d-m-Y",strtotime($obj->getFun3()));?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	Voyage No.
                                    <address>
                                    <input type="text" name="txtVNo" id="txtVNo" class="form-control" autocomplete="off" placeholder="Voyage No." value="<?php echo $obj->getFun10();?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	Cost Sheet Name
                                    <address>
                                    <input type="text" name="txtENo" id="txtENo" class="form-control" autocomplete="off" style="color:red;" value="<?php echo $obj->getCostSheetNameBasedOnID($cost_sheet_id);?>" placeholder="Cost Sheet Name" />
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    <input name="rdoDWT" class="checkbox" id="rdoDWT1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoDWT == 1) echo "checked"; ?> onClick="showDWTField();"  /><br>
                                    DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>
                                    <address>
                                    <input type="text" name="txtDWTS" id="txtDWTS"  class="form-control" autocomplete="off" placeholder="DWT" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUMMER_1");?>" disabled="disabled"/>
                                    
                                    <input type="hidden" name="txtDWTSVal" id="txtDWTSVal" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUMMER_1");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <input name="rdoDWT" class="checkbox" id="rdoDWT2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoDWT == 2) echo "checked"; ?> onClick="showDWTField();" /><br>
                                    DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>
                                    <address>
                                    <input type="text" name="txtDWTT" id="txtDWTT" class="form-control" autocomplete="off" placeholder="DWT" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"TROPICAL_1");?>" disabled="disabled">
                                    
                                    <input type="hidden" name="txtDWTTVal" id="txtDWTTVal" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"TROPICAL_1");?>" disabled="disabled">
                                    
                                    <input type="hidden" name="txtTCNo" id="txtTCNo" autocomplete="off" value="<?php echo $obj->getFun11();?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    &nbsp;
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                               		<input name="rdoCap" class="checkbox" id="rdoCap1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoCap == 1) echo "checked"; ?> onClick="showCapField();"  /><br>
                                	Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
                                    <address>
                                        <input type="text" name="txtGCap" id="txtGCap" class="form-control" autocomplete="off" placeholder="Grain Cap" value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"GRAIN");?>" disabled="disabled" onkeyup="getTotalDWT(),getTotalDWT1()"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	<input name="rdoCap" class="checkbox" id="rdoCap2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoCap == 2) echo "checked"; ?> onClick="showCapField();" /><br>
                                	Bale Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
                                    <address>
                                    	<input type="text" name="txtBCap" id="txtBCap" class="form-control" autocomplete="off" placeholder="Bale Cap" value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BALE");?>" disabled="disabled" onkeyup="getTotalDWT(),getTotalDWT1()"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	&nbsp;&nbsp;<br>
                                	SF <span style="font-size:10px; font-style:italic;">(CBM/MT)</span>
                                    <address>
                                    	<input type="text" name="txtSF" id="txtSF" class="form-control" placeholder="SF" autocomplete="off" value="<?php echo $obj->getFun9();?>"  onkeyup="getTotalDWT(),getTotalDWT1()"/>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	&nbsp;
                                </div><!-- /.col -->
                            	<div class="col-sm-4 invoice-col">
                                &nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                               		Loadable <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                    <input type="text" name="txtLoadable" id="txtLoadable" class="form-control" placeholder="Loadable" readonly autocomplete="off" value="<?php echo $obj->getFun24();?>"/>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	Market
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	<input name="rdoMMarket" id="rdoMMarket1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoMMarket == 1) echo "checked"; ?>  onclick="showMMarketField();" /><br>
                                	Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
                                    <input type="text" name="txtMTCPDRate" id="txtMTCPDRate" class="form-control" placeholder="Agreed Gross Freight" autocomplete="off" value="<?php echo $obj->getFun13();?>"  onkeyup="getFinalCalculation();" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	<input name="rdoMMarket" class="checkbox" id="rdoMMarket2" type="radio" style="cursor:pointer;" value="2" <?php if($rdoMMarket == 2) echo "checked"; ?> onClick="showMMarketField();" /><br>
                                	Lumpsum <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
                                    <input type="text" name="txtMLumpsum" id="txtMLumpsum" class="form-control" placeholder="Lumpsum" autocomplete="off" disabled value="<?php echo $obj->getFun14();?>"  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                            	<div class="col-sm-4 invoice-col">
                            		&nbsp;&nbsp;<br>
                            		Addnl Cargo Rate <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
                                    	<input type="text"  name="txtAddnlCRate" id="txtAddnlCRate" class="form-control" placeholder="Addnl Cargo Rate" autocomplete="off" value="<?php echo $obj->getFun25();?>"  onkeyup="getFinalCalculation();"/>
                                    </address>
                            </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	Cargo
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                            	<div class="col-sm-4 invoice-col">
                                	Quantity <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                    	<input type="text" name="txtCQMT" id="txtCQMT" class="form-control" placeholder="Quantity" autocomplete="off" value="<?php echo $obj->getFun15();?>"  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                            	<div class="col-sm-4 invoice-col">
                            		Cargo Type
                                <address>
                                <select  name="selCType" class="form-control" id="selCType">
									<?php
                                    $_REQUEST['selCType'] = $obj->getFun16(); 
                                    $obj->getCargoTypeList();
                                    ?>
                                </select>
                                </address>
                            	</div><!-- /.col -->
                            	<div class="col-sm-4 invoice-col">
                            		Cargo Name
                                    <address>
                                    <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCargoContarctForMapping($obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"CARGO_POSITION"));?></strong>
                                    </address>
                            	</div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	<span style="font-size:13px; font-style:italic; color:#dc631e">( Please put dead freight quantity / addnl quantity separately )</span>  
                                    <address>
                                    	<input type="hidden" name="txtAQMT" id="txtAQMT" value="<?php echo $obj->getFun26();?>"  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	<input name="rdoQty" class="checkbox" id="rdoQty1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoQty == 1) echo "checked"; ?>  onclick="showQtyField();"  /><br>
                                	DF Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                    <input type="text" name="txtDFQMT" id="txtDFQMT" class="form-control" placeholder="DF Qty" autocomplete="off" value="<?php echo $obj->getFun27();?>"  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	<input name="rdoQty" class="checkbox" id="rdoQty2" type="radio" style="cursor:pointer;" value="2" <?php if($rdoQty == 2) echo "checked"; ?> onClick="showQtyField();" /><br>
                                Addnl Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                    <input type="text" name="txtAddnlQMT" id="txtAddnlQMT" class="form-control" placeholder="Addnl Qty " autocomplete="off" readonly value="<?php echo $obj->getFun28();?>"  onkeyup="getFinalCalculation();"/>
                                        <select  name="selPort" id="selPort" style="display:none;" >
                                        <?php $obj->getPortList(); ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Load Port
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Load Port
                                    <address>
                                    <select  name="selLoadPort" class="form-control" id="selLoadPort">
                                    	<?php $obj->getPortList(); ?>
                                    </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Cargo
                                    <address>
                                        <select  name="selLPCName" class="form-control" id="selLPCName" onChange="getLOadPortQty();" >
                                        <?php 
                                        $obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Qty MT
                                    <address>
                                    <input type="text" name="txtLPQMT" id="txtLPQMT" class="form-control" placeholder="Qty MT" onKeyUp="getLoadPortCalculation();" autocomplete="off" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                                    <address>
                                    <input type="text" name="txtLPRMTDay" id="txtLPRMTDay" class="form-control" placeholder="Rate "  onkeyup="getLoadPortCalculation();" autocomplete="off" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
                                    <input type="text" name="txtLPPortCosts" id="txtLPPortCosts" class="form-control" placeholder="Port Costs" autocomplete="off" value="" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Work Days <span style="font-size:10px; font-style:italic;">(Qty/Rate)</span>
                                    <address>
                                    <input type="text" name="txtLPWorkDay" id="txtLPWorkDay" class="form-control" placeholder="Work Days" autocomplete="off" value="" readonly />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                &nbsp;
                                    <address>                    
                                    	<button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addLoadPortDetails()">ADD</button>
                                    </address>
                                </div><!-- /.col -->
                        	</div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-primary">
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                            <?php 
                                            $sql = "select * from fca_vci_load_port where FCAID='".$obj->getFun1()."'";
                                            $res = mysql_query($sql);
                                            $rec = mysql_num_rows($res); 
                                            ?>
                                                <thead>
                                                    <tr>
                                                        <th width="3%" align="center">#</th>
                                                        <th width="7%" align="left">Load Port</th>
                                                        <th width="10%" align="left">Cargo Name</th>
                                                        <th width="7%" align="left">Qty MT</th>
                                                        <th width="7%" align="left">Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                                                        <th width="7%" align="left">Work Days</th>
                                                        <th width="8%" align="left">Port Costs</th>
                                                        <input type="hidden" name="load_portID" id="load_portID" value="<?php echo $rec;?>" />
                                                    </tr>
                                                </thead>
                                                <tbody id="tblLoadPort">
                                                <?php if($rec == 0){?>
                                                <tr id="LProw_Empty"><td valign="top" align="center" colspan="7" style="color:red;">Sorry , currently zero(0) records added.</td></tr>
                                                <?php }else{
                                                $i=1;
                                                while($rows = mysql_fetch_assoc($res))
                                                { 
                                                ?>
                                                    <tr id="lp_Row_<?php echo $i;?>">
                                                        <td align="center" ><a href="#lp<?php echo $i;?>" onClick="removeLoadPort('<?php echo $i;?>');" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                        <td align="left" ><?php echo $obj->getPortNameBasedOnID($rows['LOADPORTID']);?><input type="hidden" name="txtLoadPort_<?php echo $i;?>" id="txtLoadPort_<?php echo $i;?>" value="<?php echo $rows['LOADPORTID'];?>"/></td>
                                                        <td align="left" ><?php echo $obj->getCargoContarctForMapping($rows['PURCHASE_ALLOCATIONID'],$obj->getMappingData($mappingid,"CARGO_POSITION"));?><input type="hidden" name="txtLPCID_<?php echo $i;?>" id="txtLPCID_<?php echo $i;?>" value="<?php echo $rows['PURCHASE_ALLOCATIONID'];?>"/></td>
                                                        <td align="left" ><?php echo $rows['QTY_MT'];?><input type="hidden" name="txtLpQMT_<?php echo $i;?>" id="txtLpQMT_<?php echo $i;?>" value="<?php echo $rows['QTY_MT'];?>"/></td>
                                                        <td align="left" ><?php echo $rows['RATE'];?><input type="hidden" name="txtLpRate_<?php echo $i;?>" id="txtLpRate_<?php echo $i;?>" value="<?php echo $rows['RATE'];?>"/></td>
                                                        <td align="left" ><?php echo $rows['WORK_DAYS'];?><input type="hidden" name="txtLpBLWorkDays_<?php echo $i;?>" id="txtLpBLWorkDays_<?php echo $i;?>" value="<?php echo $rows['WORK_DAYS'];?>"/></td>
                                                        <td align="left" ><?php echo $rows['PORTCOSTS'];?><input type="hidden" name="txtLPPCosts_<?php echo $i;?>" id="txtLPPCosts_<?php echo $i;?>" value="<?php echo $rows['PORTCOSTS'];?>"/></td>
                                                    </tr>
                                                <?php $i++;} }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Discharge Port(s)
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Discharge Port
                                    <address>
                                        <select  name="selDisPort" class="form-control" id="selDisPort">
                                        	<?php $obj->getPortList(); ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	Cargo
                                    <address>
                                        <select  name="selDPCName" class="form-control" id="selDPCName" onChange="getDisPortQty();" >
                                        <?php 
                                        $obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                Qty MT
                                    <address>
                                    <input type="text" name="txtDPQMT" id="txtDPQMT" class="form-control" placeholder="Qty MT" onKeyUp="getDisPortCalculation();" autocomplete="off"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                                <address>
                                <input type="text" name="txtDPRMTDay" id="txtDPRMTDay" class="form-control" placeholder="Rate" onkeyup="getDisPortCalculation();" autocomplete="off"/>
                                </address>
                                </div><!-- /.col -->

                            	<div class="col-sm-4 invoice-col">
                                    Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
                                    <input type="text" name="txtDPPortCosts" id="txtDPPortCosts" class="form-control" placeholder="Port Costs" autocomplete="off" />
                                    </address>
                            	</div><!-- /.col -->
                            	<div class="col-sm-4 invoice-col">
                                    Work Days <span style="font-size:10px; font-style:italic;">(Qty/Rate)</span>
                                    <address>
                                    <input type="text" name="txtDPWorkDay" id="txtDPWorkDay" class="form-control" placeholder="Work Days" autocomplete="off" readonly />
                                    </address>
                            	</div><!-- /.col -->
                            	<div class="col-sm-4 invoice-col">
                                	&nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	&nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                &nbsp;
                                <address>
                                	<button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addDisPortDetails()">ADD</button>
                                </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-primary">
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                            <?php 
                                            $sql1 = "select * from fca_vci_disch_port where FCAID='".$obj->getFun1()."'";
                                            $res1 = mysql_query($sql1);
                                            $rec1 = mysql_num_rows($res1); 
                                            ?>
                                                <thead>
                                                    <tr>
                                                        <th width="3%" align="center">#</th>
                                                        <th width="7%" align="left">Discharge Port</th>
                                                        <th width="10%" align="left">Cargo Name</th>
                                                        <th width="7%" align="left">Qty MT</th>
                                                        <th width="7%" align="left">Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                                                        <th width="7%" align="left">Work Days</th>
                                                        <th width="7%" align="left">Port Costs</th>
                                                        <input type="hidden" name="dis_portID" id="dis_portID" value="<?php echo $rec1;?>" />
                                                    </tr>
                                                </thead>
                                                <tbody id="tblDisPort">
                                                <?php if($rec1 == 0){?>
                                                <tr id="DProw_Empty"><td valign="top" align="center" colspan="7" style="color:red;">Sorry , currently zero(0) records added.</td></tr>
                                                <?php }else{
                                                $i=1;
                                                while($rows1 = mysql_fetch_assoc($res1))
                                                {
                                                ?>
                                                <tr id="dp_Row_<?php echo $i;?>">
                                                    <td align="center"><a href="#dp<?php echo $i;?>" onClick="removeDisPort('<?php echo $i;?>');" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                                    <td align="left"><?php echo $obj->getPortNameBasedOnID($rows1['DIS_PORT_ID']);?><input type="hidden" name="txtDisPort_<?php echo $i;?>" id="txtDisPort_<?php echo $i;?>" value="<?php echo $rows1['DIS_PORT_ID'];?>"/></td>
                                                    <td align="left"><?php echo $obj->getCargoContarctForMapping($rows1['PURCHASE_ALLOCID'],$obj->getMappingData($mappingid,"CARGO_POSITION"));?><input type="hidden" name="txtDPCID_<?php echo $i;?>" id="txtDPCID_<?php echo $i;?>" value="<?php echo $rows1['PURCHASE_ALLOCID'];?>"/></td>
                                                    <td align="left"><?php echo $rows1['QTY_MT'];?><input type="hidden" name="txtDpQMT_<?php echo $i;?>" id="txtDpQMT_<?php echo $i;?>" value="<?php echo $rows1['QTY_MT'];?>"/></td>
                                                    <td align="left"><?php echo $rows1['RATE'];?><input type="hidden" name="txtDpRate_<?php echo $i;?>" id="txtDpRate_<?php echo $i;?>" value="<?php echo $rows1['RATE'];?>"/></td>
                                                    <td align="left"><?php echo $rows1['WORK_DAYS'];?><input type="hidden" name="txtDpBLWorkDays_<?php echo $i;?>" id="txtDpBLWorkDays_<?php echo $i;?>" value="<?php echo $rows1['WORK_DAYS'];?>"/></td>
                                                    <td align="left"><?php echo $rows1['PORTCOSTS'];?><input type="hidden" name="txtDPPCosts_<?php echo $i;?>" id="txtDPPCosts_<?php echo $i;?>" value="<?php echo $rows1['PORTCOSTS'];?>"/></td>
                                                </tr>
                                                <?php $i++;} }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                   		Freight Adjustment
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="25%"></th>
                                            <th width="25%">Percent</th>
                                            <th width="25%">USD</th>
                                            <th width="25%">Per MT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Gross Freight</td>
                                            <td ></td>
                                            <td><input type="text"  name="txtFrAdjUsdGF" id="txtFrAdjUsdGF" class="form-control" autocomplete="off" size="10" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"GROSS_FREIGHT_USD");?>" />
                                            </td>
                                            <td><input type="text"  name="txtFrAdjUsdGFMT" id="txtFrAdjUsdGFMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"GROSS_FREIGHT_PERMT");?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">Dead Freight</td>
                                            <td ></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdDF" id="txtFrAdjUsdDF" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DEAD_FREIGHT_USD");?>" />
                                            </td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdDFMT" id="txtFrAdjUsdDFMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DEAD_FREIGHT_PERMT");?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" >Addnl Freight</td>
                                            <td ></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdAF" id="txtFrAdjUsdAF" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"ADDNL_FREIGHT_USD");?>" />
                                            </td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdAFMT" id="txtFrAdjUsdAFMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"ADDNL_FREIGHT_PERMT");?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">Total Freight</td>
                                            <td></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdTF" id="txtFrAdjUsdTF" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_FREIGHT_USD");?>" />
                                            </td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdTFMT" id="txtFrAdjUsdTFMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_FREIGHT_PERMT");?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" >Address Commission</td>
                                            <td align="left" valign="top"><input type="text"  name="txtFrAdjPerAC" id="txtFrAdjPerAC" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"AC_PERCENT");?>" onKeyUp="getFinalCalculation();"/></td>
                                            <td align="center" valign="top"><input type="text"  name="txtFrAdjUsdAC" id="txtFrAdjUsdAC" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"AC_USD");?>"  /></td>
                                            <td align="center" valign="top"><input type="text"  name="txtFrAdjUsdACMT" id="txtFrAdjUsdACMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"AC_PERMT");?>"  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">Brokerage</td>
                                            <td align="left"><input type="text"  name="txtFrAdjPerAgC" id="txtFrAdjPerAgC" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"AGC_PERCENT");?>" onKeyUp="getFinalCalculation(),getValue()" /></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdAgC" id="txtFrAdjUsdAgC" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"AGC_USD");?>"   />
                                            <td></td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">Net Freight Payable</td>
                                            <td align="left"></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdFP" id="txtFrAdjUsdFP" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"NETT_FRT_PAYABLE");?>"   />
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	Owner Related Costs (Others)
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                            	<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
											</tr>
										</thead>
										<tbody id="tbodyBrokerage">
										<?php 
										$sql_brok = "select * from fca_vci_brokage_commission where FCAID='".$obj->getFun1()."'";
										$res_brok = mysql_query($sql_brok);
										$num_brok = mysql_num_rows($res_brok);
										if($num_brok==0)
										{$num_brok =0;
										?>
                                             <tr id="tbrRow_1">
                                                <td><a href="#tb1'" onClick="removeBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td>
												<td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="form-control" readonly value="0.00" /></td>
												<td><select  name="selBroVList_1" class="select form-control" id="selBroVList_1"></select>
													<script>$("#selBroVList_1").html($("#selVendor").html());$("#selBroVList_1").val('');</script>
												</td>
											</tr>
									<?php }
									     else
									      {$i=0;$num_brok = $num_brok;
										  while($rows_brok = mysql_fetch_assoc($res_brok))
										  {$i = $i + 1;?>
										  
										      <tr id="tbrRow_<?php echo $i;?>">
                                                <td><a href="#tb1'" onClick="removeBrokerage(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_<?php echo $i;?>" id="txtBrCommPercent_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows_brok['BROKAGE_PERCENT'];?>" onKeyUp="getFinalCalculation();"  /></td>
												<td><input type="text" name="txtBrComm_<?php echo $i;?>" id="txtBrComm_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows_brok['BROKAGE_AMT'];?>" /></td>
												<td><select  name="selBroVList_<?php echo $i;?>" class="select form-control" id="selBroVList_<?php echo $i;?>"></select>
													<script>$("#selBroVList_<?php echo $i;?>").html($("#selVendor").html());$("#selBroVList_<?php echo $i;?>").val('<?php echo $rows_brok['VENDORID'];?>');</script>
												</td>
											</tr>
										  <?php }} ?>
											
                                            </tbody>
                                            <tbody>
											<tr>
                                                <td><button type="button" class="btn btn-primary btn-flat" onClick="addBrokerageRow()">Add</button>
                                                <input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="<?php echo $num_brok;?>"/></td>
												<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM_PERCENT");?>" readonly/></td>
												<td><input type="text" name="txtBrComm" id="txtBrComm" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM");?>" /></td>
												<td>
												</td>
											</tr>
											</tbody>
											<tbody>
											<?php 
											$sql4 = "select * from fca_vci_owner_related_cost where FCAID='".$obj->getFun1()."'";
											$res4 = mysql_query($sql4);
											$rec4 = mysql_num_rows($res4);
											$i=1;
											while($rows4 = mysql_fetch_assoc($res4))
											{
											?>
											<tr>
												<td><?php echo $obj->getOwnerRelatedCostNameBasedOnID($rows4['OWNER_RCOSTID']);?><input type="hidden" name="txtHidORCID_<?php echo $i;?>" id="txtHidORCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows4['OWNER_RCOSTID'];?>" /></td>
												<td></td>
												<td></td>
												<td>
												<?php if($rows4['AMOUNT'] > 0){?>
													<input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>" onKeyUp="getORCCalculate(1,<?php echo $i;?>);" class="form-control" autocomplete="off" value="<?php echo abs($rows4['AMOUNT']);?>"  />
												<?php }else if($rows4['AMOUNT'] < 0){?>
													<input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>"  onkeyup="getORCCalculate(2,<?php echo $i;?>);" class="form-control" autocomplete="off" value="<?php echo abs($rows4['AMOUNT']);?>" style="color:red;"  />
												<?php }else if($rows4['AMOUNT'] == 0){?>
												<?php if($obj->getOwnerRelatedCostData($rows4['OWNER_RCOSTID'],"RDO_STATUS") == 1){?>
													<input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>" onKeyUp="getORCCalculate(<?php echo $obj->getOwnerRelatedCostData($rows4['OWNER_RCOSTID'],"RDO_STATUS");?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="<?php echo abs($rows4['AMOUNT']);?>" />
												<?php }else if($obj->getOwnerRelatedCostData($rows4['OWNER_RCOSTID'],"RDO_STATUS") == 2){?>
													<input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>"  onkeyup="getORCCalculate(<?php echo $obj->getOwnerRelatedCostData($rows4['OWNER_RCOSTID'],"RDO_STATUS");?>,<?php echo $i;?>);" class="form-control" value="<?php echo abs($rows4['AMOUNT']);?>" style="color:red;"  />
												<?php }?>
												<?php }?>
												</td>
												<td>
													<select  name="selORCVList_<?php echo $i;?>" class="select form-control" id="selORCVList_<?php echo $i;?>" >
														<?php //$obj->getVendorListNewUpdate("");	?>
													</select>
													<script>$("#selORCVList_<?php echo $i;?>").html($("#selVendor").html());$("#selORCVList_<?php echo $i;?>").val('<?php echo $rows4['VENDORID'];?>')</script>
												</td><input type="hidden" name="txtHidORCAmt_<?php echo $i;?>" id="txtHidORCAmt_<?php echo $i;?>" class="form-control"  autocomplete="off" value="<?php echo $rows4['AMOUNT'];?>" />
										</tr>
										<?php $i++;}?>
											<tr>
												<td>Total Shipowner Expenses<input type="hidden" name="txtORC_id" id="txtORC_id" value="<?php echo $rec1;?>" /></td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtTTLORCAmt" id="txtTTLORCAmt" class="form-control" readonly placeholder="0.00"  value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SHIPOWNER_EXP");?>" /></td>
												<td></td>
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td style="font-weight:bold;">Value/MT</td>
												<td><input type="text" name="txtTTLORCCostMT" id="txtTTLORCCostMT" class="form-control"  readonly="true" placeholder="0.00" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SHIPOWNER_EXP_MT");?>" /></td>
											</tr>
										</tbody>
									</table>
								</div>
                            </div>
                            
                            <div class="row invoice-info" style="display:none;">
                                <div class="col-sm-4 invoice-col">
                                	Freight Payment
                                    <address>
                                    <input type="text"  name="txtTTLORCFP" id="txtTTLORCFP" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"FREIGHT_PAYMENT");?>"  />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	Cost/MT
                                    <address>
                                    	<input type="text" name="txtTTLORCFPCostMT" id="txtTTLORCFPCostMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"ORC_COSTMT");?>" />
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    All Other Shipping Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%"></th>
                                        <th width="20%"></th>
                                        <th width="20%">Cost</th>
                                        <th width="20%">Vendor</th>
                                        <th width="20%">Cost/MT</th>
                                        </tr>
                                    </thead>
                                <tbody>
                                <?php 
                                $sql2 = "select * from fca_vci_other_shipping_cost where FCAID='".$obj->getFun1()."'";
                                $res2 = mysql_query($sql2);
                                $rec2 = mysql_num_rows($res2);
                                $i=1;
                                while($rows2 = mysql_fetch_assoc($res2))
                                {
                                ?>
                                    <tr>
                                        <td align="left"><?php echo $obj->getOtherShippingCostNameBasedOnID($rows2['OTHER_SCOSTID']);?><input type="hidden" name="txtHidOSCID_<?php echo $i;?>" id="txtHidOSCID_<?php echo $i;?>" readonly value="<?php echo $rows2['OTHER_SCOSTID'];?>" /></td>
                                        <td align="left"></td>
                                        <td align="left"><input type="text"  name="txtOSCAbs_<?php echo $i;?>" id="txtOSCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows2['COST_MT'];?>" onKeyUp="getOSCostCalculate1(<?php echo $i;?>),getFinalCalculation()"/></td>
                                        <td><select  name="selAOSCVendorList_<?php echo $i; ?>" class="select form-control" id="selAOSCVendorList_<?php echo $i; ?>"></select></td>
                                		<script>$("#selAOSCVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selAOSCVendorList_<?php echo $i; ?>").val("<?php echo $rows2['VENDORID'];?>")</script>
                                        <td align="left"><input type="text" name="txtOSCCostMT_<?php echo $i;?>" id="txtOSCCostMT_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows2['COST_MT'];?>" /></td>
                                    </tr>
                                <?php $i++;}?>
                                </tbody>
                                    <tfoot >
                                        <tr height="10">
                                            <td colspan="5" align="left">
                                                <input type="hidden" name="txtAOSC_id" id="txtAOSC_id" value="<?php echo $rec2;?>" />
                                            </td>
                                         </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	All Other Miscellaneous Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%"></th>
                                            <th width="20%"></th>
                                            <th width="20%">Cost</th>
                                            <th width="20%">Vendor</th>
                                            <th width="20%">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $sql3 = "select * from fca_vci_other_misc_cost where FCAID='".$obj->getFun1()."'";
                                    $res3 = mysql_query($sql3);
                                    $rec3 = mysql_num_rows($res3);
                                    $i=1;
                                    while($rows3 = mysql_fetch_assoc($res3))
                                    {
                                    ?>
                                    <tr>
                                    <td><?php echo $obj->getOtherMiscCostNameBasedOnID($rows3['OTHER_MCOSTID']);?><input type="hidden"  readonly="true" name="txtHidOMCID_<?php echo $i;?>" id="txtHidOMCID_<?php echo $i;?>" value="<?php echo $rows3['OTHER_MCOSTID'];?>" /></td>                                    
                                    <?php if($obj->getOtherMiscCostNameBasedOnID($rows3['OTHER_MCOSTID']) == "Add Comm"){$readonly = "readonly";?>
                                    <td>
                                    	<input type="text"  name="txtOMCAbsComm_<?php echo $i;?>" id="txtOMCAbsComm_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows3['ADDCOMM'];?>" onKeyUp="getOMCCalculate(<?php echo $i;?>);" placeholder="%" />
                                    	<input type="hidden" name="txtOMCT_<?php echo $i;?>" id="txtOMCT_<?php echo $i;?>" class="form-control" readonly value="1" />
                                    </td>
                                    <?php }else{$readonly = "";?>
                                    <td></td>
                                    <?php }?>                                   
                                    <td><input type="text"  name="txtOMCAbs_<?php echo $i;?>" id="txtOMCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows3['AMOUNT'];?>" onKeyUp="getOMCCalculate(<?php echo $i;?>),getFinalCalculation()" placeholder="0.00" <?php echo $readonly; ?>/></td>
                                    <td><select  name="selAOMCVendorList_<?php echo $i; ?>" class="select form-control" id="selAOMCVendorList_<?php echo $i; ?>"></select></td>
										<script>$("#selAOMCVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selAOMCVendorList_<?php echo $i; ?>").val("<?php echo $rows3['VENDORID'];?>")</script>
                                    <td><input type="text"  name="txtOMCCostMT_<?php echo $i;?>" id="txtOMCCostMT_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows3['COST_MT'];?>" placeholder="0.00" /></td>
                                    </tr>
                                    <?php $i++;}?>
                                    </tbody>
                                    <tfoot>
                                        <tr height="10">
                                            <td colspan="5">
                                                <input type="hidden" name="txtAOMC_id" id="txtAOMC_id" value="<?php echo $rec3;?>" />
                                                <input type="hidden"  name="txtTTLOtherCost" id="txtTTLOtherCost" readonly value="" placeholder="0.00" />
                                                <input type="hidden"  name="txtTTLOtherCostMT" id="txtTTLOtherCostMT" value="" placeholder="0.00" />
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	Port Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center">Cost</th>
                                            <th width="20%" align="center">Vendor</th>
                                            <th width="20%" align="center">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbPortCosts">
                                    <?php 
                                    $mysql = "select * from fca_vci_portcosts where FCAID='".$obj->getFun1()."' order by FCA_PORTCOSTID asc";
                                    $myres = mysql_query($mysql);
                                    $myrec = mysql_num_rows($myres);
                                    if($myrec > 0)
                                    {$i=$j=$k=1;
                                        while($myrows = mysql_fetch_assoc($myres))
                                        {
                                            if($myrows['PORT'] == "Load")
                                            {?>
                                            <tr id="oscLProw_<?php echo $i;?>">
                                                <td align="left">Load Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
                                                <td align="left"></td>
                                                <td align="left"><input type="text"  name="txtLPOSCCost_<?php echo $i?>" id="txtLPOSCCost_<?php echo $i;?>" class="form-control" readonly value="<?php echo $myrows['COST'];?>" /></td>
                                                <td><select  name="selPCLPVendorList_<?php echo $i?>" class="select form-control" id="selPCLPVendorList_<?php echo $i?>"></select></td>
                                                <script>$("#selPCLPVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selPCLPVendorList_<?php echo $i; ?>").val("<?php echo $myrows['VENDORID'];?>")</script>
                                                <td align="left"><input type="text" name="txtLPOSCCostMT_<?php echo $i;?>" id="txtLPOSCCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $myrows['COST_MT'];?>" /></td>
                                           	</tr>
                                		<?php $i++;
											}else if($myrows['PORT'] == "Discharge")
                                            {
										?>
                                             <tr id="oscDProw_<?php echo $j;?>">
                                                <td align="left">Discharge Port <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
                                                <td align="left"></td>
                                                <td align="left"><input type="text"  name="txtDPOSCCost_<?php echo $j;?>" id="txtDPOSCCost_<?php echo $j;?>" class="form-control" readonly value="<?php echo $myrows['COST'];?>"  /></td>
                                                <td><select  name="selPCDPVendorList_<?php echo $j;?>" class="select form-control" id="selPCDPVendorList_<?php echo $j;?>"></select></td>
                                                <script>$("#selPCDPVendorList_<?php echo $j; ?>").html($("#selVendor").html());$("#selPCDPVendorList_<?php echo $j; ?>").val("<?php echo $myrows['VENDORID'];?>")</script>
                                                <td align="left"><input type="text" name="txtDPOSCCostMT_<?php echo $j;?>" id="txtDPOSCCostMT_<?php echo $j;?>" class="form-control" readonly value="<?php echo $myrows['COST_MT'];?>"  /></td>
                                                </tr>
                                             </tr>
                                            <?php $j++;}?>
                                    <?php }}?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <td align="left">Total Port Costs</td>
                                        <td align="left"></td>
                                        <td align="left"><input type="text"  name="txtTTLPortCosts" id="txtTTLPortCosts" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_PORT_COSTS");?>"  /></td>
                                        <td align="left"></td>
                                        <td align="left"><input type="text" name="txtTTLPortCostsMT" id="txtTTLPortCostsMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_PORT_COSTS_MT");?>"  /></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	Total Freight Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td width="20%">Total Freight Costs</td>
                                            <td width="20%"></td>
                                            <td width="20%"></td>
                                            <td width="20%"><input type="text"  name="txtTTLShippingCost" id="txtTTLShippingCost" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_SHIPPING_COST");?>" placeholder="0.00" /></td>
                                            <td width="20%"><input type="text"  name="txtTTLShippingCostMT" id="txtTTLShippingCostMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_SHIPPING_COSTMT");?>" placeholder="0.00" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php /*?><div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	Demurrage Dispatch Ship Owner
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center">Cost</th>
                                            <th width="20%" align="center">Vendor</th>
                                            <th width="20%" align="center">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbDDSW" >
                                    <?php 
                                    $sql4 = "select * from fca_vci_shipowner where FCAID='".$obj->getFun1()."'";
                                    $res4 = mysql_query($sql4);
                                    $rec4 = mysql_num_rows($res4);
                                    $i=$j=1;
                                    while($rows4 = mysql_fetch_assoc($res4))
                                    {
                                    if($rows4['PORT'] == "Load")
                                            {
                                            ?>
                                            <tr id="ddswLProw_<?php echo $i;?>">
                                                <td align="left">Load Port   <?php echo $obj->getPortNameBasedOnID($rows4['LOADPORTID']);?></td>
                                                <td align="left"></td>
                                                <td align="left"><input type="text"  name="txtddswLPCost_<?php echo $i;?>" id="txtddswLPCost_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows4['COST'];?>"/></td>
                                                <td><select  name="selDDSOLPVendorList_<?php echo $i;?>" class="select form-control" id="selDDSOLPVendorList_<?php echo $i;?>"></select></td>
                                                <script>$("#selDDSOLPVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selDDSOLPVendorList_<?php echo $i; ?>").val("<?php echo $rows4['VENDORID'];?>")</script>
                                                <td align="left"><input type="text" name="txtddswLPCostMT_<?php echo $i;?>" id="txtddswLPCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows4['COST_MT'];?>" /></td>
                                            </tr>
                                    		<?php $i++;
											}else if($rows4['PORT'] == "Discharge")
                                            {?>
                                            <tr id="ddswDProw_<?php echo $j;?>">
                                                <td align="left">Discharge Port   <?php echo $obj->getPortNameBasedOnID($rows4['LOADPORTID']);?></td>
                                                <td align="left"></td>
                                                <td align="left"><input type="text"  name="txtddswDPCost_<?php echo $j;?>" id="txtddswDPCost_<?php echo $j;?>" class="form-control" readonly value="<?php echo $rows4['COST'];?>" /></td>
                                                <td><select  name="selDDSODPVendorList_<?php echo $j;?>" class="select form-control" id="selDDSODPVendorList_<?php echo $j;?>"><?php $obj->getVendorListNewUpdate("");?></select></td>
                                                <script>$("#selDDSODPVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selDDSODPVendorList_<?php echo $i; ?>").val("<?php echo $rows4['VENDORID'];?>")</script>
                                                <td align="left"><input type="text" name="txtddswDPCostMT_<?php echo $j;?>" id="txtddswDPCostMT_<?php echo $j;?>" class="form-control" readonly value="<?php echo $rows4['COST_MT'];?>" /></td>
                                            </tr>
                                    <?php $j++;}?>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div><?php */?>
                            
                            <?php /*?><div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	Demurrage Dispatch Shipper
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center">Cost</th>
                                            <th width="20%" align="center">Vendor</th>
                                            <th width="20%" align="center">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbDDShipper" >
                                    <?php 
                                    $sql5 = "select * from fca_vci_dd_shipper where FCAID='".$obj->getFun1()."'";
                                    $res5 = mysql_query($sql5);
                                    $rec5 = mysql_num_rows($res5);
                                    $i=1;
                                    while($rows5 = mysql_fetch_assoc($res5))
                                    {
                                    ?>
                                        <tr id="ddshipLProw_<?php echo $i;?>">
                                        <td align="left">Load Port <?php echo $obj->getPortNameBasedOnID($rows5['LOADPORTID']);?></td>
                                        <td align="left"></td>
                                        <td align="left"><input type="text"  name="txtddshipLPCost_<?php echo $i;?>" id="txtddshipLPCost_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows5['COST'];?>"/></td>
                                        <td><select  name="selDDSLPVendorList_<?php echo $i;?>" class="select form-control" id="selDDSLPVendorList_<?php echo $i;?>"></select></td>
                                        <script>$("#selDDSLPVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selDDSLPVendorList_<?php echo $i; ?>").val("<?php echo $rows5['VENDORID'];?>")</script>
                                        <td align="left"><input type="text" name="txtddshipLPCostMT_<?php echo $i;?>" id="txtddshipLPCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows5['COST_MT'];?>" /></td>
                                        </tr>
                                    <?php $i++;}?>			
                                    </tbody>
                                </table>
                            </div><?php */?>
                            
                            <?php /*?><div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	Demurrage Dispatch Receiver
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center">Cost</th>
                                            <th width="20%" align="center">Vendor</th>
                                            <th width="20%" align="center">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbDDReceiver" >
                                    <?php 
                                    $sql6 = "select * from fca_vci_dd_receiver where FCAID='".$obj->getFun1()."'";
                                    $res6 = mysql_query($sql6);
                                    $rec6 = mysql_num_rows($res6);
                                    $i=1;
                                    while($rows6 = mysql_fetch_assoc($res6))
                                    {
                                    ?>
                                        <tr id="DDReceiDProw_<?php echo $i;?>">
                                            <td align="left">Discharge Port   <?php echo $obj->getPortNameBasedOnID($rows6['LOADPORTID']);?></td>
                                            <td align="left"></td>
                                            <td align="left"><input type="text"  name="txtDDReceiDPCost_<?php echo $i;?>" id="txtDDReceiDPCost_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows6['COST'];?>" /></td>
                                            <td><select  name="selDDRDPVendorList_<?php echo $i;?>" class="select form-control" id="selDDRDPVendorList_<?php echo $i;?>"><?php $obj->getVendorListNewUpdate("");?></select></td>
                                            <script>$("#selDDRDPVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selDDRDPVendorList_<?php echo $i; ?>").val('<?php echo $rows6['VENDORID'];?>')</script>
                                            <td align="left"><input type="text" name="txtDDReceiDPCostMT_<?php echo $i;?>" id="txtDDReceiDPCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows6['COST_MT'];?>" /></td>
                                        </tr>
                                    <?php $i++;}?>	
                                    </tbody>
                                </table>
                            </div><?php */?>
                            
                            <?php /*?><div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Demurrage Dispatch Nett Results
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center">Cost</th>
                                            <th width="20%" align="center">Vendor</th>
                                            <th width="20%" align="center">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbDDNR" >
                                        <tr>
                                            <td align="left">Load Port</td>
                                            <td align="left"></td>
                                            <td align="left"><input type="text"  name="txtDDNRLPCost" id="txtDDNRLPCost" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DDNR_LP_COST");?>" /></td>
                                            <td align="left">
                                                <select  name="selDDNRLPVendorList" class="select form-control" id="selDDNRLPVendorList">
                                                	<?php $obj->getVendorListNewUpdate($obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"LP_VENDORID"));?>
                                                </select>
                                        	</td>
                                            <td align="left"><input type="text" name="txtDDNRLPCostMT" id="txtDDNRLPCostMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DDNR_LP_COSTMT");?>"  /></td>
                                        </tr>
                                        <tr>
                                            <td align="left">Discharge Port</td>
                                            <td align="left"></td>
                                            <td align="left"><input type="text"  name="txtDDNRDPCost" id="txtDDNRDPCost" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DDNR_DP_COST");?>" /></td>
                                            <td align="left">
                                                <select  name="selDDNRDPVendorList" class="select form-control" id="selDDNRDPVendorList">
                                                <?php $obj->getVendorListNewUpdate($obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DP_VENDORID"));?></select>
                                            </td>
                                            <td align="left"><input type="text" name="txtDDNRDPCostMT" id="txtDDNRDPCostMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DDNR_DP_COSTMT");?>" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><?php */?>
                            
                            <div class="row invoice-info">
                                <div class="col-sm-3 invoice-col">
                                    VF Status
                                    <address>
                                        <select  name="selVType" class="form-control" id="selVType">
                                        <?php 
                                        $_REQUEST['selVType'] = $obj->getFun4();
                                        $obj->getVoyageType();
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="box-footer" align="right">
                            	<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
                            	<input type="hidden" name="action" id="action" value="submit" />
                            </div>
                        <!-----------------------------------------------ELSE PART----------------------------------------------->
						<?php }else{?>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                Fixture Type
                                    <address>
                                    <select  name="selFType" class="form-control" id="selFType" onChange="getBackgroundData();">
                                    <?php 
                                    $_REQUEST['selFType'] = 2;
                                    $obj->getFixtureType();
                                    ?>
                                    </select>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Main Particulars
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Nom ID
                                    <address>
                                    <input type="text" name="txtNomID" id="txtNomID" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getMappingData($mappingid,"NOMINATION_ID");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Vessel Name
                                    <address>
                                    <input type="text" name="txtVName" id="txtVName" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Vessel Type
                                    <address>
                                    <input type="text" name="txtVType" id="txtVType" class="form-control" autocomplete="off" placeholder="Vessel Type" readonly value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_TYPE"));?>" />
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Date<span style="font-size:10px; font-style:italic;"> (for financial year)</span>
                                    <address>
                                    <input type="text" name="txtDate" id="txtDate" class="form-control" autocomplete="off" placeholder="Date (for financial year)" value="<?php echo date("d-m-Y",time());?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Voyage No.
                                        <address>
                                        <input type="text" name="txtVNo" id="txtVNo" class="form-control" autocomplete="off" placeholder="Voyage No." value="<?php echo $obj->getFun10();?>" />
                                        </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Cost Sheet Name
                                    <address>
                                    <input type="text" name="txtENo" id="txtENo" class="form-control" autocomplete="off" style="color:red;" value="<?php echo $obj->getCostSheetNameBasedOnID($cost_sheet_id);?>" placeholder="Cost Sheet Name" />
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                            	<input name="rdoDWT" class="checkbox" id="rdoDWT1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoDWT == 1) echo "checked"; ?> onClick="showDWTField();"  /><br>
                            	DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>
                                <address>
                                    <input type="text" name="txtDWTS" id="txtDWTS"  class="form-control" autocomplete="off" placeholder="DWT" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUMMER_1");?>" disabled="disabled"/>
                                    
                                    <input type="hidden" name="txtDWTSVal" id="txtDWTSVal" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUMMER_1");?>"/>
                                </address>
                            </div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                            	<input name="rdoDWT" class="checkbox" id="rdoDWT2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoDWT == 2) echo "checked"; ?> onClick="showDWTField();" /><br>
                            	DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>
                                <address>
                                	<input type="text" name="txtDWTT" id="txtDWTT" class="form-control" autocomplete="off" placeholder="DWT" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"TROPICAL_1");?>" disabled="disabled">
                                    
                                    <input type="hidden" name="txtDWTTVal" id="txtDWTTVal" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"TROPICAL_1");?>">
                                    
                                    <input type="hidden" name="txtTCNo" id="txtTCNo" autocomplete="off" value="<?php echo $obj->getFun11();?>" />
                                </address>
                            </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    &nbsp;
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                            	<div class="col-sm-4 invoice-col">
                                	<input name="rdoCap" class="checkbox" id="rdoCap1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoCap == 1) echo "checked"; ?> onClick="showCapField();"  /><br>
                                Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
                                    <address>
                                   		<input type="text" name="txtGCap" id="txtGCap" class="form-control" autocomplete="off" placeholder="Grain Cap" value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"GRAIN");?>" disabled="disabled" onkeyup="getTotalDWT(),getTotalDWT1()"/>
                                    </address>
                            	</div><!-- /.col -->
                            	<div class="col-sm-4 invoice-col">
                                    <input name="rdoCap" class="checkbox" id="rdoCap2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoCap == 2) echo "checked"; ?> onClick="showCapField();" /><br>
                                    	Bale Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
                                    <address>
                                    	<input type="text" name="txtBCap" id="txtBCap" class="form-control" autocomplete="off" placeholder="Bale Cap" value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BALE");?>" disabled="disabled" onkeyup="getTotalDWT(),getTotalDWT1()" />
                                    </address>
                                </div><!-- /.col -->
                            	<div class="col-sm-4 invoice-col">
                            		&nbsp;&nbsp;<br>
                            		SF <span style="font-size:10px; font-style:italic;">(CBM/MT)</span>
                                    <address>
                                        <input type="text" name="txtSF" id="txtSF" class="form-control" placeholder="SF" autocomplete="off" value="<?php echo $obj->getFun9();?>"  onkeyup="getTotalDWT(),getTotalDWT1()"/>
                                    </address>
                            	</div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	&nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	&nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	Loadable <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                    <input type="text" name="txtLoadable" id="txtLoadable" class="form-control" placeholder="Loadable" readonly autocomplete="off" value="<?php echo $obj->getFun24();?>"/>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Market
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	<input name="rdoMMarket" id="rdoMMarket1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoMMarket == 1) echo "checked"; ?>  onclick="showMMarketField();"  /><br>
                                	Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
                                    	<input type="text" name="txtMTCPDRate" id="txtMTCPDRate" class="form-control" placeholder="Agreed Gross Freight" autocomplete="off" value=""  onkeyup="getFinalCalculation();" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	<input name="rdoMMarket" class="checkbox" id="rdoMMarket2" type="radio" style="cursor:pointer;" value="2" <?php if($rdoMMarket == 2) echo "checked"; ?> onClick="showMMarketField();" /><br>
                               		 Lumpsum <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
                                    	<input type="text" name="txtMLumpsum" id="txtMLumpsum" class="form-control" placeholder="Lumpsum" autocomplete="off" disabled value=""  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	&nbsp;&nbsp;<br>
                                	Addnl Cargo Rate <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
                                    	<input type="text"  name="txtAddnlCRate" id="txtAddnlCRate" class="form-control" placeholder="Addnl Cargo Rate" autocomplete="off" value=""  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Cargo
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	Quantity <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                    <input type="text" name="txtCQMT" id="txtCQMT" class="form-control" placeholder="Quantity" autocomplete="off" value="<?php echo $obj->getGreaterCargoQuantity($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));?>"  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                            
                                <div class="col-sm-4 invoice-col">
                                	Cargo Type
                                    <address>
                                        <select  name="selCType" class="form-control" id="selCType">
                                        <?php 
                                        $obj->getCargoTypeList();
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Cargo Name
                                    <address>
                                        <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCargoContarctForMapping($obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"CARGO_POSITION"));?></strong>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	<span style="font-size:13px; font-style:italic; color:#dc631e">( Please put dead freight quantity / addnl quantity separately )</span>  
                                    <address>
                                    	<input type="hidden" name="txtAQMT" id="txtAQMT" value=""  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	<input name="rdoQty" class="checkbox" id="rdoQty1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoQty == 1) echo "checked"; ?>  onclick="showQtyField();"  /><br>
                                	DF Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                    <input type="text" name="txtDFQMT" id="txtDFQMT" class="form-control" placeholder="DF Qty" autocomplete="off" value="0.00"  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	<input name="rdoQty" class="checkbox" id="rdoQty2" type="radio" style="cursor:pointer;" value="2" <?php if($rdoQty == 2) echo "checked"; ?> onClick="showQtyField();" /><br>
                                	Addnl Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                    <input type="text" name="txtAddnlQMT" id="txtAddnlQMT" class="form-control" placeholder="Addnl Qty " autocomplete="off" readonly value="0.00"  onkeyup="getFinalCalculation();"/>
                                        <select  name="selPort" id="selPort" style="display:none;" >
                                        <?php  $obj->getPortList();  ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Load Port
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	Load Port
                                    <address>
                                        <select  name="selLoadPort" class="form-control" id="selLoadPort">
                                        <?php 
                                        //$obj->getPortList();
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Cargo
                                    <address>
                                        <select  name="selLPCName" class="form-control" id="selLPCName" onChange="getLOadPortQty();" >
                                        <?php 
                                        $obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                               		Qty MT
                                    <address>
                                    <input type="text" name="txtLPQMT" id="txtLPQMT" class="form-control" placeholder="Qty MT" onKeyUp="getLoadPortCalculation();" autocomplete="off" value="" />
                                    </address>
                                </div><!-- /.col -->
								<div class="col-sm-4 invoice-col">
                                    Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                                        <address>
                                    	<input type="text" name="txtLPRMTDay" id="txtLPRMTDay" class="form-control" placeholder="Rate "  onkeyup="getLoadPortCalculation();" autocomplete="off" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                            		Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
                                    <input type="text" name="txtLPPortCosts" id="txtLPPortCosts" class="form-control" placeholder="Port Costs" autocomplete="off" />
                                    </address>
                            	</div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                            		Work Days <span style="font-size:10px; font-style:italic;">(Qty/Rate)</span>
                                    <address>
                                    <input type="text" name="txtLPWorkDay" id="txtLPWorkDay" class="form-control" placeholder="Work Days" autocomplete="off" readonly />
                                    </address>
                            	</div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	&nbsp;
                                	<address>
                            			&nbsp;
                           			</address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	&nbsp;
                                	<address>
                            			&nbsp;
                           			</address>
                                </div><!-- /.col -->
                               	<div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
                            			<button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addLoadPortDetails()">ADD</button>
                           			</address>
                            	</div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-primary">
                                        <div class="box-body no-padding">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="3%" align="center">#</th>
                                                    <th width="7%" align="left">Load Port</th>
                                                    <th width="10%" align="left">Cargo Name</th>
                                                    <th width="7%" align="left">Qty MT</th>
                                                    <th width="7%" align="left">Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                                                    <th width="7%" align="left">Work Days</th>
                                                    <th width="8%" align="left">Port Costs</th>
                                                    <input type="hidden" name="load_portID" id="load_portID" value="<?php echo $rec;?>" />
                                                </tr>
                                            </thead>
                                        <tbody id="tblLoadPort">
                                        <tr id="LProw_Empty">
                                        	<td valign="top" align="center" colspan="7" style="color:red;">Sorry , currently zero(0) records added.</td></tr>
                                        </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Discharge Port(s)
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                	Discharge Port
                                    <address>
                                    <select  name="selDisPort" class="form-control" id="selDisPort">
                                    <?php 
                                    //$obj->getPortList();
                                    ?>
                                    </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	Cargo
                                    <address>
                                        <select  name="selDPCName" class="form-control" id="selDPCName" onChange="getDisPortQty();" >
                                        <?php 
                                        $obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	Qty MT
                                    <address>
                                    <input type="text" name="txtDPQMT" id="txtDPQMT" class="form-control" placeholder="Qty MT" onKeyUp="getDisPortCalculation();" autocomplete="off" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                                    <address>
                                    <input type="text" name="txtDPRMTDay" id="txtDPRMTDay" class="form-control" placeholder="Rate "  onkeyup="getDisPortCalculation();" autocomplete="off" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
                                    <input type="text" name="txtDPPortCosts" id="txtDPPortCosts" class="form-control" placeholder="Port Costs" autocomplete="off" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Work Days <span style="font-size:10px; font-style:italic;">(Qty/Rate)</span>
                                    <address>
                                    <input type="text" name="txtDPWorkDay" id="txtDPWorkDay" class="form-control" placeholder="Work Days" autocomplete="off" readonly />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	&nbsp;
                                	<address>
                                	&nbsp;
                                	</address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                	&nbsp;
                                	<address>
                                	&nbsp;
                                	</address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    &nbsp;
                                    <address>
                                    <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addDisPortDetails()">ADD</button>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box box-primary">
                                        <div class="box-body no-padding">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                    <th width="3%" align="center">#</th>
                                                    <th width="7%" align="left">Discharge Port</th>
                                                    <th width="10%" align="left">Cargo Name</th>
                                                    <th width="7%" align="left">Qty MT</th>
                                                    <th width="7%" align="left">Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                                                    <th width="7%" align="left">Work Days</th>
                                                    <th width="7%" align="left">Port Costs</th>
                                                    <input type="hidden" name="dis_portID" id="dis_portID" value="<?php echo $rec1;?>" />
                                                    </tr>
                                                </thead>
                                            	<tbody id="tblDisPort">
                                                    <tr id="DProw_Empty">
                                                        <td valign="top" align="center" colspan="7" style="color:red;">Sorry , currently zero(0) records added.</td>
                                                    </tr>
                                            	</tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Freight Adjustment
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="25%"></th>
                                            <th width="25%">Percent</th>
                                            <th width="25%">USD</th>
                                            <th width="25%">Per MT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Gross Freight</td>
                                            <td></td>
                                            <td><input type="text"  name="txtFrAdjUsdGF" id="txtFrAdjUsdGF" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                                            <td><input type="text"  name="txtFrAdjUsdGFMT" id="txtFrAdjUsdGFMT" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                                        </tr>
                                        <tr>
                                            <td align="left">Dead Freight</td>
                                            <td></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdDF" id="txtFrAdjUsdDF" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdDFMT" id="txtFrAdjUsdDFMT" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                                        </tr>
                                        <tr>
                                            <td align="left" >Addnl Freight</td>
                                            <td ></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdAF" id="txtFrAdjUsdAF" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdAFMT" id="txtFrAdjUsdAFMT" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                                        </tr>
                                        <tr>
                                            <td align="left">Total Freight</td>
                                            <td></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdTF" id="txtFrAdjUsdTF" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdTFMT" id="txtFrAdjUsdTFMT" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                                        </tr>
                                        <tr>
                                            <td align="left">Address Commission</td>
                                            <td align="left"><input type="text"  name="txtFrAdjPerAC" id="txtFrAdjPerAC" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"/></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdAC" id="txtFrAdjUsdAC" class="form-control" autocomplete="off" readonly value="0.00"  /></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdACMT" id="txtFrAdjUsdACMT" class="form-control" autocomplete="off" readonly value="0.00"  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">Brokerage</td>
                                            <td align="left"><input type="text"  name="txtFrAdjPerAgC" id="txtFrAdjPerAgC" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation(),getValue()" /></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdAgC" id="txtFrAdjUsdAgC" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                                            <td></td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">Net Freight Payable</td>
                                            <td align="left"></td>
                                            <td align="center"><input type="text"  name="txtFrAdjUsdFP" id="txtFrAdjUsdFP" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Owner Related Costs (Others)
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                            	<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
											</tr>
										</thead>
										<tbody id="tbodyBrokerage">
                                             <tr id="tbrRow_1">
                                                <td><a href="#tb1'" onClick="removeBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td>
												<td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="form-control" readonly value="0.00" /></td>
												<td><select  name="selBroVList_1" class="select form-control" id="selBroVList_1"></select>
													<script>$("#selBroVList_1").html($("#selVendor").html());$("#selBroVList_1").val('');</script>
												</td>
											</tr>
                                            </tbody>
                                            <tbody>
											<tr>
                                                <td><button type="button" class="btn btn-primary btn-flat" onClick="addBrokerageRow()">Add</button>
                                                <input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="1"/></td>
												<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();" readonly/></td>
												<td><input type="text" name="txtBrComm" id="txtBrComm" class="form-control" readonly value="0.00" /></td>
												<td>
												</td>
											</tr>
											</tbody>
                                            
                                            <tbody>
											<?php 
											$sql1 = "select * from owner_related_cost_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
											$res1 = mysql_query($sql1);
											$rec1 = mysql_num_rows($res1);
											$i=1;
											while($rows1 = mysql_fetch_assoc($res1))
											{
											?>
											<tr>
												<td><?php echo $rows1['NAME'];?><input type="hidden" name="txtHidORCID_<?php echo $i;?>" id="txtHidORCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows1['OWNER_RCOSTID'];?>" /></td>
												<td></td>
												<td></td>												
												<td>
												<?php if($rows1['RDO_STATUS'] == 1){?>
												<input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>" onKeyUp="getORCCalculate(<?php echo $rows1['RDO_STATUS'];?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="" placeholder="0.00"  />
												<?php }else if($rows1['RDO_STATUS'] == 2){?>
												<input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>"  onkeyup="getORCCalculate(<?php echo $rows1['RDO_STATUS'];?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="" placeholder="0.00" style="color:red;"  />
												<?php }?>
												</td>
												<td>
												<select  name="selORCVList_<?php echo $i;?>" class="select form-control" id="selORCVList_<?php echo $i;?>"></select>
												<script>$("#selORCVList_<?php echo $i;?>").html($("#selVendor").html());$("#selORCVList_<?php echo $i;?>").val('');</script>
												<input type="hidden" name="txtHidORCAmt_<?php echo $i;?>" id="txtHidORCAmt_<?php echo $i;?>" autocomplete="off" value="0.00" />	
												</td>
											</tr>								
											<?php $i++;}?>
												
											<tr>
												<td>Total Shipowner Expenses <input type="hidden" name="txtORC_id" id="txtORC_id" value="<?php echo $rec1;?>" /></td>
												<td></td>
												<td></td>
												<td><input type="text"  name="txtTTLORCAmt" id="txtTTLORCAmt" class="form-control" readonly placeholder="0.00"/></td>
												<td></td>												
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td style="font-weight:bold;">Value/MT</td>
												<td><input type="text" name="txtTTLORCCostMT" id="txtTTLORCCostMT" class="form-control"  readonly="true" placeholder="0.00" /></td>
											</tr>
										</tbody>
									</table>
								</div>
                            </div>
                            <div class="row invoice-info" style="display:none;">
                                <div class="col-sm-4 invoice-col">
                                    Freight Payment
                                    <address>
                                    <input type="text"  name="txtTTLORCFP" id="txtTTLORCFP" class="form-control" autocomplete="off" readonly value="0.00"  />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Cost/MT
                                    <address>
                                    <input type="text" name="txtTTLORCFPCostMT" id="txtTTLORCFPCostMT" class="form-control" autocomplete="off" readonly value="0.00" />
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	All Other Shipping Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="20%"></th>
                                        <th width="20%"></th>
                                        <th width="20%">Cost</th>
                                        <th width="20%">Vendor</th>
                                        <th width="20%">Cost/MT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $sql2 = "select * from other_shipping_cost_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
                                $res2 = mysql_query($sql2);
                                $rec2 = mysql_num_rows($res2);
                                $i=1;
                                while($rows2 = mysql_fetch_assoc($res2))
                                { ?>
                                    <tr>
                                        <td align="left"><?php echo $rows2['NAME'];?><input type="hidden" name="txtHidOSCID_<?php echo $i;?>" id="txtHidOSCID_<?php echo $i;?>" readonly value="<?php echo $rows2['OTHER_SCOSTID'];?>" /></td>
                                        <td align="left"></td>
                                        <td align="left"><input type="text"  name="txtOSCAbs_<?php echo $i;?>" id="txtOSCAbs_<?php echo $i;?>" class="form-control" autocomplete="off"  placeholder="0.00" onKeyUp="getOSCostCalculate1(<?php echo $i;?>),getFinalCalculation()"/></td>
                                        <td><select  name="selAOSCVendorList_<?php echo $i; ?>" class="select form-control" id="selAOSCVendorList_<?php echo $i; ?>"></select></td>
										<script>$("#selAOSCVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selAOSCVendorList_<?php echo $i; ?>").val('')</script>
                                        <td align="left"><input type="text" name="txtOSCCostMT_<?php echo $i;?>" id="txtOSCCostMT_<?php echo $i;?>" class="form-control" autocomplete="off" placeholder="0.00" /></td>
                                    </tr>
                                <?php $i++;}?>
                                </tbody>
                                <tfoot >
                                    <tr height="10">
                                        <td colspan="5" align="left"><input type="hidden" name="txtAOSC_id" id="txtAOSC_id" value="<?php echo $rec2;?>" /></td>
                                    </tr>
                                </tfoot>
                            </table>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    	All Other Miscellaneous Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%"></th>
                                            <th width="20%">%</th>
                                            <th width="20%">Cost</th>
                                            <th width="20%">Vendor</th>
                                            <th width="20%">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $sql3 = "select * from other_misc_cost_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
                                    $res3 = mysql_query($sql3);
                                    $rec3 = mysql_num_rows($res3);
                                    $i=1;
                                    while($rows3 = mysql_fetch_assoc($res3))
                                    { ?>
                                    <tr>
                                        <td><?php echo $rows3['NAME'];?><input type="hidden"  readonly="true" name="txtHidOMCID_<?php echo $i;?>" id="txtHidOMCID_<?php echo $i;?>" value="<?php echo $rows3['OTHER_MCOSTID'];?>" /></td>
                                        <?php if($rows3['NAME'] == "Add Comm"){$readonly = "readonly";?>
                                        <td><input type="text"  name="txtOMCAbsComm_<?php echo $i;?>" id="txtOMCAbsComm_<?php echo $i;?>" class="form-control" autocomplete="off" value="" onKeyUp="getOMCCalculate(<?php echo $i;?>);" placeholder="%" /><input type="hidden" name="txtOMCT_<?php echo $i;?>" id="txtOMCT_<?php echo $i;?>" class="form-control" readonly value="1" /></td>
                                        <?php }else{$readonly = "";?>
                                        <td></td>
                                        <?php }?>
                                        <td><input type="text"  name="txtOMCAbs_<?php echo $i;?>" id="txtOMCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows3['AMOUNT'];?>" onKeyUp="getOMCCalculate(<?php echo $i;?>),getFinalCalculation()" placeholder="0.00" <?php echo $readonly; ?> /></td>                                        <td><select  name="selAOMCVendorList_<?php echo $i; ?>" class="select form-control" id="selAOMCVendorList_<?php echo $i; ?>"></select></td>
										<script>$("#selAOMCVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selAOMCVendorList_<?php echo $i; ?>").val('')</script>                                        
                                        <td><input type="text"  name="txtOMCCostMT_<?php echo $i;?>" id="txtOMCCostMT_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows3['COST_MT'];?>" placeholder="0.00" /></td>
                                    </tr>
                                    <?php $i++;}?>
                                         <tr height="10">
                                            <td colspan="5" align="left">
                                                <input type="hidden" name="txtAOMC_id" id="txtAOMC_id" value="<?php echo $rec3;?>" />
                                                <input type="hidden"  name="txtTTLOtherCost" id="txtTTLOtherCost" readonly value="" placeholder="0.00" />
                                                <input type="hidden"  name="txtTTLOtherCostMT" id="txtTTLOtherCostMT" value="" placeholder="0.00" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Port Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center">Cost</th>
                                            <th width="20%" align="center">Vendor</th>
                                            <th width="20%" align="center">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbPortCosts">
                                    
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td align="left">Total Port Costs</td>
                                            <td align="left"></td>
                                            <td align="left">
                                            	<input type="text"  name="txtTTLPortCosts" id="txtTTLPortCosts" class="form-control" autocomplete="off" readonly value="0.00"  />
                                            </td>
                                            <td align="left"></td>
                                            <td align="left">
                                            	<input type="text" name="txtTTLPortCostsMT" id="txtTTLPortCostsMT" class="form-control" autocomplete="off" readonly value="0.00"  />
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Total Freight Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td width="20%">Total Freight Costs</td>
                                            <td width="20%"></td>
                                            <td width="20%"></td>
                                            <td width="20%"><input type="text"  name="txtTTLShippingCost" id="txtTTLShippingCost" class="form-control" autocomplete="off" readonly value="" placeholder="0.00" /></td>
                                            <td width="20%"><input type="text"  name="txtTTLShippingCostMT" id="txtTTLShippingCostMT" class="form-control" autocomplete="off" readonly value="" placeholder="0.00" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <?php /*?><div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Demurrage Dispatch Ship Owner
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center">Cost</th>
                                            <th width="20%" align="center">Vendor</th>
                                            <th width="20%" align="center">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbDDSW" >
                                    
                                    </tbody>
                                </table>
                            </div><?php */?>
                            
                            <?php /*?><div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Demurrage Dispatch Shipper
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center">Cost</th>
                                            <th width="20%" align="center">Vendor</th>
                                            <th width="20%" align="center">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbDDShipper" >
                                    </tbody>
                                </table>
                            </div><?php */?>
                            
                            <?php /*?><div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Demurrage Dispatch Receiver
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center">Cost</th>
                                            <th width="20%" align="center">Vendor</th>
                                            <th width="20%" align="center">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbDDReceiver">
                                    </tbody>
                                </table>
                            </div><?php */?>
                            
                            <?php /*?><div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Demurrage Dispatch Nett Results
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center"></th>
                                            <th width="20%" align="center">Cost</th>
                                            <th width="20%" align="center">Vendor</th>
                                            <th width="20%" align="center">Cost/MT</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbDDNR" >
                                        <tr>
                                            <td align="left">Load Port </td>
                                            <td></td>
                                            <td align="left"><input type="text"  name="txtDDNRLPCost" id="txtDDNRLPCost" class="form-control" autocomplete="off" readonly value="0.00"/></td>
                                            <td align="left">
                                                <select  name="selDDNRLPVendorList" class="select form-control" id="selDDNRLPVendorList">
                                                	<?php $obj->getVendorListNewUpdate("");?>
                                                </select>
                                        	</td>
                                            <td align="left"><input type="text" name="txtDDNRLPCostMT" id="txtDDNRLPCostMT" class="form-control" autocomplete="off" readonly value="0.00"/></td>
                                        </tr>
                                        <tr>
                                        <td align="left">Discharge Port</td>
                                        <td align="left"></td>
                                        <td align="left">
                                        	<input type="text"  name="txtDDNRDPCost" id="txtDDNRDPCost" class="form-control" autocomplete="off" readonly value="0.00"/>
                                        </td>
                                        <td align="left">
                                        	<select  name="selDDNRDPVendorList" class="select form-control" id="selDDNRDPVendorList">
											<?php $obj->getVendorListNewUpdate("");?></select>
                                        </td>
                                        <td align="left">
                                        	<input type="text" name="txtDDNRDPCostMT" id="txtDDNRDPCostMT" class="form-control" autocomplete="off" readonly value="0.00"/>
                                         </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><?php */?>
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  	VF Status
                                    <address>
                                        <select  name="selVType" class="form-control" id="selVType">
                                        <?php $obj->getVoyageType(); ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            <div class="box-footer" align="right">
                                <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
                                <input type="hidden" name="action" id="action" value="submit" />
                            </div>
                         </div>
						<?php } ?>
                 	</form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
	$("#selLoadPort,#selDisPort").html($("#selPort").html());
	$("#txtGCap,#txtBCap,#txtSF,#txtTCPDRate,#txtLumpsum,#txtCQMT,#txtQMT,#txtDQMT,#txtFrAdjPerAC,#txtFrAdjPerAgC,#txtFrAdjUsdGF,#txtFrAdjUsdAC,#txtFrAdjUsdAgC,#txtFrAdjUsdFP,#txtFrAdjUsdGFMT,#txtFrAdjUsdACMT,#txtFrAdjApMT,#txtFrAdjNFpMT,#txtTTLORCFP,#txtTTLORCFPCostMT,#txtMTCPDRate,#txtMLumpsum,[id^=txtOSCAbs_],[id^=txtOMCAbs_],#txtAddnlCRate,#txtAddnlQMT,#txtAQMT,#txtDFQMT,#txtAddnlQMT, #txtLPQMT,#txtLPRMTDay,#txtLPBLQGross,#txtLPBLQNet,#txtLPWorkDay,#txtDPQMT,#txtDPRMTDay,#txtDPBLQGross,#txtDPBLQNet,#txtDPBLDate,#txtDPWorkDay,#txtFrAdjUsdDF,#txtFrAdjUsdDFMT,#txtFrAdjUsdAF,#txtFrAdjUsdAFMT,#txtFrAdjUsdTF,#txtFrAdjUsdTFMT,#txtLPPortCosts,#txtDPPortCosts,[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").numeric();
	
	$("#txtLPBLDate,#txtDPBLDate,#txtDate").datepicker({
		format: 'dd-mm-yyyy',
		autoclose:true
	});
	
	$(".areasize").autosize({append: "\n"});
	
	getFinalCalculation();
	showDWTField();
	showCapField();
	showMMarketField();
	showQtyField();
});

/*$(function() {
	$("#LP_list").dataTable();

});*/


function getValidate()
{
	if($("#txtTTLShippingCost").val() > 0)
	{
		getFinalCalculation();
		return true;
	}
	else
	{
		jAlert('Not saved empty cost sheet', 'Alert');
		return false;
	}
}

function getBackgroundData()
{
	if($("#selFType").val() == 1)
	{
		location.href = "voyage_estimation.php?mappingid=<?php echo $mappingid;?>&cost_sheet_id=<?php echo $cost_sheet_id;?>";
	}
	if($("#selFType").val() == 2)
	{
		location.href = "voyage_estimation1.php?mappingid=<?php echo $mappingid;?>&cost_sheet_id=<?php echo $cost_sheet_id;?>";
	}
}

function showCapField()
{
	if($("#rdoCap1").is(":checked"))
	{
		$("#txtGCap").removeAttr('disabled');
		$("#txtBCap").attr('disabled', 'disabled');
		$("#txtBCap").val('');
		getTotalDWT();
	}
	if($("#rdoCap2").is(":checked"))
	{
		$("#txtGCap").attr('disabled', 'disabled');
		$("#txtGCap").val('');
		$("#txtBCap").removeAttr('disabled');
		getTotalDWT1();
	}
}

function getTotalDWT()
{
	if($("#rdoDWT1").is(":checked")){var dwt = $("#txtDWTS").val();}
	if($("#rdoDWT2").is(":checked")){var dwt = $("#txtDWTT").val();}
	
	if($("#rdoCap1").is(":checked"))
	{
		if($("#txtGCap").val() == ""){var gcap = 0;}else{var gcap = $("#txtGCap").val();}
		if($("#txtSF").val() != "" || $("#txtSF").val() != 0)
		{
			var ttl = parseFloat(gcap) / parseFloat($("#txtSF").val());
			if(ttl > parseFloat(dwt))
			{
				//jAlert('Loadable Qty MT > DWT. Please recheck CBM and SF', 'Alert');
				$("#txtLoadable").val(dwt);
			}
			else
			{
				$("#txtLoadable").val(ttl.toFixed(2));
			}
		}
	}
}

function getTotalDWT1()
{
	if($("#rdoDWT1").is(":checked")){var dwt = $("#txtDWTS").val();}
	if($("#rdoDWT2").is(":checked")){var dwt = $("#txtDWTT").val();}
	
	if($("#rdoCap2").is(":checked"))
	{
		if($("#txtBCap").val() == ""){var bcap = 0;}else{var bcap = $("#txtBCap").val();}
		if($("#txtSF").val() != "" || $("#txtSF").val() != 0)
		{
			var ttl = parseFloat(bcap) / parseFloat($("#txtSF").val());
			if(ttl > parseFloat(dwt))
			{
				//jAlert('Loadable Qty MT > DWT. Please recheck CBM and SF', 'Alert');
				$("#txtLoadable").val(dwt);
			}
			else
			{
				$("#txtLoadable").val(ttl.toFixed(2));
			}
		}
	}
}

function showMMarketField()
{
	if($("#rdoMMarket1").is(":checked"))
	{
		$("#txtMTCPDRate").removeAttr('disabled');
		$("#txtMLumpsum").attr('disabled',true);
		$("#txtMLumpsum").val("");
		//$("#txtTTLShippingCost").val("0.00");
		$("#txtMTCPDRate").focus();
		//getAgreedFreight();
	}
	if($("#rdoMMarket2").is(":checked"))
	{
		$("#txtMTCPDRate").attr('disabled',true);
		$("#txtMLumpsum").removeAttr('disabled');
		$("#txtMTCPDRate").val("");
		//$("#txtTTLShippingCost").val("0.00");
		$("#txtMLumpsum").focus();
		//getFreightLumsump();
	}
}

function showDWTField()
{
	if($("#rdoDWT1").is(":checked"))
	{
		$("#txtDWTS").removeAttr('disabled');
		$("#txtDWTT").attr('disabled', 'disabled');
		$("#txtDWTS").val($("#txtDWTSVal").val());
		$("#txtDWTT").val('');
		getTotalDWT();
		getTotalDWT1();
	}
	if($("#rdoDWT2").is(":checked"))
	{
		$("#txtDWTS").attr('disabled', 'disabled');
		$("#txtDWTT").removeAttr('disabled');
		$("#txtDWTT").val($("#txtDWTTVal").val());
		$("#txtDWTS").val('');
		getTotalDWT();
		getTotalDWT1();
	}
}

function showQtyField()
{
	if($("#rdoQty1").is(":checked"))
	{
		$("#txtDFQMT").removeAttr('readonly');
		$("#txtAddnlQMT").attr('readonly',true);
		$("#txtAddnlQMT").val("0.00");
		$("#txtDFQMT").focus();
	}
	if($("#rdoQty2").is(":checked"))
	{
		$("#txtDFQMT").attr('readonly',true);
		$("#txtAddnlQMT").removeAttr('readonly');
		$("#txtDFQMT").val("0.00");
		$("#txtAddnlQMT").focus();
	}
}

function getCalculate()
{
	if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
	if($("#txtAQMT").val() == ""){var a_qty = 0;}else{var a_qty = $("#txtAQMT").val();}
	var calc = parseFloat(c_qty) - parseFloat(a_qty);
	if(calc >= 0){
	$("#txtDFQMT").val(calc.toFixed(2));
	$("#txtAddnlQMT").val(0);
	$("#txtHidAddnlQMT").val(0);
	}
	if(calc < 0){
	$("#txtDFQMT").val(0);
	$("#txtAddnlQMT").val(Math.abs(calc.toFixed(2)));
	$("#txtHidAddnlQMT").val(calc.toFixed(2));
	}
}

function getLOadPortQty()
{
	if($("#selLPCName").val() != "")
	{
		if($("#txtCQMT").val() != "")
		{
			$("#txtLPQMT").val(parseFloat($("#txtCQMT").val()) - parseFloat($("[id^=txtLpQMT_]").sum()));
			getLoadPortCalculation();
		}
		else
		{
			$("#txtLPQMT").val(0);
		}
	}
	else
	{
		$("#txtLPQMT,#txtLPRMTDay,#txtLPWorkDay").val("");
	}
}

function getLoadPortCalculation()
{
	if($("#txtLPQMT").val() != "" && $("#txtLPRMTDay").val() != "") 
	{
		var value = ($("#txtLPQMT").val() / $("#txtLPRMTDay").val());
		$("#txtLPWorkDay").val(value.toFixed(2));
	}
	else
	{
		$("#txtLPWorkDay").val('0.00');
	}
}

function addLoadPortDetails()
{ 
	if($("#selLoadPort").val() != "" && $("#selLPCName").val() != "" && $("#txtLPQMT").val() != "" && $("#txtLPRMTDay").val() != "" && $("#txtLPWorkDay").val() != "" && $("#txtLPPortCosts").val() != "")
	{
		var id 				= $("#load_portID").val();
		id 					= (id - 1) + 2;
							  $("#LProw_Empty").remove();
		var load_port 		= document.getElementById("selLoadPort").selectedIndex;
		var load_port_text 	= document.getElementById("selLoadPort").options[load_port].text;
		
		var cargo 			= document.getElementById("selLPCName").selectedIndex;
		var cargo_text 		= document.getElementById("selLPCName").options[cargo].text;
				
		$('<tr id="lp_Row_'+id+'"><td align="center"><a href="#lp'+id+'" onclick="removeLoadPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left">'+load_port_text+'<input type="hidden" name="txtLoadPort_'+id+'" id="txtLoadPort_'+id+'" value="'+$("#selLoadPort").val()+'"/></td><td align="left">'+cargo_text+'<input type="hidden" name="txtLPCID_'+id+'" id="txtLPCID_'+id+'" value="'+$("#selLPCName").val()+'"/></td><td align="left">'+$("#txtLPQMT").val()+'<input type="hidden" name="txtLpQMT_'+id+'" id="txtLpQMT_'+id+'" value="'+$("#txtLPQMT").val()+'"/></td><td align="left">'+$("#txtLPRMTDay").val()+'<input type="hidden" name="txtLpRate_'+id+'" id="txtLpRate_'+id+'" value="'+$("#txtLPRMTDay").val()+'"/></td><td align="left">'+$("#txtLPWorkDay").val()+'<input type="hidden" name="txtLpBLWorkDays_'+id+'" id="txtLpBLWorkDays_'+id+'" value="'+$("#txtLPWorkDay").val()+'"/></td><td align="left">'+$("#txtLPPortCosts").val()+'<input type="hidden" name="txtLPPCosts_'+id+'" id="txtLPPCosts_'+id+'" value="'+$("#txtLPPortCosts").val()+'"/></td></tr>').appendTo("#tblLoadPort");
		
		if($("#txtLPPortCosts").val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtLPPortCosts").val();}
		/*var loadid = 0;
		$('[id^=txtLpBLNet_]').each(function(index) {
					var loadid = this.value;
		});
		if(loadid == 0)
		{
			var per_mt = $("#txtCQMT").val();
		}
		else
		{
			var per_mt = loadid;
		}*/
		
		//autocomplete="off" onkeyup="getPortCostSum('+id+','+lp+');"
		
		var per_mt = $("#txtCQMT").val();
		var lpcostMT = parseFloat(lp_cost) / parseFloat(per_mt);
		var lp = "'LP'";			
		$('<tr id="oscLProw_'+id+'"><td align="left">Load Port '+load_port_text+'</td><td></td><td align="left"><input type="text"  name="txtLPOSCCost_'+id+'" id="txtLPOSCCost_'+id+'" class="form-control" readonly="true" value="'+$("#txtLPPortCosts").val()+'" /></td><td><select  name="selPCLPVendorList_'+id+'" class="select form-control" id="selPCLPVendorList_'+id+'"><?php $obj->getVendorListNewUpdate("");?></select></td><td align="left"><input type="text" name="txtLPOSCCostMT_'+id+'" id="txtLPOSCCostMT_'+id+'" class="form-control" readonly="true" value="'+lpcostMT.toFixed(2)+'"/></td></tr><tr id="oscLProw1_'+id+'" height="5"><td colspan="10" align="left" class="text" valign="top" ></td></tr>').appendTo("#tbPortCosts");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum())/ parseFloat(per_mt);
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
		/*$('<tr id="ddswLProw_'+id+'"><td align="left" valign="top" >Load Port '+load_port_text+'</td><td></td><td align="left"><input type="text"  name="txtddswLPCost_'+id+'" id="txtddswLPCost_'+id+'" class="form-control"  readonly="true" value="0.00"/></td><td><select  name="selDDSOLPVendorList_'+id+'" class="select form-control" id="selDDSOLPVendorList_'+id+'"><?php //$obj->getVendorListNewUpdate("");?></select></td><td align="left"><input type="text" name="txtddswLPCostMT_'+id+'" id="txtddswLPCostMT_'+id+'" class="form-control" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDSW");*/
		
		/*$('<tr id="ddshipLProw_'+id+'"><td align="left">Load Port '+load_port_text+'</td><td></td><td align="left" valign="top"><input type="text"  name="txtddshipLPCost_'+id+'" id="txtddshipLPCost_'+id+'" class="form-control" readonly="true" value="0.00"/></td><td><select  name="selDDSLPVendorList_'+id+'" class="select form-control" id="selDDSLPVendorList_'+id+'"><?php //$obj->getVendorListNewUpdate("");?></select></td><td align="left"><input type="text" name="txtddshipLPCostMT_'+id+'" id="txtddshipLPCostMT_'+id+'" class="form-control" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDShipper");*/
		
		$("#load_portID").val(id);
		$("#selLoadPort,#selLPCName,#txtLPQMT,#txtLPRMTDay,#txtLPBLQGross,#txtLPBLQNet,#txtLPBLDate,#txtLPWorkDay,#txtLPPortCosts").val("");
		getFinalCalculation();

	}
	else
	{
		jAlert('Please fill all the records for Load Port', 'Alert');
	}
}

function removeLoadPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){
			$("#lp_Row_"+var1).remove();
			$("#ddswLProw_"+var1).remove();		
			$("#ddshipLProw_"+var1).remove();
			$("#oscLProw_"+var1).remove();
			$("#oscLProw1_"+var1).remove();
			/*var loadid = 0;
			$('[id^=txtLpBLNet_]').each(function(index) {
						var loadid = this.value;
			});
			if(loadid == 0)
			{
				var per_mt = $("#txtCQMT").val();
			}
			else
			{
				var per_mt = loadid;
			}*/
			var per_mt = $("#txtCQMT").val();
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum())/ parseFloat(per_mt);
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			getFinalCalculation();
		}
		else{return false;}
		});	
}

function addDisPortDetails()
{
	if($("#selDisPort").val() != "" && $("#selDPCName").val() != "" && $("#txtDPQMT").val() != "" && $("#txtDPRMTDay").val() != "" && $("#txtDPWorkDay").val() != "" && $("#txtDPPortCosts").val() != "")
	{
		var id = $("#dis_portID").val();
		id = (id - 1) + 2;
		$("#DProw_Empty").remove();
		var dis_port = document.getElementById("selDisPort").selectedIndex;
		var dis_port_text = document.getElementById("selDisPort").options[dis_port].text;
		
		var cargo = document.getElementById("selDPCName").selectedIndex;
		var cargo_text = document.getElementById("selDPCName").options[cargo].text;
		
		$('<tr id="dp_Row_'+id+'"><td align="center"><a href="#dp'+id+'" onclick="removeDisPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left">'+dis_port_text+'<input type="hidden" name="txtDisPort_'+id+'" id="txtDisPort_'+id+'" value="'+$("#selDisPort").val()+'"/></td><td align="left">'+cargo_text+'<input type="hidden" name="txtDPCID_'+id+'" id="txtDPCID_'+id+'" value="'+$("#selDPCName").val()+'"/></td><td align="left">'+$("#txtDPQMT").val()+'<input type="hidden" name="txtDpQMT_'+id+'" id="txtDpQMT_'+id+'" value="'+$("#txtDPQMT").val()+'"/></td><td align="left">'+$("#txtDPRMTDay").val()+'<input type="hidden" name="txtDpRate_'+id+'" id="txtDpRate_'+id+'" value="'+$("#txtDPRMTDay").val()+'"/></td><td align="left">'+$("#txtDPWorkDay").val()+'<input type="hidden" name="txtDpBLWorkDays_'+id+'" id="txtDpBLWorkDays_'+id+'" value="'+$("#txtDPWorkDay").val()+'"/></td><td align="left">'+$("#txtDPPortCosts").val()+'<input type="hidden" name="txtDPPCosts_'+id+'" id="txtDPPCosts_'+id+'" value="'+$("#txtDPPortCosts").val()+'"/></td></tr>').appendTo("#tblDisPort");
		
		if($("#txtDPPortCosts").val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPPortCosts").val();}
		/*var loadid = 0;
		$('[id^=txtLpBLNet_]').each(function(index) {
					var loadid = this.value;
		});
		if(loadid == 0)
		{
			var per_mt = $("#txtCQMT").val();
		}
		else
		{
			var per_mt = loadid;
		}*/
		//autocomplete="off" onkeyup="getPortCostSum('+id+','+dp+');"
		
		var per_mt = $("#txtCQMT").val();
		var dpcostMT = parseFloat(dp_cost) / parseFloat(per_mt);
		var dp = "'DP'";
		$('<tr id="oscDProw_'+id+'"><td align="left">Discharge Port '+dis_port_text+'</td><td></td><td align="left"><input type="text" name="txtDPOSCCost_'+id+'" id="txtDPOSCCost_'+id+'" class="form-control"  readonly="true"  value="'+$("#txtDPPortCosts").val()+'"/></td><td><select  name="selPCDPVendorList_'+id+'" class="select form-control" id="selPCDPVendorList_'+id+'"><?php $obj->getVendorListNewUpdate("");?></select></td><td align="left"><input type="text" name="txtDPOSCCostMT_'+id+'" id="txtDPOSCCostMT_'+id+'" class="form-control" readonly="true" value="'+dpcostMT.toFixed(2)+'"/></td></tr><tr id="oscDProw1_'+id+'" height="5"><td colspan="10" align="left" class="text" valign="top" ></td></tr>').appendTo("#tbPortCosts");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum())/ parseFloat(per_mt);
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
		/*$('<tr id="ddswDProw_'+id+'"><td align="left">Discharge Port  '+dis_port_text+'</td><td></td><td align="left"><input type="text"  name="txtddswDPCost_'+id+'" id="txtddswDPCost_'+id+'" class="form-control" readonly="true" value="0.00" /></td><td><select  name="selDDSODPVendorList_'+id+'" class="select form-control" id="selDDSODPVendorList_'+id+'"><?php //$obj->getVendorListNewUpdate("");?></select></td><td align="left"><input type="text" name="txtddswDPCostMT_'+id+'" id="txtddswDPCostMT_'+id+'" class="form-control" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDSW");*/
		
		/*$('<tr id="DDReceiDProw_'+id+'"><td align="left">Discharge Port  '+dis_port_text+'</td><td></td><td align="left"><input type="text"  name="txtDDReceiDPCost_'+id+'" id="txtDDReceiDPCost_'+id+'" class="form-control" readonly="true" value="0.00" /></td><td><select  name="selDDRDPVendorList_'+id+'" class="select form-control" id="selDDRDPVendorList_'+id+'"><?php //$obj->getVendorListNewUpdate("");?></select></td><td align="left"><input type="text" name="txtDDReceiDPCostMT_'+id+'" id="txtDDReceiDPCostMT_'+id+'" class="form-control" readonly="true" value="0.00"  /></td></tr>').appendTo("#tbDDReceiver");*/
				
		$("#dis_portID").val(id);
		$("#selDisPort,#selDPCName,#txtDPQMT,#txtDPRMTDay,#txtDPBLQGross,#txtDPBLQNet,#txtDPBLDate,#txtDPWorkDay,#txtDPPortCosts").val("");
		getFinalCalculation();
	}
	else
	{
		jAlert('Please fill all the records for Discharge Port', 'Alert');
	}
}

function getPortCostSum(var1,port)
{
	/*var loadid = 0;
	$('[id^=txtLpBLNet_]').each(function(index) {
				var loadid = this.value;
	});
	if(loadid == 0)
	{
		var per_mt = $("#txtCQMT").val();
	}
	else
	{
		var per_mt = loadid;
	}*/
	var per_mt = $("#txtCQMT").val();
	if(port == 'LP')
	{
		if($("#txtLPOSCCost_"+var1).val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtLPOSCCost_"+var1).val();}
		var lpcostMT = parseFloat(lp_cost) / parseFloat(per_mt);
		$("#txtLPOSCCostMT_"+var1).val(lpcostMT.toFixed(2));
	}
	else if(port == 'DP')
	{
		if($("#txtDPOSCCost_"+var1).val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPOSCCost_"+var1).val();}
		var dpcostMT = parseFloat(dp_cost) / parseFloat(per_mt);
		$("#txtDPOSCCostMT_"+var1).val(dpcostMT.toFixed(2));
	}		
	var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum())/ parseFloat(per_mt);
	$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum().toFixed(2));
	$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
	getFinalCalculation();
}

function removeDisPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#dp_Row_"+var1).remove();
			$("#ddswDProw_"+var1).remove();
			$("#DDReceiDProw_"+var1).remove();
			$("#oscDProw_"+var1).remove();
			$("#oscDProw1_"+var1).remove();
			/*var loadid = 0;
			$('[id^=txtLpBLNet_]').each(function(index) {
						var loadid = this.value;
			});
			if(loadid == 0)
			{
				var per_mt = $("#txtCQMT").val();
			}
			else
			{
				var per_mt = loadid;
			}*/
			var per_mt = $("#txtCQMT").val();
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum())/ parseFloat(per_mt);
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			getFinalCalculation();
		}
		else{return false;}
		});	
}

function getDisPortQty()
{
	if($("#selDPCName").val() != "")
	{
		if($("#txtCQMT").val() != "")
		{
			$("#txtDPQMT").val(parseFloat($("#txtCQMT").val()) - parseFloat($("[id^=txtDpQMT_]").sum()));
			getDisPortCalculation();
		}
		else
		{
			$("#txtDPQMT").val(0);
		}
	}
	else
	{
		$("#txtDPQMT,#txtDPRMTDay,#txtDPWorkDay").val("");
	}
}

function getDisPortCalculation()
{
	if($("#txtDPQMT").val() != "" && $("#txtDPRMTDay").val() != "") 
	{
		var value = ($("#txtDPQMT").val() / $("#txtDPRMTDay").val());
		$("#txtDPWorkDay").val(value.toFixed(2));
	}
	else
	{
		$("#txtDPWorkDay").val('0.00');
	}
}

function getFinalCalculation()
{
	/*var loadid = 0;
	$('[id^=txtLpBLNet_]').each(function(index) {
				var loadid = this.value;
	});
	if(loadid == 0)
	{
		var per_mt = $("#txtCQMT").val();
	}
	else
	{
		var per_mt = loadid;
	}*/
	var per_mt = $("#txtCQMT").val();
	if($("#rdoMMarket1").is(":checked"))
	{
		if($("#txtMTCPDRate").val() == ""){var agr_gross_fr = 0;}else{var agr_gross_fr = $("#txtMTCPDRate").val();}
		//if($("#txtAQMT").val() == ""){var actual_qty = 0;}else{var actual_qty = $("#txtAQMT").val();}
		
		var gross_fr = parseFloat(agr_gross_fr) * parseFloat(per_mt);
		$("#txtFrAdjUsdGF").val(gross_fr.toFixed(2));
		
		var gross_fr_mt = parseFloat(gross_fr) / parseFloat(per_mt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		//.......................................
		if($("#txtDFQMT").val() == "" || $("#txtDFQMT").val() == 0)
		{
			var df_qty = 0;
			$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
			
		}
		else
		{
			var df_qty = $("#txtDFQMT").val();
			var dead_fr = parseFloat(agr_gross_fr) * parseFloat(df_qty);
			$("#txtFrAdjUsdDF").val(dead_fr.toFixed(2));
			
			var dead_fr_mt = parseFloat(dead_fr) / parseFloat(df_qty);
			$("#txtFrAdjUsdDFMT").val(dead_fr_mt.toFixed(2));
		}
		//.......................................
		if($("#txtAddnlCRate").val() == ""){var addnl_rate = 0;}else{var addnl_rate = $("#txtAddnlCRate").val();}
		//if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
		if($("#txtAddnlQMT").val() == "" || $("#txtAddnlQMT").val() == 0 )
		{
			var addnl_qty = 0;
			$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		}
		else
		{
			var addnl_qty = $("#txtAddnlQMT").val();
			var addnl_fr = parseFloat(addnl_rate) * parseFloat(addnl_qty);
			$("#txtFrAdjUsdAF").val(addnl_fr.toFixed(2));
			
			var addnl_fr_mt = parseFloat(addnl_fr) / parseFloat(addnl_qty);
			$("#txtFrAdjUsdAFMT").val(addnl_fr_mt.toFixed(2));
		}
		//.............................................
		
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum().toFixed(2));
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / (parseFloat(per_mt) + parseFloat(df_qty) + parseFloat(addnl_qty));
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
	}
	
	if($("#rdoMMarket2").is(":checked"))
	{
		$("#txtFrAdjUsdGF").val($("#txtMLumpsum").val());
		var gross_fr_mt = parseFloat($("#txtFrAdjUsdGF").val()) / parseFloat(per_mt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		
		$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
		$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum());
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / parseFloat(per_mt);
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));

	}
	
	if($("#txtFrAdjUsdTF").val() == ""){var ttl_fr = 0;}else{var ttl_fr = $("#txtFrAdjUsdTF").val();}
	if($("#txtFrAdjPerAC").val() == ""){var ac_per = 0;}else{var ac_per = $("#txtFrAdjPerAC").val();}
	
	var address_commission = (parseFloat(ttl_fr) * parseFloat(ac_per)) / 100;
	$("#txtFrAdjUsdAC").val(address_commission.toFixed(2));
	var address_commission_mt = parseFloat($("#txtFrAdjUsdAC").val()) / parseFloat(per_mt);
	$("#txtFrAdjUsdACMT").val(address_commission_mt.toFixed(2));
	
	if($("#txtFrAdjPerAgC").val() == ""){var ag_per = 0;}else{var ag_per = $("#txtFrAdjPerAgC").val();}
	var agent_commission = (parseFloat(ttl_fr) * parseFloat(ag_per)) / 100;
	$("#txtFrAdjUsdAgC").val(agent_commission.toFixed(2));
	
	var net_fr = parseFloat(ttl_fr) - parseFloat($("#txtFrAdjUsdAC,#txtFrAdjUsdAgC").sum());
	$("#txtFrAdjUsdFP,#txtTTLORCFP").val(net_fr.toFixed(2));
	
	var net_fr_mt = parseFloat(net_fr) / parseFloat(per_mt);
	$("#txtTTLORCFPCostMT").val(net_fr_mt.toFixed(2));
	
	$("#txtTTLShippingCost").val($("#txtTTLORCFP,[id^=txtOSCAbs_],[id^=txtOMCAbs_],#txtTTLPortCosts").sum().toFixed(2));
	var ttl_shipping_mt = parseFloat($("#txtTTLShippingCost").val()) / parseFloat(per_mt);
	$("#txtTTLShippingCostMT").val(ttl_shipping_mt.toFixed(2));
	
	/****************************Owner Related Costs (Others)*********************************/
	
	for(var l = 1;l<=$("#txtBRokageCount").val();l++)
	{
	if($("#txtBrCommPercent_"+l).val() == ""){var brokerage_comm = 0;}else{var brokerage_comm = $("#txtBrCommPercent_"+l).val();}
	if($("#txtFrAdjUsdTF").val() == ""){var t_frt = 0;}else{var t_frt = $("#txtFrAdjUsdTF").val();}
	
	var brokerage_comm_usd = (parseFloat(t_frt) * parseFloat(brokerage_comm))/100;
	$("#txtBrComm_"+l).val(brokerage_comm_usd.toFixed(2));
	}
	$("#txtBrCommPercent").val($("[id^=txtBrCommPercent_]").sum().toFixed(2));
	$("#txtBrComm").val($("[id^=txtBrComm_]").sum().toFixed(2));
	
	$("#txtTTLORCAmt").val($("[id^=txtHidORCAmt_],#txtBrComm").sum());
	var calc = parseFloat($("#txtTTLORCAmt").val()) / parseFloat(per_mt);
	$("#txtTTLORCCostMT").val(calc.toFixed(2));
	
	$("#txtTTLOwnersExpenses").val($("[id^=txtBunkerCost_],#txtTTLPortCosts,#txtTTLORCAmt").sum().toFixed(2));
	$("#txtTTLCharterersExpenses").val($("[id^=txtCCAbs_]").sum().toFixed(2));
}

function getOSCostCalculate1(var1)
{
	/*var loadid = 0;
	$('[id^=txtLpBLNet_]').each(function(index) {
				var loadid = this.value;
	});
	if(loadid == 0)
	{
		var per_mt = $("#txtCQMT").val();
	}
	else
	{
		var per_mt = loadid;
	}*/
	var per_mt = $("#txtCQMT").val();
	if($("#txtOSCAbs_"+var1).val() == ""){var osc_abs = 0;}else{var osc_abs = $("#txtOSCAbs_"+var1).val();}
	var calc = parseFloat(osc_abs) / parseFloat(per_mt);
	$("#txtOSCCostMT_"+var1).val(calc.toFixed(2));
}

function getOMCCalculate(var1)
{
	/*var loadid = 0;
	$('[id^=txtLpBLNet_]').each(function(index) {
				var loadid = this.value;
	});
	if(loadid == 0)
	{
		var per_mt = $("#txtCQMT").val();
	}
	else
	{
		var per_mt = loadid;
	}*/
	var per_mt = $("#txtCQMT").val();
	if($("#txtOMCAbs_"+var1).val() == ""){var omc_abs = 0;}else{var omc_abs = $("#txtOMCAbs_"+var1).val();}
	var calc = parseFloat(omc_abs) / parseFloat(per_mt);
	$("#txtOMCCostMT_"+var1).val(calc.toFixed(2));
	
	if($("#txtOMCT_"+var1).val()==1)
	{
		if($("#txtOMCAbsComm_"+var1).val() == ""){var omc_percnt = 0;}else{var omc_percnt = $("#txtOMCAbsComm_"+var1).val();}
		if($("#txtFrAdjUsdFP").val() == ""){var omc_adjft = 0;}else{var omc_adjft = $("#txtFrAdjUsdFP").val();}
		var omc_abs = parseFloat(parseFloat(omc_adjft) * parseFloat(omc_percnt))/100;
		$("#txtOMCAbs_"+var1).val(omc_abs.toFixed(2))
	}
	else
	{
	if($("#txtOMCAbs_"+var1).val() == ""){var omc_abs = 0;}else{var omc_abs = $("#txtOMCAbs_"+var1).val();}
	}
	var calc = parseFloat(omc_abs) / parseFloat(per_mt);
	
	$("#txtOMCCostMT_"+var1).val(calc.toFixed(2));
	
	getFinalCalculation();			
}

function getValue()
{
	$("#txtOSCAbs_1").val($("#txtFrAdjUsdAgC").val());
	/*var loadid = 0;
	$('[id^=txtLpBLNet_]').each(function(index) {
				var loadid = this.value;
	});
	if(loadid == 0)
	{
		var per_mt = $("#txtCQMT").val();
	}
	else
	{
		var per_mt = loadid;
	}*/
	var per_mt = $("#txtCQMT").val();
	if($("#txtOSCAbs_1").val() == ""){var osc_abs = 0;}else{var osc_abs = $("#txtOSCAbs_1").val();}
	var calc = parseFloat(osc_abs) / parseFloat(per_mt);
	$("#txtOSCCostMT_1").val(calc.toFixed(2));
	getFinalCalculation();
}

function addBrokerageRow()
{
	var id = $("#txtBRokageCount").val();
	if($("#txtBrCommPercent_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="tbrRow_'+id+'"><td><a href="#tb1" onclick="removeBrokerage('+id+');" ><i class="fa fa-times" style="color:red;"></a></td><td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td><td><input type="text" name="txtBrCommPercent_'+id+'" id="txtBrCommPercent_'+id+'" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td><td><input type="text" name="txtBrComm_'+id+'" id="txtBrComm_'+id+'" class="form-control" readonly value="0.00" /></td><td><select  name="selBroVList_'+id+'" class="select form-control" id="selBroVList_'+id+'"></select></td></tr>').appendTo("#tbodyBrokerage");
	    $("#selBroVList_"+id).html($("#selVendor").html());$("#selBroVList_"+id).val('');
		$("[id^=txtBrCommPercent_]").numeric();	
		$("#txtBRokageCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeBrokerage(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#tbrRow_"+var1).remove();
				getFinalCalculation();			
			 }
		else{return false;}
		});
}

function getORCCalculate(status,var1)
{
	if(status == 1)
	{
		$("#txtHidORCAmt_"+var1).val($("#txtORCAmt_"+var1).val());
	}
	if(status == 2)
	{
		$("#txtHidORCAmt_"+var1).val("-"+$("#txtORCAmt_"+var1).val());
	}
	getFinalCalculation();
}
</script>
    </body>
</html>