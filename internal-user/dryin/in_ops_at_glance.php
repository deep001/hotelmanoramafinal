<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

if (@$_REQUEST['action'] == 'submit')
 {
 	if($_REQUEST['txtStatus'] == "")
	{
		if($_REQUEST['txtMappingid2'] == "")
		{
			$msg = $obj->insertActualCostSheetName();
			header('Location : ./in_ops_at_glance.php?msg=3');
		}
		else
		{
			$msg = $obj->deleteInOpsEntry($_REQUEST['txtMappingid2']);
		}
	}
	else if($_REQUEST['txtStatus'] == "1")
	{
		$msg = $obj->convertToPostOps($_REQUEST['txtMappingid2']);
	}
	else if($_REQUEST['txtStatus'] == "2")
	{
		//$msg = $obj->deletePdfDetails($_REQUEST['txtMappingid2'],$_REQUEST['txtFileName']);
	}
	
	header('Location : ./in_ops_at_glance.php?msg=1');
 }
 
if (@$_REQUEST['action1'] == 'submit1')
{
	if (@$_REQUEST['txtuid'] == 1)
	{
		$msg = $obj->insertPerfAtSeaUploadDetails();
		header('Location : ./in_ops_at_glance.php?msg='.$msg);
	}
	else if (@$_REQUEST['txtuid'] == 2)
	{
		$msg = $obj->insertPerfAtPortUploadDetails();
		header('Location : ./in_ops_at_glance.php?msg='.$msg);
	}
}
if(isset($_REQUEST['vesselid'])){
	
	$vesselid = $_REQUEST['vesselid'];
}
else
{
	$vesselid = "";
}
$pagename 	= basename($_SERVER['PHP_SELF']);

$uid	  	= $_SESSION['uid'];
$moduleid 	= $_SESSION['moduleid'];
$rights 	= $obj->getUserRights($uid,$moduleid,4);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> In Ops at a glance added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating In Ops at a glance.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
                <?php }?>
                <?php if($msg == 3){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-ban"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Sheet Added Successfully In Ops at a glance.
				</div>
				<?php }}?>
				<div class="box box-primary">
				<h3 style=" text-align:center;">In Ops at a glance List</h3>
				
				<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                    <div style="height:10px;">
                        <input type="hidden" name="action" value="submit" /><input type="hidden" name="txtMappingid2" id="txtMappingid2" value="" />
                    </div>
					<div class="box-body table-responsive" style="overflow:auto;">
                    <table id="in_ops_at_glance" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>CS View</th>
                                <th>Business Type / Nom ID</th>
                                <th>Material Name</th>
                                <th>Vessel</th>
                                <th>Fixture Type</th>
                                <th>BAF Calculator</th>
                                <th>Voyage Financials</th>
                                <th>PDA Request</th>
                                <th>Appoint Agent</th>	
                                <th>SOF</th>	
                                <th>Laytime</th>			
                                <th>Perf at Sea</th>
                                <th>Perf in Port</th>
                                <th>Payment Grid</th>
                                <th>Deactivate</th>
                                <th>Re&nbsp;-&nbsp;Del</th>
                                <th>Complete</th>
                            </tr>
                   		</thead>
                		<tbody>
						<?php $sql = "select * from mapping_master where MODULEID='".$_SESSION['moduleid']."'  AND MCOMPANYID='".$_SESSION['company']."' and STATUS=2 order by MAPPINGID";
						  $res = mysql_query($sql);
						  $rec = mysql_num_rows($res);
						  $i=1;
						  $submit = 0;
						  if($rec == 0)
						  {
							 
						  }
						  else{
						  while($rows = mysql_fetch_assoc($res))
						  {
			 			?>
                        <tr style="font-size:10px;" id="in_row_<?php echo $i;?>">
                            <td>
                            <a href="view_cost_sheet_list.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1">View</a>
                            <br/><br/>
                            <?php if($obj->getEditNominationValues($rows['MAPPINGID']) == 0){?>
                            <a href="edit_nominations1.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1&tab=1" style="color:red;" title="Click me" >Edit Nom</a>
                            <?php }else{?>
                            <a href="edit_nominations1.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1&tab=1" style="color:blue;" title="Click me" >Edit Nom</a>
                            <?php }?>
                            <br/><br/>
                            <?php if($obj->getAllStandardsValues($rows['MAPPINGID']) == 0){ $sstatus = 1;?>
                            <a href="check_standards1.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:blue;" title="Click me" >Check</a>
                            <?php }else{ $sstatus = 0;?>
                            <a href="check_standards1.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:red;" title="Click me" >Check</a>
                            <?php }?>
                            <br/><br/>
                            <a href="documents.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:green;" title="Click me" >Docs</a>
                            </td>
			
							<td><?php echo $obj->getBusinessTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'VESSEL_IMO_ID'),"BUSINESSTYPEID"))."<br><br>".$obj->getMappingData($rows['MAPPINGID'],"NOM_NAME");?></td>
			
							<td><?php echo $obj->getCargoContarctForMapping($obj->getMappingData($rows['MAPPINGID'],"CARGO_IDS"),$obj->getMappingData($rows['MAPPINGID'],"CARGO_POSITION"));?></td>
							<td><?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'VESSEL_IMO_ID'),"VESSEL_NAME");?></td>
							<?php if($obj->getFinalCostSheetDataRec($rows['MAPPINGID']) == 0){?>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <?php }else{?>
                            <td><?php echo $obj->getFreightFixtureBasedOnID($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID"));?>
                            <br/><br/>
                    		<?php if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 1){?>
                            <a href="#check_list_tci.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:red;" title="Click me" >Check List</a>	
                            <?php }else{?>
                                <a href="#check_list.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:red;" title="Click me" >Check List</a>	
                            <?php }?>
                            </td>
                            <td>
                            <?php if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 2){
                            if($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'COA_SPOT') == 2){?>
                            <a href="baf_calculation.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:green;" title="Click me" >BAF Calculator</a>
                            <?php }}?>
                            </td>
                            <td align="center">
                            <a href='view_cost_sheet1.php?mappingid=<?php echo $rows['MAPPINGID'];?>&cost_sheet_id=<?php echo $obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO");?>&page=1' >FVF</a><br/>
                            <?php 
                            $sql1 = "select * from cost_sheet_name_master where MAPPINGID='".$rows['MAPPINGID']."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";
                
                            $res1 = mysql_query($sql1);
                            $rec1 = mysql_num_rows($res1);
                            if($rec1 > 0)
                            {
                                while($rows1 = mysql_fetch_assoc($res1))
                                {
                                    if($rows1['PROCESS'] != "EST")
                                    {
                                        if($obj->getFreightEstimationStatus($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO")) == 2 )
                                        {
                                            $href = "./cost_sheet_tci.php?mappingid=".$rows['MAPPINGID']."&cost_sheet_id=".$rows1['COST_SHEETID']."&page=1";
                                        }
                                        else
                                        {
                                            $href = "./cost_sheet_vci.php?mappingid=".$rows['MAPPINGID']."&cost_sheet_id=".$rows1['COST_SHEETID']."&page=1";
                                        }
                                        echo "<a href='".$href."'>".str_replace(' ','&nbsp;',$rows1['SHEET_NAME'])."</a><br/>";
                                        $curr_cst = $rows1['COST_SHEETID'];
                                    }
                                    else
                                    {
                                        $curr_cst = $obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO");
                                    }
                                }
                            }
                            
                            if($obj->getCurrentCostSheetStatus($rows['MAPPINGID'],$curr_cst) == 2)
                            {
                            ?>
                            <button class="btn btn-primary btn-small" data-toggle="modal" data-target="#compose-modal1" type="button" title="Add New VF" onClick="openWin1(<?php echo $rows['MAPPINGID'];?>);">A</button>
                            <?php }else{?>
                            <button class="btn btn-primary btn-small" type="button" title="Add New VF" onClick="msg();">A</button>
                            <?php }?>
                            </td>
                            <td>
                            <a href="#agency_letter_generation.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1">Generate Agency Letter</a>
                            </td>
                            <td>
                            <a href="#nominate_agent.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1">Nominate Agent</a>
                            </td>
                            <td>
                            <a href="#sof.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1">SOF</a>		
                            </td>
			
                            <td>
                            <?php 
                            if($obj->getFreightEstimationStatus($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO")) == 2 )
                            {?>
                            <a href="#laytime_calculation.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:blue;" title="Click me" >Calculations</a>
                            <?php }?>
                            </td>
							<?php if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 1){?>
                            <td>
                            <a href="#" title="Upload Text File" onClick="openWin(<?php echo $rows['MAPPINGID'];?>,<?php echo $rows['OPEN_VESSEL_ID']?>,1);">Upload</a><br/><br/><br/>
                            <a href="#" title="Bunker Voyage Report" onClick="getPdfForSea(<?php echo $rows['MAPPINGID'];?>,<?php echo $rows['OPEN_VESSEL_ID'];?>)">Report</a><br />
                            </td>
			
                            <td>
                            <a href="#" title="Upload Text File" onClick="openWin(<?php echo $rows['MAPPINGID'];?>,<?php echo $rows['OPEN_VESSEL_ID'];?>,2);">Upload</a><br /><br /><br />
                            <a href="#" onClick="getPdfForPort(<?php echo $rows['MAPPINGID'];?>,<?php echo $rows['OPEN_VESSEL_ID'];?>)">Report</a><br />
                            </td>
							<?php }else{?>
                            <td></td>
                            <td></td>
                            <?php }?>
                            <td>
                            <a href="#payment_grid.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" >Payment</a><br /><br /><br />
                            <?php if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 2){?>
                            	<a href="#invoice.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" >Invoice</a>
                            <?php }else{?>
                           		<a href="#invoice_tc.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" >Invoice</a>
                            <?php }?>
                            </td>
                            <td>
                            <?php if(in_array("1",$obj->getInternalUserRights(1))){?>
                            <a href="#A" title="Deactivate entry" onClick="getValidate2(<?php echo $rows['MAPPINGID'];?>,<?php echo $i;?>);"><i class="fa fa-times " style="color:red;"></i></a>
                            <?php }?>
                            </td>
                            <td>
                            <?php 
                            if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 1)
                            {
                                $re_del_date = $obj->getReDelDate($rows['MAPPINGID']);
                                $alarm = (int)$obj->getNomDetailsData($rows['MAPPINGID'],"ALARM");
                                $cost_sheet_id = $obj->getLatestCostSheetID($rows['MAPPINGID']);
                                $fcaid = $obj->getCurrentCostSheetFCAID($rows['MAPPINGID'],$cost_sheet_id);
                                $ttl_days = (int)$obj->getFreightEstimationTotalRecords($fcaid,"TTL_DAYS");
                    
                                $redate = strtotime($re_del_date);
                                $days = $ttl_days - $alarm;
                                $date = strtotime("+".$days." day", $redate);
                                $date1 = strtotime("+".$ttl_days." day", $redate);
                                //echo date('Y-m-d', $date);
                                if($re_del_date != "" && date('Y-m-d', $date) <= date("Y-m-d",time()))
                                {
                                $title = "  Vessel Name&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;".$obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($rows['MAPPINGID'],"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME")."<br>  Re - Del&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;".date("d M Y",$date1);
                            ?>
                            <a href="#$i" title="<?php echo $title;?>" id="alarm_<?php echo $i;?>"><button class="ox-button2" id="inner-login-button"  type="button"><b><span id="d27e53" style="background-color:red; color:#fff;" >&nbsp;Alert&nbsp;</span></b></button></a>
                            <?php }}?>
                            <?php }?>
                            </td>
                            <td>
                            <?php if(in_array("1",$obj->getInternalUserRights(2))){?>
                            <button class="ox-button" id="inner-login-button"  type="button" title="Push to Post Ops" onClick="getValidate3(<?php echo $rows['MAPPINGID'];?>);"><b><span id="d27e53">Post Ops</span></b></button>
                            <?php }?>
                            </td>
					</tr>
					<?php $i++;}}?>
                </tbody>
            </table>
        </form>
        <div class="modal fade" id="compose-modal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="box box-primary">
                            <div class="box-body no-padding">
                                <form name="frm2" id="frm2" method="post" enctype="multipart/form-data" action="<?php echo $pagename;?>">
                                    <div class="row invoice-info">
                                        <div class="col-sm-12 invoice-col">
                                            <strong style="text-align:center;text-decoration:underline;display:block;">INSTRUCTIONS FOR FIRST VOYAGE FINANCIALS ONLY</strong>
                                            <p style="padding:2%;">After FVF for first new voyage financials : just open and click "Submit to Estimate", then reopen for editin</p>
                                        </div>
                                    </div>
                                    
                                    <div class="row invoice-info" style="padding:2%">
                                        <div class="col-sm-12 invoice-col">
                                            Voyage Financials Name
                                            <address>
                                                <input type="text" name="txtFile" id="txtFile" class="form-control" autocomplete="off" value="<?php echo $obj->getNomDetailsData($mappingid,"FREIGHT_RATE");?>" placeholder="Voyage Financials Name" />
                                                <input type="hidden" name="txtMappingid1" id="txtMappingid1" />
                                            </address>
                                        </div><!-- /.col -->
                                    </div>
                                    
                                    <?php if($rights == 1){ ?>
                                    <div class="row invoice-info">
                                        <div class="col-sm-12 invoice-col" style="text-align:center;">
                                             <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
                                             <input type="hidden" name="action" id="action" value="submit" />
                                        </div><!-- /.col -->
                                    </div>
                                    <?php } ?>
                                </form>
                            </div><!-- /.box-body -->
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        </div>
    <!--   content ends here..................-->
    </section><!-- /.content -->
</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>

<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	
$("#in_ops_at_glance").dataTable();
	
//for uploading........
 $('#photoimg').live('change', function(){ 
	$("#cpDiv_"+$("#txtVar").val()).append("<span id='labelDiv_"+$("#txtVar").val()+"_"+$("#txtID_"+$("#txtVar").val()).val()+"'>abc</span>");
	$("#labelDiv_"+$("#txtVar").val()+"_"+$("#txtID_"+$("#txtVar").val()).val()).html('<img src="../../images/loader.gif" alt="Uploading...." width="20" height="10"  />');
	$("#imageform").ajaxForm({
				target: '#labelDiv_'+$("#txtVar").val()+'_'+$("#txtID_"+$("#txtVar").val()).val()
	}).submit();
	var id = $("#txtID_"+$("#txtVar").val()).val();
	id = (id - 1) + 2;
	$("#txtID_"+$("#txtVar").val()).val(id);
	$.modal.close();
});
//....................
}); 

function openWin1(mappinid)
{
	$('#basic-modal-content').modal();
	$("#txtMappingid1").val(mappinid);
	$("#simplemodal-container").css({"height":"200px"});
}

function getValidate()
{
	if($("#txtFile").val() != "")
	{
		return true;
	}
	else
	{
		jAlert('Please fill the file name', 'Alert');
		return false;
	}
}

function msg()
{
	jAlert('Please make sure the last Voyage Financials is Submit to Close', 'Alert');
}

function openWin2(var1,mappinid)
{
	$('#basic-modal-content1').modal();
	$("#txtVar").val(var1);
	$("#txtMappingid").val(mappinid);
	$("#simplemodal-container").css({"width":"300px","height":"50px"});
}	

function openWin(mappingid,vesselid,id)
{
	$('#basic-modal').modal();
	$("#txtMapid").val(mappingid);
	$("#txtVesselid").val(vesselid);
	$("#txtuid").val(id);
	$("#simplemodal-container").css({"width":"300px","height":"50px"});
}

function getValid()
{
	if($('#txtfile').val() == '')
	{
		jAlert('Please choose the file', 'Alert');
		return false;
	}
	else
	{
		var name = $('#txtfile').val();
		name = name.split('.');
		if(name[1] == 'txt')
		{
			return true;
		}
		else
		{
			jAlert('Please choose only .txt file', 'Alert');
			$('#txtfile').val('');
			return false;
		}
	}
}

function getPdfForSea(mappingid,vesselid)
{
	location.href='allPdf.php?id=12&mappingid='+mappingid+'&vesselid='+vesselid;
}

function getValidate2(var1,rowid)
{
	$("#in_row_"+rowid+" td").css({"background-color":"red"});
	jConfirm('Are you sure to de-activate this Nom ID ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtMappingid2").val(var1);
			document.frm1.submit();
		}
		else{$("#in_row_"+rowid+" td").css({"background-color":""});return false;}
		});	
}

function getValidate3(var1)
{
	jConfirm('Are you sure to send this Nom ID to post ops ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtMappingid2").val(var1);
			$("#txtStatus").val(1);
			document.frm1.submit();
		}
		else{return false;}
		});	
}

function getValidate4(var1,file_name)
{
	jConfirm('Are you sure to remove this document permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtMappingid2").val(var1);
			$("#txtStatus").val(2);
			$("#txtFileName").val(file_name);
			document.frm1.submit();
		}
		else{return false;}
		});	
}
</script>
		
</body>
</html>