<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->InsertPortCostTypeMasterRecords();
	header('Location : ./port_cost_type_list.php?msg='.$msg);
 }

$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Port Cost Type</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="port_cost_type_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				     <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             ADD PORT COST TYPE
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                          Countries
                            <address>
                              <select data-placeholder="Choose a Country here..." class="chzn-select" style="width:800px;" multiple name="selCountry[]" id="selCountry">
									<?php	$obj->getCountryListForMultiple();?>
								</select>
								
                            </address>
                        </div><!-- /.col -->
                        <div class="col-xs-12">
                           
								<div class="box-body no-padding">
								<table class="table table-striped">
								  
									<tbody id="tbd">
                                      <tr>
										<td align="left" class="input-text" width="40%" >Port Cost Type</td><td width="1%" align="left" class="text" valign="top">:</td><td align="left" class="input-text" width="40%"><input type="text" name="txtPCType_1" id="txtPCType_1" class="form-control" autocomplete="off" placeholder="" value=""/></td><td align="left" class="input-text" width="19%"><button type="button" class="btn btn-primary btn-flat" onClick="addUI_Row();">Add</button>
                                    <input type="hidden" class="input" name="txtCID" id="txtCID" value="1" size="5" /></td>
                                        
                                      </tr>
									</tbody>
								</table>
								</div><!-- /.box-body -->
							                       
                        </div><!-- /.col -->
					</div>
                    
                    
                    
                    
                        
                   
                  
                    
				<div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
				</div>
         
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/chosen.css" rel="stylesheet" type="text/css" />
<script src="../../js/chosen.jquery.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#selCountry").chosen(); 
$(".areasize").autosize({append: "\n"});
$("#frm1").validate({
	rules: {
		txtPCType:"required"
		},
	messages: {	
		txtPCType:"*"
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

});


function getValidate()
{
	if($("#selCountry").val() == null)
		{
			jAlert('Please select the country first', 'Alert');
			return false;
		}
		else 
		{
			return true;
		}
}

function addUI_Row()
{
	var id = $("#txtCID").val();
	
		if($("#txtPCType_"+id).val() != "")
		{
			id  = (id - 1 )+ 2;
			
			$('<tr><td width="16%" align="left"  valign="top"></td><td width="1%" align="left" class="text" valign="top"></td><td align="left" valign="top"><input type="text" name="txtPCType_'+id+'" id="txtPCType_'+id+'" class="form-control" size="18" autocomplete="off" value=""/></td><td align="left" class="input-text" width="19%"></td></tr>').appendTo("#tbd");
		$("#txtCID").val(id);
		}
		else
		{
			jAlert('Please fill the above entries.', 'Alert');
		}
}

</script>
    </body>
</html>