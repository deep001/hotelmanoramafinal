<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$mappingid 		= $_REQUEST['mappingid'];
$cost_sheet_id 	= $_REQUEST['cost_sheet_id'];

if($obj->getFreightEstimationStatus1($mappingid) == 2 )
{
	header('Location : ./voyage_estimation1.php?mappingid='.$mappingid.'&cost_sheet_id='.$cost_sheet_id);
}
if($obj->getFreightEstimationRecProcessWise($mappingid,$cost_sheet_id) > 0 )
{
	if($obj->getFreightEstimationStatus($mappingid,$cost_sheet_id) == 1 )
	{
		header('Location : ./update_voyage_estimation.php?mappingid='.$mappingid.'&cost_sheet_id='.$cost_sheet_id);
	}
	else
	{
		header('Location : ./update_voyage_estimation1.php?mappingid='.$mappingid.'&cost_sheet_id='.$cost_sheet_id);
	}
}
if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertTCIDetails();
	header('Location : ./nomination_at_glance.php?msg='.$msg);
}
if($obj->getLastCostSheetID($mappingid) > 0)
{
	$obj->viewFreightEstimationRecords($mappingid,$obj->getLastCostSheetID($mappingid));
	$rdoMarket 	= $obj->getFun12();
	$rdoCap 	= $obj->getFun6();
	$rdoDWT 	= $obj->getFun21();
	$pagename 	= basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&selFType=".$obj->getFun2().'&cost_sheet_id='.$cost_sheet_id;
}
else
{
	$rdoMarket 	= $rdoMMarket = $rdoCap = $rdoDWT = 1;
	$pagename 	= basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid.'&cost_sheet_id='.$cost_sheet_id;
}



$b_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"B_FULL_SPEED");
if($b_full_speed == "" ){$bfs = 0;}else{$bfs = $b_full_speed;}
$b_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"B_ECO_SPEED1");
if($b_ech_speed1 == "" ){$bes1 = 0;}else{$bes1 = $b_ech_speed1;}
$b_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"B_ECO_SPEED2");
if($b_ech_speed2 == "" ){$bes2 = 0;}else{$bes2 = $b_ech_speed2;}
$fo_b_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_FO_FULL_SPEED");
if($fo_b_full_speed == ""){$fo_bfs = 0;}else{$fo_bfs = $fo_b_full_speed;}
$fo_b_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_FO_ECO_SPEED1");
if($fo_b_ech_speed1 == ""){$fo_bes1 = 0;}else{$fo_bes1 = $fo_b_ech_speed1;}
$fo_b_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_FO_ECO_SPEED2");
if($fo_b_ech_speed2 == ""){$fo_bes2 = 0;}else{$fo_bes2 = $fo_b_ech_speed2;}
$do_b_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_DO_FULL_SPEED");
if($do_b_full_speed == ""){$do_bfs = 0;}else{$do_bfs = $do_b_full_speed;}
$do_b_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_DO_ECO_SPEED1");
if($do_b_ech_speed1 == ""){$do_bes1 = 0;}else{$do_bes1 = $do_b_ech_speed1;}
$do_b_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_DO_ECO_SPEED2");
if($do_b_ech_speed2 == ""){$do_bes2 = 0;}else{$do_bes2 = $do_b_ech_speed2;}


$l_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"L_FULL_SPEED");
if($l_full_speed == "" ){$lfs = 0;}else{$lfs = $l_full_speed;}
$l_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"L_ECO_SPEED1");
if($l_ech_speed1 == "" ){$les1 = 0;}else{$les1 = $l_ech_speed1;}
$l_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"L_ECO_SPEED2");
if($l_ech_speed2 == "" ){$les2 = 0;}else{$les2 = $l_ech_speed2;}
$fo_l_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_FO_FULL_SPEED");
if($fo_l_full_speed == ""){$fo_lfs = 0;}else{$fo_lfs = $fo_l_full_speed;}
$fo_l_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_FO_ECO_SPEED1");
if($fo_l_ech_speed1 == ""){$fo_les1 = 0;}else{$fo_les1 = $fo_l_ech_speed1;}
$fo_l_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_FO_ECO_SPEED2");
if($fo_l_ech_speed2 == ""){$fo_les2 = 0;}else{$fo_les2 = $fo_l_ech_speed2;}
$do_l_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_DO_FULL_SPEED");

if($do_l_full_speed == ""){$do_lfs = 0;}else{$do_lfs = $do_l_full_speed;}
$do_l_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_DO_ECO_SPEED1");
if($do_l_ech_speed1 == ""){$do_les1 = 0;}else{$do_les1 = $do_l_ech_speed1;}
$do_l_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_DO_ECO_SPEED2");
if($do_l_ech_speed2 == ""){$do_les2 = 0;}else{$do_les2 = $do_l_ech_speed2;}

$fo_inport_idle = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PI_FO_FULL_SPEED");
if($fo_inport_idle == ""){$foidle = 0;}else{$foidle = $fo_inport_idle;}
$fo_inport_wrking = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PW_FO_FULL_SPEED");
if($fo_inport_wrking == ""){$fowrking = 0;}else{$fowrking = $fo_inport_wrking;}

$do_inport_idle = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PI_DO_FULL_SPEED");
if($do_inport_idle == ""){$doidle = 0;}else{$doidle = $do_inport_idle;}
$do_inport_wrking = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PW_DO_FULL_SPEED");
if($do_inport_wrking == ""){$dowrking = 0;}else{$dowrking = $do_inport_wrking;}

$submitid1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUBMITID");
if($submitid1 == ""){$submitid = 0;}else{$submitid = $submitid1;}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
h2 {
    color: #1b77a6;
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 22px;
    font-weight: normal;
    line-height: 1;
    margin-bottom: 5px;
}

</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Nominations at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="in_ops_at_glance.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
						
				<div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                         Cost Sheet : Estimate
                        </h2>                            
                    </div><!-- /.col -->
                </div>
                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                	<div class="col-sm-4 invoice-col" style="display:none;" >
						<address>
						   <select  name="selVendor" class="select form-control" id="selVendor" >
								<?php $obj->getVendorListNewUpdate("");	?>
							</select> 
						</address>
					</div><!-- /.col -->
   				<?php if($obj->getLastCostSheetID($mappingid) > 0){?>	
				<div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                        	Fixture Type
                            <address>
                            <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFreightFixtureBasedOnID($obj->getFun2());?></strong>
                            </address>
                        </div><!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Main Particulars
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
	
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Nom ID
                            <address>
                            <input type="text" name="txtNomID" id="txtNomID" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getMappingData($mappingid,"NOMINATION_ID");?>" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Vessel Name
                            <address>
                            <input type="text" name="txtVName" id="txtVName" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?>" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        Vessel Type
                            <address>
                            <input type="text" name="txtVType" id="txtVType" class="form-control" autocomplete="off" placeholder="Vessel Type" readonly value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_TYPE"));?>" />
                            </address>
                        </div><!-- /.col -->
                    </div>
	
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Date<span style="font-size:10px; font-style:italic;">  (for financial year)</span>
                            <address>
                            <input type="text" name="txtDate" id="txtDate" class="form-control" autocomplete="off" placeholder="Date (for financial year)" value="<?php echo date("d-m-Y",strtotime($obj->getFun3()));?>" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Voyage No.
                            <address>
                            <input type="text" name="txtVNo" id="txtVNo" class="form-control" autocomplete="off" placeholder="Voyage No." value="<?php echo $obj->getFun10();?>" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Cost Sheet Name
                            <address>
                            <input type="text" name="txtENo" id="txtENo" class="form-control" autocomplete="off" value="<?php echo $obj->getFun5();?>" placeholder="Cost Sheet Name" style="color:#FF0000" />
                            </address>
                        </div><!-- /.col -->
                    </div>
	
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                        	<input name="rdoDWT" class="checkbox" id="rdoDWT1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoDWT == 1) echo "checked"; ?> onClick="showDWTField();"  /><br>
                        	DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>
                            <address>
                            <input type="text" name="txtDWTS" id="txtDWTS"  class="form-control" autocomplete="off" placeholder="DWT" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUMMER_1");?>" disabled="disabled"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	<input name="rdoDWT" class="checkbox" id="rdoDWT2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoDWT == 2) echo "checked"; ?> onClick="showDWTField();" /><br>
                        	DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>
                            <address>
                            <input type="text" name="txtDWTT" id="txtDWTT" class="form-control" autocomplete="off" placeholder="DWT" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"TROPICAL_1");?>" disabled="disabled"/>
                            <input type="hidden" name="txtTCNo" id="txtTCNo" autocomplete="off" value="<?php echo $obj->getFun11();?>" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	&nbsp;<br>
                        	TC No.
                            <address>
                            <input type="text" name="txtTCNo" id="txtTCNo" class="form-control" placeholder="TC No" autocomplete="off" value="<?php echo $obj->getFun11();?>" />
                            </address>
                        </div><!-- /.col -->
                    </div>
	
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                        	<input name="rdoCap" class="checkbox" id="rdoCap1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoCap == 1) echo "checked"; ?> onClick="showCapField();"  /><br>
                       		Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
                            <address>
                            <input type="text" name="txtGCap" id="txtGCap" class="form-control" autocomplete="off" placeholder="Grain Cap" value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"GRAIN");?>" disabled="disabled"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	<input name="rdoCap" class="checkbox" id="rdoCap2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoCap == 2) echo "checked"; ?> onClick="showCapField();" /><br>
                        Bale Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
                            <address>
                            <input type="text" name="txtBCap" id="txtBCap" class="form-control" autocomplete="off" placeholder="Bale Cap" value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BALE");?>" disabled="disabled" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            &nbsp;<br>
                            SF <span style="font-size:10px; font-style:italic;">(CBM/MT)</span>
                            <address>
                            <input type="text" name="txtSF" id="txtSF" class="form-control" placeholder="SF" autocomplete="off" value="<?php echo $obj->getFun9();?>"  onkeyup="getTotalDWT(),getTotalDWT1()"/>
                            </address>
                        </div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                        &nbsp;
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        &nbsp;
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	Loadable <span style="font-size:10px; font-style:italic;">(MT)</span>
                            <address>
                            <input type="text" name="txtLoadable" id="txtLoadable" class="form-control" placeholder="Loadable" readonly autocomplete="off" value="<?php echo $obj->getFun24();?>"/>
                            </address>
                        </div><!-- /.col -->
                    </div>
	
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Market
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
	
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                        	<input name="rdoMarket" id="rdoMarket1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoMarket == 1) echo "checked"; ?>  onclick="showMarketField();"  /> <br>
                        	TC Cost Per Day  <span style="font-size:10px; font-style:italic;">(USD)</span>
                            <address>
                            <input type="text" name="txtTCPDRate" id="txtTCPDRate" class="form-control" placeholder="TC Cost Per Day" autocomplete="off" value="<?php echo $obj->getFun13();?>" onKeyUp="getTTLFreight();"   />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <input name="rdoMarket" class="checkbox" id="rdoMarket2" type="radio" style="cursor:pointer;" value="2" <?php if($rdoMarket == 2) echo "checked"; ?> onClick="showMarketField();" /> <br>
                            Lumpsum <span style="font-size:10px; font-style:italic;">(USD)</span>
                            <address>
                            <input type="text" name="txtLumpsum" id="txtLumpsum" class="form-control" placeholder="Lumpsum" autocomplete="off" disabled="disabled" value="<?php echo $obj->getFun14();?>" onKeyUp="getTTLFreight();"  />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	&nbsp;
                            <address>
                            &nbsp;
                            </address>
                        </div><!-- /.col -->
                    </div>
	
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Cargo
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
	
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                       		Cargo Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                            <address>
                            <input type="text" name="txtCQMT" id="txtCQMT" class="form-control" placeholder="Cargo Qty" autocomplete="off" value="<?php echo $obj->getFun15();?>" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Cargo Type
                            <address>
                            <select  name="selCType" class="form-control" id="selCType">
                            <?php 
                            $_REQUEST['selCType'] = $obj->getFun16();
                            $obj->getCargoTypeList();
                            ?>
                            </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    	Cargo Name
                        <address>
                        	<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCargoContarctForMapping($obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"CARGO_POSITION"));?></strong>
                        <select  name="selPort" class="form-control" id="selPort" style="display:none;" >
                        	<?php $obj->getPortList(); ?>
                        </select>
                        </address>
                    </div><!-- /.col -->
                    </div>
	
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                                Sea Passage
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
	
                    <div class="row invoice-info">
                    	<div class="col-sm-4 invoice-col">
                            From Port
                            <address>
                            <select  name="selFPort" class="form-control" id="selFPort" >
								<?php 
                                    $obj->getPortList();
                                ?>
                            </select>
                            </address>
                    	</div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            To Port
                            <address>
                            <select  name="selTPort" class="form-control" id="selTPort" onChange="getDistance();">
								<?php 
                                //$obj->getPortList();
                                ?>
                            </select>
                            </address>
                        </div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                        	Distance Type
                            <address>
                                <select  name="selDType" class="form-control" id="selDType" onChange="getDistance();">
                                    <?php 
                                    $obj->getPortDistanceType();
                                    ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	Speed Adj.
                            <address>
                            <input type="text" name="txtWeather" id="txtWeather" class="form-control" placeholder="Speed Adj" autocomplete="off" value="" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	Margin <span style="font-size:10px; font-style:italic;">(days)</span>
                            <address>
                                <input type="text" name="txtMargin" id="txtMargin" class="form-control" placeholder="Margin (days)" autocomplete="off" />
                                <input type="hidden" name="txtVoyageTime" id="txtVoyageTime" class="form-control" autocomplete="off" value="0.00" />
                                <input type="hidden" name="txtTTLVoyageDays" id="txtTTLVoyageDays" class="form-control" autocomplete="off" value="0.00" />
                                <input type="hidden" name="txtTTLFoConsp" id="txtTTLFoConsp" class="form-control" readonly value="0.00"  />
                                <input type="hidden" name="txtTTLDoConsp" id="txtTTLDoConsp" class="form-control" readonly value="0.00"  />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	Distance
                            <address>
                            	<input type="text" name="txtDistance" id="txtDistance" class="form-control" placeholder="Distance" value="" />
                            	<span id="loader1" style="display:none;" ><img src="../../img/ajax-loader.gif" /></span>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	Passage Type
                            <address>
                            <select  name="selPType" class="form-control" id="selPType">
                            <?php 
                            $obj->getPassageType();
                            ?>
                            </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	Select Speed
                            <address>
                                <select  name="selSSpeed" class="form-control" id="selSSpeed" >
                                <?php 
                                $obj->getSelectSpeedList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                        	&nbsp;
                            <address>
                            	<button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addPortRotationDetails()">ADD</button>
                            </address>
                    	</div><!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                    <?php 
                                        $sql = "select * from fca_vci_sea_passage where FCAID='".$obj->getFun1()."'";
                                        $res = mysql_query($sql);
                                        $rec = mysql_num_rows($res);
                                    ?>
                                    <thead>
                                        <tr>
                                        <th width="3%" align="center">#</th>
                                        <th width="7%" align="left">From Port</th>
                                        <th width="10%" align="left">To Name</th>
                                        <th width="7%" align="left">Passage Type</th>
                                        <th width="7%" align="left">Distance</span></th>
                                        <th width="7%" align="left">Speed Adj.</th>
                                        <th width="8%" align="left">Margin<span style="font-size:10px; font-style:italic;">(days)</span></th>
                                        <input type="hidden" name="p_rotationID" id="p_rotationID" class="form-control" value="<?php echo $rec;?>" />
                                        </tr>
                                    </thead>
                                    <tbody id="tblPortRotation">
                                    <?php if($rec == 0){?>
                                    <tr id="PRrow_Empty">
                                    	<td valign="top" align="center" colspan="7" style="color:red;">Sorry , currently zero(0) records added.</td>
                                    </tr>
                                    <?php }else{
                                    $i=1;
                                    while($rows = mysql_fetch_assoc($res))
                                    {
                                    ?>
                                    <tr id="pr_Row_<?php echo $i;?>">
                                    <td align="center">
                                    <?php if($i == $rec){?>
                                    <a href="#pr<?php echo $i;?>" onClick="removePortRotation(<?php echo $i;?>,<?php echo $rows['FROM_PORTID'];?>,<?php echo $rows['TO_PORTID'];?>);" id="spcancel_<?php echo $i;?>"><i class="fa fa-times" style="color:red;"></i></a>
                                    <?php }else{?>
                                    <a href="#pr<?php echo $i;?>" onClick="removePortRotation(<?php echo $i;?>,<?php echo $rows['FROM_PORTID'];?>,<?php echo $rows['TO_PORTID'];?>);" id="spcancel_<?php echo $i;?>" style="display:none;" ><i class="fa fa-times" style="color:red;"></i></a>
                                    <?php }?>
                                    </td>
                                    <td align="left"><?php echo $obj->getPortNameBasedOnID($rows['FROM_PORTID']);?><input type="hidden" name="txtFPort_<?php echo $i;?>" id="txtFPort_<?php echo $i;?>" value="<?php echo $rows['FROM_PORTID'];?>"/></td>
                                    <td align="left"><?php echo $obj->getPortNameBasedOnID($rows['TO_PORTID']);?><input type="hidden" name="txtTPort_<?php echo $i;?>" id="txtTPort_<?php echo $i;?>" value="<?php echo $rows['TO_PORTID'];?>"/></td>
                                    <td align="left"><?php echo $obj->getPassageTypeNameBasedOnID($rows['PASSAGETYPEID'])." (".$obj->getPassageSpeedBasedOnID($rows['SPEEDID'])." )";?><input type="hidden" name="txtPType_<?php echo $i;?>" id="txtPType_<?php echo $i;?>" value="<?php echo $rows['PASSAGETYPEID'];?>"/><input type="hidden" name="txtSSpeed_<?php echo $i;?>" id="txtSSpeed_<?php echo $i;?>" value="<?php echo $rows['SPEEDID'];?>"/></td>
                                    <td align="left"><?php echo $rows['DISTANCE']." (".$obj->getDistanceTypeBasedOnID($rows['DISTANCE_TYPEID'])." )";?><input type="hidden" name="txtDistance_<?php echo $i;?>" id="txtDistance_<?php echo $i;?>" value="<?php echo $rows['DISTANCE'];?>"/><input type="hidden" name="txtDType_<?php echo $i;?>" id="txtDType_<?php echo $i;?>" value="<?php echo $rows['DISTANCE_TYPEID'];?>"/></td>
                                    <td align="left"><?php echo $rows['WEATHER'];?><input type="hidden" name="txtWeather_<?php echo $i;?>" id="txtWeather_<?php echo $i;?>" value="<?php echo $rows['WEATHER'];?>"/></td>
                                    <td align="left"><?php echo $rows['MARGIN'];?><input type="hidden" name="txtMargin_<?php echo $i;?>" id="txtMargin_<?php echo $i;?>" value="<?php echo $rows['MARGIN'];?>"/></td>
                                    </tr>
                                    <?php $i++;} }?>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Load Port(s)
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                    	<div class="col-sm-4 invoice-col">
                            Load Port
                            <address>
                            <select  name="selLoadPort" class="form-control" id="selLoadPort"  >
                            <option value=''>--Select from list--</option>
                            </select>
                            </address>
                    	</div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Cargo
                            <address>
                            <select  name="selLPCName" class="form-control" id="selLPCName" onChange="getLOadPortQty();" >
                            <?php 
                            $obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));
                            ?>
                            </select>
                            </address>
                        </div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                            <address>
                            <input type="text" name="txtPCosts" id="txtPCosts" class="form-control" placeholder="Port Costs" autocomplete="off" value="" />
                            </address>
                        </div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Qty MT
                            <address>
                            <input type="text" name="txtQMT" id="txtQMT" class="form-control" placeholder="Qty MT" onKeyUp="getLoadPortCalculation();" autocomplete="off" value="" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                            <address>
                            <input type="text" name="txtRate" id="txtRate" class="form-control" placeholder="Rate" onkeyup="getLoadPortCalculation();" autocomplete="off" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Work Days <span style="font-size:10px; font-style:italic;">(Qty/Rate)</span>
                            <address>
                            <input type="text" name="txtWDays" id="txtWDays" class="form-control" placeholder="Work Days" autocomplete="off" value="" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Idle Days
                            <address>
                            <input type="text" name="txtIDays" id="txtIDays" class="form-control" placeholder="Idle Days" autocomplete="off" value="" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            &nbsp;
                            <address>
                            	&nbsp;
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	&nbsp;
                            <address>
                            <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addLoadPortDetails()">ADD</button>
                            </address>
                        </div><!-- /.col -->
                    </div>
                    <div class="row">
                    	<div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                    <?php 
                                    $sql1 = "select * from fca_tci_load_port where FCAID='".$obj->getFun1()."'";
                                    $res1 = mysql_query($sql1,$connect);
                                    $rec1 = mysql_num_rows($res1);
                                    ?>
                                    <thead>
                                        <tr>
                                            <th width="3%" align="center">#</th>
                                            <th width="7%" align="left">Load Port</th>
                                            <th width="10%" align="left">Cargo Name</th>
                                            <th width="7%" align="left">Qty MT</th>
                                            <th width="7%" align="left">Rate<span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                                            <th width="7%" align="left">Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                            <th width="8%" align="left">Idle Days</th>
                                            <th width="8%" align="left">Work Days</th>
                                              <input type="hidden" name="load_portID" id="load_portID" value="<?php echo $rec1;?>" />
                                        </tr>
                                    </thead>
                                    <tbody id="tblLoadPort">
										<?php if($rec1 == 0){?>
                                            <tr id="LProw_Empty">
                                                <td valign="top" align="center" colspan="10" style="color:red;">Sorry , currently zero(0) records added.</td>
                                            </tr>
                                        <?php }else{
                                        $i=1;
                                        while($rows1 = mysql_fetch_assoc($res1))
                                        {?>
                                        <tr id="lp_Row_<?php echo $i;?>">
                                            <td align="center"><a href="#lp<?php echo $i;?>" onClick="removeLoadPort(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                            <td align="left"><?php echo $obj->getPortNameBasedOnID($rows1['LOADPORTID']);?><input type="hidden" name="txtLoadPort_<?php echo $i;?>" id="txtLoadPort_<?php echo $i;?>" class="form-control" value="<?php echo $rows1['LOADPORTID'];?>"/></td>
                                            <td align="left"><?php echo $obj->getCargoContarctForMapping($rows1['PURCHASE_ALLOCATIONID'],$obj->getMappingData($mappingid,"CARGO_POSITION"));?><input type="hidden" name="txtLPCID_<?php echo $i;?>" id="txtLPCID_<?php echo $i;?>" class="form-control" value="<?php echo $rows1['PURCHASE_ALLOCATIONID'];?>"/></td>
                                            <td align="left"><?php echo $rows1['QTY_MT'];?><input type="hidden" name="txtLpQMT_<?php echo $i;?>" id="txtLpQMT_<?php echo $i;?>" class="form-control" value="<?php echo $rows1['QTY_MT'];?>"/></td>
                                            <td align="left"><?php echo $rows1['RATE'];?><input type="hidden" name="txtLPRate_<?php echo $i;?>" id="txtLPRate_<?php echo $i;?>" class="form-control" value="<?php echo $rows1['RATE'];?>"/></td>
                                            <td align="left"><?php echo $rows1['PORT_COST'];?><input type="hidden" name="txtPCosts_<?php echo $i;?>" id="txtPCosts_<?php echo $i;?>" class="form-control" value="<?php echo $rows1['PORT_COST'];?>"/></td>
                                            <td align="left"><?php echo $rows1['IDLE_DAYS'];?><input type="hidden" name="txtLPIDays_<?php echo $i;?>" id="txtLPIDays_<?php echo $i;?>" class="form-control" value="<?php echo $rows1['IDLE_DAYS'];?>"/></td>
                                            <td align="left"><?php echo $rows1['WORK_DAYS'];?><input type="hidden" name="txtLPWDays_<?php echo $i;?>" id="txtLPWDays_<?php echo $i;?>" class="form-control" value="<?php echo $rows1['WORK_DAYS'];?>"/></td>
                                        </tr>
                                        <?php $i++;} }?>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                    	</div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Discharge Port(s)
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                    	<div class="col-sm-4 invoice-col">
                            Discharge Port
                            <address>
                            <select  name="selDisPort" class="form-control" id="selDisPort">
                            <option value=''>--Select from list--</option>
                            </select>
                            </address>
                    	</div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Cargo
                            <address>
                            <select  name="selDPCName" class="form-control" id="selDPCName" onChange="getDisPortQty();" >
                            <?php 
                            $obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));
                            ?>
                            </select>
                            </address>
                        </div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                            <address>
                            <input type="text" name="txtDPCosts" id="txtDPCosts" class="form-control" placeholder="Port Costs" autocomplete="off" />
                            </address>
                    	</div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Qty MT
                            <address>
                            <input type="text" name="txtDQMT" id="txtDQMT" class="form-control" placeholder="Qty MT" onKeyUp="getDisPortCalculation();" autocomplete="off" value="" />
                            </address>
                        </div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                            <address>
                            <input type="text" name="txtDRate" id="txtDRate" class="form-control" placeholder="Rate "  onkeyup="getDisPortCalculation();" autocomplete="off" value="" />
                            </address>
                    	</div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Work Days <span style="font-size:10px; font-style:italic;">(Qty/Rate)</span>
                            <address>
                            <input type="text" name="txtDWDays" id="txtDWDays" class="form-control" placeholder="Work Days" autocomplete="off" value="" />
                            </address>
                    	</div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Idle Days
                            <address>
                            <input type="text" name="txtDIDays" id="txtDIDays" class="form-control"  placeholder="Idle Days" autocomplete="off" value="" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            &nbsp;
                            <address>
                            	&nbsp;
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            &nbsp;
                            <address>
                            <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addDisPortDetails()">ADD</button>
                            </address>
                        </div><!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                    <?php 
                                        $sql2 = "select * from fca_tci_disch_port where FCAID='".$obj->getFun1()."'";
                                        $res2 = mysql_query($sql2);
                                        $rec2 = mysql_num_rows($res2);
                                    ?>
                                    <thead>
                                        <tr>
                                            <th width="3%" align="center">#</th>
                                            <th width="7%" align="left">Discharge Port</th>
                                            <th width="10%" align="left">Cargo Name</th>
                                            <th width="7%" align="left">Qty MT</th>
                                            <th width="7%" align="left">Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                                            <th width="7%" align="left">Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                            <th width="7%" align="left">Idle Days</th>
                                            <th width="7%" align="left">Work Days</th>
                                              <input type="hidden" name="dis_portID" id="dis_portID" value="<?php echo $rec2;?>" />
                                        </tr>
                                    </thead>
                                    <tbody id="tblDisPort">
                                    <?php if($rec2 == 0){?>
                                    <tr id="DProw_Empty">
                                    	<td valign="top" align="center" colspan="10" style="color:red;">Sorry , currently zero(0) records added.</td>
                                    </tr>
                                    <?php }else{
                                    $i=1;
                                    while($rows2 = mysql_fetch_assoc($res2))
                                    {?>
                                        <tr id="dp_Row_<?php echo $i;?>">
                                            <td align="center"><a href="#dp<?php echo $i;?>" onClick="removeDisPort(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                            <td align="left"><?php echo $obj->getPortNameBasedOnID($rows2['DIS_PORT_ID']);?><input type="hidden" name="txtDisPort_<?php echo $i;?>" id="txtDisPort_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['DIS_PORT_ID'];?>"/></td>
                                            <td align="left"><?php echo $obj->getCargoContarctForMapping($rows2['PURCHASE_ALLOCID'],$obj->getMappingData($mappingid,"CARGO_POSITION"));?><input type="hidden" name="txtDPCID_<?php echo $i;?>" id="txtDPCID_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['PURCHASE_ALLOCID'];?>"/></td>
                                            <td align="left"><?php echo $rows2['QTY_MT'];?><input type="hidden" name="txtDpQMT_<?php echo $i;?>" id="txtDpQMT_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['QTY_MT'];?>"/></td>
                                            <td align="left"><?php echo $rows2['RATE'];?><input type="hidden" name="txtDPRate_<?php echo $i;?>" id="txtDPRate_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['RATE'];?>"/></td>
                                            <td align="left"><?php echo $rows2['PORT_COST'];?><input type="hidden" name="txtDPCosts_<?php echo $i;?>" id="txtDPCosts_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['PORT_COST'];?>"/></td>
                                            <td align="left"><?php echo $rows2['IDLE_DAYS'];?><input type="hidden" name="txtDPIDays_<?php echo $i;?>" id="txtDPIDays_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['IDLE_DAYS'];?>"/></td>
                                            <td align="left"><?php echo $rows2['WORK_DAYS'];?><input type="hidden" name="txtDPWDays_<?php echo $i;?>" id="txtDPWDays_<?php echo $i;?>" class="form-control" value="<?php echo $rows2['WORK_DAYS'];?>"/></td>
                                        </tr>
                                    <?php $i++;} }?>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Transit Port(s)
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Transit Port
                            <address>
                            <select  name="selTLoadPort" class="form-control" id="selTLoadPort">
                            	<option value=''>--Select from list--</option>
                            </select>
                        	</address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                            <address>
                            	<input type="text" name="txtTLPCosts" id="txtTLPCosts" class="form-control" placeholder="Port Costs" autocomplete="off" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	Idle Days
                            <address>
                            	<input type="text" name="txtTLIDays" id="txtTLIDays" class="form-control"  placeholder="Idle Days" autocomplete="off" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            &nbsp;
                            <address>
                            	&nbsp;
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            &nbsp;
                            <address>
                            	&nbsp;
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        	&nbsp;
                            <address>
                            <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addTransitPortDetails()">ADD</button>
                            </address>
                        </div><!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                    <?php 
                                        $sql3 = "select * from fca_tci_transit_port where FCAID='".$obj->getFun1()."'";
                                        $res3 = mysql_query($sql3);
                                        $rec3 = mysql_num_rows($res3);
                                    ?>
                                    <thead>
                                        <tr>
                                            <th width="3%" align="center">#</th>
                                            <th width="7%" align="left">Transit Port</th>
                                            <th width="10%" align="left">Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                            <th width="7%" align="left">Idle Days</th>
                                            <input type="hidden" name="transit_portID" id="transit_portID" value="<?php echo $rec3;?>" />
                                        </tr>
                                    </thead>
                                    <tbody id="tblTransitPort">
                                    <?php if($rec3 == 0){?>
                                    <tr id="TProw_Empty">
                                    	<td valign="top" align="center" colspan="4" style="color:red;">Sorry , currently zero(0) records added.</td>
                                    </tr>
                                    <?php }else{
                                    $i=1;
                                    while($rows3 = mysql_fetch_assoc($res3))
                                    {?>
                                        <tr id="tp_Row_<?php echo $i;?>">
                                            <td align="center"><a href="#dp<?php echo $i;?>" onClick="removeTransitPort(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                            <td align="left"><?php echo $obj->getPortNameBasedOnID($rows3['LOAD_PORTID']);?><input type="hidden" name="txtTLoadPort_<?php echo $i;?>" id="txtTLoadPort_<?php echo $i;?>" class="form-control" value="<?php echo $rows3['LOAD_PORTID'];?>"/></td>
                                            <td align="left"><?php echo $rows3['PORT_COST'];?><input type="hidden" name="txtTPCosts_<?php echo $i;?>" id="txtTPCosts_<?php echo $i;?>" class="form-control" value="<?php echo $rows3['PORT_COST'];?>"/></td>
                                            <td align="left"><?php echo $rows3['IDLE_DAYS'];?><input type="hidden" name="txtTPIDays_<?php echo $i;?>" id="txtTPIDays_<?php echo $i;?>" class="form-control" value="<?php echo $rows3['IDLE_DAYS'];?>"/></td>
                                        </tr>
                                    <?php $i++;} }?>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Totals
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                        	Laden Dist
                            <address>
                            <input type="text" name="txtLDist" id="txtLDist" class="form-control"readonly="true" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"LADEN_DIST");?>" />
                            </address>
                        </div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                        Ballast Dist
                            <address>
                            <input type="text" name="txtBDist" id="txtBDist" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BALLAST_DIST");?>" />
                            </address>
                    	</div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Total Dist
                            <address>
                            <input type="text" name="txtTDist" id="txtTDist" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TOTAL_DIST");?>" />
                            </address>
                    	</div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                    	<div class="col-sm-4 invoice-col">
                        Laden Days
                            <address>
                            <input type="text" name="txtLDays" id="txtLDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"LADEN_DAYS");?>" />
                            </address>
                    	</div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                        Ballast Days
                            <address>
                            <input type="text" name="txtBDays" id="txtBDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BALLAST_DAYS");?>" />
                            </address>
                    	</div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                        	Total Sea Days
                            <address>
                            <input type="text" name="txtTSDays" id="txtTSDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SEA_DAYS");?>" />
                            </address>
                        </div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                    	<div class="col-sm-4 invoice-col">
                            Ttl Port Idle Days
                            <address>
                            <input type="text" name="txtTtPIDays" id="txtTtPIDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_IDLE_DAYS");?>" />
                            </address>
                    	</div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Ttl Port Work Days
                            <address>
                            <input type="text" name="txtTtPWDays" id="txtTtPWDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_WORK_DAYS");?>" />
                            </address>
                    	</div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Total Days
                            <address>
                            <input type="text" name="txtTDays" id="txtTDays" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DAYS");?>" />
                            </address>
                    	</div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                    	<div class="col-sm-4 invoice-col">
                        	Ttl FO Consp MT
                            <address>
                            <input type="text" name="txtTFUMT" id="txtTFUMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_FO_CONSP_MT");?>" />
                            </address>
                        </div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Ttl DO Consp MT
                            <address>
                            <input type="text" name="txtTDUMT" id="txtTDUMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DO_CONSP_MT");?>" />
                            </address>
                    	</div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">	
                        <div class="col-sm-4 invoice-col">
                          Ttl FO Consp MT (Manual)
                            <address>
                                <input type="text" name="txtTFUMTManual" id="txtTFUMTManual" class="form-control" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_FO_CONSP_MT_MANUAL");?>" onKeyUp="getBunkerAdjCalculation();"/>
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-4 invoice-col">
                          Ttl DO Consp MT (Manual)
                            <address>
                                <input type="text" name="txtTDUMTManual" id="txtTDUMTManual" class="form-control" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DO_CONSP_MT_MANUAL");?>" onKeyUp="getBunkerAdjCalculation();"/>
                            </address>
                        </div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                    	<div class="col-sm-4 invoice-col">
                        	Ttl FO Consp Off Hire
                            <address>
                            <input type="text" name="txtTFOCOffHire" id="txtTFOCOffHire" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_FO_CONSP_OFFHIRE");?>"  />
                            </address>
                        </div><!-- /.col -->
                    	<div class="col-sm-4 invoice-col">
                            Ttl DO Consp Off Hire
                            <address>
                            <input type="text" name="txtTDOCOffHire" id="txtTDOCOffHire" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DO_CONSP_OFFHIRE");?>" />
                            </address>
                    	</div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            &nbsp;
                            <address>
                            &nbsp;
                            </address>
                        </div><!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Freight Adjustment
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="25%"></th>
                                    <th width="25%"></th>
                                    <th width="25%">Percent</th>
                                    <th width="25%">USD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Total Hire <span style="font-size:10px; font-style:italic;">(USD)</span></td>
                                    <td></td>
                                    <td></td>
                                    <td><input type="text"  name="txtFrAdjUsdTH" id="txtFrAdjUsdTH" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_HIRE_USD");?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Address Commission %</td>
                                    <td ></td>
                                    <td align="center"><input type="text"  name="txtFrAdjPerAC" id="txtFrAdjPerAC" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"ADD_COMM_PER");?>"  onkeyup="getFreightAdjpercentAC();" />
                                    </td>
                                    <td align="center"><input type="text"  name="txtFrAdjUsdAC" id="txtFrAdjUsdAC" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"ADD_COMM_USD");?>"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >Brokerage Commission %</td>
                                    <td></td>
                                    <td>
                                    <input type="text"  name="txtFrAdjPerAgC" id="txtFrAdjPerAgC" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM_PER");?>"  onkeyup="getFreightAdjpercentAgC();"   />
                                    </td>
                                    <td align="center"><input type="text"  name="txtFrAdjUsdAgC" id="txtFrAdjUsdAgC" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM_USD");?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Nett Hire <span style="font-size:10px; font-style:italic;">(USD)</span></td>
                                    <td></td>
                                    <td></td>
                                    <td align="center"><input type="text"  name="txtFrAdjUsdNH" id="txtFrAdjUsdNH" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"NETT_HIRE_USD");?>"  />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                                Bunker Adjustment&nbsp;&nbsp;
                                <img src="../../img/close.png" onClick="getShowHide();" id="BID" name="BID" style="cursor:pointer;" title="Close Panel" />
                                <input type="hidden" id="txtbid" value="0" />
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="box-body table-responsive" style="overflow:auto;" id="bunker_adj">
						<?php
						$sql = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
						$res = mysql_query($sql);
						$rec = mysql_num_rows($res);
						if($rec == 0)
						{
							echo '<div style="color:red;" align="center">First fill the Bunker Grade Master Data.</div>';
						}
                    	else
                    	{
                    	?>
                        <table id="GridView1" class="table table-striped">
                            <thead>
                                <tr valign="top">
                                        <td valign="top">Select Bunker grade</td>
                                        <?php 
                                        $m=0; 
                                        while($rows = mysql_fetch_assoc($res)){
                                            $ttl_bg[] = $rows['NAME'];
                                            $ttl_bg1[] = $rows['BUNKERGRADEID'];
                                        ?>
                                        <td align="center" colspan="3"><?php echo $rows['NAME'];?>
                                    </td>
                                    <?php  $m++;}?>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <?php for($i=0;$i<count($ttl_bg);$i++){?>
                                    <td align="center">MT</td>
                                    <td align="center">Price</td>
                                    <td align="center">Cost</td>
                                    <?php }?>
                                </tr>
                                <tr>
                                    <td align="left">&nbsp;</td>
                                    <?php 
									$j=$k=0; 
									for($i=0;$i<count($ttl_bg);$i++){
									?>
                                    <td align="left"></td>
                                    <td align="center">                                    
                                        <input type="hidden" name="txtBunkerRec" id="txtBunkerRec" value="<?php echo $rec;?>"  />
                                        <input type="hidden" name="txtBunkerGradName_<?php echo $j+1;?>" id="txtBunkerGradName_<?php echo $j+1;?>" value="<?php echo $ttl_bg[$i];?>"/>
                                        <input type="hidden" name="txtBunkerGradeID_<?php echo $j+1;?>" id="txtBunkerGradeID_<?php echo $j+1;?>" value="<?php echo $ttl_bg1[$i];?>"/>
                                         <input type="hidden" name="txtBHID1[]" id="txtBHID1_<?php echo $ttl_bg1[$i];?>" value="<?php echo $ttl_bg1[$i];?>"/>
                                         <input type="hidden" name="txtTTLBAA_<?php echo $ttl_bg1[$i];?>" id="txtTTLBAA_<?php echo $ttl_bg1[$i];?>" readonly value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"OWNER_ADJ_COST");?>" />
                                        <input type="hidden" name="txtTTLEst_<?php echo $ttl_bg1[$i];?>" id="txtTTLEst_<?php echo $ttl_bg1[$i];?>" readonly value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_COST");?>"  />
                                    </td>
                                    <td align="left"></td>
                                    <?php $j++; }?>
                                </tr>
                                <tr>
                                    <td align="left">Estimated</td>
                                    <?php $j=$k=0; for($i=0;$i<count($ttl_bg);$i++){
                                    if($obj->getBunkerRec($obj->getFun1(),$mappingid,$ttl_bg1[$i]) > 0)
                                    {
                                    ?>
                                    <td align="left">
                                    <input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_1" id="txt<?php echo $ttl_bg[$i];?>_1_1" class="form-control" style="width:150px;" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',1,this.value);" autocomplete="off" readonly value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_MT");?>"/>
                                    </td>
                                    <td align="left"><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_2" id="txt<?php echo $ttl_bg[$i];?>_1_2" class="form-control" style="width:150px;" autocomplete="off" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',1,this.value,<?php echo $ttl_bg1[$i];?>);" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_PRICE");?>"/></td>
                                    <td align="left"><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_3" id="txt<?php echo $ttl_bg[$i];?>_1_3" class="form-control" style="width:150px;" readonly  value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_COST");?>"/></td>
                                    <?php } else { ?>
                                    <td align="left"><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_1" id="txt<?php echo $ttl_bg[$i];?>_1_1" class="form-control" style="width:150px;" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',1,this.value);" autocomplete="off" disabled="disabled" readonly />
                                    </td>
                                    <td align="left"><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_2" id="txt<?php echo $ttl_bg[$i];?>_1_2" class="form-control" style="width:150px;" autocomplete="off" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',1,this.value,<?php echo $ttl_bg1[$i];?>);"/></td>
                                    <td align="left"><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_3" id="txt<?php echo $ttl_bg[$i];?>_1_3" class="form-control" style="width:150px;" disabled="disabled" readonly/></td>
                                    <?php } } ?>
                                </tr>
                            </thead>
                        </table>
                        <?php } ?>
                    </div>
                    <div class="row" style="height:10px;"></div>                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            	Owner Related Costs (Other)
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="20%">&nbsp;</th>
                                    <th width="20%">&nbsp;</th>
                                    <th width="20%">&nbsp;</th>
                                    <th width="20%">&nbsp;</th>
                                    <th width="20%">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyBrokerage">
                            <?php 
                            $sql_brok = "select * from fca_tci_brokage_commission where FCAID='".$obj->getFun1()."'";
                            
                            $res_brok = mysql_query($sql_brok);
                            $num_brok = mysql_num_rows($res_brok);
                            if($num_brok==0)
                            {$num_brok =1;
                            ?>
                                 <tr id="tbrRow_1">
                                    <td><a href="#tb1'" onClick="removeBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                    <td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
                                    
                                    <td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td>
                                    <td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="form-control" readonly value="0.00" /></td>
                                    <td><select  name="selBroVList_1" class="select form-control" id="selBroVList_1"></select>
                                        <script>$("#selBroVList_1").html($("#selVendor").html());$("#selBroVList_1").val('');</script>
                                    </td>
                                </tr>
                        <?php }
                             else
                              {$i=0;$num_brok = $num_brok;
                              while($rows_brok = mysql_fetch_assoc($res_brok))
                              {$i = $i + 1;?>
                              
                                  <tr id="tbrRow_<?php echo $i;?>">
                                    <td><a href="#tb1'" onClick="removeBrokerage(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                    <td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
                                    
                                    <td><input type="text" name="txtBrCommPercent_<?php echo $i;?>" id="txtBrCommPercent_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows_brok['BROKAGE_PERCENT'];?>" onKeyUp="getFinalCalculation();"  /></td>
                                    <td><input type="text" name="txtBrComm_<?php echo $i;?>" id="txtBrComm_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows_brok['BROKAGE_AMT'];?>" /></td>
                                    <td><select  name="selBroVList_<?php echo $i;?>" class="select form-control" id="selBroVList_<?php echo $i;?>"></select>
                                        <script>$("#selBroVList_<?php echo $i;?>").html($("#selVendor").html());$("#selBroVList_<?php echo $i;?>").val('<?php echo $rows_brok['VENDORID'];?>');</script>
                                    </td>
                                </tr>
                              <?php }} ?>
                                
                                </tbody>
                                <tbody>
                                <tr>
                                    <td><button type="button" class="btn btn-primary btn-flat" onClick="addBrokerageRow()">Add</button><input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="<?php echo $num_brok;?>"/></td>
                                    <td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
                                    
                                    <td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM_PERCENT");?>" readonly/></td>
                                    <td><input type="text" name="txtBrComm" id="txtBrComm" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM");?>" /></td>
                                    <td>
                                    </td>
                                </tr>
                                </tbody>
                                
                                <tbody>
                            <!--
                                <tr>
                                    <td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
                                    <td></td>
                                    <td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="form-control" autocomplete="off"value="" onKeyUp="getFinalCalculation();"  /></td>
                                    <td><input type="text" name="txtBrComm" id="txtBrComm" class="form-control" readonly value="" /></td>
                                    <td><select  name="selFGFVListBrok" class="select form-control" id="selFGFVListBrok" ></select></td>
                                    <script>$("#selFGFVListBrok").html($("#selVendor").html());
                                    $("#selFGFVListBrok").val("<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM_VENDORID");?>");</script>
                                </tr>-->
                                
                                <?php 
                                    $sql4 = "select * from fca_tci_owner_related_cost where FCAID='".$obj->getFun1()."'";
                                    $res4 = mysql_query($sql4);
                                    $rec4 = mysql_num_rows($res4);
                                    $i=1;
                                    while($rows4 = mysql_fetch_assoc($res4))
                                    {											
                                ?>
                                <tr>
                                    <td>
                                    <?php echo $obj->getOwnerRelatedCostNameBasedOnID($rows4['OWNER_RCOSTID']);?><input type="hidden" name="txtHidORCID_<?php echo $i;?>" id="txtHidORCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows4['OWNER_RCOSTID'];?>" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                    <?php if($rows4['AMOUNT'] > 0){?>
                                    
                                    <input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>" onKeyUp="getORCCalculate(1,<?php echo $i;?>);" class="form-control" autocomplete="off" value="<?php echo abs($rows4['AMOUNT']);?>"  />
                                    
                                    <?php }else if($rows4['AMOUNT'] < 0){?>
                                    
                                    <input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>"  onkeyup="getORCCalculate(2,<?php echo $i;?>);" class="form-control" autocomplete="off" value="<?php echo abs($rows4['AMOUNT']);?>" style="color:red;"  />
                                    
                                    <?php }else if($rows4['AMOUNT'] == 0){?>
                                    <?php if($obj->getOwnerRelatedCostData($rows4['OWNER_RCOSTID'],"RDO_STATUS") == 1){?>
                                        <input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>" onKeyUp="getORCCalculate(<?php echo $obj->getOwnerRelatedCostData($rows4['OWNER_RCOSTID'],"RDO_STATUS");?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="<?php echo abs($rows4['AMOUNT']);?>" />
                                        <?php }else if($obj->getOwnerRelatedCostData($rows4['OWNER_RCOSTID'],"RDO_STATUS") == 2){?>
                                        <input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>"  onkeyup="getORCCalculate(<?php echo $obj->getOwnerRelatedCostData($rows4['OWNER_RCOSTID'],"RDO_STATUS");?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="<?php echo abs($rows4['AMOUNT']);?>" style="color:red;"  />
                                    <?php }?>
                                    <?php }?>
                                    </td>
                                    <td>
                                        <select  name="selORCVList_<?php echo $i;?>" class="select form-control" id="selORCVList_<?php echo $i;?>" >
                                            <?php //$obj->getVendorListNewUpdate("");	?>
                                        </select>
                                        <script>$("#selORCVList_<?php echo $i;?>").html($("#selVendor").html());$("#selORCVList_<?php echo $i;?>").val('<?php echo $rows4['VENDORID'];?>')</script>
                                    </td><input type="hidden" name="txtHidORCAmt_<?php echo $i;?>" id="txtHidORCAmt_<?php echo $i;?>" autocomplete="off" value="<?php echo $rows4['AMOUNT'];?>" />
                                </tr>								
                                <?php $i++;}?>
                                <tr>
                                    <td>Total Shipowner Expenses<input type="hidden" name="txtORC_id" id="txtORC_id" value="<?php echo $rec4;?>" /></td>
                                    <td></td>
                                    <td></td>
                                    <td><input type="text"  name="txtTTLORCAmt" id="txtTTLORCAmt" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SHIPOWNER_EXP");?>" /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="font-weight:bold;">Value/MT</td>
                                    <td><input type="text" name="txtTTLORCCostMT" id="txtTTLORCCostMT" class="form-control"  readonly="true" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SHIPOWNER_EXP_MT");?>" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            All Other Shipping Costs
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="20%"></th>
                                    <th width="20%"></th>
                                    <th width="20%">Cost</th>
                                    <th width="20%">Vendor</th>
                                    <th width="20%">Cost/MT</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $sql2 = "select * from fca_tci_other_shipping_cost where FCAID='".$obj->getFun1()."'";
                            $res2 = mysql_query($sql2);
                            $rec2 = mysql_num_rows($res2);
                            $i=1;
                            while($rows2 = mysql_fetch_assoc($res2))
                            {
                            ?>
                                <tr>
                                    <td><?php echo $obj->getOtherShippingCostNameBasedOnID($rows2['OTHER_SCOSTID']);?><input type="hidden" name="txtHidOSCID_<?php echo $i;?>" id="txtHidOSCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows2['OTHER_SCOSTID'];?>" /></td>
                                    <td></td>
                                    <td><input type="text"  name="txtOSCAbs_<?php echo $i;?>" id="txtOSCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows2['ABSOLUTE'];?>" onKeyUp="getOSCostCalculate1(<?php echo $i;?>);" placeholder="0.00" />
                                    </td>
                                    <td><select  name="selAOSCVendorList_<?php echo $i; ?>" class="select form-control" id="selAOSCVendorList_<?php echo $i; ?>"></select></td>
									<script>$("#selAOSCVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selAOSCVendorList_<?php echo $i; ?>").val('<?php echo $rows2['VENDORID'];?>')</script>
                                    <td><input type="text" name="txtOSCCostMT_<?php echo $i;?>" id="txtOSCCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows2['COST_MT'];?>" placeholder="0.00" />
                                    </td>
                                </tr>
                            <?php $i++; } ?>
                            <input type="hidden" name="txtAOSC_id" id="txtAOSC_id" value="<?php echo $rec2;?>" />
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Port Costs
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="20%"></th>
                                    <th width="20%"></th>
                                    <th width="20%">Cost</th>
                                    <th width="20%">Vendor</th>
                                    <th width="20%">Cost/MT</th>
                                </tr>
                            </thead>
                            <tbody id="tbPortCosts" >
                            <?php 
                            $mysql = "select * from fca_tci_portcosts where FCAID='".$obj->getFun1()."' order by FCA_PORTCOSTID asc";
                            $myres = mysql_query($mysql);
                            $myrec = mysql_num_rows($myres);
                            if($myrec > 0)
                            {$i=$j=$k=1;
                                while($myrows = mysql_fetch_assoc($myres))
                                {
                                    if($myrows['PORT'] == "Load")
                                    {?>
                                        <tr id="oscLProw_<?php echo $i;?>">
                                            <td align="left">Load Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
                                            <td></td>
                                            <td><input type="text"  name="txtLPOSCCost_<?php echo $i?>" id="txtLPOSCCost_<?php echo $i;?>" class="form-control" readonly value="<?php echo $myrows['COST'];?>" /></td>
                                            <td>
                                        	<select  name="selPCLPVendorList_<?php echo $i;?>" class="select form-control" id="selPCLPVendorList_<?php echo $i;?>"></select>
                                    		<script>$("#selPCLPVendorList_<?php echo $i;?>").html($("#selVendor").html());$("#selPCLPVendorList_<?php echo $i;?>").val('<?php echo $myrows['VENDORID'];?>');</script>
                                		</td>
                                            <td><input type="text" name="txtLPOSCCostMT_<?php echo $i;?>" id="txtLPOSCCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $myrows['COST_MT'];?>" /></td>
                                        </tr>
                        	<?php $i++;
							}else if($myrows['PORT'] == "Discharge")
                                    {?>
                                        <tr id="oscDProw_<?php echo $j;?>">
                                            <td>Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
                                            <td></td>
                                            <td><input type="text"  name="txtDPOSCCost_<?php echo $j;?>" id="txtDPOSCCost_<?php echo $j;?>" class="form-control" readonly value="<?php echo $myrows['COST'];?>"  /></td><td>
                                        	<select  name="selPCDPVendorList_<?php echo $i;?>" class="select form-control" id="selPCDPVendorList_<?php echo $i;?>"></select>
                                    		<script>$("#selPCDPVendorList_<?php echo $i;?>").html($("#selVendor").html());$("#selPCDPVendorList_<?php echo $i;?>").val('<?php echo $myrows['VENDORID'];?>');</script>
                                		</td>
                                            <td><input type="text" name="txtDPOSCCostMT_<?php echo $j;?>" id="txtDPOSCCostMT_<?php echo $j;?>" class="form-control" readonly value="<?php echo $myrows['COST_MT'];?>"  /></td>
                                        </tr>
                                    <?php $j++;}else if($myrows['PORT'] == "Transit")
                                    {?>
                                        <tr id="oscTProw_<?php echo $k;?>">
                                            <td>Transit Port   <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
                                            <td></td>
                                            <td><input type="text"  name="txtTPOSCCost_<?php echo $k;?>" id="txtTPOSCCost_<?php echo $k;?>" class="form-control" readonly value="<?php echo $myrows['COST'];?>" /></td>
                                            <td>
                                        	<select  name="selPCTPVendorList_<?php echo $i;?>" class="select form-control" id="selPCTPVendorList_<?php echo $i;?>"></select>
                                    		<script>$("#selPCTPVendorList_<?php echo $i;?>").html($("#selVendor").html());$("#selPCTPVendorList_<?php echo $i;?>").val('<?php echo $myrows['VENDORID'];?>');</script>
                                		</td>
                                            <td><input type="text" name="txtTPOSCCostMT_<?php echo $k;?>" id="txtTPOSCCostMT_<?php echo $k;?>" class="form-control" readonly value="<?php echo $myrows['COST_MT'];?>" /></td>
                                        </tr>
                                    <?php $k++;}?>
                            <?php }}?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>Total Port Costs</td>
                                    <td></td>
                                    <td><input type="text"  name="txtTTLPortCosts" id="txtTTLPortCosts" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_COSTS");?>"  /><input type="hidden" name="txtTTLPortCost" id="txtTTLPortCost" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_COST");?>"  />
                                    </td>
                                    <td></td>
                                    <td><input type="text" name="txtTTLPortCostsMT" id="txtTTLPortCostsMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_COSTS_MT");?>" placeholder="0.00"  /><input type="hidden"  name="txtTTLPCostMT" id="txtTTLPCostMT" class="form-control" readonly value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_COST_MT");?>" />
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Demurrage Dispatch Shipper
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="20%"></th>
                                    <th width="20%"></th>
                                    <th width="20%">Cost</th>
                                    <th width="20%">Vendor</th>
                                    <th width="20%">Cost/MT</th>
                                </tr>
                            </thead>
                            <tbody id="tbDDShipper" >
                            <?php 
                            $mysql2 = "select * from fca_tci_dd_shipper where FCAID='".$obj->getFun1()."'";
                            $myres2 = mysql_query($mysql2);
                            $myrec2 = mysql_num_rows($myres2);
                            if($myrec2 > 0)
                            {$i=1;
                                while($myrows2 = mysql_fetch_assoc($myres2))
                                {?>
                                    <tr id="oscDDSLProw_<?php echo $i;?>">
                                        <td>Load Port <?php echo $obj->getPortNameBasedOnID($myrows2['LOADPORTID']);?></td>
                                        <td></td>
                                        <td><input type="text"  name="txtDDSLPOSCCost_<?php echo $i;?>" id="txtDDSLPOSCCost_<?php echo $i;?>" class="form-control" value="<?php echo $myrows2['COST'];?>" autocomplete="off" onKeyUp="getTotalLP(<?php echo $i;?>);" /></td>
                                        <td>
                                        	<select  name="selDDSLPVendorList_<?php echo $i;?>" class="select form-control" id="selDDSLPVendorList_<?php echo $i;?>"></select>
                                    		<script>$("#selDDSLPVendorList_<?php echo $i;?>").html($("#selVendor").html());$("#selDDSLPVendorList_<?php echo $i;?>").val('<?php echo $myrows2['VENDORID'];?>');</script>
                                		</td>
                                        <td><input type="text" name="txtDDLLPOSCCostMT_<?php echo $i;?>" id="txtDDLLPOSCCostMT_<?php echo $i;?>" class="form-control" readonly value="<?php echo $myrows2['COST_MT'];?>" /></td>
                                    </tr>	
                            <?php $i++;}}?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Demurrage Dispatch Receiver
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="20%"></th>
                                    <th width="20%"></th>
                                    <th width="20%">Cost</th>
                                    <th width="20%">Vendor</th>
                                    <th width="20%">Cost/MT</th>
                                </tr>
                            </thead>
                            <tbody id="tbDDReceiver">
                            <?php 
                            $mysql3 = "select * from fca_tci_dd_receiver where FCAID='".$obj->getFun1()."'";
                            $myres3 = mysql_query($mysql3);
                            $myrec3 = mysql_num_rows($myres3);
                            if($myrec3 > 0)
                            {$i=1;
                                while($myrows3 = mysql_fetch_assoc($myres3))
                                {?>
                                    <tr id="oscDDRDProw_<?php echo $i;?>">
                                    <td>Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows3['LOADPORTID']);?></td>
                                    <td></td>
                                    <td><input type="text"  name="txtDDRDPOSCCost_<?php echo $i;?>" id="txtDDRDPOSCCost_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $myrows3['COST'];?>" onKeyUp="getTotalDP(<?php echo $i;?>);" /></td>
                                    <td>
                                    	<select  name="selDDRLPVendorCList_<?php echo $i;?>" class="select form-control" id="selDDRLPVendorCList_<?php echo $i;?>"></select>
                                    		<script>$("#selDDRLPVendorCList_<?php echo $i;?>").html($("#selVendor").html());$("#selDDRLPVendorCList_<?php echo $i;?>").val('<?php echo $myrows3['VENDORID'];?>');</script>
                                		</td>
                                    <td><input type="text" name="txtDDRDPOSCCostMT_<?php echo $i;?>" id="txtDDRDPOSCCostMT_<?php echo $i;?>" class="form-control"  readonly="true" value="<?php echo $myrows3['COST_MT'];?>" /></td>
                                    </tr>
                            <?php $i++;}}?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            	All Other Miscellaneous Costs
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="20%"></th>
                                    <th width="20%"></th>
                                    <th width="20%">Cost</th>
                                    <th width="20%">Vendor</th>
                                    <th width="20%">Cost/MT</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $sql3 = "select * from fca_tci_other_misc_cost where FCAID='".$obj->getFun1()."'";
                            $res3 = mysql_query($sql3);
                            $rec3 = mysql_num_rows($res3);
                            $i=1;
                            while($rows3 = mysql_fetch_assoc($res3))
                            {
                            ?>
                                <tr>
                                    <td><?php echo $obj->getOtherMiscCostNameBasedOnID($rows3['OTHER_MCOSTID']);?><input type="hidden" name="txtHidOMCID_<?php echo $i;?>" id="txtHidOMCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows3['OTHER_MCOSTID'];?>" /></td>
                                    <td></td>
                                    <td><input type="text"  name="txtOMCAbs_<?php echo $i;?>" id="txtOMCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows3['AMOUNT'];?>" onKeyUp="getOMCCalculate(<?php echo $i;?>);" />
                                    </td>
                                    <td>
                                    	<select  name="selAOMCVendorList_<?php echo $i;?>" class="select form-control" id="selAOMCVendorList_<?php echo $i;?>"></select>
                                    		<script>$("#selAOMCVendorList_<?php echo $i;?>").html($("#selVendor").html());$("#selAOMCVendorList_<?php echo $i;?>").val('<?php echo $rows3['VENDORID'];?>');</script>
                                		</td>
                                    <td><input type="text"  name="txtOMCCostMT_<?php echo $i;?>" id="txtOMCCostMT_<?php echo $i;?>" class="form-control"  readonly="true" value="<?php echo $rows3['COST_MT'];?>" /></td>
                                </tr>
                            <?php $i++;}?>
                            <tr height="10">
                            	<td colspan="10" align="left">
                                    <input type="hidden" name="txtAOMC_id" id="txtAOMC_id" value="<?php echo $rec3;?>" />
                                    <input type="hidden"  name="txtTTLOtherCost" id="txtTTLOtherCost" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_OTHER_COST");?>"/>
                                    <input type="hidden"  name="txtTTLOtherCostMT" id="txtTTLOtherCostMT" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_OTHER_COST_MT");?>"/>
                                 </td>
                               </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Total Freight Costs
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="25%"></th>
                                    <th width="25%"></th>
                                    <th width="25%">Cost</th>
                                    <th width="25%">Cost/MT</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Total Freight Costs</td>
                                    <td></td>
                                    <td><input type="text"  name="txtTTLShippingCost" id="txtTTLShippingCost" class="form-control"  readonly="true" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SHIPPING_COSTS");?>" />
                                    </td>
                                    <td><input type="text"  name="txtTTLShippingCostMT" id="txtTTLShippingCostMT" class="form-control"  readonly="true" value="<?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SHIPPING_COSTS_MT");?>" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                        	CS Status
                            <address>
                                <select  name="selVType" class="form-control" id="selVType">
                                <?php 
                                $_REQUEST['selVType'] = $obj->getFun4();
                                $obj->getVoyageType();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                    </div>
                    <div class="box-footer" align="right">
                    	<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
                    	<input type="hidden" name="action" id="action" value="submit" />
                    </div>
			</div>
            <!-------------------------------------------------------ELSE PART------------------------------------------------------->
			<?php }else{?>
            <div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    Fixture Type
                        <address>
                        <select  name="selFType" class="form-control" id="selFType" onChange="getBackgroundData();">
                        <?php 
                        $obj->getFixtureType();
                        ?>
                        </select>
                        </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        Main Particulars
                        </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    Nom ID
                        <address>
                        <input type="text" name="txtNomID" id="txtNomID" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getMappingData($mappingid,"NOMINATION_ID");?>" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    Vessel Name
                        <address>
                        <input type="text" name="txtVName" id="txtVName" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?>" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    Vessel Type
                        <address>
                        <input type="text" name="txtVType" id="txtVType" class="form-control" autocomplete="off" placeholder="Vessel Type" readonly value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_TYPE"));?>" />
                        </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    Date<span style="font-size:10px; font-style:italic;">  (for financial year)</span>
                        <address>
                        <input type="text" name="txtDate" id="txtDate" class="form-control" autocomplete="off" placeholder="Date (for financial year)" value="<?php echo date("d-m-Y",time());?>" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    Voyage No.
                        <address>
                        <input type="text" name="txtVNo" id="txtVNo" class="form-control" autocomplete="off" placeholder="Voyage No." value="" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    Cost Sheet Name
                        <address>
                        <input type="text" name="txtENo" id="txtENo" class="form-control" autocomplete="off" value="<?php echo $obj->getCostSheetNameBasedOnID($cost_sheet_id);?>" placeholder="Cost Sheet Name" style="color:#FF0000" />
                        </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    <input name="rdoDWT" class="checkbox" id="rdoDWT1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoDWT == 1) echo "checked"; ?> onClick="showDWTField();"  /><br>
                    DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>
                        <address>
                        <input type="text" name="txtDWTS" id="txtDWTS"  class="form-control" autocomplete="off" placeholder="0.0" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUMMER_1");?>" disabled="disabled"/>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    <input name="rdoDWT" class="checkbox" id="rdoDWT2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoDWT == 2) echo "checked"; ?> onClick="showDWTField();" /><br>
                    DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>
                        <address>
                        <input type="text" name="txtDWTT" id="txtDWTT" class="form-control" autocomplete="off" placeholder="0.0" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"TROPICAL_1");?>" disabled="disabled"/>
                        <input type="hidden" name="txtTCNo" id="txtTCNo" autocomplete="off" value="<?php echo $obj->getFun11();?>" />
                        </address>
                    </div><!-- /.col -->
                <div class="col-sm-4 invoice-col">
                &nbsp;<br>
                TC No.
                    <address>
                        <input type="text" name="txtTCNo" id="txtTCNo" class="form-control" placeholder="0.0" autocomplete="off" value="" />
                    </address>
                </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    <input name="rdoCap" class="checkbox" id="rdoCap1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoCap == 1) echo "checked"; ?> onClick="showCapField();"  /><br>
                    Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
                        <address>
                        <input type="text" name="txtGCap" id="txtGCap" class="form-control" autocomplete="off" placeholder="0.0" value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"GRAIN");?>" disabled="disabled"/>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    <input name="rdoCap" class="checkbox" id="rdoCap2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoCap == 2) echo "checked"; ?> onClick="showCapField();" /><br>
                    Bale Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
                        <address>
                        <input type="text" name="txtBCap" id="txtBCap" class="form-control" autocomplete="off" placeholder="0.0" value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BALE");?>" disabled="disabled" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    &nbsp;<br>
                    SF <span style="font-size:10px; font-style:italic;">(CBM/MT)</span>
                    <address>
                    <input type="text" name="txtSF" id="txtSF" class="form-control" placeholder="0.0" autocomplete="off" value="<?php echo $obj->getFun9();?>"  onkeyup="getTotalDWT(),getTotalDWT1()"/>
                    </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    &nbsp;
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    &nbsp;
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    Loadable <span style="font-size:10px; font-style:italic;">(MT)</span>
                    <address>
                    <input type="text" name="txtLoadable" id="txtLoadable" class="form-control" placeholder="0.0" readonly autocomplete="off" value="<?php echo $obj->getFun24();?>"/>
                    </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                    <h2 class="page-header">
                    Market
                    </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    <input name="rdoMarket" id="rdoMarket1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoMMarket == 1) echo "checked"; ?>  onclick="showMarketField();"  /> <br>
                    TC Cost Per Day  <span style="font-size:10px; font-style:italic;">(USD)</span>
                        <address>
                        <input type="text" name="txtTCPDRate" id="txtTCPDRate" class="form-control" placeholder="0.00" autocomplete="off" value="" onKeyUp="getTTLFreight();"   />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    <input name="rdoMarket" class="checkbox" id="rdoMarket2" type="radio" style="cursor:pointer;" value="2" <?php if($rdoMMarket == 2) echo "checked"; ?> onClick="showMarketField();" /> <br>
                    Lumpsum <span style="font-size:10px; font-style:italic;">(USD)</span>
                        <address>
                        <input type="text" name="txtLumpsum" id="txtLumpsum" class="form-control" placeholder="0.0" autocomplete="off" disabled="disabled" value="" onKeyUp="getTTLFreight();"  />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    &nbsp;
                        <address>
                        &nbsp;
                        </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        Cargo
                        </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    Cargo Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                        <address>
                        <input type="text" name="txtCQMT" id="txtCQMT" class="form-control" placeholder="Cargo Qty" autocomplete="off" value="<?php echo $obj->getGreaterCargoQuantity($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));?>" />
                        </address>
                    </div><!-- /.col -->
                
                    <div class="col-sm-4 invoice-col">
                    Cargo Type
                        <address>
                        <select  name="selCType" class="form-control" id="selCType">
                        <?php 
                        $obj->getCargoTypeList();
                        ?>
                        </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    Cargo Name
                        <address>
                        <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCargoContarctForMapping($obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"CARGO_POSITION"));?></strong>
                        <select  name="selPort" class="form-control" id="selPort" style="display:none;" >
                        <?php 
                        $obj->getPortList();
                        ?>
                        </select>
                        </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        Sea Passage
                        </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        From Port
                        <address>
                        <select  name="selFPort" class="form-control" id="selFPort" >
                        <?php 
                        //$obj->getPortList();
                        ?>
                        </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        To Port
                        <address>
                        <select  name="selTPort" class="form-control" id="selTPort" onChange="getDistance();">
                        <?php 
                        //$obj->getPortList();
                        ?>
                        </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Distance Type
                        <address>
                        <select  name="selDType" class="form-control" id="selDType" onChange="getDistance();">
                        <?php 
                        $obj->getPortDistanceType();
                        ?>
                        </select>
                        </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    Speed Adj.
                        <address>
                        <input type="text" name="txtWeather" id="txtWeather" class="form-control" placeholder="Speed Adj" autocomplete="off" value="" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    Margin Distance <span style="font-size:10px; font-style:italic;">(%)</span>
                        <address>
                            <input type="text" name="txtMargin" id="txtMargin" class="form-control" placeholder="Margin (days)" autocomplete="off" value="" />
                            <input type="hidden" name="txtVoyageTime" id="txtVoyageTime" class="form-control" autocomplete="off" value="0.00" />
                            <input type="hidden" name="txtTTLVoyageDays" id="txtTTLVoyageDays" class="form-control" autocomplete="off" value="0.00" />
                            <input type="hidden" name="txtTTLFoConsp" id="txtTTLFoConsp" class="form-control" readonly value="0.00"  />
                            <input type="hidden" name="txtTTLDoConsp" id="txtTTLDoConsp" class="form-control" readonly value="0.00"  />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Distance
                        <address>
                        <input type="text" name="txtDistance" id="txtDistance" class="form-control" placeholder="Distance" value="" />
                        <span id="loader1" style="display:none;" ><img src="../../img/ajax-loader.gif" /></span>
                        </address>
                    </div><!-- /.col -->
                
                    <div class="col-sm-4 invoice-col">
                        Passage Type
                        <address>
                        <select  name="selPType" class="form-control" id="selPType">
                        <?php 
                        $obj->getPassageType();
                        ?>
                        </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Select Speed
                        <address>
                        <select  name="selSSpeed" class="form-control" id="selSSpeed" >
                        <?php 
                        $obj->getSelectSpeedList();
                        ?>
                        </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-3 invoice-col">
                        &nbsp;
                        <address>
                        <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addPortRotationDetails()">ADD</button>
                        </address>
                    </div><!-- /.col -->
                </div>
                
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                <thead>
                                <tr>
                                <th width="3%" align="center">#</th>
                                <th width="7%" align="left">From Port</th>
                                <th width="10%" align="left">To Name</th>
                                <th width="7%" align="left">Passage Type</th>
                                <th width="7%" align="left">Distance</span></th>
                                <th width="7%" align="left">Speed Adj.</th>
                                <th width="8%" align="left">Margin<span style="font-size:10px; font-style:italic;">(days)</span>
                                <input type="hidden" name="p_rotationID" id="p_rotationID" value="0" /></th>
                                </tr>
                                </thead>
                                <tbody id="tblPortRotation">
                                    <tr id="PRrow_Empty">
                                        <td valign="top" align="center" colspan="7" style="color:red;">Sorry , currently zero(0) records added.</td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        Load Port(s)
                        </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    Load Port
                        <address>
                            <select  name="selLoadPort" class="form-control" id="selLoadPort"  >
                            <option value=''>--Select from list--</option>
                            </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    Cargo
                        <address>
                            <select  name="selLPCName" class="form-control" id="selLPCName" onChange="getLOadPortQty();" >
                            <?php 
                            $obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));
                            ?>
                            </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                        <address>
                            <input type="text" name="txtPCosts" id="txtPCosts" class="form-control" placeholder="Port Costs" autocomplete="off"/>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Qty MT
                        <address>
                        <input type="text" name="txtQMT" id="txtQMT" class="form-control" placeholder="Qty MT" onKeyUp="getLoadPortCalculation();" autocomplete="off" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                     Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                        <address>
                            <input type="text" name="txtRate" id="txtRate" class="form-control" placeholder="Rate "  onkeyup="getLoadPortCalculation();" autocomplete="off" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Work Days <span style="font-size:10px; font-style:italic;">(Qty/Rate)</span>
                        <address>
                            <input type="text" name="txtWDays" id="txtWDays" class="form-control" placeholder="Work Days" autocomplete="off"/>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Idle Days
                        <address>
                        <input type="text" name="txtIDays" id="txtIDays" class="form-control" placeholder="Idle Days" autocomplete="off" value="" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        &nbsp;
                        <address>
                        &nbsp;
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        &nbsp;
                        <address>
                        <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addLoadPortDetails()">ADD</button>
                        </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="3%" align="center">#</th>
                                            <th width="7%" align="left">Load Port</th>
                                            <th width="10%" align="left">Cargo Name</th>
                                            <th width="7%" align="left">Qty MT</th>
                                            <th width="7%" align="left">Rate<span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                                            <th width="7%" align="left">Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                            <th width="8%" align="left">Idle Days</th>
                                            <th width="8%" align="left">Work Days</th>
                                             <input type="hidden" name="load_portID" id="load_portID" value="0" />
                                        </tr>
                                    </thead>
                                    <tbody id="tblLoadPort">
                                    <tr id="LProw_Empty"><td valign="top" align="center" colspan="10" style="color:red;">Sorry , currently zero(0) records added.</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        Discharge Port(s)
                        </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        Discharge Port
                        <address>
                        <select  name="selDisPort" class="form-control" id="selDisPort">
                        <option value=''>--Select from list--</option>
                        </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Cargo
                        <address>
                        <select  name="selDPCName" class="form-control" id="selDPCName" onChange="getDisPortQty();" >
                        <?php 
                        $obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));
                        ?>
                        </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                        <address>
                        <input type="text" name="txtDPCosts" id="txtDPCosts" class="form-control" placeholder="Port Costs" autocomplete="off"/>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Qty MT
                        <address>
                        <input type="text" name="txtDQMT" id="txtDQMT" class="form-control" placeholder="Qty MT" onKeyUp="getDisPortCalculation();" autocomplete="off" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                        <address>
                        <input type="text" name="txtDRate" id="txtDRate" class="form-control" placeholder="Rate "  onkeyup="getDisPortCalculation();" autocomplete="off" value="" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Work Days <span style="font-size:10px; font-style:italic;">(Qty/Rate)</span>
                        <address>
                        <input type="text" name="txtDWDays" id="txtDWDays" class="form-control" placeholder="Work Days" autocomplete="off" value="" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Idle Days
                        <address>
                        <input type="text" name="txtDIDays" id="txtDIDays" class="form-control"  placeholder="Idle Days" autocomplete="off" value="" />
                        </address>
                    </div><!-- /.col -->
                     <div class="col-sm-4 invoice-col">
                        &nbsp;
                        <address>
                        &nbsp;
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        &nbsp;
                        <address>
                        <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addDisPortDetails()">ADD</button>
                        </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="3%" align="center">#</th>
                                            <th width="7%" align="left">Discharge Port</th>
                                            <th width="10%" align="left">Cargo Name</th>
                                            <th width="7%" align="left">Qty MT</th>
                                            <th width="7%" align="left">Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                                            <th width="7%" align="left">Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                            <th width="7%" align="left">Idle Days</th>
                                            <th width="7%" align="left">Work Days</th>
                                              <input type="hidden" name="dis_portID" id="dis_portID" value="0" />
                                        </tr>
                                    </thead>
                                    <tbody id="tblDisPort">
                                        <tr id="DProw_Empty"><td valign="top" align="center" colspan="10" style="color:red;">Sorry , currently zero(0) records added.</td></tr>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        Transit Port(s)
                        </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    Transit Port
                        <address>
                        <select  name="selTLoadPort" class="form-control" id="selTLoadPort">
                        <option value=''>--Select from list--</option>
                        </select>
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                        <address>
                        <input type="text" name="txtTLPCosts" id="txtTLPCosts" class="form-control" placeholder="Port Costs" autocomplete="off" value="" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    Idle Days
                        <address>
                        <input type="text" name="txtTLIDays" id="txtTLIDays" class="form-control"  placeholder="Idle Days" autocomplete="off" value="" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        &nbsp;
                        <address>
                        &nbsp;
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        &nbsp;
                        <address>
                        &nbsp;
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        &nbsp;
                        <address>
                        <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addTransitPortDetails()">ADD</button>
                        </address>
                    </div><!-- /.col -->
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                        <th width="3%" align="center">#</th>
                                        <th width="7%" align="left">Transit Port</th>
                                        <th width="10%" align="left">Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
                                        <th width="7%" align="left">Idle Days</th>
                                        <input type="hidden" name="transit_portID" id="transit_portID" value="1" />
                                        </tr>
                                    </thead>
                                    <tbody id="tblTransitPort">
                                    	<tr id="TProw_Empty">
                                        	<td valign="top" align="center" colspan="10" style="color:red;">Sorry , currently zero(0) records added.</td>
                                       	</tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        	Totals
                        </h2>                            
                    </div><!-- /.col -->
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    	Laden Dist
                        <address>
                        <input type="text" name="txtLDist" id="txtLDist" class="form-control"readonly="true" value="0" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    	Ballast Dist
                        <address>
                        <input type="text" name="txtBDist" id="txtBDist" class="form-control" readonly value="0" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                    	Total Dist
                        <address>
                        <input type="text" name="txtTDist" id="txtTDist" class="form-control" readonly value="0" />
                        </address>
                    </div><!-- /.col -->
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    	Laden Days
                        <address>
                        <input type="text" name="txtLDays" id="txtLDays" class="form-control" readonly value="0" />
                        </address>
                    </div><!-- /.col -->
                	<div class="col-sm-4 invoice-col">
                        Ballast Days
                        <address>
                        <input type="text" name="txtBDays" id="txtBDays" class="form-control" readonly value="0" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Total Sea Days
                        <address>
                        <input type="text" name="txtTSDays" id="txtTSDays" class="form-control" readonly value="0" />
                        </address>
                    </div><!-- /.col -->
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        Ttl Port Idle Days
                        <address>
                        <input type="text" name="txtTtPIDays" id="txtTtPIDays" class="form-control" readonly value="0" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Ttl Port Work Days
                        <address>
                        <input type="text" name="txtTtPWDays" id="txtTtPWDays" class="form-control" readonly value="0" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Total Days
                        <address>
                        <input type="text" name="txtTDays" id="txtTDays" class="form-control" readonly value="0" />
                        </address>
                    </div><!-- /.col -->
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        Ttl FO Consp MT
                        <address>
                        <input type="text" name="txtTFUMT" id="txtTFUMT" class="form-control" readonly value="" />
                        </address>
                    </div><!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        Ttl DO Consp MT
                        <address>
                        <input type="text" name="txtTDUMT" id="txtTDUMT" class="form-control" readonly value="" />
                        </address>
                    </div><!-- /.col -->
                </div>
                <div class="row invoice-info">	
                    <div class="col-sm-4 invoice-col">
                      Ttl FO Consp MT (Manual)
                        <address>
                            <input type="text" name="txtTFUMTManual" id="txtTFUMTManual" class="form-control" onKeyUp="getBunkerAdjCalculation();" value="" />
                        </address>
                    </div><!-- /.col -->
                    
                    <div class="col-sm-4 invoice-col">
                      Ttl DO Consp MT (Manual)
                        <address>
                            <input type="text" name="txtTDUMTManual" id="txtTDUMTManual" class="form-control" onKeyUp="getBunkerAdjCalculation();" value="" />
                        </address>
                    </div><!-- /.col -->
                </div>
                <div class="row invoice-info">
                	<div class="col-sm-4 invoice-col">
                        Ttl FO Consp Off Hire
                        <address>
                        <input type="text" name="txtTFOCOffHire" id="txtTFOCOffHire" class="form-control" autocomplete="off" value="0.00"  />
                        </address>
                    </div><!-- /.col -->
                	<div class="col-sm-4 invoice-col">
                        Ttl DO Consp Off Hire
                        <address>
                        <input type="text" name="txtTDOCOffHire" id="txtTDOCOffHire" class="form-control" autocomplete="off" value="0.00" />
                        </address>
                    </div><!-- /.col -->
                	<div class="col-sm-4 invoice-col">
                        &nbsp;
                        <address>
                        &nbsp;
                        </address>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        	Freight Adjustment
                        </h2>                            
                    </div><!-- /.col -->
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width="25%"></th>
                                <th width="25%"></th>
                                <th width="25%">Percent</th>
                                <th width="25%">USD</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Total Hire <span style="font-size:10px; font-style:italic;">(USD)</span></td>
                                <td></td>
                                <td></td>
                                <td><input type="text"  name="txtFrAdjUsdTH" id="txtFrAdjUsdTH" class="form-control" readonly value="0.00" /></td>
                            </tr>
                            <tr>
                                <td align="left">Address Commission %</td>
                                <td ></td>
                                <td align="center"><input type="text"  name="txtFrAdjPerAC" id="txtFrAdjPerAC" class="form-control" autocomplete="off" value="0.00" onkeyup="getFreightAdjpercentAC();" /></td>
                                <td align="center"><input type="text"  name="txtFrAdjUsdAC" id="txtFrAdjUsdAC" class="form-control" readonly value="0.00"/></td>
                            </tr>
                            <tr>
                                <td align="left" >Brokerage Commission %</td>
                                <td></td>
                                <td>
                                <input type="text"  name="txtFrAdjPerAgC" id="txtFrAdjPerAgC" class="form-control" autocomplete="off" value="0.00" onkeyup="getFreightAdjpercentAgC();" /></td>
                                <td align="center"><input type="text"  name="txtFrAdjUsdAgC" id="txtFrAdjUsdAgC" class="form-control" autocomplete="off" readonly value="0.00" /></td>
                            </tr>
                            <tr>
                                <td align="left">Nett Hire <span style="font-size:10px; font-style:italic;">(USD)</span></td>
                                <td></td>
                                <td></td>
                                <td align="center"><input type="text"  name="txtFrAdjUsdNH" id="txtFrAdjUsdNH" class="form-control" readonly value="0.00"  /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>            
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                            Bunker Adjustment&nbsp;&nbsp;
                            <img src="../../img/close.png" onClick="getShowHide();" id="BID" name="BID" style="cursor:pointer;" title="Close Panel" />
                            <input type="hidden" id="txtbid" value="0" />
                        </h2>                            
                    </div><!-- /.col -->
                </div>                
                <div class="box-body no-padding table-responsive" style="overflow:auto;" id="bunker_adj">
					<?php
						$sql = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
						$res = mysql_query($sql);
						$rec = mysql_num_rows($res);
						if($rec == 0)
						{
							echo '<div style="color:red;" align="center">First fill the Bunker Grade Master Data.</div>';
						}
						else
						{
                    	?>
                        <table class="table table-striped">
                            <thead>
                                <tr valign="top">
                                <th valign="top">Select Bunker grade</th>
									<?php $m=0; while($rows = mysql_fetch_assoc($res)){
                                    $ttl_bg[] = $rows['NAME'];
                                    $ttl_bg1[] = $rows['BUNKERGRADEID'];
                                    ?>
                                    <th align="center" colspan="3"><?php echo $rows['NAME'];?>&nbsp;&nbsp;</th>
                                	<?php  $m++;}?>
                                </tr>
                                <tr class="GridviewScrollHeader">
                                    <td>&nbsp;</td>
                                    <?php for($i=0;$i<count($ttl_bg);$i++){?>
                                    <td align="center">MT</td>
                                    <td align="center">Price</td>
                                    <td align="center">Cost</td>
                                    <?php }?>
                                </tr>
                                <tr class="GridviewScrollItem">
                                    <td align="left" valign="top" >&nbsp;</td>
                                    <?php 
									$j=$k=0; 
									for($i=0;$i<count($ttl_bg);$i++){
									?>
                                    <td align="left" valign="top"></td>
                                    <td align="center" valign="top">
                                    	<input type="hidden" name="txtBunkerRec" id="txtBunkerRec" readonly value="<?php echo $rec;?>"  />
                                    	<input type="hidden" name="txtBunkerGradName_<?php echo $j+1;?>" id="txtBunkerGradName_<?php echo $j+1;?>" value="<?php echo $ttl_bg[$i];?>"/>
                                    	<input type="hidden" name="txtBunkerGradeID_<?php echo $j+1;?>" id="txtBunkerGradeID_<?php echo $j+1;?>" value="<?php echo $ttl_bg1[$i];?>"/>
										<input type="hidden" name="txtBHID1[]" id="txtBHID1_<?php echo $ttl_bg1[$i];?>" value="<?php echo $ttl_bg1[$i];?>"/>
										<input type="hidden" name="txtTTLBAA_<?php echo $ttl_bg1[$i];?>" id="txtTTLBAA_<?php echo $ttl_bg1[$i];?>"/>
										<input type="hidden" name="txtTTLEst_<?php echo $ttl_bg1[$i];?>" id="txtTTLEst_<?php echo $ttl_bg1[$i];?>" value="0"/>
                                    </td>
                                	<td align="left"></td>
                                <?php $j++; }?>
                                </tr>
                                <tr>
                                    <td align="left">Estimated</td>
                                    <?php $j=$k=0; for($i=0; $i<count($ttl_bg); $i++){?>
										<td align="left">
										<input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_1" id="txt<?php echo $ttl_bg[$i];?>_1_1" class="form-control" style="width:150px;" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',1,this.value);" autocomplete="off" disabled="disabled" readonly />
										</td>
										<td align="left"><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_2" id="txt<?php echo $ttl_bg[$i];?>_1_2" class="form-control" style="width:150px;" autocomplete="off" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',1,this.value,<?php echo $ttl_bg1[$i];?>);"/></td>
										<td align="left"><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_3" id="txt<?php echo $ttl_bg[$i];?>_1_3" class="form-control" style="width:150px;" disabled="disabled" readonly/></td>
                                    <?php }?>
                                </tr>
                            </thead>
                        </table>
                    <?php } ?>
                </div>                
        		<div class="row" style="height:10px;"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        	Owner Related Costs (Other)
                        </h2>                            
                    </div><!-- /.col -->
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width="20%">&nbsp;</th>
                                <th width="20%">&nbsp;</th>
                                <th width="20%">&nbsp;</th>
                                <th width="20%">&nbsp;</th>
                                <th width="20%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyBrokerage">
                             <tr id="tbrRow_1">
                                <td><a href="#tb1'" onClick="removeBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
                                <td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
                                
                                <td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td>
                                <td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="form-control" readonly value="0.00" /></td>
                                <td><select  name="selBroVendorList_1" class="select form-control" id="selBroVendorList_1"></select>
                                    <script>$("#selBroVendorList_1").html($("#selVendor").html());$("#selBroVendorList_1").val('');</script>
                                </td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <td><button type="button" class="btn btn-primary btn-flat" onClick="addBrokerageRow()">Add</button>
                                	<input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="1"/>
                                </td>
                                <td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
                                
                                <td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();" readonly/></td>
                                <td><input type="text" name="txtBrComm" id="txtBrComm" class="form-control" readonly value="0.00" /></td>
                                <td>
                                </td>
                            </tr>
                       </tbody>
                        <tbody>
                            <?php 
                            $sql1 = "select * from owner_related_cost_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
                            $res1 = mysql_query($sql1);
                            $rec1 = mysql_num_rows($res1);
                            $i=1;
                            while($rows1 = mysql_fetch_assoc($res1))
                            {
                            ?>
                            <tr>
                                <td><?php echo $rows1['NAME'];?><input type="hidden" name="txtHidORCID_<?php echo $i;?>" id="txtHidORCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows1['OWNER_RCOSTID'];?>" /></td>
                                <td></td>
                                <td></td>												
                                <td>
                                <?php if($rows1['RDO_STATUS'] == 1){?>
                                <input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>" onKeyUp="getORCCalculate(<?php echo $rows1['RDO_STATUS'];?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="" placeholder="0.00"  />
                                <?php }else if($rows1['RDO_STATUS'] == 2){?>
                                <input type="text" name="txtORCAmt_<?php echo $i;?>" id="txtORCAmt_<?php echo $i;?>"  onkeyup="getORCCalculate(<?php echo $rows1['RDO_STATUS'];?>,<?php echo $i;?>);" class="form-control" autocomplete="off" value="" placeholder="0.00" style="color:red;"  />
                                <?php }?>
                                </td>
                                <td>
                                <select  name="selORCVendorList_<?php echo $i;?>" class="select form-control" id="selORCVendorList_<?php echo $i;?>"></select>
                                <script>$("#selORCVendorList_<?php echo $i;?>").html($("#selVendor").html());$("#selORCVendorList_<?php echo $i;?>").val('');</script>
                                <input type="hidden" name="txtHidORCAmt_<?php echo $i;?>" id="txtHidORCAmt_<?php echo $i;?>" autocomplete="off" value="0.00" />	
                                </td>
                            </tr>								
                            <?php $i++;}?>
                                
                            <tr>
                                <td>Total Shipowner Expenses <input type="hidden" name="txtORC_id" id="txtORC_id" value="<?php echo $rec1;?>" /></td>
                                <td></td>
                                <td></td>
                                <td><input type="text"  name="txtTTLORCAmt" id="txtTTLORCAmt" class="form-control" readonly placeholder="0.00"/></td>
                                <td></td>												
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="font-weight:bold;">Value/MT</td>
                                <td><input type="text" name="txtTTLORCCostMT" id="txtTTLORCCostMT" class="form-control"  readonly="true" placeholder="0.00" /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        	All Other Shipping Costs
                        </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width="20%"></th>
                                <th width="20%"></th>
                                <th width="20%">Cost</th>
                                <th width="20%">Vendor</th>
                                <th width="20%">Cost/MT</th>
                            </tr>
                        </thead>
                    <tbody>
                    <?php 
                    $sql2 = "select * from other_shipping_cost_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
                    $res2 = mysql_query($sql2);
                    $rec2 = mysql_num_rows($res2);
                    $i=1;
                    while($rows2 = mysql_fetch_assoc($res2))
                    {
                    ?>
                    	<tr>
                            <td><?php echo $rows2['NAME'];?><input type="hidden" name="txtHidOSCID_<?php echo $i;?>" id="txtHidOSCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows2['OTHER_SCOSTID'];?>" /></td>
                            <td></td>
                            <td><input type="text"  name="txtOSCAbs_<?php echo $i;?>" id="txtOSCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" onKeyUp="getOSCostCalculate1(<?php echo $i;?>);" placeholder="0.00" />
                            </td>
                            <td><select  name="selAOSCVendorList_<?php echo $i; ?>" class="select form-control" id="selAOSCVendorList_<?php echo $i; ?>"></select></td>
							<script>$("#selAOSCVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selAOSCVendorList_<?php echo $i; ?>").val('')</script>
                            <td><input type="text" name="txtOSCCostMT_<?php echo $i;?>" id="txtOSCCostMT_<?php echo $i;?>" class="form-control" readonly placeholder="0.00" /></td>
                        </tr>
                        	<?php $i++; } ?>
                    		<input type="hidden" name="txtAOSC_id" id="txtAOSC_id" value="<?php echo $rec2;?>" />
                    </tbody>
                    </table>
                </div>
        
           		<div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        Port Costs
                        </h2>                            
                    </div><!-- /.col -->
            	</div>
        
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width="20%"></th>
                                <th width="20%"></th>
                                <th width="20%">Cost</th>
                                <th width="20%">Vendor</th>
                                <th width="20%">Cost/MT</th>
                            </tr>
                        </thead>
                        <tbody id="tbPortCosts">
                        
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total Port Costs</td>
                                <td></td>
                                <td>
                                    <input type="text"  name="txtTTLPortCosts" id="txtTTLPortCosts" class="form-control" readonly placeholder="0.00"/>
                                    <input type="hidden" name="txtTTLPortCost" id="txtTTLPortCost" class="form-control" readonly />
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" name="txtTTLPortCostsMT" id="txtTTLPortCostsMT" class="form-control" readonly placeholder="0.00"/>
                                    <input type="hidden"  name="txtTTLPCostMT" id="txtTTLPCostMT" class="form-control" readonly value="" />
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        Demurrage Dispatch Shipper
                        </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width="20%"></th>
                                <th width="20%"></th>
                                <th width="20%">Cost</th>
                                <th width="20%">Vendor</th>
                                <th width="20%">Cost/MT</th>
                            </tr>
                        </thead>
                        <tbody id="tbDDShipper">
                        
                        </tbody>
                    </table>
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        Demurrage Dispatch Receiver
                        </h2>                            
                    </div><!-- /.col -->
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th width="20%"></th>
                            <th width="20%"></th>
                            <th width="20%">Cost</th>
                            <th width="20%">Vendor</th>
                            <th width="20%">Cost/MT</th>
                         </tr>
                         </thead>
                        <tbody id="tbDDReceiver">
                        
                        </tbody>
                    </table>
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        	All Other Miscellaneous Costs
                        </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="box-body no-padding">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th width="20%"></th>
                            <th width="20%"></th>
                            <th width="20%">Cost</th>
                            <th width="20%">Vendor</th>
                            <th width="20%">Cost/MT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $sql3 = "select * from other_misc_cost_master where MODULEID='".$_SESSION['moduleid']."' AND STATUS=1";
                        $res3 = mysql_query($sql3);
                        $rec3 = mysql_num_rows($res3);
                        $i=1;
                        while($rows3 = mysql_fetch_assoc($res3))
                        {
                        ?>
                        <tr>
                            <td><?php echo $rows3['NAME'];?><input type="hidden" name="txtHidOMCID_<?php echo $i;?>" id="txtHidOMCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows3['OTHER_MCOSTID'];?>" /></td>
                            <td></td>
                            <td><input type="text"  name="txtOMCAbs_<?php echo $i;?>" id="txtOMCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="" onKeyUp="getOMCCalculate(<?php echo $i;?>);" placeholder="0.00" />
                            </td>
                            <td><select  name="selAOMCVendorList_<?php echo $i; ?>" class="select form-control" id="selAOMCVendorList_<?php echo $i; ?>"></select></td>
							<script>$("#selAOMCVendorList_<?php echo $i; ?>").html($("#selVendor").html());$("#selAOMCVendorList_<?php echo $i; ?>").val('')</script>
                            <td><input type="text"  name="txtOMCCostMT_<?php echo $i;?>" id="txtOMCCostMT_<?php echo $i;?>" class="form-control"  readonly="true" value="" placeholder="0.00" /></td>
                        </tr>
                        <?php $i++;}?>
                    </tbody>
                    <tfoot>
                        <tr height="10">
                            <td colspan="10" align="left">
                            <input type="hidden" name="txtAOMC_id" id="txtAOMC_id" value="<?php echo $rec3;?>" />
                            <input type="hidden"  name="txtTTLOtherCost" id="txtTTLOtherCost" class="form-control"  readonly="true" placeholder="0.00" />
                            <input type="hidden"  name="txtTTLOtherCostMT" id="txtTTLOtherCostMT" class="form-control"  readonly="true" placeholder="0.00" />
                            </td>
                         </tr>
                    </tfoot>
                </table>
                </div>
        
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                        Total Freight Costs
                        </h2>                            
                    </div><!-- /.col -->
                </div>
        
                <div class="box-body no-padding">
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th width="25%"></th>
                        <th width="25%"></th>
                        <th width="25%">Cost</th>
                        <th width="25%">Cost/MT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Total Freight Costs</td>
                            <td></td>
                            <td><input type="text"  name="txtTTLShippingCost" id="txtTTLShippingCost" class="form-control"  readonly="true" placeholder="0.00" />
                            </td>
                            <td><input type="text"  name="txtTTLShippingCostMT" id="txtTTLShippingCostMT" class="form-control"  readonly="true" placeholder="0.00" /></td>
                        </tr>
                    </tbody>
                </table>
                </div>
        
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                    CS Status
                        <address>
                            <select  name="selVType" class="form-control" id="selVType">
                            <?php 
                            $obj->getVoyageType();
                            ?>
                            </select>
                        </address>
                    </div><!-- /.col -->
                </div>
                            
                <div class="box-footer" align="right">
                    <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
                    <input type="hidden" name="action" id="action" value="submit" />
                </div>
            </div>
			<?php } ?>
    	</form>
<!--   content ends here..................-->
</section><!-- /.content -->
</aside><!-- /.right-side -->

</div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
if(<?php echo $bfs;?> == 0 || <?php echo $bes1;?> == 0 || <?php echo $bes2;?> == 0 || <?php echo $lfs;?> == 0 || <?php echo $les1;?> == 0 || <?php echo $les2;?> == 0 || <?php echo $submitid;?> != 2)
{
	jAlert('Please ensure commercial parameters are completed before proceeding', 'Alert', function(r) {
	if(r){ 
		location.href = "edit_nominations.php?mappingid="+<?php echo $mappingid;?>+"&tab=3";
	}
	else{return false;}
	});		
}

	$("#selFPort,#selTPort").html($("#selPort").html());
	$("#TCI_18,#TCI_19").show();

$("#txtGCap,#txtBCap,#txtSF,#txtTCPDRate,#txtLumpsum,#txtCQMT,#txtWeather,#txtMargin,#txtLPDraftM,#txtPCosts,#txtQMT,#txtRate,#txtIDays,#txtWDays,#txtDPDraftM,#txtDPCosts,#txtDQMT,#txtDRate,#txtDIDays,#txtDWDays,#txtTPDraftM,#txtTLPCosts,#txtTLQMT,#txtTLRate,#txtTLIDays,#txtTLWDays,#txtLDist,#txtBDist,#txtTDist,#txtLDays,#txtBDays,#txtTSDays,#txtTtPIDays,#txtTtPWDays,#txtTDays,#txtTFUMT,#txtTDUMT,#txtTFOCOffHire,#txtTDOCOffHire,#txtFrAdjUsdTH,#txtFrAdjPerAC,#txtFrAdjUsdAC,#txtFrAdjPerAgC,#txtFrAdjUsdAgC,#txtFrAdjUsdNH,[id^=txtSaleAmt_],[id^=txtLINo_],#txtBunkerFONett,#txtBunkerDONett,[id^=txtORCAmt_],#txtTTLORCAmt,#txtTTLORCCostMT,[id^=txtOSCAbs_],[id^=txtOMCAbs_],#txtLoadable,[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_],#txtTFUMTManual,#txtTDUMTManual").numeric();

	$("#txtDate").datepicker({
		 format: 'dd-mm-yyyy',
		 autoclose:true
	});

	/*$('#panel1').slidePanel({
		triggerName: '#trigger1',
		position: 'fixed',
		triggerTopPos: '500px',
		panelTopPos: '500px'
	});*/

	$('[id^=txtFPort_]').each(function(index) {
		var rowid = this.id;
		var lasrvar1 = rowid.split('_')[1];
		var text = $("#selFPort [value='"+$("#txtFPort_"+lasrvar1).val()+"']").text();
		//for load port.....................
		$('<option value="'+$("#txtFPort_"+lasrvar1).val()+'">'+text+'</option>').appendTo("#selLoadPort");
		//.........ends...................
		//for transit port.....................
		$('<option value="'+$("#txtFPort_"+lasrvar1).val()+'">'+text+'</option>').appendTo("#selTLoadPort");
		//.........ends...................
	});
	$('[id^=txtTPort_]').each(function(index) {
		var rowid = this.id;
		var lasrvar1 = rowid.split('_')[1];
		var text = $("#selTPort [value='"+$("#txtTPort_"+lasrvar1).val()+"']").text();
		//for discharge port.....................
		$('<option value="'+$("#txtTPort_"+lasrvar1).val()+'">'+text+'</option>').appendTo("#selDisPort");
		//.........ends...................
	});
	
	getBunkerAdjCalculation();
	showDWTField();
	getOMCCalculate();
	getCalculate();
	getORCCalculate();
	getFinalCalculation();
	showCapField();
	//showMarketField();
	//getDistance();
});


function getBackgroundData()
{
	if($("#selFType").val() == 1)
	{
		location.href = "voyage_estimation.php?mappingid=<?php echo $mappingid;?>&cost_sheet_id=<?php echo $cost_sheet_id;?>";
	}
	if($("#selFType").val() == 2)
	{
		location.href = "voyage_estimation1.php?mappingid=<?php echo $mappingid;?>&cost_sheet_id=<?php echo $cost_sheet_id;?>";
	}
}
//showCapField

function showCapField()
{
	if($("#rdoCap1").is(":checked"))
	{
		$("#txtGCap").removeAttr('disabled');
		$("#txtBCap").attr({'disabled':'disabled'});
		getTotalDWT();
	}
	if($("#rdoCap2").is(":checked"))
	{
		$("#txtGCap").attr({'disabled':'disabled'});
		$("#txtBCap").removeAttr('disabled');
		getTotalDWT1();
	}
}

function getTotalDWT()
{
	if($("#rdoDWT1").is(":checked")){var dwt = $("#txtDWTS").val();}
	if($("#rdoDWT2").is(":checked")){var dwt = $("#txtDWTT").val();}
	
	if($("#rdoCap1").is(":checked"))
	{
		if($("#txtGCap").val() == ""){var gcap = 0;}else{var gcap = $("#txtGCap").val();}
		if($("#txtSF").val() != "" || $("#txtSF").val() != 0)
		{
			var ttl = parseFloat(gcap) / parseFloat($("#txtSF").val());
			if(ttl > parseFloat(dwt))
			{
				//jAlert('Loadable Qty MT > DWT. Please recheck CBM and SF', 'Alert');
				$("#txtLoadable").val(dwt);
			}
			else
			{
				if(isNaN(ttl)){
					$("#txtLoadable").val('0.00');
				}else{
					$("#txtLoadable").val(ttl.toFixed(2));
				}
			}
		}
	}
}

function getTotalDWT1()
{
	if($("#rdoDWT1").is(":checked")){var dwt = $("#txtDWTS").val();}
	
	if($("#rdoDWT2").is(":checked")){var dwt = $("#txtDWTT").val();}
	
	if($("#rdoCap2").is(":checked"))
	{
		if($("#txtBCap").val() == ""){var bcap = 0;}else{var bcap = $("#txtBCap").val();}
		
		if($("#txtSF").val() != "" || $("#txtSF").val() != 0)
		{
			var ttl = parseFloat(bcap) / parseFloat($("#txtSF").val());
			if(ttl > parseFloat(dwt))
			{
				//jAlert('Loadable Qty MT > DWT. Please recheck CBM and SF', 'Alert');
				$("#txtLoadable").val(dwt);
			}
			else
			{
				if(isNaN(ttl)){
					$("#txtLoadable").val('0.00');
				}else{
					$("#txtLoadable").val(ttl.toFixed(2));
				}
			}
		}
	}
}

function showDWTField()
{ 
	if($("#rdoDWT1").is(":checked"))
	{
		$("#txtDWTS").removeAttr('disabled');
		$("#txtDWTT").attr({'disabled':'disabled'});
		getTotalDWT();
		getTotalDWT1();
	}
	if($("#rdoDWT2").is(":checked"))
	{
		$("#txtDWTS").attr({'disabled':'disabled'});
		$("#txtDWTT").removeAttr('disabled');
		getTotalDWT();
		getTotalDWT1();
	}
}

function showMarketField()
{
	if($("#rdoMarket1").is(":checked"))
	{
		$("#txtTCPDRate").removeAttr('disabled');
		$("#txtLumpsum").attr({'disabled':'disabled'});
		$("#txtTCPDRate,#txtLumpsum").val("");
		$("#txtFrAdjUsdTH").val("0.00");
		$("#txtTCPDRate").focus();
	}
	if($("#rdoMarket2").is(":checked"))
	{
		$("#txtTCPDRate").attr({'disabled':'disabled'});
		$("#txtLumpsum").removeAttr('disabled');
		$("#txtTCPDRate,#txtLumpsum").val("");
		$("#txtFrAdjUsdTH").val("0.00");
		$("#txtLumpsum").focus();
	}
	getFreightAdjpercentAC();
	getFreightAdjpercentAgC();
}

function getDistance()
{
	$("#txtDistance").val("");
	$("#loader1").show();
	if($('#selFPort').val() != "" && $('#selTPort').val() != "" && $('#selDType').val() != "")
	{
		$("#txtDistance").val("");
		$.post("options.php?id=7",{selFPort:""+$("#selFPort").val()+"",selTPort:""+$("#selTPort").val()+"",selDType:""+$("#selDType").val()+""}, function(data) 
		{
				$('#txtDistance').val(data);
				$("#loader1").hide();
		});
	}
	else
	{
		$('#txtDistance').val("");
		$("#loader1").hide();
	}
}

//..............for Port Rotation details...............................................
function addPortRotationDetails()
{
	if($("#selFPort").val() != "" && $("#selTPort").val() != "" && $("#selPType").val() != "" && $("#txtDistance").val() != "" && $("#selSSpeed").val() != "")
	{
			var id = $("#p_rotationID").val();
			var lasrvar1 = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar1 = rowid.split('_')[1];
			});
		if($("#selFPort").val() == $("#txtTPort_"+lasrvar1).val() || id == 0 || lasrvar1 == "")
		{
			getVoyageTime();
			id = (id - 1) + 2;
			$("#PRrow_Empty").remove();
			var frm_port = document.getElementById("selFPort").selectedIndex;
			var frm_port_text = document.getElementById("selFPort").options[frm_port].text;
			var to_port = document.getElementById("selTPort").selectedIndex;
			var to_port_text = document.getElementById("selTPort").options[to_port].text;
			var p_type = document.getElementById("selPType").selectedIndex;
			var p_type_text = document.getElementById("selPType").options[p_type].text;
			
			var d_type = document.getElementById("selDType").selectedIndex;
			var d_type_text = document.getElementById("selDType").options[d_type].text;
			
			var speed_type = document.getElementById("selSSpeed").selectedIndex;
			var speed_type_text = document.getElementById("selSSpeed").options[speed_type].text;
			
			$('<tr id="pr_Row_'+id+'"><td align="center" ><a href="#pr'+id+'" id="spcancel_'+id+'" onclick="removePortRotation('+id+','+$("#selFPort").val()+','+$("#selTPort").val()+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left">'+frm_port_text+'<input type="hidden" name="txtFPort_'+id+'" id="txtFPort_'+id+'" value="'+$("#selFPort").val()+'"/></td><td align="left">'+to_port_text+'<input type="hidden" name="txtTPort_'+id+'" id="txtTPort_'+id+'" value="'+$("#selTPort").val()+'"/></td><td align="left">'+p_type_text+'('+speed_type_text+')<input type="hidden" name="txtPType_'+id+'" id="txtPType_'+id+'" value="'+$("#selPType").val()+'"/><input type="hidden" name="txtSSpeed_'+id+'" id="txtSSpeed_'+id+'" value="'+$("#selSSpeed").val()+'"/></td><td align="left">'+parseFloat($("#txtDistance").val())+'('+d_type_text+')<input type="hidden" name="txtDistance_'+id+'" id="txtDistance_'+id+'" value="'+parseFloat($("#txtDistance").val())+'"/><input type="hidden" name="txtDType_'+id+'" id="txtDType_'+id+'" value="'+$("#selDType").val()+'"/></td><td align="left">'+$("#txtWeather").val()+'<input type="hidden" name="txtWeather_'+id+'" id="txtWeather_'+id+'" value="'+$("#txtWeather").val()+'"/></td><td align="left">'+$("#txtMargin").val()+'<input type="hidden" name="txtMargin_'+id+'" id="txtMargin_'+id+'" value="'+$("#txtMargin").val()+'"/></td></tr>').appendTo("#tblPortRotation");
			
			//for load port.....................
			$('<option value="'+$("#selFPort").val()+'">'+frm_port_text+'</option>').appendTo("#selLoadPort");
			//.........ends...................
			//for discharge port.....................
			$('<option value="'+$("#selTPort").val()+'">'+to_port_text+'</option>').appendTo("#selDisPort");
			//.........ends...................
			//for transit port.....................
			$('<option value="'+$("#selFPort").val()+'">'+frm_port_text+'</option>').appendTo("#selTLoadPort");
			//.........ends...................
			$("#p_rotationID").val(id);
			$("#selFPort").val($("#selTPort").val());
			$("#selTPort,#selPType,#txtDistance,#txtWeather,#txtMargin,#selSSpeed").val("");
			$("#selDType").val("1");
			
			var lasrvar = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar = rowid.split('_')[1];
			});
			$("[id^=spcancel_]").hide();
			$("#spcancel_"+lasrvar).show();
			
			getTTLFreight();
			getBunkerAdjCalculation();
		}
		else
		{
			jAlert('Please select in sequence of ports', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Sea Passage', 'Alert');
	}
}

function removePortRotation(var1,portid,disportid)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){	
			//for load port.....................
			$("#selLoadPort option[value='"+$("#txtFPort_"+var1).val()+"']").remove();
			$('[id^=txtLoadPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == portid)
				{
					if($("#txtPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtPCosts_"+rowid.split('_')[1]).val();}
					if($("#txtLPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtLPIDays_"+rowid.split('_')[1]).val();}
					if($("#txtLPWDays_"+rowid.split('_')[1]).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtLPWDays_"+rowid.split('_')[1]).val();}
					
					var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
					$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
					$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDays(rowid.split('_')[1]);
					
					$("#lp_Row_"+rowid.split('_')[1]).remove();
					$("#oscLProw_"+rowid.split('_')[1]).remove();
					$("#oscLProw1_"+rowid.split('_')[1]).remove();
					$("#oscDDSLProw_"+rowid.split('_')[1]).remove();
					$("#oscDDSLProw1_"+rowid.split('_')[1]).remove();
					
					if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
					$("#txtDDNRLPCost").val($("[id^=txtDDSLPOSCCost_]").sum().toFixed(2));
					var ttlcostMT = parseFloat($("#txtDDNRLPCost").val()) / parseFloat(c_qty);
					$("#txtDDNRLPCostMT").val(ttlcostMT.toFixed(2));
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					getTTLFreight();
				}
			});
			//.........ends...................	
			//for discharge port.....................
			$("#selDisPort option[value='"+$("#txtTPort_"+var1).val()+"']").remove();
			$('[id^=txtDisPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == disportid)
				{
					if($("#txtDPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtDPCosts_"+rowid.split('_')[1]).val();}
					if($("#txtDPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtDPIDays_"+rowid.split('_')[1]).val();}
					if($("#txtDPWDays_"+rowid.split('_')[1]).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtDPWDays_"+rowid.split('_')[1]).val();}
					
					var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
					$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
					$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDaysDP(rowid.split('_')[1]);
					
					$("#dp_Row_"+rowid.split('_')[1]).remove();
					$("#oscDProw_"+rowid.split('_')[1]).remove();
					$("#oscDProw1_"+rowid.split('_')[1]).remove();			
					$("#oscDDRDProw_"+rowid.split('_')[1]).remove();
					$("#oscDDRDProw1_"+rowid.split('_')[1]).remove();
					
					
					if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
					$("#txtDDNRDPCost").val($("[id^=txtDDRDPOSCCost_]").sum().toFixed(2));
					var ttlcostMT = parseFloat($("#txtDDNRDPCost").val()) / parseFloat(c_qty);
					$("#txtDDNRDPCostMT").val(ttlcostMT.toFixed(2));
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					getTTLFreight();
				}
			});
			//.........ends...................		
			//for transit port.....................
			$("#selTLoadPort option[value='"+$("#txtFPort_"+var1).val()+"']").remove();
			$('[id^=txtTLoadPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == portid)
				{
					if($("#txtTPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtTPCosts_"+rowid.split('_')[1]).val();}
					if($("#txtTPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtTPIDays_"+rowid.split('_')[1]).val();}
								
					var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDaysTP(rowid.split('_')[1]);
					
					$("#tp_Row_"+rowid.split('_')[1]).remove();
					$("#oscTProw_"+rowid.split('_')[1]).remove();
					$("#oscTProw1_"+rowid.split('_')[1]).remove();
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					getTTLFreight();
				}
			});
			//.........ends...................
			getRemoveVoyageTime(var1);	
			$("#pr_Row_"+var1).remove();
			var lasrvar = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar = rowid.split('_')[1];

			});
			$("#selTPort,#selPType,#txtDistance,#txtWeather,#txtMargin,#selSSpeed").val("");
			$("#selFPort").val($("#txtTPort_"+lasrvar).val());
			$("[id^=spcancel_]").hide();
			$("#spcancel_"+lasrvar).show();
			getTTLFreight();
		}
		else{return false;}
		});	
}

function getRemoveVoyageTime(var1)
{
	if($("#txtMargin_"+var1).val() == ""){var margin = 0;}else{var margin = $("#txtMargin_"+var1).val();}
	if($("#txtDistance_"+var1).val() == ""){var distance = 0;}else{var distance = $("#txtDistance_"+var1).val();}
	if($("#txtWeather_"+var1).val() == ""){var speed_adj = 0;}else{var speed_adj = $("#txtWeather_"+var1).val();}
	if($("#txtPType_"+var1).val() == 1)
	{
		if($("#txtSSpeed_"+var1).val() == 1)
		{
			var ballast = <?php echo $bfs;?>;
			var fo_ballast = <?php echo $fo_bfs;?>;
			var do_ballast = <?php echo $do_bfs;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 2)
		{
			var ballast = <?php echo $bes1;?>;
			var fo_ballast = <?php echo $fo_bes1;?>;
			var do_ballast = <?php echo $do_bes1;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 3)
		{
			var ballast = <?php echo $bes2;?>;
			var fo_ballast = <?php echo $fo_bes2;?>;
			var do_ballast = <?php echo $do_bes2;?>;
		}
		
		var calc = ($("#txtDistance_"+var1).val() / (parseFloat(ballast) + parseFloat(speed_adj)) )/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));	
		
		var ttl_days = parseFloat($("#txtBDays").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtBDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var diff_fo = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ballast);
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(diff_fo.toFixed(2));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		
		var diff_do = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ballast);
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(diff_do.toFixed(2));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat($("#txtBDist").val()) - parseFloat(distance);
		$("#txtBDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));	
	}
	if($("#txtPType_"+var1).val() == 2)
	{
		if($("#txtSSpeed_"+var1).val() == 1)
		{
			var ladan = <?php echo $lfs;?>;
			var fo_ladan = <?php echo $fo_lfs;?>;
			var do_ladan = <?php echo $do_lfs;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 2)
		{
			var ladan = <?php echo $les1;?>;
			var fo_ladan = <?php echo $fo_les1;?>;
			var do_ladan = <?php echo $do_les1;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 3)
		{
			var ladan = <?php echo $les2;?>;
			var fo_ladan = <?php echo $fo_les2;?>;
			var do_ladan = <?php echo $do_les2;?>;
		}
		
		var calc = ($("#txtDistance_"+var1).val() / (parseFloat(ladan) + parseFloat(speed_adj)) )/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		
		var ttl_days = parseFloat($("#txtLDays").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtLDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var diff_fo = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ladan);
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(diff_fo.toFixed(2));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		
		var diff_do = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ladan);
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(diff_do.toFixed(2));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat($("#txtLDist").val()) - parseFloat(distance);
		$("#txtLDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
	getTTLFreight();
}
//...................Port Rotation details ends here........................................
function getLoadPortCalculation()
{
	if($("#txtQMT").val() != "" && $("#txtRate").val() != "") 
	{
		var value = ($("#txtQMT").val() / $("#txtRate").val());
		$("#txtWDays").val(value.toFixed(2));
	}
	else
	{
		$("#txtWDays").val('0.00');
	}
}

function getLOadPortQty()
{
	if($("#selLPCName").val() != "")
	{
		if($("#txtCQMT").val() != "")
		{
			$("#txtQMT").val(parseFloat($("#txtCQMT").val()) - parseFloat($("[id^=txtLpQMT_]").sum()));
			getLoadPortCalculation();
		}
		else
		{
			$("#txtQMT").val(0);
		}
	}
	else
	{
		$("#txtQMT,#txtRate,#txtWDays").val("");
	}
}

function getDisPortQty()
{
	if($("#selDPCName").val() != "")
	{
		if($("#txtCQMT").val() != "")
		{
			$("#txtDQMT").val(parseFloat($("#txtCQMT").val()) - parseFloat($("[id^=txtDpQMT_]").sum()));
			getDisPortCalculation();
		}
		else
		{
			$("#txtDQMT").val(0);
		}
	}
	else
	{
		$("#txtDQMT,#txtDRate,#txtDWDays").val("");
	}
}

//..............for Load Port details...............................................
function addLoadPortDetails()
{
	if($("#selLoadPort").val() != "" && $("#txtQMT").val() != "" && $("#txtRate").val() != "" && $("#selLPCName").val() != "")
	{
		if(parseFloat($("#txtCQMT").val()) >= parseFloat($("[id^=txtLpQMT_],#txtQMT").sum()))
		{
			getTTLVoyageDays();
			var id = $("#load_portID").val();
			id = (id - 1) + 2;
			$("#LProw_Empty").remove();
			var load_port = document.getElementById("selLoadPort").selectedIndex;
			var load_port_text = document.getElementById("selLoadPort").options[load_port].text;
			
			var cargo = document.getElementById("selLPCName").selectedIndex;
			var cargo_text = document.getElementById("selLPCName").options[cargo].text;
			
			//var crane = document.getElementById("selCrType").selectedIndex;
			//var crane_text = document.getElementById("selCrType").options[crane].text;
					
			$('<tr id="lp_Row_'+id+'"><td align="center"><a href="#lp'+id+'" onclick="removeLoadPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left">'+load_port_text+'<input type="hidden" name="txtLoadPort_'+id+'" id="txtLoadPort_'+id+'" value="'+$("#selLoadPort").val()+'"/></td><td align="left" >'+cargo_text+'<input type="hidden" name="txtLPCID_'+id+'" id="txtLPCID_'+id+'" value="'+$("#selLPCName").val()+'"/></td><td align="left">'+$("#txtQMT").val()+'<input type="hidden" name="txtLpQMT_'+id+'" id="txtLpQMT_'+id+'" value="'+$("#txtQMT").val()+'"/></td><td align="left">'+$("#txtRate").val()+'<input type="hidden" name="txtLPRate_'+id+'" id="txtLPRate_'+id+'" value="'+$("#txtRate").val()+'"/></td><td align="left">'+$("#txtPCosts").val()+'<input type="hidden" name="txtPCosts_'+id+'" id="txtPCosts_'+id+'" value="'+$("#txtPCosts").val()+'"/></td><td align="left">'+$("#txtIDays").val()+'<input type="hidden" name="txtLPIDays_'+id+'" id="txtLPIDays_'+id+'" value="'+$("#txtIDays").val()+'"/></td><td align="left">'+$("#txtWDays").val()+'<input type="hidden" name="txtLPWDays_'+id+'" id="txtLPWDays_'+id+'" value="'+$("#txtWDays").val()+'"/></td></tr>').appendTo("#tblLoadPort");
			
			if($("#txtPCosts").val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtPCosts").val();}
			var lpcostMT = parseFloat(lp_cost) / parseFloat($("#txtCQMT").val());
			var lp = "'LP'";		
		$('<tr id="oscLProw_'+id+'"><td>Load Port '+load_port_text+'</td><td></td><td><input type="text"  name="txtLPOSCCost_'+id+'" id="txtLPOSCCost_'+id+'" class="form-control" readonly="true" value="'+$("#txtPCosts").val()+'" /></td><td><select  name="selPCLPVendorList_'+id+'" class="select form-control" id="selPCLPVendorList_'+id+'"><?php $obj->getVendorListNewUpdate("");?></select></td><td><input type="text" name="txtLPOSCCostMT_'+id+'" id="txtLPOSCCostMT_'+id+'" class="form-control" readonly="true" value="'+lpcostMT.toFixed(2)+'" /></td></tr>').appendTo("#tbPortCosts");
		
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
			$('<tr id="oscDDSLProw_'+id+'"><td>Load Port   '+load_port_text+'</td><td></td><td><input type="text"  name="txtDDSLPOSCCost_'+id+'" id="txtDDSLPOSCCost_'+id+'" class="form-control" value="0.00" onkeyup="getTotalLP('+id+');" /></td><td><select  name="selDDSLPVendorList_'+id+'" class="select form-control" id="selDDSLPVendorList_'+id+'"><?php $obj->getVendorListNewUpdate("");?></select></td><td align="left"><input type="text" name="txtDDLLPOSCCostMT_'+id+'" id="txtDDLLPOSCCostMT_'+id+'" class="form-control" readonly="true" value="0.00" placeholder="0.00" /></td></tr>').appendTo("#tbDDShipper");
			
			$("#txtTtPIDays").val($("#txtTtPIDays,#txtIDays").sum().toFixed(2));
			$("#txtTtPWDays").val($("#txtTtPWDays,#txtWDays").sum().toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			$("[id^=txtDDSLPOSCCost_],[id^=txtDDNRLPOSCCost_]").numeric();
			$("#load_portID").val(id);
			$("#txtQMT,#txtRate,#selLoadPort,#txtPCosts,#txtIDays,#txtWDays,#txtLPDraftM,#selLPCName,#selCrType").val("");
			
			getTTLFreight();
			getBunkerAdjCalculation();
			getOMCCalculate();
		}
		else
		{
			jAlert('Loaded quantity is greater than Cargo Qty.', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Load Port', 'Alert');
	}
}

function getPortCostSum(var1,port)
{
	if(port == 'LP')
	{
		if($("#txtLPOSCCost_"+var1).val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtLPOSCCost_"+var1).val();}
		var lpcostMT = parseFloat(lp_cost) / parseFloat($("#txtCQMT").val());
		$("#txtLPOSCCostMT_"+var1).val(lpcostMT.toFixed(2));
	}
	else if(port == 'DP')
	{
		if($("#txtDPOSCCost_"+var1).val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPOSCCost_"+var1).val();}
		var dpcostMT = parseFloat(dp_cost) / parseFloat($("#txtCQMT").val());
		$("#txtDPOSCCostMT_"+var1).val(dpcostMT.toFixed(2));
	}
	else if(port == 'TP')
	{
		if($("#txtTPOSCCost_"+var1).val() == ""){var tp_cost = 0;}else{var tp_cost = $("#txtTPOSCCost_"+var1).val();}
		var tpcostMT = parseFloat(tp_cost) / parseFloat($("#txtCQMT").val());
		$("#txtTPOSCCostMT_"+var1).val(tpcostMT.toFixed(2));
	}
				
	var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
	$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
	$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
}

function removeLoadPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){ 
			if($("#txtPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtPCosts_"+var1).val();}
			if($("#txtLPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtLPIDays_"+var1).val();}
			if($("#txtLPWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtLPWDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
			var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			getRemoveTTLVoyageDays(var1);
			
			$("#lp_Row_"+var1).remove();
			$("#oscLProw_"+var1).remove();
			$("#oscLProw1_"+var1).remove();
			$("#oscDDSLProw_"+var1).remove();
			$("#oscDDSLProw1_"+var1).remove();
			
			if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
			$("#txtDDNRLPCost").val($("[id^=txtDDSLPOSCCost_]").sum().toFixed(2));
			var ttlcostMT = parseFloat($("#txtDDNRLPCost").val()) / parseFloat(c_qty);
			$("#txtDDNRLPCostMT").val(ttlcostMT.toFixed(2));
			
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			
			getTTLFreight();
			getBunkerAdjCalculation();
			getOMCCalculate();
		}
		else{return false;}
		});	
}

function getRemoveTTLVoyageDays(var1)
{
	if($("#txtLPIDays_"+var1).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtLPIDays_"+var1).val();}
	if($("#txtLPWDays_"+var1).val() == ""){var w_days = 0;}else{var w_days = $("#txtLPWDays_"+var1).val();}
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) -  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	//if($("#txtLPCType_"+var1).val() == 1)
	//{
		var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
	//}
	//else if($("#txtLPCType_"+var1).val() == 2)
	//{
		//var wrkingdays_fo = parseFloat(w_days) * <?php //echo $foidle;?>;
	//}
	
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2)));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
	//if($("#txtLPCType_"+var1).val() == 1)
	//{
		var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
	//}
	//else if($("#txtLPCType_"+var1).val() == 2)
	//{
		//var wrkingdays_do = parseFloat(w_days) * <?php //echo $doidle;?>;		
	//}
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2)));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	getTTLFreight();
	getBunkerAdjCalculation();
	getOMCCalculate();
}

//...................Load Port details ends here........................................
function getTotalLP(var1)
{
	if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
	var costMT = parseFloat($("#txtDDSLPOSCCost_"+var1).val()) / parseFloat(c_qty);
	$("#txtDDLLPOSCCostMT_"+var1).val(costMT.toFixed(2));
	
	$("#txtDDNRLPCost").val($("[id^=txtDDSLPOSCCost_]").sum().toFixed(2));
	var ttlcostMT = parseFloat($("#txtDDNRLPCost").val()) / parseFloat(c_qty);
	$("#txtDDNRLPCostMT").val(ttlcostMT.toFixed(2));
}

function getDisPortCalculation()
{
	if($("#txtDQMT").val() != "" && $("#txtDRate").val() != "") 
	{
		var value = ($("#txtDQMT").val() / $("#txtDRate").val());
		$("#txtDWDays").val(value.toFixed(2));
	}
	else
	{
		$("#txtDWDays").val('0.00');
	}
}

function getTTLVoyageDaysDP()
{
	if($("#txtDIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtDIDays").val();}
	if($("#txtDWDays").val() == ""){var w_days = 0;}else{var w_days = $("#txtDWDays").val();}
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	//if($("#selDPCrType").val() == 1)
	//{
		var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
	//}
	//else if($("#selDPCrType").val() == 2)
	//{
		//var wrkingdays_fo = parseFloat(w_days) * <?php //echo $foidle;?>;
	//}
	
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
	//if($("#selDPCrType").val() == 1)
	//{
		var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
	//}
	//else if($("#selDPCrType").val() == 2)
	//{
		//var wrkingdays_do = parseFloat(w_days) * <?php //echo $doidle;?>;		
	//}
	
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
	
	getTTLFreight();
	getBunkerAdjCalculation();
	getOMCCalculate();
}

//..............for Dis Port details...............................................
function addDisPortDetails()
{
	if($("#selDisPort").val() != "" && $("#txtDQMT").val() != "" && $("#txtDRate").val() != "" && $("#selDPCName").val() != "" )
	{
		if(parseFloat($("#txtCQMT").val()) >= parseFloat($("[id^=txtDpQMT_],#txtDQMT").sum()))
		{
			getTTLVoyageDaysDP();
			var id = $("#dis_portID").val();
			id = (id - 1) + 2;
			$("#DProw_Empty").remove();
			var dis_port = document.getElementById("selDisPort").selectedIndex;
			var dis_port_text = document.getElementById("selDisPort").options[dis_port].text;
			
			var cargo = document.getElementById("selDPCName").selectedIndex;
			var cargo_text = document.getElementById("selDPCName").options[cargo].text;
			
			//var crane = document.getElementById("selDPCrType").selectedIndex;
			//var crane_text = document.getElementById("selDPCrType").options[crane].text;
			
			$('<tr id="dp_Row_'+id+'"><td align="center"><a href="#dp'+id+'" onclick="removeDisPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left">'+dis_port_text+'<input type="hidden" name="txtDisPort_'+id+'" id="txtDisPort_'+id+'" value="'+$("#selDisPort").val()+'"/></td><td align="left">'+cargo_text+'<input type="hidden" name="txtDPCID_'+id+'" id="txtDPCID_'+id+'" value="'+$("#selDPCName").val()+'"/></td><td align="left">'+$("#txtDQMT").val()+'<input type="hidden" name="txtDpQMT_'+id+'" id="txtDpQMT_'+id+'" value="'+$("#txtDQMT").val()+'"/></td><td align="left">'+$("#txtDRate").val()+'<input type="hidden" name="txtDPRate_'+id+'" id="txtDPRate_'+id+'" value="'+$("#txtDRate").val()+'"/></td><td align="left">'+$("#txtDPCosts").val()+'<input type="hidden" name="txtDPCosts_'+id+'" id="txtDPCosts_'+id+'" value="'+$("#txtDPCosts").val()+'"/></td><td align="left">'+$("#txtDIDays").val()+'<input type="hidden" name="txtDPIDays_'+id+'" id="txtDPIDays_'+id+'" value="'+$("#txtDIDays").val()+'"/></td><td align="left">'+$("#txtDWDays").val()+'<input type="hidden" name="txtDPWDays_'+id+'" id="txtDPWDays_'+id+'" value="'+$("#txtDWDays").val()+'"/></td></tr>').appendTo("#tblDisPort");
			
		if($("#txtDPCosts").val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPCosts").val();}
		var dpcostMT = parseFloat(dp_cost) / parseFloat($("#txtCQMT").val());
		var dp = "'DP'";
		$('<tr id="oscDProw_'+id+'"><td>Discharge Port   '+dis_port_text+'</td><td></td><td><input type="text"  name="txtDPOSCCost_'+id+'" id="txtDPOSCCost_'+id+'" class="form-control" readonly="true" value="'+$("#txtDPCosts").val()+'" /></td><td><select  name="selPCDPVendorList_'+id+'" class="select form-control" id="selPCDPVendorList_'+id+'"><?php $obj->getVendorListNewUpdate("");?></select></td><td><input type="text" name="txtDPOSCCostMT_'+id+'" id="txtDPOSCCostMT_'+id+'" class="form-control" readonly="true" value="'+dpcostMT.toFixed(2)+'"  /></td></tr>').appendTo("#tbPortCosts");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
		$('<tr id="oscDDRDProw_'+id+'"><td>Discharge Port  '+dis_port_text+'</td><td></td><td><input type="text"  name="txtDDRDPOSCCost_'+id+'" id="txtDDRDPOSCCost_'+id+'" class="form-control" value="0.00" onkeyup="getTotalDP('+id+');" /></td><td><select  name="selDDRLPVendorCList_'+id+'" class="select form-control" id="selDDRLPVendorCList_'+id+'"><?php $obj->getVendorListNewUpdate("");?></select></td><td align="left" valign="top"><input type="text" name="txtDDRDPOSCCostMT_'+id+'" id="txtDDRDPOSCCostMT_'+id+'" class="form-control" readonly="true" value="0.00" placeholder="0.00" /></td></tr>').appendTo("#tbDDReceiver");
		
			$("[id^=txtDDRDPOSCCost_],[id^=txtDDNRDPOSCCost_]").numeric();
			$("#txtTtPIDays").val($("#txtTtPIDays,#txtDIDays").sum().toFixed(2));
			$("#txtTtPWDays").val($("#txtTtPWDays,#txtDWDays").sum().toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			$("#dis_portID").val(id);
			$("#selDisPort,#txtDQMT,#txtDRate,#txtDPDraftM,#txtDPCosts,#txtDIDays,#txtDWDays,#selDPCName,#selDPCrType").val("");
			
			getTTLFreight();
			getBunkerAdjCalculation();
			getOMCCalculate();
		}
		else
		{
			jAlert('Discharged quantity is greater than Cargo Qty.', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Discharge Port', 'Alert');
	}
}

function getTotalDP(var1)
{
	$("#txtDDNRDPCost").val($("[id^=txtDDRDPOSCCost_]").sum().toFixed(2));
	
	if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
	var costMT = parseFloat($("#txtDDRDPOSCCost_"+var1).val()) / parseFloat(c_qty);
	$("#txtDDRDPOSCCostMT_"+var1).val(costMT.toFixed(2));
	
	$("#txtDDNRDPCost").val($("[id^=txtDDRDPOSCCost_]").sum().toFixed(2));
	var ttlcostMT = parseFloat($("#txtDDNRDPCost").val()) / parseFloat(c_qty);
	$("#txtDDNRDPCostMT").val(ttlcostMT.toFixed(2));
}

function removeDisPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){ 
			if($("#txtDPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtDPCosts_"+var1).val();}
			if($("#txtDPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtDPIDays_"+var1).val();}
			if($("#txtDPWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtDPWDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
			var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			getRemoveTTLVoyageDaysDP(var1);
			
			$("#dp_Row_"+var1).remove();
			$("#oscDProw_"+var1).remove();
			$("#oscDProw1_"+var1).remove();			
			$("#oscDDRDProw_"+var1).remove();
			$("#oscDDRDProw1_"+var1).remove();
			
			if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
			$("#txtDDNRDPCost").val($("[id^=txtDDRDPOSCCost_]").sum().toFixed(2));
			var ttlcostMT = parseFloat($("#txtDDNRDPCost").val()) / parseFloat(c_qty);
			$("#txtDDNRDPCostMT").val(ttlcostMT.toFixed(2));
			
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			
			getTTLFreight();
			getBunkerAdjCalculation();
			getOMCCalculate();
		}
		else{return false;}
		});	
}

function getRemoveTTLVoyageDaysDP(var1)
{
	if($("#txtDPIDays_"+var1).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtDPIDays_"+var1).val();}
	if($("#txtDPWDays_"+var1).val() == ""){var w_days = 0;}else{var w_days = $("#txtDPWDays_"+var1).val();}
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) -  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	//if($("#txtDPCType_"+var1).val() == 1)
	//{
		var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
	//}
	//else if($("#txtDPCType_"+var1).val() == 2)
	//{
		//var wrkingdays_fo = parseFloat(w_days) * <?php //echo $foidle;?>;
	//}
	
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2)));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
	//if($("#txtDPCType_"+var1).val() == 1)
	//{
		var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
	//}
	//else if($("#txtDPCType_"+var1).val() == 2)
	//{
		//var wrkingdays_do = parseFloat(w_days) * <?php //echo $doidle;?>;		
	//}
	
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2)));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	getTTLFreight();
	getBunkerAdjCalculation();
	getOMCCalculate();
}

//..............for Transit Port details...............................................
function getTTLVoyageDaysTP()
{
	if($("#txtTLIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtTLIDays").val();}
	var ttl_days1 = parseFloat(idle_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo);
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;	
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do);
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	getTTLFreight();
}

function addTransitPortDetails()
{
	if($("#selTLoadPort").val() != "")
	{
		getTTLVoyageDaysTP();
		var id = $("#transit_portID").val();
		id = (id - 1) + 2;
		$("#TProw_Empty").remove();
		var load_port = document.getElementById("selTLoadPort").selectedIndex;
		var load_port_text = document.getElementById("selTLoadPort").options[load_port].text;
				
		$('<tr id="tp_Row_'+id+'"><td align="center"><a href="#dp'+id+'" onclick="removeTransitPort('+id+');" ><i class="fa fa-times" style="color:red;"></i></a></td><td align="left">'+load_port_text+'<input type="hidden" name="txtTLoadPort_'+id+'" id="txtTLoadPort_'+id+'" value="'+$("#selTLoadPort").val()+'"/></td><td align="left">'+$("#txtTLPCosts").val()+'<input type="hidden" name="txtTPCosts_'+id+'" id="txtTPCosts_'+id+'" value="'+$("#txtTLPCosts").val()+'"/></td><td align="left">'+$("#txtTLIDays").val()+'<input type="hidden" name="txtTPIDays_'+id+'" id="txtTPIDays_'+id+'" value="'+$("#txtTLIDays").val()+'"/></td></tr>').appendTo("#tblTransitPort");
		
		if($("#txtTLPCosts").val() == ""){var tp_cost = 0;}else{var tp_cost = $("#txtTLPCosts").val();}
		var tpcostMT = parseFloat(tp_cost) / parseFloat($("#txtCQMT").val());
		var tp = "'TP'";
		$('<tr id="oscTProw_'+id+'"><td>Transit Port   '+load_port_text+'</td><td></td><td><input type="text"  name="txtTPOSCCost_'+id+'" id="txtTPOSCCost_'+id+'" class="form-control" readonly="true" value="'+$("#txtTLPCosts").val()+'" /></td><td><select  name="selPCTPVendorList_'+id+'" class="select form-control" id="selPCTPVendorList_'+id+'"><?php $obj->getVendorListNewUpdate("");?></select></td><td><input type="text" name="txtTPOSCCostMT_'+id+'" id="txtTPOSCCostMT_'+id+'" class="form-control" readonly="true" value="'+tpcostMT.toFixed(2)+'" /></td></tr>').appendTo("#tbPortCosts");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
		$("#txtTtPIDays").val($("#txtTtPIDays,#txtTLIDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		$("#transit_portID").val(id);
		$("#selTLoadPort,#txtTLPCosts,#txtTLIDays").val("");
		
		getTTLFreight();
		getBunkerAdjCalculation();
		getOMCCalculate();
	}
	else
	{
		jAlert('Please fill all the records for Tansit Port', 'Alert');
	}
}

function getRemoveTTLVoyageDaysTP(var1)
{
	if($("#txtTPIDays_"+var1).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtTPIDays_"+var1).val();}
	var ttl_days1 = parseFloat(idle_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) -  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(margin_fo);
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;		
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(margin_do);
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	getTTLFreight();
	getBunkerAdjCalculation();
	getOMCCalculate();
}

function removeTransitPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){
			if($("#txtTPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtTPCosts_"+var1).val();}
			if($("#txtTPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtTPIDays_"+var1).val();}
						
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			getRemoveTTLVoyageDaysTP(var1);
			 
			$("#tp_Row_"+var1).remove();
			$("#oscTProw_"+var1).remove();
			$("#oscTProw1_"+var1).remove();
			
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			getTTLFreight();
			getBunkerAdjCalculation();
			getOMCCalculate();
		}
		else{return false;}
		});	
}

function getShowHide()
{
	if($("#txtbid").val() == 0)
	{
		document.BID.src = "../../img/open.png";
		$('#BID').attr('title', 'Open Panel');
		$("#bunker_adj").hide();
		$("#txtbid").val(1);
	}
	else
	{
		document.BID.src = "../../img/close.png";
		$('#BID').attr('title', 'Close Panel');
		$("#bunker_adj").show();
		$("#txtbid").val(0);
	}
}

function getCalculate1(var1,var2,var3)
{
	$("#txt"+var1+"_"+var2+"_1").val(var3);
	if(var3 == ""){var first = 0;}else{var first = var3;}
	if($("#txt"+var1+"_"+var2+"_2").val() == ""){var second = 0;}else{var second = $("#txt"+var1+"_"+var2+"_2").val();}
	var calc = parseFloat(first) * parseFloat(second);
	$("#txt"+var1+"_"+var2+"_3").val(calc.toFixed(2));
		
	if($("[id^=txtTTLBAA_]").sum().toFixed(2) < 0)
	{
		$("#txtBunkerAdjAcc").css({"color":"#ff0000"});
	}
	else
	{
		$("#txtBunkerAdjAcc").css({"color":"#808080"});
	}
	$("#txtBunkerAdjAcc").val($("[id^=txtTTLBAA_]").sum().toFixed(2));
}


function getCalculate(var1,var2,var3,var4)
{	
	$("#txt"+var1+"_"+var2+"_2").val(var3);
	if($("#txt"+var1+"_"+var2+"_1").val() == ""){var first = 0;}else{var first = $("#txt"+var1+"_"+var2+"_1").val();}
	if(var3 == ""){var second = 0;}else{var second = var3;}
	var calc = parseFloat(first) * parseFloat(second);
	$("#txt"+var1+"_"+var2+"_3").val(calc.toFixed(2));
	
	$("#txtTTLBAA_"+var4).val($("#txt"+var1+"_7_3").val());
	$("#txtTTLEst_"+var4).val($("#txt"+var1+"_1_3").val());
	
	if($("[id^=txtTTLBAA_]").sum().toFixed(2) < 0)
	{
		$("#txtBunkerAdjAcc").css({"color":"#ff0000"});
	}
	else
	{
		$("#txtBunkerAdjAcc").css({"color":"#808080"});
	}
	$("#txtBunkerAdjAcc").val($("[id^=txtTTLBAA_]").sum().toFixed(2));
	
}

function getTTLFreight()
{
	if($("#rdoMarket1").is(":checked"))
	{
		if($("#txtTCPDRate").val() == ""){var tc_cost_perday = 0;}else{var tc_cost_perday = $("#txtTCPDRate").val();}
		var ttl_freight = parseFloat(tc_cost_perday) * parseFloat($("#txtTDays").val());
		$("#txtFrAdjUsdTH").val(ttl_freight.toFixed(2));
	}
	if($("#rdoMarket2").is(":checked"))
	{
		if($("#txtLumpsum").val() == ""){var lumpsum = 0;}else{var lumpsum = $("#txtLumpsum").val();}
		$("#txtFrAdjUsdTH").val(lumpsum);
	}
	getFreightAdjpercentAC();
	getFreightAdjpercentAgC();
}

function getFreightAdjpercentAC()
{
	if($("#txtFrAdjUsdTH").val() == "0.00"){
		$("#txtFrAdjUsdAC").val("0.00");
	}
	else
	{
		if($("#txtFrAdjPerAC").val() == ""){var frt_ac_per = 0;}else{var frt_ac_per = $("#txtFrAdjPerAC").val();}
		var percent = (parseFloat($("#txtFrAdjUsdTH").val()) * parseFloat(frt_ac_per)) / 100;
		$("#txtFrAdjUsdAC").val(percent.toFixed(2));
	}
	var nett = parseFloat($("#txtFrAdjUsdTH").val()) - parseFloat($("#txtFrAdjUsdAC,#txtFrAdjUsdAgC").sum());
	$("#txtFrAdjUsdNH,#txtTTLORCAmt,#txtNHire").val(nett.toFixed(2));
}

function getFreightAdjpercentAgC()
{ 
	if($("#txtFrAdjUsdTH").val() == "0.00"){
		$("#txtFrAdjUsdAgC").val("0.00");
	}
	else
	{
		if($("#txtFrAdjPerAgC").val() == ""){var frt_ac_per = 0;}else{var frt_ac_per = $("#txtFrAdjPerAgC").val();}
		var percent = (parseFloat($("#txtFrAdjUsdTH").val()) * parseFloat(frt_ac_per)) / 100;
		$("#txtFrAdjUsdAgC,#txtOSCAbs_1").val(percent.toFixed(2));
	}
	var nett = parseFloat($("#txtFrAdjUsdTH").val()) - parseFloat($("#txtFrAdjUsdAC,#txtFrAdjUsdAgC").sum());
	$("#txtFrAdjUsdNH,#txtTTLORCAmt,#txtNHire").val(nett.toFixed(2));
}

function getVoyageTime()
{
	if($("#txtMargin").val() == ""){var margin = 0;}else{var margin = $("#txtMargin").val();}
	if($("#txtDistance").val() == ""){var distance = 0;}else{var distance = $("#txtDistance").val();}
	if($("#txtWeather").val() == ""){var speed_adj = 0;}else{var speed_adj = $("#txtWeather").val();}
	if($("#selPType").val() == 1)
	{
		if($("#selSSpeed").val() == 1)
		{
			var ballast = <?php echo $bfs;?>;
			var fo_ballast = <?php echo $fo_bfs;?>;
			var do_ballast = <?php echo $do_bfs;?>;
		}
		if($("#selSSpeed").val() == 2)
		{
			var ballast = <?php echo $bes1;?>;
			var fo_ballast = <?php echo $fo_bes1;?>;
			var do_ballast = <?php echo $do_bes1;?>;
		}
		if($("#selSSpeed").val() == 3)
		{
			var ballast = <?php echo $bes2;?>;
			var fo_ballast = <?php echo $fo_bes2;?>;
			var do_ballast = <?php echo $do_bes2;?>;
		}
		
		var calc = ($("#txtDistance").val() /  (parseFloat(ballast) + parseFloat(speed_adj)))/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) + parseFloat(calc) + parseFloat(margin);
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		var ttl_days = parseFloat(calc) + parseFloat($("#txtBDays").val()) + parseFloat(margin);
		$("#txtBDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ballast));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ballast));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat(distance) + parseFloat($("#txtBDist").val());
		$("#txtBDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
	if($("#selPType").val() == 2)
	{
		if($("#selSSpeed").val() == 1)
		{
			var ladan = <?php echo $lfs;?>;
			var fo_ladan = <?php echo $fo_lfs;?>;
			var do_ladan = <?php echo $do_lfs;?>;
		}
		if($("#selSSpeed").val() == 2)
		{
			var ladan = <?php echo $les1;?>;
			var fo_ladan = <?php echo $fo_les1;?>;
			var do_ladan = <?php echo $do_les1;?>;
		}
		if($("#selSSpeed").val() == 3)
		{
			var ladan = <?php echo $les2;?>;
			var fo_ladan = <?php echo $fo_les2;?>;
			var do_ladan = <?php echo $do_les2;?>;
		}
		
		var calc = ($("#txtDistance").val() / (parseFloat(ladan) + parseFloat(speed_adj)) )/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) + parseFloat(calc) + parseFloat(margin);
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		var ttl_days = parseFloat(calc) + parseFloat($("#txtLDays").val()) + parseFloat(margin);
		$("#txtLDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ladan));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ladan));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat(distance) + parseFloat($("#txtLDist").val());
		$("#txtLDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
	getTTLFreight();
	getBunkerAdjCalculation();
	getOMCCalculate();
}

function getTTLVoyageDays()
{
	if($("#txtIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtIDays").val();}
	if($("#txtWDays").val() == ""){var w_days = 0;}else{var w_days = $("#txtWDays").val();}
	
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	//if($("#selCrType").val() == 1)
	//{
		var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
	//}
	//else if($("#selCrType").val() == 2)
	//{
		//var wrkingdays_fo = parseFloat(w_days) * <?php //echo $foidle;?>;
	//}
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
	//if($("#selCrType").val() == 1)
	//{
		var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
	//}
	//else if($("#selCrType").val() == 2)
	//{
		//var wrkingdays_do = parseFloat(w_days) * <?php //echo $doidle;?>;		
	//}
	
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
	getTTLFreight();
}

function getORCCalculate(status,var1)
{
	if(status == 1)
	{
		$("#txtHidORCAmt_"+var1).val($("#txtORCAmt_"+var1).val());
	}
	if(status == 2)
	{
		$("#txtHidORCAmt_"+var1).val("-"+$("#txtORCAmt_"+var1).val());
	}
	$("#txtTTLORCAmt").val($("#txtFrAdjUsdNH,[id^=txtHidORCAmt_],#txtBunkerAdjAcc").sum());
	
	if($("#txtCQMT").val() == "" || $("#txtCQMT").val() == 0)
	{
		$("#txtTTLORCCostMT").val($("#txtTTLORCAmt").val());
	}
	else
	{
		var calc = parseFloat($("#txtTTLORCAmt").val()) / parseFloat($("#txtCQMT").val());
		$("#txtTTLORCCostMT").val(calc.toFixed(2));
	}
}

function getOSCostCalculate1(var1)
{
	$("#txtTTLPortCost,#txtTTLShippingCost").val($("#txtTTLORCAmt,[id^=txtOSCAbs_],#txtTTLLPCost,#txtTTLDPCost,#txtTTLTPCost,[id^=txtTTLEst_]").sum().toFixed(2));
	if($("#txtCQMT").val() == "" || $("#txtCQMT").val() == 0)
	{
		$("#txtOSCCostMT_"+var1).val($("#txtOSCAbs_"+var1).val());
		$("#txtTTLPCostMT").val($("#txtTTLPortCost").val());
		$("#txtTTLShippingCostMT").val($("#txtTTLShippingCost").val());
	}
	else
	{
		if($("#txtOSCAbs_"+var1).val() == ""){var osc_abs = 0;}else{var osc_abs = $("#txtOSCAbs_"+var1).val();}
		var calc = parseFloat(osc_abs) / parseFloat($("#txtCQMT").val());
		$("#txtOSCCostMT_"+var1).val(calc.toFixed(2));
		
		var calc1 = parseFloat($("#txtTTLPortCost").val()) / parseFloat($("#txtCQMT").val());
		$("#txtTTLPCostMT").val(calc1.toFixed(2));
		
		var calc2 = parseFloat($("#txtTTLShippingCost").val()) / parseFloat($("#txtCQMT").val());
		$("#txtTTLShippingCostMT").val(calc2.toFixed(2));
	}
	
}

function getOMCCalculate(var1)
{
	$("#txtTTLOtherCost").val($("[id^=txtOMCAbs_]").sum().toFixed(2));
	$("#txtTTLPortCost").val($("#txtTTLORCAmt,[id^=txtOSCAbs_],#txtTTLLPCost,#txtTTLDPCost,#txtTTLTPCost").sum().toFixed(2));
	$("#txtTTLShippingCost").val($("#txtTTLPortCost,#txtTTLOtherCost,#txtTTLPortCosts,#txtDDNRLPCost,#txtDDNRDPCost,[id^=txtTTLEst_]").sum().toFixed(2));
	if($("#txtCQMT").val() == "" || $("#txtCQMT").val() == 0)
	{
		$("#txtOMCCostMT_"+var1).val($("#txtOMCAbs_"+var1).val());
		$("#txtTTLOtherCostMT").val($("#txtTTLOtherCost").val());
		$("#txtTTLShippingCostMT").val($("#txtTTLShippingCost").val());
	}
	else
	{
		if($("#txtOMCAbs_"+var1).val() == ""){var omc_abs = 0;}else{var omc_abs = $("#txtOMCAbs_"+var1).val();}
		var calc = parseFloat(omc_abs) / parseFloat($("#txtCQMT").val());
		$("#txtOMCCostMT_"+var1).val(calc.toFixed(2));
		
		var calc1 = parseFloat($("#txtTTLOtherCost").val()) / parseFloat($("#txtCQMT").val());
		$("#txtTTLOtherCostMT").val(calc1.toFixed(2));
		
		var calc2 = parseFloat($("#txtTTLShippingCost").val()) / parseFloat($("#txtCQMT").val());
		$("#txtTTLShippingCostMT").val(calc2.toFixed(2));
		
		var calc3 = parseFloat($("#txtTTLPortCost").val()) / parseFloat($("#txtCQMT").val());
		$("#txtTTLPCostMT").val(calc3.toFixed(2));
	}
}

function getFinalCalculation()
{
	$("#txtTTLORCAmt").val($("#txtFrAdjUsdNH,[id^=txtHidORCAmt_],#txtBunkerAdjAcc").sum());
	if($("#txtCQMT").val() == "" || $("#txtCQMT").val() == 0)
	{
		$("#txtTTLORCCostMT").val($("#txtTTLORCAmt").val());
	}
	else
	{
		var calc = parseFloat($("#txtTTLORCAmt").val()) / parseFloat($("#txtCQMT").val());
		$("#txtTTLORCCostMT").val(calc.toFixed(2));
	}
	
	$("#txtTTLOtherCost").val($("[id^=txtOMCAbs_]").sum().toFixed(2));
	$("#txtTTLPortCost").val($("#txtTTLORCAmt,[id^=txtOSCAbs_],#txtTTLLPCost,#txtTTLDPCost,#txtTTLTPCost").sum().toFixed(2));
	$("#txtTTLShippingCost").val($("#txtTTLPortCost,#txtTTLOtherCost,#txtTTLPortCosts,#txtDDNRLPCost,#txtDDNRDPCost,[id^=txtTTLEst_]").sum().toFixed(2));
	if($("#txtCQMT").val() == "" || $("#txtCQMT").val() == 0)
	{
		$("#txtOMCCostMT_"+var1).val($("#txtOMCAbs_"+var1).val());
		$("#txtTTLOtherCostMT").val($("#txtTTLOtherCost").val());
		$("#txtTTLShippingCostMT").val($("#txtTTLShippingCost").val());
	}
	else
	{
		var calc1 = parseFloat($("#txtTTLOtherCost").val()) / parseFloat($("#txtCQMT").val());
		$("#txtTTLOtherCostMT").val(calc1.toFixed(2));
		
		var calc2 = parseFloat($("#txtTTLShippingCost").val()) / parseFloat($("#txtCQMT").val());
		$("#txtTTLShippingCostMT").val(calc2.toFixed(2));
		
		var calc3 = parseFloat($("#txtTTLPortCost").val()) / parseFloat($("#txtCQMT").val());
		$("#txtTTLPCostMT").val(calc3.toFixed(2));
	}
	//
	$("#txtBrCommPercent").val($("[id^=txtBrCommPercent_]").sum().toFixed(2));
	
}

function getValidate()
{
	if($("#txtTTLShippingCost").val() > 0)
	{
			getFinalCalculation();
			return true;
	}
	else
	{
		jAlert('Not saved empty cost sheet', 'Alert');
		return false;
	}
}

function getBunkerAdjCalculation(){
	
	var noOfBunker = $('#txtBunkerRec').val();
	for(var i = 1; i<=noOfBunker; i++){
		var bunkergrade = $('#txtBunkerGradName_'+i).val();
		var firststr    = bunkergrade.substr(0,3);
		
		if(firststr == 'IFO')
		{
			if($("#txtTFUMTManual").val() > 0)
			{
				$("#txt"+bunkergrade+"_1_1").val($("#txtTFUMTManual").val());
			}
			else
			{
				$("#txt"+bunkergrade+"_1_1").val($("#txtTFUMT").val());
			}
		}
		else if(firststr == 'MDO')
		{
			if($("#txtTDUMTManual").val() > 0)
			{
				$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMTManual").val());
			}
			else
			{
				$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMT").val());
			}
		}
		else if(firststr == 'MGO')
		{
			if($("#txtTDUMTManual").val() > 0)
			{
				$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMTManual").val());
			}
			else
			{
				$("#txt"+bunkergrade+"_1_1").val($("#txtTDUMT").val());
			}
		}
		else
		{
			$("#txt"+bunkergrade+"_1_1").val('0.00');
		}
	}
}

function addBrokerageRow()
{
	var id = $("#txtBRokageCount").val();
	if($("#txtBrCommPercent_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="tbrRow_'+id+'"><td><a href="#tb1" onclick="removeBrokerage('+id+');" ><i class="fa fa-times" style="color:red;"></a></td><td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td><td><input type="text" name="txtBrCommPercent_'+id+'" id="txtBrCommPercent_'+id+'" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td><td><input type="text" name="txtBrComm_'+id+'" id="txtBrComm_'+id+'" class="form-control" readonly value="0.00" /></td><td><select  name="selBroVList_'+id+'" class="select form-control" id="selBroVList_'+id+'"></select></td></tr>').appendTo("#tbodyBrokerage");
	    $("#selBroVList_"+id).html($("#selVendor").html());$("#selBroVList_"+id).val('');
		$("[id^=txtBrCommPercent_]").numeric();	
		$("#txtBRokageCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

</script>
    </body>
</html>