<?php 
session_start();
require_once("../../includes/display_external_user_dryout.inc.php");
require_once("../../includes/functions_external_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->updateVesselDetails();
	header('Location:./fleet.php?msg='.$msg);
 }
$obj->viewVesselRecords($id);
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->

<style>
form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-anchor"></i>&nbsp;Fleet&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Fleet</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div align="right"><a href="fleet.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				
				
				<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">EDIT VESSEL</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
					<div class="box-body">
                    
                        <div class="form-group" style="width:49%; float:left;">
							<label for="txtVName" >IMO number</label>
							<input type="text" class="form-control" id="txtIMONo" name="txtIMONo" placeholder="IMO number" value="<?php echo $obj->getFun14();?>" autocomplete="off">							
							<input type="hidden" id="uid" name="uid" value="<?php echo $id;?>" >
								
						</div>
                    
                        <div class="form-group" style="width:49%; float:left;padding-left:1%;">
							<label for="txtVName" >Vessel Name</label>
							<input type="text" class="form-control" id="txtVName" name="txtVName" readonly placeholder="Vessel Name" value="<?php echo $obj->getFun2();?>" autocomplete="off">                        </div>
                            
                        <div class="form-group" style="width:49%;  float:left;">
							<label for="selVType">Vessel Type</label>
							<select  name="selVType" class="form-control" id="selVType" >
							<?php 
							$obj->getVesselTypeList();
							?>
							</select>
						</div>
                        
                        <div class="form-group" style="width:49%; float:left;padding-left:1%;">
							<label for="txtYBDate" >Year Built</label>
							<input type="text" class="form-control" id="txtYBDate" name="txtYBDate" placeholder="yyyy" autocomplete="off" value="<?php echo $obj->getFun7();?>">                        </div>
                            
						
						<div class="form-group" style="width:49%; float:left;">
							<label for="selHullType" >Flag</label>
							<select  name="selFlag" class="form-control" id="selFlag" >
							<?php 
							$obj->getCountryNameList();
							?>
							</select>                                         
						</div>
						<div class="form-group" style="width:49%; padding-left:1%; float:left;">
							<label for="txtDWT">Summer DWAT (MT)</label>
							<input type="text" class="form-control" id="txtDWT" name="txtDWT" placeholder="Summer DWAT (MT)" value="<?php echo $obj->getFun6();?>" autocomplete="off">
						</div>
                        
                        
                        <div class="form-group" style="width:49%; float:left;">
							<label for="txtDWT">Summer Draft (M)</label>
							<input type="text" class="form-control" id="txtDraft" name="txtDraft" placeholder="Summer Draft (M)" value="<?php echo $obj->getFun11();?>" autocomplete="off">
						</div>
						
						
						
						
						<div class="form-group" style="width:49%; padding-left:1%; float:left;">
							<label for="txtCoating" >Loa (M)</label>
							<input type="text" class="form-control" id="txtLOA" name="txtLOA" placeholder="Loa (M)" value="<?php echo $obj->getFun16();?>" autocomplete="off">                                         
						</div>
						<div class="form-group" style="width:49%;  float:left;">
							<label for="txtOwner">Extreme Breadth(M)</label>
							<input type="text" class="form-control" id="txtEX_Breadth" name="txtEX_Breadth" placeholder="Extreme Breadth(M)"  value="<?php echo $obj->getFun17();?>" autocomplete="off">							  
						</div>
						
						<div class="form-group" style="width:49%; padding-left:1%; float:left;">
							<label for="txtDraft" >Grt/Nrt</label>
							<input type="text" class="form-control" id="txtGRT" name="txtGRT" placeholder="Grt/Nrt" value="<?php echo $obj->getFun18();?>" autocomplete="off">                                         
						</div>
						<div class="form-group" style="width:49%; float:left;">
							<label for="txtCraneSize">Grain(M³)</label>
							<input type="text" class="form-control" id="txtGrain" name="txtGrain" placeholder="Grain(M³)" value="<?php echo $obj->getFun19();?>" autocomplete="off">
						</div>
						
						<div class="form-group" style="width:49%;  padding-left:1%;float:left;">
							<label for="txtGrabSize" >Bale(M³)</label>
							<input type="text" class="form-control" id="txtBale" name="txtBale" placeholder="Bale(M³)" value="<?php echo $obj->getFun20();?>" autocomplete="off">                                         
						</div>
						<div class="form-group" style="width:49%; float:left;">
							<label for="txtIMO">No. Of Holds</label>
							<input type="text" class="form-control" id="txtNOH" name="txtNOH" placeholder="No. Of Holds" value="<?php echo $obj->getFun21();?>" autocomplete="off">
						</div>
						
						<div class="form-group" style="width:49%;  padding-left:1%;float:left;">
							<label for="txtVesselCode" >No. Of Hatches</label>
							<input type="text" class="form-control" id="txtNOHA" name="txtNOHA" placeholder="No. Of Hatches" value="<?php echo $obj->getFun22();?>" autocomplete="off">                                         
						</div>
						<div class="form-group" style="width:49%;float:left;">
							<label for="txtVesselGroup">Hatch Sizes (Meters)</label>
							<input type="text" class="form-control" id="txtHatch_Size" name="txtHatch_Size" placeholder="Hatch Sizes (Meters)" value="<?php echo $obj->getFun23();?>" autocomplete="off">
						</div>
						
						<div class="form-group" style="width:49%; padding-left:1%;float:left;">
							<label for="txtVesselGroup">Cargo Gear</label>
							<input type="text" class="form-control" id="txtCargo_Gear" name="txtCargo_Gear" placeholder="Cargo Gear" value="<?php echo $obj->getFun24();?>" autocomplete="off">
						</div>
						
						<div class="form-group" style="width:49%;float:left;">
							<label for="txtVesselGroup">Crane Size</label>
							<input type="text" class="form-control" id="txtCraneSize" name="txtCraneSize" autocomplete="off"  value="<?php echo $obj->getFun12();?>" placeholder="Crane Size">
						</div>
						
						<div class="form-group" style="width:49%; padding-left:1%;float:left;">
							<label for="txtVesselGroup">Grab Size</label>
							<input type="text" class="form-control" id="txtGrabSize" name="txtGrabSize" autocomplete="off" value="<?php echo $obj->getFun13();?>" placeholder="Grab Size">
						</div>
						
						<div class="form-group" style="width:49%;float:left;">
							<label for="txtVesselGroup">Owners P & I</label>
							<input type="text" class="form-control" id="txtPI" name="txtPI" placeholder="Owners P & I" value="<?php echo $obj->getFun25();?>" autocomplete="off">
						</div>
                        
                        <div class="form-group" style="width:49%; padding-left:1%;float:left;">
							<label for="txtVesselGroup">Classification Society</label>
							<select  name="selCLASS_SOC" class="form-control" id="selCLASS_SOC" >
							<?php 
                            $obj->getClaSocList();
                            ?>
                            </select>
						</div>
                        
                         <div class="form-group" style="width:49%;float:left;">
							<label for="txtVesselGroup">Business Type</label>
							<select  name="selBType" class="form-control" id="selBType" >
							<?php 
                            $obj->getBusinessTypeList();
                            ?>
                            </select>
						</div>
                        
                        <div class="form-group" style="width:49%;float:left;padding-left:1%;">
							<label for="txtVesselGroup">Remarks</label>
							<textarea name="txtRemarks" id="txtRemarks" class="form-control" cols="42" rows="4"><?php echo $obj->getFun27();?></textarea>
						</div>
						<div style="clear:both;"></div>
					</div><!-- /.box-body -->

					<div class="box-footer" align="center">
						<button type="submit" class="btn btn-primary btn-flat">Submit</button><input type="hidden" name="action" value="submit" />
					</div>
				</form>
			</div>
			
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>

<script type="text/javascript">
$(document).ready(function(){ 

$("#selVType").val(<?php echo $obj->getFun3(); ?>);
$("#selHullType").val(<?php echo $obj->getFun5(); ?>);
$("#selBType").val(<?php echo $obj->getFun10(); ?>);
$('#selFlag').val(<?php echo $obj->getFun15(); ?>);
$('#selCLASS_SOC').val(<?php echo $obj->getFun26(); ?>);

$('#txtRemarks').autosize({append: "\n"});
$("#txtIMO,#txtDWT,#txtIMONo,#txtDraft,#txtLOA,#txtEX_Breadth,#txtYBDate").numeric();

$("#txtYBDate").datepicker({
	format: " yyyy", 
	viewMode: "years", 
	minViewMode: "years"
	}).on('changeDate', function(e){
    $(this).datepicker('hide');
});


$("#frm1").validate({
	rules: {
		//txtIMONo:{required: true,remote:'options.php?id=5?uid=<?php echo $id;?>'},
		//txtVName:{required: true,remote:'options.php?id=3'},
		selVType:"required",
		txtDWT:"required",
		txtYBDate:"required",
		selFlag: "required",
		txtDWT: "required",
		txtDraft: "required"
		},
	messages: {
		//txtIMONo:"*",
		//txtVName:"*",
		selVType:"*",
		txtDWT:"*",
		txtYBDate:"*",
		selFlag: "*",
		txtDWT: "*",
		txtDraft: "*"
		},
	submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
});
</script>
    </body>
</html>