<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->changeVendorMasterStatusRecords();
	header('Location : ./addcoa.php.php?msg='.$msg);
 }

$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(4); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-truck"></i>&nbsp;Contract of Affreightment&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Contract of Affreightment</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> COA added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating COA.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
				<h3 style=" text-align:center;">Contract of Affreightment List</h3>
				<div align="right"><a href="addcoa.php" title="Add New"><button class="btn btn-info btn-flat">Add New</button></a>&nbsp;&nbsp;&nbsp;&nbsp;</div>
				<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                
                <div class="row invoice-info" style="margin-left:10px;margin-right:10px;">
                        <div class="col-sm-3 invoice-col">
                            <address>
                               <select  name="selActive" class="form-control" id="selActive" >
								<?php 
                                $obj->getActiveOrCancelled();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                            <address>
                                <input type="text" name="txtCOADate" id="txtCOADate" class="form-control" value="" placeholder="COA CP Date"  />
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-3 invoice-col">
                            <address>
                               <select  name="selOwner" class="form-control" id="selOwner">
								<?php 
                                $obj->getVendorListNewForCOA(11);
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                            <address>
                              <button type="button" class="btn btn-primary btn-flat" onclick="getCOAData();" >Load</button>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
				<div style="height:10px;">
				<input name="txttblid" id="txttblid" class="input" size="5" type="hidden" value="" />
				<input name="txttblstatus" id="txttblstatus" class="input" size="5" type="hidden" value="" />
				<input type="hidden" name="action" value="submit" />
				</div>				
				<?php $obj->displayCOAList();?>
				</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
 <script type="text/javascript">
 $(document).ready(function(){
 var i=1;
 $("[id^=myonoffswitch_]").each(function () {
        document.getElementById("myonoffswitch_"+i).setAttribute("type", "checkbox");
		i++;
    });
$('#txtCOADate').datepicker({
    format: 'dd-mm-yyyy'
});
});
$(function() {
	$("#coa_list").dataTable();

});
function getDelete(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently?', 'Confirmation', function(r) {
	if(r){ 
		$("#delId").val(var1);
		document.frm1.submit();
	}
	else{return false;}
	});
}

function getCOAData()
{
	$("#tbody").html("");
	$("#tbody").html('<tr><td height="100" colspan="8" align="center" ><img src="../../img/loading.gif" /></td></tr>');
	$.post("options.php?id=5",{selActive:""+$("#selActive").val()+"",txtCOADate:""+$("#txtCOADate").val()+"",selOwner:""+$("#selOwner").val()+""}, function(html) {
	$("#tbody").html("");
	$("#tbody").html(html);
	$("#coa_list").dataTable();
	});
	
	$("#ExcelId").val(1);
}

function removeDate()
{
	$("#txtCOADate").val("");
	$("#coadate").hide();
	$("#txtCOADate").removeClass("text hasDatepicker");
	$("#txtCOADate").addClass("text");
}

function getExcel()
{
	if($("#ExcelId").val() == 1)
	{ 
		location.href='allExcel.php?id=16&selActive='+$("#selActive").val()+'&txtCOADate='+$("#txtCOADate").val()+'&selOwner='+$("#selOwner").val();
	}
	else
	{
		location.href='allExcel.php?id=16';
	}
}
</script>
		
</body>
</html>