<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->updatePortInformationRecords();
	header('Location : ./port_information.php?msg='.$msg);
 }
$obj->viewPortInformationRecords($id);
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Port Information</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="port_information.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             UPDATE PORT INFORMATION
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                    
                    <div class="col-sm-12 invoice-col">
                           Cargo Name
                            <address>
                               <?php echo $obj->getMaterialCodeDesBasedOnId($obj->getFun15());?>
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                          Port Name
                            <address>
                               <?php echo $obj->getPortNameBasedOnCode($obj->getFun1());?>
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                          Terminal
                            <address>
                               <?php echo $obj->getTerminalNameBasedOnID($obj->getFun19());?>
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Max Draft (M)
                            <address>
                               <input type="text" name="txtMaxDraft" id="txtMaxDraft" class="form-control" placeholder="Max Draft (M)" autocomplete="off" value="<?php echo $obj->getFun2();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Max LOA(M)
                            <address>
                               <input type="text" name="txtMaxLOA" id="txtMaxLOA" class="form-control" placeholder="Max LOA(M)" autocomplete="off" value="<?php echo $obj->getFun3();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                       <div class="col-sm-12 invoice-col">
                           Max Beam(M)
                            <address>
                               <input type="text" name="txtMaxBeam" id="txtMaxBeam" class="form-control" placeholder="Max Beam(M)" autocomplete="off" value="<?php echo $obj->getFun4();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        
                        <div class="col-sm-12 invoice-col">
                           Air Draft
                            <address>
                               <input type="text" name="txtMaxHeight" id="txtMaxHeight" class="form-control" placeholder="Air Draft" autocomplete="off" value="<?php echo $obj->getFun5();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Loading Method
                            <address>
                               <textarea class="form-control areasize" name="txtLMethod" id="txtLMethod" ><?php echo $obj->getFun6();?></textarea>
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Displacement (MT)
                            <address>
                               <input type="text" name="txtDisplacement" id="txtDisplacement" class="form-control" placeholder="Displacement (MT)" autocomplete="off" value="<?php echo $obj->getFun16();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Crane out reach
                            <address>
                               <input type="text" name="txtCOR" id="txtCOR" class="form-control" placeholder="Crane out reach" autocomplete="off" value="<?php echo $obj->getFun17();?>" >
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-12 invoice-col">
                           Hatch Dimension
                            <address>
                               <input type="text" name="txtHDimension" id="txtHDimension" class="form-control" placeholder="Hatch Dimension" autocomplete="off" value="<?php echo $obj->getFun18();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Loading Rate ( MT/Day)
                            <address>
                               <input type="text" name="txtLRate_day" id="txtLRate_day" class="form-control" placeholder="Loading Rate ( MT/Day)" autocomplete="off" value="<?php echo $obj->getFun7();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                          Disch Rate ( MT/Day)
                            <address>
                               <input type="text" name="txtDRate_day" id="txtDRate_day" class="form-control" placeholder="Disch Rate ( MT/Day)" autocomplete="off" value="<?php echo $obj->getFun8();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                          DWT ( MT )
                            <address>
                               <input type="text" name="txtDWT" id="txtDWT" class="form-control" placeholder="DWT ( MT )" autocomplete="off" value="<?php echo $obj->getFun11();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                          Height of Conveyor (M)
                            <address>
                               <input type="text" name="txtDCTS" id="txtDCTS" class="form-control" placeholder="Height of Conveyor (M)" autocomplete="off" value="<?php echo $obj->getFun12();?>" >
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                         Loader (Y/N)
                            <address>
                               <select name="selLoader" id="selLoader" class="form-control">
                               <?php
								$_REQUEST['selLoader'] = $obj->getFun13();
								$obj->getLoader();
								?>
                               </select>
                               
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                           Remarks
                            <address>
                               <textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" ><?php echo $obj->getFun14();?></textarea>
                               
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                  
                    
				<div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" >Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
				</div>
         
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#txtMaxDraft,#txtMaxLOA,#txtMaxBeam,#txtMaxHeight,#txtLRate_day,#txtDRate_day,#txtLRate_hr ,#txtDRate_hr ,#txtDWT ,#txtDCTS").numeric();
$('.areasize').autosize({append: "\n"});
$("#frm1").validate({
	rules: {
		txtMaxDraft:{required :true , number: true},
		txtMaxLOA:{number: true},
		txtMaxBeam:{number: true},
		txtMaxHeight:{number: true},
		txtLRate_day:{number: true},
		txtDRate_day:{number: true},
		txtDWT:{number: true},
		txtDCTS:{number: true},
		selLoader:{required :true , number: true}
		},
	messages: {
		txtMaxDraft:{required :"*" , number: "Only Numeric."},
		txtMaxLOA:{number: "Only Numeric."},
		txtMaxBeam:{number: "Only Numeric."},
		txtMaxHeight:{number: "Only Numeric."},
		txtLRate_day:{number: "Only Numeric."},
		txtDRate_day:{number: "Only Numeric."},
		txtLRate_hr:{number: "Only Numeric."},
		txtDRate_hr:{number: "Only Numeric."},
		txtDWT:{number: "Only Numeric."},
		txtDCTS:{number: "Only Numeric."},
		selLoader:{required :"*" , number: "Only Numeric."}
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

});

</script>
    </body>
</html>