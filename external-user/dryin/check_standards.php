<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$mappingid = $_REQUEST['mappingid'];
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertStandardsDetails();
	header('Location : ./nomination_at_glance.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Nominations at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="nomination_at_glance.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             STANDARDS
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row">
                      <div class="col-xs-12">
                         <div class="box box-primary">
						    <div class="box-body no-padding">
							  <table class="table table-striped">
							  <?php 
                                $sql = "select * from mapping_standards where MAPPINGID='".$mappingid."'";
                                $res = mysql_query($sql);
                                $rec = mysql_num_rows($res);
                                if($rec == 0)
                                {
                                    $sql1 = "select * from approvals_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1 order by APPROVALID"; 
                                    $res1 = mysql_query($sql1);
                                    $rec1 = mysql_num_rows($res1);
                                    $i=1;
                                ?>
                                    <thead>
                                    <tr>
                                    <th width="41%" align="left" valign="middle">Approval Type<input type="hidden" name="txtRec" size="5" value="<?php echo $rec1;?>" /></th>
                                    <th width="8%" align="center" valign="middle">Approved</th>
                                    <th width="12%" align="center" valign="middle">Date</th>
                                    <th width="19%" align="center" valign="middle">Remark</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php while($rows1 = mysql_fetch_assoc($res1)){?>
                                    <tr>
                                    <td align="left" valign="middle"><?php echo $rows1['NAME'];?></td>
                                    <td align="center" valign="middle"><input type="checkbox" name="checkApprovedbox_<?php echo $i;?>" id="checkApprovedbox_<?php echo $i;?>" value="<?php echo $rows1['APPROVALID'];?>" onclick="getEnabled(<?php echo $i;?>);" /><input type="hidden" class="form-control" name="txtApprovalid_<?php echo $i;?>" id="txtApprovalid_<?php echo $i;?>" size="8" value="<?php echo $rows1['APPROVALID'];?>"/></td>
                                    <td align="center" valign="middle"><input type="text" class="form-control" name="txtDate_<?php echo $i;?>" id="txtDate_<?php echo $i;?>" size="8" disabled="disabled"/></td>
                                    <td align="center" valign="middle"><textarea class="form-control areasize" name="txtRemarks_<?php echo $i;?>" id="txtRemarks_<?php echo $i;?>" ></textarea></td>
                                    </tr>	  
                                    <?php $i++;}?>
                                    </tbody>
                                <?php }else{$j=1;?>
                                    <thead>
                                    <tr>
                                    <th width="30%" align="left" valign="middle" >Approval Type<input type="hidden" name="txtRec" size="5" value="<?php echo $rec;?>" /></th>
                                    <th width="8%" align="center" valign="middle" >Approved</th>
                                    <th width="15%" align="center" valign="middle" >Date</th>
                                    <th width="35%" align="center" valign="middle" >Remark</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php while($rows = mysql_fetch_assoc($res)){
                                    if($rows['APPROVAL_STATUS'] == 0){?>
                                    <tr>
                                    <td align="left" valign="middle"><?php echo $obj->getApprovalNameBasedOnID($rows['APPROVALID']);?></td>
                                    <td align="center" valign="middle"><input type="checkbox" name="checkApprovedbox_<?php echo $j;?>" id="checkApprovedbox_<?php echo $j;?>" value="<?php echo $rows['APPROVALID'];?>" onclick="getEnabled(<?php echo $j;?>);" /><input type="hidden" name="txtApprovalid_<?php echo $j;?>" id="txtApprovalid_<?php echo $j;?>" value="<?php echo $rows['APPROVALID'];?>"/></td>
                                    <td align="center" valign="middle">
                                    <?php if(date("d-m-Y",strtotime($rows['DATE'])) == "01-01-1970"){$date = "";}else{$date = date("d-m-Y",strtotime($rows['DATE']));}?>
                                    <input type="text" class="areasize" name="txtDate_<?php echo $j;?>" id="txtDate_<?php echo $j;?>" disabled="disabled" value="<?php echo $date;?>"/>
                                    </td>
                                    <td align="center" valign="middle" class="input-text" style="font-size:10px;"><textarea class="form-control areasize" name="txtRemarks_<?php echo $j;?>" id="txtRemarks_<?php echo $j;?>"><?php echo $rows['REMARKS'];?></textarea></td>			
                                    <?php }else{?>
                                    <tr>
                                    <td align="left" valign="middle"><?php echo $obj->getApprovalNameBasedOnID($rows['APPROVALID']);?></td>
                                    <td align="center" valign="middle"><input type="checkbox" name="checkApprovedbox_<?php echo $j;?>" id="checkApprovedbox_<?php echo $j;?>" checked value="<?php echo $rows['APPROVALID'];?>" onclick="getEnabled(<?php echo $j;?>);" /><input type="hidden" name="txtApprovalid_<?php echo $j;?>" id="txtApprovalid_<?php echo $j;?>"  value="<?php echo $rows['APPROVALID'];?>"/></td>
                                    <td align="center" valign="middle">
                                    <?php if(date("d-m-Y",strtotime($rows['DATE'])) == "01-01-1970"){$date = "";}else{$date = date("d-m-Y",strtotime($rows['DATE']));}?>
                                    <input type="text" class="form-control" name="txtDate_<?php echo $j;?>" id="txtDate_<?php echo $j;?>" value="<?php echo $date;?>"/>
                                    </td>
                                    <td align="center" valign="middle" ><textarea class="form-control areasize" name="txtRemarks_<?php echo $j;?>" id="txtRemarks_<?php echo $j;?>" ><?php echo $rows['REMARKS'];?></textarea></td>	  
                                    <?php }$j++;}?>
                                    </tbody>
                                <?php }?>
                              </table>
                            </div>
                         </div>
                      </div>
                   </div>
               
              
				<div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" >Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
				</div>
         
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#txtQty,#txtLINo,#txtTolerance").numeric();
$("[id^=txtDate_]").datepicker({
    format: 'dd-mm-yyyy'
});

$(".areasize").autosize({append: "\n"});

});


function getEnabled(var1)
{
	if ($("#checkApprovedbox_"+var1).attr('checked')) 
	{
		$("#txtDate_"+var1).removeAttr("disabled");
		$("#txtDate_"+var1).focus();
	}
	else
	{
		$("#txtDate_"+var1).val("");
		$("#txtDate_"+var1).attr("disabled","disabled");
	}
}

function getValidate()
{
	var arr = new Array(); var arr1 = new Array();
	var i=0;
	$('[id^=checkApprovedbox_]').each(function(index) {
	var j = i+1;
			if($(this).attr('checked'))
			{
				if($("#txtDate_"+j).val() == "")
				{
					arr[i] = j;					
				}
			}
			i++;
		});
	if(arr.length == 0)
	{
		jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
		if(r){ 
			document.frm1.submit();
		}
		else{return false;}
		});	
	}
	else 
	{
		jAlert('Please fill the Approved related values', 'Alert');
		return false;
	}
}

</script>
    </body>
</html>