<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertVesselOpenPositionDetails();
	header('Location : ./open_vessel_position.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Open Vessel Position</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="open_vessel_position.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             ADD OPEN VESSEL POSITION    
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           Vessel Name
                            <address>
                               <select  name="selVName" class="form-control" id="selVName" >
								<?php 
                                $obj->getVesselTypeListMemberWise();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                           Business Source Entity
                            <address>
                                <select  name="selVendor" class="form-control" id="selVendor" >
									<?php 
                                    $obj->getVendorListNew();
                                    ?>
                                    </select>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Port Open
                            <address>
                               <select  name="selPort" onChange="getZoneData();" class="form-control" id="selPort">
								<?php 
                                $obj->getPortListNew();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Zone Open
                            <address>
                               <select  name="selZone"  class="form-control" id="selZone">
								<?php 
                                $obj->getZoneList();
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
                        
						
					</div>
                    
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                          COA / Spot
                            <address>
                                <select  name="selCOASpot" class="form-control" id="selCOASpot" onChange="getShow();">
								<?php 
                                $obj->getCOASpotList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                          Broker
                            <address>
                                <select  name="selBroker" class="form-control" id="selBroker">
								<?php 
								$obj->getVendorListNewForCOA(12);
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                       
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Vessel Category
                            <address>
                               <select  name="selVCType" class="form-control" id="selVCType">
								<?php 
                                $obj->getVesselCategoryList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Owner
                            <address>
                                <select  name="selOwner" class="form-control" id="selOwner">
								<?php 
                                $obj->getVendorListNewForCOA(11);
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    
                    
                    
                  <div class="row invoice-info" id="tr_coa" style="display:none;">
                        <div class="col-sm-6 invoice-col">
                          COA Number
                            <address>
                               <select  name="selCOA" class="form-control" id="selCOA" onChange="getTotalShipments();">
								<?php 
                                $obj->getCOAList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            <address>
                             &nbsp;
                             </address>
                        </div><!-- /.col -->
                        
					</div>
                    
                    
                    
                    <div class="row invoice-info" id="tr_coa1" style="display:none;">
                        <div class="col-sm-6 invoice-col">
                           Number of Lift
                            <address>
                               <input type="text" name="txtNoLift" id="txtNoLift" class="form-control"  placeholder=" Number of Lift" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Total No. of Shipments
                            <address>
                                <strong><span id="ttl_shipment" ></span></strong>
                             </address>
                        </div><!-- /.col -->
                        
                    </div>
                    
                   
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           Laycan Start
                            <address>
                               <input type="text" name="txtFDate" id="txtFDate" class="form-control"  placeholder="Laycan Start" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Laycan Finish
                            <address>
                                <input type="text" name="txtTDate" id="txtTDate" class="form-control"  placeholder="Laycan Finish" autocomplete="off" value=""/>
                             </address>
                        </div><!-- /.col -->
                       
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           ETA During Fixture
                            <address>
                               <input type="text" name="txtETADate" id="txtETADate" class="form-control"  placeholder="ETA During Fixture" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                           CP Date
                            <address>
                               <input type="text" name="txtCPDate" id="txtCPDate" class="form-control"  placeholder="CP Date" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        
					</div>
                    
                    
                   
                    
                     <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                            Remarks
                            <address>
                               <textarea class="form-control areasize" name="txtNotes" id="txtNotes" rows="3" placeholder="Remarks ..." ></textarea>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                           TC Description
                            <address>
                               <textarea class="form-control areasize" name="txtDirection" id="txtDirection" rows="3" placeholder="TC Description ..." ></textarea>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    
                     <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                             &nbsp;
                                <address>
                                    <div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Inspector's Report">
                                        <i class="fa fa-paperclip"></i> Attachment
                                        <input type="file" class="form-control" multiple name="attach_file" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                    </div>
                                </address>
                            </div><!-- /.col -->
                     </div>
				
				<div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
				</div>
         
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$(".areasize").autosize({append: "\n"});
$("#txtNoLift").numeric();
$('#txtTDate,#txtFDate,#txtETADate,#txtCPDate,#txtLayDate').datepicker({
    format: 'dd-mm-yyyy'
});
}); 

function getShow()
{
	if($("#selCOASpot").val() == 1 || $("#selCOASpot").val() == 3 || $("#selCOASpot").val() == 4)
	{
		$("#tr_coa,#tr_coa1").hide();
		$("#selOwner,#selCOA,#txtNoLift,#selBroker,#txtCPDate").val("");
		$("#ttl_shipment").html("");
	}
	else if($("#selCOASpot").val() == 2)
	{
		$("#selOwner,#selCOA,#txtNoLift,#selBroker,#txtCPDate").val("");
		$("#ttl_shipment").html("");
		$("#tr_coa,#tr_coa1").show();
	}
}


function getZoneData()
{
	$("#selZone").val("");
	$.post("options.php?id=14",{selPort:""+$("#selPort").val()+""}, function(html) {
	$("#selZone").val(html);
	});
}

function getTotalShipments()
{
	if($("#selCOA").val() != "")
	{
		$("#txtNoLift").val("");
		var coa_list = <?php echo $obj->getCOAMasterDataJson();?>;
		$.each(coa_list[$("#selCOA").val()], function(index, array) {
				$("#ttl_shipment").html("");
				$("#selBroker,#selOwner,#txtCPDate").val("");
				$("#ttl_shipment").html(array['NO_OF_SHIPMENT']);
				$("#selBroker").val(array['BROKER']);
				$("#selOwner").val(array['OWNER']);
				$("#txtCPDate").val(array['COA_DATE']);
			});
	}
	else
	{
		$("#txtNoLift").val("");
		$("#ttl_shipment").html("");
		$("#selBroker,#selOwner,#txtCPDate").val("");
	}
}

</script>
    </body>
</html>