<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$id = $_REQUEST['id'];
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->cancelCOADetails();
	header('Location : ./coa_list.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
$obj->viewCOARecords($id);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(4); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-truck"></i>&nbsp;Contract of Affreightment&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Contract of Affreightment</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="coa_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             COA DETAILS  
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Cargo/Material Type
                            <address>
                              <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getCargoTypeNameBasedOnID($obj->getFun25());?></strong>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Vessel Category
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getVesselCategoryTypeBasedOnId($obj->getFun26());?></strong>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            COA Number
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getFun1();?></strong>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            COA CP Date
                            <address>
                                <strong>&nbsp;&nbsp;&nbsp;  <?php echo date("d-M-Y",strtotime($obj->getFun2()));?></strong>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Cargo Qty(MT)
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getFun3();?></strong>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Owner
                            <address>
                                <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getVendorListNewBasedOnID($obj->getFun4())." ( ".$obj->getFun4()." )";?></strong>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Broker
                            <address>
                                <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getVendorListNewBasedOnID($obj->getFun5())." ( ".$obj->getFun5()." )";?></strong>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Charterer
                            <address>
                                <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getVendorListNewBasedOnID($obj->getFun6())." ( ".$obj->getFun6()." )";?></strong>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            No. of Shipments
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getFun7();?></strong>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Shipment Qty(MT)
                            <address>
                                <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getFun8();?></strong>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Tolerance ( % )
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getFun9();?></strong>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                            Cancellation Remark
                            <address>
                               <textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" rows="3" placeholder="Remarks ..." ></textarea>
                            </address>
                        </div><!-- /.col -->
                      
					</div>
                    
				
				<div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
				</div>
         
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 

$(".areasize").autosize({append: "\n"});

});


function getValidate()
{
	if($("#txtRemarks").val() != "")
	{
	jConfirm('Are you sure to cancel this COA permanently ?', 'Confirmation', function(r) {
		if(r){ 
			document.frm1.submit();			
		}
		else{return false;}
		});	
	}
	else
	{
		jAlert('Please fill cancellation remark.', 'Alert');
		return false;
	}
}

</script>
    </body>
</html>