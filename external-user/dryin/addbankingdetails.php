<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertBankingDetails();
	header('Location : ./banking_details.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Banking Details</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="banking_details.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              BANKING DETAILS       
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Name Of Beneficiary
                            <address>
                               <input type="text" name="txtNOB" id="txtNOB" class="form-control" placeholder="Name Of Beneficiary" autocomplete="off" value="" >
                               
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Address
                            <address>
                                 <textarea class="form-control areasize" name="txtAdd" id="txtAdd" rows="3" placeholder="Address ..." ></textarea>
                            </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                            Beneficiary A/C No.
                            <address>
                               <input type="text" name="txtBAcc_No" id="txtBAcc_No" class="form-control"  placeholder="Beneficiary A/C No." autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Beneficiary Bank
                            <address>
                               <input type="text" name="txtBBank" id="txtBBank" class="form-control"  placeholder="Beneficiary Bank" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Beneficiary Bank Address
                            <address>
                                <textarea class="form-control areasize" name="txtBAdd" id="txtBAdd" rows="3" placeholder="Beneficiary Bank Address ..." ></textarea>
                            </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Beneficiary Bank Swift Code
                            <address>
                               <input type="text" name="txtBBank_SCode" id="txtBBank_SCode" class="form-control" autocomplete="off" placeholder="Beneficiary Bank Swift Code" value=""/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          IBAN No.
                            <address>
                                <input type="text" name="txtIBAN_No" id="txtIBAN_No" class="form-control" autocomplete="off" placeholder="IBAN No." value=""/>
                            </address>
                        </div><!-- /.col -->
                        
					</div>
                    
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
								<div class="box-body no-padding">
								<table class="table table-striped">
								  
									<tbody id="tbd">
                                      <tr>
										<td align="left" class="input-text" width="40%" >FED ABA</td><td width="1%" align="left" class="text" valign="top">:</td><td align="left" class="input-text" width="40%"><input type="text" name="txtFed_Aba" id="txtFed_Aba" class="form-control" autocomplete="off" placeholder="FED ABA" value=""/></td><td align="left" class="input-text" width="19%"><button type="button" class="btn btn-primary btn-flat" onClick="addUI_Row();">Add</button>
                                    <input type="hidden" class="input" name="txtCID" id="txtCID" value="0" size="5" /></td>
                                        
                                      </tr>
									</tbody>
								</table>
								</div><!-- /.box-body -->
							</div>                          
                        </div><!-- /.col -->
                    </div> 
                    
                    
                   <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             CORRESPONDENT DETAILS             
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    
                   
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Correspondent Bank Name
                            <address>
                                <input type="text" name="txtCBN" id="txtCBN" class="form-control" autocomplete="off" placeholder="Correspondent Bank Name" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Swift Code
                            <address>
                                <input type="text" name="txtSCN" id="txtSCN" class="form-control" autocomplete="off" placeholder="Swift Code" value=""/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
								<div class="box-body no-padding">
								<table class="table table-striped">
								  
									<tbody id="tbd_1">
                                      <tr>
										<td align="left" class="input-text" width="40%" >Reference</td><td width="1%" align="left" class="text" valign="top">:</td><td align="left" class="input-text" width="40%"><input type="text" name="txtRef" id="txtRef" class="form-control" autocomplete="off" placeholder="Reference" value=""/></td><td align="left" class="input-text" width="19%"><button type="button" class="btn btn-primary btn-flat" onClick="addUI_Row_1();">Add</button>
                                    <input type="hidden" class="input" name="txtBID" id="txtBID" value="0" size="5" /></td>
                                        
                                      </tr>
									</tbody>
								</table>
								</div><!-- /.box-body -->
							</div>                          
                        </div><!-- /.col -->
                    </div> 
                    
                    
				
				<div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
				</div>
         
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$(".areasize").autosize({append: "\n"});
$("#frm1").validate({
	rules: {
	txtNOB:"required"
	},
messages: {	
	txtNOB:"*"
	},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

});


function addUI_Row()
{
	var id = $("#txtCID").val();
	
	if(id == 0)
	{
		id  = (id - 1 )+ 2;
		
		$('<tr><td width="16%" align="left"  valign="top"><input type="text" name="txtBDName_'+id+'" id="txtBDName_'+id+'" class="form-control" autocomplete="off" value=""/></td><td width="1%" align="left" class="text" valign="top">:</td><td align="left" valign="top"><input type="text" name="txtBDValue_'+id+'" id="txtBDValue_'+id+'" class="form-control" size="18" autocomplete="off" value=""/></td><td align="left" class="input-text" width="19%"></td></tr>').appendTo("#tbd");
		$("#txtCID").val(id);
	}
	else
	{
		if($("#txtBDName_"+id).val() != "" && $("#txtBDValue_"+id).val() != "")
		{
			id  = (id - 1 )+ 2;
			
			$('<tr><td width="16%" align="left"  valign="top"><input type="text" name="txtBDName_'+id+'" id="txtBDName_'+id+'" class="form-control" autocomplete="off" value=""/></td><td width="1%" align="left" class="text" valign="top">:</td><td align="left" valign="top"><input type="text" name="txtBDValue_'+id+'" id="txtBDValue_'+id+'" class="form-control" size="18" autocomplete="off" value=""/></td><td align="left" class="input-text" width="19%"></td></tr>').appendTo("#tbd");
		$("#txtCID").val(id);
		}
		else
		{
			jAlert('Please fill the above entries.', 'Alert');
		}
	}	
}

function addUI_Row_1()
{
	var id = $("#txtBID").val();
	
	if(id == 0)
	{
		id  = (id - 1 )+ 2;
		
		$('<tr><td width="16%" align="left" class="text"  valign="top"><input type="text" name="txtCDName_'+id+'" id="txtCDName_'+id+'" class="form-control" size="18" autocomplete="off" value="" autofocus="on" /></td><td width="1%" align="left" class="text" valign="top">:</td><td width="32%" align="left" class="input-text" valign="top"><input type="text" name="txtCDValue_'+id+'" id="txtCDValue_'+id+'" class="form-control" size="18" autocomplete="off" value=""/></td><td align="left" class="input-text" width="19%"></td></tr>').appendTo("#tbd_1");
		$("#txtBID").val(id);
	}
	else
	{
		if($("#txtCDName_"+id).val() != "" && $("#txtCDValue_"+id).val() != "")
		{
			id  = (id - 1 )+ 2;
			
			$('<tr><td width="16%" align="left" class="text"  valign="top"><input type="text" name="txtCDName_'+id+'" id="txtCDName_'+id+'" class="form-control" size="18" autocomplete="off" value="" autofocus="on" /></td><td width="1%" align="left" class="text" valign="top">:</td><td width="32%" align="left" class="input-text" valign="top"><input type="text" name="txtCDValue_'+id+'" id="txtCDValue_'+id+'" class="form-control" size="18" autocomplete="off" value=""/></td><td align="left" class="input-text" width="19%"></td></tr>').appendTo("#tbd_1");
		$("#txtBID").val(id);
		}
		else
		{
			jAlert('Please fill the above entries.', 'Alert');
		}
	}	
}

</script>
    </body>
</html>