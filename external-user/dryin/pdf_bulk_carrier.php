<?php
session_start();
require_once("../../includes/functions_internal_user_dryin.inc.php");
include_once("../../includes/fpdf.php");
$obj = new data();
$obj->funConnect();

@$id = $_REQUEST['id'];
@$id1 = $_REQUEST['id1'];
$_SESSION['id1'] = $_REQUEST['id1'];

class PDF extends FPDF
{
	function Header()
	{
		$obj = new data();
		$obj->funConnect();
		
		$this->SetFillColor(255,255,255);
		$image_path1 = '../../img/logo1.jpg';
		$this->Image($image_path1,87,3,35,25);
		$this->SetFont('Arial','B',10);
		$this->SetTextColor(220, 99, 30);
		if($_SESSION['id1'] != 1)
		{
			$this->SetXY(60,30);
			$this->Cell(0,6,"BALTIC EXCHANGE DRY CARGO QUESTIONNAIRE",0,1,'L',1,'1');	
		}
		else
		{
			$this->SetXY(140,20);
			$this->Cell(0,6,"CHAPTER 8",0,1,'R',1,'1');
		}
		$this->SetFont('Arial','',10);
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(177,175,175);
		$this->Cell(190,1,"_________________________________________________________________________________________________",0,1,'L','1');
		$this->Ln(5);
	}
	
	function Footer()
	{
		$this->SetY(-20);
		//Select Arial italic 8
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(177,175,175);
		$this->SetFont('Arial','',10);
		$this->Cell(190,1,"_________________________________________________________________________________________________",0,1,'L','1');
		$this->Ln(3);
		$this->SetFont('Arial','',6);
		
		if($_SESSION['company'] == 1)
		{
			$this->Cell(190,4,"LSS OCEAN TRANSPORT LTD.",0,1,'C');
			$this->Cell(190,4,"Unit # 2708, Jumeirah Business Center 5, Plot No. W1, Jumeirah Lake Towers Area, Dubai, Tel +97 14 361 1958",0,1,'C');
			$this->Cell(190,4,"E-mail: libraship@libraship.com  Website:www.libraship.com",0,1,'C');
			$this->Cell(190,4,"",0,1,'C');
		}
		else if($_SESSION['company'] == 2)
		{
			$this->Cell(190,4,"ADANI SHIPPING PTE LTD.",0,1,'C');
			$this->Cell(190,4,"Unit # 2705, Jumeirah Business Center 5, Plot No. W1, Jumeirah Lake Towers Area, Dubai, Tel +97 14 361 1958",0,1,'C');
			$this->Cell(190,4,"E-mail: libraship@libraship.com  Website:www.libraship.com",0,1,'C');
			$this->Cell(190,4,"",0,1,'C');
		}
	}
	
}

$pdf = new PDF();
$pdf->AddPage('P');
$pdf->SetDisplayMode('real','single');
$pdf->SetTitle('Vessel Particular Pdf');
$pdf->SetAuthor('Seven Oceans Holdings');
$pdf->SetSubject('Vessel Particular Pdf');
$pdf->SetKeywords('Designed and Developed By Seven Oceans Holdings pvt. Ltd.');

$pdf->Ln(2);

if($id1 != 1)
{
$arr1 = $arr2 = $arr3 = array();	
$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("1.","GENERAL");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('1.1','VESSEL�S NAME',$obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME"));
$arr3[2]=array('1.2','VESSEL�S PREVIOUS NAME(S)',$obj->getVesselParticularData('VP_NAME','vessel_master_1',$_REQUEST['id']));

if(date("M d, Y",strtotime($obj->getVesselParticularData('DATE_OF_CHANGE','vessel_master_1',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$doc = '';
}
else
{
	$doc = date("M d, Y",strtotime($obj->getVesselParticularData('DATE_OF_CHANGE','vessel_master_1',$_REQUEST['id'])));
}

$arr3[3]=array('1.3','DATE(S) OF CHANGE',$doc);
$arr3[4]=array('1.4','FLAG', $obj->getCountryNameOnId($obj->getVesselParticularData('FLAG_ID','vessel_master_1',$_REQUEST['id'])));

if($obj->getVesselIMOData($_REQUEST['id'],"YEARBUILT") == '1970')
{
	$built = $obj->getCountryNameOnId($obj->getVesselParticularData('COUNTRY_ID','vessel_master_1',$_REQUEST['id']));
}
else
{
	$built = $obj->getVesselIMOData($_REQUEST['id'],"YEARBUILT").','.$obj->getCountryNameOnId($obj->getVesselParticularData('COUNTRY_ID','vessel_master_1',$_REQUEST['id']));
}

$arr3[5]=array('1.5','MONTH/YEAR AND WHERE BUILT',$built);
$arr3[6]=array('1.6','YARD NAME AND NUMBER',$obj->getVesselParticularData('YARD_NAME','vessel_master_1',$_REQUEST['id']));
$arr3[7]=array('1.7','CLASS REGISTRATION NUMBER',$obj->getVesselParticularData('CLASS_R_NO','vessel_master_1',$_REQUEST['id']));
$arr3[8]=array('1.8','OFFICIAL NUMBER',$obj->getVesselParticularData('OFFICIAL_NO','vessel_master_1',$_REQUEST['id']));
$arr3[9]=array('1.9','IMO NUMBER',$obj->getVesselParticularData('IMO_NO','vessel_master_1',$_REQUEST['id']));
$arr3[10]=array('1.10','PORT OF REGISTRY',$obj->getPortNameBasedOnID($obj->getVesselParticularData('PORT_ID','vessel_master_1',$_REQUEST['id'])));
$arr3[11]=array('1.11','OWNERS FULL STYLE AND CONTACT NUMBERS FOR  OPERATIONAL PURPOSES, IF APPROPRIATE',$obj->getVesselIMOData($_REQUEST['id'],"OWNER"));
$arr3[12]=array('1.12','MANAGERS FULL STYLE AND CONTACT NUMBERS FOR  OPERATIONAL PURPOSES, IF APPROPRIATE',$obj->getVesselParticularData('MANAGERS_FULL','vessel_master_1',$_REQUEST['id']));
$arr3[13]=array('1.13','IF CONTRACTING PARTY ARE DISPONENT OWNERS STATE :',$obj->getVesselParticularData('OWNERS_STATE','vessel_master_1',$_REQUEST['id']));
$arr3[14]=array('1.14','FULL STYLE AND CONTACT NUMBERS FOR OPERATIONAL PURPOSES',$obj->getVesselParticularData('OPERATION_PURPOSE','vessel_master_1',$_REQUEST['id']));
$arr3[15]=array('1.15','IF VESSEL ON TIME CHARTER OR BAREBOAT',$obj->getVesselParticularData('BAREBOAT','vessel_master_1',$_REQUEST['id']));

if(date("M d, Y",strtotime($obj->getVesselParticularData('DISPONENT_OWNER','vessel_master_1',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$d_owner = '';
}
else
{
	$d_owner = date("M d, Y",strtotime($obj->getVesselParticularData('DISPONENT_OWNER','vessel_master_1',$_REQUEST['id'])));
}

$arr3[16]=array('1.16','WHEN VESSEL DELIVERED TO DISPONENT OWNERS',$d_owner);

for($i=1;$i<17;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("2.","PARTICULARS OF VESSEL");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3=array('2.1','TYPE OF VESSEL',$obj->getVesselTypeBasedOnID($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_TYPE")));
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L','L','L','L');
$arr2=array(10,61,40,40,40);
$arr3=array('2.2','DEADWEIGHT ALL TOLD (METRIC TONS)','DWAT','DRAFT','TPC BASIS FULL DRAFT');
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L','L','L');
$arr2=array(10,61,40,40,40);
$arr3[1]=array('','SUMMER',$obj->getVesselIMOData($_REQUEST['id'],"DWT"),$obj->getVesselIMOData($_REQUEST['id'],"DRAFTM"),$obj->getVesselParticularData('SUMMER_3','vessel_master_1',$_REQUEST['id']));
$arr3[2]=array('','WINTER',$obj->getVesselParticularData('WINTER_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('WINTER_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('WINTER_3','vessel_master_1',$_REQUEST['id']));
$arr3[3]=array('','TROPICAL',$obj->getVesselParticularData('TROPICAL_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('TROPICAL_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('TROPICAL_3','vessel_master_1',$_REQUEST['id']));
$arr3[4]=array('','SUMMER TIMBER',$obj->getVesselParticularData('SUMMER_TIM_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('SUMMER_TIM_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('SUMMER_TIM_3','vessel_master_1',$_REQUEST['id']));
$arr3[5]=array('','WINTER TIMBER',$obj->getVesselParticularData('WINTER_TIM_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('WINTER_TIM_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('WINTER_TIM_3','vessel_master_1',$_REQUEST['id']));
$arr3[6]=array('','TROPICAL TIMBER',$obj->getVesselParticularData('TROPICAL_TIM_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('TROPICAL_TIM_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('TROPICAL_TIM_3','vessel_master_1',$_REQUEST['id']));

for($i=1;$i<7;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[3]=array('2.3','IS VESSEL FITTED FOR TRANSIT OF : PANAMA CANAL?',$obj->getNameBasedOnID($obj->getVesselParticularData('PANAMA_CANAL','vessel_master_1',$_REQUEST['id'])));
$arr3[4]=array('2.4','IS VESSEL FITTED FOR TRANSIT OF : SUEZ CANAL?',$obj->getNameBasedOnID($obj->getVesselParticularData('SUEZ_CANAL','vessel_master_1',$_REQUEST['id'])));
$arr3[5]=array('2.5','IS VESSEL FITTED FOR TRANSIT OF : ST LAWRENCE SEAWAY?',$obj->getNameBasedOnID($obj->getVesselParticularData('SEAWAY','vessel_master_1',$_REQUEST['id'])));
$arr3[6]=array('2.6','FOR PANAMA CANAL SUITABLE VESSEL STATE DEADWEIGHT ALL TOLD (METRIC TONS) ON 39ft 6ins (12.039M) (SG 0.9954)',$obj->getVesselParticularData('PANAMA_CANAL_1','vessel_master_1',$_REQUEST['id']));
$arr3[7]=array('2.7','IS PANAMA DEADWEIGHT ALL TOLD AFFECTED BY VESSEL�S BILGE TURN RADIUS?',$obj->getNameBasedOnID($obj->getVesselParticularData('PANAMA_RADIUS','vessel_master_1',$_REQUEST['id'])));
$arr3[8]=array('2.8','FOR ST LAWRENCE SEAWAY SIZE VESSEL STATE DEADWEIGHT ALL TOLD (METRIC TONS) BASIS 26 FT (7.92M) FRESH WATER',$obj->getVesselParticularData('SEAWAY_1','vessel_master_1',$_REQUEST['id']));
$arr3[9]=array('2.9','GT/NT : INTERNATIONAL',$obj->getVesselParticularData('GT_INATERNATIONAL','vessel_master_1',$_REQUEST['id']));
$arr3[10]=array('2.10','GT/NT : SUEZ',$obj->getVesselParticularData('GT_SUEZ','vessel_master_1',$_REQUEST['id']));
$arr3[11]=array('2.11','GT/NT : PANAMA',$obj->getVesselParticularData('GT_PANAMA','vessel_master_1',$_REQUEST['id']));
$arr3[12]=array('2.12','GT/NT : BRITISH',$obj->getVesselParticularData('GT_BRITISH','vessel_master_1',$_REQUEST['id']));
$arr3[13]=array('2.13','LENGTH OVERALL (METRES)',$obj->getVesselParticularData('LOA','vessel_master_1',$_REQUEST['id']));
$arr3[14]=array('2.14','LENGTH BETWEEN PERPENDICULARS (METRES)',$obj->getVesselParticularData('LBW','vessel_master_1',$_REQUEST['id']));
$arr3[15]=array('2.15','EXTREME BREADTH (METRES)',$obj->getVesselParticularData('EXTREME_BREADTH','vessel_master_1',$_REQUEST['id']));
$arr3[16]=array('2.16','DEPTH MOULDED (METRES)',$obj->getVesselParticularData('DEPTH_MODULE','vessel_master_1',$_REQUEST['id']));

for($i=3;$i<17;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L','L','L','L');
$arr2=array(10,61,40,40,40);
$arr3=array('2.17','DISTANCE (METRES) FROM WATERLINE TO TOP OF HATCH COAMINGS BASIS 50 PCT BUNKERS � NO HOLDS FLOODED','BALLAST CONDITION (BALLAST HOLDS NOT FLOODED)','HEAVY BALLAST CONDITION','LIGHT CONDITION');
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L','L','L');
$arr2=array(10,61,40,40,40);

/*$arr3[1]=array('','FULL BALLAST',$obj->getVesselParticularData('FULL_BALLAST_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('FULL_BALLAST_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('FULL_BALLAST_2','vessel_master_1',$_REQUEST['id']));
$arr3[2]=array('','LIGHT BALLAST',$obj->getVesselParticularData('LIGHT_BALLAST_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('LIGHT_BALLAST_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('LIGHT_BALLAST_3','vessel_master_1',$_REQUEST['id']));
$arr3[3]=array('','TOP SIDE TANKS EMPTY',$obj->getVesselParticularData('TSTE_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('TSTE_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('TSTE_3','vessel_master_1',$_REQUEST['id']));*/

$arr3[4]=array('','NO 1 HATCH',$obj->getVesselParticularData('NOH_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('NOH_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('NOH_3','vessel_master_1',$_REQUEST['id']));
$arr3[5]=array('','MIDSHIPS',$obj->getVesselParticularData('MIDSHIPS_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('MIDSHIPS_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('MIDSHIPS_3','vessel_master_1',$_REQUEST['id']));
$arr3[6]=array('','LAST HATCH',$obj->getVesselParticularData('LAST_HATCH_1','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('LAST_HATCH_2','vessel_master_1',$_REQUEST['id']),$obj->getVesselParticularData('LAST_HATCH_3','vessel_master_1',$_REQUEST['id']));

for($i=4;$i<7;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array('2.18','DISTANCE (METRES) FROM KEEL TO TOP OF HATCH COAMINGS (OR TOP OF HATCH COVERS IF SIDE-ROLLING HATCHES):');
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,91,90);
$arr3[1]=array('','AT',$obj->getVesselParticularData('AT_1','vessel_master_1',$_REQUEST['id']));
$arr3[2]=array('','NO 1 HATCH',$obj->getVesselParticularData('NOH_1_1','vessel_master_1',$_REQUEST['id']));
$arr3[3]=array('','MIDSHIPS',$obj->getVesselParticularData('MIDSHIPS_1_1','vessel_master_1',$_REQUEST['id']));
$arr3[4]=array('','LAST HATCH',$obj->getVesselParticularData('LAST_HATCH_1_1','vessel_master_1',$_REQUEST['id']));

for($i=1;$i<5;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[19]=array('2.19','VESSEL�S BALLASTING AND DEBALLASTING TIME (METRIC TONS PER HOUR)',$obj->getVesselParticularData('VESSEL_BALLAST','vessel_master_1',$_REQUEST['id']));
$arr3[20]=array('2.20','DISTANCE (METRES) FROM KEEL TO HIGHEST POINT OF VESSEL(INM ANTENNA)',$obj->getVesselParticularData('DISTANCE_KEEL','vessel_master_1',$_REQUEST['id']));

$ballast_tank = 'FULL BALLAST : '.$obj->getVesselParticularData('FULL_BALLAST_1','vessel_master_1',$_REQUEST['id'])."\n";
$ballast_tank .= 'LIGHT BALLAST : '.$obj->getVesselParticularData('LIGHT_BALLAST_1','vessel_master_1',$_REQUEST['id'])."\n";
$ballast_tank .= 'TOP SIDE TANKS EMPTY : '.$obj->getVesselParticularData('TSTE_1','vessel_master_1',$_REQUEST['id'])."\n\n";
$ballast_tank .= $obj->getVesselParticularData('COBT','vessel_master_1',$_REQUEST['id']);

$arr3[21]=array('2.21','CAPACITY OF : BALLAST TANKS',$ballast_tank);
$arr3[22]=array('2.22','CAPACITY OF : BALLAST HOLDS CAPACITY (STATE WHICH HOLD(S)',$obj->getVesselParticularData('COBC','vessel_master_1',$_REQUEST['id']));
$arr3[23]=array('2.23','CONSTANTS EXCLUDING FRESHWATER',$obj->getVesselParticularData('CEF','vessel_master_1',$_REQUEST['id']));
$arr3[24]=array('2.24','DAILY FRESHWATER CONSUMPTION',$obj->getVesselParticularData('DFC','vessel_master_1',$_REQUEST['id']));
$arr3[25]=array('2.25','FRESH WATER CAPACITY',$obj->getVesselParticularData('FWC','vessel_master_1',$_REQUEST['id']));
$arr3[26]=array('2.26','STATE CAPACITY AND DAILY',$obj->getVesselParticularData('SCAD','vessel_master_1',$_REQUEST['id']));
$arr3[27]=array('2.27','PRODUCTION OF EVAPORATOR',$obj->getVesselParticularData('POE','vessel_master_1',$_REQUEST['id']));
$arr3[28]=array('2.28','NORMAL FRESH WATER RESERVE',$obj->getVesselParticularData('NFWR','vessel_master_1',$_REQUEST['id']));
$arr3[29]=array('2.29','VESSEL IS FITTED WITH SHAFT GENERATOR',$obj->getNameBasedOnID($obj->getVesselParticularData('SHAFT_GEN','vessel_master_1',$_REQUEST['id'])));
$arr3[30]=array('2.30','VESSEL�S ONBOARD ELECTRICAL SUPPLY (V/Hz)',$obj->getVesselParticularData('VOES','vessel_master_1',$_REQUEST['id']));
$arr3[31]=array('2.31','DETAILS OF ALTERNATIVE SUPPLY, IF ANY',$obj->getVesselParticularData('DOAS','vessel_master_1',$_REQUEST['id']));

for($i=19;$i<32;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("3.","CLASSIFICATION SOCIETY, SURVEYS AND CERTIFICATES");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('3.1','NAME OF CLASSIFICATION SOCIETY',$obj->getClaSocNameOnId($obj->getVesselParticularData('CLASS_SOC_ID','vessel_master_2',$_REQUEST['id'])));
$arr3[2]=array('3.2','CLASS NOTATION',$obj->getVesselParticularData('CLASS_NOTATION','vessel_master_2',$_REQUEST['id']));

if(date("M d, Y",strtotime($obj->getVesselParticularData('DATE_LAST_1','vessel_master_2',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$last_date = '';
}
else
{
	$last_date = date("M d, Y",strtotime($obj->getVesselParticularData('DATE_LAST_1','vessel_master_2',$_REQUEST['id'])));
}

$arr3[3]=array('3.3','DATE OF LAST SPECIAL SURVEY',$last_date);

if(date("M d, Y",strtotime($obj->getVesselParticularData('DATE_LAST_2','vessel_master_2',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$last_date1 = '';
}
else
{
	$last_date1 = date("M d, Y",strtotime($obj->getVesselParticularData('DATE_LAST_2','vessel_master_2',$_REQUEST['id'])));
}

$arr3[4]=array('3.4','DATE OF LAST ANNUAL SURVEY',$last_date1);
$arr3[5]=array('3.5','IS VESSEL ENTERED IN CLASSIFICATION APPROVED ENHANCED SURVEY PROGRAMME',$obj->getNameBasedOnID($obj->getVesselParticularData('CLASS_1','vessel_master_2',$_REQUEST['id'])));

if(date("M d, Y",strtotime($obj->getVesselParticularData('DATE_INS_1','vessel_master_2',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$date_ins = '';
}
else
{
	$date_ins = date("M d, Y",strtotime($obj->getVesselParticularData('DATE_INS_1','vessel_master_2',$_REQUEST['id'])));
}

$arr3[6]=array('3.6','DATE OF LAST INSPECTION',$date_ins);

if(date("M d, Y",strtotime($obj->getVesselParticularData('DATE_INS_2','vessel_master_2',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$date_ins1 = '';
}
else
{
	$date_ins1 = date("M d, Y",strtotime($obj->getVesselParticularData('DATE_INS_2','vessel_master_2',$_REQUEST['id'])));
}

$arr3[7]=array('3.7','DATE OF NEXT INSPECTION',$date_ins1);
$arr3[8]=array('3.8','DOES VESSEL COMPLY WITH IACS UNIFIED REQUIREMENTS REGARDING NUMBER 1 CARGO HOLD AND DOUBLE BOTTOM TANK STEEL STRUCTURE?',$obj->getNameBasedOnID($obj->getVesselParticularData('STR','vessel_master_2',$_REQUEST['id'])));
$arr3[9]=array('3.9','HAS THIS COMPLIANCE BEEN VERIFIED BY THE CLASSIFICATION SOCIETY?',$obj->getNameBasedOnID($obj->getVesselParticularData('STR_1','vessel_master_2',$_REQUEST['id'])));

if(date("M d, Y",strtotime($obj->getVesselParticularData('DATE_DRYDOCK','vessel_master_2',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$date_dry = $obj->getPortNameBasedOnID($obj->getVesselParticularData('PORT_DRYDOCK','vessel_master_2',$_REQUEST['id']));
}
else
{
	$date_dry = date("M d, Y",strtotime($obj->getVesselParticularData('DATE_DRYDOCK','vessel_master_2',$_REQUEST['id']))).','.$obj->getPortNameBasedOnID($obj->getVesselParticularData('PORT_DRYDOCK','vessel_master_2',$_REQUEST['id']));
}

$arr3[10]=array('3.10','DATE AND PLACE OF LAST DRYDOCK',$date_dry);
$arr3[11]=array('3.11','HAS VESSEL BEEN INVOLVED IN ANY GROUNDINGS OR COLLISION IN THE LAST 12 MONTHS?  IF SO GIVE FULL DETAILS',$obj->getVesselParticularData('GROUND','vessel_master_2',$_REQUEST['id']));
$arr3[12]=array('3.12','IS VESSEL ISM CERTIFIED?',$obj->getNameBasedOnID($obj->getVesselParticularData('ISM','vessel_master_2',$_REQUEST['id'])));
$arr3[13]=array('3.13','STATE - DOC (DOCUMENT OF COMPLIANCE) CERTIFICATE NUMBER/ISSUING AUTHORITY',$obj->getVesselParticularData('STATE_DOC','vessel_master_2',$_REQUEST['id']));
$arr3[14]=array('3.14','STATE - SMC (SAFETY MANAGEMENT) CERTIFICATE NUMBER/ISSUING AUTHORITY',$obj->getVesselParticularData('STATE_SMC','vessel_master_2',$_REQUEST['id']));

if(date("M d, Y",strtotime($obj->getVesselParticularData('DATE_LAST_AUDIT','vessel_master_2',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$date1 = '';
}
else
{
	$date1 = date("M d, Y",strtotime($obj->getVesselParticularData('DATE_LAST_AUDIT','vessel_master_2',$_REQUEST['id'])));
}

if(date("M d, Y",strtotime($obj->getVesselParticularData('DATE_NEXT_AUDIT','vessel_master_2',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$date2 = '';
}
else
{
	$date2 = date("M d, Y",strtotime($obj->getVesselParticularData('DATE_NEXT_AUDIT','vessel_master_2',$_REQUEST['id'])));
}

$date_audit = $date1.'   '.$date2;

$arr3[15]=array('3.15','GIVE DATE OF LAST AND NEXT AUDIT',$date_audit);
$arr3[16]=array('3.16','STATE OUTSTANDING RECOMMENDATIONS, IF  ANY',$obj->getVesselParticularData('STATE_OUTS','vessel_master_2',$_REQUEST['id']));

if(date("M d, Y",strtotime($obj->getVesselParticularData('DATE_ADVISE','vessel_master_2',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$date_advise = $obj->getPortNameBasedOnID($obj->getVesselParticularData('PORT_ADVISE','vessel_master_2',$_REQUEST['id']));
}
else
{
	$date_advise = date("M d, Y",strtotime($obj->getVesselParticularData('DATE_ADVISE','vessel_master_2',$_REQUEST['id']))).','.$obj->getPortNameBasedOnID($obj->getVesselParticularData('PORT_ADVISE','vessel_master_2',$_REQUEST['id']));
}

$arr3[17]=array('3.17','ADVISE DATE AND PLACE OF LAST PORT STATE CONTROL',$date_advise);
$arr3[18]=array('3.18','DID VESSEL PASS MOST RECENT PORT STATE CONTROL INSPECTION WITHOUT DETENTION',$obj->getNameBasedOnID($obj->getVesselParticularData('DETEN','vessel_master_2',$_REQUEST['id'])));
$arr3[19]=array('3.19','STATE OUTSTANDING RECOMMENDATIONS, IF  ANY',$obj->getVesselParticularData('STATE_OUTS_1','vessel_master_2',$_REQUEST['id']));
$arr3[20]=array('3.20','IS VESSEL�S CREW COVERED BY FULL ITF OR BONA FIDE TRADE UNION AGREEMENT ACCEPTABLE TO ITF?',$obj->getVesselParticularData('ITF','vessel_master_2',$_REQUEST['id']));
$arr3[21]=array('3.21','IF VESSEL HAS ITF AGREEMENT STATE NUMBER, DATE OF ISSUE AND EXPIRY DATE',$obj->getVesselParticularData('ITF_AGREE','vessel_master_2',$_REQUEST['id']));

for($i=1;$i<22;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array('3.22','CERTIFICATES');
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$arr1=array('L','L','L','L','L');
$arr2=array(10,61,40,40,40);
$arr3=array('','CERTIFICATE NAME','DATE OF ISSUE','DATE OF LAST ANNUAL ENDORSEMENT','DATE OF EXPIRY');
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L','L','L');
$arr2=array(10,61,40,40,40);

$sql = "select * from vessel_master_slave where VESSEL_IMO_ID=".$_REQUEST['id'];
$res = mssql_query($sql);
$rec = mssql_num_rows($res); 
if($rec == 0)
{
	$arr3=array('','','','','');
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
}
else
{
	while($rows = mssql_fetch_assoc($res))
	{
		if(date("M d, Y",strtotime($rows['DATE_ISSUE'])) == 'Jan 01, 1970')
		{$date_issue = '';}
		else{$date_issue = date("M d, Y",strtotime($rows['DATE_ISSUE']));}
		
		if(date("M d, Y",strtotime($rows['DATE_LAST'])) == 'Jan 01, 1970')
		{$date_last = '';}
		else{$date_last = date("M d, Y",strtotime($rows['DATE_LAST']));}
		
		if(date("M d, Y",strtotime($rows['DATE_EXPIRY'])) == 'Jan 01, 1970')
		{$date_expiry = '';}
		else
		{$date_expiry = date("M d, Y",strtotime($rows['DATE_EXPIRY']));}
		
		$arr3=array('',$obj->getCertificateNameOnId($rows['CERTIFICATE_ID']),$date_issue,$date_last,$date_expiry);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		$pdf->Row2($arr3);
	}
}

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[23]=array('3.23','DO ANY RECOMMENDATIONS APPEAR ON ANY OF THE ABOVE CERTIFICATES? IF YES STATE FULL DETAILS',$obj->getVesselParticularData('FULL_DETAILS','vessel_master_2',$_REQUEST['id']));
$arr3[24]=array('3.24','IMO REGISTRATION NUMBER',$obj->getVesselParticularData('IMO_R_NO','vessel_master_2',$_REQUEST['id']));

if(date("M d, Y",strtotime($obj->getVesselParticularData('DATE_SMC','vessel_master_2',$_REQUEST['id']))) == 'Jan 01, 1970')
{
	$date_smc = '';
}
else
{
	$date_smc = date("M d, Y",strtotime($obj->getVesselParticularData('DATE_SMC','vessel_master_2',$_REQUEST['id'])));
}

$arr3[25]=array('3.25','EXPIRY DATE OF SMC CERTIFICATE',$date_smc);

for($i=23;$i<26;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("4.","CREW");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('4.1','NUMBER OF CREW',$obj->getVesselParticularData('NOC','vessel_master_3',$_REQUEST['id']));
$arr3[2]=array('4.2','NAME AND NATIONALITY OF MASTER',$obj->getVesselParticularData('NOM','vessel_master_3',$_REQUEST['id']));
$arr3[3]=array('4.3','NATIONALITY OF OFFICERS',$obj->getVesselParticularData('NOO','vessel_master_3',$_REQUEST['id']));
$arr3[4]=array('4.4','NATIONALITY OF CREW',$obj->getVesselParticularData('NOCR','vessel_master_3',$_REQUEST['id']));

for($i=1;$i<5;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("5.","CARGO ARRANGEMENTS");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','B',8);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("5.1","HOLDS");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('A.','NUMBER OF HOLDS',$obj->getVesselParticularData('NOH','vessel_master_4',$_REQUEST['id']));
$arr3[2]=array('B.','ARE VESSEL�S HOLDS CLEAR AND FREE OF ANY OBSTRUCTIONS?',$obj->getNameBasedOnID($obj->getVesselParticularData('HOLDS_CLEAR','vessel_master_4',$_REQUEST['id'])));
$arr3[3]=array('C.','GRAIN/BALE CAPACITY IN HOLDS',$obj->getVesselParticularData('GRAIN','vessel_master_4',$_REQUEST['id']));
$arr3[4]=array('D.','IS VESSEL STRENGTHENED FOR THE CARRIAGE OF HEAVY CARGOES?IF YES STATE WHICH HOLDS MAY BE LEFT EMPTY',$obj->getVesselParticularData('CARRIAGE','vessel_master_4',$_REQUEST['id']));
$arr3[5]=array('E.','IS TANKTOP STEEL AND SUITABLE FOR GRAB DISCHARGE?',$obj->getNameBasedOnID($obj->getVesselParticularData('TANKTOP','vessel_master_4',$_REQUEST['id'])));
$arr3[6]=array('F.','STATE WHETHER BULKHEAD CORRUGATIONS VERTICAL OR HORIZONTAL',$obj->getVesselParticularData('BULKHEAD','vessel_master_4',$_REQUEST['id']));
$arr3[7]=array('G','TANKTOP STRENGTH (METRIC TONS PER SQM)',$obj->getVesselParticularData('SQM','vessel_master_4',$_REQUEST['id']));
$arr3[8]=array('H.','ARE HOLDS CO2 FITTED?',$obj->getNameBasedOnID($obj->getVesselParticularData('CO2','vessel_master_4',$_REQUEST['id'])));
$arr3[9]=array('I.','ARE HOLDS FITTED WITH SMOKE DETECTION SYSTEM?',$obj->getNameBasedOnID($obj->getVesselParticularData('DET_SYS','vessel_master_4',$_REQUEST['id'])));
$arr3[10]=array('J.','IS VESSEL FITTED WITH AUSTRALIAN TYPE APPROVED HOLDS LADDERS',$obj->getNameBasedOnID($obj->getVesselParticularData('LADDER','vessel_master_4',$_REQUEST['id'])));
$arr3[11]=array('K.','IHAS VESSEL A FUNCTIONING CLASS CERTIFIED LOADMASTER/LOADICATOR OR SIMILAR CALCULATOR',$obj->getNameBasedOnID($obj->getVesselParticularData('CALC','vessel_master_4',$_REQUEST['id'])));
$arr3[12]=array('L.','ARE HOLDS HOPPERED AT? : HOLD SIDE',$obj->getNameBasedOnID($obj->getVesselParticularData('HOLD_SIDE','vessel_master_4',$_REQUEST['id'])));
$arr3[13]=array('M.','ARE HOLDS HOPPERED AT? : FORWARD BULKHEAD',$obj->getNameBasedOnID($obj->getVesselParticularData('F_BULKHEAD','vessel_master_4',$_REQUEST['id'])));
$arr3[14]=array('N.','ARE HOLDS HOPPERED AT? : AFT BULKHEAD',$obj->getNameBasedOnID($obj->getVesselParticularData('AFT_BULKHEAD','vessel_master_4',$_REQUEST['id'])));
$arr3[15]=array('O.','ARE HOLDS HOPPERED AT? : CAN VESSEL�S HOLDS BE DESCRIBED AS BOX SHAPED',$obj->getNameBasedOnID($obj->getVesselParticularData('SHAPED','vessel_master_4',$_REQUEST['id'])));
$arr3[16]=array('P.','MEASUREMENT OF ANY TANK SLOPES/HOPPERING (HEIGHT AND DISTANCE FROM VESSEL�S SIDE AT TANK TOP) (METRES)',$obj->getVesselParticularData('MOATS','vessel_master_4',$_REQUEST['id']));

for($i=1;$i<17;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L','L','L','L');
$arr2=array(10,61,40,40,40);
$arr3=array('Q.','FLAT FLOOR MEASUREMENT OF CARGO HOLDS AT TANK TOP (METRES)','Length- mtrs','Width(Fwd)-mtrs','Width(Aft)- mtrs');
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L','L','L');
$arr2=array(10,61,40,40,40);

$arr = array("H1","H2","H3","H4","H5","H6","H7");
for($i=0;$i<sizeof($arr);$i++)
{
	$arr3=array('',$arr[$i],$obj->getVesselParticularData($arr[$i].'_1','vessel_master_4',$_REQUEST['id']),$obj->getVesselParticularData($arr[$i].'_2','vessel_master_4',$_REQUEST['id']),$obj->getVesselParticularData($arr[$i].'_3','vessel_master_4',$_REQUEST['id']));
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
}

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3=array('R.','ARE VESSEL�S HOLDS ELECTRICALLY VENTILATED?IF YES STATE NUMBER OF AIRCHANGES PER HOUR BASIS EMPTY HOLDS',$obj->getVesselParticularData('VENT','vessel_master_4',$_REQUEST['id']));
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("5.2","DECK AND HATCHES");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('A.','NUMBER OF HATCHES',$obj->getVesselParticularData('NOHA','vessel_master_4',$_REQUEST['id']));
$arr3[2]=array('B.','MAKE AND TYPE OF HATCH COVERS',$obj->getVesselParticularData('NOHA_COV','vessel_master_4',$_REQUEST['id']));
$arr3[3]=array('C.','HATCH SIZES (METRES)',$obj->getVesselParticularData('HATCH_SIZE','vessel_master_4',$_REQUEST['id']));
$arr3[4]=array('D.','STRENGTH OF HATCH COVERS (METRIC TONS PER SQM)',$obj->getVesselParticularData('HATCH_SQM','vessel_master_4',$_REQUEST['id']));
$arr3[5]=array('E.','DISTANCE FROM SHIP�S RAIL TO NEAR AND FAR EDGE OF HATCH COVERS/COAMING NEAR AND FAR (METRES)',$obj->getVesselParticularData('FAR','vessel_master_4',$_REQUEST['id']));
$arr3[6]=array('F.','DISTANCE FROM BOW TO FORE OF 1ST HOLD OPENING (METRES)',$obj->getVesselParticularData('OPEN_1','vessel_master_4',$_REQUEST['id']));
$arr3[7]=array('G.','DISTANCE FROM STERN TO AFT OF LAST HOLD OPENING (METRES)',$obj->getVesselParticularData('LAST_OPEN','vessel_master_4',$_REQUEST['id']));
$arr3[8]=array('3.3','STATE DECK STRENGTH (METRIC TONS PER SQM)',$obj->getVesselParticularData('STR_SQM','vessel_master_4',$_REQUEST['id']));
$arr3[9]=array('3.4','CEMENT HOLE NUMBERS PER HOLD:',$obj->getVesselParticularData('CHNPH','vessel_master_4',$_REQUEST['id']));
$arr3[10]=array('3.5','DIAMETER OF HOLES:',$obj->getVesselParticularData('DOH','vessel_master_4',$_REQUEST['id']));
$arr3[11]=array('3.6','POSITION OF HOLES:',$obj->getVesselParticularData('POH','vessel_master_4',$_REQUEST['id']));

for($i=1;$i<12;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("6.","CARGO GEAR");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('6.1','IF GEARED STATE MAKE AND TYPE',$obj->getVesselParticularData('MAKE_TYPE','vessel_master_4',$_REQUEST['id']));
$arr3[2]=array('6.2','NUMBER OF CRANES AND WHERE SITUATED',$obj->getVesselParticularData('NOCS','vessel_master_4',$_REQUEST['id']));
$arr3[3]=array('6.3','OUTREACH (METRES) OF GEAR - BEYOND SHIP�S RAIL',$obj->getVesselParticularData('BSR','vessel_master_4',$_REQUEST['id']));
$arr3[4]=array('6.4','OUTREACH (METRES) OF GEAR - BEYOND SHIP�S RAIL WITH MAXIMUM CARGO LIFT ON HOOK',$obj->getVesselParticularData('HOOK','vessel_master_4',$_REQUEST['id']));
$arr3[5]=array('6.5','IF GANTRY CRANES/HORIZONTAL SLEWING CRANES STATE MINIMUM CLEARANCE DISTANCE CRANE HOOK TO TOP OF HATCH COAMING (METRES)',$obj->getVesselParticularData('HATCH_COAM','vessel_master_4',$_REQUEST['id']));
$arr3[6]=array('6.6','TIME NEEDED FOR FULL CYCLE WITH MAXIMUM CARGO LIFT ON HOOK',$obj->getVesselParticularData('HOOK_CARGO','vessel_master_4',$_REQUEST['id']));
$arr3[7]=array('6.7','IS GEAR COMBINABLE FOR HEAVY LIFT',$obj->getNameBasedOnID($obj->getVesselParticularData('H_LIFT','vessel_master_4',$_REQUEST['id'])));
$arr3[8]=array('6.8','ARE WINCHES ELECTRO-HYDRAULIC?',$obj->getNameBasedOnID($obj->getVesselParticularData('E_HYDRA','vessel_master_4',$_REQUEST['id'])));
$arr3[9]=array('6.9','IF VESSEL HAS GRABS ON BOARD STATE TYPE AND CAPACITY',$obj->getVesselParticularData('TYPE_CAP','vessel_master_4',$_REQUEST['id']));
$arr3[10]=array('6.10','IS VESSEL FITTED WITH SUFFICIENT LIGHTS AT EACH HATCH FOR NIGHT WORK?',$obj->getNameBasedOnID($obj->getVesselParticularData('N_WORK','vessel_master_4',$_REQUEST['id'])));
$arr3[11]=array('6.11','IS VESSEL LOGS FITTED?IF YES STATE NUMBER AND TYPE OF STANCHIONS/SOCKETS, IF ON BOARD',$obj->getVesselParticularData('BOARD','vessel_master_4',$_REQUEST['id']));

for($i=1;$i<12;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("7.","SPEED/CONSUMPTION/FUEL ENGINE");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$arr1=array('L','L','L','L');
$arr2=array(10,81,50,50);
$arr3=array('7.1','STATE VESSEL�S CONSUMPTION AT ABOUT 13,5 KNOTS (UP TO BEAUFORT SCALE FORCE 4/DOUGLAS SEA STATE 3,NO ADVERSE CURRENTS) AS FOLLOWS:','ABOUT METRIC TONS (MAIN ENGINE)','ABOUT METRIC TONS (AUXILIARIES)');
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L','L');
$arr2=array(10,81,50,50);

$arr_1 = array("LADEN","BALLAST");
for($i=0;$i<sizeof($arr_1);$i++)
{
	if($i == 0){$name = 'A.';}else{$name = 'B.';}
	$arr3=array($name,$arr_1[$i],$obj->getVesselParticularData($arr_1[$i].'_1','vessel_master_5',$_REQUEST['id']),$obj->getVesselParticularData($arr_1[$i].'_2','vessel_master_5',$_REQUEST['id']));
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
}

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[2]=array('7.2','BUNKER GRADES',$obj->getVesselParticularData('BUNKER_G','vessel_master_5',$_REQUEST['id']));
$arr3[3]=array('7.3','BUNKER CAPACITIES  BASIS 100 .PCT CAPACITY',$obj->getVesselParticularData('BUNKER_C','vessel_master_5',$_REQUEST['id']));
$arr3[4]=array('7.4','PORT CONSUMPTION PER 24 HOURS IDLE/WORKING (METRIC TONS)',$obj->getVesselParticularData('P_WORK','vessel_master_5',$_REQUEST['id']));
$arr3[5]=array('7.5','ENGINE MAKE AND TYPE',$obj->getVesselParticularData('EMAT','vessel_master_5',$_REQUEST['id']));
$arr3[6]=array('7.6','MAX OUTPUT BHP/RPM',$obj->getVesselParticularData('MOB','vessel_master_5',$_REQUEST['id']));

for($i=2;$i<7;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("8.","COMMUNICATIONS");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('8.1','CALL SIGN',$obj->getVesselParticularData('CALL_SIGN','vessel_master_6',$_REQUEST['id']));
$arr3[2]=array('8.2','NAME OF RADIO STATION WHICH VESSEL MONITORING',$obj->getVesselParticularData('NOR','vessel_master_6',$_REQUEST['id']));
$arr3[3]=array('8.3','SPECIFY VESSEL�S SATELLITE COMMUNICATIONS SYSTEM',$obj->getVesselParticularData('COMM_SYS','vessel_master_6',$_REQUEST['id']));

for($i=1;$i<4;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("9.","INSURANCES");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('9.1','HULL AND MACHINERY INSURED VALUE',$obj->getVesselParticularData('HAMIV','vessel_master_6',$_REQUEST['id']));
$arr3[2]=array('9.2','NAME OF OWNERS P AND I INSURERS',$obj->getVesselParticularData('P_I','vessel_master_6',$_REQUEST['id']));
$arr3[3]=array('9.3','WHERE IS OWNERS HULL AND MACHINERY PLACED?',$obj->getVesselParticularData('PLACED','vessel_master_6',$_REQUEST['id']));

for($i=1;$i<4;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("10.","MISCELLANEOUS");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('10.1','STATE LAST 5 (FIVE) CARGOES CARRIED AND LOAD AND DISCHARGE PORT(S) WITH MOST RECENT FIRST',$obj->getVesselParticularData('CARGO','vessel_master_6',$_REQUEST['id']));
$arr3[2]=array('10.2','IS VESSEL FITTED FOR CARRIAGE OF GRAIN IN ACCORDANCE WITH CHAPTER V1 OF SOLAS 1974 AND AMENDMENTS WITHOUT REQUIRING BAGGING, STRAPPING AND SECURING WHEN LOADING A FULL CARGO (DEADWEIGHT) OF HEAVY GRAIN IN BULK (STOWAGE FACTOR 42 CUFT) WITH ENDS UNTRIMMED?',$obj->getNameBasedOnID($obj->getVesselParticularData('CARGO_1','vessel_master_6',$_REQUEST['id'])));
$arr3[3]=array('10.3','STATE NUMBER OF HOLDS WHICH MAY BE LEFT SLACK WITHOUT REQUIRING BAGGING, STRAPPING AND SECURING',$obj->getVesselParticularData('CARGO_2','vessel_master_6',$_REQUEST['id']));

for($i=1;$i<4;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("11.","CONTAINER BULKERS/ MULTI  PURPOSE (ONLY TO BE COMPLETED IF APPLICABLE)");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('11.1','CAPACITY IN DIRECT STOW OF TEU/FEU BASIS : EMPTY',$obj->getVesselParticularData('EMPTY','vessel_master_7',$_REQUEST['id']));
$arr3[2]=array('11.2','CAPACITY IN DIRECT STOW OF TEU/FEU BASIS : TONS HOMOGENEOUS WEIGHT',$obj->getVesselParticularData('THW','vessel_master_7',$_REQUEST['id']));
$arr3[3]=array('11.3','ARE ALL CONTAINERS WITHIN REACH OF VESSEL�S GEAR:(YES/NO) IF NO STATE SELF SUSTAINED CAPACITY',$obj->getVesselParticularData('GEAR','vessel_master_7',$_REQUEST['id']));
$arr3[4]=array('11.4','IF VESSEL FITTED WITH ALL PERMANENT AND LOOSE FITTINGS/LASHING MATERIALS FOR ABOVE NUMBER OF TEU/FEU?',$obj->getNameBasedOnID($obj->getVesselParticularData('FEU','vessel_master_7',$_REQUEST['id'])));
$arr3[5]=array('11.5','IS VESSEL FITTED WITH RECESSED HOLES/SHOES ON TANKTOP AND CONTAINER SHOES ON WEATHERDECK AND HATCH COVERS?',$obj->getNameBasedOnID($obj->getVesselParticularData('COVERS','vessel_master_7',$_REQUEST['id'])));
$arr3[6]=array('11.6','ADVISE STACK WEIGHTS AND NUMBER OF TIERS ON/UNDERDECK - PER TEU',$obj->getVesselParticularData('PER_TEU','vessel_master_7',$_REQUEST['id']));
$arr3[7]=array('11.7','ADVISE STACK WEIGHTS AND NUMBER OF TIERS ON/UNDERDECK - PER FEU',$obj->getVesselParticularData('PER_FEU','vessel_master_7',$_REQUEST['id']));
$arr3[8]=array('11.8','HAS VESSEL A CONTAINER SPREADER ON BOARD?',$obj->getVesselParticularData('SPREAD_BOARD','vessel_master_7',$_REQUEST['id']));
$arr3[9]=array('11.9','NUMBER AND TYPE OF REEFER PLUGS',$obj->getVesselParticularData('REEFER_PLUGS','vessel_master_7',$_REQUEST['id']));

for($i=1;$i<10;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("12.","TWEENDECKERS");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('12.1','HAS VESSEL FOLDING TWEENS?',$obj->getNameBasedOnID($obj->getVesselParticularData('TWEENS','vessel_master_7',$_REQUEST['id'])));
$arr3[2]=array('12.2','NUMBER OF HOLDS/HATCHES',$obj->getVesselParticularData('NOHH','vessel_master_7',$_REQUEST['id']));
$arr3[3]=array('12.3','TYPE OF HATCHES',$obj->getVesselParticularData('TOH','vessel_master_7',$_REQUEST['id']));
$arr3[4]=array('12.4','HATCH SIZES (METRES) : WEATHERDECK',$obj->getVesselParticularData('WEATHER_DECK','vessel_master_7',$_REQUEST['id']));
$arr3[5]=array('12.5','HATCH SIZES (METRES) : TWEENDECK',$obj->getVesselParticularData('TWEEN_DECK','vessel_master_7',$_REQUEST['id']));
$arr3[6]=array('12.6','ARE TWEENDECKERS FLUSH?',$obj->getNameBasedOnID($obj->getVesselParticularData('FLUSH','vessel_master_7',$_REQUEST['id'])));
$arr3[7]=array('12.7','STRENGTHS (METRIC TONS PER SQM) : TANKTOP',$obj->getVesselParticularData('TANKTOP','vessel_master_7',$_REQUEST['id']));
$arr3[8]=array('12.8','STRENGTHS (METRIC TONS PER SQM) : TWEENDECK',$obj->getVesselParticularData('SQM_TD','vessel_master_7',$_REQUEST['id']));
$arr3[9]=array('12.9','STRENGTHS (METRIC TONS PER SQM) : WEATHERDECK',$obj->getVesselParticularData('SQM_WD','vessel_master_7',$_REQUEST['id']));
$arr3[10]=array('12.10','STRENGTHS (METRIC TONS PER SQM) : HATCHCOVERS',$obj->getVesselParticularData('SQM_HC','vessel_master_7',$_REQUEST['id']));
$arr3[11]=array('12.11','IS VESSEL FULLY CARGO BATTEN FITTED?',$obj->getNameBasedOnID($obj->getVesselParticularData('B_FITTED','vessel_master_7',$_REQUEST['id'])));
$arr3[12]=array('12.12','IS VESSEL CO2 FITTED/ELECTRICALLY VENTILATED?',$obj->getNameBasedOnID($obj->getVesselParticularData('CO2_FITT','vessel_master_7',$_REQUEST['id'])));
$arr3[13]=array('12.13','REMARK',$obj->getVesselParticularData('REMARK','vessel_master_7',$_REQUEST['id']));

for($i=1;$i<14;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$filename = 'VESSEL PARTICULARS('.strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME")).').pdf';
}
else
{
$arr1 = $arr2 = $arr3 = array();	
$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(128,128,128);
$pdf->SetTextColor(255,255,255);
$pdf->SetDrawColor(238,238,238);
$arr1=array('L','L');
$arr2=array(10,181);
$arr3=array("1.","CHAPTER 8");
$pdf->SetAligns($arr1);
$pdf->SetWidths($arr2);
$pdf->Row2($arr3);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);
$arr1=array('L','L','L');
$arr2=array(10,90,91);
$arr3[1]=array('1.1','NAME',strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME")));
$arr3[2]=array('1.2','TYPE',$obj->getVesselTypeBasedOnID($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_TYPE")));

if($obj->getVesselIMOData($_REQUEST['id'],"YEARBUILT") == '1970')
{
	$built_date = '';
}
else
{
	$built_date = $obj->getVesselIMOData($_REQUEST['id'],"YEARBUILT");
}

$arr3[3]=array('1.3','BUILT DATE',$built_date);
$arr3[4]=array('1.4','FLAG',$obj->getCountryNameOnId($obj->getVesselParticularData('FLAG_ID','vessel_master_1',$_REQUEST['id'])));
$arr3[5]=array('1.5','SUMMER DWAT',$obj->getVesselIMOData($_REQUEST['id'],"DWT"));
$arr3[6]=array('1.6','SUMMER DRAFT',$obj->getVesselIMOData($_REQUEST['id'],"DRAFTM"));
$arr3[7]=array('1.7','LOA (M)',$obj->getVesselParticularData('LOA','vessel_master_1',$_REQUEST['id']));
$arr3[8]=array('1.8','EXTREME BREADTH (M)',$obj->getVesselParticularData('EXTREME_BREADTH','vessel_master_1',$_REQUEST['id']));
$arr3[9]=array('1.9','GRT / NRT : International',$obj->getVesselParticularData('GT_INATERNATIONAL','vessel_master_1',$_REQUEST['id']));
//$arr3[10]=array('1.10','NRT',$obj->getVesselParticularData('NRT','vessel_master_8',$_REQUEST['id']));
$arr3[10]=array('1.10','GRAIN (M�)',$obj->getVesselParticularData('GRAIN','vessel_master_8',$_REQUEST['id']));
$arr3[11]=array('1.11','BALE (M�)',$obj->getVesselParticularData('BALE','vessel_master_8',$_REQUEST['id']));
$arr3[12]=array('1.12','NUMBER OF HOLDS',$obj->getVesselParticularData('NOH','vessel_master_4',$_REQUEST['id']));
$arr3[13]=array('1.13','NUMBER OF HATCHES',$obj->getVesselParticularData('NOHA','vessel_master_4',$_REQUEST['id']));
$arr3[14]=array('1.14','HATCH SIZES (METRES)',$obj->getVesselParticularData('HATCH_SIZE','vessel_master_4',$_REQUEST['id']));
$arr3[15]=array('1.15','CARGO GEAR (MAIN DETAILS)',$obj->getVesselParticularData('MAKE_TYPE','vessel_master_4',$_REQUEST['id']));
$arr3[16]=array('1.16','CRANE SIZE',$obj->getVesselParticularData('NOCS','vessel_master_4',$_REQUEST['id']));
$arr3[17]=array('1.17','GRAB (MAIN DETAILS)',$obj->getVesselParticularData('TYPE_CAP','vessel_master_4',$_REQUEST['id']));
$arr3[18]=array('1.18','IMO NUMBER',$obj->getVesselParticularData('IMO_NO','vessel_master_1',$_REQUEST['id']));
$arr3[19]=array('1.19','NAME OF OWNERS P AND I INSURERS',$obj->getVesselParticularData('P_I','vessel_master_8',$_REQUEST['id']));
$arr3[20]=array('1.20','NAME OF CLASSIFICATION SOCIETY',$obj->getClaSocNameOnId($obj->getVesselParticularData('CLASS_SOC_ID','vessel_master_2',$_REQUEST['id'])));

for($i=1;$i<21;$i++)
{
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3[$i]);
	unset($arr3[$i]);
}

$filename = 'CHAPTER 8('.strtoupper($obj->getVesselIMOData($_REQUEST['id'],"VESSEL_NAME")).').pdf';
}

$pdf->Output($filename,'D');

?>