<?php 
session_start();
require_once("../includes/display_admin.inc.php");
require_once("../includes/functions_admin.inc.php");
$obj = new data();
$display = new display();
$display->logout_a();
if (@$_REQUEST['action'] == 'submit')
 {
	
 	$msg = $obj->insertUserDetails();
	header('Location:./shipping_user.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-group"></i>&nbsp;Shipping User&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Shipping User</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div align="right"><a href="shipping_user.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<!--<div class="col-md-6">-->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">ADD NEW USER DETAILS</h3>
					</div><!-- /.box-header -->
                                <!-- form start -->
						<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
							<div class="box-body">
								<div class="form-group">
									<label >Company Name</label><br>
									<label style='color:#dc631e;font-size:12px;' >&nbsp;&nbsp;&nbsp;&nbsp;<?php echo strtoupper($obj->getCompanyData($_SESSION['company'],"COMPANY_NAME"));?></label>                                       
								</div>
								<div class="form-group">
									<label for="txtName">Name</label>
									<input type="text" class="form-control" id="txtName" name="txtName" placeholder="Name" autocomplete="off">
								</div>
								
								<div class="form-group">
									<label>Address</label>
									<textarea class="form-control" rows="3" name="txtAddress" id="txtAddress" placeholder="Address"></textarea>
								</div>
								<div class="form-group">
									<label for="txtPhoneNo">Phone Number</label>
									<input type="text" class="form-control" id="txtPhoneNo" name="txtPhoneNo" placeholder="Phone Number" autocomplete="off">
								</div>
								<div class="form-group">
									<label for="txtEmailid">E-mail ID</label>
									<input type="email" class="form-control" id="txtEmailid" name="txtEmailid" placeholder="E-mail ID" autocomplete="off">
								</div>
								
								<div class="form-group">
									<label for="txtUName">User Name</label>
									<input type="text" class="form-control" id="txtUName" name="txtUName" placeholder="User Name" autocomplete="off">
								</div>
								
								<div class="form-group">
									<label for="txtPassword">Password</label>
									<input type="text" class="form-control" id="txtPassword" name="txtPassword" placeholder="Password" autocomplete="off">
								</div>
								
								<div class="form-group">
									<label for="txtCPassword">Confirm Password</label>
									<input type="text" class="form-control" id="txtCPassword" name="txtCPassword" placeholder="Confirm Password" autocomplete="off">
								</div>
								<div class="form-group">
									<label for="selStatus" >Status</label>
									<select  name="selStatus" class="form-control" id="selStatus" >
									<?php 
									$obj->getUserStatusList();
									?>
									</select>                                         
								</div>
								<div class="row">
									<div class="col-xs-12">
										<h2 class="page-header">
										  USER RIGHTS
										</h2>                            
									</div><!-- /.col -->
								</div>
								<div class="row">
								<div class="col-xs-12">
									<div class="box box-primary">
										<div class="box-body no-padding">
										<table class="table table-striped">
										   <thead>
												<tr>
													<th width="70%" align="right" valign="middle">Screens</th>
													<th width="10%" align="left" style="text-align:center" valign="middle">Add / Edit</th>
													<th width="10%" align="left" style="text-align:center" valign="middle">View</th>
												</tr>
											</thead>
											<tbody>
                                                
                                                <tr>
                                                    <td colspan="3"><strong>Voyage Charterer Out</strong></td>
                                                </tr>
                                                <tr>
													<td>Cargo Planning</td>
													<td align="center"><input type="checkbox" name="checkbox_18_1" id="checkbox_18_1" value="1" onClick="getChecked(18,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_18_2" id="checkbox_18_2" value="2" onClick="getUnChecked(18,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_18_3" id="checkbox_18_3" value="3" onClick="getUnChecked(18,3);"/></td>
												</tr>
                                                <tr>
													<td>Voyage Estimates(VC)</td>
													<td align="center"><input type="checkbox" name="checkbox_2_1" id="checkbox_2_1" value="1" onClick="getChecked(2,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_2_2" id="checkbox_2_2" value="2" onClick="getUnChecked(2,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_2_3" id="checkbox_2_3" value="3" onClick="getUnChecked(2,3);"/></td>
												</tr>
                                                <tr>
													<td>Decision Chart (VC)</td>
													<td align="center"><input type="checkbox" name="checkbox_3_1" id="checkbox_3_1" value="1" onClick="getChecked(3,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_3_2" id="checkbox_3_2" value="2" onClick="getUnChecked(3,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_3_3" id="checkbox_3_3" value="3" onClick="getUnChecked(3,3);"/></td>
												</tr>
                                                <tr>
													<td>Finalised Voyage Fixtures (VC)</td>
													<td align="center"><input type="checkbox" name="checkbox_4_1" id="checkbox_4_1" value="1" onClick="getChecked(4,1);" /></td>
													<td align="center"><input type="checkbox" name="checkbox_4_2" id="checkbox_4_2" value="2" onClick="getUnChecked(4,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_4_3" id="checkbox_4_3" value="3" onClick="getUnChecked(4,3);"/></td>
												</tr>
												
												
												<tr>
													<td>In Ops at a glance (VC)</td>
													<td align="center"><input type="checkbox" name="checkbox_5_1" id="checkbox_5_1" value="1" onClick="getChecked(5,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_5_2" id="checkbox_5_2" value="2" onClick="getUnChecked(5,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_5_3" id="checkbox_5_3" value="3" onClick="getUnChecked(5,3);"/></td>
												</tr>
												
												<tr>
													<td>Vessels in Post Ops (VC)</td>
													<td align="center"><input type="checkbox" name="checkbox_6_1" id="checkbox_6_1" value="1" onClick="getChecked(6,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_6_2" id="checkbox_6_2" value="2" onClick="getUnChecked(6,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_6_3" id="checkbox_6_3" value="3" onClick="getUnChecked(6,3);"/></td>
												</tr>
												
												<tr>
													<td>Vessels in History (VC)</td>
													<td align="center"><input type="checkbox" name="checkbox_7_1" id="checkbox_7_1" value="1" onClick="getChecked(7,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_7_2" id="checkbox_7_2" value="2" onClick="getUnChecked(7,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_7_3" id="checkbox_7_3" value="3" onClick="getUnChecked(7,3);"/></td>
												</tr>
                                                
                                                
                                                <tr>
                                                    <td colspan="3"><strong>Time Charterer Out</strong></td>
                                                </tr>
                                                <tr>
													<td>Voyage Estimates(TC)</td>
													<td align="center"><input type="checkbox" name="checkbox_8_1" id="checkbox_8_1" value="1" onClick="getChecked(8,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_8_2" id="checkbox_8_2" value="2" onClick="getUnChecked(8,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_8_3" id="checkbox_8_3" value="3" onClick="getUnChecked(8,3);"/></td>
												</tr>
                                                <tr>
													<td>Decision Chart (TC)</td>
													<td align="center"><input type="checkbox" name="checkbox_9_1" id="checkbox_9_1" value="1" onClick="getChecked(9,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_9_2" id="checkbox_9_2" value="2" onClick="getUnChecked(9,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_9_3" id="checkbox_9_3" value="3" onClick="getUnChecked(9,3);"/></td>
												</tr>
                                                <tr>
													<td>Finalised Voyage Fixtures (TC)</td>
													<td align="center"><input type="checkbox" name="checkbox_10_1" id="checkbox_10_1" value="1" onClick="getChecked(10,1);" /></td>
													<td align="center"><input type="checkbox" name="checkbox_10_2" id="checkbox_10_2" value="2" onClick="getUnChecked(10,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_10_3" id="checkbox_10_3" value="3" onClick="getUnChecked(10,3);"/></td>
												</tr>
												
												
												<tr>
													<td>In Ops at a glance (TC)</td>
													<td align="center"><input type="checkbox" name="checkbox_11_1" id="checkbox_11_1" value="1" onClick="getChecked(5,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_11_2" id="checkbox_11_2" value="2" onClick="getUnChecked(5,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_11_3" id="checkbox_11_3" value="3" onClick="getUnChecked(5,3);"/></td>
												</tr>
												
												<tr>
													<td>Vessels in Post Ops (TC)</td>
													<td align="center"><input type="checkbox" name="checkbox_12_1" id="checkbox_12_1" value="1" onClick="getChecked(12,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_12_2" id="checkbox_12_2" value="2" onClick="getUnChecked(12,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_12_3" id="checkbox_12_3" value="3" onClick="getUnChecked(12,3);"/></td>
												</tr>
												
												<tr>
													<td>Vessels in History (TC)</td>
													<td align="center"><input type="checkbox" name="checkbox_13_1" id="checkbox_13_1" value="1" onClick="getChecked(13,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_13_2" id="checkbox_13_2" value="2" onClick="getUnChecked(13,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_13_3" id="checkbox_13_3" value="3" onClick="getUnChecked(13,3);"/></td>
												</tr>
                                                <tr>
                                                    <td colspan="3"><strong>COA</strong></td>
                                                </tr>
                                                <tr>
													<td>Running COAs</td>
													<td align="center"><input type="checkbox" name="checkbox_14_1" id="checkbox_14_1" value="1" onClick="getChecked(14,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_14_2" id="checkbox_14_2" value="2" onClick="getUnChecked(14,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_14_3" id="checkbox_14_3" value="3" onClick="getUnChecked(14,3);"/></td>
												</tr>
												<tr>
													<td>Cargo Relet(COA)</td>
													<td align="center"><input type="checkbox" name="checkbox_19_1" id="checkbox_19_1" value="1" onClick="getChecked(19,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_19_2" id="checkbox_19_2" value="2" onClick="getUnChecked(19,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_19_3" id="checkbox_19_3" value="3" onClick="getUnChecked(19,3);"/></td>
												</tr>
                                                <tr>
													<td>COA - In Ops</td>
													<td align="center"><input type="checkbox" name="checkbox_15_1" id="checkbox_15_1" value="1" onClick="getChecked(15,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_15_2" id="checkbox_15_2" value="2" onClick="getUnChecked(15,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_15_3" id="checkbox_15_3" value="3" onClick="getUnChecked(15,3);"/></td>
												</tr>
                                                <tr>
													<td>COA - Post Ops</td>
													<td align="center"><input type="checkbox" name="checkbox_16_1" id="checkbox_16_1" value="1" onClick="getChecked(16,1);" /></td>
													<td align="center"><input type="checkbox" name="checkbox_16_2" id="checkbox_16_2" value="2" onClick="getUnChecked(16,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_16_3" id="checkbox_16_3" value="3" onClick="getUnChecked(16,3);"/></td>
												</tr>
												<tr>
													<td>COA - History</td>
													<td align="center"><input type="checkbox" name="checkbox_17_1" id="checkbox_17_1" value="1" onClick="getChecked(17,1);"/></td>
													<td align="center"><input type="checkbox" name="checkbox_17_2" id="checkbox_17_2" value="2" onClick="getUnChecked(17,2);"/></td>
                                                    <td align="center"><input type="checkbox" name="checkbox_17_3" id="checkbox_17_3" value="3" onClick="getUnChecked(17,3);"/></td>
												</tr>
											</tbody>
										</table>
										</div><!-- /.box-body -->
									</div>                          
								</div><!-- /.col -->
							</div>
							</div><!-- /.box-body -->

							<div class="box-footer" align="center">
								<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button><input type="hidden" name="action" value="submit" />
							</div>
						</form>
                </div>
				<!--</div>			-->
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../css/chosen.css" rel="stylesheet" type="text/css" />
<script src="../js/chosen.jquery.js" type="text/javascript"></script>
<script src='../js/jquery.autosize.js'></script>
<link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.alerts.js"></script>
<script src="../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#selCModule,#selRTo").chosen(); 
$('#txtAddress').autosize({append: "\n"});
$("#frm1").validate({
	rules: {
	txtName: {required: true},
	txtEmailid: {required: true , email:true},
	txtUName: {required :true ,remote:'options.php?id=1'},
	txtPassword: {required: true},
	txtCPassword : {required :true, equalTo:"#txtPassword"},
	selStatus:{required: true},
	selGender:{required: true}
	},
messages: {
	txtName: {required: "*"},
	txtEmailid: {required: "*" },
	txtUName: {required :"*" ,remote :"This Name already exists."},
	txtPassword: {required: "*"},
	txtCPassword : {required :"*", equalTo:"Your password does not match with the original password."},
	selStatus:{required: "*"},
	selGender:{required: "*"}
	},
submitHandler: function(form) {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
});
/************************************/
function getValidate()
{
	var arr = new Array(); 
	var arr1 = new Array();
	var i=0;
	$('[id^=checkbox_]').each(function(index) {
	var j = i+1;
			if($(this).attr('checked'))
			{
				arr[i] = j;					
			}
			i++;
		});
	if(arr.length > 0)
	{
		return true;
	}
	else 
	{
		jAlert('Please give minimal authority.', 'Alert');
		return false;
	}
}

/*function getUnChecked(row,col)
{
	if ($("#checkbox_"+row+"_"+col).attr('checked')) 
	{
		$("[id^=checkbox_"+row+"]").iCheck('check');
	}
	else
	{
		$("[id^=checkbox_"+row+"]").iCheck('uncheck');
	}
}*/

function getChecked(row,col)
{
	if($('#checkbox_'+row+'_1').attr('checked'))
	{
		$("[id^=checkbox_"+row+"_2]").iCheck('uncheck');
	}	
}

function getUnChecked(row,col)
{
	if($('#checkbox_'+row+'_2').attr('checked'))
	{
		$("[id^=checkbox_"+row+"_1]").iCheck('uncheck');
	}	
}

/*function getChecked(row,col)
{
	var arr = new Array(); 
	var arr1 = new Array();
	var i=0;
	$('#checkbox_'+row+'_1').each(function(index) {
	var j = i+1;
			if($(this).attr('checked'))
			{
				arr[i] = j;
			}
			i++;
	});
	if(arr.length > 0)
	{
		$("[id^=checkbox_"+row+"_2]").iCheck('check');
	}
	else 
	{
		$("[id^=checkbox_"+row+"_2]").iCheck('uncheck');
	}
}*/
</script>
    </body>
</html>