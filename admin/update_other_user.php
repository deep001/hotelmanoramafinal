<?php 
session_start();
require_once("../includes/display_admin.inc.php");
require_once("../includes/functions_admin.inc.php");
$obj = new data();
$display = new display();
$display->logout_a();
$id = $_REQUEST['id'];
if (@$_REQUEST['action'] == 'submit')
 {
	$msg = $obj->updateOtherUserRecords();
	header('Location:./other_user.php?msg='.$msg);
 }
$obj->viewOtherUserRecords($id);
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(6); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-pagelines"></i>&nbsp;Other User&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Other User</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div align="right"><a href="other_user.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<!--<div class="col-md-6">-->
				<div class="box box-primary">
                        <!-- form start -->
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
						<div class="box-body">
							<div class="box-header">
								<h3 class="box-title">UPDATE USER DETAILS</h3>
							</div><!-- /.box-header -->
							<div class="form-group">
								<label >Company Name</label><br>
								<label style='color:#dc631e;font-size:12px;' >&nbsp;&nbsp;&nbsp;&nbsp;<?php echo strtoupper($obj->getCompanyData($obj->getFun1(),"COMPANY_NAME"));?></label>                                       
							</div>
							<div class="form-group">
								<label for="txtName">Name</label>
								<input type="text" class="form-control" id="txtName" name="txtName" placeholder="Name" autocomplete="off" value="<?php echo $obj->getFun5();?>" />
							</div>
							
							<div class="form-group">
								<label>Address</label>
								<textarea class="form-control" rows="3" name="txtAddress" id="txtAddress" placeholder="Address"><?php echo $obj->getFun2();?></textarea>
							</div>
							<div class="form-group">
								<label for="txtPhoneNo">Phone Number</label>
								<input type="text" class="form-control" id="txtPhoneNo" name="txtPhoneNo" placeholder="Phone Number" autocomplete="off" value="<?php echo $obj->getFun3();?>" />
							</div>
							<div class="form-group">
								<label for="txtEmailid">E-mail ID</label>
								<input type="email" class="form-control" id="txtEmailid" name="txtEmailid" placeholder="E-mail ID" autocomplete="off" value="<?php echo $obj->getFun4();?>" />
							</div>
							
							<div class="form-group">
								<label for="txtUName">User Name</label>
								<input type="text" class="form-control" id="txtUName" name="txtUName" placeholder="User Name" autocomplete="off" value="<?php echo $obj->getFun7();?>" />
							</div>
							
							<div class="form-group">
								<label for="txtPassword">Password</label>
								<input type="text" class="form-control" id="txtPassword" name="txtPassword" placeholder="Password" autocomplete="off" value="<?php echo $obj->getFun8();?>" />
							</div>
							
							<div class="form-group">
								<label for="txtCPassword">Confirm Password</label>
								<input type="text" class="form-control" id="txtCPassword" name="txtCPassword" placeholder="Confirm Password" autocomplete="off" value="<?php echo $obj->getFun8();?>" />
							</div>
							<div class="form-group">
								<label for="selStatus" >Status</label>
								<select  name="selStatus" class="form-control" id="selStatus" >
								<?php 
									$_REQUEST['selStatus'] = $obj->getFun6();
									$obj->getUserStatusList();
								?>
								</select>                                         
							</div>
						</div><!-- /.box-body -->
						<div class="box-body">
							<div class="box-header">
								<h3 class="box-title">USER RIGHTS</h3>
							</div><!-- /.box-header -->
							<div class="form-group">
								<label for="txtPhoneNo">Modules</label>
								<select data-placeholder="Choose a module here..." class="chzn-select" style="width:800px;" multiple name="selCProduct[]" id="selCProduct">
									<?php $obj->getProductListForUserForUpdate($obj->getFun9());?>
								</select>
							</div>
							<div class="form-group">
								<label for="txtEmailid">User Type</label>
								<select  name="selUType" id="selUType" class="form-control">
									<?php 
									$_REQUEST['selUType'] = $obj->getFun10();
									$obj->getExternalUserTypeList();
									?>
								</select>
							</div>
						</div>
						<div class="box-footer" align="center">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button><input type="hidden" name="action" value="submit" />
						</div>
					</form>
                </div>
				<!--</div>			-->
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../css/chosen.css" rel="stylesheet" type="text/css" />
<script src="../js/chosen.jquery.js" type="text/javascript"></script>
<script src='../js/jquery.autosize.js'></script>
<link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.alerts.js"></script>
<script src="../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#selCProduct").chosen(); 
$('#txtAddress').autosize({append: "\n"});
$("#frm1").validate({
	rules: {
	txtName: {required: true},
	txtEmailid: {required: true , email:true},
	txtUName: {required :true},
	txtPassword: {required: true},
	txtCPassword : {required :true, equalTo:"#txtPassword"},
	selStatus:{required: true},
	selGender:{required: true}
	},
messages: {
	txtName: {required: "*"},
	txtEmailid: {required: "*" },
	txtUName: {required :"*" ,remote :"This Name already exists."},
	txtPassword: {required: "*"},
	txtCPassword : {required :"*", equalTo:"Your password does not match with the original password."},
	selStatus:{required: "*"},
	selGender:{required: "*"}
	},
submitHandler: function(form) {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});
});

function getValidate()
{
	if($("#selCProduct").val() == null)
	{
		jAlert('Please choose a module for user', 'Alert');
		return false;
	}
	else
	{
		return true;
	}
}
</script>
    </body>
</html>