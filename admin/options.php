<?php
session_start();
include_once("../includes/functions_admin.inc.php");
@$i = $_REQUEST['id'];
switch ($i)
{
	case 1:		
        $ret = CheckUserValidate();
				break;
	case 2:
        $ret = checkDistance();
				break;
	case 3:		
        $ret = checkDuplicateVessel();
				break;
	case 4:		
        $ret = checkDuplicateIMONo();
				break;
	case 5:		
        $ret = getPortList();
				break;
	case 6:		
        $ret = getDistanceList();
				break;
	default:
		    $ret = "";
				//echo $ret;
				break; 				
}


function getDistanceList()
{
	$obj = new data();
	$connect=$obj->funConnect();
	
	$aColumns = array("DISTANCEID", "FROMPORTID", "TOPORTID", "SHORT_DIS", "NM250_DIS", "NM600_DIS", "LONG_DIS");
	
	$sIndexColumn = "DISTANCEID";
	
	$sTable = "distance_master";
	
	if(!empty($_POST['order'][0]['column']))
	{
		$sOrder = "ORDER BY  ".$aColumns[intval($_POST['order'][0]['column'])]." ".$_POST['order'][0]['dir'];
	}
	else
	{
		$sOrder = "ORDER BY FROMPORTID";
	}
	
	$sWhere = "";
	if ($_POST['search']['value']!= "")
	{
		$sWhere = "where (FROMPORTID LIKE '%".$_POST['search']['value']."%' or TOPORTID LIKE '%".$_POST['search']['value']."%' or SHORT_DIS LIKE '%".$_POST['search']['value']."%' or NM250_DIS LIKE '%".$_POST['search']['value']."%' or NM600_DIS LIKE '%".$_POST['search']['value']."%' or LONG_DIS LIKE '%".$_POST['search']['value']."%')";
	}
	
	$sQuery = "SELECT * FROM (select DISTANCEID, (select PortName from port_master where port_master.PortId=distance_master.FROMPORTID) as FROMPORTID, (select PortName from port_master where port_master.PortId=distance_master.TOPORTID) as TOPORTID, SHORT_DIS, NM250_DIS, NM600_DIS, LONG_DIS from distance_master) as innerTable $sWhere $sOrder LIMIT ".$_POST['start'].", ".$_POST['length']."";
	$rResult = mysql_query($sQuery);
	
	$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM (select * from distance_master) as innerTable $sWhere";
	
	$rResultFilterTotal = mysql_query($sQuery);
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$totalFiltered = $aResultFilterTotal[0];
	
	$sQuery = " SELECT COUNT(".$sIndexColumn.") FROM $sTable";
	$rResultTotal = mysql_query($sQuery);
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$totalFiltered = $totalData = $aResultTotal[0];
	
	$aaData = array();
	$i = $_POST['start'];
	while ( $aRow = mysql_fetch_array( $rResult ) )
	{
		$row = array(); $i = $i+1;
		
		//$details = '<a title="Delete record" href="#'.$i.'" onclick="return getDelete('.$aRow['PortId'].');" ><i class="fa fa-times" style="color:red;"></i></a>';
		
		$aaData[] = array('col1'=>$i.'.', 'col2'=>$aRow['FROMPORTID'], 'col3'=>$aRow['TOPORTID'], 'col4'=>$aRow['SHORT_DIS'], 'col5'=>$aRow['NM250_DIS'], 'col6'=>$aRow['NM600_DIS'], 'col7'=>$aRow['LONG_DIS']);
	}
	
	echo json_encode(array("draw"=>intval($_POST['draw']),"recordsTotal"=>$totalData,"recordsFiltered"=>$totalFiltered,"records" => $aaData));
}

function getPortList()
{
	$obj = new data();
	$connect=$obj->funConnect();
	
	$aColumns = array("PortId", "PortName", "PortCode", "COUNTRY_KEY", "COUNTRY_NAME");
	
	$sIndexColumn = "PortId";
	
	$sTable = "port_master";
	
	if(!empty($_POST['order'][0]['column']))
	{
		$sOrder = "ORDER BY  ".$aColumns[intval($_POST['order'][0]['column'])]." ".$_POST['order'][0]['dir'];
	}
	else
	{
		$sOrder = "ORDER BY PortName";
	}
	
	$sWhere = "";
	if ($_POST['search']['value']!= "")
	{
		$sWhere = "where (PortName LIKE '%".$_POST['search']['value']."%' or PortCode LIKE '%".$_POST['search']['value']."%' or COUNTRY_KEY LIKE '%".$_POST['search']['value']."%' or COUNTRYNAME1 LIKE '%".$_POST['search']['value']."%')";
	}
	
	$sQuery = "SELECT * FROM (select PortId, PortName, PortCode, COUNTRY_KEY, COUNTRY_NAME, (select COUNTRY_NAME from country_master where country_master.COUNTRYID=port_master.COUNTRY_NAME) as COUNTRYNAME1 from port_master) as innerTable $sWhere $sOrder LIMIT ".$_POST['start'].", ".$_POST['length']."";
	$rResult = mysql_query($sQuery);
	
	$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM (select * from port_master) as innerTable $sWhere";
	
	$rResultFilterTotal = mysql_query($sQuery);
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$totalFiltered = $aResultFilterTotal[0];
	
	$sQuery = " SELECT COUNT(".$sIndexColumn.") FROM $sTable";
	$rResultTotal = mysql_query($sQuery);
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$totalFiltered = $totalData = $aResultTotal[0];
	
	$aaData = array();
	$i = $_POST['start'];
	while ( $aRow = mysql_fetch_array( $rResult ) )
	{
		$row = array(); $i = $i+1;
		
		$details = '<a title="Delete record" href="#'.$i.'" onclick="return getDelete('.$aRow['PortId'].');" ><i class="fa fa-times" style="color:red;"></i></a>';
		
		$aaData[] = array('col1'=>$i.'.', 'col2'=>$aRow['PortName'], 'col3'=>$aRow['PortCode'], 'col4'=>$aRow['COUNTRY_KEY'], 'col5'=>$aRow['COUNTRYNAME1'],'col6'=>$details);
	}
	
	echo json_encode(array("draw"=>intval($_POST['draw']),"recordsTotal"=>$totalData,"recordsFiltered"=>$totalFiltered,"records" => $aaData));
}


function checkDuplicateIMONo()
{
	$obj = new data();
	$connect=$obj->funConnect();
	$txtIMONo = trim(ereg_replace(' +',' ',$_REQUEST['txtIMONo']));
	$query = mysql_query("select * from vessel_imo_master where MCOMPANYID='".$_SESSION['company']."' and IMO_NO='".$txtIMONo."' ");
	$num=mysql_num_rows($query);
		if($num > 0)
		{
			echo 'false';
		}
		else
		{
			echo 'true';
		}
}

function checkDuplicateVessel()
{
	$obj = new data();
	$connect=$obj->funConnect();
	$vessel_name = trim(ereg_replace(' +',' ',$_REQUEST['txtVName']));
	$query = mysql_query("select * from vessel_imo_master where MCOMPANYID='".$_SESSION['company']."' and VESSEL_NAME='".$vessel_name."' ");
	$num=mysql_num_rows($query);
		if($num > 0)
		{
			echo 'false';
		}
		else
		{
			echo 'true';
		}
}

function CheckUserValidate()
{
	$obj = new data();
	$connect=$obj->funConnect();
	$txtUName = trim(ereg_replace(' +',' ',$_REQUEST['txtUName']));
	$query = "select * from login  where USERNAME='".$txtUName."'";
	$result = mysql_query($query) or die(mysql_error());
	$num = mysql_num_rows($result);

	if($num > 0)
	{
		echo 'false';
	}
	else
	{
		echo 'true';
	}
	
}

function checkDistance()
{
	$obj = new data();
	$connect=$obj->funConnect();
	$selFPort = $_REQUEST['selFPort'];
	$selTPort = $_REQUEST['selTPort'];
	$selRoute = $_REQUEST['selRoute'];
	
	$sql = "select * from distance_master where FROMPORTID='".$selFPort."' and ROUTE='".$selRoute."' and TOPORTID='".$selTPort."'" ;
	$result = mysql_query($sql);
	$rec = mysql_num_rows($result);
	if($rec == 0)
	{
		$sql1 = "select * from distance_master where FROMPORTID='".$selTPort."' and ROUTE='".$selRoute."' and TOPORTID='".$selFPort."'" ;
		$result1 = mysql_query($sql1);
		$rec1 = mysql_num_rows($result1);
		if($rec1 == 0)
		{
			echo '0#0#0#0#';
		}
		else
		{
			$rows1 = mysql_fetch_assoc($result1);
			echo $rows1['SHORT_DIS'].'#'.$rows1['NM250_DIS'].'#'.$rows1['NM600_DIS'].'#'.$rows1['LONG_DIS'].'#'.$rows1['STATUS'];
		}
	}
	else
	{
		$rows = mysql_fetch_assoc($result);
		echo $rows['SHORT_DIS'].'#'.$rows['NM250_DIS'].'#'.$rows['NM600_DIS'].'#'.$rows['LONG_DIS'].'#'.$rows['STATUS'];
		//echo $rows['Unit'];
	}
}
?>