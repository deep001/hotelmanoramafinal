<?php 
session_start();
require_once("../includes/display_admin.inc.php");
require_once("../includes/functions_admin.inc.php");
$obj = new data();
$display = new display();
$display->logout_a();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertChartererPartyDetails();
	header('Location:./cp_master.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
/* Button */
.ox-button { font-size: 8pt;line-height: 1.5em;border-radius: 5px 5px 5px 5px;background-color: #f5f5f5;border: 1px solid #ccc;color: black;cursor: pointer;padding: 3px 1em 3px 1em;white-space: nowrap;display: inline-block;vertical-align: middle; }
.ox-button:hover { border-color: #888; }
/* firefox */
.firefox .ox-button { -moz-border-radius: 5px; background-image: -moz-linear-gradient(center top, #ffffff, #efefef); }
.firefox .ox-button:hover { -moz-box-shadow: 0px 0px 4px #aaa; }
/* webkit */
.webkit .ox-button { -webkit-border-radius: 5px;background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(white), to(#EFEFEF)); }
.webkit .ox-button:hover { -webkit-box-shadow: 0px 0px 4px #aaa; }
/* IE9 */
.IE9 .ox-button { filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#efefef'); }
.IE9 .ox-button:hover { box-shadow: 0px 0px 4px #aaa; }

</style>
</head>
    <body class="skin-blue fixed">
	<div id="basic-modal-content" align="center">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr><td>
	<form id="imageform" method="post" enctype="multipart/form-data" action="../includes/upload_files.php">
	<input type="file" name="photoimg" id="photoimg" class="ox-button" />
	</form>
	</td></tr>
	</table>
	</div>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(4); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Charterer Party</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div align="right"><a href="cp_master.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				
				<!--<div class="col-md-6">-->
				<div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">ADD NEW CHARTERER PARTY</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                                    <div class="box-body">
										<div class="form-group">
                                            <label for="txtCPName">CP Name</label>
                                            <input type="text" class="form-control" id="txtCPName" name="txtCPName" placeholder="CP Name" autocomplete="off">
                                        </div>
										
										<div class="form-group">
                                            <label for="selCType" >Cargo Type</label>
											<select  name="selCType" class="form-control" id="selCType" >
											<?php 
											$obj->getCargoTypeList();
											?>
											</select>                                         
                                        </div>
										
										<div class="form-group">
                                            <label for="selCPType" >CP Type</label>
											<select  name="selCPType" class="form-control" id="selCPType" >
											<?php 
											$obj->getCPTypeList();
											?>
											</select>                                         
                                        </div>
										
										<div class="form-group">
                                            <label for="txtCPRef">CP Ref</label>
                                            <input type="text" class="form-control" id="txtCPRef" name="txtCPRef" placeholder="CP Ref" autocomplete="off">
                                        </div>
										
										<div class="form-group">
                                            <label for="selStatus" >Status</label>
											<select  name="selStatus" class="form-control" id="selStatus" >
											<?php 
											$obj->getUserStatusList();
											?>
											</select>                                         
                                        </div>
										
										<div class="form-group">
                                            <label for="txtCPRef">Upload <span style="font-size:9px; font-style:italic;">(PDF)</span></label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a title="Upload" href="#" onClick="openWin1();" ><i class="fa fa-upload " style="font-size:22px;"></i></a>
                                        </div>
										
                                    </div><!-- /.box-body -->

                                    <div class="box-footer" align="center">
                                        <button type="submit" class="btn btn-primary btn-flat">Submit</button><input type="hidden" name="action" value="submit" />
                                    </div>
                                </form>
                            </div>
					<!--</div>			-->
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.alerts.js"></script>
<script type='text/javascript' src='../js/jquery.simplemodal.js'></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<link type='text/css' href='../css/basic.css' rel='stylesheet' media='screen' />
<script src="../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#frm1").validate({
	rules: {
	txtCPName:"required",
	selCType:"required",
	selCPType:"required",
	txtCPRef:"required",
	selStatus:"required"
	},
messages: {	
	txtCPName:"*",
	selCType:"*",
	selCPType:"*",
	txtCPRef:"*",
	selStatus:"*"
	},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

$('#photoimg').live('change', function(){
			var str = $('#photoimg').val(); 
			var array = str.substr(-3,3).toLowerCase();
			if(array == 'pdf')
			{
				$("#imageform").ajaxForm({}).submit();
				$.modal.close();
			}
			else
			{
				$.modal.close();
				jAlert('only pdf format file uploaded', 'Alert');
				//alert('only pdf format file uploaded');
			}
			});
});

function openWin1()
{
	$('#basic-modal-content').modal();
}

</script>
    </body>
</html>