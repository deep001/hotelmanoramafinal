<?php 
session_start();
require_once("../includes/display_admin.inc.php");
require_once("../includes/functions_admin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_a();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->deletePortMasterRecords();
	header('Location:./port_list.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(4); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Port</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b>  Port Details added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating Port Details.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Record removed successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
				<h3 style=" text-align:center;">Port List</h3>
				<div align="right"><a href="addport.php" title="Add New"><button class="btn btn-info btn-flat">Add New</button></a>&nbsp;&nbsp;&nbsp;&nbsp;</div>
				<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
				<div style="height:10px;">&nbsp;<input type="hidden" name="txtID" id="txtID" /><input type="hidden" name="action" id="action"  value="submit"/></div>
				
				<div class="box-body table-responsive">
				<table id="port_list" class="table table-striped display">
					<thead>
					<tr>
					<th align="center" valign="middle" width="5%" >#</th>
					<th align="left" valign="middle" width="18%"  >Port Name</th>
					<th align="left" valign="middle" width="18%"  >Port Code</th>
					<th align="left" valign="middle" width="18%"  >Country Key</th>
					<th align="left" valign="middle" width="18%"  >Country Name</th>
					<th align="center" valign="middle" width="5%" >Details</th>
					</tr>
					</thead>
				</table>
				</div>
				</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../css/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<script src="../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.alerts.js"></script>
<script src="../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	getDefaultData();
});


function getDefaultData()
{
		var table = $('#port_list').dataTable();
		if(table != null)table.fnDestroy();
	    
		table = $("#port_list").DataTable( {
			"processing": true,
			"serverSide": true,
			"stateSave": true,
			"order": [[1, 'desc']],
			"ajax": {
				"url": "options.php?id=5",
				"type": "POST",
				"dataSrc": function ( d ) {
					return d.records;
				},	
				"data": function ( d ) {},
			},
			"columns": [
				{ "data": "col1" },
				{ "data": "col2" },
				{ "data": "col3" },
				{ "data": "col4" },
				{ "data": "col5" },
				{ "data": "col6" },
				
			],
			"language": {
				  "zeroRecords": "SORRY CURRENTLY THERE ARE ZERO(0) RECORDS"
				},
			 "fnDrawCallback": function(oSettings, json) {
				  $('[data-toggle="tooltip"]').tooltip();	
				},	
			"columnDefs": [ 
					{
						"targets": 5,
						"orderable": false
					} 
				]
					
		});
}

function getDelete(distanceid)
{
	jConfirm('Are you sure you want to delete this entry?', 'Confirmation', function(r) {
	if(r){ 
		$("#txtID").val(distanceid);
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		document.frm1.submit();
	}
	else{
		return false;
		}
	});
}
</script>
		
    </body>
</html>